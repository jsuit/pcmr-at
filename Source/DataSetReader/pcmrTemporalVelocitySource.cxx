#include "pcmrException.h"
#include "pcmrTemporalVelocitySource.h"
#include "pcmrFlow4DReader.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkStreamingDemandDrivenPipeline.h"
#include "vtkDataArray.h"
#include <vtkPointData.h>
#include "pcmrTrackVTKObject.h"

BEGIN_PCMR_DECLS

#define __TRACK_POINTERS__
#ifdef __TRACK_POINTERS__
#define TRACK_POINTER(m,p)                                      \
  if(m)                                                         \
    {                                                           \
    static_cast<TrackVTKObject*>(m)->TrackObjectPointer(p);     \
    }
#else
#define TRACK_POINTER(m,p)
#endif

//#define __TRACE_REQUEST_DATA__

#define __DEBUG_HERE__(msg)                                     \
  std::cout << msg << std::endl;                                \
  std::cout << __FILE__ << ":" << __LINE__ << std::endl;

vtkStandardNewMacro(Flow4DReaderAccessor);
vtkStandardNewMacro(TemporalVelocitySource);

int RescaleVelocity(vtkDataArray *vectorData, double factor)
{
  if (vectorData->GetNumberOfComponents() != 3 )
    {
    std::cout << "RescaleVelocity: vectorData must be a vector array\n";
    return 0;
    }
  double v[3];
  for(vtkIdType i = 0; i < vectorData->GetNumberOfTuples(); i++)
    {
    vectorData->GetTuple(i, v);
    for(size_t j = 0; j < 3; j++)
      {
      v[j] *= factor;
      }
    vectorData->SetTuple(i, v);
    }
  return 1;
}

Flow4DReaderAccessor::Flow4DReaderAccessor()
{
  this->m_DataSetReader = NULL;
}

Flow4DReaderAccessor::~Flow4DReaderAccessor()
{
}

void Flow4DReaderAccessor::SetFlow4DReader(Flow4DReader *reader)
{
  this->m_DataSetReader = reader;
}

size_t Flow4DReaderAccessor::GetNumberOfTimeSteps()
{
  return this->m_DataSetReader->GetNumberOfTimeSteps();
}

double Flow4DReaderAccessor::GetLengthOfTimeStep()
{
  return this->m_DataSetReader->GetLengthOfTimeStep();
}

void Flow4DReaderAccessor::GetTimeValues(std::vector<double>& timeValues)
{
  this->m_DataSetReader->GetTimeValues(timeValues);
}

vtkImageData *Flow4DReaderAccessor::GetImage(size_t t)
{
  return this->m_DataSetReader->GetFlowImage(pcmr::Flow4DReader::FlowVector, t);
}

void TemporalVelocitySource::SetDataSetAccessor(DataFlowAccessor *accessor)
{
  this->m_DataFlowAccessor = accessor;
  this->m_TimeValues.clear();
  accessor->GetTimeValues(this->m_TimeValues);
  if (this->m_TimeValues.size()!=accessor->GetNumberOfTimeSteps())
    {
    std::cerr << "TimeValues length does not match GetNumberOfTimeSteps\n";
    this->m_TimeValues.clear();
    return;
    }
  for(size_t i = 0; i < this->m_TimeValues.size(); i++)
    {
    // assume it is given in mili-seconds
    this->m_TimeValues[i] /= 1000.0;
    }
  this->Modified();
}

float TemporalVelocitySource::GetLengthOfTimeStep() const
{
  // REVIEW: here we are assuming the length of timestep is given in
  // milliseconds
  return this->m_DataFlowAccessor?
    this->m_DataFlowAccessor->GetLengthOfTimeStep()/1000.0 : 0.0;
}

int TemporalVelocitySource::FindIndexForTime(double t)
{
  const std::vector<double> &timeValues = this->GetTimeValues();
  if (timeValues.size() == 0 || t < timeValues[0])
    {
    return -1;
    }
  const double deltaT = this->GetLengthOfTimeStep();
  const double eps = deltaT * 1.0e-6;
  for(size_t i = 0; i < timeValues.size(); i++)
    {
    if(fabs(t - timeValues[i])<eps)
      {
      return static_cast<int>(i);
      }
    }
  return -1;
}

int TemporalVelocitySource::ProcessRequest(vtkInformation* request,
                                   vtkInformationVector** inputVector,
                                   vtkInformationVector* outputVector)
{
  // generate the data
  if(request->Has(vtkDemandDrivenPipeline::REQUEST_DATA()))
    {
    return this->RequestData(request, inputVector, outputVector);
    }
  
  // execute information
  if(request->Has(vtkDemandDrivenPipeline::REQUEST_INFORMATION()))
    {
    return this->RequestInformation(request, inputVector, outputVector);
    }
  return this->Superclass::ProcessRequest(request, inputVector, outputVector);
}

int TemporalVelocitySource::FillOutputPortInformation(int, vtkInformation *info)
{
  info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkImageData");
  return 1;
}

int TemporalVelocitySource::RequestInformation(vtkInformation *,
                                       vtkInformationVector **,
                                       vtkInformationVector *outputInfoVector)
{
  // get the info objects
  vtkInformation *outInfo = outputInfoVector->GetInformationObject(0);
    
  int T = this->GetNumberOfTimeStepsInternal();
  if (T<1 || this->m_TimeValues.size() != T)
    {
    return 0;
    }
  double range[2];
  range[0] = this->m_TimeValues[0 + this->GetBaseTimeStep()];
  range[1] = this->m_TimeValues[T - 1];
  outInfo->Set(vtkStreamingDemandDrivenPipeline::TIME_RANGE(),
               range,2);
  outInfo->Set(vtkStreamingDemandDrivenPipeline::TIME_STEPS(),
               &(this->m_TimeValues[0+this->GetBaseTimeStep()]), 
               static_cast<int>(this->GetNumberOfTimeSteps()));

  pcmr::vtkImagePointer img = this->m_DataFlowAccessor->GetImage(0);
  if (!img)
    {
    throw exception_null_pointer();
    }
  int extent[6];
  img->GetExtent(extent);
  outInfo->Set(vtkStreamingDemandDrivenPipeline::WHOLE_EXTENT(), extent, 6);
  
  double spacing[3];
  img->GetSpacing(spacing);
  outInfo->Set(vtkDataObject::SPACING(), spacing[0], spacing[1], spacing[2]);
  
  double origin[3];
  img->GetOrigin(origin);
  outInfo->Set(vtkDataObject::ORIGIN(), origin, 3);

  /*
  // ascending point
  int IJK0[] = {65, 115, 4};
  // descending point
  int IJK1[] = {111, 75, 10};
  double px = origin[0] + IJK1[0] * spacing[0];
  double py = origin[1] + IJK1[1] * spacing[1];
  double pz = origin[2] + IJK1[2] * spacing[2];
  std::cout << "p=(" << px << "," << py << "," << pz << ")\n";
  */
  return 1;
}

int TemporalVelocitySource::RequestData(vtkInformation*,
                                        vtkInformationVector** vtkNotUsed(inputVector),
                                        vtkInformationVector* outputVector)
{
  this->m_NumRequestData++;
  vtkInformation *outInfo = outputVector->GetInformationObject(0);
  vtkDataObject* output = outInfo->Get(vtkDataObject::DATA_OBJECT());
  
  double timeStep = outInfo->Get(vtkStreamingDemandDrivenPipeline::UPDATE_TIME_STEP());
  
#ifdef __TRACE_REQUEST_DATA__
  std::cout << "output = " << output << "\n";
  std::cout << "requested ts = " << timeStep << "\n";
#endif
  output->GetInformation()->Set(vtkDataObject::DATA_TIME_STEP(), timeStep);
  int idx = this->FindIndexForTime(timeStep);
  if (idx == -1)
    {
    std::cerr << "could not find time interval for time-value" << timeStep << std::endl;
    return 0;
    }
#ifdef __TRACE_REQUEST_DATA__
  std::cout << "requested idx = " << idx << "\n";
#endif
  
  pcmr::vtkImagePointer imgFlow = this->m_DataFlowAccessor->GetImage(idx);
  if (!imgFlow)
    {
    throw exception_null_pointer();
    }
  // shallow copy the vector image to output
  // set the extent to be the update extent
  vtkImageData *outImage = vtkImageData::SafeDownCast(output);
  if (!outImage)
    {
    std::cerr << "could not convert output to vtkImageData\n";
    return 0;
    }
  
#ifdef __TRACE_REQUEST_DATA__
  std::cout << "outImage = " << outImage << "\n";
#endif
  outImage->DeepCopy(imgFlow);
  TRACK_POINTER(this->m_PointerTracker, outImage);

  vtkDataArray *pointVectors = outImage->GetPointData()->GetVectors();
  // REVIEW: converting to mm/s
  RescaleVelocity(pointVectors, this->GetRescaleFactor());
  return 1;
}

END_PCMR_DECLS
