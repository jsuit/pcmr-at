#include "pcmrFlow4DReader.h"
#include <algorithm>
#include <boost/filesystem.hpp>
using namespace boost::filesystem;
#include <boost/foreach.hpp>
#include "itkImageFileReader.h"
#include "itkChangeInformationImageFilter.h"
#include "itkComposeImageFilter.h"
#include "itkShiftScaleImageFilter.h"
#include "vtkPointData.h"
#include "vtkDataArray.h"
#include "vtkImageMagnitude.h"

BEGIN_PCMR_DECLS

const double Flow4DReader::LargeArteryBloodViscosity = 3.5e-3;

Flow4DReader::Flow4DReader()
{
  this->m_HeaderReader = HeaderReader::New();
  this->m_PremaskImage = NULL;
  this->m_LengthOfTimeStep = 0.0;
}

Flow4DReader::~Flow4DReader()
{
  this->ReleaseImages();
}

StatusType Flow4DReader::Clear()
{
  this->ReleaseImages();
  this->m_LengthOfTimeStep = 0.0;
  this->m_TimeValues.clear();
  return OK;
}

StatusType Flow4DReader::SetDirectory(const char* directory)
{
  path path_header(directory);

  this->ReleaseImages();

  path_header /= "header.xml";
  
  if (!exists(path_header))
    {
    return PATH_NOEXISTS;
    }
  this->m_Directory = directory;
  this->m_HeaderReader->SetFileName(path_header.string().c_str());
  this->ComputeTimeValues();
  this->ResizeContainers();
  this->LoadPremaskImage();
  return OK;
}

void Flow4DReader::ResizeContainers()
{
  size_t n = this->GetNumberOfTimeSteps();
  this->m_FlowXImage.resize(n);
  this->m_FlowYImage.resize(n);
  this->m_FlowZImage.resize(n);
  this->m_FlowVectorImage.resize(n);
  this->m_FlowModulusImage.resize(n);
  this->m_MagnitudeImage.resize(n);
  this->m_MaskImage.resize(n);
  //this->m_ItkFlowVectorImage(n);
}

StatusType Flow4DReader::ComputeTimeValues()
{
  StatusType status = this->m_HeaderReader->GetTimeValues(this->m_TimeValues);
  if (status == OK)
    {
    double dt = 0;
    BOOST_FOREACH(const std::vector<double>::value_type &t, this->m_TimeValues)
      {
      dt += t;
      }
    dt /= this->m_TimeValues.size();
    this->m_LengthOfTimeStep = dt;
    return OK;
    }
  if (status == TAG_NOTFOUND)
    {
    size_t n = this->m_HeaderReader->GetNumberOfTimeSteps();
    if (n == 0)
      {
       std::cerr << "Loaded <NumberOfTimeSteps> " << n << "is invalid, must be positive";
      return INV_TIMESTEP;
     }
    this->m_LengthOfTimeStep = this->m_HeaderReader->GetLengthOfTimeStep();
    if (this->m_LengthOfTimeStep <= 0)
      {
      std::cerr << "Loaded <TimeStepLength> " << this->m_LengthOfTimeStep << "is invalid, must be positive";
      return INV_TIMESTEP;
      }
    for(size_t i = 0; i < n; i++)
      {
      this->m_TimeValues.push_back(this->m_LengthOfTimeStep*i);
      }
    return OK;
    }
  return status;
}

int Flow4DReader::GetTimeValues(std::vector<double> & timeValues) const
{
  timeValues.resize(this->m_TimeValues.size());
  std::copy(this->m_TimeValues.begin(), this->m_TimeValues.end(), timeValues.begin());
  return 0;
}

void Flow4DReader::ReleaseImages()
{
  this->m_FlowXImage.clear();
  this->m_FlowYImage.clear();
  this->m_FlowZImage.clear();
  this->m_FlowVectorImage.clear();
  this->m_FlowModulusImage.clear();
  this->m_MagnitudeImage.clear();
  /*
  for(size_t i = 0; i < this->m_MaskImage.size(); ++i)
    {
    if (this->m_MaskImage[i])
      {
      std::cout << "mask("<< i << ") = " 
                << this->m_MaskImage[i]->GetReferenceCount() << std::endl;
      }
    }
  */
  this->m_MaskImage.clear();
  //this->m_ItkFlowVectorImage.clear();
  this->m_PremaskImage = NULL;
  this->CheckTrackedPointers();
  this->m_PointerMap.clear();
}

void Flow4DReader::OnImageDelete(vtkObject *obj, unsigned long event, void*data)
{
  vtkImageData *ptrImage = dynamic_cast<vtkImageData*>(obj);
  if (!ptrImage)
    {
    std::cerr << "Flow4DReader::OnImageDelete error: could not convert " << obj << " to vtkImageData*\n";
    return;
    }
  PointerMap::iterator it = this->m_PointerMap.find(ptrImage);
  if (it == this->m_PointerMap.end())
    {
    std::cerr << "vtkImageData at " << ptrImage << " not found inside map\n";
    }
  else
    {
    if (!it->second)
      {
      std::cerr << "vtkImageData at " << it->first << " has being already delete\n";
      }
    it->second = 0;
    }
}

void Flow4DReader::CheckTrackedPointers()
{
  bool found = false;
  BOOST_FOREACH(PointerMap::value_type i, this->m_PointerMap)
    {
    if (i.second)
      {
      std::cerr << "The vtkImageData* " << i.first << " is still alive\n";
      found = true;
      }
    }
  if (!found)
    {
    std::cerr << "All allocated vtkImageData has being released\n";
    }
}

void Flow4DReader::TrackImageDataPointer(vtkImageData *ptr)
{
  /*
  std::cout << "TrackImageDataPointer: " 
            << ptr << "(" << ptr->GetReferenceCount() <<")" <<
            std::endl;
  */
  this->m_PointerMap[ptr] = 1;
  ptr->AddObserver(vtkCommand::DeleteEvent,
                   this, &Flow4DReader::OnImageDelete);
}

vtkImageData* Flow4DReader::GetFlowImage(Component component, size_t timestep)
{
  StatusType status = this->LoadTimeStep(timestep);
  if (status != OK)
    {
    std::cerr << "GetFlowImage failed: " <<pcmr::GetStatusDescription(status) << "\n"; 
    return NULL;
    }
  vtkImageData *image = NULL;
  switch (component)
    {
    case FlowX:
      image = this->m_FlowXImage[timestep];
      break;
    case FlowY:
      image = this->m_FlowYImage[timestep];
      break;
    case FlowZ:
      image = this->m_FlowZImage[timestep];
      break;
    case FlowModulus:
      image = this->m_FlowModulusImage[timestep];
      break;
    case FlowVector:
      image = this->m_FlowVectorImage[timestep];
      break;
    }
  return image;
}

vtkImageData* Flow4DReader::GetMagnitudeImage(size_t timestep)
{
  StatusType status = this->LoadTimeStep(timestep);
  if (status != OK)
    {
    return NULL;
    }
  return this->m_MagnitudeImage[timestep];
}

vtkImageData* Flow4DReader::GetMaskImage(size_t timestep)
{
  StatusType status = this->LoadMaskForTimeStep(timestep);
  if (status != OK)
    {
    return NULL;
    }
  return this->m_MaskImage[timestep];
}

StatusType Flow4DReader::LoadMaskForTimeStep(size_t timestep)
{
  std::string pathMask;
  StatusType status;
  
  if (this->m_MaskImage[timestep])
    {
    return OK;
    }
  status = this->GetMaskFileName(timestep, pathMask);
  if (status != OK)
    {
    return status;
    }
  typedef itk::ImageFileReader<itk::MaskImage> MaskReaderType;
  MaskReaderType::Pointer reader = MaskReaderType::New();
  reader->SetFileName(pathMask);
  reader->Update();
  this->m_MaskImage[timestep] = this->ConvertItkImageToVtkImage<itk::MaskImage>(reader->GetOutput());
  return OK;
}

StatusType Flow4DReader::LoadTimeStep(size_t timestep)
{
  std::string pathFlowX, pathFlowY, pathFlowZ, pathMagnitude;
  StatusType status;
  
  if (this->m_FlowVectorImage[timestep])
    {
    return OK;
    }
  status = this->GetFlowFileName(timestep, FileFlowX, pathFlowX);
  if (status != OK)
    {
    return status;
    }
  status = this->GetFlowFileName(timestep, FileFlowY, pathFlowY);
  if (status != OK)
    {
    return status;
    }
  status = this->GetFlowFileName(timestep, FileFlowZ, pathFlowZ);
  if (status != OK)
    {
    return status;
    }
#if !defined(__PMIFlowComputation_DLL__)
  status = this->GetFlowFileName(timestep, FileMagnitude, pathMagnitude);
  if (status != OK)
    {
    return status;
    }
#endif
  itk::PhaseContrast3DImage::Pointer flowXImage = 
    this->LoadItkImage(pathFlowX, this->GetInvertComponentX());
  itk::PhaseContrast3DImage::Pointer flowYImage =
    this->LoadItkImage(pathFlowY, this->GetInvertComponentY());
  itk::PhaseContrast3DImage::Pointer flowZImage =
    this->LoadItkImage(pathFlowZ, this->GetInvertComponentZ());
  this->m_FlowXImage[timestep] = this->ConvertItkImageToVtkImage<itk::PhaseContrast3DImage>(flowXImage);
  this->m_FlowYImage[timestep] = this->ConvertItkImageToVtkImage<itk::PhaseContrast3DImage>(flowYImage);
  this->m_FlowZImage[timestep] = this->ConvertItkImageToVtkImage<itk::PhaseContrast3DImage>(flowZImage);

#if !defined(__PMIFlowComputation_DLL__)
  itk::PhaseContrast3DImage::Pointer magImage =
    this->LoadItkImage(pathMagnitude, false);
  this->m_MagnitudeImage[timestep] = this->ConvertItkImageToVtkImage<itk::PhaseContrast3DImage>(magImage);
#endif

  this->BuildVectorImage(flowXImage, flowYImage, flowZImage, timestep);
  return OK;
}

StatusType Flow4DReader::GetMaskFileName(size_t timestep, std::string &fileName)
{
  if (timestep > this->GetNumberOfTimeSteps())
    {
    return INV_TIMESTEP;
    }
  path base(this->GetDirectory());
  base /= "mask";
  
  std::stringstream ts; ts << timestep << ".vtk";
  fileName = base.string();
  fileName += "_";
  fileName += ts.str();
  if (exists(fileName))
    {
    return OK;
    }
  fileName = base.string();
  fileName += ".vtk";
  if (exists(fileName))
    {
    return OK;
    }
  std::cerr << "File \"" << fileName << "\" does not exists\n";
  return PATH_NOEXISTS;
}

StatusType Flow4DReader::GetFlowFileName(size_t timestep, 
                                         FileComponent component,
                                         std::string &fileName)
{
  if (timestep > this->GetNumberOfTimeSteps())
    {
    return INV_TIMESTEP;
    }
  path base(this->GetDirectory());
  std::string c;
  switch (component)
    {
    case FileFlowX:
    base /= "vct_";
    c = "_0";
    break;
    case FileFlowY:
    base /= "vct_";
    c = "_1";
    break;
    case FileFlowZ:
    base /= "vct_";
    c = "_2";
    break;
    case FileMagnitude:
    base /= "mag_";
    c = "";
    break;
    }
  fileName = base.string();
  std::stringstream ts; ts << timestep << c << ".vtk";
  fileName += ts.str();
  if (exists(fileName))
    {
    return OK;
    }
  std::cerr << "File \"" << fileName << "\" does not exists\n";
  return PATH_NOEXISTS;
}

itk::PhaseContrast3DImage::Pointer Flow4DReader::LoadItkImage(std::string pathImage, bool invert)
{
  typedef itk::ImageFileReader<itk::PhaseContrast3DImage> ReaderType;
  typedef itk::ChangeInformationImageFilter<itk::PhaseContrast3DImage> ChangeInformationType;
  typedef itk::ShiftScaleImageFilter<itk::PhaseContrast3DImage,itk::PhaseContrast3DImage>
    InverterType;
  InverterType::Pointer inverter;
  
  
  ChangeInformationType::Pointer changeInformation;
  bool convertUnit = false;

  ReaderType::Pointer reader = ReaderType::New();
  reader->SetFileName(pathImage);
  reader->Update();

  if (0 && this->GetGeometryUnits() == "mm")
    {
    convertUnit = true;
    changeInformation = ChangeInformationType::New();
    changeInformation->SetInput(reader->GetOutput());
    ChangeInformationType::PointType origin;
    ChangeInformationType::SpacingType spacing;
    origin = reader->GetOutput()->GetOrigin();
    spacing = reader->GetOutput()->GetSpacing();
    for(size_t i = 0; i < ChangeInformationType::ImageDimension; i++)
      {
      origin[i] /= 10.0;
      spacing[i] /= 10.0;
      }
    changeInformation->SetOutputOrigin(origin);
    changeInformation->SetOutputSpacing(spacing);
    changeInformation->ChangeOriginOn();
    changeInformation->ChangeSpacingOn();
    }

  if (invert)
    {
    inverter = InverterType::New();
    inverter->SetShift(0.0);
    inverter->SetScale(-1.0);
    if (convertUnit)
      {
      inverter->SetInput(changeInformation->GetOutput());
      }
    else
      {
      inverter->SetInput(reader->GetOutput());
      }
    inverter->Update();
    return inverter->GetOutput();
    }
  else if (convertUnit)
    {
    changeInformation->Update();
    return changeInformation->GetOutput();
    }
  else
    {
    return reader->GetOutput();
    }
}

vtkImagePointer Flow4DReader::LoadVtkImage(std::string pathImage)
{
  
  itk::PhaseContrast3DImage::Pointer ptrImage = this->LoadItkImage(pathImage, false);
  return this->ConvertItkImageToVtkImage<itk::PhaseContrast3DImage>(ptrImage);
}

StatusType Flow4DReader::LoadPremaskImage()
{
  path base(this->GetDirectory());
  base /= "premask.vtk";
  if (!exists(base))
    {
    return PATH_NOEXISTS;
    }
  this->m_PremaskImage = this->LoadVtkImage(base.string());
  //std::cout << "m_PremaskImage =" << this->m_PremaskImage << "\n";
  //this->m_PremaskImage->Print(std::cout);

  return OK;
}

void Flow4DReader::BuildVectorImage(itk::PhaseContrast3DImage *flowX,
                                    itk::PhaseContrast3DImage *flowY,
                                    itk::PhaseContrast3DImage *flowZ,
                                    size_t timestep)
{
  // compose
  // convert to vtk
  // compute norm
  // store modulus
  // store vector
  /*
  [[imgNew GetPointData] GetScalars] SetName "Velocity"
  [imgNew GetPointData] SetActiveVectors "Velocity"
  imgNew Modified

  vtkImageMagnitude vecNorm
  vecNorm SetInputData imgNew
  vecNorm Update

  [imgNew GetPointData] AddArray [[[vecNorm GetOutput] GetPointData] GetScalars]
  [imgNew GetPointData] SetActiveScalars "Magnitude"
  puts [imgNew Print]
  */
  typedef itk::ComposeImageFilter<itk::PhaseContrast3DImage,itk::PhaseContrastVector3DImage> ComposeFilter;
  ComposeFilter::Pointer composer = ComposeFilter::New();
  composer->SetInput(0, flowX);
  composer->SetInput(1, flowY);
  composer->SetInput(2, flowZ);
  composer->Update();
  vtkImagePointer vectorImage = this->ConvertItkImageToVtkImage<itk::PhaseContrastVector3DImage>(composer->GetOutput());
  vectorImage->GetPointData()->GetScalars()->SetName("Velocity");
  vectorImage->GetPointData()->SetActiveVectors("Velocity");
  this->m_FlowVectorImage[timestep] = vectorImage;

#if !defined(__PMIFlowComputation_DLL__)
  vtkSmartPointer<vtkImageMagnitude> magnitudeFilter = vtkSmartPointer<vtkImageMagnitude>::New();

  //vectorImage->Print(std::cout);
  magnitudeFilter->SetInputData(vectorImage);
  magnitudeFilter->Update();
  vtkImageData *vectorNorm = magnitudeFilter->GetOutput();
  // WATCH: the vectorNorm scalar array is shared between the
  // FlowVectorImage and the FlowModulusImage
  vectorImage->GetPointData()->AddArray(vectorNorm->GetPointData()->GetScalars());
  vectorImage->GetPointData()->SetActiveScalars("Magnitude");
  vtkImagePointer modulusImage = vtkImagePointer::New();
  modulusImage->ShallowCopy(vectorNorm);
  this->TrackImageDataPointer(modulusImage.GetPointer());
  this->m_FlowModulusImage[timestep] = modulusImage;
  //vectorImage->Print(std::cout);
#endif
}

END_PCMR_DECLS
