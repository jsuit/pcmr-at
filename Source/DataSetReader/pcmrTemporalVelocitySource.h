#ifndef __pcmrTemporalVelocitySource_h
#define __pcmrTemporalVelocitySource_h

#include "pcmrTypes.h"
#include "pcmrDataSetReader_Export.h"
#include "vtkObjectFactory.h"
#include "vtkSmartPointer.h"
#include "vtkAlgorithm.h"
#include <vector>

class vtkImageData;

BEGIN_PCMR_DECLS

class Flow4DReader;

PCMRDATASETREADER_EXPORT 
int RescaleVelocity(vtkDataArray *vectorData, double factor);

class DataFlowAccessor : public vtkObject
{
 public:
  virtual size_t GetNumberOfTimeSteps() = 0;
  virtual double GetLengthOfTimeStep() = 0;
  virtual void GetTimeValues(std::vector<double>& timeValues) = 0;
  virtual vtkImageData *GetImage(size_t t) = 0;
};

class PCMRDATASETREADER_EXPORT Flow4DReaderAccessor : public DataFlowAccessor
{
 public:
  static Flow4DReaderAccessor *New();
  vtkTypeMacro(Flow4DReaderAccessor, DataFlowAccessor);

  Flow4DReaderAccessor();
  virtual ~Flow4DReaderAccessor();
  void SetFlow4DReader(Flow4DReader *reader);
  virtual size_t GetNumberOfTimeSteps();
  virtual double GetLengthOfTimeStep();
  virtual void GetTimeValues(std::vector<double>& timeValues);
  virtual vtkImageData *GetImage(size_t t);
 protected:
  Flow4DReaderAccessor(const Flow4DReaderAccessor&); // Not implemented.
  void operator=(const Flow4DReaderAccessor&);  // Not implemented.
  
 private:
  // should it be a ::Pointer?
  Flow4DReader *m_DataSetReader;
};

class PCMRDATASETREADER_EXPORT TemporalVelocitySource : public vtkAlgorithm
{
public:
  static TemporalVelocitySource *New();
  vtkTypeMacro(TemporalVelocitySource, vtkAlgorithm);

  void SetDataSetAccessor(DataFlowAccessor *accessor);

  void SetBaseTimeStep(int idx)
  {
    if (idx >= 0 && idx < this->GetNumberOfTimeSteps() && 
        idx != this->GetBaseTimeStep())
      {
        this->m_BaseTimeStep = idx;
        this->Modified();
      }
  }

  int GetBaseTimeStep() const
  {
    return this->m_BaseTimeStep;
  }

  void SetRescaleFactor(float factor)
  {
    this->m_RescaleFactor = factor;
  }

  float GetRescaleFactor() const
  {
    return this->m_RescaleFactor;
  }

  const std::vector<double>& GetTimeValues() const
  {
    return this->m_TimeValues;
  }

  int GetNumRequestData()
  {
    return this->m_NumRequestData;
  }

  int GetNumberOfTimeStepsInternal()
  {
    return static_cast<int>(this->GetTimeValues().size());
  }

  int GetNumberOfTimeSteps()
  {
    return this->GetNumberOfTimeStepsInternal() - this->GetBaseTimeStep();
  }

  float GetLengthOfTimeStep() const;

  void SetPointerTracker(void *tracker)
  {
    this->m_PointerTracker = tracker;
  }

protected:
  TemporalVelocitySource()
  {
    this->m_NumRequestData = 0;
    this->m_RescaleFactor = 1.0;
    this->SetNumberOfInputPorts(0);
    this->SetNumberOfOutputPorts(1);
    this->m_BaseTimeStep = 0;
    this->m_PointerTracker = NULL;
  }

  ~TemporalVelocitySource()
  {
  }

  int FindIndexForTime(double t);

  int ProcessRequest(vtkInformation* request,
                     vtkInformationVector** inputVector,
                     vtkInformationVector* outputVector);

  int FillOutputPortInformation(int, vtkInformation *info);

  virtual int RequestInformation(vtkInformation *,
                                 vtkInformationVector **,
                                 vtkInformationVector *outputInfoVector);

  int RequestData(vtkInformation*,
                  vtkInformationVector** vtkNotUsed(inputVector),
                  vtkInformationVector* outputVector);

private:
  TemporalVelocitySource(const TemporalVelocitySource&); // Not implemented.
  void operator=(const TemporalVelocitySource&);  // Not implemented.

  vtkSmartPointer<DataFlowAccessor> m_DataFlowAccessor;
  std::vector<double> m_TimeValues;
  float m_TimeStepLength;
  int m_NumRequestData;
  float m_RescaleFactor;
  int m_BaseTimeStep;

  void *m_PointerTracker;
};

END_PCMR_DECLS

#endif

