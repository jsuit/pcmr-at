#include <boost/foreach.hpp>
#include "pcmrFlow4DReader.h"
#include <iostream>
#include "gtest/gtest.h"

static int _ARGC = 0;
static char **_ARGV = NULL;

TEST(TestFlow4DReader, TestFlow4DReader)
{
  ASSERT_TRUE(_ARGV!=NULL);
  ASSERT_EQ(2, _ARGC) << "usage: '" << _ARGV[0] << "' study\n";

  pcmr::Flow4DReader::Pointer reader = pcmr::Flow4DReader::New();

  pcmr::StatusType status = reader->SetDirectory(_ARGV[1]);
  ASSERT_EQ(pcmr::OK, status) << "SetDirectory: " << pcmr::GetStatusDescription(status) << "\n";

  std::cout << "Velocity Encoding = " << reader->GetVelocityEncoding() << "\n";
  std::cout << "Number of Time Steps = " << reader->GetNumberOfTimeSteps() << "\n";
  std::cout << "Length of Time Step = " << reader->GetLengthOfTimeStep() << "\n";
  std::cout << "Image Orientation = " << reader->GetImageOrientation() << "\n";
  std::vector<double> timeValues;
  if (reader->GetTimeValues(timeValues) == 0)
    {
    double t0 = 0.0;

    for(size_t i = 0; i < timeValues.size(); i++)
      {
      std::cout << "Trigger(" << i << ") = " << timeValues[i];
      if (i==0)
        {
        std::cout << std::endl;
        }
      else
        {
        double t1 = timeValues[i];
        double delta = t1 - t0;
        std::cout << ", delta = " << delta << std::endl;
        }
        t0 = timeValues[i];
      }
    }
  std::vector<std::string> properties;
  reader->GetExtendedProperties(properties);
  pcmr::HeaderReader::Property prop;
  BOOST_FOREACH(std::string &v, properties)
    {
    reader->GetExtendedProperty(v, prop);
    std::cout << prop.description << " = " << prop.value << "\n";
    }

  std::cout << "Read again ...\n";
  reader->SetDirectory(_ARGV[1]);
  pcmr::vtkImagePointer mag_3 = reader->GetMagnitudeImage(3);
  ASSERT_TRUE(mag_3!=NULL) << "Could not load mag_3\n";
  pcmr::vtkImagePointer vect_3 = reader->GetFlowImage(pcmr::Flow4DReader::FlowVector,3);
  ASSERT_TRUE(mag_3!=NULL) << "Could not load vect_3\n";
  for(size_t i = 0; i < reader->GetNumberOfTimeSteps(); i++)
    {
    std::cout << "accessing timestep "<< i << " ...\n";
    reader->GetMagnitudeImage(i);
    }
}

int main(int argc, char **argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  _ARGC = argc;
  _ARGV = argv;
  return RUN_ALL_TESTS();
}
