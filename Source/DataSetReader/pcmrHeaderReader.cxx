#include "pcmrHeaderReader.h"
#include <boost/property_tree/xml_parser.hpp>
#include <iostream>
#include <boost/foreach.hpp>
#include <ctype.h>

BEGIN_PCMR_DECLS

HeaderReader::~HeaderReader()
{
}

StatusType HeaderReader::SetFileName(const char* path)
{
  this->m_FileName = path;
  // REVIEW: check error here
  read_xml(m_FileName, this->m_PropertyTree);
  return OK;
}

std::string HeaderReader::GetFormatVersion() const
{
  return this->m_PropertyTree.get<std::string>("Study.FormatVersion", "0.0");
}

std::string HeaderReader::GetSourceSequence() const
{
  return this->m_PropertyTree.get<std::string>("Study.SourceSequence", "");
}

size_t HeaderReader::GetNumberOfTimeSteps() const
{
  return this->m_PropertyTree.get<size_t>("Study.NumberOfTimeSteps");
}

double HeaderReader::GetLengthOfTimeStep() const
{
  return this->m_PropertyTree.get<double>("Study.LengthOfTimeStep", 0);
}

double HeaderReader::GetBloodViscocity(double defaultValue) const
{
  return this->m_PropertyTree.get<double>("Study.BloodViscocity", defaultValue);
}

double HeaderReader::GetVelocityEncoding() const
{
  return this->m_PropertyTree.get<double>("Study.VelocityEncoding");
}

double HeaderReader::GetMinimumVelocity() const
{
  return this->m_PropertyTree.get<double>("Study.MinimumVelocity");
}

double HeaderReader::GetMaximumVelocity() const
{
  return this->m_PropertyTree.get<double>("Study.MaximumVelocity");
}

std::string HeaderReader::GetImageOrientation() const
{
  return this->m_PropertyTree.get<std::string>("Study.ImageOrientation");
}

std::string HeaderReader::GetGeometryUnits() const
{
  return this->m_PropertyTree.get<std::string>("Study.Units.Geometry", "mm");
}

std::string HeaderReader::GetVelocityUnits() const
{
  return this->m_PropertyTree.get<std::string>("Study.Units.Velocity", "cm/s");
}

bool HeaderReader::GetInvertComponentX() const
{
  return this->m_PropertyTree.get<bool>("Study.InvertComponentX", false);
}

bool HeaderReader::GetInvertComponentY() const
{
  return this->m_PropertyTree.get<bool>("Study.InvertComponentY", true);
}

bool HeaderReader::GetInvertComponentZ() const
{
  return this->m_PropertyTree.get<bool>("Study.InvertComponentZ", false);
}

StatusType HeaderReader::GetTimeValues(std::vector<double> &timeValues) const
{
  timeValues.clear();
  boost::optional<const boost::property_tree::ptree&> nodeTimes = this->m_PropertyTree.get_child_optional("Study.TimeValues");
  if (!nodeTimes)
    {
    return TAG_NOTFOUND;
    }
  char *endptr;
  double previous = 0.0;
  bool first = 1;
  BOOST_FOREACH(const boost::property_tree::ptree::value_type &v, 
                this->m_PropertyTree.get_child("Study.TimeValues"))
    {
    double t = strtod(v.second.data().c_str(), &endptr);
    if (*endptr)
      {
      std::cerr << "GetTimeValues: failed converting " << v.second.data().c_str() << " to double\n";
      timeValues.clear();
      return INV_TIMESTEP;
      }
    if (first)
      {
      if (t < 0.0)
        {
        std::cerr << "GetTimeValues: time-value " << v.second.data().c_str() << " must not be negative\n";
        timeValues.clear();
        return INV_TIMESTEP;
        }
      first = false;
      }
    else if (t <= previous)
      {
      timeValues.clear();
      return SEQ_NOT_INCR;
      }
    previous = t;
    timeValues.push_back(t);
    }
  return OK;
}

StatusType HeaderReader::GetExtendedProperties(std::vector<std::string> &properties) const
{
  properties.clear();
  boost::optional<const boost::property_tree::ptree&> nodeProperties = this->m_PropertyTree.get_child_optional("Study.ExtendedFields");
  if (!nodeProperties)
    {
    return TAG_NOTFOUND;
    }
  BOOST_FOREACH(const boost::property_tree::ptree::value_type &v, 
                this->m_PropertyTree.get_child("Study.ExtendedFields"))
    {
    properties.push_back(v.first);
    }
  return OK;
}

StatusType HeaderReader::GetExtendedProperty(const std::string& key, Property &property) const
{
  // REVIEW: check if the key exists
  std::string key_full("Study.ExtendedFields.");
  key_full += key;
  property.description = this->m_PropertyTree.get<std::string>(key_full + ".Description");
  property.value = this->m_PropertyTree.get<std::string>(key_full + ".Value");
  return OK;
}

END_PCMR_DECLS
