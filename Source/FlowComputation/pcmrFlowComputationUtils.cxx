#include "pcmrFlowComputationUtils.h"
#include "vtkPolyData.h"

BEGIN_PCMR_DECLS

vtkSmartPointer<vtkPolyData> CreatePointSetFromCollection(const SeedCollection& seeds)
{
  vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
  vtkSmartPointer<vtkPolyData> pointSet = vtkSmartPointer<vtkPolyData>::New();

  const size_t n = seeds.size();

  points->SetNumberOfPoints(n);
  for(size_t i = 0; i < n; i++)
    {
    points->SetPoint(i, seeds[i].x, seeds[i].y, seeds[i].z);
    }
  pointSet->SetPoints(points);
  
  return pointSet;
}

END_PCMR_DECLS
