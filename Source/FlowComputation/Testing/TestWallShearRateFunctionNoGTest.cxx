#include <iostream>
#include "pcmrFlow4DReader.h"
#include "pcmrWallShearRateFunction.h"
#include "itkVTKImageToImageFilter.h"
#include "itkTimeProbe.h"

static int _ARGC = 0;
static char **_ARGV = NULL;

static const char* StudyDir = NULL;
static pcmr::Flow4DReader::Pointer dataSet = NULL;

int main(int argc, char **argv)
{
  std::cout << typeid(int).name() << std::endl;

  _ARGC = argc;
  _ARGV = argv;
  assert(_ARGV!=NULL);

  StudyDir = _ARGV[1];
  assert(StudyDir!=NULL);

  dataSet = pcmr::Flow4DReader::New();

  pcmr::StatusType status = dataSet->SetDirectory(StudyDir);
  if (status != pcmr::OK)
    {
    dataSet = NULL;
    }
  assert(pcmr::OK==status);
  
  typedef itk::VTKImageToImageFilter<itk::PhaseContrast3DImage> ImporterType;
  typedef itk::VTKImageToImageFilter<itk::MaskImage> MaskImporterType;
  ImporterType::Pointer importerX = ImporterType::New();
  ImporterType::Pointer importerY = ImporterType::New();
  ImporterType::Pointer importerZ = ImporterType::New();
  MaskImporterType::Pointer importerMask = MaskImporterType::New();
  vtkImageData *vtkImgX = dataSet->GetFlowImage(pcmr::Flow4DReader::FlowX,
                                                3);
  assert(vtkImgX!=NULL);
  vtkImageData *vtkImgY = dataSet->GetFlowImage(pcmr::Flow4DReader::FlowY,
                                                3);
  assert(vtkImgY!=NULL);
  vtkImageData *vtkImgZ = dataSet->GetFlowImage(pcmr::Flow4DReader::FlowZ,
                                                3);
  assert(vtkImgZ!=NULL);
  vtkImageData *vtkMask = dataSet->GetMaskImage(3);
  assert(vtkMask!=NULL);

  importerX->SetInput(vtkImgX);
  importerY->SetInput(vtkImgY);
  importerZ->SetInput(vtkImgZ);
  importerMask->SetInput(vtkMask);
  importerX->Update();
  importerY->Update();
  importerZ->Update();
  importerMask->Update();

  pcmr::WallShearRateFunction::Pointer wsrFunction = pcmr::WallShearRateFunction::New();

  wsrFunction->SetComponentXImage(importerX->GetOutput());
  wsrFunction->SetComponentYImage(importerY->GetOutput());
  wsrFunction->SetComponentZImage(importerZ->GetOutput());
  wsrFunction->SetMaskImage(importerMask->GetOutput());
  double pts[][3] = 
    {
      {249.840251806795, -61.1493222228418, 206.076549673628},
      {177.110735716123, 74.1027336556252,  203.86202071702}
    };

  itk::TimeProbe clock;
 
  clock.Start();
  for (int i = 0; i < 10; i++)
    {
    double wss = wsrFunction->EvaluateAtPhysicalPoint(pts[1]);
    //std::cout << "wss = " << wss << std::endl;
    }
  clock.Stop();
  std::cout << "Mean: " << clock.GetMean() << std::endl;
  std::cout << "Total: " << clock.GetTotal() << std::endl;
  return 0;
}
