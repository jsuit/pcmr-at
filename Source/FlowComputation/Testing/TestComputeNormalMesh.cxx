#include <iostream>
#include "gtest/gtest.h"
#include "itkMeshFileReader.h"
// to use pcmr::TriangleMeshType
#include "pcmrWallShearRateFunction2.h"

#include "itkQuadEdgeMeshExtendedTraits.h"
#include "itkNormalQuadEdgeMeshFilter.h"
#include "itkMeshFileWriter.h"

typedef itk::MeshFileReader<pcmr::TriangleMeshType> MeshReaderType;

static int _ARGC = 0;
static char **_ARGV = NULL;
static const char* MeshFileInput = NULL;
static const char* MeshFileOutput = NULL;
static MeshReaderType::Pointer  polyDataReader = NULL;

typedef itk::Vector<pcmr::TriangleMeshType::CoordRepType, 3> VectorType;
typedef itk::QuadEdgeMeshExtendedTraits <
    VectorType,
    3,
    2,
    pcmr::TriangleMeshType::CoordRepType,
    pcmr::TriangleMeshType::CoordRepType,
    VectorType,
    bool,
    bool > QEMTraits;

typedef itk::QuadEdgeMesh <VectorType, 3, QEMTraits > QEMeshType;
typedef itk::NormalQuadEdgeMeshFilter<pcmr::TriangleMeshType, QEMeshType> NormalFilterType;
NormalFilterType::WeightType WeightType = 
  //NormalFilterType::GOURAUD;
  //NormalFilterType::THURMER;
  NormalFilterType::AREA;
typedef itk::MeshFileWriter<QEMeshType> MeshWriterType;
static MeshWriterType::Pointer  polyDataWriter = NULL;

TEST(TestComputeNormalMesh, TestCommandLineArgs)
{
  ASSERT_TRUE(_ARGV!=NULL);
  ASSERT_EQ(_ARGC,3) << "usage: '" << _ARGV[0] << "' input_mesh output_mesh\n";
  MeshFileInput = _ARGV[1];
  MeshFileOutput = _ARGV[2];
}

TEST(TestComputeNormalMesh, TestLoadMesh)
{
  ASSERT_TRUE(MeshFileInput!=NULL);
  polyDataReader = MeshReaderType::New();
  polyDataReader->SetFileName(MeshFileInput);
  try
    {
    polyDataReader->Update();
    }
  catch(...)
    {
    std::cout << "Unable to read " << MeshFileInput << std::endl;
    polyDataReader = NULL;
    }
}

TEST(TestComputeNormalMesh, TestCompute)
{
  ASSERT_TRUE(MeshFileOutput!=NULL);
  ASSERT_TRUE(polyDataReader.GetPointer()!=NULL);
  NormalFilterType::Pointer normals = NormalFilterType::New();
  normals->SetInput(polyDataReader->GetOutput());
  normals->SetWeight(WeightType);
  normals->Update();
  polyDataWriter = MeshWriterType::New();
  polyDataWriter->SetInput(normals->GetOutput());
  polyDataWriter->SetFileName(MeshFileOutput);
  try
    {
    polyDataWriter->Update();
    }
  catch(...)
    {
    std::cout << "Unable to write " << MeshFileOutput << std::endl;
    }
}

int main(int argc, char **argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  _ARGC = argc;
  _ARGV = argv;
  return RUN_ALL_TESTS();
}
