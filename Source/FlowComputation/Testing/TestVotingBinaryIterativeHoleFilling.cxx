  
#include "itkPhaseContrastImage.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itkSimpleFilterWatcher.h"
#include "itkVotingBinaryIterativeHoleFillingImageFilter.h"

typedef itk::ImageFileReader<itk::MaskImage> MaskReaderType;
typedef itk::ImageFileWriter<itk::MaskImage> MaskWriterType;
typedef itk::VotingBinaryIterativeHoleFillingImageFilter<itk::MaskImage> VotingBinaryIterativeHoleFillingType;

int main(int argc, char* argv[])
{
  if (argc != 3)
    {
    std::cerr << "Usage:\n";
    std::cerr << argv[0] << " input_mask output_mask\n";
    return -1;
    }
  
  MaskReaderType::Pointer reader = MaskReaderType::New();
  
  reader->SetFileName(argv[1]);
  
  VotingBinaryIterativeHoleFillingType::Pointer fillHoles = VotingBinaryIterativeHoleFillingType::New();

  itk::SimpleFilterWatcher watcherFill(fillHoles, "FillHoles");
  
  itk::MaskImage::SizeType radius = {2,2,2};

  fillHoles->SetRadius( radius );
  fillHoles->SetBackgroundValue( 0 );
  fillHoles->SetForegroundValue( 1 );
  fillHoles->SetMajorityThreshold( 1 );
  fillHoles->SetMaximumNumberOfIterations( 15 );

  fillHoles->SetInput(reader->GetOutput());

  MaskWriterType::Pointer writer = MaskWriterType::New();
  writer->SetInput(fillHoles->GetOutput());
  writer->SetFileName(argv[2]);
  writer->Update();
  
  return 0;
}
