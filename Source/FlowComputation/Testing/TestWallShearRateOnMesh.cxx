#include <iostream>
#include "pcmrFlow4DReader.h"
#include "gtest/gtest.h"
#include "pcmrWallShearRateFunction2.h"
#include "itkMeshFileReader.h"
#include "itkTimeProbe.h"

#include "itkQuadEdgeMeshExtendedTraits.h"
#include "itkNormalQuadEdgeMeshFilter.h"
#include "itkMeshFileWriter.h"

typedef itk::MeshFileReader<pcmr::TriangleMeshType> MeshReaderType;
typedef itk::MeshFileWriter<pcmr::TriangleMeshType> MeshWriterType;

typedef itk::Vector<pcmr::TriangleMeshType::CoordRepType, 3> VectorType;
typedef itk::QuadEdgeMeshExtendedTraits <
    VectorType,
    3,
    2,
    pcmr::TriangleMeshType::CoordRepType,
    pcmr::TriangleMeshType::CoordRepType,
    VectorType,
    bool,
    bool > QEMTraits;

typedef itk::QuadEdgeMesh <VectorType, 3, QEMTraits > QEMeshType;
typedef itk::NormalQuadEdgeMeshFilter<pcmr::TriangleMeshType, QEMeshType> NormalFilterType;
NormalFilterType::WeightType WeightType = 
  //NormalFilterType::GOURAUD;
  //NormalFilterType::THURMER;
  NormalFilterType::AREA;

static int _ARGC = 0;
static char **_ARGV = NULL;

static const char* StudyDir = NULL;
static const char* MeshFileInput = NULL;
static const char* MeshFileOutput = NULL;

static pcmr::Flow4DReader::Pointer dataSet = NULL;
static int TimeStep = -1;

static MeshReaderType::Pointer  polyDataReader = NULL;
static MeshWriterType::Pointer  polyDataWriter = NULL;

template <class V>
std::ostream & DumpVector(std::ostream & out, V &x, size_t size)
{
  out << "[";
  for(int i = 0; i < size; i++)
    {
    out << (i?", ":"") << x[i];
    }
  out << "]";
  return out;
}

int ParsePositive(const char* name, const char* value, double &v)
{
  v = atof(value);
  if (v <= 0)
    {
    std::cerr << "Invalid " << name << " argument: " << value << std::endl;
    return -1;
    }
  return 0;
}

int ParsePositive(const char* name, const char* value, int &v)
{
  v = atoi(value);
  if (v <= 0)
    {
    std::cerr << "Invalid " << name << " argument: " << value << std::endl;
    return -1;
    }
  return 0;
}

TEST(TestWallShearRateOnMesh, CommandLineArgs)
{
  ASSERT_TRUE(_ARGV!=NULL);
  ASSERT_EQ(_ARGC,5) << "usage: '" << _ARGV[0] << "' study time-step meshInput meshOutput\n";
  StudyDir = _ARGV[1];
  TimeStep = atoi(_ARGV[2]);
  ASSERT_GE(TimeStep,0) << " time-step must be positive";
  MeshFileInput = _ARGV[3];
  MeshFileOutput = _ARGV[4];
}

TEST(TestWallShearRateOnMesh, LoadDataSet)
{
  ASSERT_TRUE(StudyDir!=NULL);
  dataSet = pcmr::Flow4DReader::New();

  pcmr::StatusType status = dataSet->SetDirectory(StudyDir);
  if (status != pcmr::OK)
    {
    dataSet = NULL;
    }
  ASSERT_EQ(pcmr::OK, status) << "SetDirectory: " << pcmr::GetStatusDescription(status) << "\n";
  if (TimeStep < 0 || TimeStep >= dataSet->GetNumberOfTimeSteps())
    {
    int ts = dataSet->GetNumberOfTimeSteps() / 5;
    std::cout << "Invalid time-step '" << TimeStep << "' using default " << ts << std::endl;
    TimeStep = ts;
    }
  polyDataReader = MeshReaderType::New();
  polyDataReader->SetFileName(MeshFileInput);
  polyDataReader->Update();
}

inline void UpdateStatValue(double x, 
                            double &min_x, double &max_x, double& sum_x)
{
  if (x < min_x)
    {
    min_x = x;
    }
  else if (x > max_x)
    {
    max_x = x;
    }
  sum_x += x;
}

struct TestStatistic
{
  size_t count;
  double min_v, max_v, sum_v;
  double min_d, max_d, sum_d;
  double min_g, max_g, sum_g;
  double min_wsr0, max_wsr0, sum_wsr0;
  double min_wsr1, max_wsr1, sum_wsr1;

  TestStatistic()
    : count(0),
      min_v(1.0e6), max_v(-1.0e6), sum_v(0),
      min_d(1.0e6), max_d(-1.0e6), sum_d(0),
      min_g(1.0e6), max_g(-1.0e6), sum_g(0),
      min_wsr0(1.0e10), max_wsr0(-1.0e10), sum_wsr0(0),
      min_wsr1(1.0e10), max_wsr1(-1.0e10), sum_wsr1(0)
  {
  }
  
  static 
  void PrintNumber(std::ostream &o, double x)
  {
    o << std::setw(10) << std::setiosflags(ios::fixed) << std::setprecision(3) << x;
  }

  void Print(std::ostream &o)
  {
    o << std::setw(10) << "Variable" << std::setw(10) <<  "Minimum"
      << std::setw(10) << "Maximum" << std::setw(10) << "Average\n";
    o << std::setw(10) << "Velocity";
    PrintNumber(o, this->min_v);
    PrintNumber(o, this->max_v);
    PrintNumber(o, this->sum_v/this->count);
    o << std::endl;
    o << std::setw(10) << "Distance";
    PrintNumber(o, this->min_d);
    PrintNumber(o, this->max_d);
    PrintNumber(o, this->sum_d/this->count);
    o << std::endl;
    o << std::setw(10) << "Gradient";
    PrintNumber(o, this->min_g);
    PrintNumber(o, this->max_g);
    PrintNumber(o, this->sum_g/this->count);
    o << std::endl;
    o << std::setw(10) << "WSR0";
    PrintNumber(o, this->min_wsr0);
    PrintNumber(o, this->max_wsr0);
    PrintNumber(o, this->sum_wsr0/this->count);
    o << std::endl;
    o << std::setw(10) << "WSR1";
    PrintNumber(o, this->min_wsr1);
    PrintNumber(o, this->max_wsr1);
    PrintNumber(o, this->sum_wsr1/this->count);
    o << std::endl;
  }
};

void UpdateStatFunction(pcmr::WallShearRateFunction2::Pointer wsrFunction,
                        const pcmr::WallShearRateFunction2::PointType &p,
                        TestStatistic & stats)
{
  pcmr::WallShearRateFunction2::GradientType gradient[4];
  pcmr::WallShearRateFunction2::OutputType value[4];
  wsrFunction->EvaluateGradientAtPhysicalPoint(p, value, gradient);
  //std::cout << "velocidad = " << value[1] << std::endl;
  UpdateStatValue(value[1], stats.min_v, stats.max_v, stats.sum_v);
  //std::cout << "distance = " << value[3] << std::endl;
  UpdateStatValue(fabs(value[3]), stats.min_d, stats.max_d, stats.sum_d);
  typedef vnl_vector_ref<pcmr::WallShearRateFunction2::GradientType::ValueType> VectorRefType;
  VectorRefType g = gradient[1].GetVnlVector();
  //std::cout << "gradient = " << g.two_norm() << std::endl;
  UpdateStatValue(g.two_norm(), stats.min_g, stats.max_g, stats.sum_g);
  double wsr0 = wsrFunction->EvaluateAtPhysicalPoint(p);
  UpdateStatValue(wsr0, stats.min_wsr0, stats.max_wsr0, stats.sum_wsr0);
  double normal[3];
  normal[0] = p[0];
  normal[1] = p[1];
  normal[2] = p[2];
  double wsr1 = wsrFunction->EvaluateAtPhysicalPointWithNormal(p, normal);
  UpdateStatValue(wsr1, stats.min_wsr1, stats.max_wsr1, stats.sum_wsr1);
  ++stats.count;
  //std::cout << "stats.sum_g = " << stats.sum_g << " stats.count = " << stats.count << std::endl;
}

TEST(TestWallShearRateOnMesh, ComputeWSR)
{
  pcmr::WallShearRateFunction2::Pointer wsrFunction = pcmr::WallShearRateFunction2::New();

  wsrFunction->SetVelocityUnitFactor(10);
  wsrFunction->SetComputeWallCondition(true);
  wsrFunction->SetSplineOrder(3);
  ASSERT_GE(TimeStep,0) << " time-step must be positive";
  pcmr::StatusType status = wsrFunction->SetImageDataFromStudy(dataSet, TimeStep);
  ASSERT_TRUE(status==pcmr::NO_MASK || status == pcmr::OK);
  status = wsrFunction->SetBoundaryMesh(polyDataReader->GetOutput());
  ASSERT_EQ(pcmr::OK, status);
  wsrFunction->SetCorrectedFlowFileName("WSR2_CorrectedFlow_Function.vtk");
  wsrFunction->SetLevelImageFileName("WSR2_LevelImage_Function.vtk");

  NormalFilterType::Pointer normals = NormalFilterType::New();
  normals->SetInput(polyDataReader->GetOutput());
  normals->SetWeight(WeightType);
  normals->Update();

  typedef pcmr::TriangleMeshType::PointType               PointType;
  typedef pcmr::TriangleMeshType::PointIdentifier         PointIdentifier;

  pcmr::TriangleMeshType::Pointer meshInput = polyDataReader->GetOutput();
  QEMeshType::Pointer meshNormal = normals->GetOutput();

  ASSERT_EQ(meshInput->GetNumberOfPoints(),meshNormal->GetNumberOfPoints());

  for(PointIdentifier i = 0; i < meshInput->GetNumberOfPoints(); i++)
    {
    PointType pt;
    meshInput->GetPoint(i, &pt);
    double p[3];
    p[0] = pt[0];
    p[1] = pt[1];
    p[2] = pt[2];
    VectorType normalComputed;
    meshNormal->GetPointData(i, &normalComputed);
    double normal[3];
    normal[0] = normalComputed[0];
    normal[1] = normalComputed[1];
    normal[2] = normalComputed[2];
    double wsr = wsrFunction->EvaluateAtPhysicalPointWithNormal(p, normal);
    if (wsr < 50 && fabs(p[1]) < 10)
      {
      std::cout << "Low WSR = " << wsr << "\n" << 
        "  P = (" << p[0] <<", " << p[1] <<", " << p[2] << ")\n"
        "  N = (" << normal[0] <<", " << normal[1] <<", " << normal[2] << ")\n";
      }
    meshInput->SetPointData(i, wsr);
    }
  polyDataWriter = MeshWriterType::New();
  polyDataWriter->SetInput(meshInput);
  polyDataWriter->SetFileName(MeshFileOutput);
  try
    {
    polyDataWriter->Update();
    }
  catch(...)
    {
    std::cout << "Unable to write " << MeshFileOutput << std::endl;
    }
}

int main(int argc, char **argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  _ARGC = argc;
  _ARGV = argv;
  return RUN_ALL_TESTS();
}

/*
./TestWallShearRate2Parabolic ../../../../../../biomedical/data/Studies/Parabolic_UPC_LABSON_0.5 0 4 ../../../../../../biomedical/data/Studies/Parabolic_UPC_LABSON_0.5/WSR/sim_mesh.vtk 
*/
