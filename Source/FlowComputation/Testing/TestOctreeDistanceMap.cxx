#include <iostream>
#include "pcmrTypes.h"
#include "pcmrBSplineDistanceImplicitFunction.h"
#include "pcmrFlow4DReader.h"
#include "gtest/gtest.h"
#include "itkSignedMaurerDistanceMapImageFilter.h"
#include "itkVTKImageToImageFilter.h"
#include "TestHelper.h"
#include "vtkNew.h"
#include "vtkHyperOctreeSampleFunction.h"
#include "vtkXMLHyperOctreeWriter.h"
#include "vtkThreshold.h"
#include "vtkUnstructuredGridWriter.h"

static int _ARGC = 0;
static char **_ARGV = NULL;

static const char* StudyDir = NULL;
static pcmr::Flow4DReader::Pointer dataSet = NULL;

TEST(TestDistanceMap, TestCommandLineArgs)
{
  ASSERT_TRUE(_ARGV!=NULL);
  ASSERT_GE(_ARGC,1) << "usage: '" << _ARGV[0] << "' study\n";
  StudyDir = _ARGV[1];
}

TEST(TestDistanceMap, TestLoadDataSet)
{
  ASSERT_TRUE(StudyDir!=NULL);
  dataSet = pcmr::Flow4DReader::New();

  pcmr::StatusType status = dataSet->SetDirectory(StudyDir);
  if (status != pcmr::OK)
    {
    dataSet = NULL;
    }
  ASSERT_EQ(pcmr::OK, status) << "SetDirectory: " << pcmr::GetStatusDescription(status) << "\n";
}

TEST(TestDistanceMap, TestComputeDistance)
{
  typedef itk::VTKImageToImageFilter<itk::MaskImage> MaskImporterType;
  MaskImporterType::Pointer importerMask = MaskImporterType::New();
  vtkImageData *vtkMask = dataSet->GetMaskImage(3);
  ASSERT_TRUE(vtkMask!=NULL);

  importerMask->SetInput(vtkMask);

  typedef itk::SignedMaurerDistanceMapImageFilter<itk::MaskImage,itk::PhaseContrast3DImage> DistanceFilterType;
  DistanceFilterType::Pointer distanceFilter = DistanceFilterType::New();
  distanceFilter->SetInput(importerMask->GetOutput());
  distanceFilter->SquaredDistanceOff();
  distanceFilter->UseImageSpacingOn();
 
  WriteImage(distanceFilter->GetOutput(), "DistanceMapOriginal.vtk");

  vtkNew<BSplineDistanceImplicitFunction> implicit;
  implicit->SetSplineOrder(1);
  implicit->SetDistanceFieldGrid(distanceFilter->GetOutput());

  double pts[][3] = 
    {
      {249.840251806795, -61.1493222228418, 206.076549673628},
      {177.110735716123, 74.1027336556252,  203.86202071702}
    };
  for (int i = 0; i < sizeof(pts)/sizeof(pts[0]); i++)
    {
    double *p = pts[i];
    double v = implicit->EvaluateFunction(p);
    std::cout << "v = " << v << std::endl;
    }
  implicit->Print(std::cout);
  vtkNew<vtkHyperOctreeSampleFunction> sampleOctreeDistance;
  sampleOctreeDistance->SetImplicitFunction(implicit.GetPointer());
  sampleOctreeDistance->SetDimension(3);
  sampleOctreeDistance->SetLevels(10);
  const itk::MaskImage::SpacingType &spacing = importerMask->GetOutput()->GetSpacing();
  const itk::MaskImage::PointType &origin = importerMask->GetOutput()->GetOrigin();
  const itk::MaskImage::RegionType &region = importerMask->GetOutput()->GetLargestPossibleRegion();
  double octreeSize[3];
  double octreeOrigin[3];
  for (int i = 0; i < 3; i++)
    {
    octreeOrigin[i] = origin[i] + spacing[i];
    octreeSize[i] = (region.GetSize(i)-2)*spacing[i];
    }
  sampleOctreeDistance->SetOrigin(octreeOrigin);
  sampleOctreeDistance->SetSize(octreeSize);
  sampleOctreeDistance->SetOutputScalarTypeToFloat();
  sampleOctreeDistance->SetThreshold(0.05);
  sampleOctreeDistance->Print(std::cout);
  sampleOctreeDistance->Update();
  std::cout << "NumberOfFunctionEvaluation = "
            << implicit->GetNumberOfFunctionEvaluation()
            << std::endl;
  /*
  vtkNew<vtkXMLHyperOctreeWriter> writerX;
  writerX->SetInputConnection(sampleOctreeDistance->GetOutputPort());
  writerX->SetFileName("HyperOctreeSample.vto");
  writerX->Update();
  */
  std::cout << "About to extract cells on boundary\n";
  vtkNew<vtkThreshold> extract;
  extract->SetInputConnection(sampleOctreeDistance->GetOutputPort());
  extract->ThresholdBetween(-0.5, 0.5);
  extract->Update();
  std::cout << "Cell on boundary extracted\n";
  vtkNew<vtkUnstructuredGridWriter> gridWriter;
  gridWriter->SetInputConnection(extract->GetOutputPort());
  gridWriter->SetFileName("OctreeCells.vtk");
  gridWriter->Update();
}

int main(int argc, char **argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  _ARGC = argc;
  _ARGV = argv;
  return RUN_ALL_TESTS();
}
