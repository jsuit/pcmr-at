#include <iostream>
#include "pcmrFlow4DReader.h"
#include "gtest/gtest.h"
#include "vtkPolyData.h"
#include "vtkNew.h"
#include "itkMeshFileReader.h"
#include "itkAntiAliasBinaryImageFilter.h"
#include "itkTriangleMeshToBinaryImageFilter.h"
#include "itkVTKImageToImageFilter.h"
// to use pcmr::TriangleMeshType
#include "pcmrWallShearRateFunction2.h"
#include "itkMath.h"
#include "TestHelper.h"

typedef itk::Mesh<float, 3>           TriangleMeshType;
typedef itk::MeshFileReader<TriangleMeshType> MeshReaderType;
typedef itk::TriangleMeshToBinaryImageFilter<TriangleMeshType,itk::MaskImage> TriangleMeshToBinaryImageFilterType;

//#define DONT_GENERATE_INTERPOLATED_IMAGE

static int _ARGC = 0;
static char **_ARGV = NULL;

static const char* StudyDir = NULL;
static const char* MeshFile = NULL;
static pcmr::Flow4DReader::Pointer dataSet = NULL;
static MeshReaderType::Pointer  polyDataReader = NULL;

TEST(TestAntiAliasBinaryImageFilter, TestCommandLineArgs)
{
  ASSERT_TRUE(_ARGV!=NULL);
  ASSERT_TRUE(_ARGC==2 || _ARGC==3) << "usage: '" << _ARGV[0] << "' study ?mesh?\n";
  StudyDir = _ARGV[1];
  if (_ARGC == 3)
    {
    MeshFile = _ARGV[2];
    }
}

TEST(TestAntiAliasBinaryImageFilter, TestLoadDataSet)
{
  ASSERT_TRUE(StudyDir!=NULL);
  dataSet = pcmr::Flow4DReader::New();

  pcmr::StatusType status = dataSet->SetDirectory(StudyDir);
  if (status != pcmr::OK)
    {
    dataSet = NULL;
    }
  ASSERT_EQ(pcmr::OK, status) << "SetDirectory: " << pcmr::GetStatusDescription(status) << "\n";
  if (MeshFile)
    {
    polyDataReader = MeshReaderType::New();
    polyDataReader->SetFileName(MeshFile);
    polyDataReader->Update();
    }
}

TEST(TestAntiAliasBinaryImageFilter, TestComputeDistance)
{
  ASSERT_TRUE(dataSet);

  typedef itk::VTKImageToImageFilter<itk::MaskImage> MaskImporterType;
  MaskImporterType::Pointer importerMask = NULL;
  TriangleMeshToBinaryImageFilterType::Pointer rasterFilter = NULL;
  if (MeshFile)
    {
    typedef itk::VTKImageToImageFilter<itk::PhaseContrast3DImage> ImporterType;
    ImporterType::Pointer importerX = ImporterType::New();
    vtkImageData *vtkImgX = dataSet->GetFlowImage(pcmr::Flow4DReader::FlowX, 0);
    importerX->SetInput(vtkImgX);
    importerX->Update();

    rasterFilter = TriangleMeshToBinaryImageFilterType::New();
    rasterFilter->SetInput(polyDataReader->GetOutput());
    const itk::MaskImage::PointType &origin = importerX->GetOutput()->GetOrigin();
    const itk::MaskImage::SpacingType &spacing = importerX->GetOutput()->GetSpacing();
    const itk::MaskImage::RegionType &region = importerX->GetOutput()->GetLargestPossibleRegion();
    rasterFilter->SetOrigin(origin);
    rasterFilter->SetSpacing(spacing);
    rasterFilter->SetSize(region.GetSize());
    rasterFilter->SetInsideValue(1);
    rasterFilter->SetOutsideValue(0);
    rasterFilter->Update();
    }
  else
    {
    importerMask = MaskImporterType::New();
    vtkImageData *vtkMask = dataSet->GetMaskImage(0);
    ASSERT_TRUE(vtkMask!=NULL);
    importerMask->SetInput(vtkMask);
    }

  typedef itk::AntiAliasBinaryImageFilter<itk::MaskImage,itk::PhaseContrast3DImage> DistanceFilterType;
  DistanceFilterType::Pointer distanceFilter = DistanceFilterType::New();
  //distanceFilter->UseImageSpacingOn();
  distanceFilter->SetNumberOfLayers(6);
  if (MeshFile)
    {
    std::cout << "Computing distance from rasterized mesh\n";
    distanceFilter->SetInput(rasterFilter->GetOutput());
    }
  else
    {
    std::cout << "Computing distance from given mask\n";
    distanceFilter->SetInput(importerMask->GetOutput());
    }
  distanceFilter->SetMaximumRMSError(0.007);
  //distanceFilter->SetMaximumRMSError(0.01);
  //distanceFilter->SetNumberOfIterations(2000);
  WriteImage(distanceFilter->GetOutput(), "AntiAliasedDistanceFieldOriginal.vtk");
  distanceFilter->Print(std::cout);
}

int main(int argc, char **argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  _ARGC = argc;
  _ARGV = argv;
  return RUN_ALL_TESTS();
}
