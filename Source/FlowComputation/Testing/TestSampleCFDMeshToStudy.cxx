#include <iostream>
#include "gtest/gtest.h"
#include "pcmrSampleCFDMeshToStudy.h"
#include "vtkUnstructuredGrid.h"
#include "vtkUnstructuredGridReader.h"
#include "vtkXMLUnstructuredGridReader.h"
#include "vtkSmartPointer.h"

static int _ARGC = 0;
static char **_ARGV = NULL;
static char *CFDMeshFile = NULL;
static char *PrefixOutput = NULL;
static vtkSmartPointer<vtkUnstructuredGrid> CFDMesh;
static double Spacing[] = { 1.5, 1.5, 1.5 };
static int SampleSize = 25;

int ParsePositive(const char* name, const char* value, double &v)
{
  v = atof(value);
  if (v <= 0)
    {
    std::cerr << "Invalid " << name << " argument: " << value << std::endl;
    return -1;
    }
  return 0;
}

int ParsePositive(const char* name, const char* value, int &v)
{
  v = atoi(value);
  if (v <= 0)
    {
    std::cerr << "Invalid " << name << " argument: " << value << std::endl;
    return -1;
    }
  return 0;
}

TEST(TestSampleCFDMeshToStudy, CommandLineArgs)
{
  ASSERT_TRUE( _ARGV != NULL );
  ASSERT_TRUE( _ARGC == 3 || _ARGC == 6 || _ARGC == 7 ) << "usage: '" << _ARGV[0] << "' CFDMesh PrefixOutput ?sx sy sz n?\n";
  CFDMeshFile = _ARGV[1];
  PrefixOutput = _ARGV[2];
  if( _ARGC >= 6 )
    {
    double spacing;
    if( ParsePositive( "sx", _ARGV[ 3 ], spacing) == 0 )
      {
      Spacing[ 0 ] = spacing;
      }
    else
      {
      std::cout << "assuming default spacing = 1.0\n";
      Spacing[ 0 ] = 1.0;
      }
    if( ParsePositive( "sy", _ARGV[ 4 ], spacing) == 0 )
      {
      Spacing[ 1 ] = spacing;
      }
    else
      {
      std::cout << "assuming default spacing = 1.0\n";
      Spacing[ 1 ] = 1.0;
      }
    if( ParsePositive( "sz", _ARGV[ 5 ], spacing) == 0 )
      {
      Spacing[ 2 ] = spacing;
      }
    else
      {
      std::cout << "assuming default spacing = 1.0\n";
      Spacing[ 2 ] = 1.0;
      }
    }
  if ( _ARGC == 7 )
    {
    int nsample;
    if( ParsePositive( "n", _ARGV[ 6 ], nsample ) == 0 )
      {
      SampleSize = nsample;
      }
    else
      {
      std::cout << "assuming SampleSize = 25\n";
      SampleSize = 25;
      }
    }

}

TEST( TestSampleCFDMeshToStudy, ReadCFD )
{
  ASSERT_TRUE( CFDMeshFile != NULL );
  vtkSmartPointer<vtkXMLUnstructuredGridReader> reader =  vtkSmartPointer<vtkXMLUnstructuredGridReader>::New( );
  reader->SetFileName( CFDMeshFile );
  reader->Update( );
  CFDMesh = reader->GetOutput( );
}

template <class T>
void DumpArray( const char* name, const T *array, int size = 3 )
{
  std::cout << name << " = [";
  for( int i = 0; i < size; i++ )
    {
    std::cout << ( !i ? "" : ", " ) << array[ i ];
    }
  std::cout << "]\n";
}

TEST( TestSampleCFDMeshToStudy, Voxelize )
{
  unsigned int ExtraPad = 8;
  pcmr::StatusType status =
    pcmr::SampleCFDMeshToStudy( CFDMesh, PrefixOutput, Spacing, ExtraPad, SampleSize, 0.1 );
  ASSERT_TRUE( status == pcmr::OK );
}

int main(int argc, char **argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  _ARGC = argc;
  _ARGV = argv;
  return RUN_ALL_TESTS();
}
