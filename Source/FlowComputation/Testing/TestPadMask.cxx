  
#include "itkPhaseContrastImage.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itkSimpleFilterWatcher.h"
#include "itkConstantPadImageFilter.h"

typedef itk::PhaseContrast3DImage ImageType;
//typedef itk::MaskImage ImageType;

typedef itk::ImageFileReader<ImageType> MaskReaderType;
typedef itk::ImageFileWriter<ImageType> MaskWriterType;
typedef itk::ConstantPadImageFilter<ImageType,ImageType> PadFilterType;

int main(int argc, char* argv[])
{
  if (argc < 3 || argc > 4)
  {
    std::cerr << "Usage:\n";
    std::cerr << argv[0] << " input_image.vtk output_image_padded.vtk ?n?\n";
    return -1;
  }
  
  MaskReaderType::Pointer reader = MaskReaderType::New();
  
  reader->SetFileName(argv[1]);
  
  int nPad = -1;
  if (argc==4)
    {
    nPad = atoi(argv[3]);
    }
  if (nPad <= 0)
    {
    std::cout << "Invalid nPad value =" << nPad << ". Using default value 8\n";
    nPad = 8;
    }
  itk::MaskImage::SizeType lowerExtendRegion;
  lowerExtendRegion[0] = nPad;
  lowerExtendRegion[1] = nPad;
  lowerExtendRegion[2] = nPad;
  
  itk::MaskImage::SizeType upperExtendRegion;
  upperExtendRegion[0] = nPad;
  upperExtendRegion[1] = nPad;
  upperExtendRegion[2] = nPad;
  
  PadFilterType::Pointer padFilter = PadFilterType::New();
  itk::SimpleFilterWatcher watcherFill(padFilter, "Pad filter");
  
  padFilter->SetPadLowerBound(lowerExtendRegion);
  padFilter->SetPadUpperBound(upperExtendRegion);
  padFilter->SetInput(reader->GetOutput());
  
  MaskWriterType::Pointer writer = MaskWriterType::New();
  writer->SetInput(padFilter->GetOutput());
  writer->SetFileName(argv[2]);
  writer->Update();
  
  return 0; 
}


