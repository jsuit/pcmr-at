#include <iostream>
#include "pcmrFlow4DReader.h"
#include "gtest/gtest.h"
#include "itkVTKImageToImageFilter.h"
#include "itkSignedMaurerDistanceMapImageFilter.h"
#include "pcmrImageUtil.h"
#include "itkZeroCrossingImageFilter.h"

static int _ARGC = 0;
static char **_ARGV = NULL;

static const char* StudyDir = NULL;
static pcmr::Flow4DReader::Pointer dataSet = NULL;

TEST(TestZeroCrossing, TestCommandLineArgs)
{
  ASSERT_TRUE(_ARGV!=NULL);
  ASSERT_GE(_ARGC,1) << "usage: '" << _ARGV[0] << "' study\n";
  StudyDir = _ARGV[1];
}

TEST(TestZeroCrossing, TestLoadDataSet)
{
  ASSERT_TRUE(StudyDir!=NULL);
  dataSet = pcmr::Flow4DReader::New();

  pcmr::StatusType status = dataSet->SetDirectory(StudyDir);
  if (status != pcmr::OK)
    {
    dataSet = NULL;
    }
  ASSERT_EQ(pcmr::OK, status) << "SetDirectory: " << pcmr::GetStatusDescription(status) << "\n";
}

TEST(TestZeroCrossing, TestComputeDistance)
{
  typedef itk::VTKImageToImageFilter<itk::MaskImage> MaskImporterType;
  MaskImporterType::Pointer importerMask = MaskImporterType::New();
  vtkImageData *vtkMask = dataSet->GetMaskImage(3);
  ASSERT_TRUE(vtkMask!=NULL);

  importerMask->SetInput(vtkMask);

  typedef itk::SignedMaurerDistanceMapImageFilter<itk::MaskImage,itk::PhaseContrast3DImage> DistanceFilterType;
  DistanceFilterType::Pointer distanceFilter = DistanceFilterType::New();
  distanceFilter->SetInput(importerMask->GetOutput());
  distanceFilter->SquaredDistanceOff();
  distanceFilter->UseImageSpacingOn();
 
  pcmr::WriteImage(distanceFilter->GetOutput(), "DistanceFieldOriginal.vtk");

  typedef itk::ZeroCrossingImageFilter<itk::PhaseContrast3DImage,itk::MaskImage> ZeroCrossingType;
  ZeroCrossingType::Pointer extractor = ZeroCrossingType::New();
  extractor->SetInput(distanceFilter->GetOutput());
  pcmr::WriteImage(extractor->GetOutput(), "ZeroCrossing.vtk");
  
}

int main(int argc, char **argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  _ARGC = argc;
  _ARGV = argv;
  return RUN_ALL_TESTS();
}
