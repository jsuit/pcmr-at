#include <iostream>
#include "pcmrFlow4DReader.h"
#include "gtest/gtest.h"
#include "itkBSplineControlPointImageFunction.h"
#include "itkTimeProbe.h"
#include "itkVTKImageToImageFilter.h"
#include "itkComposeImageFilter.h"

static int _ARGC = 0;
static char **_ARGV = NULL;

static const char* StudyDir = NULL;
static pcmr::Flow4DReader::Pointer dataSet = NULL;
static int TimeStep = -1;
static double Radius = 0;

int ParsePositive(const char* name, const char* value, double &v)
{
  v = atof(value);
  if (v <= 0)
    {
    std::cerr << "Invalid " << name << " argument: " << value << std::endl;
    return -1;
    }
  return 0;
}

int ParsePositive(const char* name, const char* value, int &v)
{
  v = atoi(value);
  if (v <= 0)
    {
    std::cerr << "Invalid " << name << " argument: " << value << std::endl;
    return -1;
    }
  return 0;
}

TEST(TestApproximateSplineFunction, CommandLineArgs)
{
  ASSERT_TRUE(_ARGV!=NULL);
  ASSERT_EQ(_ARGC,4) << "usage: '" << _ARGV[0] << "' study time-step radius\n";
  StudyDir = _ARGV[1];
  TimeStep = atoi(_ARGV[2]);
  ASSERT_GE(TimeStep,0) << " time-step must be positive";
  ASSERT_TRUE(!ParsePositive("radius", _ARGV[3], Radius));
}

TEST(TestApproximateSplineFunction, LoadDataSet)
{
  ASSERT_TRUE(StudyDir!=NULL);
  dataSet = pcmr::Flow4DReader::New();

  pcmr::StatusType status = dataSet->SetDirectory(StudyDir);
  if (status != pcmr::OK)
    {
    dataSet = NULL;
    }
  ASSERT_EQ(pcmr::OK, status) << "SetDirectory: " << pcmr::GetStatusDescription(status) << "\n";
  if (TimeStep < 0 || TimeStep >= dataSet->GetNumberOfTimeSteps())
    {
    int ts = dataSet->GetNumberOfTimeSteps() / 5;
    std::cout << "Invalid time-step '" << TimeStep << "' using default " << ts << std::endl;
    TimeStep = ts;
    }
}

TEST(TestApproximateSplineFunction, Evaluate)
{
  ASSERT_TRUE(dataSet);
  typedef itk::VTKImageToImageFilter<itk::PhaseContrast3DImage> ImporterType;
  ImporterType::Pointer importerX = ImporterType::New();
  ImporterType::Pointer importerY = ImporterType::New();
  ImporterType::Pointer importerZ = ImporterType::New();
  vtkImageData *vtkImgX = dataSet->GetFlowImage(pcmr::Flow4DReader::FlowX,
                                                TimeStep);
  vtkImageData *vtkImgY = dataSet->GetFlowImage(pcmr::Flow4DReader::FlowY,
                                                TimeStep);
  vtkImageData *vtkImgZ = dataSet->GetFlowImage(pcmr::Flow4DReader::FlowZ,
                                                TimeStep);
  importerX->SetInput(vtkImgX);
  importerY->SetInput(vtkImgY);
  importerZ->SetInput(vtkImgZ);
  typedef itk::ComposeImageFilter<itk::PhaseContrast3DImage,itk::PhaseContrastVector1DImage> ComposeFilter;
  ComposeFilter::Pointer composer = ComposeFilter::New();
  //composer->SetInput(0, importerX->GetOutput());
  composer->SetInput(0, importerY->GetOutput());
  //composer->SetInput(2, importerZ->GetOutput());
  composer->Update();
  itk::PhaseContrastVector1DImage::Pointer itkFlow = composer->GetOutput();

  itk::PhaseContrastVector1DImage::SpacingType spacing = itkFlow->GetSpacing();
  const itk::PhaseContrastVector1DImage::PointType &origin = itkFlow->GetOrigin();
  typedef itk::PhaseContrastVector1DImage::RegionType RegionType;
  RegionType region = itkFlow->GetLargestPossibleRegion();

  typedef itk::BSplineControlPointImageFunction<itk::PhaseContrastVector1DImage> BSplinerType;
  BSplinerType::Pointer bspliner = BSplinerType::New();
  bspliner->SetOrigin(origin);
  bspliner->SetSpacing(spacing);
  bspliner->SetSize(region.GetSize());
  bspliner->SetSplineOrder(5);
  bspliner->SetInputImage(itkFlow);

  double pts[][3] = 
    {
      {0, 0, 0},
      {0, 0, 0},
      {0, 0, 0},
      {0, 0, 0},
      {0, 0, 0},
      {0, 0, 0},
      {0, 0, 0},
      {0, 0, 0}
     };
  pts[1][2] = Radius/2.0;
  pts[2][2] = -Radius/2.0;
  pts[3][2] = Radius;
  pts[4][2] = -Radius;
  pts[5][2] = Radius-0.05;
  pts[6][2] = Radius-0.25;
  pts[7][2] = Radius-0.1;
  
  BSplinerType::GradientType gradient;
  BSplinerType::OutputType value;
  BSplinerType::PointType p;

  for (int i = 0; i < sizeof(pts)/sizeof(pts[0]); i++)
    {
    p = pts[i];
    std::cout << "Evaluating at point " << p << std::endl;
    value = bspliner->EvaluateAtParametricPoint(p);
    std::cout << "Value: ";
    std::cout << value << std::endl;
    gradient = bspliner->EvaluateGradientAtParametricPoint(p);
    std::cout << "Gradient:\n";
    std::cout << gradient << std::endl;
    }
}

int main(int argc, char **argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  _ARGC = argc;
  _ARGV = argv;
  return RUN_ALL_TESTS();
}
