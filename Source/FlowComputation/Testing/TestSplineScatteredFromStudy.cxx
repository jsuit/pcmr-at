#include <iostream>
#include "pcmrImageUtil.h"
#include "pcmrFlow4DReader.h"
#include "gtest/gtest.h"
#include "itkPointSet.h"
#include "itkBSplineScatteredDataPointSetToImageFilter.h"
#include "itkTimeProbe.h"
#include "itkVTKImageToImageFilter.h"
#include "itkComposeImageFilter.h"
#include "itkMesh.h"
#include "itkMeshFileReader.h"
#include "itkMersenneTwisterRandomVariateGenerator.h"
#include "itkImageSliceIteratorWithIndex.h"

typedef itk::Mesh<float,3> TriangleMeshType;
typedef itk::MeshFileReader<TriangleMeshType> MeshReaderType;

static int _ARGC = 0;
static char **_ARGV = NULL;

static const char* StudyDir = NULL;
static pcmr::Flow4DReader::Pointer dataSet = NULL;
static int TimeStep = -1;
static const char* MeshFile = NULL;
static MeshReaderType::Pointer  polyDataReader = NULL;

int ParsePositive(const char* name, const char* value, double &v)
{
  v = atof(value);
  if (v <= 0)
    {
    std::cerr << "Invalid " << name << " argument: " << value << std::endl;
    return -1;
    }
  return 0;
}

int ParsePositive(const char* name, const char* value, int &v)
{
  v = atoi(value);
  if (v <= 0)
    {
    std::cerr << "Invalid " << name << " argument: " << value << std::endl;
    return -1;
    }
  return 0;
}

TEST(TestSplineScatteredFromStudy, CommandLineArgs)
{
  ASSERT_TRUE(_ARGV!=NULL);
  ASSERT_EQ(_ARGC,4) << "usage: '" << _ARGV[0] << "' study time-step mesh\n";
  StudyDir = _ARGV[1];
  TimeStep = atoi(_ARGV[2]);
  MeshFile = _ARGV[3];
  ASSERT_GE(TimeStep,0) << " time-step must be positive";
}

TEST(TestSplineScatteredFromStudy, LoadDataSet)
{
  ASSERT_TRUE(StudyDir!=NULL);
  dataSet = pcmr::Flow4DReader::New();

  pcmr::StatusType status = dataSet->SetDirectory(StudyDir);
  if (status != pcmr::OK)
    {
    dataSet = NULL;
    }
  ASSERT_EQ(pcmr::OK, status) << "SetDirectory: " << pcmr::GetStatusDescription(status) << "\n";
  if (TimeStep < 0 || TimeStep >= dataSet->GetNumberOfTimeSteps())
    {
    int ts = dataSet->GetNumberOfTimeSteps() / 5;
    std::cout << "Invalid time-step '" << TimeStep << "' using default " << ts << std::endl;
    TimeStep = ts;
    }
  polyDataReader = MeshReaderType::New();
  polyDataReader->SetFileName(MeshFile);
  polyDataReader->Update();
}

TEST(TestSplineScatteredFromStudy, Evaluate)
{
  typedef itk::Statistics::MersenneTwisterRandomVariateGenerator GeneratorType;
  GeneratorType::Pointer generator = GeneratorType::New();

  generator->Initialize();

  ASSERT_TRUE(dataSet);
  typedef itk::VTKImageToImageFilter<itk::PhaseContrast3DImage> ImporterType;
  ImporterType::Pointer importerX = ImporterType::New();
  ImporterType::Pointer importerY = ImporterType::New();
  ImporterType::Pointer importerZ = ImporterType::New();
  vtkImageData *vtkImgX = dataSet->GetFlowImage(pcmr::Flow4DReader::FlowX,
                                                TimeStep);
  vtkImageData *vtkImgY = dataSet->GetFlowImage(pcmr::Flow4DReader::FlowY,
                                                TimeStep);
  vtkImageData *vtkImgZ = dataSet->GetFlowImage(pcmr::Flow4DReader::FlowZ,
                                                TimeStep);
  importerX->SetInput(vtkImgX);
  importerY->SetInput(vtkImgY);
  importerZ->SetInput(vtkImgZ);
  typedef itk::ComposeImageFilter<itk::PhaseContrast3DImage,itk::PhaseContrastVector3DImage> ComposeFilter;
  ComposeFilter::Pointer composer = ComposeFilter::New();
  composer->SetInput(0, importerX->GetOutput());
  composer->SetInput(1, importerY->GetOutput());
  composer->SetInput(2, importerZ->GetOutput());
  composer->Update();
  itk::PhaseContrastVector3DImage::Pointer itkFlow = composer->GetOutput();

  itk::PhaseContrastVector3DImage::SpacingType spacing = itkFlow->GetSpacing();
  const itk::PhaseContrastVector3DImage::PointType &origin = itkFlow->GetOrigin();
  typedef itk::PhaseContrastVector1DImage::RegionType RegionType;
  RegionType region = itkFlow->GetLargestPossibleRegion();
  
  typedef itk::PointSet <itk::PhaseContrastVector3DImage::PixelType, 3>   PointSetType;
  PointSetType::Pointer pointSet = PointSetType::New();

  itk::ImageRegionIteratorWithIndex<itk::PhaseContrastVector3DImage>
    It( itkFlow, region );

  // Iterate through the input image which consists of multivalued
  // foreground pixels (=nonzero) and background values (=zero).
  // The foreground pixels comprise the input point set.

  int count_r = 0;
  for ( It.GoToBegin(); !It.IsAtEnd(); ++It )
    {
    itk::PhaseContrastVector3DImage::PixelType p = It.Get();
    double r = generator->GetUniformVariate(0, 1);
    if ( fabs(p[0]) > 0 || fabs(p[1]) > 0 || fabs(p[2]) > 0 || r < 0.4)
      {
      // We extract both the 3-D location of the point
      // and the pixel value of that point.

      PointSetType::PointType point;
      itkFlow->TransformIndexToPhysicalPoint( It.GetIndex(), point );

      unsigned long i = pointSet->GetNumberOfPoints();
      pointSet->SetPoint( i, point );

      //PointSetType::PixelType V( DataDimension );
      //V[0] = static_cast<RealType>( It.Get() );
      pointSet->SetPointData( i, p );

      if (!(fabs(p[0]) > 0 || fabs(p[1]) > 0 || fabs(p[2]) > 0) && r < 0.4)
        {
        ++count_r;
        }
      }
    }
#if 0
  typedef itk::ImageSliceIteratorWithIndex<itk::PhaseContrastVector3DImage> SliceIteratorType;
  SliceIteratorType itSlice(itkFlow, region);
  itk::PhaseContrastVector3DImage::PixelType nullVelocity;
  nullVelocity[0] = nullVelocity[1] = nullVelocity[2] = 0.0;
  for (unsigned int i = 0; i < 3; i++)
    {
    for (unsigned int j = i+1; j < 3; j++)
      {
      itSlice.SetFirstDirection(i);
      itSlice.SetSecondDirection(j);
      // visit first slice
      itSlice.GoToBegin();
      size_t count0 = 0;
      while( !itSlice.IsAtEndOfSlice() )
        {
        while( !itSlice.IsAtEndOfLine() )
          {
          PointSetType::PointType point;
          itkFlow->TransformIndexToPhysicalPoint(itSlice.GetIndex(), point );
          unsigned long i = pointSet->GetNumberOfPoints();
          pointSet->SetPoint(i, point);
          pointSet->SetPointData(i, nullVelocity);
          ++itSlice;
          ++count0;
          }
        itSlice.NextLine();
        }
      // visit last slice
      size_t count1 = 0;
      itSlice.GoToReverseBegin();
      while( !itSlice.IsAtReverseEndOfSlice() )
        {
        while( !itSlice.IsAtReverseEndOfLine() )
          {
          PointSetType::PointType point;
          itkFlow->TransformIndexToPhysicalPoint(itSlice.GetIndex(), point );
          unsigned long i = pointSet->GetNumberOfPoints();
          pointSet->SetPoint(i, point);
          pointSet->SetPointData(i, nullVelocity);
          --itSlice;
          ++count1;
          }
        itSlice.PreviousLine();
        }
      if (count1 != count0)
        {
        std::cout << "last slice distint first slice size\n";
        std::cout << count0 << " != " << count1 << std::endl;
        }
      }
    }
#endif

  std::cout << count_r << " random null velocity were set\n";
  // Instantiate the B-spline filter and set the desired parameters.
  typedef itk::BSplineScatteredDataPointSetToImageFilter
    <PointSetType, itk::PhaseContrastVector3DImage> FilterType;
  FilterType::Pointer filter = FilterType::New();
  filter->SetSplineOrder( 2 );
  FilterType::ArrayType ncps;
  ncps.Fill( 4 );
  filter->SetNumberOfControlPoints( ncps );
  filter->SetNumberOfLevels( 5 );
  FilterType::ArrayType close;
  close.Fill( 0 );
  filter->SetCloseDimension( close );

  // Define the parametric domain.
  filter->SetOrigin( origin );
  filter->SetSpacing( spacing );
  filter->SetSize( region.GetSize() );
  filter->SetDirection( itkFlow->GetDirection() );

  filter->SetInput( pointSet );

  try
    {
    filter->Update();
    }
  catch (...)
    {
    ASSERT_TRUE(false) << "Test 1: itkBSplineScatteredDataImageFilter exception thrown";
    }

  std::cout << "Origin: " << filter->GetOrigin() << std::endl;
  std::cout << "Spacing: " << filter->GetSpacing() << std::endl;
  std::cout << "Size: " << filter->GetSize() << std::endl;
  std::cout << "Direction: " << filter->GetDirection() << std::endl;

  std::cout << "Number of control points: " <<
    filter->GetNumberOfControlPoints() << std::endl;
  std::cout << "Current number of control points: " <<
    filter->GetCurrentNumberOfControlPoints() << std::endl;
  std::cout << "Number of levels: " <<
    filter->GetNumberOfLevels() << std::endl;
  std::cout << "Close dimension: " <<
    filter->GetCloseDimension() << std::endl;
  std::cout << "Spline order: " << filter->GetSplineOrder() << std::endl;
  pcmr::WriteImage(filter->GetOutput(), "SplineScatteredFromStudy.vtk");
  
}

int main(int argc, char **argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  _ARGC = argc;
  _ARGV = argv;
  return RUN_ALL_TESTS();
}
