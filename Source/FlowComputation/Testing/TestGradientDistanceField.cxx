#include <iostream>
#include "pcmrFlow4DReader.h"
#include "gtest/gtest.h"
#include "vtkPolyData.h"
#include "vtkNew.h"
#include "itkComposeImageFilter.h"
#include "itkSignedMaurerDistanceMapImageFilter.h"
#include "itkGradientRecursiveGaussianImageFilter.h"
#include "itkUnaryFunctorImageFilter.h"
#include "itkBSplineControlPointImageFilter.h"
#include "itkVTKImageToImageFilter.h"
#include "itkMath.h"
#include "TestHelper.h"

template< class TIVector, class TOVector>
class NormalizeVector
{
public:
  NormalizeVector() {};
  ~NormalizeVector() {};
  bool operator!=( const NormalizeVector & ) const
    {
    return false;
    }
  bool operator==( const NormalizeVector & other ) const
    {
    return !(*this != other);
    }
  inline TOVector operator()( const TIVector & A ) const
    {
      TOVector v( A );
      v.Normalize();
      return v;
    }
};

static int _ARGC = 0;
static char **_ARGV = NULL;

static const char* StudyDir = NULL;
static pcmr::Flow4DReader::Pointer dataSet = NULL;

TEST(TestDistanceMap, TestCommandLineArgs)
{
  ASSERT_TRUE(_ARGV!=NULL);
  ASSERT_GE(_ARGC,1) << "usage: '" << _ARGV[0] << "' study\n";
  StudyDir = _ARGV[1];
}

TEST(TestDistanceMap, TestLoadDataSet)
{
  ASSERT_TRUE(StudyDir!=NULL);
  dataSet = pcmr::Flow4DReader::New();

  pcmr::StatusType status = dataSet->SetDirectory(StudyDir);
  if (status != pcmr::OK)
    {
    dataSet = NULL;
    }
  ASSERT_EQ(pcmr::OK, status) << "SetDirectory: " << pcmr::GetStatusDescription(status) << "\n";
}

TEST(TestDistanceMap, TestComputeDistance)
{
  typedef itk::VTKImageToImageFilter<itk::MaskImage> MaskImporterType;
  MaskImporterType::Pointer importerMask = MaskImporterType::New();
  vtkImageData *vtkMask = dataSet->GetMaskImage(0);
  ASSERT_TRUE(vtkMask!=NULL);

  importerMask->SetInput(vtkMask);

  typedef itk::SignedMaurerDistanceMapImageFilter<itk::MaskImage,itk::PhaseContrast3DImage> DistanceFilterType;
  DistanceFilterType::Pointer distanceFilter = DistanceFilterType::New();
  distanceFilter->SetInput(importerMask->GetOutput());
  distanceFilter->SquaredDistanceOff();
  distanceFilter->UseImageSpacingOn();
 
  WriteImage(distanceFilter->GetOutput(), "DistanceFieldOriginal.vtk");
  itk::MaskImage::SpacingType spacing;
  itk::MaskImage::PointType origin;
  itk::MaskImage::RegionType region;

  typedef itk::ComposeImageFilter<itk::PhaseContrast3DImage,itk::PhaseContrastVector1DImage> ComposeFilter;
  ComposeFilter::Pointer composer = ComposeFilter::New();
  composer->SetInput(0, distanceFilter->GetOutput());
  composer->Update();
  typedef itk::BSplineControlPointImageFilter<itk::PhaseContrastVector1DImage> BSplineFilterType;
  BSplineFilterType::Pointer bsplineFilter = BSplineFilterType::New();
  
  const size_t level = 3;
  GetSubdivisionInfo(distanceFilter->GetOutput(), level, origin, spacing, region);
  region.Print(std::cout);
  bsplineFilter->SetOrigin(origin);
  bsplineFilter->SetSpacing(spacing);
  bsplineFilter->SetSize(region.GetSize());
  bsplineFilter->SetSplineOrder( 3 );
  bsplineFilter->SetInput(composer->GetOutput());
  bsplineFilter->Update();
  bsplineFilter->GetOutput()->Print(std::cout);
  WriteImage(bsplineFilter->GetOutput(), "DistanceFieldInterpolated.vtk");

  typedef itk::GradientRecursiveGaussianImageFilter<itk::PhaseContrast3DImage,itk::Covariant3DImage> GradientFilterType;
  GradientFilterType::Pointer gradientFilter = GradientFilterType::New();
  gradientFilter->SetInput(distanceFilter->GetOutput());
  WriteImage(gradientFilter->GetOutput(), "GradientDistanceFieldInterpolated.vtk");

  typedef itk::UnaryFunctorImageFilter<itk::Covariant3DImage, itk::Covariant3DImage, NormalizeVector<itk::Covariant3DImage::PixelType,itk::Covariant3DImage::PixelType> > NormalizeFilterType;
  NormalizeFilterType::Pointer normalizeFilter = NormalizeFilterType::New();
  normalizeFilter->SetInput(gradientFilter->GetOutput());
  WriteImage(normalizeFilter->GetOutput(), "NGradientDistanceFieldInterpolated.vtk");
}

int main(int argc, char **argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  _ARGC = argc;
  _ARGV = argv;
  return RUN_ALL_TESTS();
}
