#include <iostream>
#include "pcmrFlow4DReader.h"
#include "gtest/gtest.h"
#include "itkVTKImageToImageFilter.h"
#include "itkSignedMaurerDistanceMapImageFilter.h"
#include "pcmrImageUtil.h"
#include "itkLevelSetNeighborhoodExtractor.h"

static int _ARGC = 0;
static char **_ARGV = NULL;

static const char* StudyDir = NULL;
static pcmr::Flow4DReader::Pointer dataSet = NULL;

TEST(TestLevelSetNeigborhoodExtractor, TestCommandLineArgs)
{
  ASSERT_TRUE(_ARGV!=NULL);
  ASSERT_GE(_ARGC,1) << "usage: '" << _ARGV[0] << "' study\n";
  StudyDir = _ARGV[1];
}

TEST(TestLevelSetNeigborhoodExtractor, TestLoadDataSet)
{
  ASSERT_TRUE(StudyDir!=NULL);
  dataSet = pcmr::Flow4DReader::New();

  pcmr::StatusType status = dataSet->SetDirectory(StudyDir);
  if (status != pcmr::OK)
    {
    dataSet = NULL;
    }
  ASSERT_EQ(pcmr::OK, status) << "SetDirectory: " << pcmr::GetStatusDescription(status) << "\n";
}

TEST(TestLevelSetNeigborhoodExtractor, TestComputeDistance)
{
  typedef itk::VTKImageToImageFilter<itk::MaskImage> MaskImporterType;
  MaskImporterType::Pointer importerMask = MaskImporterType::New();
  vtkImageData *vtkMask = dataSet->GetMaskImage(3);
  ASSERT_TRUE(vtkMask!=NULL);

  importerMask->SetInput(vtkMask);

  typedef itk::SignedMaurerDistanceMapImageFilter<itk::MaskImage,itk::PhaseContrast3DImage> DistanceFilterType;
  DistanceFilterType::Pointer distanceFilter = DistanceFilterType::New();
  distanceFilter->SetInput(importerMask->GetOutput());
  distanceFilter->SquaredDistanceOff();
  distanceFilter->UseImageSpacingOn();
 
  pcmr::WriteImage(distanceFilter->GetOutput(), "DistanceFieldOriginal.vtk");

  typedef itk::LevelSetNeighborhoodExtractor<itk::PhaseContrast3DImage> LevelSetExtractorType;
  typedef LevelSetExtractorType::NodeContainer NodeContainerType;
  LevelSetExtractorType::Pointer extractor = LevelSetExtractorType::New();
  extractor->SetInputLevelSet(distanceFilter->GetOutput());
  extractor->SetLevelSetValue(0);
  extractor->Locate();
  //extractor->Print(std::cout);
  NodeContainerType::Pointer outside = extractor->GetOutsidePoints();
  NodeContainerType::Pointer inside = extractor->GetInsidePoints();
  std::cout << "Outside boundary has " << outside->size() << " nodes\n";
  std::cout << "Inside boundary has " << inside->size() << " nodes\n";
  //distanceFilter->GetOutput()->Print(std::cout);
  itk::PhaseContrast3DImage::Pointer insideImage = itk::PhaseContrast3DImage::New();
  insideImage->CopyInformation(distanceFilter->GetOutput());
  insideImage->SetRegions(distanceFilter->GetOutput()->GetLargestPossibleRegion());
  insideImage->Allocate();

  //insideImage->Print(std::cout);

  itk::ImageRegionIterator<itk::PhaseContrast3DImage> outputIterator(insideImage, insideImage->GetLargestPossibleRegion());
  while(!outputIterator.IsAtEnd())
    {
    outputIterator.Set(0);
    ++outputIterator;
    }
  NodeContainerType::iterator nodeIt;
  size_t countMask = 0;
  for(nodeIt = inside->begin(); nodeIt != inside->end(); ++nodeIt)
    {
    itk::PhaseContrast3DImage::IndexType index = nodeIt->GetIndex();
    double v = distanceFilter->GetOutput()->GetPixel(index);
    if (importerMask->GetOutput()->GetPixel(index))
      {
      ++countMask;
      }
    insideImage->SetPixel(index, 1);
    }
  std::cout << "There are " << countMask << " points inside the mask\n";
  pcmr::WriteImage(insideImage.GetPointer(), "InsideImage.vtk");
  
}

int main(int argc, char **argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  _ARGC = argc;
  _ARGV = argv;
  return RUN_ALL_TESTS();
}
