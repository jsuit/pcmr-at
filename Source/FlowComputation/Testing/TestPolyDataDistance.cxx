#include "vtkImplicitPolyDataDistance.h"
#include "vtkPolyDataNormals.h"
#include "vtkPolyDataReader.h"
#include "vtkPolyData.h"
#include "vtkQuadricDecimation.h"
#include "vtkSmartPointer.h"

std::ostream &PointPrint(std::ostream &out, double p[])
{
  out << "[";
  for(int i = 0; i < 3; i++)
    {
    out << (i? ", ":"") << p[i];
    }
  out << "]";
  return out;
}

int main(int argc, char *argv[])
{
  vtkSmartPointer<vtkPolyDataReader> reader = vtkSmartPointer<vtkPolyDataReader>::New();
  if (argc > 1)
    {
    reader->SetFileName(argv[1]);
    }
  else
    {
    // assume fix_mesh.vtk is in cwd
    reader->SetFileName("fix_mesh.vtk");
    }
  reader->Update();

  double bounds[6];
  reader->GetOutput()->GetBounds(bounds);
  std::cout << "bounds = [";
  for(int i = 0; i < 6; i++)
    {
    std::cout << (i? ", ":"") << bounds[i];
    }
  std::cout << "]\n";
  vtkSmartPointer<vtkImplicitPolyDataDistance> distance = vtkSmartPointer<vtkImplicitPolyDataDistance>::New();
  distance->SetInput(reader->GetOutput());

  double pointOutside0[] = {20.1489985602283, 214.994921655297, 204.774962757447};
  double pointOutside1[] = {3.61977956624778, 205.144823711696, 204.774962757442};
  PointPrint(std::cout, pointOutside0) << " ==> " << distance->EvaluateFunction(pointOutside0) << std::endl;
  PointPrint(std::cout, pointOutside1) << " ==> " << distance->EvaluateFunction(pointOutside1) << std::endl;
  return 0;
}

// ~/Data/pcmr/Studies/SIEMENS/CVS/WSS/SEGS/CVS_VV_Voting_EroDil3_DMap_MC_Lap_QSimp_Lap_Trimmed_Closed.vtk
