#include <iostream>
#include "pcmrFlow4DReader.h"
#include "gtest/gtest.h"
#include "vtkPolyData.h"
#include "vtkNew.h"
#include "itkBSplineControlPointImageFunction.h"
#include "itkBSplineControlPointImageFilter.h"
#include "itkVTKImageToImageFilter.h"
#include "itkComposeImageFilter.h"
#include "itkMath.h"
#include "itkImageFileWriter.h"
#include "itkVTKImageIO.h"

static int _ARGC = 0;
static char **_ARGV = NULL;

static const char* StudyDir = NULL;
static pcmr::Flow4DReader::Pointer dataSet = NULL;

TEST(TestInterpolateFlow, TestCommandLineArgs)
{
  ASSERT_TRUE(_ARGV!=NULL);
  ASSERT_GE(_ARGC,1) << "usage: '" << _ARGV[0] << "' study\n";
  StudyDir = _ARGV[1];
}

TEST(TestInterpolateFlow, TestLoadDataSet)
{
  ASSERT_TRUE(StudyDir!=NULL);
  dataSet = pcmr::Flow4DReader::New();

  pcmr::StatusType status = dataSet->SetDirectory(StudyDir);
  if (status != pcmr::OK)
    {
    dataSet = NULL;
    }
  ASSERT_EQ(pcmr::OK, status) << "SetDirectory: " << pcmr::GetStatusDescription(status) << "\n";
}

TEST(TestInterpolateFlow, TestInterpolate)
{
  typedef itk::VTKImageToImageFilter<itk::PhaseContrast3DImage> ImporterType;
  ImporterType::Pointer importerX = ImporterType::New();
  ImporterType::Pointer importerY = ImporterType::New();
  ImporterType::Pointer importerZ = ImporterType::New();
  vtkImageData *vtkImgX = dataSet->GetFlowImage(pcmr::Flow4DReader::FlowX,
                                                3);
  vtkImageData *vtkImgY = dataSet->GetFlowImage(pcmr::Flow4DReader::FlowY,
                                                3);
  vtkImageData *vtkImgZ = dataSet->GetFlowImage(pcmr::Flow4DReader::FlowZ,
                                                3);
  importerX->SetInput(vtkImgX);
  importerY->SetInput(vtkImgY);
  importerZ->SetInput(vtkImgZ);
  typedef itk::ComposeImageFilter<itk::PhaseContrast3DImage,itk::PhaseContrastVector3DImage> ComposeFilter;
  ComposeFilter::Pointer composer = ComposeFilter::New();
  composer->SetInput(0, importerX->GetOutput());
  composer->SetInput(1, importerY->GetOutput());
  composer->SetInput(2, importerZ->GetOutput());
  composer->Update();
  itk::PhaseContrastVector3DImage::Pointer itkFlow = composer->GetOutput();

  itk::PhaseContrastVector3DImage::SpacingType spacing = itkFlow->GetSpacing();
  const itk::PhaseContrastVector3DImage::PointType &origin = itkFlow->GetOrigin();
  typedef itk::PhaseContrastVector3DImage::RegionType RegionType;
  RegionType region = itkFlow->GetLargestPossibleRegion();

  itkFlow->Print(std::cout);

  region.Print(std::cout);
 
  int nx, ny, nz;
  double dx, dy, dz;
  const size_t level = 3;
  if (spacing[0] < spacing[1])
    {
    if (spacing[0] < spacing[2])
      {
      region.SetSize(0, region.GetSize(0)*level);
      region.SetSize(1, itk::Math::Round<RegionType::SizeValueType>(region.GetSize(1)*(spacing[1]/spacing[0])*level));
      region.SetSize(2, itk::Math::Round<RegionType::SizeValueType>(region.GetSize(2)*(spacing[2]/spacing[0])*level));
      spacing[0] /= level;
      spacing[2] = spacing[1] = spacing[0];
      }
    else
      {
      region.SetSize(0, itk::Math::Round<RegionType::SizeValueType>(region.GetSize(0)*(spacing[0]/spacing[2])*level));
      region.SetSize(1, itk::Math::Round<RegionType::SizeValueType>(region.GetSize(1)*(spacing[1]/spacing[2])*level));
      region.SetSize(2, region.GetSize(2)*level);
      spacing[2] /= level;
      spacing[1] = spacing[0] = spacing[2];
      }
    }
  else
    {
    if (spacing[1] < spacing[2])
      {
      region.SetSize(0, itk::Math::Round<RegionType::SizeValueType>(region.GetSize(0)*(spacing[0]/spacing[1])*level));
      region.SetSize(1, region.GetSize(1)*level);
      region.SetSize(2, itk::Math::Round<RegionType::SizeValueType>(region.GetSize(2)*(spacing[2]/spacing[1])*level));
      spacing[1] /= level;
      spacing[2] = spacing[0] = spacing[1];
      }
    else
      {
      region.SetSize(0, itk::Math::Round<RegionType::SizeValueType>(region.GetSize(0)*(spacing[0]/spacing[2])*level));
      region.SetSize(1, itk::Math::Round<RegionType::SizeValueType>(region.GetSize(1)*(spacing[1]/spacing[2])*level));
      region.SetSize(2, region.GetSize(2)*level);
      spacing[2] /= level;
      spacing[1] = spacing[0] = spacing[2];
      }    
    }
  itk::PhaseContrastVector3DImage::Pointer interpFlow = itk::PhaseContrastVector3DImage::New();
  interpFlow->SetSpacing(spacing);
  interpFlow->SetRegions(region);
  interpFlow->Allocate();

  region.Print(std::cout);
 
  
  typedef itk::BSplineControlPointImageFunction<itk::PhaseContrastVector3DImage> BSplinerType;
  BSplinerType::Pointer bspliner = BSplinerType::New();

  bspliner->SetOrigin(origin);
  bspliner->SetSpacing(spacing);
  bspliner->SetSize(region.GetSize());
  bspliner->SetSplineOrder( 3 );
  bspliner->SetInputImage(itkFlow);
  

  typedef itk::BSplineControlPointImageFilter<itk::PhaseContrastVector3DImage> BSplineFilterType;
  BSplineFilterType::Pointer bsplineFilter = BSplineFilterType::New();
  bsplineFilter->SetOrigin(origin);
  bsplineFilter->SetSpacing(spacing);
  bsplineFilter->SetSize(region.GetSize());
  bsplineFilter->SetSplineOrder( 3 );
  bsplineFilter->SetInput(itkFlow);

  bsplineFilter->Print(std::cout);

  bsplineFilter->Update();
  bsplineFilter->GetOutput()->Print(std::cout);
  typedef itk::ImageFileWriter<itk::PhaseContrastVector3DImage>  WriterType;
  typedef itk::VTKImageIO ImageIOType;
  WriterType::Pointer writer = WriterType::New();
  ImageIOType::Pointer vtkIO = ImageIOType::New();
  writer->SetInput(bsplineFilter->GetOutput());
  writer->SetFileName("/tmp/InterpolatedFlow3.vtk");
  writer->SetImageIO(vtkIO);
  try
    {
    writer->Update();
    }
  catch( itk::ExceptionObject & err )
    {
    std::cerr << "ExceptionObject caught !" << std::endl;
    std::cerr << err << std::endl;
    //return EXIT_FAILURE;
    }
  return;

  BSplinerType::OutputType data;
  BSplinerType::GradientType dataGrad;
  typedef itk::ImageRegionIteratorWithIndex<itk::PhaseContrastVector3DImage> IteratorType;
  IteratorType outputIt(interpFlow, region);
  size_t lastSlice = 0;
  for (outputIt.GoToBegin(); !outputIt.IsAtEnd(); ++outputIt)
    {
    itk::PhaseContrastVector3DImage::IndexType index = outputIt.GetIndex();
    data = bspliner->EvaluateAtIndex(index);
    //dataGrad = bspliner->EvaluateGradientAtIndex(outputIt.GetIndex());
    outputIt.Set(data);
    if (lastSlice != index[2])
      {
      lastSlice = index[2];
      std::cout << "Changing to slice " << lastSlice << std::endl;
      }
    }
}

int main(int argc, char **argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  _ARGC = argc;
  _ARGV = argv;
  return RUN_ALL_TESTS();
}
