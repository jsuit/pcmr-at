#include <iostream>
#include "pcmrFlow4DReader.h"
#include "gtest/gtest.h"
#include "vtkPolyData.h"
#include "vtkNew.h"
#include "itkBSplineControlPointImageFunction.h"
#include "itkBSplineControlPointImageFilter.h"
#include "itkVTKImageToImageFilter.h"
#include "itkComposeImageFilter.h"
#include "itkMaskImageFilter.h"
#include "itkMath.h"
#include "TestHelper.h"

//#define DONT_GENERATE_INTERPOLATED_IMAGE

static int _ARGC = 0;
static char **_ARGV = NULL;

static const char* StudyDir = NULL;
static pcmr::Flow4DReader::Pointer dataSet = NULL;

TEST(TestInterpolateMaskedFlow, TestCommandLineArgs)
{
  ASSERT_TRUE(_ARGV!=NULL);
  ASSERT_GE(_ARGC,1) << "usage: '" << _ARGV[0] << "' study\n";
  StudyDir = _ARGV[1];
}

TEST(TestInterpolateMaskedFlow, TestLoadDataSet)
{
  ASSERT_TRUE(StudyDir!=NULL);
  dataSet = pcmr::Flow4DReader::New();

  pcmr::StatusType status = dataSet->SetDirectory(StudyDir);
  if (status != pcmr::OK)
    {
    dataSet = NULL;
    }
  ASSERT_EQ(pcmr::OK, status) << "SetDirectory: " << pcmr::GetStatusDescription(status) << "\n";
}

TEST(TestInterpolateMaskedFlow, TestInterpolate)
{
  typedef itk::VTKImageToImageFilter<itk::PhaseContrast3DImage> ImporterType;
  typedef itk::VTKImageToImageFilter<itk::MaskImage> MaskImporterType;
  ImporterType::Pointer importerX = ImporterType::New();
  ImporterType::Pointer importerY = ImporterType::New();
  ImporterType::Pointer importerZ = ImporterType::New();
  MaskImporterType::Pointer importerMask = MaskImporterType::New();
  vtkImageData *vtkImgX = dataSet->GetFlowImage(pcmr::Flow4DReader::FlowX,
                                                3);
  ASSERT_TRUE(vtkImgX!=NULL);
  vtkImageData *vtkImgY = dataSet->GetFlowImage(pcmr::Flow4DReader::FlowY,
                                                3);
  ASSERT_TRUE(vtkImgY!=NULL);
  vtkImageData *vtkImgZ = dataSet->GetFlowImage(pcmr::Flow4DReader::FlowZ,
                                                3);
  ASSERT_TRUE(vtkImgZ!=NULL);
  vtkImageData *vtkMask = dataSet->GetMaskImage(3);
  ASSERT_TRUE(vtkMask!=NULL);
  importerX->SetInput(vtkImgX);
  importerY->SetInput(vtkImgY);
  importerZ->SetInput(vtkImgZ);
  importerMask->SetInput(vtkMask);
  typedef itk::ComposeImageFilter<itk::PhaseContrast3DImage,itk::PhaseContrastVector3DImage> ComposeFilter;
  ComposeFilter::Pointer composer = ComposeFilter::New();
  composer->SetInput(0, importerX->GetOutput());
  composer->SetInput(1, importerY->GetOutput());
  composer->SetInput(2, importerZ->GetOutput());
  composer->Update();
  itk::PhaseContrastVector3DImage::Pointer itkFlow = composer->GetOutput();

  itk::PhaseContrast3DImage::SpacingType spacing;
  itk::PhaseContrast3DImage::PointType origin;
  typedef itk::PhaseContrast3DImage::RegionType RegionType;
  RegionType region;

  itkFlow->Print(std::cout);

  const size_t level = 3;
  GetSubdivisionInfo(importerX->GetOutput(), level, origin, spacing, region);
 
  typedef itk::MaskImageFilter<itk::PhaseContrastVector3DImage,itk::MaskImage> MaskFilterType;
  MaskFilterType::Pointer masker = MaskFilterType::New();
  masker->SetInput(itkFlow);
  masker->SetMaskImage(importerMask->GetOutput());
  masker->Update();

  WriteImage(masker->GetOutput(), "MaskedFlowOriginal.vtk");

#ifndef DONT_GENERATE_INTERPOLATED_IMAGE
  typedef itk::BSplineControlPointImageFilter<itk::PhaseContrastVector3DImage> BSplineFilterType;
  BSplineFilterType::Pointer bsplineFilter = BSplineFilterType::New();
  bsplineFilter->SetOrigin(origin);
  bsplineFilter->SetSpacing(spacing);
  bsplineFilter->SetSize(region.GetSize());
  bsplineFilter->SetSplineOrder(3);
  bsplineFilter->SetInput(masker->GetOutput());

  bsplineFilter->Update();
  WriteImage(bsplineFilter->GetOutput(), "MaskedFlowInterpolated.vtk");
#endif

  itk::PhaseContrastVector3DImage::PointType::ValueType p[3] = 
    {123.914038689804, 74.9864054886203, 178.431703246562};
  itk::PhaseContrastVector3DImage::PointType pt = p, ptOriginal, pt1;
  itk::PhaseContrastVector3DImage::IndexType indexOriginal, indexInterpolated;
  itkFlow->TransformPhysicalPointToIndex(pt, indexOriginal);
  std::cout << pt << " has original index " << indexOriginal << std::endl;
  itkFlow->TransformIndexToPhysicalPoint(indexOriginal, ptOriginal);
  std::cout << indexOriginal << " map to original point " << ptOriginal << std::endl;

#ifndef DONT_GENERATE_INTERPOLATED_IMAGE
  bsplineFilter->GetOutput()->TransformPhysicalPointToIndex(pt, indexInterpolated);
  std::cout << pt << " has interpolated index " << indexInterpolated << std::endl;

  bsplineFilter->GetOutput()->TransformIndexToPhysicalPoint(indexInterpolated, pt1);
  std::cout << indexInterpolated << " map to interpolated point " << pt1 << std::endl;
#endif

  itk::PhaseContrastVector3DImage::PixelType v = masker->GetOutput()->GetPixel(indexOriginal);
  std::cout << "Flow meassured at " << indexOriginal << " is " << v << std::endl;

#ifndef DONT_GENERATE_INTERPOLATED_IMAGE
  v = bsplineFilter->GetOutput()->GetPixel(indexInterpolated);
  std::cout << "Flow approximated at " << indexInterpolated << " is " << v << std::endl;
#endif

 typedef itk::BSplineControlPointImageFunction<itk::PhaseContrastVector3DImage> BSplinerType;
  BSplinerType::Pointer bsplineFunction = BSplinerType::New();

  bsplineFunction->SetOrigin(origin);
  bsplineFunction->SetSpacing(spacing);
  bsplineFunction->SetSize(region.GetSize());
  bsplineFunction->SetSplineOrder( 3 );
  bsplineFunction->SetInputImage(masker->GetOutput());
  

  BSplinerType::OutputType data;

#ifndef DONT_GENERATE_INTERPOLATED_IMAGE
  data = bsplineFunction->EvaluateAtIndex(indexInterpolated);
  std::cout << "Flow evaluated at index " << indexInterpolated << " is " << data << std::endl;
#endif

  data = bsplineFunction->EvaluateAtParametricPoint(ptOriginal);
  std::cout << "Flow evaluated at point " << ptOriginal << " is " << data << std::endl;
}

int main(int argc, char **argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  _ARGC = argc;
  _ARGV = argv;
  return RUN_ALL_TESTS();
}
