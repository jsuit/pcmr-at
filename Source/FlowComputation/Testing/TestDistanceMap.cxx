#include <iostream>
#include "pcmrFlow4DReader.h"
#include "gtest/gtest.h"
#include "vtkPolyData.h"
#include "vtkNew.h"
#include "itkComposeImageFilter.h"
#include "itkGradientRecursiveGaussianImageFilter.h"
#include "itkSignedMaurerDistanceMapImageFilter.h"
#include "itkBSplineControlPointImageFunction.h"
#include "itkBSplineControlPointImageFilter.h"
#include "itkVTKImageToImageFilter.h"
#include "itkMath.h"
#include "TestHelper.h"

#define DONT_GENERATE_INTERPOLATED_IMAGE

static int _ARGC = 0;
static char **_ARGV = NULL;

static const char* StudyDir = NULL;
static pcmr::Flow4DReader::Pointer dataSet = NULL;

TEST(TestDistanceMap, TestCommandLineArgs)
{
  ASSERT_TRUE(_ARGV!=NULL);
  ASSERT_EQ(_ARGC,2) << "usage: '" << _ARGV[0] << "' study\n";
  StudyDir = _ARGV[1];
}

TEST(TestDistanceMap, TestLoadDataSet)
{
  ASSERT_TRUE(StudyDir!=NULL);
  dataSet = pcmr::Flow4DReader::New();

  pcmr::StatusType status = dataSet->SetDirectory(StudyDir);
  if (status != pcmr::OK)
    {
    dataSet = NULL;
    }
  ASSERT_EQ(pcmr::OK, status) << "SetDirectory: " << pcmr::GetStatusDescription(status) << "\n";
}

TEST(TestDistanceMap, TestComputeDistance)
{
  typedef itk::VTKImageToImageFilter<itk::MaskImage> MaskImporterType;
  MaskImporterType::Pointer importerMask = MaskImporterType::New();
  vtkImageData *vtkMask = dataSet->GetMaskImage(3);
  ASSERT_TRUE(vtkMask!=NULL);

  importerMask->SetInput(vtkMask);

  typedef itk::SignedMaurerDistanceMapImageFilter<itk::MaskImage,itk::PhaseContrast3DImage> DistanceFilterType;
  DistanceFilterType::Pointer distanceFilter = DistanceFilterType::New();
  distanceFilter->SetInput(importerMask->GetOutput());
  distanceFilter->SquaredDistanceOff();
  distanceFilter->UseImageSpacingOn();
 
  WriteImage(distanceFilter->GetOutput(), "DistanceFieldOriginal.vtk");
  itk::MaskImage::SpacingType spacing;
  itk::MaskImage::PointType origin;
  itk::MaskImage::RegionType region;

  typedef itk::ComposeImageFilter<itk::PhaseContrast3DImage,itk::PhaseContrastVector1DImage> ComposeFilter;
  ComposeFilter::Pointer composer = ComposeFilter::New();
  composer->SetInput(0, distanceFilter->GetOutput());
  composer->Update();
  
  const size_t level = 3;
  GetSubdivisionInfo(distanceFilter->GetOutput(), level, origin, spacing, region);
#ifndef DONT_GENERATE_INTERPOLATED_IMAGE
  typedef itk::BSplineControlPointImageFilter<itk::PhaseContrastVector1DImage> BSplineFilterType;
  BSplineFilterType::Pointer bsplineFilter = BSplineFilterType::New();
  //region.Print(std::cout);
  bsplineFilter->SetOrigin(origin);
  bsplineFilter->SetSpacing(spacing);
  bsplineFilter->SetSize(region.GetSize());
  bsplineFilter->SetSplineOrder( 3 );
  bsplineFilter->SetInput(composer->GetOutput());
  bsplineFilter->Update();
  //bsplineFilter->GetOutput()->Print(std::cout);
  WriteImage(bsplineFilter->GetOutput(), "DistanceFieldInterpolated.vtk");

  typedef itk::GradientRecursiveGaussianImageFilter<itk::PhaseContrastVector1DImage,itk::Covariant3DImage> GradientVector1DFilterType;
  GradientVector1DFilterType::Pointer gradientDistance = GradientVector1DFilterType::New();
  gradientDistance->SetInput(bsplineFilter->GetOutput());
  WriteImage(gradientDistance->GetOutput(), "GradientDistanceFieldInterpolated.vtk");
#endif

  itk::PhaseContrast3DImage::PointType::ValueType p[3] = 
    {123.914038689804, 74.9864054886203, 178.431703246562};
  itk::PhaseContrast3DImage::PointType pt = p, ptOriginal, pt1;
  itk::PhaseContrast3DImage::IndexType indexOriginal, indexInterpolated;
  importerMask->GetOutput()->TransformPhysicalPointToIndex(pt, indexOriginal);
  std::cout << pt << " has original index " << indexOriginal << std::endl;
  importerMask->GetOutput()->TransformIndexToPhysicalPoint(indexOriginal, ptOriginal);
  std::cout << indexOriginal << " map to original point " << ptOriginal << std::endl;

#ifndef DONT_GENERATE_INTERPOLATED_IMAGE
    bsplineFilter->GetOutput()->TransformPhysicalPointToIndex(pt, indexInterpolated);
  std::cout << pt << " has interpolated index " << indexInterpolated << std::endl;

  bsplineFilter->GetOutput()->TransformIndexToPhysicalPoint(indexInterpolated, pt1);
  std::cout << indexInterpolated << " map to interpolated point " << pt1 << std::endl;
#endif

  itk::PhaseContrast3DImage::PixelType v = distanceFilter->GetOutput()->GetPixel(indexOriginal);
  std::cout << "Distance meassured at " << indexOriginal << " is " << v << std::endl;

#ifndef DONT_GENERATE_INTERPOLATED_IMAGE
  v = bsplineFilter->GetOutput()->GetPixel(indexInterpolated)[0];
  std::cout << "Distance approximated at " << indexInterpolated << " is " << v << std::endl;
  std::cout << "G(d) at " << indexInterpolated << " is " << gradientDistance->GetOutput()->GetPixel(indexInterpolated) << std::endl;
#endif

 typedef itk::BSplineControlPointImageFunction<itk::PhaseContrastVector1DImage> BSplinerType;
  BSplinerType::Pointer bsplineFunction = BSplinerType::New();

  bsplineFunction->SetOrigin(origin);
  bsplineFunction->SetSpacing(spacing);
  bsplineFunction->SetSize(region.GetSize());
  bsplineFunction->SetSplineOrder( 3 );
  bsplineFunction->SetInputImage(composer->GetOutput());
  
  BSplinerType::OutputType data;

#ifndef DONT_GENERATE_INTERPOLATED_IMAGE
  data = bsplineFunction->EvaluateAtIndex(indexInterpolated);
  std::cout << "Distance evaluated at index " << indexInterpolated << " is " << data << std::endl;
#endif

  data = bsplineFunction->EvaluateAtParametricPoint(ptOriginal);
  std::cout << "Distance evaluated at point " << ptOriginal << " is " << data << std::endl;
}

int main(int argc, char **argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  _ARGC = argc;
  _ARGV = argv;
  return RUN_ALL_TESTS();
}
