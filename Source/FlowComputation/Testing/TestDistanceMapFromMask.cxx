  
#include "itkPhaseContrastImage.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itkSimpleFilterWatcher.h"
#include "itkAntiAliasBinaryImageFilter.h"

typedef itk::ImageFileReader<itk::MaskImage> MaskReaderType;
typedef itk::ImageFileWriter<itk::PhaseContrast3DImage> ImageWriterType;
typedef itk::AntiAliasBinaryImageFilter<itk::MaskImage,itk::PhaseContrast3DImage> DistanceFilterType;

int main(int argc, char* argv[])
{
  if (argc != 3)
    {
    std::cerr << "Usage:\n";
    std::cerr << argv[0] << " input_mask.vtk output_dmap.vtk\n";
    return -1;
    }
  
  MaskReaderType::Pointer reader = MaskReaderType::New();
  
  reader->SetFileName(argv[1]);
  
  DistanceFilterType::Pointer distanceFilter = DistanceFilterType::New();
  itk::SimpleFilterWatcher watcherFill(distanceFilter, "DistanceMap calculator");

  distanceFilter->UseImageSpacingOn();
  distanceFilter->SetNumberOfLayers(5);

  distanceFilter->SetInput(reader->GetOutput());
  distanceFilter->SetMaximumRMSError(0.005);

  ImageWriterType::Pointer writer = ImageWriterType::New();
  writer->SetInput(distanceFilter->GetOutput());
  writer->SetFileName(argv[2]);
  writer->Update();
  
  return 0;
}
