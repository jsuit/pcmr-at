#include <cmath>
#include "itkPhaseContrastImage.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itkConstantPadImageFilter.h"
#include "itkBinaryBallStructuringElement.h"
#include "itkBinaryOpeningByReconstructionImageFilter.h"
#include "itkBinaryMorphologicalOpeningImageFilter.h"
#include "itkVotingBinaryIterativeHoleFillingImageFilter.h"
#include "itkAntiAliasBinaryImageFilter.h"
#include "itkImageToVTKImageFilter.h"
#include "vtkImageMarchingCubes.h"
#include "vtkNew.h"
#include "vtkPolyDataWriter.h"
#include "vtkPolyData.h"
#include "vtkCellArray.h"
#include "vtkPoints.h"
#include "vtkQuadricDecimation.h"

#include "pcmrTaubinSmoother.h"

typedef itk::PhaseContrast3DImage ImageType;
typedef itk::MaskImage MaskType;

typedef itk::BinaryBallStructuringElement<MaskType::PixelType, 3> StructuringElementType;

typedef itk::ImageFileReader<MaskType> MaskReaderType;
typedef itk::ImageFileWriter<MaskType> MaskWriterType;
typedef itk::ImageFileWriter<ImageType> ImageWriterType;
typedef itk::ConstantPadImageFilter<MaskType,MaskType> PadFilterType;
typedef itk::BinaryMorphologicalOpeningImageFilter<MaskType,MaskType,StructuringElementType> BinaryOpeningFilterType;
typedef itk::VotingBinaryIterativeHoleFillingImageFilter<MaskType> HoleFillingFilterType;
typedef itk::AntiAliasBinaryImageFilter<MaskType,ImageType> LevelSetFilterType;
typedef itk::ImageToVTKImageFilter< ImageType > ImageToVTKImageFilterType;

class vtkMeshVisitor
{
public:
  void InitVisitor( vtkPolyData *pd )
  {
    this->m_Mesh = pd;
    this->m_IdNode = 0;
    pd->GetPolys()->InitTraversal();
  }

  vtkPolyData *m_Mesh;
  vtkIdType m_IdNode;

  static
  bool NextNode(float &x, float &y, float &z, void *data)
  {
    vtkMeshVisitor *visitor = static_cast<vtkMeshVisitor*>(data);
    vtkPoints *points = visitor->m_Mesh->GetPoints();
    if ( visitor->m_IdNode >= points->GetNumberOfPoints( ) )
      {
      return false;
      }
    double pt[3];
    points->GetPoint(visitor->m_IdNode, pt);
    
    x = pt[0];
    y = pt[1];
    z = pt[2];
    ++visitor->m_IdNode;
    return true;
  }

  static
  bool NextTriangle(size_t &n1, size_t &n2, size_t &n3, void *data)
  {
    vtkMeshVisitor *visitor = static_cast<vtkMeshVisitor*>(data);
    vtkIdType *idPts = 0;
    vtkIdType nPts = 0;
    vtkCellArray *cells = visitor->m_Mesh->GetPolys( );
    if ( cells->GetNextCell( nPts, idPts ) )
      {
      assert( nPts == 3 );
      n1 = idPts[ 0 ];
      n2 = idPts[ 1 ];
      n3 = idPts[ 2 ];
      return true;
      }
    else
      {
      return false;
      }
  }
};

int main(int argc, char* argv[])
{
  if (argc < 3 || argc > 4)
  {
    std::cerr << "Usage:\n";
    std::cerr << argv[0] << " input_image.vtk output_mesh.vtk ?n?\n";
    return -1;
  }
  
  MaskReaderType::Pointer reader = MaskReaderType::New();
  
  reader->SetFileName(argv[1]);
  
  int nPad = 8;
  if (argc==4)
    {
    nPad = atoi(argv[3]);
    }
  if (nPad <= 0)
    {
    std::cout << "Invalid nPad value =" << nPad << ". Using default value 8\n";
    nPad = 8;
    }
  itk::MaskImage::SizeType lowerExtendRegion;
  lowerExtendRegion[0] = nPad;
  lowerExtendRegion[1] = nPad;
  lowerExtendRegion[2] = nPad;
  
  itk::MaskImage::SizeType upperExtendRegion;
  upperExtendRegion[0] = nPad;
  upperExtendRegion[1] = nPad;
  upperExtendRegion[2] = nPad;
  
  PadFilterType::Pointer padFilter = PadFilterType::New();
  
  padFilter->SetPadLowerBound(lowerExtendRegion);
  padFilter->SetPadUpperBound(upperExtendRegion);
  padFilter->SetInput(reader->GetOutput());
  std::cout << "Extending volume boundary ...\n";
  padFilter->Update();

  unsigned int radius = 3;
  /*
  StructuringElementType structuringElement;
  structuringElement.SetRadius(radius);
  structuringElement.CreateStructuringElement();*/
 
  MaskWriterType::Pointer writerMask = MaskWriterType::New();
  ImageWriterType::Pointer writerImage = ImageWriterType::New();
  vtkNew<vtkPolyDataWriter> polydataWriter;

  BinaryOpeningFilterType::Pointer openingFilter = BinaryOpeningFilterType::New( );
  openingFilter->SetForegroundValue( 1 );
  openingFilter->SetBackgroundValue( 0 );
  openingFilter->SetRadius( 2 );
  openingFilter->SetInput( padFilter->GetOutput( ) );
  std::cout << "Testing binary opening on mask ...\n";
  openingFilter->Update();
#define WRITE_INTERMEDIATE_RESULTS
#if defined(WRITE_INTERMEDIATE_RESULTS)
  writerMask->SetInput( openingFilter->GetOutput( ) );
  writerMask->SetFileName( "/tmp/binary_opening.vtk" );
  writerMask->Update();
#endif
  
  unsigned int radiusFill = 1;
  itk::MaskImage::SizeType radiusFillArray;
  radiusFillArray[0] = radiusFillArray[1] = radiusFillArray[2] = radiusFill;
  unsigned int maxiterFill = 5;
  HoleFillingFilterType::Pointer fillholeFilter = HoleFillingFilterType::New();
  fillholeFilter->SetRadius(radiusFillArray);
  fillholeFilter->SetBackgroundValue(0);
  fillholeFilter->SetForegroundValue(1);
  fillholeFilter->SetMajorityThreshold(1);
  fillholeFilter->SetMaximumNumberOfIterations( maxiterFill );
  fillholeFilter->SetInput( openingFilter->GetOutput( ) );
  std::cout << "Applying hole-filling to mask ...\n";
  fillholeFilter->Update();

#if defined(WRITE_INTERMEDIATE_RESULTS)
  writerMask->SetInput(fillholeFilter->GetOutput());
  writerMask->SetFileName("/tmp/fill-hole.vtk");
  writerMask->Update();
#endif

  unsigned int numberOfLayers = 6;
  LevelSetFilterType::Pointer levelsetFilter = LevelSetFilterType::New();
  levelsetFilter->SetInput(fillholeFilter->GetOutput());
  levelsetFilter->SetNumberOfLayers(numberOfLayers);
  levelsetFilter->UseImageSpacingOn();
  levelsetFilter->SetMaximumRMSError(0.007);
  std::cout << "Applying antialias to binary mask ...\n";
  levelsetFilter->Update();

#if defined(WRITE_INTERMEDIATE_RESULTS)
  writerImage->SetInput(levelsetFilter->GetOutput());
  writerImage->SetFileName("/tmp/levelset.vtk");
  writerImage->Update();
#endif

  ImageToVTKImageFilterType::Pointer connectVTK = ImageToVTKImageFilterType::New( );
  connectVTK->SetInput( levelsetFilter->GetOutput( ) );
  connectVTK->Update( );
  vtkNew<vtkImageMarchingCubes> marchingCubes;
  marchingCubes->SetInputData( connectVTK->GetOutput( ) );
  marchingCubes->SetNumberOfContours( 1 );
  marchingCubes->SetValue(0, -0.5 );
  marchingCubes->ComputeScalarsOff( );
  marchingCubes->ComputeNormalsOff( );
  marchingCubes->ComputeGradientsOff( );
  std::cout << "Applying marching cube to level-set ...\n";
  marchingCubes->Update();

#if defined(WRITE_INTERMEDIATE_RESULTS)
  polydataWriter->SetInputConnection( marchingCubes->GetOutputPort( ) );
  polydataWriter->SetFileName( "/tmp/marching-cubes.vtk" );
  polydataWriter->Update( );
#endif

  TaubinSmoother taubinSmoother;
  vtkMeshVisitor vtkVisitor;
  vtkVisitor.InitVisitor( marchingCubes->GetOutput( ) );
  taubinSmoother.build_external(&vtkMeshVisitor::NextNode, &vtkMeshVisitor::NextTriangle, &vtkVisitor);
  float taubin1Steps = 20;
  float taubin1Lambda = 0.8;
  float taubin1Mu = -0.3;
  TaubinSmoother::method taubin1Method = TaubinSmoother::laplace;

  taubinSmoother.taubin_smooth(taubin1Method, taubin1Lambda, taubin1Mu, taubin1Steps);
  

  vtkNew< vtkPolyData > smoothMesh1;
  smoothMesh1->ShallowCopy( marchingCubes->GetOutput( ) );
  for( size_t i = 0; i < smoothMesh1->GetNumberOfPoints(); i++)
    {
    smoothMesh1->GetPoints()->SetPoint(i, 
                                       taubinSmoother.vertices[i].x, 
                                       taubinSmoother.vertices[i].y,
                                       taubinSmoother.vertices[i].z);
    }
	
#if defined(WRITE_INTERMEDIATE_RESULTS)
  polydataWriter->SetInputData( smoothMesh1.GetPointer() );
  polydataWriter->SetFileName( "/tmp/taubin_phase1.vtk" );
  polydataWriter->Update( );
#endif

  vtkNew< vtkQuadricDecimation > decimationFilter;
  decimationFilter->SetInputData( smoothMesh1.GetPointer( ) );
  decimationFilter->SetTargetReduction( 0.5 );
  std::cout << "Applying quadric-decimation to smoothed mesh ...\n";
  decimationFilter->Update( );

#if defined(WRITE_INTERMEDIATE_RESULTS)
  polydataWriter->SetInputConnection( decimationFilter->GetOutputPort( ) );
  polydataWriter->SetFileName( "/tmp/quadric_decimation.vtk" );
  polydataWriter->Update( );
#endif

  vtkVisitor.InitVisitor( decimationFilter->GetOutput( ) );
  taubinSmoother.clear();
  taubinSmoother.build_external( &vtkMeshVisitor::NextNode, &vtkMeshVisitor::NextTriangle, &vtkVisitor );
  float taubin2Steps = 10;
  float taubin2Lambda = 0.6;
  float taubin2Mu = -0.1;
  TaubinSmoother::method taubin2Method = TaubinSmoother::laplace;

  taubinSmoother.taubin_smooth(taubin2Method, taubin2Lambda, taubin2Mu, taubin2Steps);

  vtkNew< vtkPolyData > smoothMesh2;
  smoothMesh2->ShallowCopy( decimationFilter->GetOutput( ) );
  for( size_t i = 0; i < smoothMesh2->GetNumberOfPoints(); i++)
    {
    smoothMesh2->GetPoints()->SetPoint(i, 
                                       taubinSmoother.vertices[i].x, 
                                       taubinSmoother.vertices[i].y,
                                       taubinSmoother.vertices[i].z);
    }
  polydataWriter->SetInputData( smoothMesh2.GetPointer() );
  polydataWriter->SetFileName( argv[2] );
  polydataWriter->Update( );

  return 0;

}


