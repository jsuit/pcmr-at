#include <iostream>
#include "pcmrFlow4DReader.h"
#include "gtest/gtest.h"
#include "itkVTKImageToImageFilter.h"
#include "itkSignedMaurerDistanceMapImageFilter.h"
#include "pcmrImageUtil.h"
#include "itkLevelSetNeighborhoodExtractor.h"
#include "itkComposeImageFilter.h"
#include "itkMaskImageFilter.h"
#include "itkImageDuplicator.h"
#include "itkStructHashFunction.h"
#include "itksys/hash_map.hxx"

static int _ARGC = 0;
static char **_ARGV = NULL;

static const char* StudyDir = NULL;
static pcmr::Flow4DReader::Pointer dataSet = NULL;

TEST(TestLevelSetNeigborhoodExtractor, TestCommandLineArgs)
{
  ASSERT_TRUE(_ARGV!=NULL);
  ASSERT_GE(_ARGC,1) << "usage: '" << _ARGV[0] << "' study\n";
  StudyDir = _ARGV[1];
}

TEST(TestLevelSetNeigborhoodExtractor, TestLoadDataSet)
{
  ASSERT_TRUE(StudyDir!=NULL);
  dataSet = pcmr::Flow4DReader::New();

  pcmr::StatusType status = dataSet->SetDirectory(StudyDir);
  if (status != pcmr::OK)
    {
    dataSet = NULL;
    }
  ASSERT_EQ(pcmr::OK, status) << "SetDirectory: " << pcmr::GetStatusDescription(status) << "\n";
}

struct CumulativeAverageType {
  itk::PhaseContrastVector3DImage::PixelType v;
  size_t count;
  CumulativeAverageType()
  {
    v[0] = v[1] = v[2] = 0;
    count = 0;
  }

  CumulativeAverageType(const CumulativeAverageType& a)
  {
    this->v = a.v;
    this->count = a.count;
  }
  
  CumulativeAverageType& operator=(const CumulativeAverageType& a)
  {
    this->v = a.v;
    this->count = a.count;
    return *this;
  }

  CumulativeAverageType& operator+=(const itk::PhaseContrastVector3DImage::PixelType & v)
  {
    size_t n = this->count++;
    double a = 1/this->count;
    double b = n*a;
    for(int i = 0; i < 3; i++)
      {
      this->v[i] *= b;
      this->v[i] += a*v[i];
      }
    return *this;
  }
};

typedef itksys::hash_map<itk::PhaseContrast3DImage::IndexType,CumulativeAverageType,itk::StructHashFunction<itk::PhaseContrast3DImage::IndexType> > HashIndexType;

void NearWallInterpolate(const itk::PhaseContrastVector3DImage::PixelType &v0,
                         double d0, double d1,
                         itk::PhaseContrastVector3DImage::PixelType &v1)
{
  double rd = d1/d0;
  for(int i = 0; i < 3; i++)
    {
    v1[i] = v0[i]*rd;
    }
}

void PropagateLevel(itk::MaskImage *levelImage, 
                    itk::PhaseContrast3DImage *distanceImage, 
                    HashIndexType *currentLevel, 
                    HashIndexType *nextLevel,
                    itk::MaskImage::PixelType nextLabel)
{
  itk::MaskImage::SizeType nbrRadius = {{1,1,1}};
  itk::ConstNeighborhoodIterator<itk::MaskImage> 
    levelIter(nbrRadius, levelImage, levelImage->GetLargestPossibleRegion());
  const itk::MaskImage::SizeValueType n = levelIter.Size();
  const itk::MaskImage::SizeValueType c = n>>1;
  bool isInBound;
  // propagate from level 2 to level 3
  for(HashIndexType::iterator it = currentLevel->begin();
      it != currentLevel->end(); ++it)
    {
    levelIter.SetLocation(it->first);
    if (it->second.count == 0) continue;
    for (int i = 0; i < n; i++)
      {
      if (i == c) continue;
      itk::MaskImage::PixelType l = levelIter.GetPixel(i, isInBound);
      if (isInBound && l == 0)
        {
        itk::MaskImage::IndexType idx = levelIter.GetIndex(i);
        itk::PhaseContrastVector3DImage::PixelType v;
        NearWallInterpolate(it->second.v,
                            distanceImage->GetPixel(it->first),
                            distanceImage->GetPixel(idx),
                            v);
        //(*nextLevel)[idx] += it->second.v;
        (*nextLevel)[idx] += v;
        levelImage->SetPixel(idx, nextLabel);
        }
      }
    }
}

TEST(TestLevelSetNeigborhoodExtractor, TestComputeDistance)
{
  typedef itk::ImageDuplicator<itk::MaskImage> MaskDuplicatorType;
  typedef itk::VTKImageToImageFilter<itk::PhaseContrast3DImage> ImporterType;
  typedef itk::VTKImageToImageFilter<itk::MaskImage> MaskImporterType;
  ImporterType::Pointer importerX = ImporterType::New();
  ImporterType::Pointer importerY = ImporterType::New();
  ImporterType::Pointer importerZ = ImporterType::New();
  MaskImporterType::Pointer importerMask = MaskImporterType::New();
  vtkImageData *vtkImgX = dataSet->GetFlowImage(pcmr::Flow4DReader::FlowX,
                                                0);
  ASSERT_TRUE(vtkImgX!=NULL);
  vtkImageData *vtkImgY = dataSet->GetFlowImage(pcmr::Flow4DReader::FlowY,
                                                0);
  ASSERT_TRUE(vtkImgY!=NULL);
  vtkImageData *vtkImgZ = dataSet->GetFlowImage(pcmr::Flow4DReader::FlowZ,
                                                0);
  ASSERT_TRUE(vtkImgZ!=NULL);
  vtkImageData *vtkMask = dataSet->GetMaskImage(0);
  ASSERT_TRUE(vtkMask!=NULL);

  importerX->SetInput(vtkImgX);
  importerY->SetInput(vtkImgY);
  importerZ->SetInput(vtkImgZ);
  importerMask->SetInput(vtkMask);

  typedef itk::ComposeImageFilter<itk::PhaseContrast3DImage,itk::PhaseContrastVector3DImage> ComposeFilter;
  ComposeFilter::Pointer composer = ComposeFilter::New();
  composer->SetInput(0, importerX->GetOutput());
  composer->SetInput(1, importerY->GetOutput());
  composer->SetInput(2, importerZ->GetOutput());
  composer->Update();

  typedef itk::MaskImageFilter<itk::PhaseContrastVector3DImage,itk::MaskImage> MaskFlowFilterType;
  MaskFlowFilterType::Pointer maskerFlow = MaskFlowFilterType::New();
  maskerFlow->SetInput(composer->GetOutput());
  maskerFlow->SetMaskImage(importerMask->GetOutput());
  maskerFlow->Update();
  pcmr::WriteImage(maskerFlow->GetOutput(), "MaskedFlowOriginal.vtk");

  typedef itk::SignedMaurerDistanceMapImageFilter<itk::MaskImage,itk::PhaseContrast3DImage> DistanceFilterType;
  DistanceFilterType::Pointer distanceFilter = DistanceFilterType::New();
  distanceFilter->SetInput(importerMask->GetOutput());
  distanceFilter->SquaredDistanceOff();
  distanceFilter->UseImageSpacingOn();
 
  pcmr::WriteImage(distanceFilter->GetOutput(), "DistanceFieldOriginal.vtk");

  typedef itk::LevelSetNeighborhoodExtractor<itk::PhaseContrast3DImage> LevelSetExtractorType;
  typedef LevelSetExtractorType::NodeContainer NodeContainerType;
  LevelSetExtractorType::Pointer extractor = LevelSetExtractorType::New();
  extractor->SetInputLevelSet(distanceFilter->GetOutput());
  extractor->SetLevelSetValue(0);
  extractor->Locate();
  //extractor->Print(std::cout);
  NodeContainerType::Pointer outside = extractor->GetOutsidePoints();
  NodeContainerType::Pointer levelSetInside = extractor->GetInsidePoints();
  std::cout << "Outside boundary has " << outside->size() << " nodes\n";
  std::cout << "Inside boundary has " << levelSetInside->size() << " nodes\n";
  
  HashIndexType hashVoxel0;
  HashIndexType hashVoxel1;
  HashIndexType *currentLevel = &hashVoxel0;
  HashIndexType *nextLevel = &hashVoxel1;
  HashIndexType *hashTmp;

  //distanceFilter->GetOutput()->Print(std::cout);
  itk::PhaseContrast3DImage::Pointer boundaryImage = itk::PhaseContrast3DImage::New();
  boundaryImage->CopyInformation(distanceFilter->GetOutput());
  boundaryImage->SetRegions(distanceFilter->GetOutput()->GetLargestPossibleRegion());
  boundaryImage->Allocate();

  //insideImage->Print(std::cout);

  itk::ImageRegionIterator<itk::PhaseContrast3DImage> outputIterator(boundaryImage, boundaryImage->GetLargestPossibleRegion());
  while(!outputIterator.IsAtEnd())
    {
    outputIterator.Set(0);
    ++outputIterator;
    }
  NodeContainerType::iterator nodeIt;
  size_t countMask = 0;
  size_t countNegative = 0;
  itk::MaskImage::SizeType nbrRadius = {{1,1,1}};
  typedef itk::ConstNeighborhoodIterator<itk::MaskImage>::SizeValueType SizeValueType;
  typedef itk::ConstNeighborhoodIterator<itk::MaskImage>::IndexType IndexType;
  typedef itk::ConstNeighborhoodIterator<itk::MaskImage>::OffsetType OffsetType;
  /*
  itk::NeighborhoodIterator<itk::PhaseContrastVector3DImage> 
    velIter(nbrRadius, composer->GetOutput(), 
             composer->GetOutput()->GetLargestPossibleRegion());
  */
  itk::PhaseContrastVector3DImage::PixelType nullV;
  nullV[0] = nullV[1] = nullV[2] = 0.0;
  MaskDuplicatorType::Pointer dupMask = MaskDuplicatorType::New();
  dupMask->SetInputImage(importerMask->GetOutput());
  dupMask->Update();
  itk::MaskImage::Pointer levelImage = dupMask->GetModifiableOutput();
  itk::ConstNeighborhoodIterator<itk::MaskImage> 
    levelIter(nbrRadius, levelImage, levelImage->GetLargestPossibleRegion());
  const SizeValueType n = levelIter.Size();
  const SizeValueType n_1 = n - 1;
  const SizeValueType c = n>>1;
  bool tmpFlag = true;
  for(nodeIt = levelSetInside->begin();
      nodeIt != levelSetInside->end(); 
      ++nodeIt)
    {
    itk::PhaseContrast3DImage::IndexType index = nodeIt->GetIndex();
    double v = distanceFilter->GetOutput()->GetPixel(index);
    if (v < 0)
      {
      ++countNegative;
      }
    if (levelImage->GetPixel(index))
      {
      ++countMask;
      }
    boundaryImage->SetPixel(index, 1);
    // masker->GetOutput()->SetPixel(index, nullV); 
    // now fix velocity near the wall from outside
    levelIter.SetLocation(index);
    //velIter.SetLocation(index);
    bool isInBound;
    for (SizeValueType i = 0; i < c; i++)
      {
      if (i == c) continue;
      IndexType idx1 = levelIter.GetIndex(i);
      // idx2 is the opposite of idx1 wrt the center voxel
      IndexType idx2 = levelIter.GetIndex(n_1-i);
      if (tmpFlag)
        {
        const int nd = i > 0 ? (int) log10 ((double) i) + 1 : 1;
        std::cout << i << " : " << idx1 - index << std::endl
                  << std::string(nd+3, ' ') << idx2 - index <<std::endl;
        }
      int m1 = levelIter.GetPixel(i, isInBound);
      if (!isInBound) continue;
      
      if (!m1)
        {
        // idx is outside
        // pixel idx1 could be updated from value of idx2
        
        // initialize counter of idx1 if it is the first time that we
        // visit idx1
        if (currentLevel->count(idx1) == 0)
          {
          (*currentLevel)[idx1] = CumulativeAverageType();
          }

        int m2 = levelIter.GetPixel(n_1-i, isInBound);
        if (!isInBound) continue;
        double d2 = distanceFilter->GetOutput()->GetPixel(idx2);
        if (m2 && d2 < 0.0)
          {
          // idx2 is inside
          // idx1 can be updated from idx2
          // ++hashContour[idx1];
          itk::PhaseContrastVector3DImage::PixelType v;
          NearWallInterpolate(composer->GetOutput()->GetPixel(idx2),
                              d2, distanceFilter->GetOutput()->GetPixel(idx1),
                              v);
          (*currentLevel)[idx1] += v;
          // mark this voxel as visited at level 2
          levelImage->SetPixel(idx1, 2);
          }
        }
      else
        {
        int m2 = levelIter.GetPixel(n_1-i, isInBound);
        if (!isInBound) continue;
        if (!m2)
          {
          // pixel idx2 is outside
          // pixel idx2 could be updated from value of idx1
          
          // initialize counter of idx2 if it is the first time that we
          // visit idx2
          if (currentLevel->count(idx2) == 0)
            {
            (*currentLevel)[idx2] = CumulativeAverageType();
            }
          double d1 = distanceFilter->GetOutput()->GetPixel(idx1);
          if (d1 < 0) // idx1 is insize
            {
            // idx2 can be updated from idx1
            // ++hashContour[idx2];
            itk::PhaseContrastVector3DImage::PixelType v;
            NearWallInterpolate(composer->GetOutput()->GetPixel(idx1),
                                d1, distanceFilter->GetOutput()->GetPixel(idx2),
                                v);
            (*currentLevel)[idx2] += v;
            // mark this voxel as visited at level 2
            levelImage->SetPixel(idx2, 2);
            }
          }
        }
      }
    tmpFlag = false;
    }

#if 0
  // process the voxels with count == 0
  for(HashIndexType::iterator it = currentLevel->begin();
      it != currentLevel->end(); ++it)
    {
    if (it->second.count == 0)
      {
      levelIter.SetLocation(it->first);
      for (int i = 0; i < n; i++)
        {
        if (i == c) continue;
        IndexType idx = levelIter.GetIndex(i);
        if (currentLevel->count(idx)>0)
          {
          // idx is an interpolated voxel, average it into it==current voxel
          it->second += composer->GetOutput()->GetPixel(idx);
          }
        }
      if (it->second.count > 0)
        {
        levelImage->SetPixel(it->first, 2);
        }
      else
        {
        std::cerr << "could not correct voxel " << it->first << std::endl;
        }
      }
    }
#endif

  // propagate from level 2 to level 3
  for(HashIndexType::iterator it = currentLevel->begin();
      it != currentLevel->end(); ++it)
    {
    levelIter.SetLocation(it->first);
    // this voxel does not have a valid velocity
    if (it->second.count == 0) continue;
    bool isInBound;
    for (int i = 0; i < n; i++)
      {
      if (i == c) continue;
      int l = levelIter.GetPixel(i, isInBound);
      if (isInBound && l == 0)
        {
        IndexType idx = levelIter.GetIndex(i);
        itk::PhaseContrastVector3DImage::PixelType v;
        NearWallInterpolate(it->second.v,
                            distanceFilter->GetOutput()->GetPixel(it->first),
                            distanceFilter->GetOutput()->GetPixel(idx),
                            v);
        //(*nextLevel)[idx] += it->second.v;
        (*nextLevel)[idx] += v;
        levelImage->SetPixel(idx, 3);
        }
      }
    }
  
  std::cout << "Hash_Map reported " << currentLevel->size() << " items\n";
  size_t minUpdater = 28;
  size_t minCounter = 0;
  size_t maxUpdater = 0;
  size_t zeroCounter = 0;

  // set the interpolated value into the output velocity image
  // first currentLevel
  for(HashIndexType::iterator it = currentLevel->begin();
      it != currentLevel->end(); ++it)
    {
    maskerFlow->GetOutput()->SetPixel(it->first, it->second.v);
    if (it->second.count == 1)
      {
      ++minCounter;
      }
    boundaryImage->SetPixel(it->first, it->second.count+2);
    if (it->second.count == 0)
      {
      ++zeroCounter;
      }
    else
      {
      if (minUpdater > it->second.count)
        {
        minUpdater = it->second.count;
        }
      else
        if (maxUpdater < it->second.count)
          {
          maxUpdater = it->second.count;
          }
      }
    }
  // then next level
  for(HashIndexType::iterator it = nextLevel->begin();
      it != nextLevel->end(); ++it)
    {
    maskerFlow->GetOutput()->SetPixel(it->first, it->second.v);
    }

  hashTmp = currentLevel;
  currentLevel = nextLevel;
  nextLevel = hashTmp;
  nextLevel->clear();
  PropagateLevel(levelImage, distanceFilter->GetOutput(), 
                 currentLevel, nextLevel, 4);
  for(HashIndexType::iterator it = nextLevel->begin();
      it != nextLevel->end(); ++it)
    {
    maskerFlow->GetOutput()->SetPixel(it->first, it->second.v);
    }
  
  pcmr::WriteImage(maskerFlow->GetOutput(), "FixedMaskedFlowOriginal.vtk");
  std::cout << "zeroCounter = " << zeroCounter << std::endl;
  std::cout << "minCounter = " << minCounter << std::endl;
  std::cout << "minUpdater = " << minUpdater << std::endl;
  std::cout << "maxUpdater = " << maxUpdater << std::endl;
  std::cout << "There are " << countMask << " points inside the mask\n";
  std::cout << "There are " << countNegative << " interior points\n";
  pcmr::WriteImage(boundaryImage.GetPointer(), "WallNbrImage.vtk");
  pcmr::WriteImage(levelImage.GetPointer(), "LevelImage.vtk");
  
}

int main(int argc, char **argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  _ARGC = argc;
  _ARGV = argv;
  return RUN_ALL_TESTS();
}
