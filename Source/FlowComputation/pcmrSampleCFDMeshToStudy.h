#ifndef __pcmrSampleUGridToStudy_h
#define __pcmrSampleUGridToStudy_h

#include "pcmrTypes.h"
#include "pcmrFlowComputation_Export.h"

class vtkUnstructuredGrid;

BEGIN_PCMR_DECLS

PCMRFLOWCOMPUTATION_EXPORT
StatusType SampleCFDMeshToStudy( vtkUnstructuredGrid *CFDMesh,
				 const char *PrefixPath,
				 double Spacing[3], 
				 unsigned int ExtraPad, 
                                 unsigned int SamplePerCell,
                                 double velocityFactor );
END_PCMR_DECLS

#endif
