#include "pcmrWallShearRateImage.h"
#include "pcmrWallShearRateImageFilter.h"
#include "pcmrImageUtil.h"

BEGIN_PCMR_DECLS

StatusType ComputeWallShearRateImage(Flow4DReader *dataSet, size_t timeStep,
                                     itk::PhaseContrast3DImage::Pointer &wsrImage)
{
  WallShearRateImageFilter::Pointer wsrFilter = WallShearRateImageFilter::New();
  StatusType status = wsrFilter->SetInputFromStudy(dataSet, 3);
  if (status != OK)
    {
    return status;
    }
  wsrFilter->Update();
  wsrImage = wsrFilter->GetOutput();
  return OK;
  
  return OK;
}

END_PCMR_DECLS
