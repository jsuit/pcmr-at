#ifndef __pcmrWallShearRateImage_h
#define __pcmrWallShearRateImage_h

#include "pcmrFlowComputation_Export.h"
#include "pcmrFlow4DReader.h"

BEGIN_PCMR_DECLS

PCMRFLOWCOMPUTATION_EXPORT
StatusType ComputeWallShearRateImage(Flow4DReader *dataSet, size_t timeStep,
                                     itk::PhaseContrast3DImage::Pointer &wsrImage);

END_PCMR_DECLS

#endif
