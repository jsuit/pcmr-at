#ifndef __pcmrFlowQuantifyContour_h
#define __pcmrFlowQuantifyContour_h

#include "pcmrTypes.h"
#include "vtkPolyData.h"
#include "vtkParametricSpline.h"
#include "pcmrFlowComputation_Export.h"
#include <map>

BEGIN_PCMR_DECLS

PCMRFLOWCOMPUTATION_EXPORT
int FlowQuantifyVtkContour(vtkPolyData *planeInterpolated,
                           vtkParametricSpline *splineContour,
                           double normal[],
                           int contourResolution,
                           std::map<std::string,float> &table);
END_PCMR_DECLS

#endif
