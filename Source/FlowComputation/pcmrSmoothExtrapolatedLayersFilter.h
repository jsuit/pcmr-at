#ifndef __pcmrSmoothExtrapolatedLayersFilter_h
#define __pcmrSmoothExtrapolatedLayersFilter_h

#include "pcmrDefs.h"
#include "itkSmoothingRecursiveGaussianImageFilter.h"
#include "itkBinaryThresholdImageFilter.h"
#include "itkMaskImageFilter.h"
#include "itkAddImageFilter.h"
#include "itkPhaseContrastImage.h"

BEGIN_PCMR_DECLS

template <class TInputImage, class TMaskImage>
class SmoothExtrapolatedLayersFilter :
  public itk::ImageToImageFilter<TInputImage, TInputImage>
{
public:
  typedef  SmoothExtrapolatedLayersFilter              Self;
  typedef itk::ImageToImageFilter<TInputImage,TInputImage> Superclass;
  typedef itk::SmartPointer<Self>                        Pointer;
  typedef itk::SmartPointer<const Self>                  ConstPointer;

  /** Method for creation through object factory */
  itkNewMacro(Self);

  /** Run-time type information */
  itkTypeMacro(SmoothExtrapolatedLayersFilter, itk::ImageToImageFilter);

  /** Display */
  void PrintSelf( std::ostream& os, itk::Indent indent ) const;
  void SetLayerMaskImage( const itk::MaskImage * maskLayer );
  const itk::MaskImage * GetLayerMaskImage() const;
 
protected:

  SmoothExtrapolatedLayersFilter();

  typedef itk::SmoothingRecursiveGaussianImageFilter<TInputImage, TInputImage> SmoothingFilterType;
  typedef typename SmoothingFilterType::SigmaArrayType SigmaArrayType;
  typedef itk::BinaryThresholdImageFilter<TMaskImage,TMaskImage> ThresholdImageType;
  typedef itk::MaskImageFilter<TInputImage,TMaskImage> MaskFilterType;
  typedef itk::AddImageFilter<TInputImage> AddFilterType;

  void GenerateData();

private:

  SmoothExtrapolatedLayersFilter(Self&);   // intentionally not implemented
  void operator=(const Self&);          // intentionally not implemented

  typename SmoothingFilterType::Pointer  m_GaussianSmoother;
  typename ThresholdImageType::Pointer   m_ThresholdInside;
  typename ThresholdImageType::Pointer   m_ThresholdOutside;
  typename MaskFilterType::Pointer       m_MaskInside;
  typename MaskFilterType::Pointer       m_MaskOutside;
  typename AddFilterType::Pointer        m_AddImages;
};

END_PCMR_DECLS

#ifndef ITK_MANUAL_INSTANTIATION
#include "pcmrSmoothExtrapolatedLayersFilter.txx"
#endif

#endif


