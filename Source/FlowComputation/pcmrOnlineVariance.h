#ifndef __pcmrOnlineVariance_h
#define __pcmrOnlineVariance_h

#include "pcmrDefs.h"

BEGIN_PCMR_DECLS

class OnlineVariance
{
public:
  OnlineVariance()
  {
    this->Reset();
  }

  ~OnlineVariance()
  {
  }

  void Reset()
  {
    this->m_Size = 0;
    this->m_Mean = 0.0;
    this->m_M2 = 0.0;
  }
  
  size_t GetSize()
  {
    return this->m_Size;
  }

  double GetMean()
  {
    return this->m_Mean;
  }

  double GetVariance()
  {
    return this->m_M2/(this->GetSize() - 1);
  }

  void AddNewData(double x)
  {
    ++this->m_Size;
    double delta = x - this->m_Mean;
    this->m_Mean += delta / this->m_Size;
    this->m_M2 += delta * (x - this->m_Mean);
  }

private:
  double m_Mean;
  double m_M2;
  size_t m_Size;
};

END_PCMR_DECLS

#endif

