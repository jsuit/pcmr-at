#ifndef __pcmrWallShearRateFunction2_h
#define __pcmrWallShearRateFunction2_h

#include "pcmrDefs.h"
#include "itkPhaseContrastImage.h"
#include "pcmrSmoothExtrapolatedLayersFilter.h"
#include "pcmrFlow4DReader.h"
#include "itkObject.h"
#include "itkObjectFactory.h"
#include "itkTimeStamp.h"
#include "itkPolyLineParametricPath.h"
#include "pcmrFlowComputation_Export.h"
#include "pcmrCurveFit.h"

class vtkPolyData;

#define SIGNEDMAURER 0
#define POLYMESH     1
#define ANTIALIAS    2

//#define __PCMR_DISTANCE_FILTER__   SIGNEDMAURER
#define __PCMR_DISTANCE_FILTER__   POLYMESH
//#define __PCMR_DISTANCE_FILTER__   ANTIALIAS

#define __USE_SIGNED_MAURER_FILTER__

#if (__PCMR_DISTANCE_FILTER__==SIGNEDMAURER)
//#ifdef __USE_SIGNED_MAURER_FILTER__
#include "itkSignedMaurerDistanceMapImageFilter.h"
#elif (__PCMR_DISTANCE_FILTER__==ANTIALIAS)
#include "itkAntiAliasBinaryImageFilter.h"
#elif (__PCMR_DISTANCE_FILTER__==POLYMESH)
#include "pcmrPolyMeshToSignedDistanceImageFilter.h"
#endif

#include "itkGradientRecursiveGaussianImageFilter.h"
#include "itkBSplineInterpolateImageFunction.h"
#include "itkLinearInterpolateImageFunction.h"

BEGIN_PCMR_DECLS

class PCMRFLOWCOMPUTATION_EXPORT WallShearRateFunction2 : public itk::Object
{
 public:
  typedef itk::LinearInterpolateImageFunction<itk::PhaseContrast3DImage> LinearInterpolateFunctionType;
  typedef itk::GradientRecursiveGaussianImageFilter<itk::PhaseContrast3DImage,itk::Covariant3DImage> GradientFilterType;
  typedef itk::LinearInterpolateImageFunction<itk::Covariant3DImage> LinearInterpolateGradientFunctionType;
  typedef itk::BSplineInterpolateImageFunction<itk::PhaseContrast3DImage> BSplineFunctionType;
  typedef pcmr::SmoothExtrapolatedLayersFilter<itk::PhaseContrast3DImage,itk::MaskImage> SmoothFilterType;
  typedef BSplineFunctionType::CoordRepType CoordRepType;
  typedef BSplineFunctionType::PointType PointType;
  typedef BSplineFunctionType::IndexType IndexType;
  typedef BSplineFunctionType::ContinuousIndexType ContinuousIndexType;
  typedef itk::PhaseContrast3DImage::SizeType SizeType;
  typedef itk::PhaseContrast3DImage::SpacingType SpacingType;
  // see itkMatrix.h
  typedef BSplineFunctionType::CovariantVectorType GradientType;
  typedef BSplineFunctionType::OutputType OutputType;

  /** Standard class typedefs. */
  typedef WallShearRateFunction2        Self;
  typedef itk::Object                    Superclass;
  typedef itk::SmartPointer<Self>        Pointer;
  typedef itk::SmartPointer<const Self>  ConstPointer;

  /** Method for creation through the object factory. */
  itkNewMacro(Self);
 
  /** Run-time type information (and related methods). */
  itkTypeMacro(WallShearRateFunction2, itk::Object);

  typedef enum {BLOCATOR_LEVELSET, BLOCATOR_BINARYMASK} BoundaryLocatorType;

  WallShearRateFunction2();
  virtual ~WallShearRateFunction2();

  // VelocityUnitFactor
  void SetVelocityUnitFactor(double vFactor)
  {
    if (vFactor < 0)
      {
      return;
      }
    this->m_VelocityUnitFactor = vFactor;
  }

  double GetVelocityUnitFactor()
  {
    return this->m_VelocityUnitFactor;
  }

  // SetViscocity
  void SetViscocity(double viscocity)
  {
    if (viscocity < 0)
      {
        return;
      }
    this->m_Viscocity = viscocity;
  }

  double GetViscocity()
  {
    return this->m_Viscocity;
  }

  // SetImageDataFromStudy
  StatusType SetImageDataFromStudy(Flow4DReader *reader, size_t timeStep);

  // SetComponentXImage
  void SetComponentXImage(itk::PhaseContrast3DImage *grid);

  // SetComponentYImage
  void SetComponentYImage(itk::PhaseContrast3DImage *grid);

  // SetComponentZImage
  void SetComponentZImage(itk::PhaseContrast3DImage *grid);

  // SetMaskImage
  void SetMaskImage(itk::MaskImage *grid);

  itk::MaskImage *GetMaskImage() const
    {
      return this->m_ImageMask;
    }

  BoundaryLocatorType GetBoundaryLocator() const
  {
    return this->m_BoundaryLocator;
  }
  
  void SetBoundaryLocator(BoundaryLocatorType m)
  {
    this->m_BoundaryLocator = m;
  }

  StatusType SetBoundaryMesh(vtkPolyData *mesh);

  void SetSplineOrder(unsigned int n)
  {
    for (int i = 0; i < 4; i++)
      {
        this->m_BSplineFunction[i]->SetSplineOrder(n);
      }
  }

  unsigned int GetSplineOrder()
  {
    return this->m_BSplineFunction[X]->GetSplineOrder();
  }

  // Evaluate
  void EvaluateAtPhysicalPoint(const PointType &point, double wsr[]);
  void EvaluateAtPhysicalPointWithNormal(const PointType &point, const double normal[], double wsr[]);
  double EvaluateAtPhysicalPoint(const PointType &point);
  double EvaluateAtPhysicalPointWithNormal(const PointType &point, const double normal[]);
  void EvaluateAtContinuousIndex(const ContinuousIndexType &index, const double normal[], double wsr[]);
  double EvaluateAtContinuousIndex(const ContinuousIndexType &index, const double normal[]);

  void EvaluateVorticityAtPhysicalPoint( const PointType &point, double vorticity[] );
  void EvaluateVorticityAtContinuousIndex( const ContinuousIndexType &index,
					   double vorticity[] );

  void EvaluateNormalFromDistanceMap(const PointType &point, double normal[]);
  void EvaluateNormalFromDistanceMap(const ContinuousIndexType &index, double normal[]);
  bool EvaluateUnitNormalFromDistanceMap(const PointType &point, double normal[]);

  int EvaluateNormalInward(const IndexType &index, PointType::VectorType &InwardNormal);

  int EvaluateGradientAtPhysicalPoint(const PointType &point, 
                                      OutputType value[],
                                      GradientType gradient[]);

  void SetCorrectedFlowFileName(const std::string & fileName)
  {
    this->m_CorrectedFlowFileName = fileName;
  }

  void SetLevelImageFileName(const std::string & fileName)
  {
    this->m_LevelImageFileName = fileName;
  }

  const std::string & GetCorrectedFlowFileName() const
  {
    return this->m_CorrectedFlowFileName;
  }

  bool GetComputeWallCondition(void) const
  {
    return this->m_ComputeWallCondition;
  }
  
  void SetComputeWallCondition(bool c)
  {
    this->m_ComputeWallCondition = c;
  }

#if (__PCMR_DISTANCE_FILTER__==ANTIALIAS)
  void SetMaximumRMSError(double x)
  {
    this->m_DistanceFilter->SetMaximumRMSError(x);
  }
  
  double GetMaximumRMSError()
  {
    return this->m_DistanceFilter->GetMaximumRMSError();
  }
#endif

 protected:
  typedef std::vector<itk::PhaseContrast3DImage::IndexType> VectorIndexType;

  StatusType ComputeMaskBoundary(VectorIndexType &arrayInside, 
                                 VectorIndexType &arrayOutside);
  StatusType CreateRasterMaskFromBoundaryMesh();
  void ComputeSegmentAtNormalDirection(const IndexType &indexStart, int steps, 
				       IndexType &indexEnd,
				       const PointType::VectorType &normalInward);
  float GetDistanceAtContinuousIndex( const ContinuousIndexType &index );
  int ExtrapolateVelocityAtVoxel_Bresenham(const IndexType &index, int steps,
					   float v[], PointType::VectorType &NormalInward);

  int FindExtremeVertex( const ContinuousIndexType &index,
			 const PointType::VectorType &normal,
			 ContinuousIndexType &index1,
			 PointType::VectorType &ciNormal );

  typedef itk::PolyLineParametricPath<3> ParametricPathType;

  int BuildParametricRay( const ContinuousIndexType &index,
			  const PointType::VectorType &normal,
			  ParametricPathType::Pointer path );

  int SampleAlongRay( const IndexType &index,
		      const PointType::VectorType &normalInward,
		      const itk::MaskImage::PixelType nextLevelTag,
		      double maxDistance,
		      double raySampleVx[],
		      double raySampleVy[],
		      double raySampleVz[],
		      double raySampleD[],
		      int sampleSize );

  int BuildPolynomialInward(itk::MaskImage::PixelType nextLevelTag,
			    const IndexType &index,
			    const PointType::VectorType &normalInward,
			    CurveFit *curveFit[]);
  void PropagateCurve(const IndexType &index,
		      const PointType::VectorType &normalInward,
		      CurveFit *curveFit[]);
  int ExtrapolateVelocityAtExteriorVoxel(const IndexType &index,
					 itk::MaskImage::PixelType nextLevelTag,
					 PointType::VectorType &normalInward, double v[]);
  int ExtrapolateVelocityAtExteriorVoxel(const IndexType &index,
					 itk::MaskImage::PixelType nextLevelTag,
					 const PointType::VectorType &normalInward, double v[]);
  void GetVoxelDirection(const IndexType &from, const IndexType &to, double dir[3]);
  void GetVoxelDirection(const IndexType &from, const IndexType &to, PointType::VectorType &dir);
  double ComputeDirectionWeight(const IndexType &from, const IndexType &to);
  void ExtrapolateVelocityLayer(void *currentLevel,
				void *nextLevel,
				itk::MaskImage::PixelType nextLevelTag,
				double tolDistance);
  StatusType ComputeWallCondition();
  void Initialize();
  bool IsModified();
  itk::TimeStamp m_InitializationTime;

#if (__PCMR_DISTANCE_FILTER__==SIGNEDMAURER)
  //#ifdef __USE_SIGNED_MAURER_FILTER__
  typedef itk::SignedMaurerDistanceMapImageFilter<itk::MaskImage,itk::PhaseContrast3DImage> DistanceFilterType;
#elif (__PCMR_DISTANCE_FILTER__==ANTIALIAS)
  typedef itk::AntiAliasBinaryImageFilter<itk::MaskImage,itk::PhaseContrast3DImage> DistanceFilterType;
#elif (__PCMR_DISTANCE_FILTER__==POLYMESH)
  typedef PolyMeshToSignedDistanceImageFilter DistanceFilterType;
#endif
  DistanceFilterType::Pointer m_DistanceFilter;
  GradientFilterType::Pointer m_DistanceGradientFilter;

  typedef enum {X=0, Y=1, Z=2, D=3} ComponentType;
  itk::PhaseContrast3DImage::Pointer m_ImageComponentOriginal[4];
  itk::PhaseContrast3DImage::Pointer m_ImageComponent[4];
  GradientFilterType::Pointer m_NumericalGradient[3];
  SmoothFilterType::Pointer m_SmoothFlow[3];
  BSplineFunctionType::Pointer m_BSplineFunction[4];
  LinearInterpolateFunctionType::Pointer m_LinearInterpolateFunction[4];
  LinearInterpolateGradientFunctionType::Pointer m_LinearInterpolateCovariantFunction[3];
  itk::MaskImage::Pointer m_ImageMask;
  itk::MaskImage::Pointer m_ImageLevelMask;
  itk::MaskImage::Pointer m_ImageRasterMask;

  vtkSmartPointer<vtkPolyData> m_BoundaryMesh;

  double m_Viscocity;
  double m_VelocityUnitFactor;
  double m_SizeVoxel;
  std::string m_CorrectedFlowFileName;
  std::string m_LevelImageFileName;

  bool m_ComputeWallCondition;
  BoundaryLocatorType m_BoundaryLocator;
private:
  void EvaluateDMapGradient(const ContinuousIndexType &index, double grad[]);
  void GetNormalFromGradientImage(const IndexType &index, double normal[]);
  WallShearRateFunction2( const Self& ); //purposely not implemented
  void operator=( const Self& );          //purposely not implemented
};

END_PCMR_DECLS

#endif
