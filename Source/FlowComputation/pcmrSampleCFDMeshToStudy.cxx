
#include "pcmrCFDVelocityToImageFilter.h"
#include "vtkUnstructuredGrid.h"
#include "itkImageFileWriter.h"
#include "itkMultiplyImageFilter.h"
#include "pcmrSampleCFDMeshToStudy.h"

BEGIN_PCMR_DECLS

StatusType SampleCFDMeshToStudy( vtkUnstructuredGrid *CFDMesh,
                                 const char *PrefixPath,
                                 double Spacing[3], 
                                 unsigned int ExtraPad, 
                                 unsigned int SamplePerCell,
                                 double velocityFactor )
{
  double origin[3];
  pcmr::CFDVelocityToImageFilter::SizeType dimensions;
  double bounds[ 6 ];
  CFDMesh->GetBounds( bounds );
  int n[ 3 ];
  for( int i = 0; i < 3; i++ )
    {
    double min = bounds[ 2*i ];
    int n = int( ceil(double( bounds[ 2*i + 1 ] - min )/Spacing[i] ) );
    dimensions[ i ] = n + 2 * ExtraPad;
    origin[ i ] = min - ExtraPad * Spacing[i];
    }
  pcmr::CFDVelocityToImageFilter::Pointer voxelizer = 
    pcmr::CFDVelocityToImageFilter::New( );
  voxelizer->SetInputCFD( CFDMesh );
  voxelizer->SetOrigin( origin );
  voxelizer->SetSize( dimensions );
  voxelizer->SetSpacing( Spacing );
  voxelizer->SetSampleSize( SamplePerCell );
  voxelizer->Update( );
  typedef itk::MultiplyImageFilter<pcmr::CFDVelocityToImageFilter::OutputImageType> MultiplyImageFilterType; 
  MultiplyImageFilterType::Pointer convertToCM = MultiplyImageFilterType::New( );
  convertToCM->SetConstant2( velocityFactor );
  typedef itk::ImageFileWriter<pcmr::CFDVelocityToImageFilter::OutputImageType> ImageWriterType;
  ImageWriterType::Pointer writer = ImageWriterType::New( );
  writer->SetInput( convertToCM->GetOutput( ) );

 for( int idx = 0; idx < 3; idx++ )
    {
    convertToCM->SetInput( voxelizer->GetOutput( idx ) );
    std::string prefix( PrefixPath );
    prefix += "_";
    prefix += ('0' + idx);
    prefix += ".vtk";
    writer->SetFileName( prefix );
    writer->Update( );
    }
 return OK;
}

END_PCMR_DECLS
