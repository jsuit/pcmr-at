#ifndef __pcmrFlowComputation_h
#define __pcmrFlowComputation_h

#include "pcmrDefs.h"
#include <vector>

BEGIN_PCMR_DECLS

/**
   Point structure used to pass the coordinates of a seed point
 */
struct SeedPoint
{
  float x;
  float y;
  float z;
  SeedPoint(float _x = 0.0, float _y = 0.0, float _z = 0.0)
  : x(_x), y(_y), z(_z) { }
};

typedef std::vector<SeedPoint> SeedCollection;

END_PCMR_DECLS

#endif
