#include "pcmrWallShearRateFunction2.h"
#include "itkVTKImageToImageFilter.h"
#include "pcmrImageUtil.h"
#include "itkBinaryContourImageFilter.h"
#include "itkLevelSetNeighborhoodExtractor.h"
#include "itkImageDuplicator.h"
#include "itkStructHashFunction.h"
#include "itksys/hash_map.hxx"

#include "itkComposeImageFilter.h"

#include "itkBinaryThresholdImageFilter.h"

#include "itkTriangleMeshToBinaryImageFilter.h"
#include "itkRegionOfInterestImageFilter.h"
#include "itkLineConstIterator.h"
#include "itkPathIterator.h"
#include "itkConstNeighborhoodIterator.h"
#include "itkImageRegionConstIteratorWithIndex.h"
#include "pcmrCurveFit.h"
#include "pcmrPolynomialFit.h"
#include "vtkPoints.h"
#include "vtkPolyData.h"
#include "vtkCellArray.h"

BEGIN_PCMR_DECLS

typedef itk::Mesh<float, 3>           itkTriangleMeshType;

// internal constants

#define WSR_NORM_EPS          1.0e-8

#define WSR_OK                   0 
#define WSR_EXTRAPOLATE_TOOFEW   1
#define WSR_NORMAL_SMALL         2
#define WSR_EXTRAPOLATE_LOOP     3
#define WSR_EXTRAPOLATE_UNKNOWN  4
#define WSR_INDEX_OUTSIDE        5
#define WSR_INSIDE_NOT_FOUND     6

#define WSR_LEVEL_EXTENDED 127

#define __WSR_TRACE_MAIN_STEPS__

#if defined(_MSC_VER)
#if !defined(PCMR_DEPLOY)
#define PCMR_DEPLOY
#endif
#else
#define __WSR_WRITE_RASTERIZED_IMAGE__
#define __WSR_WRITE_DISTANCE_MAP_IMAGE__
#define __WSR_WRITE_EXTENDED_IMAGE__
#endif

#if defined(__WSR_TRACE_MAIN_STEPS__)
#define TRACE_MAIN_STEP(S,M)                    \
  std::cout << "[TRACE] -- " << S << " -- " << M << std::endl;
#else
#define TRACE_MAIN_STEP(S,M)
#endif

//#define TRACE_COMPUTATION
//#define __TRACE_COMPUTEWALLCONDITION__

#if (__PCMR_DISTANCE_FILTER__==ANTIALIAS)
//#if !defined(__USE_SIGNED_MAURER_FILTER__)
#define __WSR_INSIDE_POSITIVE__
#else
#endif

#define WSR2_DUMP_VELOCITY(msg, v)                                      \
  std::cout << msg << "[" << v[0] << "," << v[1] << "," << v[2]  << "," << v[3] << "]"
#define WSR2_DUMP_VELOCITY3(msg, v)                                      \
  std::cout << msg << "[" << v[0] << "," << v[1] << "," << v[2]  << "]"

struct CumulativeAverageType {
  float v[4];
  itk::PhaseContrast3DImage::PointType::VectorType NormalInward;
  VnlPolynomial Poly;
  size_t count;
  CumulativeAverageType()
    : NormalInward(), Poly(0)
  {
    v[0] = v[1] = v[2] = v[3] = 0.0;
    count = 0;
  }

  CumulativeAverageType(const CumulativeAverageType& a)
    : NormalInward(a.NormalInward), Poly(a.Poly.coefficients())
  {
    for (int i = 0; i < 4; i++)
      {
      this->v[i] = a.v[i];
      };
    this->count = a.count;
  }
  
  CumulativeAverageType& operator=(const CumulativeAverageType& a)
  {
    for (int i = 0; i < 4; i++)
      {
      this->v[i] = a.v[i];
      };
    this->count = a.count;
    this->NormalInward = a.NormalInward;
    this->Poly.set_coefficients(a.Poly.coefficients());
    return *this;
  }

  void SetDistance(float d)
  {
    this->v[3] = d;
  }

  void AvgVelocity(const float *v)
  {
    size_t n = this->count++;
    float a = 1.0/this->count;
    float b = n*a;
    for(int i = 0; i < 3; i++)
      {
      this->v[i] *= b;
      this->v[i] += a*v[i];
      }
  }

  void AvgVelocity(const double *v)
  {
    size_t n = this->count++;
    double a = 1.0/this->count;
    double b = n*a;
    for(int i = 0; i < 3; i++)
      {
      this->v[i] *= b;
      this->v[i] += a*v[i];
      }
  }

  /* double v[4]*/
  CumulativeAverageType& operator+=(const float *v)
  {
    this->AvgVelocity(v);
    // copy the distance;
    this->v[3] = v[3];
    return *this;
  }

  CumulativeAverageType& operator+=(const CumulativeAverageType& cavt)
  {
    this->AvgVelocity(cavt.v);
    return *this;
  }
};

typedef itksys::hash_map<itk::PhaseContrast3DImage::IndexType,CumulativeAverageType,itk::StructHashFunction<itk::PhaseContrast3DImage::IndexType> > HashIndexType;

/* 
  float v0[4] , v0[3] is distance
  float v1[3]
 */
inline
void NearWallInterpolate(const float *v0, float d1, float *v1)
{
  const double k = 1.0; 
  float rd = d1/v0[3];
  for(int i = 0; i < 3; i++)
    {
    v1[i] = v0[i]*rd*k;
    }
}

WallShearRateFunction2::WallShearRateFunction2()
{
  this->m_DistanceFilter = DistanceFilterType::New();
#if (__PCMR_DISTANCE_FILTER__==SIGNEDMAURER)
  this->m_DistanceFilter->SquaredDistanceOff();
  this->m_DistanceFilter->UseImageSpacingOn();
#elif (__PCMR_DISTANCE_FILTER__==ANTIALIAS)
  this->m_DistanceFilter->SetNumberOfLayers(6);
  this->m_DistanceFilter->SetMaximumRMSError(0.005);
  this->m_DistanceFilter->UseImageSpacingOn();
#elif (__PCMR_DISTANCE_FILTER__==POLYMESH)
#else
  #error "Distance filter type not defined"
#endif
  this->m_DistanceGradientFilter = GradientFilterType::New();
  for(int i = 0; i < 4; i++)
    {
    this->m_BSplineFunction[i] = BSplineFunctionType::New();
    this->m_BSplineFunction[i]->SetSplineOrder(3);
    this->m_LinearInterpolateFunction[ i ] = LinearInterpolateFunctionType::New( );
    if ( i < 3 )
      {
      this->m_SmoothFlow[ i ] = SmoothFilterType::New( );
      this->m_NumericalGradient[ i ] = GradientFilterType::New( );
      this->m_LinearInterpolateCovariantFunction[ i ] = LinearInterpolateGradientFunctionType::New( );
      this->m_LinearInterpolateCovariantFunction[ i ]->SetInputImage( this->m_NumericalGradient[ i ]->GetOutput( ) );
      }
    }
  this->m_VelocityUnitFactor = 1.0;
  this->m_Viscocity = 3.5e-3;
  this->m_ComputeWallCondition = true;
  this->m_BoundaryLocator = BLOCATOR_LEVELSET;
  this->m_SizeVoxel = 1.0;
}

WallShearRateFunction2::~WallShearRateFunction2()
{
}

template <class TVector>
static void DumpVector3D(const char *msg, const TVector &x)
{
  std::cout << msg << " = [";
  for(int i = 0; i < 3; i++)
    {
    std::cout << (i?", ":"") << x[i];
    }
  std::cout << "]\n";
}

template <class T>
static void DumpArray(const char *msg, T *x, int size)
{
  std::cout << msg << " [";
  for(int i = 0; i < size; i++)
    {
    std::cout << (i?", ":"") << x[i];
    }
  std::cout << "]\n";
}

void WallShearRateFunction2::EvaluateAtPhysicalPoint(const PointType &point, double wsr[])
{
  double normal[3];
  this->EvaluateNormalFromDistanceMap(point, normal);
  this->EvaluateAtPhysicalPointWithNormal(point, normal, wsr);
}

void WallShearRateFunction2::EvaluateAtPhysicalPointWithNormal(const PointType &point, 
                                                               const double normal[], double wsr[])
{
  ContinuousIndexType index;

  this->m_ImageComponentOriginal[X]->TransformPhysicalPointToContinuousIndex(point,
                                                                             index);
  this->EvaluateAtContinuousIndex(index, normal, wsr);
}

double WallShearRateFunction2::EvaluateAtPhysicalPoint(const PointType &point)
{
  double normal[3];
  this->EvaluateNormalFromDistanceMap(point, normal);
  return this->EvaluateAtPhysicalPointWithNormal(point, normal);
}

double WallShearRateFunction2::EvaluateAtPhysicalPointWithNormal(const PointType &point, const double normal[])
{
  ContinuousIndexType index;

  this->m_ImageComponentOriginal[X]->TransformPhysicalPointToContinuousIndex( point,
                                                                              index );
  double wsr = this->EvaluateAtContinuousIndex( index, normal );
  return wsr;
}

void WallShearRateFunction2::EvaluateAtContinuousIndex(const ContinuousIndexType &index,
                                                       const double normal[],
                                                       double wsr[])
{
  typedef vnl_matrix<BSplineFunctionType::OutputType>     MatrixType;
  typedef vnl_vector<BSplineFunctionType::OutputType>     VectorType;
  typedef vnl_vector_ref<BSplineFunctionType::OutputType> VectorRefType;

  if (this->IsModified())
    {
    this->Initialize();
    }

  MatrixType gradV(3,3);
  
  for (int i = 0; i < 3; i++)
    {
//#define __WSR_USE_GAUSSIAN_GRADIENT__
#if defined( __WSR_USE_GAUSSIAN_GRADIENT__ )
    GradientType gf = this->m_LinearInterpolateCovariantFunction[i]->EvaluateAtContinuousIndex(index);
#else
    GradientType gf = this->m_BSplineFunction[i]->EvaluateDerivativeAtContinuousIndex(index);
#endif
    VectorRefType gi = gf.GetVnlVector();
    gradV.set_row(i, gi);
    }
  MatrixType symGradV = gradV.transpose();
  symGradV += gradV;
  symGradV *= 0.5;
  VectorType N(normal, 3);
  N.normalize();
  VectorType tau = symGradV * N;
  // project in order to make it tanget to the boundary, this fix the
  // error related to n * v == 0
  VectorType tau_p = tau - dot_product(tau,N)*N;
  const double k = 2*this->m_VelocityUnitFactor;
  wsr[0] = k * tau_p(0);
  wsr[1] = k * tau_p(1);
  wsr[2] = k * tau_p(2);
}

// ver vnl/vnl_matrix.h
double WallShearRateFunction2::EvaluateAtContinuousIndex(const ContinuousIndexType &index,
                                                         const double normal[])
{
  double wsr[3];
  this->EvaluateAtContinuousIndex(index, normal, wsr);
  double tau = 0;
  for (int i = 0; i < 3; i++)
    {
    tau += wsr[i]*wsr[i];
    }
  return sqrt(tau);
}

void WallShearRateFunction2::EvaluateVorticityAtPhysicalPoint( const PointType &point, double vorticity[] )
{
  ContinuousIndexType index;

  this->m_ImageComponentOriginal[X]->TransformPhysicalPointToContinuousIndex(point,
                                                                             index);
  this->EvaluateVorticityAtContinuousIndex( index, vorticity );
}

void WallShearRateFunction2::EvaluateVorticityAtContinuousIndex( const ContinuousIndexType &index,
                                                                 double vorticity[] )
{
  typedef vnl_matrix<BSplineFunctionType::OutputType>     MatrixType;
  typedef vnl_vector<BSplineFunctionType::OutputType>     VectorType;
  typedef vnl_vector_ref<BSplineFunctionType::OutputType> VectorRefType;

  if (this->IsModified())
    {
    this->Initialize();
    }

  MatrixType gradV(3,3);
  
  for( int i = 0; i < 3; i++ )
    {
    GradientType gf = this->m_BSplineFunction[i]->EvaluateDerivativeAtContinuousIndex( index );
    VectorRefType gi = gf.GetVnlVector( );
    gradV.set_row( i, gi );
    }
  vorticity[0] = gradV( 2, 1 ) - gradV( 1, 2 );
  vorticity[1] = gradV( 0, 2 ) - gradV( 2, 0 );
  vorticity[2] = gradV( 1, 0 ) - gradV( 0, 1 );
}

void WallShearRateFunction2::EvaluateNormalFromDistanceMap(const PointType &point, double normal[])
{
  ContinuousIndexType index;

  this->m_ImageComponentOriginal[X]->TransformPhysicalPointToContinuousIndex(point,
                                                                             index);
  this->EvaluateNormalFromDistanceMap(index, normal);
}

void WallShearRateFunction2::EvaluateNormalFromDistanceMap(const ContinuousIndexType &index, double normal[])
{
  if (this->IsModified())
    {
    this->Initialize();
    }
  this->EvaluateDMapGradient(index, normal);
}

void WallShearRateFunction2::EvaluateDMapGradient(const ContinuousIndexType &index, double normal[])
{
  const GradientType &grad = this->m_BSplineFunction[3]->EvaluateDerivativeAtContinuousIndex(index);
  for(int i = 0; i < 3; i++)
    {
// this is due to that SIGNED_MAURER generate interior as negative

    // the gradient of the signed distance function point to the
    // positive side, ie., if the interior is positive the normal
    // points inward, otherwise if the interior is negative the normal
    // points outward
#if defined(__WSR_INSIDE_POSITIVE__)
    normal[i] = grad[i];
#else
    normal[i] = -grad[i];
#endif
    }
}
  
float WallShearRateFunction2::GetDistanceAtContinuousIndex( const ContinuousIndexType &index )
{
  return this->m_LinearInterpolateFunction[ D ]->EvaluateAtContinuousIndex( index );
}

void WallShearRateFunction2::GetNormalFromGradientImage(const IndexType &index, double normal[])
{
  const GradientFilterType::OutputPixelType &grad = this->m_DistanceGradientFilter->GetOutput()->GetPixel(index);
  
  for(int i = 0; i < 3; i++)
    {
// this is due to that SIGNED_MAURER generate interior as negative

    // the gradient of the signed distance function point to the
    // positive side, ie., if the interior is positive the normal
    // points inward, otherwise if the interior is negative the normal
    // points outward
#if defined(__WSR_INSIDE_POSITIVE__)
    normal[i] = grad[i];
#else
    normal[i] = -grad[i];
#endif
    }
}

bool WallShearRateFunction2::EvaluateUnitNormalFromDistanceMap(const PointType &point, double normal[])
{
  double _normal[3];

  this->EvaluateNormalFromDistanceMap(point, _normal);
  double norm = sqrt(_normal[0]*_normal[0]+_normal[1]*_normal[1]+_normal[1]*_normal[1]);
  if (norm <1.0e-10)
    {
    return false;
    }
  normal[0] = _normal[0]/norm;
  normal[1] = _normal[1]/norm;
  normal[2] = _normal[2]/norm;
  
  return true;
}

int WallShearRateFunction2::EvaluateGradientAtPhysicalPoint(const PointType &point,
                                                            OutputType value[4],
                                                            GradientType gradient[4])
{
  if (this->IsModified())
    {
    this->Initialize();
    }
  for(int i = 0; i < 4; i++)
    {
    this->m_BSplineFunction[i]->EvaluateValueAndDerivative(point, value[i], gradient[i]);
    }
  return 0;
}

void WallShearRateFunction2::Initialize()
{
  typedef itk::ImageDuplicator<itk::PhaseContrast3DImage> ImageDuplicatorType;

  // make a copy of the flow components
  for( int i = 0; i < 3; i++ )
    {
    ImageDuplicatorType::Pointer dup = ImageDuplicatorType::New( );
    dup->SetInputImage( this->m_ImageComponentOriginal[ i ] );
    dup->Update( );
    this->m_ImageComponent[ i ] = dup->GetModifiableOutput( );
    }
  // setup the internal pipeline
  if (!this->m_ImageComponent[X])
    {
    itkExceptionMacro("Component X of flow is NULL. Use method SetComponentXImage or SetImageDataFromStudy.");
    }
  if (!this->m_ImageComponent[Y])
    {
    itkExceptionMacro("Component Y of flow is NULL. Use method SetComponentYImage or SetImageDataFromStudy.");
    }
  if (!this->m_ImageComponent[Z])
    {
    itkExceptionMacro("Component Z of flow is NULL. Use method SetComponentZImage or SetImageDataFromStudy.");
    }

  this->m_ImageComponent[X]->Update();
  this->m_ImageComponent[Y]->Update();
  this->m_ImageComponent[Z]->Update();

  // compute the raster mask 
  this->CreateRasterMaskFromBoundaryMesh();

#if  (__PCMR_DISTANCE_FILTER__==POLYMESH)
  this->m_DistanceFilter->SetInputMesh(this->m_BoundaryMesh);
  this->m_DistanceFilter->SetSize(this->m_ImageComponent[X]->GetLargestPossibleRegion().GetSize());
  this->m_DistanceFilter->SetSpacing(this->m_ImageComponent[X]->GetSpacing());
  this->m_DistanceFilter->SetOrigin(this->m_ImageComponent[X]->GetOrigin());
  this->m_DistanceFilter->SetDirection(this->m_ImageComponent[X]->GetDirection());
  this->m_DistanceFilter->SetSignMask(this->m_ImageRasterMask);
  this->m_DistanceFilter->Update();
  typedef itk::BinaryThresholdImageFilter<itk::PhaseContrast3DImage, itk::MaskImage> ThresholdFilterType;
  ThresholdFilterType::Pointer thresholder = ThresholdFilterType::New();
  thresholder->SetInput(this->m_DistanceFilter->GetOutput());
  thresholder->SetLowerThreshold(-1000);
  thresholder->SetUpperThreshold(0);
  thresholder->SetInsideValue(1);
  thresholder->SetOutsideValue(0);
  thresholder->Update();
  this->m_ImageMask = thresholder->GetOutput();

#else
  this->m_ImageMask = this->m_ImageRasterMask;
  if (!this->m_ImageMask)
    {
    itkExceptionMacro("Mask of vessel is NULL. Use method SetMaskImage or SetBoundaryMesh.");
    }
  this->m_ImageMask->Update();

  this->m_DistanceFilter->SetInput(this->m_ImageMask);
  TRACE_MAIN_STEP("DistanceFilter", "START");
  this->m_DistanceFilter->Update();
  TRACE_MAIN_STEP("DistanceFilter", "END");
#endif
  
  this->m_ImageComponent[D] = this->m_DistanceFilter->GetOutput();
  this->m_DistanceGradientFilter->SetInput(this->m_DistanceFilter->GetOutput());
  TRACE_MAIN_STEP("GraidentDistanceFilter", "START");
  this->m_DistanceGradientFilter->Update();
  TRACE_MAIN_STEP("GradientDistanceFilter", "END");
#if defined(__WSR_WRITE_DISTANCE_MAP_IMAGE__) && !defined(PCMR_DEPLOY)
  WriteImage(this->m_DistanceFilter->GetOutput(), "/tmp/wsr_dmap.vtk");
#endif
  
  const SpacingType &spacing = this->m_ImageComponent[D]->GetSpacing();
  this->m_SizeVoxel = sqrt(spacing[0]*spacing[0] + 
                           spacing[1]*spacing[1] +
                           spacing[2]*spacing[2]);
  /*
  itk::MaskImage::SpacingType spacing;
  itk::MaskImage::PointType origin;
  itk::MaskImage::RegionType region;
  const size_t level = 1;
  GetSubdivisionInfo(this->m_ImageMask.GetPointer(),
                     level, origin, spacing, region);
  */
  // REVIEW: what to do in order to work on an isotropic space

  //std::cout << "this->m_ComposeVector->GetOutput()->Update();\n";
  this->m_BSplineFunction[D]->SetInputImage(this->m_ImageComponent[D]);
  for(int i = 0; i < 4; i++)
    {
    this->m_LinearInterpolateFunction[ i ]->SetInputImage( this->m_ImageComponent[ i ] );
    }
  StatusType statusWall = OK;
  if ( this->GetComputeWallCondition( ) )
    {
    statusWall = this->ComputeWallCondition( );
    if ( statusWall == OK )
      {
      for( int i = 0; i < 3; i++ )
        {
        this->m_SmoothFlow[ i ]->SetInput( this->m_ImageComponent[i] );
        this->m_SmoothFlow[ i ]->SetLayerMaskImage( this->m_ImageLevelMask );
        this->m_NumericalGradient[ i ]->SetInput( this->m_SmoothFlow[i]->GetOutput( ) );
        }

      if ( this->m_CorrectedFlowFileName != "" )
        {
      typedef itk::ComposeImageFilter<itk::PhaseContrast3DImage,itk::PhaseContrastVector3DImage> ComposeVectorFilterType;
        ComposeVectorFilterType::Pointer composeVector = ComposeVectorFilterType::New();
        for(int i = 0; i < 3; i++)
          {
          composeVector->SetInput(i, this->m_SmoothFlow[i]->GetOutput( ) );
          }
        WriteImage(composeVector->GetOutput(),
                   this->m_CorrectedFlowFileName.c_str());
#if defined(__WSR_WRITE_EXTENDED_IMAGE__)
        for(int i = 0; i < 3; i++)
          {
          composeVector->SetInput(i, this->m_ImageComponent[i] );
          }
        WriteImage( composeVector->GetOutput( ), "ExtendedFlowImage.vtk" );
#endif
        }
      }
    }

  for( int i = 0; i < 3; i++ )
    {
    if ( this->GetComputeWallCondition( ) && statusWall == OK )
      {
      this->m_SmoothFlow[ i ]->Update( );
      this->m_BSplineFunction[ i ]->SetInputImage( this->m_SmoothFlow[ i ]->GetOutput( ) );
      }
    else
      {
      this->m_BSplineFunction[i]->SetInputImage( this->m_ImageComponent[ i ] );
      this->m_NumericalGradient[ i ]->SetInput( this->m_ImageComponent[ i ] );
      }
    this->m_NumericalGradient[ i ]->Update( );
    }
  this->m_InitializationTime.Modified( );
}

inline bool distanceIsInside(float d_in, float d_out)
{
#if defined(__WSR_INSIDE_POSITIVE__)
  return d_in > d_out;
#else
  return d_in < d_out;
#endif
}

inline bool canExtrapolate(float d_in, float d_central, float d_out, double tol)
{
#if defined(__WSR_INSIDE_POSITIVE__)
  return (d_in - d_central > tol) && (d_central - d_out > tol);
#else
  return  (d_central - d_in > tol) && (d_out - d_central > tol);
#endif
}

/*
The outside voxel can be updated from the inside if the outside is not
too close to the boundary.
*/
inline bool checkMinimumDistance(float dOut, float dIn, float tol)
{
#if defined(__WSR_INSIDE_POSITIVE__)
  return (dOut <= -tol && dIn >= tol);
#else
  return (dOut >= tol && dIn <= -tol);
#endif
}

inline
void LinearExtrapolateVelocity(float *v_out, const float *v_central, const float *v_in)
{
  double slope = (v_out[3] - v_central[3])/(v_in[3] - v_central[3]);
  for(int i = 0; i < 3; i ++)
    {
    v_out[i] = (v_in[i]-v_central[i])*slope + v_central[i];
    }
}

/*
p0, p1, p2

p0 ==> d0=0, v0
p1 ==> d1,   v1
p2 ==> d2,   v2

(v1-v0)/(d1-d0) = (v2-v1)/(d2-d1)

v1 - (d1/(d2-d1))*(v2-v1) = v0

*/
inline
void ExtrapolateVelocityOnRay_Linear(float *p0, const float *p1, const float *p2)
{
  double slope = p1[3]/(p2[3] - p1[3]);
  for(int i = 0; i < 3; i ++)
    {
    p0[i] = p1[i] - (p2[i] - p1[i])*slope;
    }
}

#define READ_VPIXEL(I,idx,v)                    \
  for(int __i = 0; __i < 3; __i++)              \
    {                                           \
    (v)[__i] = I[__i]->GetPixel(idx);           \
    }                                         

#define READ_4DPIXEL(I,idx,v)                         \
  for(int __i = 0; __i < 4; __i++)                    \
    {                                                 \
    (v)[__i] = I[__i]->GetPixel(idx);                 \
    }                                         

#define WRITE_VPIXEL(I,idx,v)                              \
  for(int __i = 0; __i < 3; __i++)                         \
    {                                                      \
    I[__i]->SetPixel(idx, (v)[__i]);                       \
    }                

#define WRITE_4DPIXEL(I,idx,v)                             \
  for(int __i = 0; __i < 4; __i++)                         \
    {                                                      \
    I[__i]->SetPixel(idx, (v)[__i]);                       \
    }                

void WallShearRateFunction2::GetVoxelDirection(const IndexType &from,
                                               const IndexType &to,
                                               double dir[3])
{
  PointType ptFrom, ptTo;
  this->m_ImageComponent[D]->TransformIndexToPhysicalPoint(from, ptFrom);
  this->m_ImageComponent[D]->TransformIndexToPhysicalPoint(to, ptTo);
  dir[0] = ptTo[0] - ptFrom[0];
  dir[1] = ptTo[1] - ptFrom[1];
  dir[2] = ptTo[2] - ptFrom[2];
}

void WallShearRateFunction2::GetVoxelDirection(const IndexType &from,
                                               const IndexType &to,
                                               PointType::VectorType &dir)
{
  PointType ptFrom, ptTo;
  this->m_ImageComponent[D]->TransformIndexToPhysicalPoint(from, ptFrom);
  this->m_ImageComponent[D]->TransformIndexToPhysicalPoint(to, ptTo);
  dir = ptTo -  ptFrom;
}

double WallShearRateFunction2::ComputeDirectionWeight(const IndexType &from,
                                                      const IndexType &to)
{
  ContinuousIndexType contIdx(from);
  double normal[3];
  // REVIEW:
  // the normal can be cached as the voxels can be visited more
  // than once
  this->EvaluateDMapGradient(contIdx, normal);
  double dir[3];
  this->GetVoxelDirection(from, to, dir);
  double weight = dir[0]*normal[0] + dir[1]*normal[1] + dir[2]*normal[2];
  return weight;
}

int WallShearRateFunction2::EvaluateNormalInward(const IndexType &index, PointType::VectorType &NormalInward)
{
  double normalTmp[3];
#if (__PCMR_DISTANCE_FILTER__==POLYMESH)
  PointType pt;

  this->m_ImageComponentOriginal[X]->TransformIndexToPhysicalPoint(index, pt);
  double _pt[3];
  _pt[0] = pt[0];
  _pt[1] = pt[1];
  _pt[2] = pt[2];
//#define __USE_IDEAL_NORMAL__
#if defined(__USE_IDEAL_NORMAL__)
  const double phantomInletCenterX = -40.391069470588235;
  const double phantomInletCenterY = 0.010682684705882229;
  NormalInward[0] = phantomInletCenterX - _pt[0];
  NormalInward[1] = phantomInletCenterY - _pt[1];
  NormalInward[2] = 0.0;
#else
  // this evaluate the outward direction
  this->m_DistanceFilter->EvaluateNormal(_pt, normalTmp);
  NormalInward = normalTmp;
  NormalInward *= -1;
#endif
#else
  this->GetNormalFromGradientImage(index, normalTmp);
  NormalInward = normalTmp;
#endif
  double normRay = NormalInward.GetNorm();
  if (normRay < WSR_NORM_EPS)
    {
    std::cerr << "too small normal direction " << NormalInward << std::endl;
    return WSR_NORMAL_SMALL;
    }
  NormalInward /= normRay;
  return WSR_OK;
}

#if !defined(_MSC_VER)
#define __WSR_TRACE_RAY__
#define __WSR_TRACE_LEVEL__      3
#define __WSR_TRACE_SLICE__      49
#define __WSR_TRACE_COMPONENT__  Z
#endif

void WallShearRateFunction2::ComputeSegmentAtNormalDirection(const IndexType &indexStart, 
                                                            int steps, 
                                                            IndexType &indexEnd, 
                                                            const PointType::VectorType &normalInward)
{
  PointType ptEnd;

  this->m_ImageComponent[D]->TransformIndexToPhysicalPoint(indexStart, ptEnd);
  ptEnd += steps * this->m_SizeVoxel * normalInward;
  this->m_ImageComponent[D]->TransformPhysicalPointToIndex(ptEnd, indexEnd);
}

/* x = x0 + l * r */
/* if x0 is outside the bounds a negative value or zero is returned */
inline 
double Ray_GetLambda( double xmin, double xmax, double x0, double r )
{
  const double eps0 = -1.0e-10;
  const double eps1 = 1.0e-10;

  if ( r > 0 )
    {
    if ( r < eps0 )
      {
      return std::numeric_limits<float>::max( );
      }
    return ( xmax - x0 ) / r;
    }
  if ( r < 0 )
    {
    if ( r > eps1 )
      {
      return std::numeric_limits<float>::max( );
      }
    return ( xmin - x0 ) / r;
    }
  return std::numeric_limits<float>::max( );
}

inline void
Ray_SampleIndex( const WallShearRateFunction2::ContinuousIndexType &indexStart,
                 const WallShearRateFunction2::PointType::VectorType &rayDirection,
                 double t,
                 WallShearRateFunction2::ContinuousIndexType &index )
{
  for( int i = 0; i < 3; i++ )
    {
    index[ i ] = indexStart[ i ] + t * rayDirection[ i ];
    }
}

int WallShearRateFunction2::FindExtremeVertex( const ContinuousIndexType &index,
                                               const PointType::VectorType &normal,
                                               ContinuousIndexType &index1,
                                               PointType::VectorType &ciNormal)
{ 
  const PointType &origin = this->m_ImageLevelMask->GetOrigin();
  // build a physical point which is an offset from the origin
  PointType tmpNormal = origin + normal;
  // the transformation of that point is the normal in the continuous
  // index space. This can be done more eficiently if the
  // transformation just involve the spacing
  ContinuousIndexType _ciNormal;
  this->m_ImageLevelMask->TransformPhysicalPointToContinuousIndex( tmpNormal, _ciNormal );
  // now compute on the CI space
  ciNormal = _ciNormal.GetDataPointer( );
  ciNormal.Normalize( );
#if defined(__WSR_TRACE_RAY__) && !defined(PCMR_DEPLOY)
  if( index[2] == __WSR_TRACE_SLICE__ )
    {
    std::cout << "FindExtremeVertex: " << index << " normal = " << ciNormal << std::endl; 
    }
#endif
  const itk::MaskImage::RegionType &region = this->m_ImageLevelMask->GetLargestPossibleRegion( );
  const itk::MaskImage::SizeType &size = region.GetSize();
  double rmin = 0.0;
  for( int i = 0; i < 3; i++ )
    {
    double r = Ray_GetLambda( 0, size[ i ] - 1, index[ i ], ciNormal[ i ] );
    if ( r > 0 )
      {
      if ( i == 0 || r < rmin )
        {
        rmin = r;
        }
      }
    else
      {
      rmin = 0;
      break;
      }
    } 
  for( int i = 0; i < 3; i++ )
    {
    index1[ i ] = index[ i ] + rmin * ciNormal[ i ];
    }
  return WSR_OK;
}

int WallShearRateFunction2::BuildParametricRay( const ContinuousIndexType &index,
                                                const PointType::VectorType &normal,
                                                ParametricPathType::Pointer path )
{
  const itk::MaskImage::RegionType & region = this->m_ImageLevelMask->GetLargestPossibleRegion( );
  if ( !region.IsInside( index ) )
    {
    return WSR_INDEX_OUTSIDE;
    }
  ContinuousIndexType indexEnd;
  PointType::VectorType ciNormal;
  int status = this->FindExtremeVertex( index, normal, indexEnd,  ciNormal );
  if ( status != WSR_OK )
    {
    return status;
    }
  path->AddVertex( index );
  path->AddVertex( indexEnd );
  return WSR_OK;
}

int WallShearRateFunction2::SampleAlongRay( const IndexType &index,
                                            const PointType::VectorType &normalInward,
                                            const itk::MaskImage::PixelType nextLevelTag,
                                            double maxDistance,
                                            double raySampleVx[],
                                            double raySampleVy[],
                                            double raySampleVz[],
                                            double raySampleD[],
                                            const int sampleSize )
{
  const itk::MaskImage::RegionType & region = this->m_ImageLevelMask->GetLargestPossibleRegion( );
  if ( !region.IsInside( index ) )
    {
    return WSR_INDEX_OUTSIDE;
    }
  ContinuousIndexType indexStart( index );
  ContinuousIndexType indexEnd;
  PointType::VectorType ciNormal;
  int status = this->FindExtremeVertex( indexStart, normalInward, indexEnd,  ciNormal );
  if ( status != WSR_OK )
    {
    return status;
    }
  ParametricPathType::Pointer ray = ParametricPathType::New( );
  ray->AddVertex( indexStart );
  ray->AddVertex( indexEnd );

  PointType pointStart;
  this->m_ImageLevelMask->TransformContinuousIndexToPhysicalPoint( indexStart,
                                                                   pointStart );
  PointType pointEnd;
  this->m_ImageLevelMask->TransformContinuousIndexToPhysicalPoint( indexEnd,
                                                                   pointEnd );
  PointType::VectorType cindexDirection = indexEnd - indexStart;
  PointType::VectorType rayGlobalDir = pointEnd - pointStart;
  double lengthSegment = rayGlobalDir.GetNorm( );
  
  itk::MaskImage::OffsetType offsetZero;
  offsetZero.Fill( 0 );
  // move to the first voxel marked as 2
  bool isAtEnd = false;
  IndexType lastIndexOut( index );
  IndexType currentIndex( index );
  ParametricPathType::InputType t = ray->StartOfInput( );
  ParametricPathType::InputType t_out = t;
  while ( !isAtEnd )
    {
    const itk::MaskImage::OffsetType offset = ray->IncrementInput( t );
    if ( offsetZero == offset )
      {
      isAtEnd = true;
      }
    else
      {
      currentIndex += offset;
      itk::MaskImage::PixelType l = this->m_ImageLevelMask->GetPixel( currentIndex );
      if ( l == 2 || l == 1 )
        {
        break;
        }
      lastIndexOut = currentIndex;
      t_out = t;
      }
    }
  if ( isAtEnd )
    {
    return WSR_INSIDE_NOT_FOUND;
    }
  ContinuousIndexType cindexOut;
  Ray_SampleIndex( indexStart, cindexDirection, t_out, cindexOut );
  double d_out = this->GetDistanceAtContinuousIndex(  cindexOut );
  ContinuousIndexType cindexT;
  Ray_SampleIndex( indexStart, cindexDirection, t, cindexT );
  double d_t = this->GetDistanceAtContinuousIndex(  cindexT );
  ParametricPathType::InputType t0;
  if ( !( d_out > d_t ) )
    {
    t0 = ( t_out + t ) / 2;
    }
  else if ( d_out < 0 )
    {
    t0 = t_out;
    }
  else if( d_t > 0)
    {
    t0 = t;
    }
  else
    {
    t0 = -d_out * ( t - t_out ) / ( d_t - d_out ) + t_out;
    }
#if defined(__WSR_TRACE_RAY__) && !defined(PCMR_DEPLOY)
  if ( !( d_out > d_t ) )
    {
    float v[4];
    std::cout << "===================================================\n";
    std::cout << "t_out = " << t_out << std::endl;
    std::cout << "d_out = " << d_out << std::endl;
    std::cout << "t = " << t << std::endl;
    std::cout << "d_t = " << d_t << std::endl;
    std::cout << "t0 = " << t0 << std::endl;
    std::cout << "t0 - t = " << t0 - t << std::endl;
    std::cout << "lengthSegment = " << lengthSegment << std::endl;
    std::cout << "lastIndexOut = " << lastIndexOut << std::endl;
    READ_4DPIXEL(this->m_ImageComponent, lastIndexOut, v);
    ContinuousIndexType cindexTOUT;
    Ray_SampleIndex( indexStart, cindexDirection, t_out, cindexTOUT );
    std::cout << "cindexTOUT = " << cindexTOUT << std::endl;
    WSR2_DUMP_VELOCITY(" vel(t_out) = ", v ) << std::endl;
    ContinuousIndexType cindexT;
    Ray_SampleIndex( indexStart, cindexDirection, t, cindexT );
    std::cout << "cindexT = " << cindexT << std::endl;
    IndexType indexT;
    for( int i = 0; i < 3; i++ )
      {
      indexT[i] = itk::Math::RoundHalfIntegerUp<IndexType::IndexValueType>( cindexT[i] );
      }
    std::cout << "indexT = " << indexT << std::endl;
    READ_4DPIXEL(this->m_ImageComponent, indexT, v);
    WSR2_DUMP_VELOCITY(" vel(t) = ", v ) << std::endl;
    std::cout << cindexDirection << std::endl;
    std::cout << "===================================================\n";
    }
#endif

  assert( itk::Math::FloatDifferenceULP(t0, t_out) > 0 || itk::Math::FloatAlmostEqual( t0, t_out ) );
  assert( itk::Math::FloatDifferenceULP(t, t0) || itk::Math::FloatAlmostEqual( t0, t ) > 0 );
  std::vector<ParametricPathType::InputType> parameters;
  parameters.push_back( t0 );
  if ( !itk::Math::FloatAlmostEqual( t0, t ) )
    {
    parameters.push_back( t );
    }
  ParametricPathType::InputType t1 = t;
  // now collect the voxelx within maxDistance, then generate samples 
  while ( !isAtEnd )
    {
    const itk::MaskImage::OffsetType offset = ray->IncrementInput( t );
    if ( offsetZero == offset )
      {
      isAtEnd = true;
      }
    else
      {
      currentIndex += offset;
      itk::MaskImage::PixelType l = this->m_ImageLevelMask->GetPixel( currentIndex );
      if ( ( l != 1 && l != 2 ) || lengthSegment*(t-t0) > maxDistance  )
        {
        // we leave the mask or reach the distance limit
        break;
        }
      parameters.push_back( t );
      t1 = t;
      }
    }
  if ( itk::Math::FloatAlmostEqual( t0, t1 ) )
    {
    return WSR_EXTRAPOLATE_TOOFEW;
    }
  std::vector<ParametricPathType::InputType> *sample;
  std::vector<ParametricPathType::InputType> sampleUniform;
  if ( sampleSize <= parameters.size() )
    {
    sample = &parameters;
    }
  else
    {
    ParametricPathType::InputType a = t0;
    ParametricPathType::InputType d = ( t1 - t0 ) / ( sampleSize - 1 );
    sampleUniform.push_back( a );
    for ( int i = 1; i < sampleSize; i++ )
      {
      a += d;
      sampleUniform.push_back( a );
      sample = &sampleUniform;
      }
    }
  for( int i = 0; i < sample->size(); i++ )
    {
    ParametricPathType::InputType d = (*sample)[i];
    ContinuousIndexType ci;
    Ray_SampleIndex( indexStart, cindexDirection, d, ci );
    raySampleVx[ i ] = this->m_LinearInterpolateFunction[ X ]->EvaluateAtContinuousIndex( ci );
    raySampleVy[ i ] = this->m_LinearInterpolateFunction[ Y ]->EvaluateAtContinuousIndex( ci );
    raySampleVz[ i ] = this->m_LinearInterpolateFunction[ Z ]->EvaluateAtContinuousIndex( ci );
    raySampleD [ i ] = d * lengthSegment;
    }
  return WSR_OK;
}

#define __USE_PARAMETRIC_SAMPLER__
#if defined( __USE_PARAMETRIC_SAMPLER__ )

int WallShearRateFunction2::BuildPolynomialInward( itk::MaskImage::PixelType nextLevelTag,
                                                   const IndexType &index,
                                                   const PointType::VectorType &normalInward,
                                                   CurveFit *curveFit[] )
{
  // no more than 16 sample will be collected!!!!!
  double vRay[4][16];
  const double maxDistance = 3;
  const int sampleSize = 10;
  int status = this->SampleAlongRay( index, normalInward, nextLevelTag, 
                                     maxDistance, vRay[0], vRay[1], vRay[2], vRay[3], sampleSize );
  if ( status != WSR_OK )
    {
    std::cout << "SampleAlongRay failed. ERROR_CODE = " << status << std::endl;
    return status;
    }
#if defined(__WSR_TRACE_RAY__) && !defined(PCMR_DEPLOY)
  if( index[2] == __WSR_TRACE_SLICE__ && nextLevelTag == __WSR_TRACE_LEVEL__ )
    {
    std::cout << "SampleAlongRay OK\n";
    DumpArray( "X = ", vRay[ D ], sampleSize ); 
    DumpArray( "V = ", vRay[ __WSR_TRACE_COMPONENT__ ], sampleSize ); 
    }
#endif
  for ( int iv = 0; iv < 3; iv++ )
    {
    curveFit[ iv ]->SetData( vRay[ 3 ], vRay[ iv ], sampleSize );
    }
  return WSR_OK;
}

#else
int WallShearRateFunction2::BuildPolynomialInward( itk::MaskImage::PixelType nextLevelTag,
                                                   const IndexType &index,
                                                   const PointType::VectorType &normalInward,
                                                   CurveFit *curveFit[] )
{
  typedef itk::PolyLineParametricPath<3>                         PathType;
  typedef itk::PathConstIterator<itk::PhaseContrast3DImage, PathType> IterType;
  int steps = nextLevelTag+1;
  IndexType indexEnd;
  this->ComputeSegmentAtNormalDirection(index, steps, indexEnd, normalInward);
  ContinuousIndexType cindexStart(index);
  ContinuousIndexType cindexEnd(indexEnd);
  PointType pointStart;
  this->m_ImageLevelMask->TransformIndexToPhysicalPoint( index, pointStart );
  PathType::Pointer path = PathType::New();
  path->AddVertex(cindexStart);
  path->AddVertex(cindexEnd);
  IterType itLine(this->m_ImageComponent[D], path);
  double dirVoxel[3];
  itk::MaskImage::PixelType ltag = this->m_ImageLevelMask->GetPixel(itLine.GetIndex());
  if (!ltag || ltag >= nextLevelTag)
    {
    ++itLine;
    }
  // only two indexes are needed to do linear extrapolation
  const int deg = 3;
  const int iMin = deg + 1;
  const int iMax = deg + 3;
  const int nLinear = 2;
  int i;
  // no more than 16 sample will be collected!!!!!
  double vRay[4][16];

  int countUninitialized = 0;

  // here we must select a valid start point which must have 
  // ltag > nextLevelTag - 1, the end point is cindexEnd

#if defined(__WSR_TRACE_RAY__) && !defined(PCMR_DEPLOY)
  if( index[2] == __WSR_TRACE_SLICE__ && nextLevelTag == __WSR_TRACE_LEVEL__ )
    {
    float v[4];
    READ_4DPIXEL(this->m_ImageComponent, index, v);
    std::cout << "BuildPolynomialInward at" << index << " ";
    WSR2_DUMP_VELOCITY(" vel = ", v ) << std::endl;
    std::cout << "Normal = " << normalInward << std::endl;
    }
#endif

  for(i = 0; !itLine.IsAtEnd() && i < iMax; ++itLine)
    {
    itk::MaskImage::PixelType ltag = this->m_ImageLevelMask->GetPixel(itLine.GetIndex());
    if (!ltag || ltag > nextLevelTag)
      {
      // this situation will be reported at the end
      ++countUninitialized;
#if defined(__WSR_TRACE_RAY__) && !defined(PCMR_DEPLOY)
    if( index[2] == __WSR_TRACE_SLICE__ && nextLevelTag == __WSR_TRACE_LEVEL__ )
      {
      std::cout << ", " << itLine.Get() << "(?)";
      std::cout << "[" << this->m_ImageComponent[Y]->GetPixel(itLine.GetIndex()) << "]";
      std::cout << "\nREJECTING " << index << std::endl;
      }
#endif
      return WSR_EXTRAPOLATE_UNKNOWN;
      }
    this->GetVoxelDirection(index, itLine.GetIndex(), dirVoxel);
    PointType::VectorType vecVoxel(dirVoxel);
    double dRay = vecVoxel * normalInward;
#if defined(__WSR_TRACE_RAY__) && !defined(PCMR_DEPLOY)
    if( index[2] == __WSR_TRACE_SLICE__ && nextLevelTag == __WSR_TRACE_LEVEL__ )
      {
      std::cout << (i ? ", " : "") << itLine.Get() << "(" << dRay << ")";
      std::cout << "[" << this->m_ImageComponent[__WSR_TRACE_COMPONENT__]->GetPixel(itLine.GetIndex()) << "]";
      }
#endif

#define __USE_LINEAR_INTERPOLATOR__
#if defined( __USE_LINEAR_INTERPOLATOR__ )
    PointType::VectorType offsetNext( normalInward );
    offsetNext *= dRay;
    PointType pointNext( pointStart );
    pointNext += offsetNext;
    ContinuousIndexType cindexNext;
    this->m_ImageLevelMask->TransformPhysicalPointToContinuousIndex( pointNext,
                                                                     cindexNext );

    for (int iv = 0; iv < 3; iv++) 
      {
      vRay[iv][i] = this->m_LinearInterpolateFunction[ iv ]->EvaluateAtContinuousIndex( cindexNext );
      }
#if defined(__WSR_TRACE_RAY__) && !defined(PCMR_DEPLOY)
    if( index[2] == __WSR_TRACE_SLICE__ && nextLevelTag == __WSR_TRACE_LEVEL__ )
      {
      std::cout << "{" << vRay[__WSR_TRACE_COMPONENT__][i] << "}";
      }
#endif
#else
    READ_VPIXEL(this->m_ImageComponent, itLine.GetIndex(), vdata);
    for (int iv = 0; iv < 3; iv++) {vRay[iv][i] = vdata[iv];}
#endif
    vRay[3][i] = dRay;
    ++i;
    }

#if defined(__WSR_TRACE_RAY__) && !defined(PCMR_DEPLOY)
  if( index[2] == __WSR_TRACE_SLICE__ && nextLevelTag == __WSR_TRACE_LEVEL__ )
    {
    std::cout << std::endl;
    }
#endif

#if defined(__WSR_TRACE_RAY__) && !defined(PCMR_DEPLOY)
  if( index[2] == __WSR_TRACE_SLICE__ && nextLevelTag == __WSR_TRACE_LEVEL__ )
    {
    // no more than 16 sample will be collected!!!!!
    double vRay[4][16];
    const double maxDistance = 3.5;
    const int sampleSize = 10;
    int status = this->SampleAlongRay( index, normalInward, nextLevelTag, 
                                       maxDistance, vRay[0], vRay[1], vRay[2], vRay[3], sampleSize );
    if ( status != WSR_OK )
      {
      std::cout << "SampleAlongRay failed. ERROR_CODE = " << status << std::endl;
      }
    else
      {
      std::cout << "SampleAlongRay OK\n";
      DumpArray( "X = ", vRay[ D ], sampleSize ); 
      DumpArray( "V = ", vRay[ __WSR_TRACE_COMPONENT__ ], sampleSize ); 
      }
    }
#endif
  if ( i >= iMin )
    {
    int nsample = i;
    //std:cout << " nsample=" << nsample;
    VnlVectorRef x( nsample, vRay[3] );
    for( int iv = 0; iv < 3; iv++ )
      {
      VnlVectorRef y(nsample, vRay[iv]);
#if defined(__WSR_TRACE_RAY__) && !defined(PCMR_DEPLOY)
      if( index[2] == __WSR_TRACE_SLICE__ && nextLevelTag == __WSR_TRACE_LEVEL__ && iv == __WSR_TRACE_COMPONENT__ )
        {
        std::cout << "x = " << x << std::endl;
        std::cout << "y = " << y << std::endl;
        }
#endif
      curveFit[ iv ]->SetDegree( deg );
      curveFit[ iv ]->SetData( vRay[3], vRay[iv], nsample );
      //PolynomialFit( x, y, deg, curveFit[ iv ] );
      }
    }
  else // ( i< 2)
    {
    std::cerr << "Too few points to extrapolate linearly at voxel " << index << std::endl;
    return WSR_EXTRAPOLATE_TOOFEW;
    }
  return WSR_OK;
}
#endif


int WallShearRateFunction2::ExtrapolateVelocityAtExteriorVoxel(const IndexType &index,
                                                               itk::MaskImage::PixelType nextLevelTag,
                                                               PointType::VectorType &normalInward,
                                                               double v[])
{
  int status = this->EvaluateNormalInward(index, normalInward);
  if (status != WSR_OK)
    {
    return status;
    }
  return this->ExtrapolateVelocityAtExteriorVoxel(index, nextLevelTag, 
                                                  const_cast<const PointType::VectorType &>(normalInward), v);

}

void WallShearRateFunction2::PropagateCurve( const IndexType &index,
                                             const PointType::VectorType &normalInward,
                                             CurveFit *curveFit[] )
{
  typedef itk::PolyLineParametricPath<3> PathType;
  typedef itk::PathConstIterator<itk::MaskImage, PathType> IterType;
  PointType::VectorType normalOutward( normalInward );
  normalOutward *= -1.0;
  const int nlayers = 1;
  // center of reference voxel
  PointType pointStart;
  this->m_ImageLevelMask->TransformIndexToPhysicalPoint( index, pointStart );
  // helper point to cross the outside
  PointType pointCrossOutside = pointStart;
  pointCrossOutside += nlayers * this->m_SizeVoxel * normalOutward;
  ContinuousIndexType cindexPathStart( index );
  ContinuousIndexType cindexPathEnd;
  bool inside = this->m_ImageComponent[D]->TransformPhysicalPointToContinuousIndex( pointCrossOutside, cindexPathEnd );
  // build the path to traverse the voxels
  PathType::Pointer path = PathType::New( );
  path->AddVertex( cindexPathStart );
  path->AddVertex( cindexPathEnd );
  IterType itLine( this->m_ImageLevelMask, path );
  bool saveGWD = ::itk::Object::GetGlobalWarningDisplay( );
  ::itk::Object::GlobalWarningDisplayOff( );
  ++itLine;
  const itk::PhaseContrast3DImage::RegionType &region = this->m_ImageComponent[0]->GetLargestPossibleRegion( );
  for(int i = 0;
      region.IsInside( itLine.GetIndex() ) &&
        !itLine.IsAtEnd() && i < nlayers; ++itLine, ++i)
    {
    itk::MaskImage::PixelType tag = this->m_ImageLevelMask->GetPixel( itLine.GetIndex( ) );
    float v[3];
    double dirVoxel[3];
    this->GetVoxelDirection( index, itLine.GetIndex(), dirVoxel );
    PointType::VectorType vecVoxel(dirVoxel);
    double dRay = vecVoxel * normalOutward;
    if ( !tag || tag == WSR_LEVEL_EXTENDED )
      {
      for( int j = 0; j < 3; j++ )
        {
        v[j] = curveFit[j]->Evaluate( -dRay );
        }
      if( tag == WSR_LEVEL_EXTENDED )
        {
        float _v[3];
        READ_VPIXEL(this->m_ImageComponent, itLine.GetIndex(), _v);
        for( int j = 0; j < 3; j++ )
          {
          v[ j ] = ( v[ j ] + _v[ j ] ) / 2;
          }
        }
      WRITE_VPIXEL(this->m_ImageComponent, itLine.GetIndex(), v);
      this->m_ImageLevelMask->SetPixel( itLine.GetIndex(), WSR_LEVEL_EXTENDED );
      }
    }
  ::itk::Object::SetGlobalWarningDisplay( saveGWD );
}

int WallShearRateFunction2::ExtrapolateVelocityAtExteriorVoxel( const IndexType &index,
                                                                itk::MaskImage::PixelType nextLevelTag,
                                                                const PointType::VectorType &normalInward,
                                                                double v[])
{
  //VnlPolynomial polyFit[3] = {0, 0, 0};
  CurvePolynomialFit _curveFit[3];
  //CurveNaturalSplineFit _curveFit[3];
  CurveFit *curveFit[3] = { _curveFit, _curveFit + 1, _curveFit + 2 };

  for( int iv = 0; iv < 3; iv++ )
    {
    curveFit[ iv ]->SetDegree( 2 );
    }

 #if defined(__WSR_TRACE_RAY__) && !defined(PCMR_DEPLOY)
  if (index[2] == __WSR_TRACE_SLICE__ && nextLevelTag == __WSR_TRACE_LEVEL__ )
    {
    PointType ptIndex;
    float v[4];
    READ_4DPIXEL(this->m_ImageComponent, index, v);
    this->m_ImageComponent[D]->TransformIndexToPhysicalPoint(index, ptIndex);
    std::cout << "======================================================\n";
    std::cout << "Extrapolating at  = " << index << " == " << ptIndex
              << " tag = (" << int(this->m_ImageLevelMask->GetPixel(index)) << ")";
    WSR2_DUMP_VELOCITY(" vel = ", v ) << std::endl;
    }
#endif
  int status = BuildPolynomialInward( nextLevelTag, index, normalInward, curveFit );
  if (status == WSR_OK)
    {
    for (int i = 0; i < 3; i++)
      {
      v[i] = curveFit[ i ]->Evaluate( 0.0 );
      }
    this->PropagateCurve( index, normalInward, curveFit );
    }
#if defined(__WSR_TRACE_RAY__) && !defined(PCMR_DEPLOY)
  if (index[2] == __WSR_TRACE_SLICE__ && nextLevelTag == __WSR_TRACE_LEVEL__ )
    {
    curveFit[__WSR_TRACE_COMPONENT__]->Print( std::cout );
    WSR2_DUMP_VELOCITY3( "vel_interp = ", v ) << std::endl;
    }
#endif  
  return status;
}

#define __WSR_ENTRAPOLATE_ONLY_ONE_SIDE__

#ifdef __WSR_ENTRAPOLATE_ONLY_ONE_SIDE__
void WallShearRateFunction2::ExtrapolateVelocityLayer(void *_currentLevel,
                                                      void *_nextLevel,
                                                      itk::MaskImage::PixelType nextLevelTag,
                                                      double tolDistance)
{
  std::cout << "Extrapolating velocity to layer " << int(nextLevelTag) << std::endl;

  HashIndexType *currentLevel = static_cast<HashIndexType*>(_currentLevel);
  HashIndexType *nextLevel = static_cast<HashIndexType*>(_nextLevel);
  itk::MaskImage::SizeType nbrRadius = {{1,1,1}};
  // iterator to visit the neighbors of the voxels processed at each
  // level
  typedef itk::ConstNeighborhoodIterator<itk::MaskImage> NeighborhoodIteratorType;

  NeighborhoodIteratorType
    levelIter( nbrRadius, this->m_ImageLevelMask,
               this->m_ImageLevelMask->GetLargestPossibleRegion() );
  const NeighborhoodIteratorType::SizeValueType n = levelIter.Size();

  #if 0
  std::cout << "levelIter.Size() = " << levelIter.Size() << std::endl;
  for (NeighborhoodIteratorType::SizeValueType i = 0; i < n; i++)
    {
    std::cout << "at local index " << i 
              << " we have offset " << levelIter.GetOffset(i) << std::endl; 
    }
  #endif
  // index of last voxel in the neighborhood
  const NeighborhoodIteratorType::SizeValueType n_1 = n - 1;
  // index of center  voxel in the neighborhood
  const NeighborhoodIteratorType::SizeValueType c = n>>1;
  HashIndexType postAverage0;
  HashIndexType postAverage1;

  nextLevel->clear();
  size_t accepted;
  size_t rejected;
  do
    {
    accepted = 0;
    rejected = 0;
    for(HashIndexType::iterator it = currentLevel->begin();
        it != currentLevel->end(); ++it)
      {
      // loop over the neighborhood at current index, but just at the exterior
      // which are the voxels marked as 0 on this->m_ImageLevelMask
      levelIter.SetLocation(it->first);
      bool isInBound;
      // iterate the neighborhood of center voxel
      
      for (itk::MaskImage::SizeValueType i = 0; i < n; i++)
        {
        // the center of the neighborhood is the voxel processed from
        // currentLevel
        if (i == c) continue;
        int l1 = levelIter.GetPixel(i, isInBound);
        if (!isInBound) continue;
        if (!l1 || l1 == WSR_LEVEL_EXTENDED )
          {
          IndexType idx1 = levelIter.GetIndex(i);
          // idx1 is outside and is a neighbor of a boundary voxel idx1
          // could be updated along the normal direction
          // float v1[3];
          bool FitPoly = nextLevelTag>0;
          //int status = this->ExtrapolateVelocityAtExteriorVoxel(idx1, _nextLevel,this->m_ImageLevelMask, nextLevelTag, FitPoly);
          double v[3];
          PointType::VectorType normalInward;
          int status = this->ExtrapolateVelocityAtExteriorVoxel(idx1, nextLevelTag, normalInward, v);
          if (status == WSR_OK || l1 == WSR_LEVEL_EXTENDED )
            {
            CumulativeAverageType cavg;
            if ( status == WSR_OK )
              {
              for (int i = 0; i < 3; i++) 
                {
                cavg.v[i] = v[i];
                }
              }
            if ( l1 == WSR_LEVEL_EXTENDED )
              {
              float v[3];
              READ_VPIXEL( this->m_ImageComponent, idx1, v);
              // this is an average that give more weight to the
              // velocity just extrapolated
              cavg.AvgVelocity( v );
              }
            WRITE_VPIXEL(this->m_ImageComponent, idx1, cavg.v);
            this->m_ImageLevelMask->SetPixel(idx1, nextLevelTag);
            cavg.NormalInward = normalInward;
            (*nextLevel)[idx1] = cavg;

            ++accepted;
            if (postAverage0.find(idx1) != postAverage0.end())
              {
              postAverage0.erase(idx1);
              }
            }
          else
            {
            ++rejected;
            if (postAverage0.find(idx1) == postAverage0.end())
              {
              postAverage0[idx1] = CumulativeAverageType();
              }
            }
          }
        else
          {
          // this voxel has being already visited
          // extrapolate from index to this voxel in outward direction
          // and average
          }
        } // iterate the neighborhood of center voxel
      } // propagate from the currentLevel to the exterior
#if defined(__WSR_TRACE_RAY__) && !defined(PCMR_DEPLOY)
    std::cout << "In layer " << int(nextLevelTag) 
              << " accepted = " << accepted 
              << " rejected = " << rejected
              << " postAverage0.size() = " << postAverage0.size() << std::endl;
#endif
    }
  while (rejected > 0 && accepted > 0);
  
  // now process the rest of voxels which were not able to extrapolate
  std::cout << "There are " << postAverage0.size() << " remaining voxels to process\n";
  bool hasChanged = false;
  HashIndexType *postAvg0 = &postAverage0;
  HashIndexType *postAvg1 = &postAverage1;
  do
    {
    hasChanged = false;
    postAvg1->clear();

    for(HashIndexType::iterator it = postAvg0->begin();
        it != postAvg0->end(); ++it)
      {
      // this is a precondition check, after tunning it can be removed
      if (nextLevel->find(it->first) != nextLevel->end())
        {
        std::cout << it->first;
        WSR2_DUMP_VELOCITY(" was already extrapolated with value:", (*nextLevel)[it->first].v);
        continue;
        }
      levelIter.SetLocation(it->first);
      CumulativeAverageType &cavt = it->second;
#if defined(__TRACE_COMPUTEWALLCONDITION__)
      std::cout << "==================================================\n";
      std::cout << "Going to average " << it->first;
      WSR2_DUMP_VELOCITY(" ", it->second.v) << std::endl;
#endif
      for (itk::MaskImage::SizeValueType i = 0; i < levelIter.Size(); i++)
        {
        // the center of the neighborhood is the voxel processed
        if (i == c) continue;
        bool isInBound;
        int tag = levelIter.GetPixel(i, isInBound);
        if(isInBound && tag && tag <= nextLevelTag)
          {
          IndexType idxNbr = levelIter.GetIndex(i);
          double w = this->ComputeDirectionWeight(it->first, idxNbr);
          if (w > 1.0e-5)
            {
            float vNbr[3];
            READ_VPIXEL(this->m_ImageComponent, idxNbr, vNbr);
#if defined(__TRACE_COMPUTEWALLCONDITION__)
            WSR2_DUMP_VELOCITY("adding ", vNbr) << " (w=" << w << ")"<<std::endl;
#endif
            cavt.AvgVelocity(vNbr);
            }
          }
        } // for ( levelIter )
      if (cavt.count)
        {
#if defined(__TRACE_COMPUTEWALLCONDITION__)
        WSR2_DUMP_VELOCITY("after averaging ", cavt.v) << std::endl;
#endif
        (*nextLevel)[it->first] = cavt;
        WRITE_VPIXEL(this->m_ImageComponent, it->first, cavt.v);
        this->m_ImageLevelMask->SetPixel(it->first, nextLevelTag);
        hasChanged = true;
        }
      else
        {
        std::cout << "unable to average " << it->first << std::endl;
        this->m_ImageLevelMask->SetPixel(it->first, 15);
        (*postAvg1)[it->first] = cavt;
        }
      }
    if (hasChanged && postAvg1->size())
      {
      std::cout << "going to revisit the list of unprocessed again\n";
      HashIndexType *tmp = postAvg1;
      postAvg1 = postAvg0;
      postAvg0 = tmp;
      }
    else
      {
      std::cout << "After post-processing, there are " << postAvg1->size() << " remaining voxels to process\n";
      break;
      }
    } while (1);
}

#else

void WallShearRateFunction2::ExtrapolateVelocityLayer(void *_currentLevel,
                                                      void *_nextLevel,
                                                      itk::MaskImage::PixelType nextLevelTag,
                                                      double tolDistance)
{
  std::cout << "Extrapolating velocity to layer " << int(nextLevelTag) << std::endl;

  HashIndexType *currentLevel = static_cast<HashIndexType*>(_currentLevel);
  HashIndexType *nextLevel = static_cast<HashIndexType*>(_nextLevel);

  nextLevel->clear();
  for(HashIndexType::iterator it = currentLevel->begin();
      it != currentLevel->end(); ++it)
    {
    ExtrapolateVelocityFromBoundaryVoxel(it->first, it->second.NormalInward,
                                         _nextLevel, nextLevelTag, true);
    }

 #if defined(__WSR_TRACE_RAY__) && !defined(PCMR_DEPLOY)
  std::cout << "Going to process nextLevel of size = " << nextLevel->size() << std::endl;
#endif

  size_t accepted;
  size_t rejected;
  HashIndexType currentHash_;
  HashIndexType nextHash_;
  HashIndexType *currentHash = nextLevel;
  HashIndexType *nextHash = &nextHash_;
  do
    {
    accepted = 0;
    rejected = 0;
    nextHash->clear();
    for(HashIndexType::iterator it = currentHash->begin();
        it != currentHash->end(); ++it)
      {
      double v[3];
      int status = this->ExtrapolateVelocityAtExteriorVoxel(it->first, nextLevelTag,
                                                            const_cast<const PointType::VectorType&>(it->second.NormalInward),
                                                            v);
      
      if (status == WSR_OK)
        {
        ++accepted;
        this->m_ImageLevelMask->SetPixel(it->first, nextLevelTag);
        
#if defined(__WSR_TRACE_RAY__) && !defined(PCMR_DEPLOY)
        std::cout << "going to average " << it->first;
        WSR2_DUMP_VELOCITY(" which has vel =  ", it->second.v) << " diff = " << fabs(it->second.v[1]-v[1]) << std::endl;
#endif
        it->second.AvgVelocity(v);
#if defined(__WSR_TRACE_RAY__) && !defined(PCMR_DEPLOY)
        WSR2_DUMP_VELOCITY(" after averaging vel =  ", it->second.v) << std::endl;
#endif
        WRITE_VPIXEL(this->m_ImageComponent, it->first, it->second.v);
        }
      else
        {
        ++rejected;
        //std::cout << "could not average " << it->first << " because of " << status << std::endl;
        (*nextHash)[it->first] = it->second;
        }
      }
    std::cout << "In layer " << int(nextLevelTag) 
              << " accepted = " << accepted 
              << " rejected = " << rejected << std::endl;
    if (rejected > 0 && accepted > 0)
      {
      HashIndexType *tmp = nextHash;
      nextHash = currentHash == nextLevel ? &currentHash_ : currentHash;
      currentHash = tmp;
      }
    else
      {
      break;
      }
    }
  while (1);
}
#endif

typedef itk::LevelSetNeighborhoodExtractor<itk::PhaseContrast3DImage> LevelSetExtractorType;
typedef LevelSetExtractorType::NodeContainer NodeContainerType;

StatusType WallShearRateFunction2::ComputeMaskBoundary(VectorIndexType &arrayInside, 
                                                       VectorIndexType &arrayOutside)
{
  arrayInside.clear();
  arrayOutside.clear();
  if (this->GetBoundaryLocator() == BLOCATOR_LEVELSET)
    {
    LevelSetExtractorType::Pointer extractor = LevelSetExtractorType::New();
    extractor->SetInputLevelSet(this->m_DistanceFilter->GetOutput());
    extractor->SetLevelSetValue(0);
    extractor->Locate();
    NodeContainerType::Pointer levelInside = extractor->GetInsidePoints();
    NodeContainerType::Pointer levelOutside = extractor->GetOutsidePoints();
    NodeContainerType::iterator nodeIt;
    arrayInside.reserve(levelInside->Size());
    arrayOutside.reserve(levelOutside->Size());
    for(nodeIt = levelInside->begin();
        nodeIt != levelInside->end(); 
        ++nodeIt)
      {
      arrayInside.push_back(nodeIt->GetIndex());
      }
    for(nodeIt = levelOutside->begin();
        nodeIt != levelOutside->end(); 
        ++nodeIt)
      {
      arrayOutside.push_back(nodeIt->GetIndex());
      }
    }
  else
    {
    if (!this->m_ImageMask)
      {
      return NO_MASK;
      }
    typedef itk::BinaryContourImageFilter<itk::MaskImage,
                                          itk::MaskImage> BorderFilterType;
    BorderFilterType::Pointer borderFilter = BorderFilterType::New();
    borderFilter->SetInput(this->m_ImageMask);
    borderFilter->SetForegroundValue(1);
    borderFilter->SetBackgroundValue(0);
    borderFilter->SetFullyConnected(true);
    borderFilter->Update();
    typedef itk::ImageRegionConstIteratorWithIndex<itk::MaskImage> MaskIteratorTpe;
    MaskIteratorTpe it(borderFilter->GetOutput(),
                       borderFilter->GetOutput()->GetLargestPossibleRegion());
    
    for(it.GoToBegin(); !it.IsAtEnd(); ++it)
      {
      if (it.Get())
        {
        arrayInside.push_back(it.GetIndex());
        }
      }
    }
  return OK;
}

StatusType WallShearRateFunction2::ComputeWallCondition()
{
  std::cout << "WallShearRateFunction2::ComputeWallCondition()\n";

  // extract level-set 0
  VectorIndexType arrayInside, arrayOutside;
  ComputeMaskBoundary(arrayInside, arrayOutside);
  
  LevelSetExtractorType::Pointer extractor = LevelSetExtractorType::New();
  std::cout << "Level-set 0 has " << arrayInside.size() << " nodes\n";
  std::cout << "Level-set outside has " << arrayOutside.size() << " nodes\n";

  if ( arrayInside.size() == 0 )
    {
    return NO_MASK;
    }
     
  HashIndexType hashVoxel0;
  HashIndexType hashVoxel1;
  HashIndexType *currentLevel = &hashVoxel0;
  HashIndexType *nextLevel = &hashVoxel1;
  
  typedef itk::ImageDuplicator<itk::MaskImage> MaskDuplicatorType;
  MaskDuplicatorType::Pointer dupMask = MaskDuplicatorType::New();
  dupMask->SetInputImage(this->m_ImageMask);
  dupMask->Update();
  // this image (this->m_ImageLevelMask) is to keep track of the level of voxels processed in
  // order. All outside voxels are marked as 0, as they come from a
  // mask.
  this->m_ImageLevelMask = dupMask->GetModifiableOutput();

  // propagate from the level-set 0 to the exterior
  VectorIndexType::iterator nodeIt;
  
#define __WSR_COMPUTE_RANGE_LEVELS__
#ifdef __WSR_COMPUTE_RANGE_LEVELS__
  double min0 = 1e+10, max0 = -1e-10;
  double min1 = 1e+10, max1 = -1e-10;
  double minv0 = -1.0, maxv0 = 0.0;
  int count0 = 0, count1 = 0;
  for(nodeIt = arrayInside.begin();
      nodeIt != arrayInside.end(); 
      ++nodeIt)
    {
    itk::PhaseContrast3DImage::IndexType index = *nodeIt;
    const itk::MaskImage::PixelType &l = this->m_ImageLevelMask->GetPixel(index);
    float v[4];
    READ_4DPIXEL(this->m_ImageComponent,index,v);
    count0 += (l>0);
    if (v[3] <= min0 )
      {
      min0 = v[3];
      }
    else if (v[3] >= max0)
      {
      max0 = v[3];
      }
    double _v = 0.0;
    for(int i = 0; i < 3; i++)
      {
      _v += v[i]*v[i];
      }
    if (minv0 < 0)
      {
      minv0 = _v;
      maxv0 = _v;
      }
    else if(_v < minv0)
      {
      minv0 = _v;
      }
    else if(_v > maxv0)
      {
      maxv0 = _v;
      }
    }
  if (max0 < min0)
    {
    max0 = min0;
    }
  for(nodeIt = arrayOutside.begin();
      nodeIt != arrayOutside.end(); 
      ++nodeIt)
    {
    itk::PhaseContrast3DImage::IndexType index = *nodeIt;
    const itk::MaskImage::PixelType &l = this->m_ImageLevelMask->GetPixel(index);
    float v[4];
    READ_4DPIXEL(this->m_ImageComponent,index,v);
    count1 += (l>0);
    if (v[3] <= min1)
      {
      min1 = v[3];
      }
    else if (v[3] >= max1)
      {
      max1 = v[3];
      } 
    }
  if (max1 < min1)
    {
    max1 = min1;
    }
  std::cout << "Level-Inside has " << count0 << " in mask\n";
  std::cout << "Level-Outside has " << count1 << " in mask\n";
  std::cout << "Range of distance for Level-Inside is [" << min0 << "," << max0 << "]\n";
  std::cout << "Range of distance for Level-Outside is [" << min1 << "," << max1 << "]\n";
  std::cout << "Range of velocity for Level-Inside is [" << sqrt(minv0) << "," << sqrt(maxv0) << "]\n";
#endif 

  // compute the minimun voxel spacing to test the near wall computation
  const SpacingType &spacing = this->m_ImageLevelMask->GetSpacing();
  const double minVoxelFactor = 0.1;
  double minVoxelSize = spacing[0];
  if (spacing[1] < minVoxelSize)
    {
    minVoxelSize = spacing[1];
    }
  else if (spacing[2] < minVoxelSize)
    {
    minVoxelSize = spacing[2];
    }
  const double tolDistance = minVoxelSize * minVoxelFactor;


  // initialize currentLevel with levelSet0
  for(nodeIt = arrayInside.begin();
      nodeIt != arrayInside.end(); 
      ++nodeIt)
    {
    itk::PhaseContrast3DImage::IndexType index = *nodeIt;
    CumulativeAverageType cavt;
    READ_4DPIXEL(this->m_ImageComponent, index, cavt.v);
    this->EvaluateNormalInward(index, cavt.NormalInward);
    (*currentLevel)[index] = cavt;
    this->m_ImageLevelMask->SetPixel(index, 2);
    }

  const int NumberOfLayers = 4;
  for (itk::MaskImage::PixelType tag = 3; tag < 3 + NumberOfLayers; ++tag)
    {
    this->ExtrapolateVelocityLayer(currentLevel, nextLevel, tag, tolDistance);
    // swap the levels
    HashIndexType *hashTmp = nextLevel;
    nextLevel = currentLevel;
    currentLevel = hashTmp;
    }
  if (this->m_LevelImageFileName != "")
    {
    WriteImage(this->m_ImageLevelMask.GetPointer(), this->m_LevelImageFileName.c_str());
    }  
  return OK;
}

bool WallShearRateFunction2::IsModified()
{
  return (this->m_InitializationTime < this->GetMTime());
}

// SetImageDataFromStudy
StatusType WallShearRateFunction2::SetImageDataFromStudy(Flow4DReader *dataSet, size_t timeStep)
{
  if (timeStep >= dataSet->GetNumberOfTimeSteps())
    {
    return INV_TIMESTEP;
    }
  typedef itk::VTKImageToImageFilter<itk::PhaseContrast3DImage> ImporterType;
  typedef itk::VTKImageToImageFilter<itk::MaskImage> MaskImporterType;
  ImporterType::Pointer importerX = ImporterType::New();
  ImporterType::Pointer importerY = ImporterType::New();
  ImporterType::Pointer importerZ = ImporterType::New();
  MaskImporterType::Pointer importerMask = MaskImporterType::New();
  vtkImageData *vtkImgX = dataSet->GetFlowImage(Flow4DReader::FlowX, timeStep);
  if (!vtkImgX)
    {
    return NO_FLOW_X;
    }
  vtkImageData *vtkImgY = dataSet->GetFlowImage(Flow4DReader::FlowY, timeStep);
  if (!vtkImgY)
    {
    return NO_FLOW_Y;
    }
  vtkImageData *vtkImgZ = dataSet->GetFlowImage(Flow4DReader::FlowZ, timeStep);
  if (!vtkImgZ)
    {
    return NO_FLOW_Z;
    }
  vtkImageData *vtkMask = dataSet->GetMaskImage(timeStep);
  importerX->SetInput(vtkImgX);
  importerY->SetInput(vtkImgY);
  importerZ->SetInput(vtkImgZ);
  importerX->Update();
  importerY->Update();
  importerZ->Update();
  this->SetComponentXImage(importerX->GetOutput());
  this->SetComponentYImage(importerY->GetOutput());
  this->SetComponentZImage(importerZ->GetOutput());
  if (vtkMask)
    {
    importerMask->SetInput(vtkMask);
    importerMask->Update();
    this->SetMaskImage(importerMask->GetOutput());
    }
  return OK;
}

// SetComponentXImage
void WallShearRateFunction2::SetComponentXImage(itk::PhaseContrast3DImage *grid)
{
  this->m_ImageComponentOriginal[X] = grid;
  this->Modified();
}

// SetComponentYImage
void WallShearRateFunction2::SetComponentYImage(itk::PhaseContrast3DImage *grid)
{
  this->m_ImageComponentOriginal[Y] = grid;
  this->Modified();
}

// SetComponentZImage
void WallShearRateFunction2::SetComponentZImage(itk::PhaseContrast3DImage *grid)
{
  this->m_ImageComponentOriginal[Z] = grid;
  this->Modified();
}

// SetMaskImage
void WallShearRateFunction2::SetMaskImage(itk::MaskImage *grid)
{
  this->m_ImageMask = grid;
  this->Modified();
}

StatusType WallShearRateFunction2::SetBoundaryMesh(vtkPolyData *mesh)
{
  if (mesh != this->m_BoundaryMesh)
    {
    this->m_BoundaryMesh = mesh;
    this->Modified();
    }
  return OK;
}

itkTriangleMeshType::Pointer CreateITKTriangleMesh(vtkPolyData *vtkMesh)
{
  typedef itk::TriangleCell<itkTriangleMeshType::CellType> TriangleType;
  vtkIdType *idPts = 0;
  vtkIdType nPts, cellId;
  vtkPoints *points = vtkMesh->GetPoints();

  itkTriangleMeshType::PointType pt;
  double _pt[3];
  itkTriangleMeshType::Pointer itkMesh = itkTriangleMeshType::New();

  for(vtkIdType i = 0; i < points->GetNumberOfPoints(); i++)
    {
    points->GetPoint(i, _pt);
    pt[0] = _pt[0];
    pt[1] = _pt[1];
    pt[2] = _pt[2];
    itkMesh->SetPoint(i, pt);    
    }
  
  vtkCellArray *inputPolys=vtkMesh->GetPolys();

  cellId = 0;
  inputPolys->InitTraversal();
  itkTriangleMeshType::CellType::CellAutoPointer cellPointer;
  while (inputPolys->GetNextCell(nPts, idPts))
    {
    if (nPts != 3)
      {
      continue;
      }
    assert(nPts==3);
    cellPointer.TakeOwnership(new TriangleType);
    for (int j = 0; j < 3; j++)
      {
      cellPointer->SetPointId(j, idPts[j]);
      }
    itkMesh->SetCell(cellId, cellPointer);
    ++cellId;
    }
  return itkMesh;
}

// see examples here
// /usr/local/src/Medical/Kitware/git/ITK/Modules/Core/Mesh/test/itkTriangleMeshToBinaryImageFilterTest*

StatusType WallShearRateFunction2::CreateRasterMaskFromBoundaryMesh()
{
  if (!this->m_BoundaryMesh)
    {
    itkExceptionMacro("Boundary mesh is NULL. Use method SetBoundaryMesh"); 
    }
  if (!this->m_ImageComponent[X])
    {
    itkExceptionMacro("Component X of flow is NULL. Use method SetComponentXImage or SetImageDataFromStudy.");    
    }
  this->m_ImageComponent[X]->Update();
  itkTriangleMeshType::Pointer tmpMesh = CreateITKTriangleMesh(this->m_BoundaryMesh);
  typedef itk::TriangleMeshToBinaryImageFilter<itkTriangleMeshType,itk::MaskImage> TriangleMeshToBinaryImageFilterType;
  typedef itk::RegionOfInterestImageFilter<itk::MaskImage, itk::MaskImage> RoiFilterType;

  TriangleMeshToBinaryImageFilterType::Pointer rasterFilter = TriangleMeshToBinaryImageFilterType::New();
  rasterFilter->SetInput(tmpMesh);
  const itk::MaskImage::PointType &origin0 = this->m_ImageComponent[X]->GetOrigin();
  const itk::MaskImage::SpacingType &spacing = this->m_ImageComponent[X]->GetSpacing();
  const itk::MaskImage::RegionType &region0 = this->m_ImageComponent[X]->GetLargestPossibleRegion();
  // enlarge de region n layers
  const int extraLayers = 10;
  itk::MaskImage::PointType origin;
  itk::MaskImage::SizeType size;
  for(int i = 0; i < 3; i ++)
    {
    origin[i] = origin0[i] - spacing[i] * extraLayers;
    size[i] = region0.GetSize()[i] + extraLayers*2;
    }
  rasterFilter->SetOrigin(origin);
  rasterFilter->SetSpacing(spacing);
  //rasterFilter->SetSize(region.GetSize());
  rasterFilter->SetSize(size);
  rasterFilter->SetInsideValue(1);
  rasterFilter->SetOutsideValue(0);

  // now recover the original region
  itk::MaskImage::IndexType desiredStart;
  desiredStart.Fill(extraLayers);
 
  itk::MaskImage::SizeType desiredSize(region0.GetSize());
 
  itk::MaskImage::RegionType desiredRegion(desiredStart, desiredSize);

  try
    {
    rasterFilter->Update();
    }
  catch( itk::ExceptionObject & excp )
    {
    std::cerr << "Error during Update() " << std::endl;
    std::cerr << excp << std::endl;
    return NO_MASK;
    }
  
  //rasterFilter->GetOutput()->Print(std::cout);
  //desiredRegion.Print(std::cout);

  RoiFilterType::Pointer roiFilter = RoiFilterType::New();
  roiFilter->SetRegionOfInterest(desiredRegion);
  roiFilter->SetInput(rasterFilter->GetOutput());
  try 
    {
    TRACE_MAIN_STEP("RasterFilter + ROIFilter", "START");
    //rasterFilter->Update();
    roiFilter->Update();
    TRACE_MAIN_STEP("RasterFilter + ROIFilter", "END");
    }
  catch( itk::ExceptionObject & err )
  {
    std::cerr << "ExceptionObject caught !" << std::endl;
    std::cerr << err << std::endl;
    return NO_MASK;
  }
  //this->m_ImageMask = rasterFilter->GetOutput();
  this->m_ImageRasterMask = roiFilter->GetOutput();

#if defined(__WSR_WRITE_RASTERIZED_IMAGE__) && !defined(PCMR_DEPLOY)
  WriteImage(roiFilter->GetOutput(), "/tmp/wsr_raster_mask.vtk");
#endif
  this->Modified();
  return OK;
};

END_PCMR_DECLS
