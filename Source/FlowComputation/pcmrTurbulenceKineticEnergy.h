#ifndef __pcmrTurbulenceKineticEnergy_h
#define __pcmrTurbulenceKineticEnergy_h

#include "pcmrFlowComputation_Export.h"
#include "pcmrFlow4DReader.h"

BEGIN_PCMR_DECLS

PCMRFLOWCOMPUTATION_EXPORT
StatusType ComputeTKE(Flow4DReader* DataSet, vtkImageData *OutputTKE);

END_PCMR_DECLS

#endif

