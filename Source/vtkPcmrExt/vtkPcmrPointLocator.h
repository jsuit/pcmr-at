/*=========================================================================
This source has no copyright.  It is intended to be copied by users
wishing to create their own VTK classes locally.
=========================================================================*/
// .NAME  - Example class using VTK.
// .SECTION Description
// vtkPcmrImagePlaneWidget is a simple class that uses VTK.  This class can be
// copied and modified to produce your own classes.

#ifndef __vtkPcmrPointLocator_h
#define __vtkPcmrPointLocator_h

#include "vtkPcmrExtModule.h" // export macro
#include "vtkPointLocator.h"

class vtkImageData;

class VTKPCMREXT_EXPORT vtkPcmrPointLocator : public vtkPointLocator
{
public:
  static vtkPcmrPointLocator* New();
  vtkTypeMacro(vtkPcmrPointLocator, vtkPointLocator);
  void PrintSelf(ostream& os, vtkIndent indent);

  void GenerateBinaryRepresentation( vtkImageData *image );

protected:
  vtkPcmrPointLocator();
  ~vtkPcmrPointLocator();

private:
  vtkPcmrPointLocator(const vtkPcmrPointLocator&);  // Not implemented.
  void operator=(const vtkPcmrPointLocator&);  // Not implemented.
};

#endif
