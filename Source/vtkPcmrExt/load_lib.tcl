#lappend auto_path /usr/local/vtk-gitlab/lib/tcltk/vtk-7.1
package require vtk
package require vtkPcmrExtTCL

#set cwd [file dir [info script]]
#load [file join $cwd build libvtkPcmrExtTCL-7.1.so]

vtkPcmrImagePlaneWidget pwidget
puts "PlaneOrientation = [pwidget GetPlaneOrientation]"

vtkImageGaussianSource isrc
isrc Update

pwidget SetInputConnection [isrc GetOutputPort ]
pwidget SetInputData [isrc GetOutput ]
exit

pwidget SetPlaneOrientation 2

vtkSphereSource sph
sph Update

vtkPcmrPointLocator bin
bin SetDataSet [sph GetOutput]
bin AutomaticOff
bin BuildLocator
vtkImageData id
bin GenerateBinaryRepresentation id
puts [id Print]
exit
