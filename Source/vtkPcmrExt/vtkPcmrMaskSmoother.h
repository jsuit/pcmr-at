/*=========================================================================
This source has no copyright.  It is intended to be copied by users
wishing to create their own VTK classes locally.
=========================================================================*/
// .NAME  - Example class using VTK.
// .SECTION Description

// vtkPcmrMaskSmoother is a class which implements the
// segmentation pipeline based on a sampling of the flow.

#ifndef __vtkPcmrMaskSmoother_h
#define __vtkPcmrMaskSmoother_h

#include "vtkPcmrExtModule.h" // export macro
#include "vtkObject.h"
#include "vtkSmartPointer.h"

class vtkImageData;
class vtkPolyData;

class VTKPCMREXT_EXPORT vtkPcmrMaskSmoother : public vtkObject
{
public:
  static vtkPcmrMaskSmoother* New();
  vtkTypeMacro(vtkPcmrMaskSmoother, vtkObject);
  void PrintSelf(ostream& os, vtkIndent indent);
  
  void SetInputMask( vtkImageData *mask );
  vtkImageData * GetInputMask();
  vtkPolyData * GetBoundarySmoothed();
  void SetPadding( int pad_i, int pad_j, int pad_k );
  int * GetPadding();

protected:
  vtkPcmrMaskSmoother();
  ~vtkPcmrMaskSmoother();

  void ComputeBoundarySmoothed();

  vtkImageData *m_Mask;
  vtkSmartPointer<vtkPolyData> m_BoundarySmoothedFinal;
  int m_Padding[3];
private:
  vtkPcmrMaskSmoother(const vtkPcmrMaskSmoother &);  // Not implemented.
  void operator=(const vtkPcmrMaskSmoother &);  // Not implemented.
};

#endif
