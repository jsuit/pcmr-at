/*=========================================================================
This source has no copyright.  It is intended to be copied by users
wishing to create their own VTK classes locally.
=========================================================================*/
// .NAME  - Example class using VTK.
// .SECTION Description
// vtkPcmrWrapper is a simple class to wrapp some VTK method which are not
// wrapped because some arguments are not wrapped. This class can be
// copied and modified to produce your own classes.

#ifndef __vtkPcmrWrapper_h
#define __vtkPcmrWrapper_h

#include "vtkPcmrExtModule.h" // export macro
#include "vtkObject.h"

class vtkParametricSpline;
class vtkAxis;

class VTKPCMREXT_EXPORT vtkPcmrWrapper : public vtkObject
{
public:
  static vtkPcmrWrapper* New();
  vtkTypeMacro(vtkPcmrWrapper, vtkObject);
  void PrintSelf(ostream& os, vtkIndent indent);

  void AxisSetTitle( vtkAxis* axis, const char *title );
  
  void ParametricSplineProjectOn( vtkParametricSpline *spline, 
				  double ox, double oy, double oz,
				  double nx, double ny, double nz );
protected:
  vtkPcmrWrapper();
  ~vtkPcmrWrapper();

private:
  vtkPcmrWrapper(const vtkPcmrWrapper&);  // Not implemented.
  void operator=(const vtkPcmrWrapper&);  // Not implemented.
};

#endif
