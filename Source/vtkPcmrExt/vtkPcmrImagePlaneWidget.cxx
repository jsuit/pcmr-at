/*=========================================================================
This source has no copyright.  It is intended to be copied by users
wishing to create their own VTK classes locally.
=========================================================================*/
#include "vtkPcmrImagePlaneWidget.h"
#include "vtkAlgorithmOutput.h"
#include "vtkImageData.h"
#include "vtkImageMapToColors.h"
#include "vtkImageReslice.h"
#include "vtkLookupTable.h"
#include "vtkTexture.h"

#include "vtkObjectFactory.h"

vtkStandardNewMacro(vtkPcmrImagePlaneWidget);

//----------------------------------------------------------------------------
vtkPcmrImagePlaneWidget::vtkPcmrImagePlaneWidget() : vtkImagePlaneWidget()
{
}

//----------------------------------------------------------------------------
vtkPcmrImagePlaneWidget::~vtkPcmrImagePlaneWidget()
{
}

//----------------------------------------------------------------------------
void vtkPcmrImagePlaneWidget::SetInputConnection(vtkAlgorithmOutput* aout)
{
  //std::cout << "vtkPcmrImagePlaneWidget::SetInputConnection\n";
  this->vtkPolyDataSourceWidget::SetInputConnection(aout);

  this->ImageData = vtkImageData::SafeDownCast(
    aout->GetProducer()->GetOutputDataObject(
      aout->GetIndex()));

  if( !this->ImageData )
    {
    // If NULL is passed, remove any reference that Reslice had
    // on the old ImageData
    //
    this->Reslice->SetInputData(NULL);
    return;
    }

  double range[2];
  this->ImageData->GetScalarRange(range);

  if ( !this->UserControlledLookupTable )
    {
    this->LookupTable->SetTableRange(range[0],range[1]);
    this->LookupTable->Build();
    }

  this->OriginalWindow = range[1] - range[0];
  this->OriginalLevel = 0.5*(range[0] + range[1]);

  if( fabs( this->OriginalWindow ) < 0.001 )
    {
    this->OriginalWindow = 0.001 * ( this->OriginalWindow < 0.0 ? -1 : 1 );
    }
  if( fabs( this->OriginalLevel ) < 0.001 )
   {
   this->OriginalLevel = 0.001 * ( this->OriginalLevel < 0.0 ? -1 : 1 );
   }

  this->SetWindowLevel(this->OriginalWindow,this->OriginalLevel);

  this->Reslice->SetInputConnection(aout);
  int interpolate = this->ResliceInterpolate;
  this->ResliceInterpolate = -1; // Force change
  this->SetResliceInterpolate(interpolate);

  this->ColorMap->SetInputConnection(this->Reslice->GetOutputPort());

  this->Texture->SetInputConnection(this->ColorMap->GetOutputPort());
  this->Texture->SetInterpolate(this->TextureInterpolate);

  //this->SetPlaneOrientation(this->PlaneOrientation);
}

//----------------------------------------------------------------------------
void vtkPcmrImagePlaneWidget::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}
