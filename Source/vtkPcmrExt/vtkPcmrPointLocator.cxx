/*=========================================================================
This source has no copyright.  It is intended to be copied by users
wishing to create their own VTK classes locally.
=========================================================================*/
#include "vtkPcmrPointLocator.h"

#include "vtkImageData.h"

#include "vtkObjectFactory.h"

vtkStandardNewMacro(vtkPcmrPointLocator);

//----------------------------------------------------------------------------
vtkPcmrPointLocator::vtkPcmrPointLocator() : vtkPointLocator()
{
}

//----------------------------------------------------------------------------
vtkPcmrPointLocator::~vtkPcmrPointLocator()
{
}

// Build the binary mask representation of locator.
void vtkPcmrPointLocator::GenerateBinaryRepresentation( vtkImageData *image )
{
  int ii, i, j, k, idx, offset[3], sliceSize;
  image->SetDimensions( this->Divisions );
  image->SetOrigin( this->Bounds[0], this->Bounds[2], this->Bounds[4] );
  image->SetSpacing( this->H );
  image->AllocateScalars( VTK_UNSIGNED_CHAR, 1 );

  sliceSize = this->Divisions[0] * this->Divisions[1];
  for ( k=0; k < this->Divisions[2]; k++)
    {
    offset[2] = k * sliceSize;
    for ( j=0; j < this->Divisions[1]; j++)
      {
      offset[1] = j * this->Divisions[0];
      for ( i=0; i < this->Divisions[0]; i++)
        {
        offset[0] = i;
        idx = offset[0] + offset[1] + offset[2];
        unsigned char* voxel = static_cast<unsigned char*>(image->GetScalarPointer(i,j,k));        if ( this->HashTable[idx] == NULL )
          {
          //inside = 0;
          *voxel = 0;
          }
        else
          {
          //inside = 1;
          *voxel = 1;
          }
        }
      }
    }
}

//----------------------------------------------------------------------------
void vtkPcmrPointLocator::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}
