#if !defined( __chebyshev_h__ )
#define __chebyshev_h__

#include "nr3.h"

struct Chebyshev {
	Int n,m;
	VecDoub c;
	Doub a,b;

	Chebyshev(Doub func(Doub), Doub aa, Doub bb, Int nn);
	Chebyshev(VecDoub &cc, Doub aa, Doub bb)
		: n(cc.size()), m(n), c(cc), a(aa), b(bb) {}
	Int setm(Doub thresh) {while (m>1 && abs(c[m-1])<thresh) m--; return m;}
	
	Doub eval(Doub x, Int m);
	inline Doub operator() (Doub x) {return eval(x,m);}
	
	Chebyshev derivative();
	Chebyshev integral();

	VecDoub polycofs(Int m);
	inline VecDoub polycofs() {return polycofs(m);}
	Chebyshev(VecDoub &pc);
	
};

#endif
