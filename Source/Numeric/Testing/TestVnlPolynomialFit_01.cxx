#include "pcmrPolynomialFit.h"
#include <iostream>

int main(int argc, char *argv[])
{
  /*
  double data_x[5] = { 3.53862,   3.97765,  7.51627, 10.8693, 11.4939};
  double data_y[5] = { -41.9883,  -46.6048, -42.7943, -42.2081, -44.1133};
  */
  //-0(0.499965)[23.4375], -0(0.99993)[43.75],
  //--0(1.49989)[],-0(1.99986)[75], -0(2.49982)[85.9375] nsample=5 ==>
  //--5.59265e-13
  /*
  double data_x[5] = { 0.499965,   0.99993,  1.49989, 1.99986, 2.49982};
  double data_y[5] = { 23.4375,    43.75,    60.9375, 75,      85.9375};
  */

  // -0.0940918(0)[4.6875], -0.797913(0.704768)[35.9375], -1.49933(1.40954)[60.9375], -1.87769(1.79053)[71.875], -2.58476(2.4953)[87.5]
  
  
  double data_x[] = { 0,      0.7047683,  1.40954, 1.79053, 2.4953};
  double data_y[] = { 4.6875, 35.9375,    60.9375, 71.875,  87.5};
  pcmr::VnlVector x(data_x, sizeof(data_x)/sizeof(data_x[0]));
  pcmr::VnlVector y(data_y, sizeof(data_y)/sizeof(data_y[0]));
  pcmr::VnlPolynomial polFit(2);

  pcmr::PolynomialFit(x, y, 2, polFit);
  std::cout << polFit.coefficients() << std::endl;
  std::cout << polFit.evaluate(-0.5) << std::endl;
  return 0;
}
