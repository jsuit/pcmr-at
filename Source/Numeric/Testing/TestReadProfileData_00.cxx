#include "pcmrProfileData.h"
#include <iostream>

int main( int argc, char *argv[] )
{
  double *x = NULL;
  double *y = NULL;
  std::string header;

  if( argc != 2 )
    {
    std::cout << "usage: " << argv[0] << " profile.txt\n";
    return -1;
    }

  int n = ReadProfileData( argv[1], x, y, header );
  std::cout << header << std::endl;
  std::cout << n << " points where read\n";
  ReleaseProfileData( x, y );
  return 0;
}
