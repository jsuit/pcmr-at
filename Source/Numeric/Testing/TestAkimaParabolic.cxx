#include "pcmrAkima.h"
#include <iostream>
#include <math.h>

int main(int argc, const char *argv[])
{
  double delta_x = 0.1;
  double radius = 10;
  double min_x = -radius;
  double v0 = 100;

  const size_t N = 10;
  double data_x[N];
  double data_y[N];
  
  for (size_t i = 0; i < N; i++)
    {
    data_x[i] = -radius + i*delta_x;
    data_y[i] = v0*(1-pow(fabs(data_x[i])/radius,2));
    }

  pcmr::Akima akima;

  akima.SetKnots(data_x, data_y, N);
  std::cout << "X;FX\n";
  
  const double M = N + 5;
  for(size_t i = 0; i < M; ++i)
    {
    double x = -radius - delta_x*(M-N) + delta_x*i;
    double fx;
    pcmr::FitErrorCode status = akima.Evaluate(x, fx);
    if ( status == pcmr::ERROR_OK )
      {
      std::cout << x << ";" << fx << std::endl;
      }
    }
  return 0;
}
