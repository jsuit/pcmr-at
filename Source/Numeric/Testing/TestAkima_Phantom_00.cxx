#include "pcmrAkima.h"
#include <iostream>
#include "pcmrPolynomialFit.h"

double AkimaMape( pcmr::Akima &akima, double x[], double y[], size_t size )
{
  double mape = 0.0;
  for( size_t i = 0; i < size; i++ )
    {
    double fx;
    akima.Evaluate( x[i], fx );
    mape += fabs( fx - y[i] ) / y[i];
    }
  return mape/size;
}

double FitMape( pcmr::VnlPolynomial &fit, double x[], double y[], size_t size )
{
  double mape = 0.0;
  for( size_t i = 0; i < size; i++ )
    {
    double fx = fit.evaluate( x[i] );
    mape += fabs( fx - y[i] ) / y[i];
    }
  return mape/size;
}

/*
======================================================
Extrapolating at  = [9, 11, 49] == [-49.4997, -5.99977, -138.5] tag = (0) vel = [-0.0016399,0.00868544,6.78132,0.326609]
BuildPolynomialInward at[9, 11, 49]  vel = [-0.0016399,0.00868544,6.78132,0.326609]
-1.74669(2.0733)[35.4115]{34.2766}, -2.99418(3.33375)[48.3595]{47.1327}, -5.08044(5.40705)[51.1076]{51.1342}, -7.13442(7.48034)[51.2212]{51.2123}, -8.36617(8.7408)[51.0194]{50.9968}, -10.4824(10.8141)[50.5007]{50.5171}
x = 2.0733 3.33375 5.40705 7.48034 8.7408 10.8141
y = 34.2766 47.1327 51.1342 51.2123 50.9968 50.5171
poly = 0.122483 -2.87007 21.2015 2.25209
vel_interp = [-0.0456898,-0.065246,2.25209]
======================================================
Extrapolating at  = [21, 11, 49] == [-31.4997, -5.99977, -138.5] tag = (0) vel = [-0.00788184,0.0136688,5.12825,0.321705]
BuildPolynomialInward at[21, 11, 49]  vel = [-0.00788184,0.0136688,5.12825,0.321705]
-1.74192(2.0934)[33.88]{32.995}, -2.98992(3.31162)[45.4908]{44.2375}, -5.08331(5.40502)[50.5882]{50.4548}, -7.13542(7.49841)[50.912]{50.9405}, -8.36636(8.71664)[50.9154]{50.714}, -10.4824(10.81)[50.5007]{50.3986}
x = 2.0934 3.31162 5.40502 7.49841 8.71664 10.81
y = 32.995 44.2375 50.4548 50.9405 50.714 50.3986
poly = 0.105612 -2.55551 19.7527 2.23276
vel_interp = [-0.0189227,0.00704903,2.23276]
======================================================

*/
int main(int argc, const char *argv[])
{
  double data_x[] = { 0, 2.0934, 3.31162, 5.40502, 7.49841, 8.71664, 10.81 };
  double data_y[] = { 0, 32.995, 44.2375, 50.4548, 50.9405, 50.714, 50.3986};

  double data_x_r[] = { 2.0934, 2.0934, 3.31162, 3.31162, 5.40502, 7.49841, 8.71664, 10.81 };
  double data_y_r[] = { 32.995, 32.995, 44.2375, 44.2375, 50.4548, 50.9405, 50.714, 50.3986};

  pcmr::Akima akima;

  akima.SetKnots( data_x+1, data_y+1, 5 );
  double fx;
  pcmr::FitErrorCode status = akima.Evaluate( 0.0, fx );
  std::cout << "status = " << status << std::endl;
  if (status == pcmr::ERROR_OK)
    {
    std::cout << "f(0.0) = " << fx << std::endl;
    akima.Evaluate( 0.1, fx );
    std::cout << "f(0.1) = " << fx << std::endl;
    }
  std::cout << "MAPE(Akima) = " << AkimaMape( akima, data_x+1, data_y+1, 5 ) << std::endl;

  pcmr::VnlVector x(data_x+1, sizeof(data_x)/sizeof(data_x[0])-1);
  pcmr::VnlVector y(data_y+1, sizeof(data_y)/sizeof(data_y[0])-1);
  pcmr::VnlVector x_r(data_x_r, sizeof(data_x_r)/sizeof(data_x_r[0]));
  pcmr::VnlVector y_r(data_y_r, sizeof(data_y_r)/sizeof(data_y_r[0]));
  std::cout << "x = " << x << std::endl;
  std::cout << "y = " << y << std::endl;
  pcmr::VnlPolynomial polFit( 3 );

  pcmr::PolynomialFit( x, y, 2, polFit );
  std::cout << polFit.coefficients( ) << std::endl;
  std::cout << "MAPE(Fit) = " << FitMape( polFit, data_x+1, data_y+1, sizeof(data_x)/sizeof(data_x[0])-1 ) << std::endl;
  const double eps = 0.1;
  std::cout << "polFit( " << x[0] << " ) = " << polFit.evaluate( x[0] ) << std::endl;
  std::cout << "polFit( " << x[0]+eps << " ) = " << polFit.evaluate( x[0]+eps ) << std::endl;
  std::cout << "polFit( " << x[0]-eps << " ) = " << polFit.evaluate( x[0]-eps ) << std::endl;
  std::cout << "polFit( 0.5 ) = " << polFit.evaluate( 0.5 ) << std::endl;
  std::cout << "polFit( 0.0 ) = " << polFit.evaluate( 0.0 ) << std::endl;
  std::cout << "polFit( -0.5 ) = " << polFit.evaluate( -0.5 ) << std::endl;
  std::cout << "polFit( -1.0 ) = " << polFit.evaluate( -1.0 ) << std::endl;
  double m = polFit.devaluate( x[0] );
  double b = y[0] - m * x[0];
  std::cout << "m = " << m << std::endl;
  std::cout << "b = " << b << std::endl;
  data_x[0] = -0.5;
  data_y[0] = -0.5*m + b;

  pcmr::VnlVector x_all(data_x, sizeof(data_x)/sizeof(data_x[0])-2);
  pcmr::VnlVector y_all(data_y, sizeof(data_y)/sizeof(data_y[0])-2);
  std::cout << "x_all = " << x_all << std::endl;
  std::cout << "y_all = " << y_all << std::endl;
  pcmr::VnlPolynomial polFit_all( 3 );
  pcmr::PolynomialFit( x_all, y_all, 3, polFit_all );
  std::cout << polFit_all.coefficients( ) << std::endl;
  std::cout << "polFit_all( 0.0 ) = " << polFit_all.evaluate( 0.0 ) << std::endl;

  return 0;
}
