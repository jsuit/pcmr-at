#include <iostream>
#include "pcmrCurveFit.h"

double Mape( pcmr::CurveFit &curve, double x[], double y[], size_t size )
{
  double mape = 0.0;
  for( size_t i = 0; i < size; i++ )
    {
    double fx = curve.Evaluate( x[i] );
    mape += fabs( fx - y[i] ) / y[i];
    }
  return mape/size;
}

/*
======================================================
Extrapolating at  = [9, 11, 49] == [-49.4997, -5.99977, -138.5] tag = (0) vel = [-0.0016399,0.00868544,6.78132,0.326609]
BuildPolynomialInward at[9, 11, 49]  vel = [-0.0016399,0.00868544,6.78132,0.326609]
-1.74669(2.0733)[35.4115]{34.2766}, -2.99418(3.33375)[48.3595]{47.1327}, -5.08044(5.40705)[51.1076]{51.1342}, -7.13442(7.48034)[51.2212]{51.2123}, -8.36617(8.7408)[51.0194]{50.9968}, -10.4824(10.8141)[50.5007]{50.5171}
x = 2.0733 3.33375 5.40705 7.48034 8.7408 10.8141
y = 34.2766 47.1327 51.1342 51.2123 50.9968 50.5171
poly = 0.122483 -2.87007 21.2015 2.25209
vel_interp = [-0.0456898,-0.065246,2.25209]
======================================================
Extrapolating at  = [21, 11, 49] == [-31.4997, -5.99977, -138.5] tag = (0) vel = [-0.00788184,0.0136688,5.12825,0.321705]
BuildPolynomialInward at[21, 11, 49]  vel = [-0.00788184,0.0136688,5.12825,0.321705]
-1.74192(2.0934)[33.88]{32.995}, -2.98992(3.31162)[45.4908]{44.2375}, -5.08331(5.40502)[50.5882]{50.4548}, -7.13542(7.49841)[50.912]{50.9405}, -8.36636(8.71664)[50.9154]{50.714}, -10.4824(10.81)[50.5007]{50.3986}
x = 2.0934 3.31162 5.40502 7.49841 8.71664 10.81
y = 32.995 44.2375 50.4548 50.9405 50.714 50.3986
poly = 0.105612 -2.55551 19.7527 2.23276
vel_interp = [-0.0189227,0.00704903,2.23276]
======================================================

*/
int main(int argc, const char *argv[])
{
  double data_x[] = { 0, 2.0934, 3.31162, 5.40502, 7.49841, 8.71664, 10.81 };
  double data_y[] = { 0, 32.995, 44.2375, 50.4548, 50.9405, 50.714, 50.3986};

  pcmr::CurveAkimaFit akima;

  pcmr::FitErrorCode status = akima.SetData( data_x+1, data_y+1, 5 );
  if (status != pcmr::ERROR_OK)
    {
    std::cout << "error in akima.SetData " << status << std::endl;
    }
  else
    {
    std::cout << "f(0.0) = " << akima.Evaluate( 0.0 ) << std::endl;
    std::cout << "f(0.1) = " << akima.Evaluate( 0.1 ) << std::endl;
    }

  std::cout << "MAPE(Akima) = " << Mape( akima, data_x+1, data_y+1, 5 ) 
            << std::endl;

        
  pcmr::CurvePolynomialFit polyFit;

  status = polyFit.SetData( data_x + 1, data_y + 1,
                            sizeof( data_x ) / sizeof( data_x[ 0 ] )-1 );
  if (status != pcmr::ERROR_OK)
    {
    std::cout << "error in polyFit.SetData " << status << std::endl;
    }
  else
    {
    polyFit.Print( std::cout );
    std::cout << "f(0.0) = " << polyFit.Evaluate( 0.0 ) << std::endl;
    std::cout << "f(0.1) = " << polyFit.Evaluate( 0.1 ) << std::endl;
    }

  return 0;
}
