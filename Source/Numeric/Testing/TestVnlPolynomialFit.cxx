#include "vnl/vnl_fastops.h"
#include "vnl/vnl_matrix.h"
#include "vnl/vnl_vector.h"
#include "vnl/algo/vnl_cholesky.h"

#include <iostream>

int main(int argc, char *argv[])
{
  double data_x[5] = { 3.53862,   3.97765,  7.51627, 10.8693, 11.4939};
  double data_y[5] = { -41.9883,  -46.6048, -42.7943, -42.2081, -44.1133};

  vnl_vector<double> cursor(5);
  cursor.fill(1);
  
  const int deg = 3;
  vnl_matrix<double> X(5, deg+1);
  
  for(int i = 0; i <= deg; i++)
    {
    X.set_column(i, cursor);
    if (i < deg)
      {
      for(int j = 0; j < 5; j++)
        {
        cursor[j] *= data_x[j];
        }
      }
    }
  vnl_matrix<double> XtX(deg+1, deg+1);
  vnl_fastops::AtA(XtX, X);

  std::cout << X << std::endl;
  std::cout << XtX << std::endl;

  vnl_cholesky chol(XtX);

  vnl_vector<double> b(data_y, 5);
  vnl_vector<double> b1(deg+1);
  vnl_fastops::AtB(b1, X, b);
  
  vnl_vector<double> sol = chol.solve(b1);

  std::cout << "sol = " << sol << std::endl;
  return 0;
}
