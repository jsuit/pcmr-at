#include "pcmrProfileData.h"
#include <boost/filesystem.hpp>
using namespace boost::filesystem;
#include <iostream>
#include <fstream>
#include <sstream>

size_t ReadHeader( std::ifstream &infile, std::string &header )
{
  std::string line;
  while (std::getline(infile, line))
    {
    if( line[0] == '#' )
      {
      header += "\n";
      header += line;
      }
    else
      {
      std::istringstream iss(line);
      size_t n;
      if ( !( iss >> n ) ) 
        {
        return 0;
        }
      return n;
      }
    }
  return 0;
}

int ReadData( std::ifstream &infile, double *&x, double *&y, size_t n)
{
  std::string line;
  size_t i;
  x = new double[n];
  y = new double[n];
  for( i = 0; i < n; i++ )
    {
    if ( std::getline( infile, line ) )
      {
      std::istringstream iss(line);
      // std::cout << "line = " << line << std::endl;
      double a, b;
      if (!(iss >> a >> b)) 
        {
        break; 
        } // error
      // process pair (a,b)
      x[i] = a;
      y[i] = b;
      }
    }
  if ( i < n )
    {
    delete []x;
    delete []y;
    return 0;
    }
  return i;
}

int ReadProfileData( const char* FilePath, double *&x, double *&y, std::string &header )
{
  path root( PROFILE_ROOT_PATH );
  path pathFile0( FilePath);
  path pathFile;
  
  if( pathFile0.is_absolute( ) )
    {
    pathFile = pathFile0;
    }
  else
    {
    pathFile = root;
    pathFile /= FilePath;
    }
  if( !exists( pathFile ) )
    {
    std::cout << "File " << pathFile << " does not exists. Could not read profile\n";
    return 0;
    }
  std::cout << "Reading profile from file " << pathFile << std::endl;
  std::ifstream infile( pathFile.c_str( ) );
  if( !infile )
    {
    return 0;
    }
  size_t n = ReadHeader( infile, header );
  return ReadData( infile, x, y, n );
}

void ReleaseProfileData( double *x, double *y )
{
  if( x )
    {
    delete []x;
    }
  if( y )
    {
    delete []y;
    }
}
