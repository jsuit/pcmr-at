#ifndef __pcmrCurveFit_h
#define __pcmrCurveFit_h

#include "pcmrFitErrorCodes.h"
#include "Numeric_Export.h"
#include "pcmrAkima.h"
#include "pcmrPolynomialFit.h"
#include "NaturalSpline.h"
#include <iostream>

BEGIN_PCMR_DECLS

class PCMRNUMERIC_EXPORT CurveFit
{
 public:

  CurveFit( );

  virtual ~CurveFit( );

  virtual void SetDegree( size_t degree );

  size_t GetDegree( )
  {
    return this->m_Degree;
  }

  virtual FitErrorCode SetData( const double x[], const double y[], size_t size ) = 0;
  virtual double Evaluate( double x ) = 0;
  virtual void Print( std::ostream & out );

protected:
  size_t m_Degree;
};

class PCMRNUMERIC_EXPORT CurveAkimaFit : public CurveFit
{
 public:
  CurveAkimaFit();
  virtual ~CurveAkimaFit();
  virtual void SetDegree( size_t degree );
  virtual FitErrorCode SetData( const double x[], const double y[], size_t size );
  virtual double Evaluate( double x );
  virtual void Print( std::ostream & out );

 private:
  Akima m_Akima;
  
};

class PCMRNUMERIC_EXPORT CurvePolynomialFit : public CurveFit
{
public:
  CurvePolynomialFit();
  virtual ~CurvePolynomialFit();
  virtual FitErrorCode SetData( const double x[], const double y[], size_t size )    ;
  virtual double Evaluate( double  );
  virtual void Print( std::ostream & out );

private:
  VnlPolynomial m_Polynomial;
};

class PCMRNUMERIC_EXPORT CurveNaturalSplineFit : public CurveFit
{
public:
  CurveNaturalSplineFit();
  virtual ~CurveNaturalSplineFit();
  virtual void SetDegree( size_t degree );
  virtual FitErrorCode SetData( const double x[], const double y[], size_t size )    ;
  virtual double Evaluate( double x );
  virtual void Print( std::ostream & out );

private:
  NaturalSpline::spline m_Spline;
};
END_PCMR_DECLS

#endif
