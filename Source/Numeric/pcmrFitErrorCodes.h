#ifndef __pcmrFitErrorCodes_h__
#define __pcmrFitErrorCodes_h__

#include "pcmrDefs.h"

BEGIN_PCMR_DECLS

typedef enum 
  {
    ERROR_OK = 0,
    ERROR_ALLOC,
    ERROR_MINSIZE,
    ERROR_INCREASING,
    ERROR_NODATA
  } FitErrorCode;

END_PCMR_DECLS

#endif
