#include "pcmrCurveFit.h"
#include <iostream>

BEGIN_PCMR_DECLS

// CurveFit

CurveFit::CurveFit( )
  : m_Degree( 3 )
{
}

CurveFit::~CurveFit( )
{
}

void CurveFit::SetDegree( size_t degree )
{
  this->m_Degree = degree;
}

void CurveFit::Print( std::ostream & out )
{
  out << "CurveFit: " << this << std::endl;
}

// CurveAkimaFit

CurveAkimaFit::CurveAkimaFit( )
{
  this->m_Degree = 3;
}

CurveAkimaFit::~CurveAkimaFit( )
{
}

void CurveAkimaFit::SetDegree( size_t degree )
{
  // std::cout << "The degree for Akima can not be changed, it is fixed to 3\n";
  this->m_Degree = 3;
}

FitErrorCode CurveAkimaFit::SetData( const double x[], const double y[], size_t size )
{
  return this->m_Akima.SetKnots( x, y, size );
}

double CurveAkimaFit::Evaluate( double x )
{
  double fx;
  
  this->m_Akima.Evaluate( x, fx );
  return fx;
}

void CurveAkimaFit::Print( std::ostream & out )
{
  out << "CurveAkimaFit: " << this << std::endl;
}


// CurvePolynomialFit

CurvePolynomialFit::CurvePolynomialFit( )
  : m_Polynomial( 3 )
{
}

CurvePolynomialFit::~CurvePolynomialFit( )
{
}

FitErrorCode CurvePolynomialFit::SetData( const double x[], const double y[], size_t size )
{
  if ( size <= this->GetDegree( ) )
    {
    return ERROR_MINSIZE;
    }
  pcmr::VnlVector x_vector( x, size );
  pcmr::VnlVector y_vector( y, size );

  pcmr::PolynomialFit( x_vector, y_vector, this->GetDegree( ), this->m_Polynomial );
  return ERROR_OK;
}

double CurvePolynomialFit::Evaluate( double x )
{
  return this->m_Polynomial.evaluate( x );
}

void CurvePolynomialFit::Print( std::ostream & out )
{
  out << "CurvePolynomialFit: " << this->m_Polynomial.coefficients( ) << std::endl;
}

CurveNaturalSplineFit::CurveNaturalSplineFit()
{
}

CurveNaturalSplineFit::~CurveNaturalSplineFit()
{
}

void CurveNaturalSplineFit::SetDegree( size_t degree )
{
  // std::cout << "The degree for NaturalSpline can not be changed, it is fixed to 3\n";
  this->m_Degree = 3;
}

FitErrorCode CurveNaturalSplineFit::SetData( const double x[], const double y[], size_t size )
{
  std::vector<double> _x;
  std::vector<double> _y;
  
  for( int i = 0; i < size; i++ )
    {
    _x.push_back( x[i] );
    _y.push_back( y[i] );
    }
  this->m_Spline.set_points( _x, _y );
  return ERROR_OK;
}

double CurveNaturalSplineFit::Evaluate( double x )
{
  return this->m_Spline( x );
}

void CurveNaturalSplineFit::Print( std::ostream & out )
{
}


END_PCMR_DECLS
