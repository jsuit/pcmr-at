#include "pcmrAkima.h"
#include <math.h>

BEGIN_PCMR_DECLS

Akima::Akima()
  : m_Size(0), m_Knots(NULL), m_Coefficients(NULL)
{
}

Akima::~Akima()
{
  this->Clear();
}

void Akima::Clear()
{
  if (this->m_Knots)
    {
    delete []this->m_Knots;
    this->m_Knots = NULL;
    }
  if (this->m_Coefficients)
    {
    delete []this->m_Coefficients;
    this->m_Coefficients = NULL;
    }
  this->m_Size = 0;
}

FitErrorCode Akima::SetKnots(const double x[], const double fx[], size_t size)
{
  this->Clear();
  if (size < 5)
    {
    return ERROR_MINSIZE;
    }
  this->m_Coefficients = new Coefficient[size];
  if (!this->m_Coefficients)
    {
    return ERROR_ALLOC;
    }
  this->m_Knots = new Knot[size];
  if (!this->m_Knots)
    {
    delete []this->m_Coefficients;
    return ERROR_ALLOC;
    }
  double lx;
  for(size_t i = 0; i < size; i++)
    {
    if (i == 0)
      {
      lx = this->m_Knots[0].x = x[0];
      this->m_Knots[0].fx = fx[0];
      }
    else
      {
      if (x[i] <= lx)
        {
        this->Clear();
        return ERROR_INCREASING;
        }
      lx = this->m_Knots[i].x = x[i];
      this->m_Knots[i].fx = fx[i];
      }
    }
  this->m_Size = size;
  return this->ComputeCoefficients();
}

FitErrorCode Akima::Evaluate(double x, double &fx)
{
  size_t index;
 
  if(!this->m_Size)
    {
    return ERROR_NODATA;
    } 
  index = this->FindKnot(x);
  const Knot &knot = this->m_Knots[index];
  const double x0 = knot.x;
  const double _x = x - x0;
  const Coefficient & coeff = this->m_Coefficients[index];
  fx = knot.fx + _x * (coeff.b + _x * (coeff.c + coeff.d * _x));
  return ERROR_OK;
}

FitErrorCode Akima::ComputeCoefficients()
{
  double *_w = new double[this->m_Size+4];
  if (!_w)
    {
    this->Clear();
    return ERROR_ALLOC;
    }
  double *w = _w + 2;

  for (size_t i = 0; i <= this->m_Size - 2; ++i)
    {
    const Knot &knot0 = this->m_Knots[i]; 
    const Knot &knot1 = this->m_Knots[i+1]; 
    w[i] = (knot1.fx - knot0.fx) / (knot1.x - knot0.x);
    }

  // boundary conditions
  w[-2] = 3.0 * w[0] - 2.0 * w[1];
  w[-1] = 2.0 * w[0] - w[1];
  w[this->m_Size - 1] = 2.0 * w[this->m_Size - 2] - w[this->m_Size - 3];
  w[this->m_Size] = 3.0 * w[this->m_Size - 2] - 2.0 * w[this->m_Size - 3];

  size_t last = this->m_Size - 1;
  for (size_t i = 0; i < last; i++)
    {
    Coefficient &coeff = this->m_Coefficients[i]; 
    const double NE = fabs (w[i + 1] - w[i]) + fabs (w[i - 1] - w[i - 2]);
    if (NE == 0.0)
      {
      coeff.b = w[i];
      coeff.c = 0.0;
      coeff.d = 0.0;
      }
    else
      {
      const double h = this->m_Knots[i+1].x - this->m_Knots[i].x;
      const double NE_next = fabs (w[i+2] - w[i+1]) + fabs (w[i] - w[i-1]);
      const double alpha = fabs (w[i-1] - w[i-2]) / NE;
      double alpha_p1;
      double tL_p1;
      if (NE_next == 0.0)
        {
        tL_p1 = w[i];
        }
      else
        {
        alpha_p1 = fabs (w[i] - w[i-1]) / NE_next;
        tL_p1 = (1.0 - alpha_p1) * w[i] + alpha_p1 * w[i + 1];
        }
      coeff.b = (1.0 - alpha) * w[i - 1] + alpha * w[i];
      coeff.c = (3.0 * w[i] - 2.0 * coeff.b - tL_p1) / h;
      coeff.d = (coeff.b + tL_p1 - 2.0 * w[i]) / (h * h);
      }
    }
  delete []_w;
  return ERROR_OK;
}

size_t Akima::FindKnot(double x)
{
  if (x <= this->m_Knots[0].x)
    {
    return 0;
    }
  if (x >= this->m_Knots[this->m_Size-1].x)
    {
    return this->m_Size-2;
    }
  size_t lo = 0;
  size_t hi = this->m_Size-1;
  while(hi > lo + 1) {
    size_t i = (hi + lo)/2;
    if(this->m_Knots[i].x > x)
      {
      hi = i;
      }
    else
      {
      lo = i;
      }
  }
  return lo;
}

END_PCMR_DECLS
