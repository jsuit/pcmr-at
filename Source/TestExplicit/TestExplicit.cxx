#include "TestExplicit.h"
#include "itkImageRegionIteratorWithIndex.h"

template class itk::Image<unsigned char,3>;

static size_t ss = sizeof(itk::Image<unsigned char,3>);

void DumpImage(TestExplicitImage *image)
{
  std::cout << typeid(TestExplicitImage).name() << std::endl;
  image->Update();
  image->Print(std::cout);
}

TestExplicitCustomFilter::TestExplicitCustomFilter()
{
}

TestExplicitCustomFilter::~TestExplicitCustomFilter()
{
}


void TestExplicitCustomFilter::ThreadedGenerateData(const Superclass::OutputImageRegionType& outputRegionForThread, itk::ThreadIdType threadId)
{
  std::cout << "ThreadedGenerateData ID: " << threadId << std::endl;

  TestExplicitImage::Pointer output = this->GetOutput();
  TestExplicitImage::ConstPointer input = this->GetInput();

  // output iterator for the region of this thread
  itk::ImageRegionIteratorWithIndex<TestExplicitImage> itOut(output, outputRegionForThread);
  itk::ImageRegionConstIteratorWithIndex<TestExplicitImage> itIn(input, outputRegionForThread);

  while( !itOut.IsAtEnd() )
    {
    itOut.Set(itIn.Get());
    ++itOut;
    ++itIn;
    }
}
