/*=========================================================================
This source has no copyright.  It is intended to be copied by users
wishing to create their own VTK classes locally.
=========================================================================*/
#include "vmtkSimpleCapPolyData.h"

#include "vtkObjectFactory.h"

vtkStandardNewMacro(vmtkSimpleCapPolyData);

//----------------------------------------------------------------------------
vmtkSimpleCapPolyData::vmtkSimpleCapPolyData()
{
  this->m_Capper = vtkSmartPointer<vtkvmtkSimpleCapPolyData>::New();
}

//----------------------------------------------------------------------------
vmtkSimpleCapPolyData::~vmtkSimpleCapPolyData()
{
}

int vmtkSimpleCapPolyData::ProcessRequest(
  vtkInformation *request,
  vtkInformationVector **inputVector,
  vtkInformationVector *outputVector)
{
  return this->m_Capper->ProcessRequest( request, inputVector, outputVector );
}

//----------------------------------------------------------------------------
void vmtkSimpleCapPolyData::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
  os << indent << "CellEntityIdsArrayName: " << this->GetCellEntityIdsArrayName() << std::endl;
}
