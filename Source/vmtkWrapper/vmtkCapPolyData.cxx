/*=========================================================================
This source has no copyright.  It is intended to be copied by users
wishing to create their own VTK classes locally.
=========================================================================*/
#include "vmtkCapPolyData.h"

#include "vtkObjectFactory.h"

vtkStandardNewMacro(vmtkCapPolyData);

//----------------------------------------------------------------------------
vmtkCapPolyData::vmtkCapPolyData()
{
  this->m_Capper = vtkSmartPointer<vtkvmtkCapPolyData>::New();
}

//----------------------------------------------------------------------------
vmtkCapPolyData::~vmtkCapPolyData()
{
}

int vmtkCapPolyData::ProcessRequest(
  vtkInformation *request,
  vtkInformationVector **inputVector,
  vtkInformationVector *outputVector)
{
  return this->m_Capper->ProcessRequest( request, inputVector, outputVector );
}

//----------------------------------------------------------------------------
void vmtkCapPolyData::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
  os << indent << "CellEntityIdsArrayName: " << this->GetCellEntityIdsArrayName() << std::endl;
}
