#ifndef __pcmrTypes_h
#define __pcmrTypes_h

#include "pcmrDefs.h"
#include "pcmrCommon_Export.h"

BEGIN_PCMR_DECLS

typedef enum
{
  VC_UNKNOWN = 0,
  X,
  Y,
  Z,
  VelocityComponentTypeEnd
} VelocityComponentType;

typedef enum
{
  PHASE_UNKNOWN = 0,
  RL,
  AP,
  FH,
  PhaseDirectionTypeEnd
} PhaseDirectionType;

typedef enum
{
  CIC_UNKNOWN = 0,
  MIXED,
  MAGNITUDE,
  PHASE,
  ComplexImageComponentTypeEnd
} ComplexImageComponentType;

typedef enum
{
  ST_UNDEFINED = 0,
  OK,
  NO_PHILIPS,
  NO_PHASE4D,
  TAG_NOTFOUND,
  IMG_MISSMATCH,
  NO_MRI_STORAGE,
  NO_ENHANCEDMRI,
  NO_MODMR,
  NO_FLOWENCODED,
  NO_ACQ3D,
  NO_PHASECONTRAST,
  SQ_EMPTY,
  SQ_LENGTH_MISSMATCH,
  NO_MEMORY,
  UNK_COMPLEX_IC,
  INV_PCV_LENGTH,
  NULL_PCV,
  ST_NOREAD,
  DCM_NO_PM,
  DCM_NO_MVOL,
  VALUE_MISSMATCH,
  CSA_ITEM_EMPTY,
  INV_DICOMDIR,
  NO_DRECSEQ,
  DRECSEQ_EMPTY,
  PATH_NOEXISTS,
  PATH_NOTDIR,
  FAIL_OPEN_W,
  FAIL_IMG_READINFO,
  INV_SLICE_INDEX,
  INV_TIMESTEP,
  INV_FILENAME,
  FAIL_IMG_READBUFFER,
  UNK_PIXEL_TYPE,
  GENERAL_ERROR,
  NO_FLOW_X,
  NO_FLOW_Y,
  NO_FLOW_Z,
  NO_MASK,
  SEQ_NOT_INCR,
  INV_DIRECTION,
  StatusTypeEnd
} StatusType;

PCMRCOMMON_EXPORT const char*GetStatusDescription( int i );

PCMRCOMMON_EXPORT const char*GetVelocityComponentName( int i );

PCMRCOMMON_EXPORT const char*GetPhaseDirectionName( int i );

PCMRCOMMON_EXPORT const char*GetComplexImageComponentName( int i );

PCMRCOMMON_EXPORT ComplexImageComponentType GetComplexImageComponentId( const char *name );

END_PCMR_DECLS;

#endif
