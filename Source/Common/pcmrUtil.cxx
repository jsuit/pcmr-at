#include "pcmrUtil.h"
#include "gdcmImageRegionReader.h"
#include "gdcmImageHelper.h"
#include "gdcmBoxRegion.h"

#include "itkImage.h"
#include "itkImportImageFilter.h"
#include "itkScalarToRGBColormapImageFilter.h"
#include "itkImageFileWriter.h"

BEGIN_PCMR_DECLS

template <class PixelType>
StatusType WriteBufferToPNG(const char* fileName,
                            const PixelType *buffer, size_t resx, size_t resy,
                            double sx, double sy)
{
  const unsigned int Dimension = 2;
  typedef itk::Image<PixelType, Dimension > ImageType;
  typedef itk::ImportImageFilter< PixelType, Dimension > ImportFilterType;
 
  typename ImportFilterType::Pointer importFilter = ImportFilterType::New();
 
  typename ImportFilterType::SizeType  size;
 
  size[0]  = resx;  // size along X
  size[1]  = resy;  // size along Y
  const unsigned int numberOfPixels = resx * resy;
 
  typename ImportFilterType::IndexType start;
  start.Fill( 0 );
 
  typename ImportFilterType::RegionType region;
  region.SetIndex(start);
  region.SetSize(size);
 
  importFilter->SetRegion(region);
 
  double origin[Dimension];
  origin[0] = 0.0;    // X coordinate
  origin[1] = 0.0;    // Y coordinate
 
  importFilter->SetOrigin( origin );
 
  double spacing[Dimension];
  spacing[0] = sx;    // along X direction
  spacing[1] = sy;    // along Y direction
 
  importFilter->SetSpacing(spacing);  
  const bool importImageFilterWillOwnTheBuffer = false;
  importFilter->SetImportPointer(const_cast<PixelType*>(buffer), numberOfPixels,
                                 importImageFilterWillOwnTheBuffer);

  typedef itk::RGBPixel<unsigned char>    RGBPixelType;
  typedef itk::Image<RGBPixelType, 2>  RGBImageType;
  typedef itk::ScalarToRGBColormapImageFilter<ImageType, RGBImageType> RGBFilterType;
  typename RGBFilterType::Pointer rgbFilter = RGBFilterType::New();
  rgbFilter->SetInput(importFilter->GetOutput());
  rgbFilter->SetColormap( RGBFilterType::Grey );
 
  typedef itk::ImageFileWriter<RGBImageType> WriterType;
  typename WriterType::Pointer writer = WriterType::New();
 
  writer->SetFileName(fileName);
 
  writer->SetInput(rgbFilter->GetOutput());
  writer->Update();
 
  return OK;
}

StatusType WriteBufferToPNG(const char* fileName, const gdcm::PixelFormat & pf,
                            const char *buffer, size_t resx, size_t resy,
                            double sx, double sy)
{
  switch (pf.GetScalarType())
    {
    case gdcm::PixelFormat::UINT16:
      return WriteBufferToPNG<uint16_t>(fileName,
                                        reinterpret_cast<const uint16_t*>(buffer),
                                        resx, resy, sx, sy);
      break;
    default:
      std::cout << "Pixel format not implemented: " 
                << pf.GetScalarTypeAsString() << std::endl;
      return UNK_PIXEL_TYPE;
    }
}

StatusType WriteSliceToPNG(const char* fileDICOM, const char* filePNG,
                           unsigned int sliceIndex)
{
  gdcm::ImageRegionReader reader;
  reader.SetFileName(fileDICOM);
  if (!reader.ReadInformation())
    {
    return FAIL_IMG_READINFO;
    }
  const gdcm::File &file = reader.GetFile();
  std::vector<unsigned int> dimension = gdcm::ImageHelper::GetDimensionsValue(file);
  if (sliceIndex < 0 || sliceIndex > dimension[2])
    {
    return INV_SLICE_INDEX;
    }
  std::vector<double> spacing = gdcm::ImageHelper::GetSpacingValue(file);
  gdcm::PixelFormat pf = gdcm::ImageHelper::GetPixelFormatValue(file);
  gdcm::BoxRegion box;
  box.SetDomain(0, dimension[0]-1, 0, dimension[1]-1,
                sliceIndex, sliceIndex);
  reader.SetRegion(box);
  size_t buffer_len = reader.ComputeBufferLength();
  char *buffer = new char[buffer_len];
  if (!buffer)
    {
    return NO_MEMORY;
    }
  if (!reader.ReadIntoBuffer(buffer, buffer_len))
    {
    delete []buffer;
    return FAIL_IMG_READBUFFER;
    }
  return WriteBufferToPNG(filePNG, pf, buffer, dimension[0], dimension[1],
                          spacing[0], spacing[1]);
}

END_PCMR_DECLS
