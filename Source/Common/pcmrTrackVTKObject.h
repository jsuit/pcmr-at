#ifndef __pcmrTrackVTKObject_h
#define __pcmrTrackVTKObject_h

#include "pcmrDefs.h"
#include "pcmrCommon_Export.h"
#include "vtkObject.h"

#include <iostream>
#include <boost/unordered_map.hpp> 

BEGIN_PCMR_DECLS

class PCMRCOMMON_EXPORT TrackVTKObject : public vtkObject
{
 public:
  static TrackVTKObject *New();
  void PrintSelf(ostream &os, vtkIndent indent);

  void CheckPointers();
  void TrackObjectPointer(vtkObject *ptr);
  void SetName(const char *name)
  {
    this->m_Name = name;
  }
  const char* GetName() const
  {
    return this->m_Name.c_str();
  }

 protected:
  
  TrackVTKObject();
  ~TrackVTKObject();

 private:
  TrackVTKObject(const TrackVTKObject&);  // Not implemented.
  void operator=(const TrackVTKObject&);  // Not implemented.

  void OnObjectDelete(vtkObject *obj, unsigned long event, void*data);

  std::string m_Name;
  size_t m_Registered;
  size_t m_Deleted;
  typedef boost::unordered_map<vtkObject*, int> PointerMap;
  PointerMap m_PointerMap;

};

END_PCMR_DECLS

#endif

