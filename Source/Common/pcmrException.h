#ifndef __pcmrException_h__

#include "pcmrDefs.h"
#include "pcmrCommon_Export.h"
#include <boost/exception/all.hpp>
#include <string>

BEGIN_PCMR_DECLS

struct PCMRCOMMON_EXPORT exception_base: virtual std::exception, virtual boost::exception
{
  exception_base();
};

struct PCMRCOMMON_EXPORT exception_io_error: virtual exception_base
{
  exception_io_error();
};

struct PCMRCOMMON_EXPORT exception_io_open: virtual exception_base
{
  exception_io_open();
};

struct PCMRCOMMON_EXPORT exception_mem_error: virtual exception_base
{
  exception_mem_error();
};

struct PCMRCOMMON_EXPORT exception_null_pointer: virtual exception_mem_error 
{
  exception_null_pointer();
};

struct PCMRCOMMON_EXPORT exception_path_noexist: virtual exception_io_error
{
  exception_path_noexist();
};

typedef boost::error_info<struct tag_file_name,std::string> error_file_name;

END_PCMR_DECLS

#endif
