#include "pcmrException.h"

BEGIN_PCMR_DECLS

exception_base::exception_base()
{
}

exception_io_error::exception_io_error()
{
}

exception_io_open::exception_io_open()
{ 
}

exception_mem_error::exception_mem_error()
{
}

exception_path_noexist::exception_path_noexist()
{
}

exception_null_pointer::exception_null_pointer()
{
}

END_PCMR_DECLS
