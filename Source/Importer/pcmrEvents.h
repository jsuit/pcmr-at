#ifndef __pcmrEvents_h
#define __pcmrEvents_h

#include "itkEventObject.h"
#include "pcmrDefs.h"

BEGIN_PCMR_DECLS

itkEventMacro(StartScanEvent,         itk::EventObject);
itkEventMacro(EndScanEvent,           itk::EventObject);
itkEventMacro(StartPremaskEvent,      itk::EventObject);
itkEventMacro(EndPremaskEvent,        itk::EventObject);
itkEventMacro(StartPrepareWriteEvent, itk::EventObject);
itkEventMacro(EndPrepareWriteEvent,   itk::EventObject);
itkEventMacro(StartWriteEvent,        itk::EventObject);
itkEventMacro(EndWriteEvent,          itk::EventObject);
itkEventMacro(AwakeEvent,             itk::EventObject);

END_PCMR_DECLS

#endif
