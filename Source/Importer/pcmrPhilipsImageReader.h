#ifndef __pcmrPhilipsImageReader_h
#define __pcmrPhilipsImageReader_h

#include <utility>
#include "pcmr4DImporter_Export.h"
#include "pcmrPhilipsDefs.h"
#include "itkPhaseContrastImage.h"

BEGIN_PHILIPS_DECLS

class PCMR4DIMPORTER_EXPORT RescalerType
{
 public:
  
  RescalerType()
    : m_Identity(true), m_Slope(1.0), m_Intercept(0.0)
    {
    }
  
  RescalerType( float slope, float intercept )
    : m_Identity(false), m_Slope(slope), m_Intercept(intercept)
  {
  }
  
  ~RescalerType() {}
  
  float GetSlope() const
  {
    return m_Slope;
  }

  float GetIntercept() const
  {
    return m_Intercept;
  }

  bool IsIdentity() const
  {
    return m_Identity;
  }

  inline
  itk::PCPixelType operator()( const itk::PCPixelType & x ) const
  {
    return this->GetSlope() * x + this->GetIntercept();
  }

  bool operator!=( const RescalerType & other ) const
  {
    return other.GetSlope() != this->GetSlope()
      || other.GetIntercept() != this->GetIntercept();
  }

  bool operator==( const RescalerType & other ) const
  {
    return !(*this != other);
  }

  void Print( std::ostream & out ) const
  {
    out << "Slope = " << this->m_Slope << std::endl
        << "Intercept = " << this->m_Intercept << std::endl;
  }
  
 private:
  bool m_Identity;
  float m_Slope;
  float m_Intercept;
};
 
typedef std::pair<itk::PhaseContrastImage::Pointer,itk::PhaseContrastImage::Pointer>
  PhaseContrastMixedImageType;

PCMR4DIMPORTER_EXPORT itk::PhaseContrastImage::Pointer
ReadPhaseImage(const char* dicomFile,
               unsigned int numberOfPhases,
               unsigned int numberOfVolumes,
               const RescalerType& phaseRescale,
               float zSpacing = 0.0,
               unsigned int minX = 0, unsigned int maxX = 0,
               unsigned int minY = 0, unsigned int maxY = 0);

PCMR4DIMPORTER_EXPORT itk::PhaseContrastImage::Pointer
ReadMagnitudeImage(const char* dicomFile,
                   unsigned int numberOfPhases,
                   unsigned int numberOfVolumes,
                   float zSpacing = 0.0,
                   unsigned int minX = 0, unsigned int maxX = 0,
                   unsigned int minY = 0, unsigned int maxY = 0);
 
PCMR4DIMPORTER_EXPORT PhaseContrastMixedImageType
ReadMixedImage(const char* dicomFile,
               unsigned int numberOfPhases,
               unsigned int numberOfVolumes,
               const RescalerType& phaseRescale,
               float zSpacing = 0.0,
               unsigned int minX = 0, unsigned int maxX = 0,
               unsigned int minY = 0, unsigned int maxY = 0);
  

END_PHILIPS_DECLS

#endif
