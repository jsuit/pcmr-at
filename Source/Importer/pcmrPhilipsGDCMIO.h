#ifndef __pcmrPhilipsGDCMIO_h
#define __pcmrPhilipsGDCMIO_h

#include "gdcmReader.h"
#include "pcmr4DImporter_Export.h"
#include "pcmrTypes.h"
#include "pcmrPhilipsDefs.h"
#include "itkIndent.h"

#include "itkObject.h"
#include "itkObjectFactory.h"

BEGIN_PHILIPS_DECLS

struct PCMR4DIMPORTER_EXPORT FrameType
{
  unsigned int m_PhaseNumber;
  unsigned int m_SliceNumberMR;
  ComplexImageComponentType m_ComplexImageComponent;
  float m_PixelSpacing[2];
  float m_SliceThickness;
  float m_SpacingBetweenSlices;
  float m_TriggerTime;
  float m_RescaleIntercept;
  float m_RescaleSlope;

  FrameType()
    : m_PhaseNumber(0),
    m_SliceNumberMR(0),
    m_ComplexImageComponent( CIC_UNKNOWN ),
    m_SliceThickness(1.0),
    m_SpacingBetweenSlices(1.0),
    m_TriggerTime(0.0),
    m_RescaleIntercept(0.0),
    m_RescaleSlope(1.0)
      {
        m_PixelSpacing[0] = m_PixelSpacing[1] = 1.0;
      }
  void Print( std::ostream &_os, itk::Indent indent = 0 ) const;
};
  
class PCMR4DIMPORTER_EXPORT EnhancedMRI : public itk::Object
{
public:

  /** Standard class typedefs. */
  typedef  EnhancedMRI                   Self;
  typedef itk::Object                    Superclass;
  typedef itk::SmartPointer<Self>        Pointer;
  typedef itk::SmartPointer<const Self>  ConstPointer;

  /** Method for creation through the object factory. */
  itkNewMacro(Self);  

  /** Run-time type information (and related methods). */
  itkTypeMacro(EnhancedMRI, itk::Object);

  EnhancedMRI();

  EnhancedMRI( const char* pathDICOM );

  virtual ~EnhancedMRI();

  void SetFileName(const char* pathDICOM)
  {
    this->m_FileName = pathDICOM;
  }

  const std::string &GetFileName()
  {
    return this->m_FileName;
  }

  StatusType Read();

  StatusType ReadDataSet(const gdcm::DataSet &ds);

  StatusType GetStatus() const
  {
    return this->m_Status;
  }

  ComplexImageComponentType GetComplexImageComponent() const
  {
    return this->m_ComplexImageComponent;
  }

  unsigned int GetNumberOfRows() const
  {
    return this->m_NumberOfRows;
  }
  
  unsigned int GetNumberOfColumns() const
  {
    return this->m_NumberOfColumns;
  }
  
  unsigned int GetNumberOfSlicesMR() const
  {
    return this->m_NumberOfSlicesMR;
  }
  
  unsigned int GetNumberOfFrames() const
  {
    return this->m_NumberOfFrames;
  }
  
  unsigned int GetNumberOfPhases() const
  {
    return this->m_NumberOfPhases;
  }

  VelocityComponentType GetVelocityComponent() const
  {
    return this->m_VelocityComponent;
  }

  float GetPCVelocity() const
  {
    return this->m_PCVelocity;
  }

  int GetIndexPCVelocity() const
  {
    return this->m_IndexPCVelocity;
  }

  const FrameType *GetFrameInfo( int i ) const
  {
    if ( i >= 0 && static_cast<size_t>(i) < this->GetNumberOfFrames() )
      {
        return this->m_FramesInfo + i;
      }
    return NULL;
  }
  
  void 	Print( std::ostream &_os, itk::Indent indent = 0 ) const;

  /** */
  static StatusType ScanDirectory(const char *dir,
                                  std::string &pathFlowX,
                                  std::string &pathFlowY,
                                  std::string &pathFlowZ);

  /** */
  static StatusType CheckManufacturer( const gdcm::DataSet &ds );
  /** */
  static StatusType CheckEnhancedMRImageType( const gdcm::DataSet &ds );
  /** */
  static StatusType ReadComplexImageComponent( const gdcm::DataSet &ds, std::string &comp );
  /** */
  static StatusType CheckModalityMR( const gdcm::DataSet &ds );
  /** */    
  static StatusType CheckFlowEncodeAcquisition( const gdcm::DataSet &ds );
  /** */
  static StatusType CheckMRAcquisitionType3D( const gdcm::DataSet &ds );
  /** */
  static StatusType CheckPhaseContrast( const gdcm::DataSet &ds );
  /** */
  static StatusType CheckEnhancedMRSOPClassUID( const gdcm::DataSet &ds );
  /** */
  static StatusType ReadNumberOfFrames( const gdcm::DataSet &ds, unsigned int &nframes );
  /** */
  static StatusType ReadNumberOfRows( const gdcm::DataSet &ds, unsigned int &nrows );
  /** */ 
  static StatusType ReadNumberOfColumns( const gdcm::DataSet &ds, unsigned int &ncolumns );
  /** */
  static StatusType ReadNumberOfPhasesMR( const gdcm::DataSet &ds, unsigned int &nphases );
  /** */
  static StatusType ReadPhaseNumber( const gdcm::DataSet &ds, unsigned int &fn );
  /** */
  static StatusType ReadNumberOfSlicesMR( const gdcm::DataSet &ds, unsigned int &nslices );
  /** */
  static StatusType ReadSliceNumberMR( const gdcm::DataSet &ds, unsigned int &sn );
  /** */
  static StatusType ReadPCVelocity( const gdcm::DataSet &ds, float pcv[] );
  /** */
  static StatusType ReadVolumeInfo( const gdcm::DataSet &ds,
                                    unsigned int & rows, unsigned int & columns, unsigned int & slices );
  /** */
  static StatusType ReadPixelSpacing( const gdcm::DataSet &ds, float spacing[] );
  /** */
  static StatusType ReadSliceThickness( const gdcm::DataSet &ds, float &sliceThickness );
  /** */
  static StatusType ReadSpacingBetweenSlices( const gdcm::DataSet &ds, float & spacingBetweenSlices );
  /** */
  static StatusType ReadTriggerTime( const gdcm::DataSet &ds, float & triggerTime );
  /** */
  static StatusType ReadRescaleIntercept( const gdcm::DataSet &ds, float & rescaleIntercept );
  /** */
  static StatusType ReadRescaleSlope( const gdcm::DataSet &ds, float & rescaleSlope );

  /** */
  static StatusType ReadImageType( const gdcm::DataSet &ds, std::vector<std::string> &imageType );
  static StatusType ReadImageType( const gdcm::DataSet &ds, std::string &imageType );

  /** */
  static StatusType ReadSeriesInstanceUID( const gdcm::DataSet &ds, std::string &serieUID );

  /** */
  static StatusType ReadSOPClassUID( const gdcm::DataSet &ds, std::string &sopcUID );

  /** */
  static StatusType ReadModality( const gdcm::DataSet &ds, std::string &modality );

  /** */
  static StatusType ReadMRAcquisitionType( const gdcm::DataSet &ds, std::string &acquisitionType );

  /** */
  static StatusType IsMRImageStorage( const gdcm::DataSet &ds );

  /** */
  static StatusType IsEnhancedMRImageStorage( const gdcm::DataSet &ds );

protected:
  /** */
  static StatusType TryEnhancedDICOM(const char *filename,
                                     std::string &pathFlowX,
                                     std::string &pathFlowY,
                                     std::string &pathFlowZ);
  /** */
  static StatusType ScanDirectory0(const char *dir,
                                   std::string &pathFlowX,
                                   std::string &pathFlowY,
                                   std::string &pathFlowZ);


  StatusType ReadGlobalParameters( const gdcm::DataSet &ds );
  
  /** */
  StatusType ReadFramesInfo(const gdcm::DataSet &ds);

  
private:
  // we should deal with the following AT element
  // (0020,9167) AT (2005,140f)                              #   4, 1 FunctionalGroupPointer
  std::string m_FileName;
  StatusType m_Status;
  bool m_MagnitudePresent;
  unsigned int m_NumberOfRows;
  unsigned int m_NumberOfColumns;
  unsigned int m_NumberOfSlicesMR;
  unsigned int m_NumberOfFrames;
  unsigned int m_NumberOfPhases;
  FrameType * m_FramesInfo;
  float m_PCVelocity;
  int m_IndexPCVelocity;
  VelocityComponentType m_VelocityComponent;
  ComplexImageComponentType m_ComplexImageComponent;
};

;

END_PHILIPS_DECLS

#endif
