#ifndef __pcmrSiemensGDCMIO_h
#define __pcmrSiemensGDCMIO_h

#include "pcmrSiemensDefs.h"
#include "gdcmCSAHeader.h"
#include "pcmrGDCMIO.h"

BEGIN_SIEMENS_DECLS

StatusType ReadInstanceNumber( const gdcm::DataSet &ds,
                               unsigned int &InstanceNumber );

StatusType ReadCardiacNumberOfImages( const gdcm::DataSet &ds, 
                                      unsigned int &CardiacNumberOfImages );

StatusType ReadRepetitionTime( const gdcm::DataSet &ds,
                               float &RepetitionTime );

StatusType ReadTriggerTime( const gdcm::DataSet &ds, float &TriggerTime );

ComplexImageComponentType GetPhaseContrastComponent( const gdcm::DataSet &ds );

StatusType GetCSAVelocityEncoding( gdcm::CSAHeader &csa, float &venc );
StatusType GetCSAEncodingDirection( gdcm::CSAHeader &csa, int &direction );
StatusType GetCSADirectionPositive( gdcm::CSAHeader &csa, int &positive );

END_SIEMENS_DECLS

#endif
