#ifndef __pcmrGDCMIO_h
#define __pcmrGDCMIO_h

#include "gdcmReader.h"
#include "pcmrTypes.h"
#include "pcmr4DImporter_Export.h"

BEGIN_PCMR_DECLS

typedef enum 
{ UNKNOWN, SIEMENS, PHILIPS_UNENHANCED, PHILIPS_ENHANCED } ImporterType;

template< int TVR, int TVM >
StatusType TryTag( uint16_t group, uint16_t element,
                   const gdcm::DataSet &ds,
                   gdcm::Element<TVR,TVM> &el )
{
  gdcm::Tag tag( group, element );
  if( !ds.FindDataElement( tag ) ) 
    {
      return TAG_NOTFOUND;
    }
  const gdcm::DataElement& data = ds.GetDataElement( tag );
  el.SetFromDataElement( data );
  return OK;
}

bool PCMR4DIMPORTER_EXPORT IsDICOMDIR( gdcm::File &file );

bool PCMR4DIMPORTER_EXPORT HasPixelData( gdcm::DataSet &ds );

StatusType ReadManufacturer( gdcm::DataSet &ds, std::string & ID );

StatusType PCMR4DIMPORTER_EXPORT CheckManufacturer( gdcm::DataSet &ds, const char* ID );

StatusType PCMR4DIMPORTER_EXPORT CheckProtocolName( gdcm::DataSet &ds, const char* ID );

StatusType PCMR4DIMPORTER_EXPORT CheckMRAcquisitionType( const gdcm::DataSet &ds, const char* Value );

StatusType PCMR4DIMPORTER_EXPORT DetectImporterTypeAtFile( const char *filename, ImporterType &importerType );

StatusType PCMR4DIMPORTER_EXPORT DetectImporterTypeAtDirectory( const char *dir, ImporterType &importerType );

/** */
StatusType PCMR4DIMPORTER_EXPORT ReadSeriesInstanceUID( const gdcm::DataSet &ds, std::string &serieUID );

StatusType PCMR4DIMPORTER_EXPORT ReadImageType( const gdcm::DataSet &ds, std::string &imageType );

StatusType PCMR4DIMPORTER_EXPORT ReadImageType( const gdcm::DataSet &ds, std::vector<std::string> &imageType );

END_PCMR_DECLS;

#endif

