#include <iostream>
#include "pcmrGDCMIO.h"

int main( int argc, const char* argv[] )
{
  if ( argc != 2 )
    {
    std::cout << "Usage: " << argv[0] << " dir\n";
    return -1;
    }

  pcmr::ImporterType importerType;
  DetectImporterTypeAtDirectory( argv[1], importerType );
  std::cout << "Importer type detected = " << importerType << std::endl;
  return 0;
}
