#include <iostream>
#include "pcmrSiemensScanner.h"

int main( int argc, const char *argv[] )
{
  if ( argc != 2 )
    {
    std::cout << "Usage: " << argv[0] << " dir\n";
    return -1;
    }
  pcmr::Siemens::Scanner::Pointer scanner = pcmr::Siemens::Scanner::New( );
  pcmr::StatusType status = scanner->Scan( argv[1] );
  if ( status != pcmr::OK )
    {
    return -1;
    }
  std::vector<std::string> filenames;

  scanner->SelectMagnitudeVolume( 0, filenames );
  std::cout << "MAG(0) = " << filenames[0] << std::endl;
  filenames.clear( );

  scanner->SelectPhaseXVolume( 1, filenames );
  std::cout << "FLOW_X(1) = " << filenames[0] << std::endl;
  filenames.clear( );

  scanner->SelectPhaseYVolume( 2, filenames );
  std::cout << "FLOW_Y(2) = " << filenames[0] << std::endl;
  filenames.clear( );

  scanner->SelectPhaseZVolume( 3, filenames );
  std::cout << "FLOW_Z(3) = " << filenames[0] << std::endl;
  filenames.clear( );
  return 0;
}
