#include <iostream>
#include <boost/filesystem.hpp>
#include "pcmrPhilipsGDCMIO.h"

int main( int argc, const char* argv[] )
{
  if ( argc != 2 )
    {
    std::cout << "Usage: " << argv[0] << " IM_dicom\n";
    return -1;
    }

  pcmr::StatusType status;

  gdcm::Reader reader;
  reader.SetFileName( argv[1] );
  if(!reader.Read())
    {
    std::cout << "Could not read: \"" << argv[1] << "\"" << std::endl;
    return -1;
    }
  // The output of gdcm::Reader is a gdcm::File
  gdcm::File &file = reader.GetFile();

  // the dataset is the the set of element we are interested in:
  gdcm::DataSet &ds = file.GetDataSet();
  status = pcmr::Philips::EnhancedMRI::IsMRImageStorage( ds );
  if ( status == pcmr::OK )
    {
    std::cout << "IsMRImageStorage = YES\n";
    }
  else if ( status == pcmr::NO_MRI_STORAGE )
    {
    std::cout << "IsMRImageStorage = NO\n";
    }
  else
    {
    std::cout << pcmr::GetStatusDescription( status ) << std::endl;
    return -1;
    }

  std::string seriesInstance;
  status = pcmr::Philips::EnhancedMRI::ReadSeriesInstanceUID( ds, seriesInstance );
  if ( status == pcmr::OK )
    {
    std::cout << "Series Instance UID = " << seriesInstance << std::endl;
    }
  else
    {
    std::cout << pcmr::GetStatusDescription( status ) << std::endl;
    return -1;
    }

  std::string imageType;
  status = pcmr::Philips::EnhancedMRI::ReadImageType( ds, imageType );
  if ( status == pcmr::OK )
    {
    std::cout << "Image Type = " << imageType << std::endl;
    }
  else
    {
    std::cout << pcmr::GetStatusDescription( status ) << std::endl;
    return -1;
    }
  
  std::string modality;
  status = pcmr::Philips::EnhancedMRI::ReadModality( ds, modality );
  if ( status == pcmr::OK )
    {
    std::cout << "Modality = " << modality << std::endl;
    }
  else
    {
    std::cout << pcmr::GetStatusDescription( status ) << std::endl;
    return -1;
    }

  std::string acquisitionType;
  status = pcmr::Philips::EnhancedMRI::ReadMRAcquisitionType( ds, acquisitionType );
  if ( status == pcmr::OK )
    {
    std::cout << "MR Acquisition Type = " << acquisitionType << std::endl;
    }
  else
    {
    std::cout << pcmr::GetStatusDescription( status ) << std::endl;
    return -1;
    }

  unsigned int nphases;
  status = pcmr::Philips::EnhancedMRI::ReadNumberOfPhasesMR( ds, nphases );
  if ( status == pcmr::OK )
    {
    std::cout << "Number of Phases MR = " << nphases << "\n";
    }
  else 
    {
    std::cout << pcmr::GetStatusDescription( status ) << std::endl;
    return -1;
    }

  unsigned int phase;
  status = pcmr::Philips::EnhancedMRI::ReadPhaseNumber( ds, phase );
  if ( status == pcmr::OK )
    {
    std::cout << "Phase Number = " << phase << "\n";
    }
  else 
    {
    std::cout << pcmr::GetStatusDescription( status ) << std::endl;
    return -1;
    }

  float trigger;
  status = pcmr::Philips::EnhancedMRI::ReadTriggerTime( ds, trigger );
  if ( status == pcmr::OK )
    {
    std::cout << "Trigger Time = " << trigger << "\n";
    }
  else 
    {
    std::cout << pcmr::GetStatusDescription( status ) << std::endl;
    return -1;
    }

  unsigned int sliceNumber;
  status = pcmr::Philips::EnhancedMRI::ReadSliceNumberMR( ds, sliceNumber );
  if ( status == pcmr::OK )
    {
    std::cout << "Slice Number MR = " << sliceNumber << "\n";
    }
  else 
    {
    std::cout << pcmr::GetStatusDescription( status ) << std::endl;
    return -1;
    }

  float pcv[3];
  status = pcmr::Philips::EnhancedMRI::ReadPCVelocity( ds, pcv );
  if ( status == pcmr::OK )
    {
    std::cout << "Velocity = ";
    for ( int i = 0; i < 3; i++ )
      {
      std::cout << (i?"\\":"") << pcv[i];
      }
    std::cout << std::endl;
    }
  else 
    {
    std::cout << pcmr::GetStatusDescription( status ) << std::endl;
    return -1;
    }
  
  gdcm::Tag tsq_AcqParameters(0x2005,0x140f);
  if (!ds.FindDataElement( tsq_AcqParameters ) )
    {
    std::cout << "Acquisition Parameters (Philips) Sequence not found";
    }
  else
    {
    const gdcm::DataElement &sq_AcqParameters = ds.GetDataElement( tsq_AcqParameters );
    const gdcm::SequenceOfItems *sqi_AcqParameters = sq_AcqParameters.GetValueAsSQ();
    if ( !sqi_AcqParameters )
      {
      std::cout << "0x2005,0x140f could be retrieved as sequence\n";
      return -1;
      }
    const gdcm::Item & item_AcqParameters = sqi_AcqParameters->GetItem( 1 );
    const gdcm::DataSet& nds_AcqParameters = item_AcqParameters.GetNestedDataSet();
    status = pcmr::Philips::EnhancedMRI::CheckPhaseContrast( nds_AcqParameters );
    if ( status == pcmr::OK )
      {
      std::cout << "Phase Contrast = YES\n";
      }
    else if ( status == pcmr::NO_PHASECONTRAST )
      {
      std::cout << "Phase Contrast = NO\n";
      }
    else 
      {
      std::cout << pcmr::GetStatusDescription( status ) << std::endl;
      return -1;
      }
    }
  return 0;
}
