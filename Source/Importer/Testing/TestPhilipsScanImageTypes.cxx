#include <iostream>
#include <map>
#include <boost/filesystem.hpp>
#include "pcmrPhilipsGDCMIO.h"
#include <sstream>

using namespace boost::filesystem;

typedef std::map< std::string, std::vector<std::string> > MapType;

pcmr::StatusType ReadImageType( const char *filename, std::string &imageType )
{
  gdcm::Reader reader;
  reader.SetFileName( filename );
  if( !reader.Read( ) )
    {
    return pcmr::ST_NOREAD;
    }
   // The output of gdcm::Reader is a gdcm::File
  gdcm::File &file = reader.GetFile();

  // the dataset is the the set of element we are interested in:
  gdcm::DataSet &ds = file.GetDataSet();

  pcmr::StatusType status = pcmr::Philips::EnhancedMRI::CheckMRAcquisitionType3D( ds );
  if (status != pcmr::OK)
    {
    return status;
    }

  float velocity[3];
  status = pcmr::Philips::EnhancedMRI::ReadPCVelocity( ds, velocity );
  if ( status != pcmr::OK )
    {
    return status;
    }
  std::stringstream ss;
  for( int i = 0; i < 3; i++ )
    {
    ss << (i ? "\\" : "") << velocity[ i ];
    }
  imageType = ss.str( );
  return pcmr::OK;
  //return pcmr::Philips::EnhancedMRI::ReadImageType( ds, imageType );
  //return pcmr::Philips::EnhancedMRI::ReadSeriesInstanceUID( ds, imageType );
}

pcmr::StatusType ScanDirectory( const char *dir, MapType &mapTypes )
{
  path pathDir(dir);
  std::cout << "Scanning directory \"" << dir << "\"" << std::endl;
  for (directory_iterator it = directory_iterator(pathDir);
       it != directory_iterator(); it++)
    {
    pcmr::StatusType status;
    if (is_directory(it->status()))
      {
      status = ScanDirectory( it->path().string().c_str(), mapTypes );
      }
    else
      {
      std::string imageType;
      status = ReadImageType( it->path().string().c_str(), imageType );
      if ( status == pcmr::OK )
        {
        mapTypes[ imageType ].push_back(it->path().string().c_str());
        }
      else
        {
        std::cout << "could not read " << it->path().string() << std::endl;
        }
      }
    }
  return pcmr::OK;
}

int main( int argc, const char* argv[] )
{
  if ( argc != 2 )
    {
    std::cout << "Usage: " << argv[0] << " dir\n";
    return -1;
    }
  MapType mapType;

  ScanDirectory( argv[1], mapType );

  for ( MapType::iterator it = mapType.begin(); it != mapType.end(); ++it )
    {
    std::cout << it->first << " <--> " << it->second.size() << ", " << it->second[0] << std::endl;
    }

  return 0;
}
