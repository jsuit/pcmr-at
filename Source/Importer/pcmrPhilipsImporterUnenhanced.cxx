#include "pcmrPhilipsImporterUnenhanced.h"
#include "itkRegionOfInterestImageFilter.h"
#include "itkCommand.h"
#include "itkGDCMImageIO.h"
#include "itkImageSeriesReader.h"
#include "itkPhaseContrastImage.h"
#include "itkShiftScaleImageFilter.h"

typedef itk::ImageSeriesReader<itk::PhaseContrast3DImage> ReaderType;
typedef itk::ShiftScaleImageFilter<itk::PhaseContrast3DImage,
                                   itk::PhaseContrast3DImage> ShiftScaleType;
BEGIN_PHILIPS_DECLS

ImporterUnenhanced::ImporterUnenhanced()
  : BaseImporter()
{
  TileFilterType::LayoutArrayType layout;
  layout[0] = 1;
  layout[1] = 1;
  layout[2] = 1;
  layout[3] = 0;

  for ( size_t i = 0; i < 4; i++ )
    {
    this->m_Tilers[i] = TileFilterType::New();
    this->m_Tilers[i]->SetLayout( layout );
    }
  this->m_PCMRAFilter = itk::PhaseContrastTimeAveragedImageFilter::New( );
  this->m_Scanner = Scanner::New( );
  this->AddAwakeObserver( this->m_PCMRAFilter );
  this->AddAwakeObserver( this->m_Scanner );

  //std::cout << "Siemens::Importer Testing AwakeEvent()\n";
  //this->InvokeEvent(AwakeEvent());
  //this->TestAwakeEvent();
  //std::cout << "Salio algo?\n";
}

ImporterUnenhanced::~ImporterUnenhanced()
{
}

StatusType ImporterUnenhanced::ReadInformation()
{
  this->InvokeEvent( StartScanEvent() );
  this->GetStatusRef( ) = this->m_Scanner->Scan( this->GetSequenceDirectory().c_str( ) );
  this->FillExtendedDictionary( );
  this->InvokeEvent( EndScanEvent( ) );
  return this->GetStatus( );
}

size_t ImporterUnenhanced::GetNumberOfTimeSteps( )
{
  return this->m_Scanner->GetNumberOfPhases( );
}

float ImporterUnenhanced::GetLengthOfTimeStep( )
{
  return this->m_Scanner->GetLengthOfTimeStep( );
}

float ImporterUnenhanced::GetVelocityEncoding( )
{
  float venc = this->m_Scanner->GetVelocityEncoding( );
  return venc <= 0 ? this->GetDefaultVelocityEncoding( ) : venc;
}

float ImporterUnenhanced::GetMinimumVelocity()
{
  return 0.0;
}

float ImporterUnenhanced::GetMaximumVelocity()
{
  return this->GetVelocityEncoding();
}

bool ImporterUnenhanced::GetInvertPhase(unsigned int idx)
{
  std::string raiCode = this->GetImageOrientationLabel();
  char c = raiCode[idx];
  return (c == 'L' || c == 'P' || c == 'S');
}

bool ImporterUnenhanced::GetInvertComponentX()
{
  return this->GetInvertPhase(0);
}

bool ImporterUnenhanced::GetInvertComponentY()
{
  return this->GetInvertPhase(1);
}

bool ImporterUnenhanced::GetInvertComponentZ()
{
  return this->GetInvertPhase(2);
}

std::string ImporterUnenhanced::GetOneDicom()
{
  return this->m_Scanner->GetOneDicomFile( );
}

std::string ImporterUnenhanced::GetImageOrientationLabel()
{
  return this->m_Scanner->GetSpatialOrientation();
}

void ImporterUnenhanced::GetTimeValuesInternal( std::vector<double> &timeValues )
{
  this->m_Scanner->GetTimeStepValues( timeValues );
}

StatusType ImporterUnenhanced::GetRepresentativeSlice( std::string &fileName, size_t &sliceIndex )
{
  return this->GetMagnitudeRepresentativeSlice( fileName, sliceIndex );
}

StatusType ImporterUnenhanced::GetMagnitudeRepresentativeSlice( std::string &fileName, size_t &sliceIndex )
{
  size_t representativeTimeStep = 3;

  fileName = this->m_Scanner->GetMagnitudeCentralSlice( representativeTimeStep );
  sliceIndex = 0;
  return OK;
}

StatusType ImporterUnenhanced::GetPhaseRepresentativeSlice( VelocityComponentType vc,
                                                  std::string &fileName, size_t &sliceIndex )
{
  size_t representativeTimeStep = 3;

  fileName = this->m_Scanner->GetPhaseCentralSlice(representativeTimeStep,
                                                   this->VelocityComponentToPhaseDirection(vc));
  sliceIndex = 0;
  return OK;
}

PhaseDirectionType ImporterUnenhanced::VelocityComponentToPhaseDirection(VelocityComponentType vc)
{
  char c;
  const std::string raiCode = this->GetImageOrientationLabel();
  switch(vc) {
    case X:
      c = raiCode[0];
      break;
    case Y:
      c = raiCode[1];
      break;
    case Z:
      c = raiCode[2];
      break;
    default:
      return PHASE_UNKNOWN;
  }
  switch(c) {
    case 'R':
    case 'L':
      return RL;
    case 'A':
    case 'P':
      return AP;
    case 'I':
    case 'S':
      return FH;
    default:
      return PHASE_UNKNOWN;
  }
}

StatusType ImporterUnenhanced::PrepareForWriting( const char* studyDirectory )
{
  typedef itk::GDCMImageIO ImageIOType;
  size_t sizeVolume = this->m_Scanner->GetNumberOfSlicesPerVolume();
  ReaderType::FileNamesContainer filenamesMag,
    filenamesX, filenamesY, filenamesZ;

  size_t numberOfTimeSteps = this->GetNumberOfTimeSteps();
  for ( size_t ts = 0; ts < numberOfTimeSteps; ts++ )
    {
    std::cout << " Extracting timestep number " << ts << std::endl;
    // read magnitude for this timestep
    this->InvokeEvent( AwakeEvent( ) );
    this->m_Scanner->GetMagnitudeFiles( ts, filenamesMag );
    itk::PhaseContrast3DImage::Pointer imgMag = this->ReadSeries3D( filenamesMag );
    imgMag->Update();
    this->m_Tilers[0]->SetInput( ts, imgMag );

    // read v_x component for this timestep
    this->InvokeEvent( AwakeEvent( ) );
    this->m_Scanner->GetPhaseFiles(this->VelocityComponentToPhaseDirection(pcmr::X),
                                   ts, filenamesX);
    itk::PhaseContrast3DImage::Pointer imgPhaseX = this->ReadSeries3D(filenamesX);
    this->m_Tilers[ 1 ]->SetInput( ts, imgPhaseX );

    // read v_y component for this timestep
    this->InvokeEvent( AwakeEvent( ) );
    this->m_Scanner->GetPhaseFiles(this->VelocityComponentToPhaseDirection(pcmr::Y),
                                   ts, filenamesY );
    itk::PhaseContrast3DImage::Pointer imgPhaseY = this->ReadSeries3D( filenamesY );
    this->m_Tilers[ 2 ]->SetInput( ts, imgPhaseY );

    // read v_z component for this timestep
    this->InvokeEvent( AwakeEvent( ) );
    this->m_Scanner->GetPhaseFiles(this->VelocityComponentToPhaseDirection(pcmr::Z),
                                   ts, filenamesZ );
    itk::PhaseContrast3DImage::Pointer imgPhaseZ = this->ReadSeries3D( filenamesZ );
    this->m_Tilers[ 3 ]->SetInput( ts, imgPhaseZ );
    }

  this->m_PCMRAFilter->SetNoiseMaskThreshold( 0.01 );
  this->m_PCMRAFilter->SetMagnitudeImage( this->m_Tilers[ 0 ]->GetOutput( ) );
  this->m_PCMRAFilter->SetPhaseXImage   ( this->m_Tilers[ 1 ]->GetOutput( ) );
  this->m_PCMRAFilter->SetPhaseYImage   ( this->m_Tilers[ 2 ]->GetOutput( ) );
  this->m_PCMRAFilter->SetPhaseZImage   ( this->m_Tilers[ 3 ]->GetOutput( ) );
  this->m_PCMRAFilter->Update( );
  //this->m_PCMRAFilter->Print( std::cout );

  return OK;
}

itk::PhaseContrast3DImage::ConstPointer ImporterUnenhanced::GetPremaskImage( size_t ts )
{
  return this->m_PCMRAFilter->GetOutput( );
}

itk::PhaseContrast3DImage::ConstPointer ImporterUnenhanced::GetMagnitudeImage( size_t ts )
{
  return this->m_Tilers[ 0 ]->GetInput( ts );
}

itk::PhaseContrast3DImage::ConstPointer ImporterUnenhanced::GetPhaseImage( size_t ts, size_t component )
{
  return this->m_Tilers[ component + 1 ]->GetInput( ts );
}

itk::PhaseContrast3DImage::Pointer ImporterUnenhanced::ReadSeries3D( const std::vector<std::string> &filenames )
{
  typedef itk::GDCMImageIO ImageIOType;
  typedef itk::RegionOfInterestImageFilter< itk::PhaseContrast3DImage, itk::PhaseContrast3DImage >
    ROIFilterType;

  ImageIOType::Pointer gdcmIO = ImageIOType::New( );
  gdcmIO->LoadPrivateTagsOn();
  ReaderType::Pointer seriesReader = ReaderType::New( );

  seriesReader->SetImageIO( gdcmIO );
  seriesReader->SetFileNames( filenames );
  try
    {
    seriesReader->Update( );
    }
  catch ( itk::ExceptionObject &excp )
    {
    std::cerr << "Exception thrown while reading the series" << std::endl;
    std::cerr << excp << std::endl;
    return NULL;
    }
  ROIFilterType::Pointer roiFilter = ROIFilterType::New( );
  if (this->HasROI())
  {
    itk::PhaseContrast3DImage::IndexType startROI;
    startROI[0] = this->GetMinimumX( );
    startROI[1] = this->GetMinimumY( );
    startROI[2] = 0;
    itk::PhaseContrast3DImage::SizeType sizeROI;
    itk::PhaseContrast3DImage::RegionType R1 = seriesReader->GetOutput( )->GetLargestPossibleRegion();
    sizeROI = R1.GetSize( );
    sizeROI[0] = ( this->GetMaximumX( ) - this->GetMinimumX()-1 );
    sizeROI[1] = ( this->GetMaximumY( ) - this->GetMinimumY()-1 );
    roiFilter->SetInput( seriesReader->GetOutput( ) );
    itk::PhaseContrast3DImage::RegionType roiRegion( startROI, sizeROI );
    roiFilter->SetRegionOfInterest( roiRegion );
    roiFilter->Update( );
  }
  std::string strIntercept, strSlope;
  if (!gdcmIO->GetValueFromTag("0028|1052", strIntercept) || !gdcmIO->GetValueFromTag("0028|1053", strSlope))
  {
    ShiftScaleType::Pointer shiftScaler = ShiftScaleType::New();

    gdcmIO->GetValueFromTag("2005|1409", strIntercept);
    gdcmIO->GetValueFromTag("2005|140a", strSlope);
    // std::cout << "using private intercept = " << strIntercept << std::endl;
    // std::cout << "using private slope = " << strSlope << std::endl;
    double scale = atof(strSlope.c_str());
    double shift = atof(strIntercept.c_str()) / scale;

    shiftScaler->SetScale(scale);
    shiftScaler->SetShift(shift);
    if(this->HasROI())
    {
      shiftScaler->SetInput(roiFilter->GetOutput());
    }
    else
    {
      shiftScaler->SetInput(seriesReader->GetOutput());
    }
    shiftScaler->Update();
    return shiftScaler->GetOutput();
  }
  //std::cout << "The image was rescaled on READ" << std::endl;
  //std::cout << "intercept used = " << strIntercept << std::endl;
  //std::cout << "scale used = " << strSlope << std::endl;
  if(this->HasROI())
  {
    roiFilter->Update();
    return roiFilter->GetOutput();
  }
  seriesReader->GetOutput();
}

END_PHILIPS_DECLS
