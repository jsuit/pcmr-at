#ifndef __pcmrPhilipsImporterUnenhanced_h
#define __pcmrPhilipsImporterUnenhanced_h

#include "pcmrBaseImporter.h"
#include "pcmr4DImporter_Export.h"
#include "pcmrPhilipsScannerUnenhanced.h"
#include "itkTileImageFilter.h"
#include "itkPhaseContrastTimeAveragedImageFilter.h"

typedef itk::TileImageFilter<itk::PhaseContrast3DImage,
                             itk::PhaseContrastImage> TileFilterType;

BEGIN_PHILIPS_DECLS

class PCMR4DIMPORTER_EXPORT ImporterUnenhanced : public BaseImporter
{
 public:
  /** Standard class typedefs. */
  typedef ImporterUnenhanced             Self;
  typedef BaseImporter                   Superclass;
  typedef itk::SmartPointer<Self>        Pointer;
  typedef itk::SmartPointer<const Self>  ConstPointer;

  /** Method for creation through the object factory. */
  itkNewMacro(Self);

  /** Run-time type information (and related methods). */
  itkTypeMacro(ImporterUnenhanced, BaseImporter);

  virtual StatusType ReadInformation();
  virtual size_t GetNumberOfTimeSteps();
  virtual float GetLengthOfTimeStep();
  virtual float GetVelocityEncoding();
  virtual float GetMinimumVelocity();
  virtual float GetMaximumVelocity();
  virtual bool GetInvertComponentX();
  virtual bool GetInvertComponentY();
  virtual bool GetInvertComponentZ();
  virtual std::string GetOneDicom();
  virtual std::string GetImageOrientationLabel();

  /*
  virtual void TestEvents()
  {
    Importer::TestEvents();
    std::cout << "Invocando this->m_Scanner->InvokeEvent(AwakeEvent())\n";
    this->m_Scanner->InvokeEvent(AwakeEvent());
  }
  */

 protected:
  ImporterUnenhanced();
  virtual ~ImporterUnenhanced();

  virtual void GetTimeValuesInternal( std::vector<double> &timeValues );
  virtual StatusType GetRepresentativeSlice( std::string &fileName, size_t &sliceIndex );
  virtual StatusType GetMagnitudeRepresentativeSlice( std::string &fileName, size_t &index );
  virtual StatusType GetPhaseRepresentativeSlice (VelocityComponentType vc,
                                                 std::string &fileName, size_t &index );
  virtual StatusType PrepareForWriting(const char* studyDirectory);
  virtual itk::PhaseContrast3DImage::ConstPointer GetPremaskImage(size_t ts);
  virtual itk::PhaseContrast3DImage::ConstPointer GetMagnitudeImage(size_t ts);
  virtual itk::PhaseContrast3DImage::ConstPointer GetPhaseImage(size_t ts, size_t component);

  itk::PhaseContrast3DImage::Pointer ReadSeries3D( const std::vector<std::string> &filenames );

  PhaseDirectionType VelocityComponentToPhaseDirection(VelocityComponentType vc);
  bool GetInvertPhase(unsigned int idx);

 private:

  Scanner::Pointer m_Scanner;
  TileFilterType::Pointer m_Tilers[4];
  itk::PhaseContrastTimeAveragedImageFilter::Pointer m_PCMRAFilter;
};

END_PHILIPS_DECLS

#endif
