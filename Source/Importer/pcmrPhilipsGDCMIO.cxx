#include "pcmrGDCMIO.h"
#include "pcmrPhilipsGDCMIO.h"
#include "pcmrEvents.h"
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>

using namespace boost::filesystem;

BEGIN_PHILIPS_DECLS

void FrameType::Print( std::ostream &_os, itk::Indent indent ) const
{
  _os << indent << "PhaseNumber = " << m_PhaseNumber << "\n"
      << indent << "SliceNumberMR = " << m_SliceNumberMR << "\n"
      << indent << "ComplexImageComponent = " << GetComplexImageComponentName( m_ComplexImageComponent ) << "\n"
      << indent << "PixelSpacing = " << m_PixelSpacing[0] << "/" << m_PixelSpacing[1] << "\n"
      << indent << "SliceThickness = " << m_SliceThickness << "\n"
      << indent << "SpacingBetweenSlices = " << m_SpacingBetweenSlices << "\n"
      << indent << "TriggerTime = " << m_TriggerTime << "\n"
      << indent << "RescaleSlope = " << m_RescaleSlope << "\n"
      << indent << "RescaleIntercept = " << m_RescaleIntercept << "\n";

}

EnhancedMRI::EnhancedMRI()
  :
  m_Status(ST_UNDEFINED),
  m_MagnitudePresent(false),
  m_NumberOfRows(0),
  m_NumberOfColumns(0),
  m_NumberOfSlicesMR(0),
  m_NumberOfFrames(0),
  m_NumberOfPhases(0),
  m_FramesInfo(NULL),
  m_PCVelocity(0.0),
  m_VelocityComponent(VC_UNKNOWN),
  m_ComplexImageComponent(CIC_UNKNOWN)
{
}

EnhancedMRI::EnhancedMRI(const char* pathDICOM)
  :
  m_FileName(pathDICOM),
  m_Status(ST_UNDEFINED),
  m_MagnitudePresent(false),
  m_NumberOfRows(0),
  m_NumberOfColumns(0),
  m_NumberOfSlicesMR(0),
  m_NumberOfFrames(0),
  m_NumberOfPhases(0),
  m_FramesInfo(NULL),
  m_PCVelocity(0.0),
  m_VelocityComponent(VC_UNKNOWN),
  m_ComplexImageComponent(CIC_UNKNOWN)
{
  this->Read();
}

EnhancedMRI::~EnhancedMRI()
{
  if ( this->m_FramesInfo )
    {
      delete []this->m_FramesInfo;
      this->m_FramesInfo = NULL;
    }
}

StatusType EnhancedMRI::Read()
{
  gdcm::Reader reader;
  reader.SetFileName(this->GetFileName().c_str());
  if( !reader.Read() )
    {
    this->m_Status = ST_NOREAD;
    }
  return this->ReadDataSet(reader.GetFile().GetDataSet());
}

StatusType EnhancedMRI::ReadDataSet(const gdcm::DataSet &ds)
{
  this->ReadGlobalParameters( ds );
  if ( this->GetStatus() != OK )
    {
      return this->GetStatus();
    }
  this->m_Status = this->ReadFramesInfo(ds);
  if ( this->GetStatus() != OK )
    {
      return this->GetStatus();
    }
  return OK;
}

void EnhancedMRI::Print( std::ostream &_os, itk::Indent indent ) const
{
  if ( this->m_Status == OK ) {
    itk::Indent ind1 = indent.GetNextIndent();
    _os << "Philip PhaseContrast MR Image 4D --\n"
        << ind1 << "ComplexImageComponent = " << GetComplexImageComponentName( this->GetComplexImageComponent() ) << "\n"
        << ind1 << "NumberOfRows = " << this->GetNumberOfRows() << "\n"
        << ind1 << "NumberOfColumns = " << this->GetNumberOfColumns() << "\n"
        << ind1 << "NumberOfSlicesMR = " << this->GetNumberOfSlicesMR() << "\n"
        << ind1 << "NumberOfFrames = " << this->GetNumberOfFrames() << "\n"
        << ind1 << "NumberOfPhases = " << this->GetNumberOfPhases() << "\n"
        << ind1 << "Velocity Component = " << GetVelocityComponentName( this->GetVelocityComponent() ) << "\n"
        << ind1 << "PCVelocity = " << this->GetPCVelocity() << "\n";
  } else {
    _os << "Invalid Philip PhaseContrast MR Image 4D: "
        << GetStatusDescription( this->m_Status );
  }
}

StatusType EnhancedMRI::ScanDirectory(const char *dir,
                                      std::string &pathFlowX,
                                      std::string &pathFlowY,
                                      std::string &pathFlowZ)
{
  pathFlowX = "";
  pathFlowY = "";
  pathFlowZ = "";
  pcmr::StatusType status = EnhancedMRI::ScanDirectory0(dir,
                                                        pathFlowX,
                                                        pathFlowY,
                                                        pathFlowZ);
  if (pathFlowX == "")
    {
    std::cout << "PhilipsPCMR_ScanDirectory: X component not found\n";
    return pcmr::NO_ENHANCEDMRI;
    }
  if (pathFlowY == "")
    {
    std::cout << "PhilipsPCMR_ScanDirectory: Y component not found\n";
    return pcmr::NO_ENHANCEDMRI;
    }
  if (pathFlowZ == "")
    {
    std::cout << "PhilipsPCMR_ScanDirectory: Z component not found\n";
    return pcmr::NO_ENHANCEDMRI;
    }
  return pcmr::OK;
}

StatusType EnhancedMRI::TryEnhancedDICOM(const char *filename,
                                         std::string &pathFlowX,
                                         std::string &pathFlowY,
                                         std::string &pathFlowZ)
{
  StatusType status;
  gdcm::Reader reader;
  reader.SetFileName(filename);
  if(!reader.Read())
    {
    std::cout << "Could not read: \"" << filename << "\"" << std::endl;
    return pcmr::OK;
    }
  // The output of gdcm::Reader is a gdcm::File
  gdcm::File &file = reader.GetFile();

  if (IsDICOMDIR(file))
    {
    std::cout << "Found DICOMDIR: \"" << filename << "\"" << std::endl;
    return OK;
    }
  // the dataset is the the set of element we are interested in:
  gdcm::DataSet &ds = file.GetDataSet();
  if ((status=EnhancedMRI::CheckManufacturer(ds)) != OK )
    {
    std::cout << "EnhancedMRI::CheckManufacturer: "
              << GetStatusDescription(status) << std::endl;
    return OK;
    }
  if ((status=EnhancedMRI::CheckEnhancedMRImageType(ds)) != OK )
    {
    std::cout << "EnhancedMRI::CheckEnhancedMRImageType: "
              << GetStatusDescription(status) << std::endl;
    return OK;
    }
  if ((status=EnhancedMRI::CheckEnhancedMRSOPClassUID(ds)) != OK )
    {
    std::cout << "EnhancedMRI::CheckEnhancedMRSOPClassUID: "
              << GetStatusDescription(status) << std::endl;
    return OK;
    }
  if ( (status=EnhancedMRI::CheckModalityMR(ds)) != OK )
    {
    std::cout << "EnhancedMRI::CheckModalityMR: "
              << GetStatusDescription(status) << std::endl;
      return OK;
    }
  if ( (status=EnhancedMRI::CheckFlowEncodeAcquisition(ds)) != OK )
    {
    std::cout << "EnhancedMRI::CheckFlowEncodeAcquisition: "
              << pcmr::GetStatusDescription(status) << std::endl;
      return OK;
    }
  if ( (status=EnhancedMRI::CheckMRAcquisitionType3D(ds)) != OK )
    {
    std::cout << "EnhancedMRI::CheckMRAcquisitionType3D: "
              << GetStatusDescription(status) << std::endl;
      return OK;
    }
  if ( (status=EnhancedMRI::CheckPhaseContrast(ds)) != OK )
    {
    std::cout << "EnhancedMRI::CheckPhaseContrast: "
              << GetStatusDescription(status) << std::endl;
      return OK;
    }
  float pcv[3];
  if ((status=EnhancedMRI::ReadPCVelocity(ds,pcv)) != OK )
    {
    return status;
    }
  int i;
  for (i = 0; i < 3; i++)
    {
    if (pcv[i] != 0.0)
      {
      break;
      }
    }
  if (i == 3)
    {
    return pcmr::NULL_PCV;
    }
  // REVIEW: this index mapping does not correspond to the actual flow
  // component.
  // FH:     0     0    150     (this is phase Y sequence "IM_0005")
  // AP:     0     150  0       (this is phase X sequence "IM_0001")
  // RL:     150  0     0       (this is phase Z sequence "IM_0003")
  switch (i)
    {
    case 0:
      if (pathFlowZ == "")
        {
        pathFlowZ = filename;
        }
      else
        {
        std::cout << "PhilipsPCMR_TryEnhancedDICOM: Z component already found at file " << pathFlowZ << ", while processing file " << filename << std::endl;
        }
      break;
    case 1:
      if (pathFlowX == "")
        {
        pathFlowX = filename;
        }
      else
        {
        std::cout << "PhilipsPCMR_TryEnhancedDICOM: X component already found at file " << pathFlowX << ", while processing file " << filename << std::endl;
        }
      break;
    default:
      if (pathFlowY == "")
        {
        pathFlowY = filename;
        }
      else
        {
        std::cout << "PhilipsPCMR_TryEnhancedDICOM: Y component already found at file " << pathFlowY << ", while processing file " << filename << std::endl;
        }
      break;
    }
  return OK;
}

StatusType EnhancedMRI::ScanDirectory0(const char *dir,
                                       std::string &pathFlowX,
                                       std::string &pathFlowY,
                                       std::string &pathFlowZ)
{
  path pathDir(dir);
  std::cout << "Scanning directory \"" << dir << "\"" << std::endl;
  for (directory_iterator it = directory_iterator(pathDir);
       it != directory_iterator(); it++)
    {
    pcmr::StatusType status;
    if (is_directory(it->status()))
      {
      status = EnhancedMRI::ScanDirectory0(it->path().string().c_str(),
                                           pathFlowX,
                                           pathFlowY,
                                           pathFlowZ);
      }
    else
      {
      status = EnhancedMRI::TryEnhancedDICOM(it->path().string().c_str(),
                                             pathFlowX,
                                             pathFlowY,
                                             pathFlowZ);
      }
    if (status != OK)
      {
      std::cout << "aborted directory scanning at file "
                << it->path()
                << ": " << GetStatusDescription(status) << std::endl;
      return status;
      }
    }
  return OK;
}

StatusType EnhancedMRI::ReadGlobalParameters( const gdcm::DataSet &ds )
{
  int i;

  if ( (this->m_Status=EnhancedMRI::CheckManufacturer( ds )) != OK )
    {
      return this->m_Status;
    }
  if ( (this->m_Status=EnhancedMRI::CheckEnhancedMRImageType( ds )) != OK )
    {
      return this->m_Status;
    }
  if ( (this->m_Status=EnhancedMRI::CheckEnhancedMRSOPClassUID( ds )) != OK )
    {
      return this->m_Status;
    }
  if ( (this->m_Status=EnhancedMRI::CheckModalityMR( ds )) != OK )
    {
      return this->m_Status;
    }
  if ( (this->m_Status=EnhancedMRI::CheckFlowEncodeAcquisition( ds )) != OK )
    {
      return this->m_Status;
    }
  if ( (this->m_Status=EnhancedMRI::CheckMRAcquisitionType3D( ds )) != OK )
    {
      return this->m_Status;
    }
  if ( (this->m_Status=EnhancedMRI::CheckPhaseContrast( ds )) != OK )
    {
      return this->m_Status;
    }
  float pcv[3];
  this->m_IndexPCVelocity = -1;
  if ( (this->m_Status=EnhancedMRI::ReadPCVelocity( ds, pcv )) != OK )
    {
      return this->m_Status;
    }
  for ( i = 0; i < 3; i++ )
    {
    if ( pcv[i] != 0.0 )
      {
      break;
      }
    }
  if ( i == 3 )
    {
      return NULL_PCV;
    }
  this->m_PCVelocity = pcv[i];
  this->m_IndexPCVelocity = i;
  // REVIEW: this index mapping does not correspond to the actual flow
  // component.
  // FH:     0     0    150     (this is phase Y sequence "IM_0005")
  // AP:     0     150  0       (this is phase X sequence "IM_0001")
  // RL:     150  0     0       (this is phase Z sequence "IM_0003")
  switch ( i )
    {
    case 0:
      this->m_VelocityComponent = Z;
      break;
    case 1:
      this->m_VelocityComponent = X;
      break;
    default:
      this->m_VelocityComponent = Y;
      break;
    }
  // Global ComplexImageComponent
  std::string cicomp;
  if ( (this->m_Status=EnhancedMRI::ReadComplexImageComponent( ds, cicomp )) != OK )
    {
      return this->m_Status;
    }
  for ( i = CIC_UNKNOWN+1; i < ComplexImageComponentTypeEnd; i++ )
    {
      const char* cic = GetComplexImageComponentName( i );
      if ( !cicomp.compare( 0, strlen( cic ), cic ) )
        {
          this->m_ComplexImageComponent = static_cast<ComplexImageComponentType>( i );
          break;
        }
    }
  if ( i == ComplexImageComponentTypeEnd )
    {
      this->m_Status = UNK_COMPLEX_IC;
      return this->m_Status;
    }
  if ( (this->m_Status=EnhancedMRI::ReadNumberOfFrames( ds, this->m_NumberOfFrames )) != OK )
    {
      return this->m_Status;
    }
  if ( (this->m_Status=EnhancedMRI::ReadNumberOfPhasesMR( ds, this->m_NumberOfPhases )) != OK )
    {
      return this->m_Status;
    }
  if ( (this->m_Status=EnhancedMRI::ReadVolumeInfo( ds,
                                                    this->m_NumberOfRows,
                                                    this->m_NumberOfColumns,
                                                    this->m_NumberOfSlicesMR )) != OK )
    {
      return this->m_Status;
    }
  return this->m_Status;
}

StatusType EnhancedMRI::ReadFramesInfo(const gdcm::DataSet &ds)
{
  StatusType status;
  //(5200,9230) SQ (Sequence with undefined length #=2050)  # u/l, 1 PerFrameFunctionalGroupsSequence
  // http://www.dabsoft.ch/dicom/6/6/#(5200,9230)
  gdcm::Tag tsq_PerFrameInfo(0x5200,0x9230);
  if( !ds.FindDataElement( tsq_PerFrameInfo ) )
    {
      return TAG_NOTFOUND;
    }
  const gdcm::DataElement &sq_PerFrameInfo = ds.GetDataElement( tsq_PerFrameInfo );
  const gdcm::SequenceOfItems *sqi = sq_PerFrameInfo.GetValueAsSQ();
  if ( !sqi )
    {
      return SQ_EMPTY;
    }
  size_t size = this->GetNumberOfFrames();
  if (!this->m_FramesInfo)
    {
    this->m_FramesInfo = new FrameType[size];
    }
  if ( !this->m_FramesInfo )
    {
      return NO_MEMORY;
    }
  for( unsigned int i = 0; i < size; i++ )
    {
      const gdcm::Item & item = sqi->GetItem( i + 1 );
      const gdcm::DataSet& nds_item = item.GetNestedDataSet();
      // (0018,9226) SQ (Sequence with undefined length #=1)    # u/l, 1 MRImageFrameTypeSequence ## 4
      //http://www.dabsoft.ch/dicom/6/6/#(0018,9226)
      gdcm::Tag tsq_MRIFType(0x0018,0x9226);
      if( !nds_item.FindDataElement( tsq_MRIFType ) )
        {
        delete [](this->m_FramesInfo);
        this->m_FramesInfo = NULL;
        return TAG_NOTFOUND;
        }
      const gdcm::DataElement &sq_MRIFType = nds_item.GetDataElement( tsq_MRIFType );
      const gdcm::SequenceOfItems *sqi_MRIFType = sq_MRIFType.GetValueAsSQ();
      if ( !sqi_MRIFType )
        {
        delete [](this->m_FramesInfo);
        this->m_FramesInfo = NULL;
        return SQ_EMPTY;
        }

      const gdcm::Item & item_MRIFType = sqi_MRIFType->GetItem( 1 );
      const gdcm::DataSet& nds_MRIFType = item_MRIFType.GetNestedDataSet();
      std::string cicomp;
      if ( (status=EnhancedMRI::ReadComplexImageComponent( nds_MRIFType, cicomp )) != OK )
        {
        delete [](this->m_FramesInfo);
          this->m_FramesInfo = NULL;
          return status;
        }
      this->m_FramesInfo[i].m_ComplexImageComponent = GetComplexImageComponentId( cicomp.c_str() );
      //(2005,140f) SQ (Sequence with undefined length #=1)  # u/l, 1 Unknown Tag & Data
      // PHILIP PRIVATE (2005 is odd), this tag appears in (0020,9222) SQ (Sequence with undefined length #=5)     # u/l, 1 DimensionIndexSequence
      // (0020,9167) AT (2005,140f)                              #   4, 1 FunctionalGroupPointer
      gdcm::Tag tsq_FGroup(0x2005,0x140f);
      if( !nds_item.FindDataElement( tsq_FGroup ) )
        {
        delete [](this->m_FramesInfo);
          this->m_FramesInfo = NULL;
          return TAG_NOTFOUND;
        }
      const gdcm::DataElement &sq_FGroup = nds_item.GetDataElement( tsq_FGroup );
      const gdcm::SequenceOfItems *sqi_FGroup = sq_FGroup.GetValueAsSQ();
      if ( !sqi_FGroup )
        {
        delete [](this->m_FramesInfo);
        this->m_FramesInfo = NULL;
        return SQ_EMPTY;
        }
      const gdcm::Item & item_FGroup = sqi_FGroup->GetItem( 1 );
      const gdcm::DataSet& nds_FGroup = item_FGroup.GetNestedDataSet();

      if ( (status=EnhancedMRI::ReadPhaseNumber( nds_FGroup,
                                                 this->m_FramesInfo[i].m_PhaseNumber )) != OK )
        {
        delete [](this->m_FramesInfo);
        this->m_FramesInfo = NULL;
        return status;
        }

      if ( (status=EnhancedMRI::ReadSliceNumberMR(nds_FGroup,
                                                  this->m_FramesInfo[i].m_SliceNumberMR )) != OK )
        {
        delete [](this->m_FramesInfo);
        this->m_FramesInfo = NULL;
        return status;
        }

      if ( (status=EnhancedMRI::ReadPixelSpacing(nds_FGroup,
                                                 this->m_FramesInfo[i].m_PixelSpacing )) != OK )
        {
        delete [](this->m_FramesInfo);
        this->m_FramesInfo = NULL;
        return status;
        }
      if ( (status=EnhancedMRI::ReadSliceThickness(nds_FGroup,
                                                   this->m_FramesInfo[i].m_SliceThickness)) != OK)
        {
        delete [](this->m_FramesInfo);
        this->m_FramesInfo = NULL;
        return status;
        }
      if ( (status=EnhancedMRI::ReadSpacingBetweenSlices(nds_FGroup,
                                                         this->m_FramesInfo[i].m_SpacingBetweenSlices ))!= OK)
        {
        delete [](this->m_FramesInfo);
        this->m_FramesInfo = NULL;
        return status;
        }

      if ( (status=EnhancedMRI::ReadTriggerTime(nds_FGroup,
                                                this->m_FramesInfo[i].m_TriggerTime ))!= OK)
        {
        delete [](this->m_FramesInfo);
        this->m_FramesInfo = NULL;
        return status;
        }

      if ( (status=EnhancedMRI::ReadRescaleIntercept(nds_FGroup,
                                                     this->m_FramesInfo[i].m_RescaleIntercept ))!= OK)
        {
        delete [](this->m_FramesInfo);
        this->m_FramesInfo = NULL;
        return status;
        }
      if ( (status=EnhancedMRI::ReadRescaleSlope(nds_FGroup,
                                                 this->m_FramesInfo[i].m_RescaleSlope ))!= OK)
        {
        delete [](this->m_FramesInfo);
        this->m_FramesInfo = NULL;
        return status;
        }
      if (!(i % 100))
        {
        this->InvokeEvent(AwakeEvent());
        }
    }
  return OK;
}

StatusType EnhancedMRI::CheckManufacturer(const gdcm::DataSet &ds)
{
  // (0008,0070) LO [Philips Medical Systems]          #  24, 1 Manufacturer
  // http://www.dabsoft.ch/dicom/6/6/#(0008,0070)
  gdcm::Element<gdcm::VR::LO,gdcm::VM::VM1> el_Manufacturer;
  if ( pcmr::TryTag<gdcm::VR::LO,gdcm::VM::VM1>( 0x0008, 0x0070, ds,
                                           el_Manufacturer ) == OK)
    {
      const gdcm::VRToType<gdcm::VR::LO>::Type & manufacturer = el_Manufacturer.GetValue();
      const std::string philipsID0("Philips Medical Systems");
      const std::string philipsID1("Philips Healthcare");
      if ( !manufacturer.compare( 0, philipsID0.length(), philipsID0 ) ||
           !manufacturer.compare( 0, philipsID1.length(), philipsID1 ))
        {
          return OK;
        }
      else
        {
          return NO_PHILIPS;
        }
    }
  return TAG_NOTFOUND;
}

StatusType EnhancedMRI::CheckEnhancedMRImageType( const gdcm::DataSet &ds )
{
  // (0008,0008) CS [DERIVED\PRIMARY\FLOW_ENCODED\MIXED]     #  34, 4 ImageType
  // http://www.dabsoft.ch/dicom/6/6/#(0008,0008)
  gdcm::Element<gdcm::VR::CS,gdcm::VM::VM4> el_ImageType;
  if ( pcmr::TryTag<gdcm::VR::CS,gdcm::VM::VM4>( 0x0008, 0x0008, ds,
                                           el_ImageType ) == OK)
    {
      const char *itype[] = { "DERIVED", "PRIMARY", "FLOW_ENCODED", "MIXED" };
      for ( int i = 0; i < el_ImageType.GetLength(); i++ )
        {
          const gdcm::VRToType<gdcm::VR::CS>::Type & _itype = el_ImageType.GetValue( i );
#define PCMR_DUMP_IMAGE_TYPE
#if defined(PCMR_DUMP_IMAGE_TYPE)
          std::cout << (i?"\\":"")
                    <<  _itype
                    << (i==(el_ImageType.GetLength()-1)?"\n":"");
#endif
          if ( _itype.compare( itype[ i ] ) )
            {
              return IMG_MISSMATCH;
            }
        }
      return OK;
    }
  return TAG_NOTFOUND;
}

StatusType EnhancedMRI::ReadComplexImageComponent( const gdcm::DataSet &ds, std::string &comp )
{
  //(0008,9208) CS [MIXED]                        #   6, 1 ComplexImageComponent
  // http://www.dabsoft.ch/dicom/6/6/#(0008,9208)
  gdcm::Element<gdcm::VR::CS,gdcm::VM::VM1> el_ComplexImageComponent;
  if ( pcmr::TryTag<gdcm::VR::CS,gdcm::VM::VM1>( 0x0008, 0x9208, ds,
                                           el_ComplexImageComponent ) == OK )
    {
      comp = el_ComplexImageComponent.GetValue();
      return OK;
    }
  return TAG_NOTFOUND;
}

StatusType EnhancedMRI::ReadModality( const gdcm::DataSet &ds, std::string &modality )
{
  // (0008,0060) CS [MR]                 #   2, 1 Modality
  // http://www.dabsoft.ch/dicom/6/6/#(0008,0060)
  gdcm::Element<gdcm::VR::CS,gdcm::VM::VM1> el_Modality;
  if ( pcmr::TryTag<gdcm::VR::CS,gdcm::VM::VM1>( 0x0008, 0x0060, ds,
                                           el_Modality ) == OK)
    {
    modality = el_Modality.GetValue().c_str();
    boost::trim_right( modality );
    return OK;
    }
  return TAG_NOTFOUND;
}

StatusType EnhancedMRI::CheckModalityMR( const gdcm::DataSet &ds )
{
  // (0008,0060) CS [MR]                 #   2, 1 Modality
  // http://www.dabsoft.ch/dicom/6/6/#(0008,0060)
  gdcm::Element<gdcm::VR::CS,gdcm::VM::VM1> el_Modality;
  if ( pcmr::TryTag<gdcm::VR::CS,gdcm::VM::VM1>( 0x0008, 0x0060, ds,
                                           el_Modality ) == OK)
    {
      const gdcm::VRToType<gdcm::VR::CS>::Type & modality = el_Modality.GetValue();

      if ( modality.compare( "MR" ) )
        {
          return NO_MODMR;
        }
      return OK;
    }
  return TAG_NOTFOUND;
}

StatusType EnhancedMRI::CheckFlowEncodeAcquisition( const gdcm::DataSet &ds )
{
  //(0008,9209) CS [FLOW_ENCODED]               #  12, 1 AcquisitionContrast
  // http://www.dabsoft.ch/dicom/6/6/#(0008,9209)
  gdcm::Element<gdcm::VR::CS,gdcm::VM::VM1> el_AcquisitionContrast;
  if ( pcmr::TryTag<gdcm::VR::CS,gdcm::VM::VM1>( 0x0008, 0x9209, ds,
                                           el_AcquisitionContrast ) == OK)
    {
      const gdcm::VRToType<gdcm::VR::CS>::Type & acquisition = el_AcquisitionContrast.GetValue();

      if ( acquisition.compare( "FLOW_ENCODED" ) )
        {
          return NO_FLOWENCODED;
        }
      return OK;
    }
  return TAG_NOTFOUND;
}

StatusType EnhancedMRI::ReadMRAcquisitionType( const gdcm::DataSet &ds,
                                               std::string &acquisitionType  )
{
  //(0018,0023) CS [3D]                         #   2, 1 MRAcquisitionType
  // http://www.dabsoft.ch/dicom/6/6/#(0018,0023)
  gdcm::Element<gdcm::VR::CS,gdcm::VM::VM1> el_MRAcquisitionType;
  if ( pcmr::TryTag<gdcm::VR::CS,gdcm::VM::VM1>( 0x0018, 0x0023, ds,
                                           el_MRAcquisitionType ) == OK)
    {
    acquisitionType = el_MRAcquisitionType.GetValue().c_str();
    boost::trim_right( acquisitionType );
    return OK;
    }
  return TAG_NOTFOUND;
}

StatusType EnhancedMRI::CheckMRAcquisitionType3D( const gdcm::DataSet &ds )
{
  //(0018,0023) CS [3D]                         #   2, 1 MRAcquisitionType
  // http://www.dabsoft.ch/dicom/6/6/#(0018,0023)
  gdcm::Element<gdcm::VR::CS,gdcm::VM::VM1> el_MRAcquisitionType;
  if ( pcmr::TryTag<gdcm::VR::CS,gdcm::VM::VM1>( 0x0018, 0x0023, ds,
                                           el_MRAcquisitionType ) == OK)
    {
      const gdcm::VRToType<gdcm::VR::CS>::Type & acquisition = el_MRAcquisitionType.GetValue();

      if ( acquisition.compare( "3D" ) )
        {
          return NO_ACQ3D;
        }
      return OK;
    }
  return TAG_NOTFOUND;
}

StatusType EnhancedMRI::CheckPhaseContrast( const gdcm::DataSet &ds )
{
  //(0018,9014) CS [YES]                        #   4, 1 PhaseContrast
  // http://www.dabsoft.ch/dicom/6/6/#(0018,9014)
  gdcm::Element<gdcm::VR::CS,gdcm::VM::VM1> el_PhaseContrast;
  if ( pcmr::TryTag<gdcm::VR::CS,gdcm::VM::VM1>( 0x0018, 0x9014, ds,
                                           el_PhaseContrast ) == OK)
    {
      const gdcm::VRToType<gdcm::VR::CS>::Type & phase_contrast = el_PhaseContrast.GetValue();
      const char expected[] = "YES";
      if ( phase_contrast.compare( 0, strlen( expected ), expected ) )
        {
          return NO_PHASECONTRAST;
        }
      return OK;
    }
  return TAG_NOTFOUND;
}

StatusType EnhancedMRI::ReadImageType( const gdcm::DataSet &ds,
                                       std::vector<std::string> &imageType  )
{
  // (0008,0008) CS [DERIVED\PRIMARY\FLOW_ENCODED\MIXED]     #  34, 4 ImageType
  // http://www.dabsoft.ch/dicom/6/6/#(0008,0008)
  gdcm::Element<gdcm::VR::CS,gdcm::VM::VM5> el_ImageType;
  if ( pcmr::TryTag<gdcm::VR::CS,gdcm::VM::VM5>( 0x0008, 0x0008, ds,
                                                 el_ImageType ) == OK)
    {
    int i;
    for ( i = 0; i < el_ImageType.GetLength(); i++ )
      {
      imageType.push_back( el_ImageType.GetValue( i ).c_str() );
      }
    boost::trim_right( imageType[ i - 1 ] );
    return OK;
    }
  return TAG_NOTFOUND;
}

StatusType EnhancedMRI::ReadImageType( const gdcm::DataSet &ds,
                                       std::string &imageType )
{
  StatusType status;

  std::vector<std::string> _imageType;
  status = EnhancedMRI::ReadImageType( ds, _imageType );
  if ( status != OK )
    {
    return status;
    }
  imageType = "";
  for( int i = 0; i < _imageType.size(); i++ )
    {
    if ( i )
      {
      imageType += '\\';
      }
    imageType += _imageType[i];
    }
  return OK;
}

StatusType EnhancedMRI::ReadSeriesInstanceUID( const gdcm::DataSet &ds, std::string &serieUID )
{
  // (0020,000e) UI [1.3.46.670589.11.17203.5.0.8916.2014121015370642411]         # 52,1 Series Instance UID
  gdcm::Element<gdcm::VR::UI,gdcm::VM::VM1> el_SeriesInstance;
  if ( pcmr::TryTag<gdcm::VR::UI,gdcm::VM::VM1>( 0x0020, 0x000e, ds,
                                                 el_SeriesInstance ) == OK)
    {
    const gdcm::VRToType<gdcm::VR::LO>::Type &tagValue = el_SeriesInstance.GetValue();

    serieUID = tagValue.c_str();
    boost::trim_right( serieUID );
    return OK;
    }
  return TAG_NOTFOUND;
}

StatusType EnhancedMRI::ReadSOPClassUID( const gdcm::DataSet &ds, std::string &sopcUID )
{
  // (0008,0016) UI =                      #  28, 1 SOPClassUID
  // http://www.dabsoft.ch/dicom/4/B.5/
  // http://www.dabsoft.ch/dicom/6/6/#(0008,0016)
  gdcm::Element<gdcm::VR::UI,gdcm::VM::VM1> el_MediaStorage;
  if ( pcmr::TryTag<gdcm::VR::UI,gdcm::VM::VM1>( 0x0008, 0x0016, ds,
                                           el_MediaStorage ) == OK)
    {
    const gdcm::VRToType<gdcm::VR::LO>::Type &tagValue = el_MediaStorage.GetValue();

    sopcUID = tagValue.c_str();
    boost::trim_right( sopcUID );
    return OK;
    }
  return TAG_NOTFOUND;
}


StatusType EnhancedMRI::IsMRImageStorage( const gdcm::DataSet &ds )
{
  std::string value;
  StatusType status = EnhancedMRI::ReadSOPClassUID( ds, value );
  if ( status != OK )
    {
    return status;
    }
  const std::string MRI_ID( "1.2.840.10008.5.1.4.1.1.4" );
  if ( !value.compare( MRI_ID ) )
    {
    return OK;
    }
  else
    {
    return NO_MRI_STORAGE;
    }
}

StatusType EnhancedMRI::IsEnhancedMRImageStorage( const gdcm::DataSet &ds )
{
  std::string value;
  StatusType status = EnhancedMRI::ReadSOPClassUID( ds, value );
  if ( status != OK )
    {
    return status;
    }
  const std::string EnhancedMRI_ID( "1.2.840.10008.5.1.4.1.1.4.1" );
  if ( !value.compare( EnhancedMRI_ID ) )
    {
    return OK;
    }
  else
    {
    return NO_ENHANCEDMRI;
    }
}

StatusType EnhancedMRI::CheckEnhancedMRSOPClassUID( const gdcm::DataSet &ds )
{
  // (0008,0016) UI =EnhancedMRImageStorage              #  28, 1 SOPClassUID
  // http://www.dabsoft.ch/dicom/4/B.5/
  // http://www.dabsoft.ch/dicom/6/6/#(0008,0016)
  gdcm::Element<gdcm::VR::UI,gdcm::VM::VM1> el_MediaStorage;
  if ( pcmr::TryTag<gdcm::VR::UI,gdcm::VM::VM1>( 0x0008, 0x0016, ds,
                                           el_MediaStorage ) == OK)
    {
      const gdcm::VRToType<gdcm::VR::LO>::Type & SOPClassUID = el_MediaStorage.GetValue();
      const std::string EnhancedMRI_ID( "1.2.840.10008.5.1.4.1.1.4.1" );
      if ( !SOPClassUID.compare( 0, EnhancedMRI_ID.length(), EnhancedMRI_ID ) )
        {
          return OK;
        }
      else
        {
          return NO_ENHANCEDMRI;
        }
    }
  return TAG_NOTFOUND;
}

StatusType EnhancedMRI::ReadNumberOfFrames( const gdcm::DataSet &ds, unsigned int &nframes )
{
  //(0028,0008) IS [2050]                       #   4, 1 NumberOfFrames
  // http://www.dabsoft.ch/dicom/6/6/#(0028,0008)
  gdcm::Element<gdcm::VR::IS,gdcm::VM::VM1> el_NumberOfFrames;
  if ( pcmr::TryTag<gdcm::VR::IS,gdcm::VM::VM1>( 0x0028, 0x0008, ds,
                                           el_NumberOfFrames ) == OK)
    {
      nframes = el_NumberOfFrames.GetValue();
      return OK;
    }
  return TAG_NOTFOUND;
}

StatusType EnhancedMRI::ReadNumberOfRows( const gdcm::DataSet &ds, unsigned int &nrows )
{
  //(0028,0010) US 256                          #   2, 1 Rows
  // http://www.dabsoft.ch/dicom/6/6/#(0028,0010)
  gdcm::Element<gdcm::VR::US,gdcm::VM::VM1> el_Rows;
  if ( pcmr::TryTag<gdcm::VR::US,gdcm::VM::VM1>( 0x0028, 0x0010, ds,
                                           el_Rows ) == OK)
    {
      nrows = el_Rows.GetValue();
      return OK;
    }
  return TAG_NOTFOUND;
}

StatusType EnhancedMRI::ReadNumberOfColumns( const gdcm::DataSet &ds, unsigned int &ncolumns )
{
  //(0028,0011) US 256                          #   2, 1 Columns
  // http://www.dabsoft.ch/dicom/6/6/#(0028,0011)
  gdcm::Element<gdcm::VR::US,gdcm::VM::VM1> el_Columns;
  if ( pcmr::TryTag<gdcm::VR::US,gdcm::VM::VM1>( 0x0028, 0x0011, ds,
                                           el_Columns ) == OK)
    {
      ncolumns = el_Columns.GetValue();
      return OK;
    }
  return TAG_NOTFOUND;
}

StatusType EnhancedMRI::ReadPixelSpacing( const gdcm::DataSet &ds, float spacing[] )
{
  //(0028,0030) DS [1.25\1.25]                  #  10, 2 PixelSpacing
  // http://www.dabsoft.ch/dicom/6/6/#(0028,0030)
  gdcm::Element<gdcm::VR::DS,gdcm::VM::VM2> el_PixelSpacing;
  if ( pcmr::TryTag<gdcm::VR::DS,gdcm::VM::VM2>( 0x0028, 0x0030, ds,
                                           el_PixelSpacing ) == OK)
    {
      spacing[0] = el_PixelSpacing.GetValue( 0 );
      spacing[1] = el_PixelSpacing.GetValue( 1 );
      return OK;
    }
  return TAG_NOTFOUND;
}

StatusType EnhancedMRI::ReadSliceThickness( const gdcm::DataSet &ds, float &sliceThickness )
{
  // (0018,0050) DS [3]                         #   2, 1 SliceThickness
  // http://www.dabsoft.ch/dicom/6/6/#(0018,0050)
  gdcm::Element<gdcm::VR::DS,gdcm::VM::VM1> el_SliceThickness;
  if ( pcmr::TryTag<gdcm::VR::DS,gdcm::VM::VM1>( 0x0018, 0x0050, ds,
                                           el_SliceThickness ) == OK)
    {
      sliceThickness = el_SliceThickness.GetValue();
      return OK;
    }
  return TAG_NOTFOUND;
}

StatusType EnhancedMRI::ReadSpacingBetweenSlices( const gdcm::DataSet &ds, float & spacingBetweenSlices )
{
  // (0018,0088) DS [1.5]                                    #   4, 1 SpacingBetweenSlices
  // http://www.dabsoft.ch/dicom/6/6/#(0018,0088)
  gdcm::Element<gdcm::VR::DS,gdcm::VM::VM1> el_SpacingBetweenSlices;
  if ( pcmr::TryTag<gdcm::VR::DS,gdcm::VM::VM1>( 0x0018, 0x0088, ds,
                                           el_SpacingBetweenSlices ) == OK)
    {
      spacingBetweenSlices = el_SpacingBetweenSlices.GetValue();
      return OK;
    }
  return TAG_NOTFOUND;
}

StatusType EnhancedMRI::ReadTriggerTime( const gdcm::DataSet &ds, float & triggerTime )
{
  // (0018,1060) DS [1113]                                   #   4, 1 TriggerTime
  // http://www.dabsoft.ch/dicom/6/6/#(0018,1060)
  gdcm::Element<gdcm::VR::DS,gdcm::VM::VM1> el_TriggerTime;
  if ( pcmr::TryTag<gdcm::VR::DS,gdcm::VM::VM1>( 0x0018, 0x1060, ds,
                                           el_TriggerTime ) == OK)
    {
      triggerTime = el_TriggerTime.GetValue();
      return OK;
    }
  return TAG_NOTFOUND;
}

StatusType EnhancedMRI::ReadRescaleIntercept( const gdcm::DataSet &ds, float & rescaleIntercept )
{
  // try private tag firs
  // (2005,1409) DS [-200]                                             # 4,1 Rescale Intercept Philips
  gdcm::Element<gdcm::VR::DS,gdcm::VM::VM1> el_RescaleIntercept;
  if ( pcmr::TryTag<gdcm::VR::DS,gdcm::VM::VM1>(0x2005, 0x1409, ds,
                                                el_RescaleIntercept ) == OK)
    {
    rescaleIntercept = el_RescaleIntercept.GetValue();
    return OK;
    }
  // (0028,1052) DS [-200]                                   #   4, 1 RescaleIntercept
  // http://www.dabsoft.ch/dicom/6/6/#(0028,1052)
  if ( pcmr::TryTag<gdcm::VR::DS,gdcm::VM::VM1>( 0x0028, 0x1052, ds,
                                           el_RescaleIntercept ) == OK)
    {
      rescaleIntercept = el_RescaleIntercept.GetValue();
      return OK;
    }
  return TAG_NOTFOUND;
}

StatusType EnhancedMRI::ReadRescaleSlope( const gdcm::DataSet &ds, float & rescaleSlope )
{
  // try private tag first
  // (2005,140a) DS [0.09768009768009]                                 # 16,1 Rescale Slope Philips
  gdcm::Element<gdcm::VR::DS,gdcm::VM::VM1> el_RescaleSlope;
  if ( pcmr::TryTag<gdcm::VR::DS,gdcm::VM::VM1>(0x2005, 0x140a, ds,
                                                el_RescaleSlope ) == OK)
    {
    rescaleSlope = el_RescaleSlope.GetValue();
    return OK;
    }
  // (0028,1053) DS [0.09768009768009]                       #  16, 1 RescaleSlope
  // http://www.dabsoft.ch/dicom/6/6/#(0028,1053)
  if ( pcmr::TryTag<gdcm::VR::DS,gdcm::VM::VM1>( 0x0028, 0x1053, ds,
                                           el_RescaleSlope ) == OK)
    {
      rescaleSlope = el_RescaleSlope.GetValue();
      return OK;
    }
  return TAG_NOTFOUND;
}

StatusType EnhancedMRI::ReadNumberOfPhasesMR( const gdcm::DataSet &ds, unsigned int &nphases )
{
  //(2001,1017) SL 25                           #   4, 1 NumberOfPhasesMR
  // Philips private
  gdcm::Element<gdcm::VR::SL,gdcm::VM::VM1> el_NumberOfPhasesMR;
  if ( pcmr::TryTag<gdcm::VR::SL,gdcm::VM::VM1>( 0x2001, 0x1017, ds,
                                           el_NumberOfPhasesMR ) == OK)
    {
      nphases = el_NumberOfPhasesMR.GetValue();
      return OK;
    }
  return TAG_NOTFOUND;
}

StatusType EnhancedMRI::ReadPhaseNumber( const gdcm::DataSet &ds, unsigned int &fn )
{
  // (2001,1008) IS [1]                         #   2, 1 PhaseNumber
  // Philips private
  gdcm::Element<gdcm::VR::IS,gdcm::VM::VM1> el_PhaseNumber;
  if ( pcmr::TryTag<gdcm::VR::IS,gdcm::VM::VM1>( 0x2001, 0x1008, ds,
                                           el_PhaseNumber ) == OK)
    {
      fn = el_PhaseNumber.GetValue();
      return OK;
    }
  return TAG_NOTFOUND;
}

StatusType EnhancedMRI::ReadNumberOfSlicesMR( const gdcm::DataSet &ds, unsigned int &nslices )
{
  //(2001,1018) SL 41                           #   4, 1 NumberOfSlicesMR
  // Philips private
  gdcm::Element<gdcm::VR::SL,gdcm::VM::VM1> el_NumberOfSlicesMR;
  if ( pcmr::TryTag<gdcm::VR::SL,gdcm::VM::VM1>( 0x2001, 0x1018, ds,
                                           el_NumberOfSlicesMR ) == OK)
    {
      nslices = el_NumberOfSlicesMR.GetValue();
      return OK;
    }
  return TAG_NOTFOUND;
}

StatusType EnhancedMRI::ReadSliceNumberMR( const gdcm::DataSet &ds, unsigned int &sn )
{
  // (2001,100a) IS [1]             #   2, 1 SliceNumberMR
  // Philips private
  gdcm::Element<gdcm::VR::IS,gdcm::VM::VM1> el_SliceNumberMR;
  if ( pcmr::TryTag<gdcm::VR::IS,gdcm::VM::VM1>( 0x2001, 0x100a, ds,
                                           el_SliceNumberMR ) == OK)
    {
      sn = el_SliceNumberMR.GetValue();
      return OK;
    }
  return TAG_NOTFOUND;
}

StatusType EnhancedMRI::ReadPCVelocity( const gdcm::DataSet &ds, float pcv[] )
{
  //(2001,101a) FL 0\200\0                                  #  12, 3 PCVelocity
  // Philips private
  gdcm::Element<gdcm::VR::FL,gdcm::VM::VM1_n> el_PCVelocity;
  if ( pcmr::TryTag<gdcm::VR::FL,gdcm::VM::VM1_n>( 0x2001, 0x101a, ds,
                                             el_PCVelocity ) == OK)
    {
      if ( el_PCVelocity.GetLength() != 3 )
        {
          return INV_PCV_LENGTH;
        }
      for ( int i = 0; i < 3; i++ )
        {
          pcv[i] = el_PCVelocity.GetValue( i );
        }
      return OK;
    }
  return TAG_NOTFOUND;
}

StatusType EnhancedMRI::ReadVolumeInfo( const gdcm::DataSet &ds,
                                        unsigned int & rows,
                                        unsigned int & columns,
                                        unsigned int & slices )
{
  StatusType status = ST_UNDEFINED;

  if ( (status=EnhancedMRI::ReadNumberOfRows( ds, rows ) ) != OK )
    {
      return status;
    }

  if ( (status=EnhancedMRI::ReadNumberOfColumns( ds, columns ) ) != OK )
    {
      return status;
    }

  if ( (status=EnhancedMRI::ReadNumberOfSlicesMR( ds, slices ) ) != OK )
    {
      return status;
    }

  return OK;
}

END_PHILIPS_DECLS
