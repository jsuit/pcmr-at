#include <iostream>
#include "pcmrSiemensScanner.h"
#include "pcmrSiemensGDCMIO.h"
#include "gdcmPrivateTag.h"

#include "pcmrEvents.h"

#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>
using namespace boost::filesystem;

BEGIN_SIEMENS_DECLS

#define STRING_CMP( STR, VALUE )                 \
  STR.compare( 0, sizeof(VALUE)-1, VALUE )

StatusType PCStudyInfo::ReadFromDICOM(const char *pathDCM)
{
  gdcm::Reader reader;
  StatusType status;

  /*
  std::ifstream inputStream;
  inputStream.open(pathDCM, std::ios::binary);
  reader.SetStream(inputStream);
  */
  reader.SetFileName(pathDCM);
  if (!reader.Read())
    {
    std::cerr << "Could not read: \"" << pathDCM << "\"" << std::endl;
    return ST_NOREAD;
    }
  // The output of gdcm::Reader is a gdcm::File
  gdcm::File &file = reader.GetFile();
  // the dataset is the the set of element we are interested in:
  gdcm::DataSet &ds = file.GetDataSet();

  this->m_ImageComponentType = GetPhaseContrastComponent(ds);
  if (this->m_ImageComponentType == CIC_UNKNOWN)
    {
    return DCM_NO_PM;
    }

  status = ReadCardiacNumberOfImages(ds, this->m_NumberOfVolumes);   
  if (status!=OK)
    {
    return status;
    }
  if (this->m_NumberOfVolumes<=1)
    {
    return DCM_NO_MVOL;
    }
  ReadRepetitionTime(ds, this->m_TimeStep);
  // try to read the rest of the fields from CSA Header
  gdcm::CSAHeader csa;
  gdcm::PrivateTag csaTag(csa.GetCSAImageHeaderInfoTag());
  if(ds.FindDataElement(csaTag))
    {
    csa.LoadFromDataElement(ds.GetDataElement(csaTag));
    this->ReadOtherFieldsFromCSA(csa);
    }

  return OK;
}

StatusType PCStudyInfo::ReadOtherFieldsFromCSA(gdcm::CSAHeader &csa)
{
  // look for VelocityEncoding
  this->m_VelocityEncoding = 0.0;
  return GetCSAVelocityEncoding(csa, this->m_VelocityEncoding);
}

StatusType PCSliceInfo::ReadFromDataSet(const gdcm::DataSet &ds,
                                        ComplexImageComponentType &imageType)
{
  StatusType status;
  imageType = GetPhaseContrastComponent(ds);

  // (0008,1140) ?? (SQ)                    # 306,1 Referenced Image Sequence
  // (0018,0020) CS [GR]                    # 2,1-n Scanning Sequence
  // (0018,0023) ?? (CS) [3D]               # 2,1 MR Acquisition Type

  // REVIEW esto es temporal, hay que ser inmune a 2D o 3D
  if (CheckMRAcquisitionType(ds, "3D") != OK) 
    {
    return NO_ACQ3D;
    }
  // (0018,0089) ?? (IS) [128 ]             # 4,1 Number of Phase Encoding Steps

  if (imageType == MAGNITUDE || imageType == PHASE)
    {
    status = ReadInstanceNumber(ds, this->m_InstanceNumber);
    if (status != OK)
      {
      std::cerr <<  "Error '" << GetStatusDescription(status) 
                << "' while reading InstanceNumber" << std::endl;
      return status;
      }
    status = ReadTriggerTime(ds, this->m_TimeStep);
    if (status != OK)
      {
      std::cerr <<  "Error '" << GetStatusDescription(status) 
                << "' while reading TriggerTime" << std::endl;
      return status;
      }
    // try to read the rest of the fields from CSA Header
    gdcm::CSAHeader csa;
    gdcm::PrivateTag csaTag(csa.GetCSAImageHeaderInfoTag());
    if(ds.FindDataElement(csaTag))
      {
      csa.LoadFromDataElement(ds.GetDataElement(csaTag));
      this->ReadOtherFieldsFromCSA(csa);
      }
    return OK;
    }
  else
    {
    /*
    std::cerr <<  "Error '" << GetStatusDescription(DCM_NO_PM) 
              << "' while reading '" << pathDCM << "'\n";
    */
    return DCM_NO_PM;
    }
}

StatusType PCSliceInfo::ReadOtherFieldsFromCSA(gdcm::CSAHeader &csa)
{
  StatusType status1, status2;

  this->m_FlowEncodingDirection = -1;
  status1 = pcmr::Siemens::GetCSAEncodingDirection(csa, this->m_FlowEncodingDirection);
  this->m_PhaseEncodingDirectionPositive = -1;
  status2 = pcmr::Siemens::GetCSADirectionPositive(csa, this->m_PhaseEncodingDirectionPositive);
  if (status1 != OK)
    {
    return status1;
    }
  else if (status2 != OK)
    {
    return status2;
    }
  return OK;
}

StatusType Scanner::Scan(const char* pathDirectory)
{
  path pathInputDir(pathDirectory);
  if (!exists(pathInputDir))
    {
    //std::cerr << "Input path \"" << inputDir << "\" does not exists" << std::endl;
    return PATH_NOEXISTS;
    }
  if (!is_directory(pathInputDir)) 
    {
    //std::cerr << "Input path \"" << inputDir << "\" is not a directory" << std::endl;
    return PATH_NOTDIR;
    }

  // check if the directory contains a DICOMDIR
  path pathDICOMDIR(pathInputDir);
  pathDICOMDIR /= "DICOMDIR";
  if (exists(pathDICOMDIR))
    return this->ScanDICOMDIR(pathDICOMDIR.string().c_str());
  else
    return this->ScanDirectory(pathDirectory);
}

const std::string & Scanner::GetMagnitudeCentralSlice(size_t idxTimeStep)
{
  size_t sizeVolume = this->m_SeriesMagnitude.size()/this->GetNumberOfTimeSteps();
  std::cout << "this->GetNumberOfTimeSteps() = " << this->GetNumberOfTimeSteps() << std::endl;
  std::cout << "sizeVolume = " << sizeVolume << std::endl;
  size_t idx = sizeVolume * idxTimeStep + sizeVolume/2;
  
  return this->m_SeriesMagnitude[idx].m_Path;
}

const std::string & Scanner::GetPhaseCentralSlice(size_t idxTimeStep,
                                                  VelocityComponentType vc)
{
  size_t sizeVolume = this->m_SeriesMagnitude.size()/this->GetNumberOfTimeSteps();
  size_t base = 0;
  switch (vc)
    {
    case X:
      base = this->m_SeriesPhase.size()/3;
      break;
    case Y:
      base = 0;
      break;
    case Z:
      base = 2*(this->m_SeriesPhase.size()/3);
      break;
    default:
      return "";
    }
  size_t idx = base + sizeVolume * idxTimeStep + sizeVolume/2;
  
  return this->m_SeriesPhase[idx].m_Path;
}

int Scanner::SelectMagnitudeVolume(size_t idxTimeStep,
                                   std::vector<std::string> &filenames)
{
  size_t sizeVolume = this->m_SeriesMagnitude.size()/this->GetNumberOfTimeSteps();
 
  for ( size_t i = sizeVolume * idxTimeStep;
        i < sizeVolume * (idxTimeStep + 1); i++ ) 
    {
    filenames.insert(filenames.begin(), this->m_SeriesMagnitude[i].m_Path);
    //filenames.push_back( seriesMagnitude[i].m_Path );
    }
  return 0;
}

void Scanner::GetTimeValues(std::vector<double> & timeValues)
{
  timeValues.clear();
  size_t sizeVolume = this->m_SeriesMagnitude.size()/this->GetNumberOfTimeSteps();
  for (size_t t = 0; t < this->GetNumberOfTimeSteps(); t++)
    {
    size_t idx = t*sizeVolume;
    timeValues.push_back(this->m_SeriesMagnitude[idx].m_TimeStep);
    }
}

// orthogonal to axial view, phase y comes first in order
int Scanner::SelectPhaseYVolume(size_t idxTimeStep,
                                std::vector<std::string> &filenames)
{
  size_t sizeVolume = this->m_SeriesPhase.size()/(3*this->GetNumberOfTimeSteps());
  size_t baseY = 0;

  for ( size_t i = baseY + sizeVolume * idxTimeStep;
        i < baseY + sizeVolume * ( idxTimeStep + 1 ); i++ ) 
    {
    filenames.insert(filenames.begin(), this->m_SeriesPhase[i].m_Path);
    //filenames.push_back( seriesPhase[i].m_Path );
    }
  return 0;
}

// orthogonal to coronal view
int Scanner::SelectPhaseXVolume(size_t idxTimeStep,
                                std::vector<std::string> & filenames )
{
  size_t sizeVolume = this->m_SeriesPhase.size()/(3*this->GetNumberOfTimeSteps());
  // skip slices for PhaseY which comes first
  size_t baseX = this->m_SeriesPhase.size()/3;

  for ( size_t i = baseX + sizeVolume * idxTimeStep;
        i < baseX + sizeVolume * (idxTimeStep + 1); i++ ) 
    {
    filenames.insert(filenames.begin(), this->m_SeriesPhase[i].m_Path);
    //filenames.push_back( seriesPhase[i].m_Path );
    }
  return 0;
}

// orthogonal to sagittal view
int Scanner::SelectPhaseZVolume(size_t idxTimeStep,
                                std::vector<std::string> &filenames)
{
  size_t sizeVolume = this->m_SeriesPhase.size()/(3*this->GetNumberOfTimeSteps());
  size_t baseX = this->m_SeriesPhase.size()/3;
  size_t baseZ = baseX*2;

  for ( size_t i = baseZ + sizeVolume * idxTimeStep;
        i < baseZ + sizeVolume * ( idxTimeStep + 1 ); i++ ) 
    {
    filenames.insert(filenames.begin(), this->m_SeriesPhase[i].m_Path);
    //filenames.push_back( seriesPhase[i].m_Path );
    }
  return 0;
}

StatusType Scanner::ScanDICOMDIR(const char* DICOMDIR)
{
  this->Clear();

  this->InvokeEvent(AwakeEvent());
  // Instanciate the reader:
  gdcm::Reader reader;
  reader.SetFileName(DICOMDIR);
  if(!reader.Read())
    {
    //std::cerr << "Could not read: " << DICOMDIR << std::endl;
    return ST_NOREAD;
    }
  std::stringstream strStream;
  path pathDcmRoot(path(DICOMDIR).parent_path());

  //std::cout << "pathDcmRoot: '" << pathDcmRoot << "'\n";
  //std::cout.flush();

  // The output of gdcm::Reader is a gdcm::File
  gdcm::File &file = reader.GetFile();

  // the dataset is the the set of element we are interested in:
  gdcm::DataSet &ds = file.GetDataSet();

  gdcm::FileMetaInformation &fmi = file.GetHeader();

  gdcm::MediaStorage ms;
  ms.SetFromFile(file);
  if(ms != gdcm::MediaStorage::MediaStorageDirectoryStorage)
    {
    //std::cout << "This file is not a DICODIR" << std::endl;
    return INV_DICOMDIR;
    }

  if (fmi.FindDataElement(gdcm::Tag (0x0002, 0x0002)))
    {  
    strStream.str("");
    fmi.GetDataElement(gdcm::Tag (0x0002, 0x0002)).GetValue().Print(strStream);
    std::cout << fmi.GetDataElement(gdcm::Tag (0x0002, 0x0002)) << "\n";
    }
  else
    {
    std::cerr << " Media Storage Sop Class UID not present" << std::endl;
    }

  //TODO il faut trimer strm.str() avant la comparaison au cas ou...
  if ("1.2.840.10008.1.3.10"!=strStream.str())
    {
    //std::cout << "This file is not a DICOMDIR" << std::endl;
    return pcmr::INV_DICOMDIR;
    }

  // (0004,1220) SQ (Sequence with explicit length #=1887)   # 1512498, 1 DirectoryRecordSequence
  gdcm::Tag tsq_DirectoryRecord(0x0004,0x1220);
  if(!ds.FindDataElement(tsq_DirectoryRecord))
    {
    //std::cout << "Fatal error: DirectoryRecordSequence not found\n";
    return NO_DRECSEQ;
    }
  const gdcm::DataElement &sq_DirectoryRecord = ds.GetDataElement(tsq_DirectoryRecord);
  const gdcm::SmartPointer<gdcm::SequenceOfItems> sqi = sq_DirectoryRecord.GetValueAsSQ();
  if (!sqi)
    {
    //std::cout << "Fatal error: DirectoryRecordSequence found but it's empty\n";
    return DRECSEQ_EMPTY;
    }
  
  std::cout << "DirectoryRecordSequence has " << sqi->GetNumberOfItems() << " number of entries\n";

  typedef enum
  {
    RT_DONTCARE,
    RT_SERIES,
    RT_IMAGE
  } DcmDirRecordType;

  DcmDirRecordType previousItemType = RT_DONTCARE;
  ComplexImageComponentType collectImage = pcmr::CIC_UNKNOWN;
  
  for (size_t i = 0; i < sqi->GetNumberOfItems(); i++)
    {
    if (!(i % 10))
      {
      this->InvokeEvent(AwakeEvent());
      }
    const gdcm::Item & item = sqi->GetItem(i + 1);
    strStream.str("");
    if (item.FindDataElement(gdcm::Tag (0x0004, 0x1430)))
      {
      item.GetDataElement(gdcm::Tag (0x0004, 0x1430)).GetValue().Print(strStream);
      }
    if (!STRING_CMP(strStream.str(), "SERIES"))
      {
      ++this->m_NumberOfSeries;
      previousItemType = RT_SERIES;
      collectImage = pcmr::CIC_UNKNOWN;
      }
    else if (!STRING_CMP(strStream.str(), "IMAGE"))
      {
      ++this->m_NumberOfImages;
      strStream.str("");
      if (item.FindDataElement(gdcm::Tag(0x0004, 0x1500)))
        {
        item.GetDataElement(gdcm::Tag(0x0004, 0x1500)).GetValue().Print(strStream); 
        }
      else
        {
        std::cout << "Error: there is an image with a path 0x0004,0x1500 unspecified\n";
        continue;
        }
      
      path pathImage(pathDcmRoot);
      std::string pathRel(strStream.str());
      std::replace(pathRel.begin(), pathRel.end(), '\\', '/');
      pathImage /= pathRel;
      // process first image from this serie
      if (previousItemType == RT_SERIES)
        {
        // check if it is a 4D flow or magnitude series
        PCStudyInfo studyInfo;
        StatusType status;
        //pcmr::ComplexImageComponentType iType = TryPcmrDicom(pathImage.c_str(), studyInfo);
        status = studyInfo.ReadFromDICOM(pathImage.string().c_str());
        if (status == pcmr::OK)
          {
          if (studyInfo.GetImageComponentType() == MAGNITUDE)
            {
            collectImage = MAGNITUDE;
            this->m_InfoFromMagnitude = studyInfo;
            }
          else if (studyInfo.GetImageComponentType() == pcmr::PHASE)
            {
            collectImage = pcmr::PHASE;
            this->m_InfoFromPhase = studyInfo;
            }
          }
        }
      if (collectImage == pcmr::MAGNITUDE)
        {
        // insert in the collection of magnitude images
        // std::cout << pathImage << std::endl;
        Scanner::InsertNewPcmrSlice(pathImage.string().c_str(), this->m_SeriesMagnitude);
        }
      else if (collectImage == pcmr::PHASE)
        {
        // insert in the collection of flow images
        Scanner::InsertNewPcmrSlice(pathImage.string().c_str(), this->m_SeriesPhase);
        }
      previousItemType = RT_IMAGE;
      }
    else
      {
      // interrupt series
      collectImage = pcmr::CIC_UNKNOWN;
      previousItemType = RT_DONTCARE;
      }
    }
  std::cout << "There were found " << this->m_SeriesMagnitude.size() 
            << " slices of magnitude\n";
  std::cout << "There were found " << this->m_SeriesPhase.size() 
            << " slices of phase\n";

  if (!this->m_SeriesMagnitude.size() || !this->m_SeriesPhase.size())
    {
    /*
    std::cerr << "No magnitude or phase found during directory scan at: \"" 
              << dicomDir << "\"" << std::endl;*/
    return DCM_NO_PM;
    }
  std::cout << "Sorting series ...\n";
  std::sort(this->m_SeriesMagnitude.begin(), this->m_SeriesMagnitude.end());
  std::sort(this->m_SeriesPhase.begin(), this->m_SeriesPhase.end());
  std::cout << "Series sorted!\n";

  return OK;
}

StatusType Scanner::TryDICOM(const char *dcm)
{
  pcmr::StatusType status;
  gdcm::Reader reader;

  reader.SetFileName( dcm );
  if( !reader.Read() )
    {
    std::cout << "Could not read: \"" << dcm << "\"" << std::endl;
    return OK;
    }
  // The output of gdcm::Reader is a gdcm::File
  gdcm::File &file = reader.GetFile();

  if (IsDICOMDIR(file))
    {
    std::cout << "Found DICOMDIR: \"" << dcm << "\"" << std::endl; 
    return OK;
    }

  // the dataset is the the set of element we are interested in:
  gdcm::DataSet &ds = file.GetDataSet();
  
  status = CheckManufacturer( ds, "SIEMENS" );
  if (status != OK)
    {
    /*
    std::cerr << "Not a SIEMENS DICOM: "
              << pcmr::GetStatusDescription(status) << ", while reading \"" << dcm << "\"" << std::endl;*/
    return status;
    }
  
  if (!HasPixelData(ds))
    {
    return OK;
    }
  ++this->m_NumberOfImages;
  // check if it is a 4D flow or magnitude series
  ComplexImageComponentType imageType;
  PCSliceInfo sliceInfo;
  status = sliceInfo.ReadFromDataSet(ds, imageType);
  if (status == OK)
    {
    sliceInfo.m_Path = dcm;
    if (imageType == pcmr::MAGNITUDE)
      {
      this->m_SeriesMagnitude.push_back(sliceInfo);
      }
    else if (imageType == pcmr::PHASE)
      {
      this->m_SeriesPhase.push_back(sliceInfo);
      }
    }
  return OK;
}

StatusType Scanner::ScanDirectory0(const char* pathDirectory,
                                   size_t &progress)
{
  path pathDir(pathDirectory);
  std::cout << "Scanning directory \"" << pathDir << "\"" << std::endl;
  for ( directory_iterator it = directory_iterator(pathDir);
        it != directory_iterator(); it++ )
    {
    ++progress;
    StatusType status;
    if (is_directory(it->status()))
      {
      status = this->ScanDirectory0( it->path().string().c_str(),progress);
      }
    else
      {
      if (!(progress%30))
        {
        this->InvokeEvent(AwakeEvent());
        }
      status = this->TryDICOM(it->path().string().c_str());
      }
    if (status != OK)
      {
      std::cout << "aborted directory scanning at file " 
                << it->path() 
                << ": " << GetStatusDescription(status) << std::endl;
      return status;
      }
    }
  
  return OK;
}

StatusType Scanner::ScanDirectory(const char* pathDirectory)
{
  std::cout << "Scanner::ScanDirectory\n";
  size_t progress = 0;
  this->Clear();
  StatusType status = this->ScanDirectory0(pathDirectory, progress);
  if (status != OK)  
    {
    return status;
    }
  std::cout << "seriesMagnitude.size() = " << this->m_SeriesMagnitude.size() << "\n";
  std::cout << "seriesPhase.size() = " << this->m_SeriesPhase.size() << "\n";
  if ( !this->m_SeriesMagnitude.size() || !this->m_SeriesPhase.size() )
    {
    /*
    std::cerr << "No magnitude or phase found during directory scan at: \"" 
              << dir << "\"" << std::endl;*/
    return DCM_NO_PM;
    }
  this->m_InfoFromMagnitude.ReadFromDICOM(this->m_SeriesMagnitude[0].m_Path.c_str());
  std::cout << "pcmrStudyInfoFromMagnitude:\n";
  this->m_InfoFromMagnitude.Print();
  this->m_InfoFromPhase.ReadFromDICOM(this->m_SeriesPhase[1].m_Path.c_str());
  std::cout << "pcmrStudyInfoFromPhase:\n";
  this->m_InfoFromPhase.Print();

  std::cout << "Sorting series ...\n";
  std::sort( this->m_SeriesMagnitude.begin(), this->m_SeriesMagnitude.end() );
  std::sort( this->m_SeriesPhase.begin(), this->m_SeriesPhase.end() );
  std::cout << "Series sorted!\n";
  return OK;
}

StatusType Scanner::InsertNewPcmrSlice(const gdcm::DataSet &ds,
                                       const char *pathDCM,
                                       std::vector<pcmr::Siemens::PCSliceInfo>& container)
{
  PCSliceInfo sliceInfo;
  ComplexImageComponentType pcmrType = pcmr::CIC_UNKNOWN;
  StatusType status;
  
  status = sliceInfo.ReadFromDataSet( ds, pcmrType );
  if ( status == pcmr::NO_ACQ3D )
    {
    return pcmr::OK;
    }
  else if ( status == pcmr::OK )
    {
    sliceInfo.m_Path = pathDCM;
    container.push_back( sliceInfo );
    return pcmr::OK;
    }
  else
    {
    /*std::cerr <<  "Error '" << pcmr::GetStatusDescription(status) 
              << "' while reading '" << pathDCM << "'\n";*/
    return status;
    }
}

StatusType Scanner::InsertNewPcmrSlice(const char *pathDCM,
                                       std::vector<PCSliceInfo>& container)
{
  gdcm::Reader reader;
  reader.SetFileName(pathDCM);
  if(!reader.Read())
    {
    //std::cerr << "Could not read: " << pathDCM << std::endl;
    return ST_NOREAD;
    }
  // The output of gdcm::Reader is a gdcm::File
  gdcm::File &file = reader.GetFile();
  // the dataset is the the set of element we are interested in:
  gdcm::DataSet &ds = file.GetDataSet();

  return InsertNewPcmrSlice(ds, pathDCM, container);
}

END_SIEMENS_DECLS
