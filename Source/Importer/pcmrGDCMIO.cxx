#include "pcmrGDCMIO.h"
#include "pcmrPhilipsGDCMIO.h"
#include "gdcmMediaStorage.h"
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>

using namespace boost::filesystem;

BEGIN_PCMR_DECLS

bool IsDICOMDIR( gdcm::File &file )
{
  gdcm::FileMetaInformation &fmi = file.GetHeader();

  gdcm::MediaStorage ms;
  ms.SetFromFile( file );
  return ms == gdcm::MediaStorage::MediaStorageDirectoryStorage;
}

bool HasPixelData( gdcm::DataSet &ds )
{
  // (7fe0,0010)
  gdcm::Tag tag( 0x7fe0, 0x0010 );
  if( ds.FindDataElement( tag ) ) 
    {
    return true;
    }
  else
    {
    return false;
    }
}

StatusType ReadManufacturer( gdcm::DataSet &ds, std::string & ID )
{
  gdcm::Element<gdcm::VR::LO,gdcm::VM::VM1> el_Manufacturer;
  if ( TryTag<gdcm::VR::LO,gdcm::VM::VM1>( 0x0008, 0x0070, ds,
                                           el_Manufacturer ) == OK)
    {
    const gdcm::VRToType<gdcm::VR::LO>::Type & manufacturer = el_Manufacturer.GetValue();
    ID = manufacturer.c_str( );
    boost::trim_right( ID );
    return OK;
    }
  return TAG_NOTFOUND;
}

StatusType CheckManufacturer( gdcm::DataSet &ds, const char* ID )
{
  // (0008,0070) ?? (LO) [SIEMENS ]                          # 8,1 Manufacturer
  // http://www.dabsoft.ch/dicom/6/6/#(0008,0070)
  gdcm::Element<gdcm::VR::LO,gdcm::VM::VM1> el_Manufacturer;
  if ( TryTag<gdcm::VR::LO,gdcm::VM::VM1>( 0x0008, 0x0070, ds,
                                           el_Manufacturer ) == OK)
    {
    const gdcm::VRToType<gdcm::VR::LO>::Type & manufacturer = el_Manufacturer.GetValue();
    if ( !manufacturer.compare( 0, strlen( ID ), ID ) )
      {
      return OK;
      }
    else
      {
      return VALUE_MISSMATCH;
      }
    }
  return TAG_NOTFOUND;
}

StatusType CheckProtocolName( gdcm::DataSet &ds, const char* ID )
{
  // (0018,1030) ?? (LO) [mm_fs4DMRI SIEMENS]     # 18,1 Protocol Name    
  // http://www.dabsoft.ch/dicom/6/6/#(0018,1030)
  gdcm::Element<gdcm::VR::LO,gdcm::VM::VM1> el_ProtocolName;
  if ( TryTag<gdcm::VR::LO,gdcm::VM::VM1>( 0x0018, 0x1030, ds,
                                           el_ProtocolName ) == OK)
    {
    const gdcm::VRToType<gdcm::VR::LO>::Type & protocol = el_ProtocolName.GetValue();
    if ( !protocol.compare( 0, strlen( ID ), ID ) )
      {
      return OK;
      }
    else
      {
      return VALUE_MISSMATCH;
      }
    }
  return TAG_NOTFOUND;
}

StatusType CheckMRAcquisitionType( const gdcm::DataSet &ds, const char* Value )
{
  //(0018,0023) CS [3D]                         #   2, 1 MRAcquisitionType
  // http://www.dabsoft.ch/dicom/6/6/#(0018,0023)
  gdcm::Element<gdcm::VR::CS,gdcm::VM::VM1> el_MRAcquisitionType;
  if ( pcmr::TryTag<gdcm::VR::CS,gdcm::VM::VM1>( 0x0018, 0x0023, ds,
                                           el_MRAcquisitionType ) == OK)
    {
      const gdcm::VRToType<gdcm::VR::CS>::Type & acquisition = el_MRAcquisitionType.GetValue();
      
      if ( acquisition.compare( Value ) )
        {
          return VALUE_MISSMATCH;
        }
      return OK;
    }
  return TAG_NOTFOUND;
}

pcmr::StatusType DetectImporterTypeAtFile( const char *filename, 
                                           ImporterType &importerType )
{
  gdcm::Reader reader;
  reader.SetFileName( filename );
  if( !reader.Read( ) )
    {
    return pcmr::ST_NOREAD;
    }
  // The output of gdcm::Reader is a gdcm::File
  gdcm::File &file = reader.GetFile();

  // the dataset is the the set of element we are interested in:
  gdcm::DataSet &ds = file.GetDataSet();

  std::string manufacturer;
  pcmr::StatusType status = pcmr::ReadManufacturer( ds, manufacturer );
  boost::to_upper( manufacturer );
  std::cout << manufacturer << std::endl;
  if ( status == pcmr::OK )
    {
    if ( /* PHILIPS */ manufacturer.find( "PHILIPS" ) != std::string::npos )
      {
      if ( pcmr::Philips::EnhancedMRI::CheckEnhancedMRSOPClassUID( ds ) == pcmr::OK )
        {
        importerType = PHILIPS_ENHANCED;
        }
      else
        {
        importerType = PHILIPS_UNENHANCED;
        }
      }
    else if ( /* SIEMENS */ manufacturer.find( "SIEMENS" ) != std::string::npos )
      {
      importerType = SIEMENS;
      }
    else
      {
      importerType = UNKNOWN;
      }
    return pcmr::OK;
    }
  return status;
}

pcmr::StatusType DetectImporterTypeAtDirectory( const char *dir, 
                                                ImporterType &importerType )
{
  path pathDir(dir);
  std::cout << "Scanning directory \"" << dir << "\"" << std::endl;
  for (directory_iterator it = directory_iterator(pathDir);
       it != directory_iterator(); it++)
    {
    pcmr::StatusType status;
    if (is_directory(it->status()))
      {
      status = DetectImporterTypeAtDirectory( it->path().string().c_str(), importerType );
      if ( status == pcmr::OK && importerType != UNKNOWN )
        {
        return pcmr::OK;
        }
      }
    else
      {
      status = DetectImporterTypeAtFile( it->path().string().c_str(),
                                         importerType );
      if ( status == pcmr::OK && importerType != UNKNOWN )
        {
        return pcmr::OK;
        }
      }
    }
  return pcmr::OK;
}

StatusType ReadImageType( const gdcm::DataSet &ds, 
                          std::vector<std::string> &imageType  )
{
  // (0008,0008) CS [DERIVED\PRIMARY\FLOW_ENCODED\MIXED]     #  34, 4 ImageType
  // http://www.dabsoft.ch/dicom/6/6/#(0008,0008)
  gdcm::Element<gdcm::VR::CS,gdcm::VM::VM5> el_ImageType;
  if ( pcmr::TryTag<gdcm::VR::CS,gdcm::VM::VM5>( 0x0008, 0x0008, ds,
                                                 el_ImageType ) == OK)
    {
    int i;
    for ( i = 0; i < el_ImageType.GetLength(); i++ )
      {
      imageType.push_back( el_ImageType.GetValue( i ).c_str() );
      }
    boost::trim_right( imageType[ i - 1 ] );
    return OK;
    }
  return TAG_NOTFOUND;
}

StatusType ReadImageType( const gdcm::DataSet &ds, 
                          std::string &imageType )
{
  StatusType status;

  std::vector<std::string> _imageType;
  status = ReadImageType( ds, _imageType );
  if ( status != OK )
    {
    return status;
    }
  imageType = "";
  for( int i = 0; i < _imageType.size(); i++ )
    {
    if ( i )
      {
      imageType += '\\';
      }
    imageType += _imageType[i];
    }
  return OK;
}

StatusType ReadSeriesInstanceUID( const gdcm::DataSet &ds,
                                  std::string &serieUID )
{
  // (0020,000e) UI [1.3.46.670589.11.17203.5.0.8916.2014121015370642411]         # 52,1 Series Instance UID
  gdcm::Element<gdcm::VR::UI,gdcm::VM::VM1> el_SeriesInstance;
  if ( pcmr::TryTag<gdcm::VR::UI,gdcm::VM::VM1>( 0x0020, 0x000e, ds,
                                                 el_SeriesInstance ) == OK)
    {
    const gdcm::VRToType<gdcm::VR::LO>::Type &tagValue = el_SeriesInstance.GetValue();

    serieUID = tagValue.c_str();
    boost::trim_right( serieUID );
    return OK;
    }
  return TAG_NOTFOUND;
}

END_PCMR_DECLS;
