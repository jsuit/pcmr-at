#ifndef __pcmrPhilipsImporter_h
#define __pcmrPhilipsImporter_h

#include "pcmrBaseImporter.h"
#include "pcmr4DImporter_Export.h"
#include "pcmrPhilipsGDCMIO.h"
#include "itkImageAdaptor.h"
#include "itkPhaseContrastTimeAveragedImageFilter.h"

BEGIN_PHILIPS_DECLS

class NegatePixelAccessor  
{
public:
  /** External typedef. It defines the external aspect
   * that this class will exhibit. */
  typedef float ExternalType;

  /** Internal typedef. It defines the internal real
   * representation of data. */
  typedef float InternalType;

  inline void Set(float & output, const float & input) 
  {
    output = -input;
  }

  inline float Get( const float & input )  const
  {
    return -input;
  }
};

class NegateImageAdaptor : public itk::ImageAdaptor<itk::PhaseContrastImage, NegatePixelAccessor >
{
public:
  /** Standard class typedefs. */
  typedef NegateImageAdaptor           Self;
  typedef itk::ImageAdaptor<itk::PhaseContrastImage, NegatePixelAccessor> Superclass;
  typedef itk::SmartPointer<Self>        Pointer;
  typedef itk::SmartPointer<const Self>  ConstPointer;
  
  /** Method for creation through the object factory. */
  itkNewMacro(Self);  

  /** Run-time type information (and related methods). */
  itkTypeMacro( NegateImageAdaptor, ImageAdaptor );

protected:
  NegateImageAdaptor() {}
  virtual ~NegateImageAdaptor() {}
  
private:
  NegateImageAdaptor(const Self&); //purposely not implemented
  void operator=(const Self&); //purposely not implemented

};

class PCMR4DIMPORTER_EXPORT Importer : public BaseImporter
{
 public:
  /** Standard class typedefs. */
  typedef Importer                       Self;
  typedef BaseImporter                   Superclass;
  typedef itk::SmartPointer<Self>        Pointer;
  typedef itk::SmartPointer<const Self>  ConstPointer;

  /** Method for creation through the object factory. */
  itkNewMacro(Self);  

  /** Run-time type information (and related methods). */
  itkTypeMacro(Importer, BaseImporter);

  Importer();
  virtual ~Importer();
  virtual StatusType ReadInformation();
  virtual size_t GetNumberOfTimeSteps();
  virtual float GetLengthOfTimeStep();
  virtual float GetVelocityEncoding();
  virtual float GetMinimumVelocity();
  virtual float GetMaximumVelocity();
  virtual bool GetInvertComponentX();
  virtual bool GetInvertComponentY();
  virtual bool GetInvertComponentZ();
  virtual std::string GetOneDicom();

 protected:
  void GetTimeValuesInternal(std::vector<double> &timeValues);
  virtual StatusType GetRepresentativeSlice(std::string &fileName, size_t &sliceIndex);
  virtual StatusType GetMagnitudeRepresentativeSlice(std::string &fileName, size_t &index);
  virtual StatusType GetPhaseRepresentativeSlice(VelocityComponentType vc,
                                                 std::string &fileName, size_t &index);
  virtual StatusType PrepareForWriting(const char* studyDirectory);
  virtual itk::PhaseContrast3DImage::ConstPointer GetPremaskImage(size_t ts);
  virtual itk::PhaseContrast3DImage::ConstPointer GetMagnitudeImage(size_t ts);
  virtual itk::PhaseContrast3DImage::ConstPointer GetPhaseImage(size_t ts, size_t component);
  float ComputeAverageTimeStep();
 private:
  EnhancedMRI::Pointer m_EnhancedMRI_X;
  EnhancedMRI::Pointer m_EnhancedMRI_Y;
  EnhancedMRI::Pointer m_EnhancedMRI_Z;
  itk::PhaseContrastTimeAveragedImageFilter::Pointer m_PCMRAFilter;
  itk::PhaseContrastImage::Pointer m_Image4DMagnitude;
  itk::PhaseContrastImage::Pointer m_Image4DPhaseX;
  itk::PhaseContrastImage::Pointer m_Image4DPhaseY;
  itk::PhaseContrastImage::Pointer m_Image4DPhaseZ;
  // REVIEW: used to negate Z component, still don't known why it is
  // needed.
  NegateImageAdaptor::Pointer m_Negator;
  float m_AverageTimeStep;
  std::string m_FileNamePhaseX;
  std::string m_FileNamePhaseY;
  std::string m_FileNamePhaseZ;
};

END_PHILIPS_DECLS

#endif
