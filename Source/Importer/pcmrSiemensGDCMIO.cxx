#include "pcmrSiemensGDCMIO.h"
#include "gdcmPrivateTag.h"

BEGIN_SIEMENS_DECLS

StatusType ReadInstanceNumber(const gdcm::DataSet &ds,
                               unsigned int &InstanceNumber)
{
  //(0020,0013) IS [649]                      #   4, 1 InstanceNumber
  // http://www.dabsoft.ch/dicom/6/6/#(0020,0013)
  gdcm::Element<gdcm::VR::IS,gdcm::VM::VM1> el_InstanceNumber;
  if (TryTag<gdcm::VR::IS,gdcm::VM::VM1>(0x0020, 0x0013, ds,
                                           el_InstanceNumber) == OK)
    {
      InstanceNumber = el_InstanceNumber.GetValue();
      return OK;
    }
  return TAG_NOTFOUND;
}

StatusType ReadCardiacNumberOfImages(const gdcm::DataSet &ds, 
                                      unsigned int &CardiacNumberOfImages)
{
  // (0018,1090) IS [27]                           #   2, 1 CardiacNumberOfImages
  // http://www.dabsoft.ch/dicom/6/6/#(0018,1090)
  gdcm::Element<gdcm::VR::IS,gdcm::VM::VM1> el_CardiacNumberOfImages;
  if (TryTag<gdcm::VR::IS,gdcm::VM::VM1>(0x0018, 0x1090, ds,
                                           el_CardiacNumberOfImages) == OK)
    {
      CardiacNumberOfImages = el_CardiacNumberOfImages.GetValue();
      return OK;
    }
  return TAG_NOTFOUND;
}

StatusType ReadRepetitionTime(const gdcm::DataSet &ds, float &RepetitionTime)
{
  // (0018,0080) DS [40.8]                           #   4, 1 RepetitionTime
  // http://www.dabsoft.ch/dicom/6/6/#(0018,0080)
  gdcm::Element<gdcm::VR::DS,gdcm::VM::VM1> el_RepetitionTime;
  if (TryTag<gdcm::VR::DS,gdcm::VM::VM1>(0x0018, 0x0080, ds,
                                           el_RepetitionTime) == OK)
    {
    RepetitionTime = el_RepetitionTime.GetValue();
    return OK;
    }
  return TAG_NOTFOUND;
}

StatusType ReadTriggerTime(const gdcm::DataSet &ds, float &TriggerTime)
{
  // (0018,1060) DS [17.5]                           #   4, 1 TriggerTime
  // http://www.dabsoft.ch/dicom/6/6/#(0018,1060)
  gdcm::Element<gdcm::VR::DS,gdcm::VM::VM1> el_TriggerTime;
  if (TryTag<gdcm::VR::DS,gdcm::VM::VM1>(0x0018, 0x1060, ds,
                                           el_TriggerTime) == OK)
    {
      TriggerTime = el_TriggerTime.GetValue();
      return OK;
    }
  return TAG_NOTFOUND;
}

int TryValueAsString(const gdcm::DataSet &ds, gdcm::Tag tag,
                      std::string &value)
{
  if (!ds.FindDataElement(tag)) 
    {
    return -1;
    }
  gdcm::DataElement &de = const_cast<gdcm::DataElement &>(ds.GetDataElement(tag));
  const gdcm::ByteValue *bv = de.GetByteValue();
  if (!bv)
    {
    return -2;
    }
  const std::vector<char>& vec(*bv);
  value.resize(bv->GetLength());
  std::copy(vec.begin(), vec.end(), value.begin());
  value.push_back('\0');
  return 0;
}

ComplexImageComponentType GetPhaseContrastComponent(const gdcm::DataSet &ds)
{
  ComplexImageComponentType type;

  // REVIEW: no se pueden mezclar, cuando se acepta una hay que
  // quedarse con esa.
  std::string strMinimun(  "ORIGINAL\\PRIMARY\\");
  std::string strPhase(    "ORIGINAL\\PRIMARY\\P\\ND");
  std::string strPhase1(    "ORIGINAL\\PRIMARY\\P\\DIS2D");
  std::string strMagnitude("ORIGINAL\\PRIMARY\\M\\ND");
  std::string strMagnitude1("ORIGINAL\\PRIMARY\\M\\DIS2D");

  std::string imageType;
  if (!TryValueAsString(ds, gdcm::Tag(0x0008,0x0008), imageType))
    {
    if (!strncmp(strMagnitude.c_str(), imageType.c_str(),
                   strMagnitude.length()))
      {
      return MAGNITUDE;
      }
    if (!strncmp(strMagnitude1.c_str(), imageType.c_str(),
                   strMagnitude1.length()))
      {
      return MAGNITUDE;
      }
    else if (!strncmp(strPhase.c_str(), imageType.c_str(),
                        strPhase.length()))
      {
      return PHASE;
      }
    else if (!strncmp(strPhase1.c_str(), imageType.c_str(),
                        strPhase1.length()))
      {
      return PHASE;
      }
    }
  return CIC_UNKNOWN;
}

ComplexImageComponentType GetPhaseContrastComponent0(const gdcm::DataSet &ds)
{
  ComplexImageComponentType type;

  const char* strPhase =      "ORIGINAL\\PRIMARY\\P\\ND";
  const char* strMagnitude =  "ORIGINAL\\PRIMARY\\M\\ND";
  // (0008,0008) CS [ORIGINAL\PRIMARY\P\ND]                  #  22, 4 ImageType
  // http://www.dabsoft.ch/dicom/6/6/#(0008,0008)

  // da error si el valor real tiene menos de 4 elementos
  gdcm::Element<gdcm::VR::CS,gdcm::VM::VM4> el_ImageType;
  if (TryTag<gdcm::VR::CS,gdcm::VM::VM4>(0x0008, 0x0008, ds,
                                           el_ImageType) == OK)
    {
      const char *itype[] = { "ORIGINAL", "PRIMARY", "P", "ND" };

      for (int i = 0; i < 3 /*el_ImageType.GetLength()*/; i++)
        {
          const gdcm::VRToType<gdcm::VR::CS>::Type & _itype = el_ImageType.GetValue(i);
//#define PCMR_DUMP_IMAGE_TYPE
#if defined(PCMR_DUMP_IMAGE_TYPE)
          std::cout << (i?"\\'":"'")
                    <<  _itype 
                    << (i==(el_ImageType.GetLength()-1)?"'\n":"'");
#endif
          if (i == 2)
            {
            if (!_itype.compare(0, 1, "P"))
              {
              type = PHASE;
              }
            else if (!_itype.compare(0, 1, "M"))
              {
              type = MAGNITUDE;
              }
            else
              {
              return CIC_UNKNOWN;
              }
            }
          else 
            {
            if (_itype.compare(0, strlen(itype[ i ]), itype[ i ]))
              {
              return CIC_UNKNOWN;
              }
            }
        }
      return type;
    }
  return CIC_UNKNOWN;
}

template< int TVR, int TVM >
StatusType CSATryElement(const char *name,
                          gdcm::CSAHeader &csa,
                          typename gdcm::Element<TVR,TVM>::Type &value)
{
  if (!csa.FindCSAElementByName(name)) 
    {
      return TAG_NOTFOUND;
    }
  const gdcm::CSAElement & csaElement = csa.GetCSAElementByName(name);
  //std::cout << csaElement << std::endl;
  if (csaElement.IsEmpty())
    {
      return CSA_ITEM_EMPTY;
    }
  gdcm::Element<TVR, TVM> el;
  el.Set(csaElement.GetValue());
  value = el.GetValue();
  return OK;
}

StatusType GetCSAVelocityEncoding(gdcm::CSAHeader &csa, float &venc)
{
  StatusType status;
  double _venc;
  
  status = CSATryElement<gdcm::VR::DS, gdcm::VM::VM1>("FlowVenc", csa, _venc);
  if (status == OK)
    {
    venc = _venc;
    }
  return status;
}

StatusType GetCSAEncodingDirection(gdcm::CSAHeader &csa, int &direction)
{
  return CSATryElement<gdcm::VR::IS, gdcm::VM::VM1>("FlowEncodingDirection",
                                                     csa, direction);
}

StatusType GetCSADirectionPositive(gdcm::CSAHeader &csa, int &positive)
{
  return CSATryElement<gdcm::VR::IS, gdcm::VM::VM1>("PhaseEncodingDirectionPositive",
                                                     csa, positive);
}

END_SIEMENS_DECLS
