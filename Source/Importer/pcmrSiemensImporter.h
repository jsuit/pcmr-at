#ifndef __pcmrSiemensImporter_h
#define __pcmrSiemensImporter_h

#include "pcmr4DImporter_Export.h"
#include "pcmrSiemensScanner.h"
#include "pcmrBaseImporter.h"

#include "itkTileImageFilter.h"

#include "itkPhaseContrastTimeAveragedImageFilter.h"

#include <vector>
#include <string>

typedef itk::TileImageFilter<itk::PhaseContrast3DImage,
                             itk::PhaseContrastImage> TileFilterType;

BEGIN_SIEMENS_DECLS

class PCMR4DIMPORTER_EXPORT Importer : public BaseImporter
{
 public:
  /** Standard class typedefs. */
  typedef Importer                       Self;
  typedef BaseImporter                   Superclass;
  typedef itk::SmartPointer<Self>        Pointer;
  typedef itk::SmartPointer<const Self>  ConstPointer;

  /** Method for creation through the object factory. */
  itkNewMacro(Self);  

  /** Run-time type information (and related methods). */
  itkTypeMacro(Importer, BaseImporter);

  Importer();
  virtual ~Importer();
  virtual StatusType ReadInformation();
  virtual size_t GetNumberOfTimeSteps();
  virtual float GetLengthOfTimeStep();
  virtual float GetVelocityEncoding();
  virtual float GetMinimumVelocity();
  virtual float GetMaximumVelocity();
  virtual bool GetInvertComponentX();
  virtual bool GetInvertComponentY();
  virtual bool GetInvertComponentZ();
  virtual std::string GetOneDicom();

  /*
  virtual void TestEvents()
  {
    Importer::TestEvents();
    std::cout << "Invocando this->m_Scanner->InvokeEvent(AwakeEvent())\n";
    this->m_Scanner->InvokeEvent(AwakeEvent());
  }
  */

 protected:
  virtual void GetTimeValuesInternal(std::vector<double> &timeValues);
  virtual StatusType GetRepresentativeSlice(std::string &fileName, size_t &sliceIndex);
  virtual StatusType GetMagnitudeRepresentativeSlice(std::string &fileName, size_t &index);
  virtual StatusType GetPhaseRepresentativeSlice(VelocityComponentType vc,
                                                 std::string &fileName, size_t &index);
  virtual StatusType PrepareForWriting(const char* studyDirectory);
  virtual itk::PhaseContrast3DImage::ConstPointer GetPremaskImage(size_t ts);
  virtual itk::PhaseContrast3DImage::ConstPointer GetMagnitudeImage(size_t ts);
  virtual itk::PhaseContrast3DImage::ConstPointer GetPhaseImage(size_t ts, size_t component);

  itk::PhaseContrast3DImage::Pointer ReadSeries3D(std::vector<std::string> filenames);
 private:
  Scanner::Pointer m_Scanner;
  TileFilterType::Pointer m_Tilers[4];
  itk::PhaseContrastTimeAveragedImageFilter::Pointer m_PCMRAFilter;
};

END_SIEMENS_DECLS

#endif
