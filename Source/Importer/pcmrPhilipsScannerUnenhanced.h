#ifndef __pcmrPhilipsScanner_h
#define __pcmrPhilipsScanner_h

#include "pcmrPhilipsGDCMIO.h"
#include "itkObject.h"
#include "itkObjectFactory.h"

BEGIN_PHILIPS_DECLS

class PCMR4DIMPORTER_EXPORT Scanner : public itk::Object
{
 public:
  /** Standard class typedefs. */
  typedef Scanner                        Self;
  typedef itk::Object                    Superclass;
  typedef itk::SmartPointer<Self>        Pointer;
  typedef itk::SmartPointer<const Self>  ConstPointer;

  /** Method for creation through the object factory. */
  itkNewMacro(Self);

  /** Run-time type information (and related methods). */
  itkTypeMacro(Scanner, itk::Object);

  Scanner()
    : m_NumberOfTimeSteps( 0 ), m_VelocityEncoding( 0.0 ),
    m_VelocityEncodingHint( 0.0 )
    {}

  StatusType Scan( const std::string &dir );

  const std::string& GetSpatialOrientation()
  {
    return this->m_SpatialOrientation;
  }

  void SetVelocityEncodingHint( float venc )
  {
    this->m_VelocityEncodingHint = venc;
  }

  float GetVelocityEncodingHint( )
  {
    return this->m_VelocityEncodingHint;
  }

  size_t GetNumberOfFilesPerComponent( );

  size_t GetNumberOfSlicesPerVolume( );

  size_t GetNumberOfPhases( );

  void GetTimeStepValues( std::vector<double> &timeValues );

  double GetLengthOfTimeStep( );

  float GetVelocityEncoding( );

  void GetMagnitudeFiles( size_t timeStep, std::vector<std::string> &files );

  std::string GetMagnitudeCentralSlice( size_t timeStep );

  pcmr::StatusType GetPhaseFiles(pcmr::PhaseDirectionType direction,
                                 size_t timeStep, std::vector<std::string> &files);

  std::string GetPhaseCentralSlice( size_t timeStep, pcmr::PhaseDirectionType vc);

  std::string GetOneDicomFile( );

private:
  struct SliceInfoType
  {
    std::string path;
    float time;
    unsigned int sliceNumber;
    float venc;
    SliceInfoType()
    : path( "" ), time( 0.0 ), sliceNumber( 0 ), venc( 0 ) { }

    bool operator < ( const SliceInfoType &a ) const
    {
      return ( this->time == a.time ?
               ( this->sliceNumber < a.sliceNumber ) :
               ( this->time < a.time ) );
    }

    SliceInfoType &operator = ( const SliceInfoType &a )
    {
      this->path = a.path;
      this->time = a.time;
      this->sliceNumber = a.sliceNumber;
      return *this;
    }
  };

  typedef std::vector< SliceInfoType > SliceContainerType;

  void Clear( );

  void ComputeSpatialOrientation();

  StatusType TryDICOMFile( const std::string &filename,
                           SliceInfoType &sliceInfo,
                           pcmr::ComplexImageComponentType &complexComponent,
                           pcmr::VelocityComponentType &direction );
  StatusType ScanDirectory( const std::string &dir, size_t &progress );

  size_t m_NumberOfTimeSteps;
  float m_VelocityEncoding;
  SliceContainerType m_SerieMagnitude;
  SliceContainerType m_SeriePhaseRL;
  SliceContainerType m_SeriePhaseAP;
  SliceContainerType m_SeriePhaseFH;
  std::string m_SpatialOrientation;
  float m_VelocityEncodingHint;
};

END_PHILIPS_DECLS

#endif
