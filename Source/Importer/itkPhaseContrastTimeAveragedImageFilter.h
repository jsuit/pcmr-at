#ifndef __itkPhaseContrastTimeAveragedImageFilter_h
#define __itkPhaseContrastTimeAveragedImageFilter_h

#include "pcmrDefs.h"
#include "pcmr4DImporter_Export.h"
#include "itkImageToImageFilter.h"
#include "itkPhaseContrastImage.h"

namespace itk
{
/** \class PhaseContrastTimeAveragedImageFilter
 * \brief Implements the time-averaged PC-MRA.
 *
 * \ingroup ImageFilters
 */

class PhaseContrastTimeAveragedImageFilter : public ImageToImageFilter< PhaseContrastImage, PhaseContrast3DImage >
{
public:
  /** Standard class typedefs. */
  typedef PhaseContrastTimeAveragedImageFilter             Self;
  typedef ImageToImageFilter< PhaseContrastImage, PhaseContrast3DImage > Superclass;
  typedef SmartPointer< Self >        Pointer;
  /** Method for creation through the object factory. */
  itkNewMacro(Self);
 
  /** Run-time type information (and related methods). */
  itkTypeMacro(PhaseContrastTimeAveragedImageFilter, ImageToImageFilter);
 
  /** The 4D magnitude image.*/
  void SetMagnitudeImage(const PhaseContrastImage* image);
 
  /** The 4D X component of the phase image.*/
  void SetPhaseXImage(const PhaseContrastImage* image);

  /** The 4D Y component of the phase image.*/
  void SetPhaseYImage(const PhaseContrastImage* image);

  /** The 4D Z component of the phase image.*/
  void SetPhaseZImage(const PhaseContrastImage* image);

  itkGetConstMacro(NoiseMaskThreshold, float);
  void SetNoiseMaskThreshold( float threshold );

  itkGetConstMacro(MaximumMagnitude, float);
  itkGetConstMacro(MaximumMagnitudeIndex, Superclass::InputImageType::IndexType)
  itkGetConstMacro(MinimumMagnitude, float);
  itkGetConstMacro(MinimumMagnitudeIndex, Superclass::InputImageType::IndexType)
  itkGetConstMacro(MaximumVelocity, float);
  itkGetConstMacro(MaximumVelocityIndex, Superclass::InputImageType::IndexType)
  itkGetConstMacro(MinimumVelocity, float);
  itkGetConstMacro(MinimumVelocityIndex, Superclass::InputImageType::IndexType)

  void Print( std::ostream & _out );
  
protected:
  PhaseContrastTimeAveragedImageFilter();
  ~PhaseContrastTimeAveragedImageFilter() {}

  /** This filter produce an image which has different dimension than
   * its inputs. As such, PhaseContrastTimeAveragedImageFilter needs
   * to provide an implementation for GenerateOutputInformation() in
   * order to inform the pipeline execution model.
   *
   * \sa ProcessObject::GenerateOutputInformaton()  */
  virtual void GenerateOutputInformation();

  virtual void BeforeThreadedGenerateData();
  virtual void AfterThreadedGenerateData();
 
  /** Does the real work. */
  virtual void ThreadedGenerateData(const Superclass::OutputImageRegionType& outputRegionForThread, ThreadIdType threadId);
  
private:
  PhaseContrastTimeAveragedImageFilter(const Self &); //purposely not implemented
  void operator=(const Self &);  //purposely not implemented

  float m_NoiseMaskThreshold;
  float m_NoiseMaskThresholdValue;
  float m_MaximumMagnitude;
  float m_MinimumMagnitude;
  float m_MaximumVelocity;
  float m_MinimumVelocity;
  Superclass::InputImageType::IndexType m_MinimumMagnitudeIndex;
  Superclass::InputImageType::IndexType m_MaximumMagnitudeIndex;
  Superclass::InputImageType::IndexType m_MinimumVelocityIndex;
  Superclass::InputImageType::IndexType m_MaximumVelocityIndex;
  std::vector<float> m_MinimumAtThread;
  std::vector<Superclass::InputImageType::IndexType> m_MinimumIndexAtThread;
  std::vector<float> m_MaximumAtThread;
  std::vector<Superclass::InputImageType::IndexType> m_MaximumIndexAtThread;
};

}

#endif

