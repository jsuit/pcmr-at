#include "pcmrSiemensGDCMIO.h"

#include "pcmrSiemensImporter.h"
#include "pcmrImporterHelper.h"

#include "itkGDCMImageIO.h"
#include "itkImageSeriesReader.h"
#include "itkRescaleIntensityImageFilter.h"
#include "itkUnaryFunctorImageFilter.h"
#include "itkOrientImageFilter.h"
#include "itkRegionOfInterestImageFilter.h"
#include "itkCommand.h"

BEGIN_SIEMENS_DECLS

template< class TInput, class TOutput >
class NegateValue
{
public:
  NegateValue() {}
  ~NegateValue() {}

  bool operator!=(const NegateValue &) const
  {
    return false;
  }

  bool operator==(const NegateValue & other) const
  {
    return !( *this != other );
  }

  inline TOutput operator()(const TInput & A) const
  { return -A; }
};

// REVIEW: this parameters are ad-hoc, should be better understood.
struct PreprocessParameters
{
  bool m_negateX;
  bool m_negateY;
  bool m_negateZ;

  void Print( )
  {
    std::cout << "Preprocess phase directions:\n";
    std::cout << (m_negateX?"do":"don't") << " negate X\n";
    std::cout << (m_negateY?"do":"don't") << " negate Y\n";
    std::cout << (m_negateZ?"do":"don't") << " negate Z\n";
  }

  pcmr::StatusType ReadFromDICOM( const char* dcm )
  {
    pcmr::StatusType status;
    gdcm::Reader reader;

    this->m_negateX = false;
    this->m_negateY = false;
    this->m_negateZ = true;

    reader.SetFileName( dcm );
    if( !reader.Read() )
      {
      std::cerr << "Could not read: " << dcm << std::endl;
      return pcmr::ST_NOREAD;
      }
    gdcm::File &file = reader.GetFile();
    gdcm::DataSet &ds = file.GetDataSet();
  
    // This is the name of the protocol in Creu Blanca
    const char *protocolCreuBlanca = "mm_fs4DMRI";
    status = pcmr::CheckProtocolName(ds, protocolCreuBlanca);
    if ( status == pcmr::OK )
      {
      this->m_negateX = true;
      this->m_negateY = true;
      }
    else
      {
      std::cerr <<  "Error '" << pcmr::GetStatusDescription( status ) 
                << "' while reading '" << dcm << "'\n";
      }
    return status;
  }
};

typedef itk::ImageSeriesReader<itk::PhaseContrast3DImage> ReaderType;

Importer::Importer()
  : BaseImporter()
{
  TileFilterType::LayoutArrayType layout;
  layout[0] = 1;
  layout[1] = 1;
  layout[2] = 1;
  layout[3] = 0;
  
  for ( size_t i = 0; i < 4; i++ )
    {
    this->m_Tilers[i] = TileFilterType::New();
    this->m_Tilers[i]->SetLayout( layout );
    }
  this->m_PCMRAFilter = itk::PhaseContrastTimeAveragedImageFilter::New();
  this->m_Scanner = Scanner::New();
  this->AddAwakeObserver(this->m_PCMRAFilter);
  this->AddAwakeObserver(this->m_Scanner);
  
  //std::cout << "Siemens::Importer Testing AwakeEvent()\n";
  //this->InvokeEvent(AwakeEvent());
  //this->TestAwakeEvent();
  //std::cout << "Salio algo?\n";
}

Importer::~Importer()
{
}

StatusType Importer::ReadInformation()
{
  this->InvokeEvent(StartScanEvent());
  this->GetStatusRef() = this->m_Scanner->Scan(this->GetSequenceDirectory().c_str());
  this->FillExtendedDictionary();
  this->InvokeEvent(EndScanEvent());
  return this->GetStatus();
}

size_t Importer::GetNumberOfTimeSteps()
{
  return this->m_Scanner->GetNumberOfTimeSteps();
}

float Importer::GetLengthOfTimeStep()
{
  return this->m_Scanner->GetLengthOfTimeStep();
}

float Importer::GetVelocityEncoding()
{
  float venc = this->m_Scanner->GetVelocityEncoding();
  return venc <= 0 ? this->GetDefaultVelocityEncoding() : venc;

}

float Importer::GetMinimumVelocity()
{
  return 0.0;
}

float Importer::GetMaximumVelocity()
{
  return this->GetVelocityEncoding();
}

bool Importer::GetInvertComponentX()
{
  return false;
}

bool Importer::GetInvertComponentY()
{
  return true;
}

bool Importer::GetInvertComponentZ()
{
  return false;
}

std::string Importer::GetOneDicom()
{
  return this->m_Scanner->GetSeriesPhase()[0].m_Path;
}

void Importer::GetTimeValuesInternal(std::vector<double> &timeValues)
{
  this->m_Scanner->GetTimeValues(timeValues);
}

StatusType Importer::GetRepresentativeSlice(std::string &fileName, size_t &sliceIndex)
{
  return this->GetMagnitudeRepresentativeSlice(fileName, sliceIndex);
}

StatusType Importer::GetMagnitudeRepresentativeSlice(std::string &fileName, size_t &sliceIndex)
{
  size_t representativeTimeStep = 3;

  fileName = this->m_Scanner->GetMagnitudeCentralSlice(representativeTimeStep);
  sliceIndex = 0;
  return OK;
}

StatusType Importer::GetPhaseRepresentativeSlice(VelocityComponentType vc,
                                                 std::string &fileName, size_t &sliceIndex)
{
  size_t representativeTimeStep = 3;

  fileName = this->m_Scanner->GetPhaseCentralSlice(representativeTimeStep, vc);
  sliceIndex = 0;
  return OK;
}

typedef itk::OrientImageFilter<itk::PhaseContrast3DImage,itk::PhaseContrast3DImage>
OrienterType;

#define __DUMP_ORIENT_IMAGE_FILTER__
#ifdef __DUMP_ORIENT_IMAGE_FILTER__
static bool DumpFlipAndPermute = false;

static 
void DoDumpOrientImageFilter(OrienterType::Pointer orienter)
{
  orienter->Update();
  const OrienterType::PermuteOrderArrayType permuteOrder = orienter->GetPermuteOrder();
  OrienterType::FlipAxesArrayType flipAxes = orienter->GetFlipAxes();
  
  orienter->Print(std::cout);
  const OrienterType::InputImageType *input = orienter->GetInput();
  const std::string prompt("DoDumpOrientImageFilter");
  pcmr::DumpDirections( prompt, input );
}

#define DUMP_ORIENT_IMAGE_FILTER_ON(t)          \
  if (ts==(t))                                  \
    {                                           \
    DumpFlipAndPermute = true;                  \
    }

#define DUMP_ORIENT_IMAGE_FILTER_OFF             \
  DumpFlipAndPermute = false;

#define DUMP_ORIENT_IMAGE_FILTER(o)             \
  do                                            \
    {                                           \
      if (DumpFlipAndPermute)                   \
        {                                       \
        DoDumpOrientImageFilter(o);             \
        }                                       \
    }                                           \
  while(0);
#else
#define DUMP_ORIENT_IMAGE_FILTER_ON(t)
#define DUMP_ORIENT_IMAGE_FILTER_OFF
#define DUMP_ORIENT_IMAGE_FILTER(o) 
#endif

StatusType Importer::PrepareForWriting(const char* studyDirectory)
{
  typedef itk::GDCMImageIO ImageIOType;
  typedef itk::RescaleIntensityImageFilter <itk::PhaseContrast3DImage,itk::PhaseContrast3DImage> RescaleFilterType;
  typedef itk::UnaryFunctorImageFilter
    < itk::PhaseContrast3DImage,
      itk::PhaseContrast3DImage,
      NegateValue<itk::PhaseContrast3DImage::PixelType,itk::PhaseContrast3DImage::PixelType> > NegateImage3DType;
  
  float velocityEncoding = this->GetVelocityEncoding();
  // REVIEW: why I'm reading VENC = 0.0?
  if (velocityEncoding <= 0)
    {
    velocityEncoding = 150.0;
    }
  size_t sizeVolume = this->m_Scanner->GetNumberOfSlicesPerVolume();
  ReaderType::FileNamesContainer filenamesMag, 
    filenamesX, filenamesY, filenamesZ;

  filenamesMag.reserve(sizeVolume);
  filenamesX.reserve  (sizeVolume);
  filenamesY.reserve  (sizeVolume);
  filenamesZ.reserve  (sizeVolume);

  PreprocessParameters preprocessP;
  preprocessP.ReadFromDICOM((this->m_Scanner->GetSeriesPhase()[0]).m_Path.c_str());
  size_t numberOfTimeSteps = this->GetNumberOfTimeSteps();
  for ( size_t ts = 0; ts < numberOfTimeSteps; ts++ )
    {
    std::cout << " Extracting timestep number " << ts << std::endl;
    // read magnitude for this timestep
    this->InvokeEvent(AwakeEvent());
    filenamesMag.clear();
    this->m_Scanner->SelectMagnitudeVolume(ts, filenamesMag );
    DUMP_ORIENT_IMAGE_FILTER_ON(2);
    itk::PhaseContrast3DImage::Pointer imgMag = this->ReadSeries3D(filenamesMag);
    imgMag->Update();
    this->m_Tilers[0]->SetInput(ts, imgMag);

    // read v_x component for this timestep
    this->InvokeEvent(AwakeEvent());
    filenamesX.clear();
    this->m_Scanner->SelectPhaseXVolume(ts, filenamesX);
    itk::PhaseContrast3DImage::Pointer imgPhaseX = this->ReadSeries3D(filenamesX);
    RescaleFilterType::Pointer rescalerX = RescaleFilterType::New();
    rescalerX->SetOutputMinimum(-velocityEncoding);
    rescalerX->SetOutputMaximum(+velocityEncoding);
    rescalerX->SetInput(imgPhaseX);
    if (preprocessP.m_negateX)
      {
      NegateImage3DType::Pointer negateX = NegateImage3DType::New();
      negateX->SetInput(rescalerX->GetOutput());
      negateX->Update();
      this->m_Tilers[1]->SetInput(ts, negateX->GetOutput());
      }
    else 
      {
      rescalerX->Update();
      this->m_Tilers[1]->SetInput(ts, rescalerX->GetOutput());
      }

    // read v_y component for this timestep
    this->InvokeEvent(AwakeEvent());
    filenamesY.clear();
    this->m_Scanner->SelectPhaseYVolume(ts, filenamesY);
    itk::PhaseContrast3DImage::Pointer imgPhaseY = this->ReadSeries3D(filenamesY);
    RescaleFilterType::Pointer rescalerY = RescaleFilterType::New();
    rescalerY->SetOutputMinimum(-velocityEncoding);
    rescalerY->SetOutputMaximum(+velocityEncoding);
    rescalerY->SetInput(imgPhaseY);
    if (preprocessP.m_negateY)
      {
      NegateImage3DType::Pointer negateY = NegateImage3DType::New();
      negateY->SetInput(rescalerY->GetOutput());
      negateY->Update();
      this->m_Tilers[2]->SetInput( ts, negateY->GetOutput() );
      }
    else
      {
      rescalerY->Update();
      this->m_Tilers[2]->SetInput(ts, rescalerY->GetOutput());
      }

    // read v_z component for this timestep
    this->InvokeEvent(AwakeEvent());
    filenamesZ.clear();
    this->m_Scanner->SelectPhaseZVolume(ts, filenamesZ);
    itk::PhaseContrast3DImage::Pointer imgPhaseZ = this->ReadSeries3D(filenamesZ);
    RescaleFilterType::Pointer rescalerZ = RescaleFilterType::New();
    rescalerZ->SetOutputMinimum(-velocityEncoding);
    rescalerZ->SetOutputMaximum(+velocityEncoding);
    rescalerZ->SetInput(imgPhaseZ);
    if ( preprocessP.m_negateZ )
      {
      NegateImage3DType::Pointer negateZ = NegateImage3DType::New();
      negateZ->SetInput(rescalerZ->GetOutput());
      negateZ->Update();
      this->m_Tilers[3]->SetInput(ts, negateZ->GetOutput());
      }
    else
      {
      rescalerZ->Update();
      this->m_Tilers[3]->SetInput(ts, rescalerZ->GetOutput());
      }
    DUMP_ORIENT_IMAGE_FILTER_OFF;
    }

  this->m_PCMRAFilter->SetNoiseMaskThreshold( 0.01 );
  this->m_PCMRAFilter->SetMagnitudeImage(this->m_Tilers[0]->GetOutput());
  this->m_PCMRAFilter->SetPhaseXImage   (this->m_Tilers[1]->GetOutput());
  this->m_PCMRAFilter->SetPhaseYImage   (this->m_Tilers[2]->GetOutput());
  this->m_PCMRAFilter->SetPhaseZImage   (this->m_Tilers[3]->GetOutput());
  this->m_PCMRAFilter->Update();
  this->m_PCMRAFilter->Print(std::cout);
 
  return OK;
}

itk::PhaseContrast3DImage::ConstPointer Importer::GetPremaskImage(size_t ts)
{
  return this->m_PCMRAFilter->GetOutput();
}

itk::PhaseContrast3DImage::ConstPointer Importer::GetMagnitudeImage(size_t ts)
{
  return this->m_Tilers[0]->GetInput(ts);
}

itk::PhaseContrast3DImage::ConstPointer Importer::GetPhaseImage(size_t ts, size_t component)
{
  return this->m_Tilers[component+1]->GetInput(ts);
}

itk::PhaseContrast3DImage::Pointer Importer::ReadSeries3D(ReaderType::FileNamesContainer filenames)
{
  typedef itk::GDCMImageIO ImageIOType;
  typedef itk::RegionOfInterestImageFilter< itk::PhaseContrast3DImage, itk::PhaseContrast3DImage >
    ROIFilterType;

  ImageIOType::Pointer gdcmIO = ImageIOType::New();
  ReaderType::Pointer seriesReader = ReaderType::New();
  
  seriesReader->SetImageIO(gdcmIO);
  seriesReader->SetFileNames(filenames);
  try
    {
    seriesReader->Update();
    }
  catch (itk::ExceptionObject &excp)
    {
    std::cerr << "Exception thrown while reading the series" << std::endl;
    std::cerr << excp << std::endl;
    return NULL;
    }
  ROIFilterType::Pointer roiFilter = ROIFilterType::New();
  if (this->HasROI())
    {
    itk::PhaseContrast3DImage::IndexType startROI;
    startROI[0] = this->GetMinimumX();
    startROI[1] = this->GetMinimumY();
    startROI[2] = 0;
    itk::PhaseContrast3DImage::SizeType sizeROI;
    itk::PhaseContrast3DImage::RegionType R1 = seriesReader->GetOutput()->GetLargestPossibleRegion();
    sizeROI = R1.GetSize();
    sizeROI[0] = (this->GetMaximumX()-this->GetMinimumX()-1);
    sizeROI[1] = (this->GetMaximumY()-this->GetMinimumY()-1);
    roiFilter->SetInput(seriesReader->GetOutput());
    itk::PhaseContrast3DImage::RegionType roiRegion(startROI, sizeROI);
    roiFilter->SetRegionOfInterest(roiRegion);
    roiFilter->Update();
    }
  OrienterType::Pointer orienter = OrienterType::New();
  orienter->UseImageDirectionOn();
  /*SetDesiredCoordinateOrientationToSagittal is equivalent to
   *  SetDesiredCoordinateOrientation(ITK_COORDINATE_ORIENTATION_ASL).
   */

  // this orientation gets the correct side labels inside VolView
  // orienter->SetDesiredCoordinateOrientationToAxial();

  // this gets the correct orientation inside Aorta4D, restore this to
  // production!!!!
  orienter->SetDesiredCoordinateOrientationToSagittal();

  // this similar a variation of sagittal = ASL
  // orienter->SetDesiredCoordinateOrientation(itk::SpatialOrientation::ITK_COORDINATE_ORIENTATION_PIL);
  if (this->HasROI())
    {
    orienter->SetInput(roiFilter->GetOutput());
    }
  else
    {
    orienter->SetInput(seriesReader->GetOutput());
    }
  orienter->Update();
  DUMP_ORIENT_IMAGE_FILTER(orienter);
  return orienter->GetOutput();
}

END_SIEMENS_DECLS
