#include "pcmrTaubinSmoother.h"
#include "itkMesh.h"
#include "itkMeshFileReader.h"

typedef itk::Mesh< float, 3 > MeshType;
typedef itk::MeshFileReader< MeshType > MeshReaderType;

class itkMeshVisitor
{
public:
  void InitVisitor( MeshType *mesh )
  {
    this->m_Mesh = mesh;
    this->m_IdNode = 0;
    this->m_CellIterator = mesh->GetCells()->Begin();
  }

  MeshType *m_Mesh;
  size_t m_IdNode;
  MeshType::CellsContainer::ConstIterator m_CellIterator;
};

bool itkVisitNextNode(float &x, float &y, float &z, void *data)
{
  itkMeshVisitor *visitor = static_cast<itkMeshVisitor*>(data);
  if ( visitor->m_IdNode >= visitor->m_Mesh->GetNumberOfPoints( ) )
    {
    return false;
    }
  MeshType::PointType pt;
  
  visitor->m_Mesh->GetPoint(visitor->m_IdNode, &pt);
  
  x = pt[0];
  y = pt[1];
  z = pt[2];
  ++visitor->m_IdNode;
  return true;
}

bool itkVisitNextTriangle(size_t &n1, size_t &n2, size_t &n3, void *data)
{
  itkMeshVisitor *visitor = static_cast<itkMeshVisitor*>(data);
  if ( visitor->m_CellIterator == visitor->m_Mesh->GetCells()->End() )
    {
    return false;
    }
  typedef MeshType::CellType CellType;
  typedef itk::TriangleCell< CellType > TriangleType;
  CellType * cell = visitor->m_CellIterator.Value();
  if ( cell->GetType() == CellType::TRIANGLE_CELL )
    {
    TriangleType * triangle = dynamic_cast<TriangleType *>( cell );
    TriangleType::PointIdConstIterator it = triangle->PointIdsBegin();
    n1 = *it++;
    n2 = *it++;
    n3 = *it++;
    }
  ++visitor->m_CellIterator;
  return true;
}

int main(int argc, const char *argv[])
{
  if ( argc != 3 )
    {
    std::cerr << "Usage:\n";
    std::cerr << argv[0] << " input.vtk output.vtk\n";
    return -1;
    }
  MeshReaderType::Pointer polyDataReader = MeshReaderType::New();
  polyDataReader->SetFileName( argv[1] );
  polyDataReader->Update();

  TaubinSmoother smoother;
  itkMeshVisitor itkVisitor;
  //polyDataReader->GetOutput( )->Print( std::cout );
  itkVisitor.InitVisitor( polyDataReader->GetOutput( ) );
  smoother.build_external(&itkVisitNextNode, &itkVisitNextTriangle, &itkVisitor);
 
  smoother.save_to_binary_stereo_lithography_file( "/tmp/check_external_mesh.stl" );
  //smoother.fix_cracks();
  float n = 10;
  float lambda = 0.9;
  float mu = -0.1;
  TaubinSmoother::method met = TaubinSmoother::laplace;

  smoother.taubin_smooth(met, lambda, mu, n);

  smoother.save_to_binary_stereo_lithography_file( argv[2] );
  return 0;
}
