source StudyHelper.tcl

StudyHelper::InitFromArgv

set usePremask 0

if { $usePremask } {
  set Image3D [StudyHelper::ReadImagePremask]
} else {
  set flowImage [StudyHelper::GetItkVectorImage]
  
  set Modulus [VectorMagnitudeFilter]
  $Modulus SetInput $flowImage
  $Modulus Update
  set Image3D [ $Modulus GetOutput ]
}

set writer [ VTKScalarImageWriter ]

# paso 01 - Gs: suavisado, en el paper se hace referencia a un suavisado
# gausiano que produce la imagen Gs, aqui empleamos un suavisado que
# preserva las aristas.

# Notas: deberiamos explorar otros filtros de la misma familia.

set cfTimeStep 0.0625
set cfIterations 10
set curvatureFlow [ CurvatureFlowFilter ]
$curvatureFlow SetInput $Image3D
$curvatureFlow SetNumberOfIterations $cfIterations
$curvatureFlow SetTimeStep $cfTimeStep
$curvatureFlow AddObserver ProgressEvent {
  puts "CurvatureFlow Progress: [ $curvatureFlow GetProgress ]"
}

puts "01 - generando imagen suavisada Gs ..."
$curvatureFlow Update

StudyHelper::WriteImage "Vesselness_Gs.vtk" [ $curvatureFlow GetOutput ] \
    -writer $writer -msg "01.1 - guardando imagen suavisada Gs %O"

# paso 02 - VI: vessel image, se aplica el filtro de mejora de
# estructuras tubulares.

set useItkFilters 1

set sigmaMin 0.1
set sigmaMax 6
set sigmaSteps 15

if { $useItkFilters } {
# paso 02.1 - construyo el filtro hesiano de ayuda
  set alpha 0.5
  set beta  0.5
  set gamma 5
  set brightObject 1
  set scaleMeassure 0
  set sigmaStepScale "logarithmic"
  # (0: points (blobs), 1: lines (vessels), 2: planes (plate-like
  # structures), 3: hyper-planes. ObjectDimension must be smaller
  # than ImageDimension.
  set objectDimension 1
  set objectnessFilter [ HessianToObjectnessMeasureFilter ]
  if { 1 } {
    StudyHelper::CheckSet \
        $objectnessFilter ScaleObjectnessMeasure $scaleMeassure
    StudyHelper::CheckSet \
        $objectnessFilter BrightObject $brightObject
    StudyHelper::CheckSet \
        $objectnessFilter Alpha $alpha
    StudyHelper::CheckSet \
        $objectnessFilter Beta $beta
    StudyHelper::CheckSet \
        $objectnessFilter Gamma $gamma
    StudyHelper::CheckSet \
        $objectnessFilter ObjectDimension $objectDimension
  }

  set vesselness [ MultiScaleHessianBasedMeasureFilter ]
    $vesselness SetHessianToMeasureFilter $objectnessFilter
  if { $sigmaStepScale eq "logarithmic" } {
    $vesselness SetSigmaStepMethodToLogarithmic
  } else {
    $vesselness SetSigmaStepMethodToEquispaced
  }
} else {
  set vesselness [ MultiScaleVesselnessFilter ]
}
 
#$vesselness SetInput [ $curvatureFlow GetOutput ]
$vesselness SetInput [ $curvatureFlow GetOutput ]
StudyHelper::CheckSet $vesselness SigmaMin $sigmaMin
StudyHelper::CheckSet $vesselness SigmaMax $sigmaMax
StudyHelper::CheckSet $vesselness NumberOfSigmaSteps $sigmaSteps

set VE_Progress 0
$vesselness AddObserver StartEvent {
  global VE_Progress

  set VE_Progress 0
  puts "MultiScaleVesselnessFilter StartEvent"
  puts -nonewline "|$VE_Progress"
  flush stdout
}

$vesselness AddObserver EndEvent {
  puts "|"
  puts "MultiScaleVesselnessFilter EndEvent"
}

$vesselness AddObserver ProgressEvent  {
  global VE_Progress

  set p [$vesselness GetProgress]
  if { [ expr {$p - $VE_Progress} ] >= 0.05 } {
    set VE_Progress $p
    puts -nonewline "|$p"
    flush stdout
  }
}

puts "02 - generando imagen VI resultado de aplicar MultiScaleVesselnessFilter ..."
$vesselness Update

StudyHelper::WriteImage "Vesselness_VI.vtk" [ $vesselness GetOutput ] \
    -writer $writer -msg "02.1 - guardando imagen VI %O"

# paso 03 - TVI: thresholded vessel image, se aplica un threshold con
# el valor cita de la ecuacion [6]

set insideValue 1
set cita 0.01
set binaryThreshold [ BinaryThresholdFilter ]
  $binaryThreshold SetInput [ $vesselness GetOutput ]
  $binaryThreshold SetInsideValue $insideValue
  $binaryThreshold SetLowerThreshold $cita

puts "03 - generando mascara ..."
$binaryThreshold Update

StudyHelper::WriteImage "Vesselness_TVI.vtk" [ $binaryThreshold GetOutput ] \
    -writer $writer -msg "03.1 - guardando imagen TVI %O"


# paso 04 - VEI: vessel enhanced image, aplica la mascara TVI sobre la
# imagen original, se corresponde con la ecuacion [6]

set castFilter [ Std2MaskCastFilter ]
  $castFilter SetInput [ $binaryThreshold GetOutput ]

# Notas: en la mascara puede ir retocada con una combinacion de los
# filtros erode y dilate.

set maskFilter [ ScalarMaskFilter ]
  $maskFilter SetInput1 [ $curvatureFlow GetOutput ]
  $maskFilter SetInput2 [ $castFilter GetOutput ]

puts "04 - generando VEI resultado de aplicar la mascara ..."
$maskFilter Update

StudyHelper::WriteImage "Vesselness_VEI.vtk" [ $maskFilter GetOutput ] \
    -writer $writer -msg "04.1 - guardando imagen VEI %O"

# paso 05 - GsVEI: filtro gausiano aplicado a la imagen de vasos
# extraida en el paso 4.

# Notas: deberiamos explorar otros filtros de la misma familia. He
# identificado para estos los filtros DiscreteGaussianImageFilter (No
# implementado en ITKTCL) y RecursiveGaussianFilter.

set sigma 0.5
set order 0
set gaussianFilter [ RecursiveGaussianFilter ]
  $gaussianFilter SetInput [ $maskFilter GetOutput ]
  $gaussianFilter SetSigma $sigma 
  $gaussianFilter SetOrder $order

StudyHelper::WriteImage "Vesselness_GsVEI.vtk" [ $gaussianFilter GetOutput ] \
    -writer $writer -msg "05.1 - guardando imagen GsVEI %O"

exit
