source "StudyHelper.tcl"

StudyHelper::InitFromArgv

set dmap [StudyHelper::GetItkDistanceMapFromMask]
set extractZeroLevel [ZeroCrossingFilter]
$extractZeroLevel SetInput $dmap
$extractZeroLevel Update
StudyHelper::WriteImage "zero_cross.vtk" [$extractZeroLevel GetOutput]
puts "STATUS: OK"
exit