#lappend auto_path /usr/local/lib/vtk-5.5/

package require vtk
package require vtkinteraction
#source "itkTester.tcl"
source "StudyHelper.tcl"

StudyHelper::InitFromArgv

proc GotoTimeStep { ts } {
  StudyHelper::ActivateTimeStep [expr {round($ts)}]
}

set AnimateStop 1
set WriteToVideo 1

proc AnimateTS { } {
  global AnimateStop
  global currentTimeStep
  global WriteToVideo

  while { !$AnimateStop } {
    set currentTimeStep [ StudyHelper::GetNextTimeStep ]
    GotoTimeStep $currentTimeStep
    if { $WriteToVideo } {
      WriteFrameToVideo
    }
    update
  }
}

proc WriteFrameToVideo { } {
  global WindowToImage
  global VideoWriter

  puts "WriteFrameToVideo"

  vtkWindowToImageFilter w2i
  w2i SetInput renWin
  w2i Update
  $VideoWriter SetInputConnection [w2i GetOutputPort]

  #$WindowToImage SetInput renWin
  #$WindowToImage Update
  #renWin Render

  $VideoWriter Update
  $VideoWriter Write
  w2i Delete
  #CaptureImage
}

proc ExportCurrentInterpolatedMesh { path } {
  vtkUnstructuredGridWriter writer
  writer SetFileName $path
  writer SetInput [probeMesh GetOutput]
  writer Update
  writer Delete
}

proc OpenVideo { } {
  global VideoWriter
  global WindowToImage

  puts "OpenVideo"

  if { [ $VideoWriter GetClassName ] eq "vtkAVIWriter" } {
    set ext ".avi"
  } else {
    set ext ".mpeg"
  }
  $VideoWriter SetInputConnection [$WindowToImage GetOutputPort]
  $VideoWriter SetFileName "InterpolatedMesh${ext}"
  $VideoWriter SetRate [StudyHelper::GetNeededFrameRate]
  $VideoWriter Start
  WriteFrameToVideo
}

proc CloseVideo { } {
  puts "CloseVideo"

  global VideoWriter

  $VideoWriter End
}

proc StartAnimation { btn } {
  global WriteToVideo
  global AnimateStop
  if { $AnimateStop } {
    $btn configure -text "Stop"
    set AnimateStop 0
    if { $WriteToVideo } {
      OpenVideo
    }
    AnimateTS
  } else {
    $btn configure -text "Play"
    set AnimateStop 1
    if { $WriteToVideo } {
      CloseVideo
    }
  }
}

proc CaptureImage { } {
  global WindowToImage
  global currentTimeStep

  vtkPNGWriter writer
  #vtkWindowToImageFilter w2i
  #w2i SetInput renWin
  #w2i Update
  $WindowToImage SetInput renWin
  $WindowToImage Update
  
  #writer SetInputConnection [w2i GetOutputPort]
  writer SetInputConnection [$WindowToImage GetOutputPort]
  writer SetFileName frame${currentTimeStep}.png

  renWin Render

  writer Write
  writer Delete
  #w2i Delete
}

StudyHelper::AddObserver ActivateTimeStep {
  puts "Observer ActivateTimeStep [ StudyHelper::GetCurrentTimeStep ]"
  set imgVector [ StudyHelper::GetVectorImage ]
  probeMesh SetSource $imgVector
  probe SetSource [probeMesh GetOutput]
}

StudyHelper::AddObserver ActivateTimeStep {
  renWin Render
} -last yes

#set imgVector [ StudyHelper::GetVectorImage ]
set mesh [ StudyHelper::GetMeshObject ]
puts [ $mesh GetClassName ]

vtkProbeFilter probeMesh
  probeMesh SetInput $mesh

vtkPlaneWidget planeWidget 
    planeWidget SetInput $mesh
    planeWidget NormalToXAxisOn
    planeWidget SetResolution 50
    planeWidget SetRepresentationToOutline
    #planeWidget SetPlaceFactor 0.75
    planeWidget SetHandleSize 0.02
    planeWidget PlaceWidget

vtkPolyData plane
    planeWidget GetPolyData plane

vtkProbeFilter probe
    probe SetInput plane

#set imgVector [ GotoTimeStep ]

# create pipeline for rendering
#
vtkLookupTable lut
  lut SetHueRange 0.667 0.0 
  lut SetRange 0 [StudyHelper::GetVelocityEncoding]
  lut SetVectorModeToMagnitude
  lut Build

vtkPolyDataMapper contourMapper
    contourMapper SetInputConnection [ probe GetOutputPort ]
    #contourMapper SetInput [ StudyHelper::GetOutputAA probe ]
    contourMapper UseLookupTableScalarRangeOn
    contourMapper SetLookupTable lut

vtkActor contourActor
    contourActor SetMapper contourMapper
    contourActor VisibilityOff

#set probeAA [ StudyHelper::GetOutputAA probe ]
vtkHedgeHog hhog
  #hhog SetInput [ StudyHelper::GetOutputAA probe ]
  #StudyHelper::ConnectAA hhog probe
  hhog SetInputConnection [ probe GetOutputPort ]
  hhog SetScaleFactor 0.05

vtkPolyDataMapper hhogMapper
  hhogMapper SetInputConnection [hhog GetOutputPort]
  #StudyHelper::ConnectAA hhogMapper hhog
  hhogMapper UseLookupTableScalarRangeOn
  hhogMapper SetLookupTable lut

vtkActor hhogActor
  hhogActor SetMapper hhogMapper

# we introduce geometry filter in order to visualize only the
# elements (or faces of elements) on the object surface
vtkGeometryFilter geomFilter
geomFilter SetInput $mesh
vtkPolyDataMapper elementsMapper
elementsMapper SetInput [geomFilter GetOutput]

# the elements will be drawn in uniform color independent on the
# points or cells fields
elementsMapper ScalarVisibilityOff
vtkActor elementsActor
elementsActor SetMapper elementsMapper
set elementsProp [elementsActor GetProperty]

# nice gold color
$elementsProp SetColor 0.91 0.87 0.67

# we switch the diffuse lighting of to make the color of the
# object independent on their orientation
$elementsProp SetDiffuse 0
$elementsProp SetAmbient 1
# we do not need any fancy shading
$elementsProp SetInterpolationToFlat
#$elementsProp SetInterpolationToGouraud
$elementsProp SetOpacity 0.5


# We draw edges just as a set of lines - it is simple
# but has the drawback that with certain magnification and
# object orientation some lines might be partially obscured.
# The alternative would be to wrap a thin tube around
# each edge. This allows to draw edges as a thick 3D object
# which would stand out from the flat surfaces of elements.
vtkExtractEdges edgesFilter
edgesFilter SetInput [geomFilter GetOutput]
vtkPolyDataMapper edgesMapper
edgesMapper SetInput [edgesFilter GetOutput]
edgesMapper ScalarVisibilityOff
vtkActor edgesActor
edgesActor SetMapper edgesMapper
set edgesProp [edgesActor GetProperty]
$edgesProp SetColor 0 0 0
$edgesProp SetDiffuse 0
$edgesProp SetAmbient 1
$edgesProp SetLineWidth 2

vtkOutlineFilter outline
  #outline SetInput $imgVector
  outline SetInput $mesh
    
vtkPolyDataMapper outlineMapper
  outlineMapper SetInput [outline GetOutput]
vtkActor outlineActor
  outlineActor SetMapper outlineMapper

# Create the RenderWindow, Renderer and both Actors
#
vtkRenderer ren1
vtkRenderWindow renWin
    renWin AddRenderer ren1

set WindowToImage [ vtkWindowToImageFilter New ]
$WindowToImage SetInput renWin
if { [ llength [ info command vtkAVIWriter ] ] } {
  set VideoWriter [vtkAVIWriter New]
} else {
  set VideoWriter [vtkFFMPEGWriter New]
}
$VideoWriter SetInputConnection [$WindowToImage GetOutputPort]

# Add the actors to the renderer, set the background and size
#
ren1 AddActor outlineActor
ren1 AddActor contourActor
ren1 AddActor hhogActor
ren1 AddActor elementsActor
ren1 AddActor edgesActor

ren1 SetBackground 1 1 1
renWin SetSize 500 500
ren1 SetBackground 0.7 0.7 0.7
#iren Initialize

# render the image
#
#iren AddObserver UserEvent {wm deiconify .vtkInteract}

# prevent the tk window from showing up then start the event loop
wm withdraw .

toplevel .top
wm title .top "Test: Interpolate Study"
wm protocol .top WM_DELETE_WINDOW ::vtk::cb_exit

set display_frame [frame .top.f1]
set ctrl_buttons [frame .top.btns]

grid $display_frame -row 0 -column 0 -sticky snew
grid $ctrl_buttons  -row 1 -column 0 -sticky snew
grid rowconfigure .top 0 -weight 1
grid columnconfigure .top 0 -weight 1


set labTS [ label $ctrl_buttons.lbts -text "Time step" ]
grid $labTS -row 0 -column 0 -sticky se

set currentTimeStep [ StudyHelper::GetCurrentTimeStep ]
set scaleTS [ scale $ctrl_buttons.scts \
                  -from 0 -to [expr {[StudyHelper::GetNumberOfTimeStep]-1}] \
                  -variable currentTimeStep -showvalue 1 \
                  -orient horizontal \
                  -command GotoTimeStep ]
grid $scaleTS -row 0 -column 1 -sticky sew

set play_button [button $ctrl_buttons.btnPlay -text "Play" \
                     -command [list StartAnimation $ctrl_buttons.btnPlay]]
grid $play_button -row 0 -column 2 -sticky s

set quit_button [button $ctrl_buttons.btn1 -text "Quit" -command  ::vtk::cb_exit]
grid $quit_button -row 0 -column 3 -sticky s

grid columnconfigure $ctrl_buttons 1 -weight 1


set render_widget [vtkTkRenderWidget $display_frame.r \
                       -width 600 -height 600  -rw renWin]

grid $render_widget -sticky snew
grid rowconfigure $display_frame 0 -weight 1
grid columnconfigure $display_frame 0 -weight 1
::vtk::bind_tk_render_widget $render_widget

set iren [[$render_widget GetRenderWindow] GetInteractor]
#vtkRenderWindowInteractor iren
#    iren SetRenderWindow renWin

vtkInteractorStyleTrackballCamera style
$iren SetInteractorStyle style

# Associate the line widget with the interactor

planeWidget SetInteractor $iren
planeWidget AddObserver EnableEvent BeginInteraction
planeWidget AddObserver StartInteractionEvent BeginInteraction
planeWidget AddObserver InteractionEvent ProbeData

[ren1 GetActiveCamera] Zoom 1.5
ren1 ResetCamera
renWin Render


# Actually generate contour lines.
proc BeginInteraction {} {
    planeWidget GetPolyData plane
    contourActor VisibilityOn
}

proc ProbeData {} {
    planeWidget GetPolyData plane
    [probe GetOutput] Update
}

