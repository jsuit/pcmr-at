source StudyHelper.tcl

StudyHelper::InitFromArgv

set _cwd_ [file dir [info script]]

set flowImage [StudyHelper::GetItkVectorImage]

set maskImage0 [StudyHelper::GetImageMask]

# REVIEW:
# en lugar de erosionar debemos restar el ZeroCrossing
#
set useZeroCrossing 1
if {$useZeroCrossing} {
  puts "eliminando el ZeroCrossing"
  set dmap [StudyHelper::GetItkDistanceMapFromMask]
  set extractZeroLevel [ZeroCrossingFilter]
  $extractZeroLevel SetInput $dmap
  $extractZeroLevel Update
  set invertFilter [InvertIntensityFilter]
  $invertFilter SetMaximum 1.0
  $invertFilter SetInput [$extractZeroLevel GetOutput]
  set multiplyFilter [MultiplyFilter_Scalar]
  $multiplyFilter SetInput1 $maskImage0
  $multiplyFilter SetInput2 [$invertFilter GetOutput]
  set maskImageSource $multiplyFilter
} else {
  puts "erosionando un voxel"
  set filterErode [BinaryErodeFilter]
  $filterErode SetInput $maskImage0
  $filterErode Update
  set maskImageSource $filterErode
}

set castFilter [ Std2MaskCastFilter ]
$castFilter SetInput [$maskImageSource GetOutput]
$castFilter Update
puts "mascara calculada"

set maskVectorFilter [ VectorMaskFilter ]
$maskVectorFilter SetInput1 $flowImage
$maskVectorFilter SetInput2 [ $castFilter GetOutput ]
$maskVectorFilter Update

set GradientFilter GradientScalarFilter
set GradientFilter GradientRecursiveGaussianFilter

set Modulus [VectorMagnitudeFilter]
#$Modulus SetInput $flowImage
$Modulus SetInput [$maskVectorFilter GetOutput]
$Modulus Update

set uImage [$Modulus GetOutput]

set uGradient [$GradientFilter]
$uGradient SetInput $uImage
$uGradient Update
set ugradImage [ $uGradient GetOutput ]

set dmImage [StudyHelper::GetItkDistanceMapFromMask]

set negateFilter [ShiftScaleFilter]
$negateFilter SetShift 0
$negateFilter SetScale -1
$negateFilter SetInput $dmImage

set dmGradient [$GradientFilter]
$dmGradient SetInput [$negateFilter GetOutput]
$dmGradient Update

set dmgradImage [$dmGradient GetOutput]

#puts "dmgradImage(100,100,10) = [$dmgradImage GetPixelValue {100 100 10}]"
#puts "dmgradImage(100,100,9) = [$dmgradImage GetPixelValue {100 100 9}]"
#puts "dmgradImage(50,75,8) = [$dmgradImage GetPixelValue {50 75 8}]"
# este punto da gradiente nulo cuando se usa GradientScalarFilter
#puts "dmgradImage(47,105,8) = [$dmgradImage GetPixelValue {47 105 8}]"
set dmgUnit [NormalizeFilter_Covariant]
$dmgUnit SetInput $dmgradImage
$dmgUnit Update

set dmgradImage1 [$dmgUnit GetOutput]

#puts "dmgradImage1(100,100,10) = [$dmgradImage1 GetPixelValue {100 100 10}]"
#puts "dmgradImage1(100,100,9) = [$dmgradImage1 GetPixelValue {100 100 9}]"
#puts "dmgradImage1(50,75,8) = [$dmgradImage1 GetPixelValue {50 75 8}]"

puts "voy a multiplicar"

set wsrMultiply [ MultiplyFilter_Covariant ]
$wsrMultiply SetInput1 $ugradImage
$wsrMultiply SetInput2 $dmgradImage1
$wsrMultiply Update

set wsrImage [$wsrMultiply GetOutput]

set Writer [VTKScalarImageWriter]
$Writer SetInput $wsrImage
$Writer SetFileName [file join $_cwd_ "wsr.vtk"]
$Writer Update

puts "finalizado"
exit

TODO:

- Apply CurvatureAnisotropicDiffusionImageFilter/GradientAnisotropicDiffusionImageFilter 
  to the signed distance function. Compare both filters.
