package require vtkinteraction

source StudyHelper.tcl
StudyHelper::InitFromArgv

#set imgMagnitude [StudyHelper::GetMagnitudeImage]
set imgVelocityX [StudyHelper::GetVelocityImage -component "VX"]
set imgVelocityY [StudyHelper::GetVelocityImage -component "VY"]
set imgVelocityZ [StudyHelper::GetVelocityImage -component "VZ"]

#$imgMagnitude Print
#$imgVelocityY Print

#set vtkImgVY [StudyHelper::VtkImport $imgVelocityY -masked no]
foreach c {VX VY VZ} {
  set vtkImgComp($c) [StudyHelper::GetVelocityImage -component $c -vtk yes]
}

#puts [$vtkImgComp(|V|) Print]

vtkImageAppendComponents iac
foreach c {VX VY VZ} {
  iac AddInput $vtkImgComp($c)
}
iac Update

set imgVector  [iac GetOutput]

puts [$imgVector Print]


