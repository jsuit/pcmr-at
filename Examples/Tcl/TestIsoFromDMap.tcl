package require vtk
package require vtkinteraction
source "StudyHelper.tcl"
source "HiresShot.tcl"

set _cwd_ [file dir [info script]]
StudyHelper::InitFromArgv

array set cmdopt {
  -wss ""
}
array set cmdopt $argv

vtkImageMarchingCubes marching
  marching SetInput [ StudyHelper::GetDistanceMapFromMask ]
  marching SetValue 0 -0.01

vtkSmoothPolyDataFilter smoother
  smoother SetInputConnection [marching GetOutputPort]
  smoother SetNumberOfIterations 700

vtkPolyDataMapper isoMapper
vtkActor isoActor
  isoActor SetMapper isoMapper

if { $cmdopt(-wss) ne "" } {
  if {[file pathtype $cmdopt(-wss)] eq "relative"} {
    set $cmdopt(-wss) [file join $_cwd_ $cmdopt(-wss)]
  }
  vtkStructuredPointsReader wssReader
    wssReader SetFileName $cmdopt(-wss)
    wssReader Update  
  vtkProbeFilter probeSurface
    probeSurface SetInputConnection [smoother GetOutputPort]
    probeSurface SetSourceConnection  [wssReader GetOutputPort]
    probeSurface Update
  vtkLookupTable lut
    lut SetHueRange 0.667 0.0
    lut SetRange {*}[[probeSurface GetOutput] GetScalarRange]
    # lut SetRange -40 2
    lut Build
  isoMapper SetInputConnection [probeSurface GetOutputPort]
  isoMapper UseLookupTableScalarRangeOn
  isoMapper SetLookupTable lut
} else {
  isoMapper SetInputConnection [smoother GetOutputPort]
  isoMapper ScalarVisibilityOff
  isoMapper ImmediateModeRenderingOn
  eval [isoActor GetProperty] SetDiffuseColor 0 0.7 0
  eval [isoActor GetProperty] SetOpacity 0.1
}

# Create the RenderWindow, Renderer and both Actors
#
vtkRenderer ren1
vtkRenderWindow renWin
    renWin AddRenderer ren1

set WindowToImage [ vtkWindowToImageFilter New ]
$WindowToImage SetInput renWin
if { [ llength [ info command vtkAVIWriter ] ] } {
  set VideoWriter [vtkAVIWriter New]
} else {
  set VideoWriter [vtkFFMPEGWriter New]
}
$VideoWriter SetInputConnection [$WindowToImage GetOutputPort]

set flagRecordingInteraction 0
proc RecordWindowInteraction { } {
  if { $::flagRecordingInteraction } {
    set ::flagRecordingInteraction 0
    CloseVideo
  } else {
    if { [ $::VideoWriter GetClassName ] eq "vtkAVIWriter" } {
      set ext ".avi"
    } else {
      set ext ".mpeg"
    }
    set path "record${ext}"
    OpenVideo $path
    set ::flagRecordingInteraction 1
    ScheduleNextFrame
  }
}

proc OpenVideo { path } {
  global VideoWriter
  global WindowToImage

  #puts "OpenVideo"

  $VideoWriter SetInputConnection [$WindowToImage GetOutputPort]
  $VideoWriter SetFileName $path
  $VideoWriter SetRate [StudyHelper::GetNeededFrameRate]
  $VideoWriter Start
}

proc CloseVideo { } {
  #puts "CloseVideo"

  global VideoWriter

  $VideoWriter End  
}

proc ScheduleNextFrame { } {
  #puts "ScheduleNextFrame"
  if { $::flagRecordingInteraction } {
    WriteFrameToVideo
    after [expr 1000/24] ScheduleNextFrame
    #update
  }
}

proc WriteFrameToVideo { } {
  global WindowToImage
  global VideoWriter

  #puts "WriteFrameToVideo"

  vtkWindowToImageFilter w2i
  w2i SetInput renWin
  w2i Update
  $VideoWriter SetInputConnection [w2i GetOutputPort]

  #$WindowToImage SetInput renWin
  #$WindowToImage Update
  #renWin Render

  $VideoWriter Update
  $VideoWriter Write
  w2i Delete
}

# Add the actors to the renderer, set the background and size
#
ren1 AddActor isoActor
ren1 SetBackground 1 1 1
renWin SetSize 500 500
ren1 SetBackground 0.7 0.7 0.7
#iren Initialize

# render the image
#
#iren AddObserver UserEvent {wm deiconify .vtkInteract}

# prevent the tk window from showing up then start the event loop
wm withdraw .

toplevel .top
wm title .top "Test: Probe Isosurface"
wm protocol .top WM_DELETE_WINDOW ::vtk::cb_exit

set display_frame [frame .top.f1]
set ctrl_buttons [frame .top.btns]

grid $display_frame -row 0 -column 0 -sticky snew
grid $ctrl_buttons  -row 1 -column 0 -sticky snew
grid rowconfigure .top 0 -weight 1
grid columnconfigure .top 0 -weight 1


set quit_button [button $ctrl_buttons.btn1 -text "Quit" -command  ::vtk::cb_exit]
grid $quit_button -row 0 -column 0 -sticky s

grid columnconfigure $ctrl_buttons 1 -weight 1


set render_widget [vtkTkRenderWidget $display_frame.r \
                       -width 600 -height 600  -rw renWin]

grid $render_widget -sticky snew
grid rowconfigure $display_frame 0 -weight 1
grid columnconfigure $display_frame 0 -weight 1
::vtk::bind_tk_render_widget $render_widget

set iren [[$render_widget GetRenderWindow] GetInteractor]
#vtkRenderWindowInteractor iren
#    iren SetRenderWindow renWin

vtkInteractorStyleTrackballCamera style
$iren SetInteractorStyle style

if { $cmdopt(-wss) ne "" } {
  vtkScalarBarActor scalarBar
    scalarBar SetOrientationToHorizontal
    scalarBar SetLookupTable lut
  vtkScalarBarWidget barWidget
    barWidget SetScalarBarActor scalarBar
    barWidget SetInteractor $iren
    barWidget On
}

[ren1 GetActiveCamera] Zoom 1.5
ren1 ResetCamera
renWin Render


