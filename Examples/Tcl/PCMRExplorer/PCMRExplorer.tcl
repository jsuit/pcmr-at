package require vtk
package require vtkinteraction

# This example demonstrates how to use the vtkImagePlaneWidget 
# to probe a 3D image dataset with three orthogonal planes.  
# Buttons are provided to:
# a) capture the render window display to a tiff file
# b) x,y,z buttons reset the widget to orthonormal 
#    positioning, set the horizontal slider to move the
#    associated widget along its normal, and set the
#    camera to face the widget  
# c) right clicking on x,y,z buttons pops up a menu to set 
#    the associated widget's reslice interpolation mode 
#

# Start by loading some data.
#
if { $argc > 0 } {
  source "StudyHelper.tcl"
  StudyHelper::InitFromArgv

  set allImages(MAG) [StudyHelper::GetMagnitudeImage -vtk yes]
  set allImages(VX) [StudyHelper::GetVelocityImage -component "VX" -vtk yes]
  set allImages(VY) [StudyHelper::GetVelocityImage -component "VY" -vtk yes]
  set allImages(VZ) [StudyHelper::GetVelocityImage -component "VZ" -vtk yes]
  set allImages(|V|) [StudyHelper::GetVelocityImage -component "|V|" -vtk yes]

  set theImage $allImages(|V|)
  
  if {0} {
    set imgVector [ StudyHelper::GetVectorImage -masked no ]
    vtkVectorNorm v16
    v16 NormalizeOff
    v16 SetInput $imgVector
    v16 Update
  }

} else {
vtkVolume16Reader v16
  v16 SetDataDimensions 64 64
  v16 SetDataByteOrderToLittleEndian
  v16 SetFilePrefix "$VTK_DATA_ROOT/Data/headsq/quarter"
  v16 SetImageRange 1 93
  v16 SetDataSpacing 3.2 3.2 1.5
  v16 Update
  set theImage [v16 GetOutput]
}

scan [$theImage GetExtent] "%d %d %d %d %d %d" \
        xMin xMax yMin yMax zMin zMax

set spacing [$theImage GetSpacing]
set sx [lindex $spacing 0]
set sy [lindex $spacing 1]
set sz [lindex $spacing 2]

set origin [$theImage GetOrigin]
set ox [lindex $origin 0]
set oy [lindex $origin 1]
set oz [lindex $origin 2]

# An outline is shown for context.
#
vtkOutlineFilter outline
  #outline SetInputConnection [v16 GetOutputPort]
  outline SetInput $theImage

vtkPolyDataMapper outlineMapper
  outlineMapper SetInputConnection [outline GetOutputPort]

vtkActor outlineActor
  outlineActor SetMapper outlineMapper

# The shared picker enables us to use 3 planes at one time
# and gets the picking order right
#
vtkCellPicker picker
  picker SetTolerance 0.005

foreach {dimX dimY dimZ} [$theImage GetDimensions] break;

# The 3 image plane widgets are used to probe the dataset.
#
vtkImagePlaneWidget planeWidgetX
  planeWidgetX DisplayTextOn
  planeWidgetX SetInput $theImage
  planeWidgetX SetPlaneOrientationToXAxes
  planeWidgetX SetSliceIndex [expr {$dimX/2}]
  planeWidgetX SetPicker picker
  planeWidgetX SetKeyPressActivationValue "x"
  set prop1 [planeWidgetX GetPlaneProperty]
  $prop1 SetColor 1 0 0    

vtkImagePlaneWidget planeWidgetY
  planeWidgetY DisplayTextOn
  planeWidgetY SetInput $theImage
  planeWidgetY SetPlaneOrientationToYAxes
  planeWidgetY SetSliceIndex [expr {$dimY/2}]
  planeWidgetY SetPicker picker
  planeWidgetY SetKeyPressActivationValue "y"
  set prop2 [planeWidgetY GetPlaneProperty]
  $prop2 SetColor 1 1 0
  planeWidgetY SetLookupTable [planeWidgetX GetLookupTable]

# for the z-slice, turn off texture interpolation:
# interpolation is now nearest neighbour, to demonstrate
# cross-hair cursor snapping to pixel centers
#
vtkImagePlaneWidget planeWidgetZ
  planeWidgetZ DisplayTextOn
  planeWidgetZ SetInput $theImage
  planeWidgetZ SetPlaneOrientationToZAxes
  planeWidgetZ SetSliceIndex [expr {$dimZ/2}]
  planeWidgetZ SetPicker picker
  planeWidgetZ SetKeyPressActivationValue "z"
  set prop3 [planeWidgetZ GetPlaneProperty]
  $prop3 SetColor 0 0 1
  planeWidgetZ SetLookupTable [planeWidgetX GetLookupTable]

set current_widget planeWidgetZ
set mode_widget  planeWidgetZ

planeWidgetX AddObserver EndWindowLevelEvent WindowLevelXCallback
planeWidgetY AddObserver EndWindowLevelEvent WindowLevelYCallback
planeWidgetZ AddObserver EndWindowLevelEvent WindowLevelZCallback
planeWidgetX AddObserver ResetWindowLevelEvent WindowLevelXCallback
planeWidgetY AddObserver ResetWindowLevelEvent WindowLevelYCallback
planeWidgetZ AddObserver ResetWindowLevelEvent WindowLevelZCallback

# Create a composite orientation marker using
# vtkAnnotatedCubeActor and vtkAxesActor.
#
set xLabel [StudyHelper::GetOrientationLabel 0]
set yLabel [StudyHelper::GetOrientationLabel 1]
set zLabel [StudyHelper::GetOrientationLabel 2]

vtkAnnotatedCubeActor cube
  cube SetXMinusFaceText [lindex $xLabel 0]
  cube SetXPlusFaceText  [lindex $xLabel 1]
  cube SetYMinusFaceText [lindex $yLabel 0]
  cube SetYPlusFaceText  [lindex $yLabel 1]
  cube SetZMinusFaceText [lindex $zLabel 0]
  cube SetZPlusFaceText  [lindex $zLabel 1]
  cube SetXFaceTextRotation 90
  #cube SetYFaceTextRotation 180
  cube SetZFaceTextRotation -90
  cube SetFaceTextScale 0.65
  set property [ cube GetCubeProperty ]
  $property SetColor 0.5 1 1
  set property [ cube GetTextEdgesProperty ]
  $property SetLineWidth 1
  $property SetDiffuse 0
  $property SetAmbient 1
  $property SetColor 0.18 0.28 0.23
  set property [ cube GetXPlusFaceProperty ]
  $property SetColor 0 0 1
  $property SetInterpolationToFlat
  set property [ cube GetXMinusFaceProperty ]
  $property SetColor 0 0 1
  $property SetInterpolationToFlat
  set property [ cube GetYPlusFaceProperty ]
  $property SetColor 0 1 0
  $property SetInterpolationToFlat
  set property [ cube GetYMinusFaceProperty ]
  $property SetColor 0 1 0
  $property SetInterpolationToFlat
  set property [ cube GetZPlusFaceProperty ]
  $property SetColor 1 0 0
  $property SetInterpolationToFlat
  set property [ cube GetZMinusFaceProperty ]
  $property SetColor 1 0 0
  $property SetInterpolationToFlat

vtkAxesActor axes
  axes SetShaftTypeToCylinder
  axes SetXAxisLabelText "x"
  axes SetYAxisLabelText "y"
  axes SetZAxisLabelText "z"
  axes SetTotalLength 1.5 1.5 1.5
  vtkTextProperty tprop
  #tprop ItalicOn
  #tprop ShadowOn
  #tprop SetFontFamilyToTimes
  [ axes GetXAxisCaptionActor2D ] SetCaptionTextProperty tprop
  vtkTextProperty tprop2
  #tprop2 ShallowCopy tprop
  [ axes GetYAxisCaptionActor2D ] SetCaptionTextProperty tprop2
  vtkTextProperty tprop3
  #tprop3 ShallowCopy tprop
  [ axes GetZAxisCaptionActor2D ] SetCaptionTextProperty tprop3

# Combine the two actors into one with vtkPropAssembly ...
#
  vtkPropAssembly assembly
  assembly AddPart axes
  assembly AddPart cube

# Add the composite marker to the widget.  The widget
# should be kept in non-interactive mode and the aspect
# ratio of the render window taken into account explicitly, 
# since the widget currently does not take this into 
# account in a multi-renderer environment.
# 
  vtkOrientationMarkerWidget marker
  marker SetOutlineColor 0.93 0.57 0.13
  marker SetOrientationMarker assembly
  marker SetViewport 0.0 0.0 0.15 0.3

# Create the RenderWindow and Renderer
#
vtkRenderer ren1
vtkRenderWindow renWin
  renWin AddRenderer ren1

# Add the outline actor to the renderer, set the background color and size
#
ren1 AddActor outlineActor
renWin SetSize 600 600
ren1 SetBackground  0.1 0.1 0.2


# Create the GUI
#
wm withdraw .
toplevel .top
wm title .top "vtkImagePlaneWidget Example"
wm protocol .top WM_DELETE_WINDOW ::vtk::cb_exit

set popm [menu .top.mm -tearoff 0]
set mode 1
$popm add radiobutton -label "nearest" -variable mode -value 0  \
           -command SetInterpolation
$popm add radiobutton -label "linear" -variable mode -value 1  \
           -command SetInterpolation
$popm add radiobutton -label "cubic" -variable mode -value 2  \
           -command SetInterpolation

set display_frame [frame .top.f1]

set ctrl_buttons [frame .top.btns]

pack $display_frame $ctrl_buttons \
        -side top -anchor n \
        -fill both -expand f

set quit_button [button $ctrl_buttons.btn1  \
        -text "Quit" \
        -command  ::vtk::cb_exit]

set capture_button [button $ctrl_buttons.btn2  \
        -text "Tif" \
        -command CaptureImage]

proc AlignAxis { axis } {
  array set id {
    0 X
    1 Y
    2 Z
  }
  Align$id($axis)axis
}

set x_button [button $ctrl_buttons.btn3  \
                  -text  "Sagittal" \
                  -command [list AlignAxis [StudyHelper::GetSagittalAxis]]]

set y_button [button $ctrl_buttons.btn4  \
                  -text "Axial" \
                  -command [list AlignAxis [StudyHelper::GetAxialAxis]]]

set z_button [button $ctrl_buttons.btn5  \
                  -text "Coronal" \
                  -command [list AlignAxis [StudyHelper::GetCoronalAxis]]]

set last_btn -1
bind $x_button <Button-3> "set last_btn 0; configMenu; $popm post %X %Y"
bind $y_button <Button-3> "set last_btn 1; configMenu; $popm post %X %Y"
bind $z_button <Button-3> "set last_btn 2; configMenu; $popm post %X %Y"

# Share the popup menu among buttons, keeping
# track of associated widget's interpolation mode
#
proc configMenu { } {
  global last_btn popm mode mode_widget
  if { $last_btn == 0 } {
    set mode_widget planeWidgetX
  } elseif { $last_btn == 1 } {
    set mode_widget planeWidgetY
  } else {
    set mode_widget planeWidgetZ
  }
  set mode [$mode_widget GetResliceInterpolate]    
  $popm entryconfigure $last_btn -variable mode   
}

pack $quit_button $capture_button $x_button $y_button $z_button \
        -side left \
        -expand t -fill both

# Create the render widget
#
set renderer_frame [frame $display_frame.rFm]

pack $renderer_frame \
        -padx 3 -pady 3 \
        -side left -anchor n \
        -fill both -expand f

set render_widget [vtkTkRenderWidget $renderer_frame.r \
        -width 600 \
        -height 600 \
        -rw renWin]

pack $render_widget $display_frame  \
        -side top -anchor n \
        -fill both -expand f

# Add a slice scale to browse the current slice stack
#

set slice_number [$current_widget GetSliceIndex]

scale .top.slice \
        -from $zMin \
        -to $zMax \
        -orient horizontal \
        -command SetSlice \
        -variable slice_number \
        -label "Slice"

pack .top.slice \
        -fill x -expand f

proc SetSlice {slice} {
  global current_widget 
  $current_widget SetSliceIndex $slice
  ren1 ResetCameraClippingRange
  renWin Render
}

::vtk::bind_tk_render_widget $render_widget
# Set the interactor for the widgets
#
set iact [[$render_widget GetRenderWindow] GetInteractor]
planeWidgetX SetInteractor $iact
planeWidgetX On
planeWidgetY SetInteractor $iact
planeWidgetY On
planeWidgetZ SetInteractor $iact
planeWidgetZ On

marker SetInteractor $iact
marker SetEnabled 1
marker InteractiveOff

# Create an initial interesting view
#
set cam1 [ren1 GetActiveCamera]
$cam1 Elevation 110
$cam1 SetViewUp 0 0 -1
$cam1 Azimuth 45
ren1 ResetCameraClippingRange

# Render it
#
$render_widget Render

# Supporting procedures
#

# Align the camera so that it faces the desired widget
#
# Sagittal
#
#    - look from  L(eft) to R(ight)
#    - A(nterior) located at the left of the screen (Coronal)
#    - I(nferior) located at the botton (Axial)
#
# Axial
#
#    - look from S(uperior) to I(nferior)
#    - A(nterior) located at the left of the screen
#    - L(eft) located at the botton (Sagittal)
#
# Coronal
#
#    - look from A(anterior) to P(osterior)
#    - R(ight) located at the left of the screen
#    - I(nferior) located at the botton (Axial)

proc AlignCamera { } {
  global ox oy oz sx sy sz xMax xMin yMax yMin zMax zMin
  global current_widget

  set iaxis [$current_widget GetPlaneOrientation]
  if {[catch {StudyHelper::GetBodyPlaneName $iaxis} bodyPlane]} {
    return
  }
  
  set c(0) [expr $ox + (0.5*($xMax - $xMin))*$sx]
  set c(1) [expr $oy + (0.5*($yMax - $yMin))*$sy]
  set c(2) [expr $oy + (0.5*($zMax - $zMin))*$sz]
  set s(0) $sx
  set s(1) $sy
  set s(2) $sz
  array set up {
    0 0
    1 0
    2 0
  }
  # direction from center to camera position
  array set focal {
    0 0
    1 0
    2 0    
  }
  array set otherAxes {
    0 {1 2}
    1 {0 2}
    2 {0 1}
  }

  array set anatomicalLocation {
    Sagittal,near    "L"
    Sagittal,partner "Axial"
    Sagittal,down    "I"
    Axial,near       "S"
    Axial,partner    "Sagittal"
    Axial,down       "L"
    Coronal,near     "A"
    Coronal,partner  "Axial"
    Coronal,down     "I"
  }
  set nearfar [StudyHelper::GetOrientationLabel $iaxis]
  if {[lindex $nearfar 0] eq $anatomicalLocation($bodyPlane,near)} {
    set focal($iaxis) -1
  } else {
    set focal($iaxis) 1
  }
  foreach ia $otherAxes($iaxis) {
    if {[StudyHelper::GetBodyPlaneName $ia] eq 
        $anatomicalLocation($bodyPlane,partner)} {
      set nf [StudyHelper::GetOrientationLabel $ia]
      if {[lindex $nf 0] eq $anatomicalLocation($bodyPlane,down)} {
        set up($ia) 1
      } else {
        set up($ia) -1
      }
      break
    }
  }
  foreach ia {0 1 2} {
    set p($ia) [expr {$c($ia) + $focal($ia)*$s($ia)}]
  }
  set camera [ ren1 GetActiveCamera ]
  $camera SetViewUp $up(0) $up(1) $up(2)
  $camera SetFocalPoint $c(0) $c(1) $c(2)
  $camera SetPosition $p(0) $p(1) $p(2)
  $camera OrthogonalizeViewUp
  ren1 ResetCamera
  #ren1 ResetCameraClippingRange 
  renWin Render
}

proc AlignCamera0 { } {
  global ox oy oz sx sy sz xMax xMin yMax yMin zMax zMin slice_number
  global current_widget
  set cx [expr $ox + (0.5*($xMax - $xMin))*$sx]
  set cy [expr $oy + (0.5*($yMax - $yMin))*$sy]
  set cz [expr $oy + (0.5*($zMax - $zMin))*$sz]
  set vx 0
  set vy 0
  set vz 0
  set nx 0
  set ny 0
  set nz 0
  set iaxis [$current_widget GetPlaneOrientation]
  switch $iaxis {
    0 {
      set vz -1
      set nx [expr $ox + $xMax*$sx]
      set cx [expr $ox + $slice_number*$sx]
    }
    1 {
      set vz -1
      set ny [expr $oy + $yMax*$sy]
      set cy [expr $oy + $slice_number*$sy]
    }
    2 {
      set vy -1
      set nz [expr -$oz - $zMax*$sz]
      set cz [expr $oz + $slice_number*$sz]
    }
  }
  set px [expr $cx + $nx*2]
  set py [expr $cy + $ny*2]
  set pz [expr $cz + $nz*3]

  set camera [ ren1 GetActiveCamera ]
  $camera SetViewUp $vx $vy $vz
  $camera SetFocalPoint $cx $cy $cz
  $camera SetPosition $px $py $pz
  $camera OrthogonalizeViewUp
  ren1 ResetCamera
  #ren1 ResetCameraClippingRange 
  renWin Render
}

# Capture the display and place in a tiff
#
proc CaptureImage { } {
  vtkWindowToImageFilter w2i
  vtkTIFFWriter writer

  w2i SetInput renWin
  w2i Update
  writer SetInputConnection [w2i GetOutputPort]
  writer SetFileName image.tif
  renWin Render
  writer Write

  writer Delete
  w2i Delete
}

# Align the widget back into orthonormal position,
# set the slider to reflect the widget's position,
# call AlignCamera to set the camera facing the widget
#
proc AlignXaxis { } {
  global xMax xMin current_widget slice_number
  set po [ planeWidgetX GetPlaneOrientation ]
  if { $po == 3 } {
    planeWidgetX SetPlaneOrientationToXAxes
    set slice_number [expr ($xMax - $xMin)/2]
    planeWidgetX SetSliceIndex $slice_number
  } else {
    set slice_number [planeWidgetX GetSliceIndex]
  }
  set current_widget planeWidgetX
  .top.slice config -from $xMin -to $xMax 
  .top.slice set $slice_number
  AlignCamera
}

proc AlignYaxis { } {
  global yMin yMax current_widget slice_number
  set po [ planeWidgetY GetPlaneOrientation ]
  if { $po == 3 } {
    planeWidgetY SetPlaneOrientationToYAxes
    set slice_number [expr ($yMax - $yMin)/2]
    planeWidgetY SetSliceIndex $slice_number
  } else {
    set slice_number [planeWidgetY GetSliceIndex]
  }
  set current_widget planeWidgetY
  .top.slice config -from $yMin -to $yMax
  .top.slice set $slice_number
  AlignCamera
}

proc AlignZaxis { } {
  global zMin zMax current_widget slice_number
  set po [ planeWidgetZ GetPlaneOrientation ]
  if { $po == 3 } {
    planeWidgetZ SetPlaneOrientationToZAxes
    set slice_number [expr ($zMax - $zMin)/2]
    planeWidgetZ SetSliceIndex $slice_number
  } else {
    set slice_number [planeWidgetZ GetSliceIndex]
  }
  set current_widget planeWidgetZ
  .top.slice config -from $zMin -to $zMax 
  .top.slice set $slice_number
  AlignCamera
}

# Set the widget's reslice interpolation mode
# to the corresponding popup menu choice
#
proc SetInterpolation { } {
  global mode_widget mode 
  if { $mode == 0 } {
    $mode_widget TextureInterpolateOff
  } else {
    $mode_widget TextureInterpolateOn
  }
  $mode_widget SetResliceInterpolate $mode
  renWin Render
}

proc WindowLevelXCallback {} {
  set w [planeWidgetX GetWindow]
  set l [planeWidgetX GetLevel]  
  planeWidgetY SetWindowLevel $w $l 1 
  planeWidgetZ SetWindowLevel $w $l 1
}

proc WindowLevelYCallback {} {
  set w [planeWidgetY GetWindow]
  set l [planeWidgetY GetLevel]  
  planeWidgetX SetWindowLevel $w $l 1 
  planeWidgetZ SetWindowLevel $w $l 1
}

proc WindowLevelZCallback {} {
  set w [planeWidgetZ GetWindow]
  set l [planeWidgetZ GetLevel]  
  planeWidgetX SetWindowLevel $w $l 1 
  planeWidgetY SetWindowLevel $w $l 1
}

proc ChangeInputImage { name } {
  global allImages

  outline SetInput $allImages($name)
  
  foreach p {planeWidgetX planeWidgetY planeWidgetZ} {
    set index [$p GetSliceIndex]
    $p SetInput $allImages($name)
    $p SetSliceIndex $index
  }
  renWin Render
}

# Have problem while changing the image, the problem is related to the
# window level:
#
#
#  ChangeInputImage VY
#  planeWidgetX SetWindowLevel 360 0 0
#
#  Solution, keep track of the window level used for each image, after
#  changing the image set the las window level, but you have to set it
#  forall of the plane widgets
#
#  planeWidgetX SetWindowLevel 360 0 0
#  planeWidgetY SetWindowLevel 360 0 0
#  planeWidgetZ SetWindowLevel 360 0 0