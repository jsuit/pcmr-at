package require snit

snit::type MedicalOrientation {

  typevariable AxesLabel -array {
    A,bodyplane Coronal
    P,bodyplane Coronal
    L,bodyplane Sagittal
    R,bodyplane Sagittal
    S,bodyplane Axial
    I,bodyplane Axial
    A,next P
    P,next A
    L,next R
    R,next L
    S,next I
    I,next S
  }

  variable Orientation

  typemethod CheckOrientation { value } {
    if {[string length $value] != 3} {
      error "invalid orientation value '$value', must be a string of length 3"
    }
  }

  constructor {} {
    set Orientation "ASL"
  }

  destructor {
  }

  method SetOrientation { value } {
    MedicalOrientation CheckOrientation $value
    set Orientation $value
  }

  method GetOrientation {} {
    return $Orientation
  }

  method GetPlaneLabel { axis } {
    if { $axis < 0 || $axis > 2} {
      error "invalid axis index, must be: 0, 1, or 2"
    }
    set axisLabel [string index $Orientation $axis]
  }
  
  method GetBodyPlaneName { axis } {
    if { $axis < 0 || $axis > 2} {
      error "invalid axis index, must be: 0, 1, or 2"
    }
    set axisLabel [string index $Orientation $axis]
    return $AxesLabel($axisLabel,bodyplane)
  }

  method GetOrientationLabel { axis } {
    if { $axis < 0 || $axis > 2} {
      error "invalid axis index, must be: 0, 1, or 2"
    }
    set axisLabel [string index $Orientation $axis]
    return [list $axisLabel $AxesLabel($axisLabel,next)]
  }

  method GetSagittalAxis { } {
    set i [string first "R" $Orientation]
    if {$i != -1} {
      return $i
    }
    set i [string first "L" $Orientation]
    if {$i != -1} {
      return $i
    }
    error "could not find sagittal axis, wrong image orientation '$Orientation'"
  }

  method GetCoronalAxis { } {
    set i [string first "A" $Orientation]
    if {$i != -1} {
      return $i
    }
    set i [string first "P" $Orientation]
    if {$i != -1} {
      return $i
    }
    error "could not find coronal axis, wrong image orientation '$Orientation'"
  }

  method GetAxialAxis { } {
    set i [string first "S" $Orientation]
    if {$i != -1} {
      return $i
    }
    set i [string first "I" $Orientation]
    if {$i != -1} {
      return $i
    }
    error "could not find axial axis, wrong image orientation '$Orientation'"
  }
}