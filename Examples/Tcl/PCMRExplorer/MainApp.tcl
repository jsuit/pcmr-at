package require snit

snit::type MainApp {
  typevariable argv $::argv
  typevariable argc $::argc

  typevariable WorkingDirectory [file normalize [file dirname [info script]]]

  typemethod GetWorkingDirectory {} {
    return $WorkingDirectory
  }

  typemethod GetArgValue {} {
    return $argv
  }

  typemethod GetArgCount {} {
    return $argc
  }

  constructor {} {
    puts "MainApp constructor"

    set argv $::argv
    set argc $::argc
  }

  destructor {
    puts "MainApp destructor"
  }

  method run {} {
    
  }

  method _createViewer {} {
  }

  method _createRenderer {} {
  }
}