package require vtk

namespace eval StudyHelper {
  variable TmpDir
  variable argc $::argc
  variable argv $::argv
  variable MyCwd [ file dirname [ info script ] ]

  variable HeaderInfo
  variable BaseDir ""
  variable MeshFileName ""
  variable CurrentTimeStep 0
  variable itkImageMask ""
  variable ImageDistanceMap ""
  variable ShouldInvertComponent
  variable MeshObject ""

  array set ShouldInvertComponent {
    0 no
    1 yes
    2 no
  }

  variable CacheVolumes

  if { $tcl_platform(platform) eq "unix" } {
    set TmpDir /tmp
  } else {
    if { [ info exist env(TEMP) ] && [ file exists $env(TEMP) ] } {
      set TmpDir $env(TEMP)
    } elseif { [ info exist env(TMP) ] && [ file exists $env(TMP) ] } {
      set TmpDir $env(TMP)
    } else {
      set TmpDir "C:/"
    }
  }
  
  proc GetTmpDir { } {
    variable TmpDir

    return $TmpDir
  }

  proc GetBaseDirectoryFromArgv { } {
    variable argc
    variable argv

    _ValidateArgvLength
    array set options {
      -timestep 0
    }
    if { [ file exists "C:/data/studies/frances" ] } {
      set options(-study) "C:/data/studies/frances"
    } else {
      set options(-study) {C:\BioCIMNE\Data\STUDIES\ST01}
    }
    array set options $argv

    set studyDir $options(-study)
    CheckBaseDirectory $studyDir
    return $studyDir
  }

  proc GetMeshFileFromArgv { } {
    variable argc
    variable argv

    _ValidateArgvLength
    set options(-mesh) ""
    array set options $argv

    if { $options(-mesh) ne "" } {
      if { ![ file exists $options(-mesh) ] ||
           ![ file readable $options(-mesh) ] } {
        error "Mesh file '$options(-mesh)' doesn't exists or is not readable"
      }
    }
    return $options(-mesh)
  }

  proc GetTimeStepFromArgv { } {
    variable argc
    variable argv

    _ValidateArgvLength
    array set options {
      -timestep 0
    }
    array set options $::argv
    set timeStep $options(-timestep)
    if { ![ string is integer $timeStep ] || $timeStep < 0 } {
      error "Invalid time step requested '$timeStep' must be positive integer"
    }
    return $timeStep
  }

  proc CheckBaseDirectory { {studyDir ""} } {
    variable BaseDir

    if { $studyDir eq "" } {
      set studyDir $BaseDir
    } 
    if { ![file exists $studyDir ] } {
      error "The study '$studyDir' does not exists."
    }
    if { [ file type $studyDir ] eq "link" } {
      set _studyDir [ file readlink $studyDir ]
      puts "'$studyDir' is a link point to '$_studyDir'"
      set studyDir $_studyDir
    }

    if { [ file type $studyDir ] ne "directory" } {
      error "The study requested must be a directory: '$studyDir'"
      exit
    }

    if { ![ file exists [ file join $studyDir "header.sth" ] ] } {
      error "The directory '$studyDir' is not a valid Aorta4D study: does not contains header.sth"
    }
  }
  
  proc GetVectorFileNames { {timeStep ""} } {
    variable BaseDir
    variable CurrentTimeStep

    if { $timeStep eq "" } {
      set timeStep $CurrentTimeStep
    }
    array unset fileComponent
    foreach comp {0 1 2} {
      set fname [ file join $BaseDir "vct_${timeStep}_${comp}.vtk" ]
      if { ![ file exists $fname ] } {
	puts "Invalid study, the file '$fname' does not exists"
	exit
      }
      set fileComponent($comp) $fname
    }
    return [ array get fileComponent ]
  }
  
  proc WriteImage { fileName image args } {
    array set options {
      -writer ""
      -msg ""
    }
    array set options $args
    if { $options(-writer) eq "" } {
      set writer [ VTKScalarImageWriter ]
    } else {
      set writer $options(-writer)
    }
    set outputName [ file join [ GetTmpDir ] $fileName ]
    $writer SetInput $image
    $writer SetFileName $outputName
    if { $options(-msg) eq "" } {
      set msg "Writing imagen to $outputName ..."
    } else {
      set msg [string map [list %O $outputName] $options(-msg)]
    }
    puts $msg
    $writer Update
    if  { $options(-writer) eq "" } {
      rename $writer ""
    }
  }

  proc CheckSet { filter method value } {
    # apply the Set method
    $filter Set$method $value
    set checkValue [ $filter Get$method ]
    if { $checkValue != $value } {
      error "Get$method returned $checkValue and must be $value"
    }
  }

  proc ReadImageMask { } {
    variable BaseDir
    variable itkImageMask

    CheckBaseDirectory
    set maskFile [ file join $BaseDir "mask.vtk" ]
    set mask3DReader [  Image3DReader ]
    $mask3DReader SetFileName $maskFile
    puts "Reading mask file from '$maskFile'"
    $mask3DReader Update
    set itkImageMask [ $mask3DReader GetOutput ]
  }

  proc ReadImagePremask { } {
    variable BaseDir

    CheckBaseDirectory
    set maskFile [ file join $BaseDir "premask.vtk" ]
    set mask3DReader [  Image3DReader ]
    $mask3DReader SetFileName $maskFile
    puts "Reading premask file from '$maskFile'"
    $mask3DReader Update
    return [ $mask3DReader GetOutput ]
    # should release pipeline
  }

  proc ReadHeaderInfoFromDir { dir } {
    variable HeaderInfo

    set handle [open [file join $dir "header.sth"] "r"]
    gets $handle HeaderInfo
    if { ![dict exists $HeaderInfo venc] } {
      dict set HeaderInfo venc 150
    }
    # this is to fix the name smoothness_trheshold
    if { [dict exists $HeaderInfo smoothness_trheshold] } {
      dict set HeaderInfo smoothness_threshold \
          [dict get $HeaderInfo smoothness_trheshold]
      dict unset HeaderInfo smoothness_trheshold
    }
    if { ![dict exists $HeaderInfo length_time_step] } {
      set n [ dict get $HeaderInfo time_step_count ]
      # assume the adquisition is 1 second long, the timestep is
      # meassured in miliseconds
      dict set HeaderInfo length_time_step [ expr {1000.0/$n} ]
    }
    close $handle
  }

  proc GetNeededFrameRate { } {
    return [ expr {int( (1.0/([GetLengthOfTimeStep])) * 1000 )} ]
  }

  proc GetNumberOfTimeStep { } {
    variable HeaderInfo

    return [ dict get $HeaderInfo time_step_count ]
  }

  proc GetVelocityEncoding { } {
    variable HeaderInfo

    return [ dict get $HeaderInfo venc ]
  }

  proc GetLengthOfTimeStep { } {
    variable HeaderInfo

    return [ dict get $HeaderInfo length_time_step ]
  }

  proc GetImageMask { } {
    variable itkImageMask

    if { $itkImageMask eq "" } {
      ReadImageMask
    }
    return $itkImageMask
  }

  proc GetMeshObject { } {
    variable MeshObject

    if { $MeshObject eq "" } {
      ReadMeshFromFile
    }
    return $MeshObject
  }

  proc ReadMeshFromFile { } {
    variable MeshObject
    variable MeshFileName

    puts "ReadMeshFromFile: $MeshFileName"
    vtkUnstructuredGridReader meshReader
    meshReader SetFileName $MeshFileName
    meshReader Update

    set MeshObject [meshReader GetOutput]
    #meshReader Delete
  }

  proc GetDistanceMapFromMask { } {
    variable ImageDistanceMap

    if { $ImageDistanceMap eq "" } {
      _BuildDistanceMapFromMask
    }
    return $ImageDistanceMap
  }

  proc GetItkDistanceMapFromMask { } {
    set imageMask [ GetImageMask ]
    set distFilter [ SignedMaurerDistanceMapFilter ]
    $distFilter SquaredDistanceOff
    $distFilter UseImageSpacingOn
    $distFilter SetInput $imageMask
    $distFilter Update
    $distFilter GetOutput
    # should release pipeline
  }

  proc _BuildDistanceMapFromMask { } {
    variable ImageDistanceMap

    set imageMask [ GetImageMask ]
    set distFilter [ SignedMaurerDistanceMapFilter ]
    $distFilter SquaredDistanceOff
    $distFilter UseImageSpacingOn
    $distFilter SetInput $imageMask
    $distFilter Update
    set exporter [ VTKScalarImageExport ]
    $exporter SetInput [ $distFilter GetOutput ]
    set importer [ vtkImageImport New ]
    $exporter Connect2VTK $importer
    $importer Update
    set ImageDistanceMap [ $importer GetOutput ]
    return $ImageDistanceMap
  }
  
  proc GetCurrentTimeStep { } {
    variable CurrentTimeStep
    
    return $CurrentTimeStep
  }

  proc GetNextTimeStep { } {
    variable CurrentTimeStep
    
    set ts $CurrentTimeStep
    incr ts
    if { $ts >= [GetNumberOfTimeStep] } {
      set ts 0
    }
    return $ts
  }

  proc ExportCurrentVectorImage { path } {
    vtkStructuredPointsWriter writer
    writer SetFileName $path
    writer SetInput [ GetVectorImage ]
    writer Update
    writer Delete
  }

  proc GetItkVectorImage { args } {
    variable ShouldInvertComponent

    set options(-timestep) [GetCurrentTimeStep]
    array set options $args
    set numberTS [GetNumberOfTimeStep]
    set reqTS $options(-timestep)
    if { $reqTS < 0 || 
         $reqTS >= $numberTS } {
      error "invalid timestep value $reqTS, must be in \[0..$numberTS\]"
    }

    array set fileComponent [ GetVectorFileNames $reqTS ]

    if {[package vcompare [package require itktcl] "2.0"]>=0} {
      set composer [ComposeImageFilter]
    } else {
      set composer [ComposeFilter]
    }

    foreach comp {0 1 2} {
      set readerObjects($comp) [ Image3DReader ]  
      $readerObjects($comp) SetFileName $fileComponent($comp)
      puts "Reading component $comp from $fileComponent($comp)"
      $readerObjects($comp) Update
      set idx [ expr { $comp + 1 } ]
      if { $ShouldInvertComponent($comp) } {
        set invert [ ShiftScaleFilter ]
        $invert SetShift 0
        $invert SetScale -1
        $invert SetInput [ $readerObjects($comp) GetOutput ]
        $composer SetInput$idx [ $invert GetOutput ]
      } else {
        $composer SetInput$idx [ $readerObjects($comp) GetOutput ]
      }
    }
    return [$composer GetOutput]
  }

  proc GetVectorImage { args } {
    variable Basedir
    variable ShouldInvertComponent
    variable CacheVolumes

    array set options {
      -masked yes
    }
    set options(-timestep) [GetCurrentTimeStep]
    array set options $args
    parray options
    set numberTS [GetNumberOfTimeStep]
    set reqTS $options(-timestep)
    if { $reqTS < 0 || 
         $reqTS >= $numberTS } {
      error "invalid timestep value $reqTS, must be in \[0..$numberTS\]"
    }
    if { [ info exists CacheVolumes($reqTS) ] } {
      puts "accesing cached volumen for timestep $reqTS"
      return $CacheVolumes($reqTS)
    }
    array set fileComponent [ GetVectorFileNames $reqTS ]

    set composer [ComposeFilter]

    foreach comp {0 1 2} {
      set readerObjects($comp) [ Image3DReader ]  
      $readerObjects($comp) SetFileName $fileComponent($comp)
      puts "Reading component $comp from $fileComponent($comp)"
      $readerObjects($comp) Update
      set idx [ expr { $comp + 1 } ]
      if { $ShouldInvertComponent($comp) } {
        set invert [ ShiftScaleFilter ]
        $invert SetShift 0
        $invert SetScale -1
        $invert SetInput [ $readerObjects($comp) GetOutput ]
        $composer SetInput$idx [ $invert GetOutput ]
      } else {
        $composer SetInput$idx [ $readerObjects($comp) GetOutput ]
      }
    }
    set exporter [ VTKVectorImageExport ]
    if { $options(-masked) } {
      puts "Masking the vector field composed by using the mask from the study"
      set castFilter [ Std2MaskCastFilter ]
      $castFilter SetInput [ GetImageMask ]
      set maskVectorFilter [ VectorMaskFilter ]
      $maskVectorFilter SetInput1 [ $composer GetOutput ]
      $maskVectorFilter SetInput2 [ $castFilter GetOutput ]
      $maskVectorFilter Update
      $exporter SetInput [ $maskVectorFilter GetOutput ]
    } else {
      $exporter SetInput [ $composer GetOutput ]
    }
    set importer [ vtkImageImport New ]
    $exporter Connect2VTK $importer
    $importer Update
    set imgVector [ $importer GetOutput ]

    set pd [ $imgVector GetPointData ]
    set scalars [ $pd GetScalars ]
    set vectors [ $scalars New ]
    $vectors SetName "Velocity"
    $vectors DeepCopy $scalars
    $pd SetVectors $vectors
    $pd RemoveArray "scalars"
    set norm [ vtkVectorNorm New ]
      $norm NormalizeOff
      $norm SetInput $imgVector
      $norm Update

    [[[$norm GetOutput ] GetPointData] GetScalars] SetName "Magnitude"
    set CacheVolumes($reqTS) [ $norm GetOutput ]
    return $CacheVolumes($reqTS)

    # esto no se ejecuta
    vtkAssignAttribute aa
    aa SetInputConnection [ $importer GetOutputPort ]
    aa Assign SCALARS VECTORS POINT_DATA
    return [ aa GetOutput ]
  }

  proc ActivateTimeStep { ts args } {
    variable CurrentTimeStep

    set numberTS [GetNumberOfTimeStep]
    if { $ts < 0 || $ts >= $numberTS } {
      error "invalid timestep value $reqTS, must be in \[0..$numberTS\]"
    }
    set CurrentTimeStep $ts
    set options(-masked) yes
    array set options $args
    GetVectorImage -timestep $ts -masked $options(-masked)
    NotifyObservers ActivateTimeStep
  }

  proc AdvanceTimeStep { } {
    ActivateTimeStep [ GetNextTimeStep ]
  }

  proc GetObservers { event } {
    variable Observers

    if { [ info exists Observers($event) ] } {
      set result $Observers($event)
    } else {
      set result ""
    }
    if { [ info exists Observers($event,last) ] } {
      lappend result $Observers($event,last)
    }
    return $result
  }

  proc InvokeScript { script } {
    set toeval [ string map [ list %T [GetCurrentTimeStep] ] $script ]
    eval $script
  }

  proc NotifyObservers { event } {
    foreach script [GetObservers $event] {
      InvokeScript $script
    }
  }

  proc AddObserver { event script args } {
    variable Observers

    array set options {
      -last no
    }
    array set options $args
    if { $options(-last) } {
      set Observers($event,last) $script
    } else {
      lappend Observers($event) $script
    }
  }

  proc ConnectAA { to from } {
    set AA [ vtkAssignAttribute New ]
    $AA SetInputConnection [ $from GetOutputPort ]
    $AA Assign SCALARS VECTORS POINT_DATA
    $to SetInputConnection [ $AA GetOutputPort ]
  }

  proc GetOutputAA { filter } {
    set AA [ vtkAssignAttribute New ]
    $AA SetInputConnection [ $filter GetOutputPort ]
    $AA Assign SCALARS VECTORS POINT_DATA
    return [ $AA GetOutput ]
  }

  proc _ValidateArgvLength { } {
    variable argc 
    if { [ expr {$argc % 2} ]  } {
      error "Expected an even number of command line arguments: -study path -timestep n -mesh file.vtk"
    }
  }

  proc InitFromArgv { } {
    variable BaseDir
    variable CurrentTimeStep
    variable MeshFileName 

    set BaseDir [ GetBaseDirectoryFromArgv ]
    set CurrentTimeStep [ GetTimeStepFromArgv ]
    set MeshFileName [ GetMeshFileFromArgv ]

    ReadHeaderInfoFromDir $BaseDir
  }
  
  proc LoadItkTcl { } {
    variable MyCwd
    
    set ext [ info sharedlibextension ]
    if { $::tcl_platform(platform) eq "windows" } {
      set prefix ""
    } else {
      set prefix "lib"
    }
    set libname ${prefix}itktcl${ext}
    if { [ info exists ::env(USE_ITKTCL_LIB) ] } {
      puts "using lib $::env(USE_ITKTCL_LIB)"
      load $::env(USE_ITKTCL_LIB)
    }
    package require itktcl
  }
}

# export USE_ITKTCL_LIB=/home/jsperez/GID/BioCIMNE/biomedical/trunk/itkTCL/cmake_build/debug/libitktcl.so
StudyHelper::LoadItkTcl

# TODO:
# Considera pasar la mascara al pipeline de VTK
