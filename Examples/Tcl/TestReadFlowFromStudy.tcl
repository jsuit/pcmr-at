#source "itkTester.tcl"
source "StudyHelper.tcl"

StudyHelper::InitFromArgv

puts [ StudyHelper::GetVectorFileNames ]
puts [ StudyHelper::GetVectorFileNames 3 ]

set imgMask [ StudyHelper::GetImageMask ]
puts [ $imgMask Print ]

set imgVector [ StudyHelper::GetVectorImage -masked yes ]
puts [$imgVector Print]

exit