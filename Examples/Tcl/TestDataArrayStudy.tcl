#lappend auto_path /usr/local/lib/vtk-5.5/

package require vtk
#package require vtkinteraction
source "StudyHelper.tcl"

StudyHelper::InitFromArgv

set imgVector [ StudyHelper::GetVectorImage -masked yes ]

set pd [ $imgVector GetPointData ]
set scalars [ $pd GetScalars ]
set vectors [ $scalar New ]
$vectors SetName vectors
$vectors DeepCopy $scalars
$pd SetVectors $vectors
$pd RemoveArray "scalars"

#set vectors [ $scalars DeepCopy ]
#$vectors SetName "vectors"
#t$pd SetVectors $vectors
