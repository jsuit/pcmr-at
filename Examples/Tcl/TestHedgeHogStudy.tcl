#lappend auto_path /usr/local/lib/vtk-5.5/

package require vtk
package require vtkinteraction
source "StudyHelper.tcl"

StudyHelper::InitFromArgv

proc GotoTimeStep { {ts ""} } {
  if { $ts eq "" } {
    set ts [StudyHelper::GetCurrentTimeStep]
  }
  StudyHelper::ActivateTimeStep $ts -masked no
}

set AnimateStop 1
set WriteToVideo 1

proc AnimateTS { } {
  global AnimateStop
  global currentTimeStep
  global WriteToVideo

  while { !$AnimateStop } {
    set currentTimeStep [ StudyHelper::GetNextTimeStep ]
    GotoTimeStep $currentTimeStep
    if { $WriteToVideo } {
      WriteFrameToVideo
    }
    if { $currentTimeStep == [expr {[StudyHelper::GetNumberOfTimeStep]-1}] } {
      StartStopAnimation $::play_button
    }
    update
  }
}

proc WriteFrameToVideo { } {
  global WindowToImage
  global VideoWriter

  puts "WriteFrameToVideo"

  vtkWindowToImageFilter w2i
  w2i SetInput renWin
  w2i Update
  $VideoWriter SetInputConnection [w2i GetOutputPort]

  #$WindowToImage SetInput renWin
  #$WindowToImage Update
  #renWin Render

  $VideoWriter Update
  $VideoWriter Write
  w2i Delete
  #CaptureImage
}

proc OpenVideo { } {
  global VideoWriter
  global WindowToImage

  puts "OpenVideo"

  if { [ $VideoWriter GetClassName ] eq "vtkAVIWriter" } {
    set ext ".avi"
  } else {
    set ext ".mpeg"
  }
  $VideoWriter SetInputConnection [$WindowToImage GetOutputPort]
  $VideoWriter SetFileName "HedgeHog${ext}"
  $VideoWriter SetRate [StudyHelper::GetNeededFrameRate]
  $VideoWriter Start
  WriteFrameToVideo
}

proc CloseVideo { } {
  puts "CloseVideo"

  global VideoWriter

  $VideoWriter End
}

proc StartStopAnimation { btn } {
  global WriteToVideo
  global AnimateStop
  if { $AnimateStop } {
    $btn configure -text "Stop"
    set AnimateStop 0
    if { $WriteToVideo } {
      OpenVideo
    }
    AnimateTS
  } else {
    $btn configure -text "Play"
    set AnimateStop 1
    if { $WriteToVideo } {
      CloseVideo
    }
  }
}

proc CaptureImage { } {
  global WindowToImage
  global currentTimeStep

  vtkPNGWriter writer
  #vtkWindowToImageFilter w2i
  #w2i SetInput renWin
  #w2i Update
  $WindowToImage SetInput renWin
  $WindowToImage Update
  
  #writer SetInputConnection [w2i GetOutputPort]
  writer SetInputConnection [$WindowToImage GetOutputPort]
  writer SetFileName frame${currentTimeStep}.png

  renWin Render

  writer Write
  writer Delete
  #w2i Delete
}

StudyHelper::AddObserver ActivateTimeStep {
  maskPoint SetInput [ StudyHelper::GetVectorImage -masked no ]
}

StudyHelper::AddObserver ActivateTimeStep {
  renWin Render
} -last yes

set imgVector [ StudyHelper::GetVectorImage -masked no ]

$imgVector Update
vtkMaskPoints maskPoint
 maskPoint SetInput $imgVector
 maskPoint SetOnRatio 5
 maskPoint SetMaximumNumberOfPoints [ $imgVector GetNumberOfPoints ]
#$maskPoint RandomModeOn

vtkVectorNorm magnitude
  magnitude NormalizeOff
  magnitude SetInputConnection [ maskPoint GetOutputPort ]
  magnitude Update

# create pipeline for rendering
#
vtkHedgeHog hhog
  hhog SetInputConnection [ magnitude GetOutputPort ]
  #hhog SetInputConnection [ maskPoint GetOutputPort ]
  hhog SetScaleFactor 0.05


vtkLookupTable lut
  lut SetHueRange 0.667 0.0 
  lut SetRange 0 180
  lut SetVectorModeToMagnitude
  lut Build

vtkPolyDataMapper vectorMapper
  vectorMapper SetInputConnection [hhog GetOutputPort]
  vectorMapper UseLookupTableScalarRangeOn
  vectorMapper SetLookupTable lut

vtkActor vectorActor
  vectorActor SetMapper vectorMapper

vtkImageMarchingCubes marching
  marching SetInput [ StudyHelper::GetDistanceMapFromMask ]
  marching SetValue 0 0
vtkPolyDataMapper isoMapper
  isoMapper SetInputConnection [marching GetOutputPort]
  isoMapper ScalarVisibilityOff
  isoMapper ImmediateModeRenderingOn
vtkActor isoActor
  isoActor SetMapper isoMapper
  eval [isoActor GetProperty] SetDiffuseColor 0 0.7 0
  eval [isoActor GetProperty] SetOpacity 0.1

vtkOutlineFilter outline
  outline SetInput $imgVector
    
vtkPolyDataMapper outlineMapper
  outlineMapper SetInput [outline GetOutput]
vtkActor outlineActor
  outlineActor SetMapper outlineMapper

# Create the RenderWindow, Renderer and both Actors
#
vtkRenderer ren1
vtkRenderWindow renWin
    renWin AddRenderer ren1

set WindowToImage [ vtkWindowToImageFilter New ]
$WindowToImage SetInput renWin
if { [ llength [ info command vtkAVIWriter ] ] } {
  set VideoWriter [vtkAVIWriter New]
} else {
  set VideoWriter [vtkFFMPEGWriter New]
}
$VideoWriter SetInputConnection [$WindowToImage GetOutputPort]

#vtkRenderWindowInteractor iren
#    iren SetRenderWindow renWin

#vtkInteractorStyleTrackballCamera style
#[renWin GetInteractor] SetInteractorStyle style

# Add the actors to the renderer, set the background and size
#
ren1 AddActor isoActor
ren1 AddActor outlineActor
ren1 AddActor vectorActor
ren1 SetBackground 1 1 1
renWin SetSize 500 500
ren1 SetBackground 0.7 0.7 0.7
#iren Initialize

# render the image
#
#iren AddObserver UserEvent {wm deiconify .vtkInteract}

# prevent the tk window from showing up then start the event loop
wm withdraw .

toplevel .top
wm title .top "Test: Hedge Hog"
wm protocol .top WM_DELETE_WINDOW ::vtk::cb_exit

set display_frame [frame .top.f1]
set ctrl_buttons [frame .top.btns]

grid $display_frame -row 0 -column 0 -sticky snew
grid $ctrl_buttons  -row 1 -column 0 -sticky snew
grid rowconfigure .top 0 -weight 1
grid columnconfigure .top 0 -weight 1


set labTS [ label $ctrl_buttons.lbts -text "Time step" ]
grid $labTS -row 0 -column 0 -sticky se

set currentTimeStep [ StudyHelper::GetCurrentTimeStep ]
set scaleTS [ scale $ctrl_buttons.scts \
                  -from 0 -to [expr {[StudyHelper::GetNumberOfTimeStep]-1}] \
                  -variable currentTimeStep -showvalue 1 \
                  -orient horizontal \
                  -command GotoTimeStep ]
grid $scaleTS -row 0 -column 1 -sticky sew

set play_button [button $ctrl_buttons.btnPlay -text "Play" \
                     -command [list StartStopAnimation $ctrl_buttons.btnPlay]]
grid $play_button -row 0 -column 2 -sticky s

set quit_button [button $ctrl_buttons.btn1 -text "Quit" -command  ::vtk::cb_exit]
grid $quit_button -row 0 -column 3 -sticky s

grid columnconfigure $ctrl_buttons 1 -weight 1


set render_widget [vtkTkRenderWidget $display_frame.r \
                       -width 600 -height 600  -rw renWin]

grid $render_widget -sticky snew
grid rowconfigure $display_frame 0 -weight 1
grid columnconfigure $display_frame 0 -weight 1
::vtk::bind_tk_render_widget $render_widget

set iren [[$render_widget GetRenderWindow] GetInteractor]
#vtkRenderWindowInteractor iren
#    iren SetRenderWindow renWin

vtkInteractorStyleTrackballCamera style
$iren SetInteractorStyle style

[ren1 GetActiveCamera] Zoom 1.5
ren1 ResetCamera
renWin Render
