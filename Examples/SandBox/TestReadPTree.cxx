#include <boost/foreach.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <iostream>

using namespace boost::property_tree;

int main(int argc, const char* argv[])
{
  ptree pt;
  
  if (argc != 2)
    {
    std::cout << "usage: " << "'" << argv[0] << "' header.xml\n";
    return -1;
    } 
  read_xml(argv[1], pt);
  BOOST_FOREACH(ptree::value_type &v,
                pt.get_child("Study.ExtendedFields"))
    {
    std::cout << v.first << "\n";
    std::string key = "Study.ExtendedFields.";
    key += v.first + ".Description";
    std::string description = pt.get<std::string>(key);
    std::cout << description;
    }
  return 0;
}
