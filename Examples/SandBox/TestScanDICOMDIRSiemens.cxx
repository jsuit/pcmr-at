#include "pcmrGDCMIO.h"

#include "gdcmMediaStorage.h"
#include "itkGDCMImageIO.h"
#include "itkImageSeriesReader.h"
#include "itkImageFileWriter.h"
#include "itkTileImageFilter.h"
#include "itkRescaleIntensityImageFilter.h"
#include "itkFlipImageFilter.h"
#include "itkUnaryFunctorImageFilter.h"

#include "itkPhaseContrastImage.h"
#include "itkPhaseContrastTimeAveragedImageFilter.h"
#include "itkPhaseContrastTimeStdDevImageFilter.h"

#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>
#include <iostream>

using namespace boost::filesystem;

template< class TInput, class TOutput >
class NegateValue
{
public:
  NegateValue() {}
  ~NegateValue() {}

  bool operator!=(const NegateValue &) const
  {
    return false;
  }

  bool operator==(const NegateValue & other) const
  {
    return !( *this != other );
  }

  inline TOutput operator()(const TInput & A) const
  { return -A; }
};

template< class TInputImage, class TOutputImage >
class NegateImageFilter:
  public itk::UnaryFunctorImageFilter< TInputImage, TOutputImage,
                                       NegateValue<
                                         typename TInputImage::PixelType,
                                         typename TOutputImage::PixelType > >
{
public:
  /** Standard class typedefs. */
  typedef NegateImageFilter Self;
  typedef itk::UnaryFunctorImageFilter<
    TInputImage, TOutputImage,
    NegateValue< typename TInputImage::PixelType,
                 typename TOutputImage::PixelType > >  Superclass;

  typedef itk::SmartPointer< Self >       Pointer;
  typedef itk::SmartPointer< const Self > ConstPointer;

  /** Method for creation through the object factory. */
  itkNewMacro(Self);

  /** Runtime information support. */
  itkTypeMacro(NegateImageFilter,
               itk::UnaryFunctorImageFilter);

protected:
  NegateImageFilter() {}
  virtual ~NegateImageFilter() {}
private:
  NegateImageFilter(const Self &); //purposely not implemented
  void operator=(const Self &); //purposely not implemented
};

#define STRING_CMP( STR, VALUE )                 \
  STR.compare( 0, sizeof(VALUE)-1, VALUE )

pcmr::StatusType ReadInstanceNumber( const gdcm::DataSet &ds, unsigned int &InstanceNumber )
{
  //(0020,0013) IS [649]                      #   4, 1 InstanceNumber
  // http://www.dabsoft.ch/dicom/6/6/#(0020,0013)
  gdcm::Element<gdcm::VR::IS,gdcm::VM::VM1> el_InstanceNumber;
  if ( pcmr::TryTag<gdcm::VR::IS,gdcm::VM::VM1>( 0x0020, 0x0013, ds,
                                           el_InstanceNumber ) == pcmr::OK)
    {
      InstanceNumber = el_InstanceNumber.GetValue();
      return pcmr::OK;
    }
  return pcmr::TAG_NOTFOUND;
}

pcmr::StatusType ReadCardiacNumberOfImages( const gdcm::DataSet &ds, unsigned int &CardiacNumberOfImages )
{
  // (0018,1090) IS [27]                           #   2, 1 CardiacNumberOfImages
  // http://www.dabsoft.ch/dicom/6/6/#(0018,1090)
  gdcm::Element<gdcm::VR::IS,gdcm::VM::VM1> el_CardiacNumberOfImages;
  if ( pcmr::TryTag<gdcm::VR::IS,gdcm::VM::VM1>( 0x0018, 0x1090, ds,
                                           el_CardiacNumberOfImages ) == pcmr::OK)
    {
      CardiacNumberOfImages = el_CardiacNumberOfImages.GetValue();
      return pcmr::OK;
    }
  return pcmr::TAG_NOTFOUND;
}

pcmr::StatusType ReadRepetitionTime( const gdcm::DataSet &ds, float &RepetitionTime )
{
  // (0018,0080) DS [40.8]                           #   4, 1 RepetitionTime
  // http://www.dabsoft.ch/dicom/6/6/#(0018,0080)
  gdcm::Element<gdcm::VR::DS,gdcm::VM::VM1> el_RepetitionTime;
  if ( pcmr::TryTag<gdcm::VR::DS,gdcm::VM::VM1>( 0x0018, 0x0080, ds,
                                                 el_RepetitionTime ) == pcmr::OK)
    {
      RepetitionTime = el_RepetitionTime.GetValue();
      return pcmr::OK;
    }
  return pcmr::TAG_NOTFOUND;
}

pcmr::StatusType ReadTriggerTime( const gdcm::DataSet &ds, float &TriggerTime )
{
  // (0018,1060) DS [17.5]                           #   4, 1 TriggerTime
  // http://www.dabsoft.ch/dicom/6/6/#(0018,1060)
  gdcm::Element<gdcm::VR::DS,gdcm::VM::VM1> el_TriggerTime;
  if ( pcmr::TryTag<gdcm::VR::DS,gdcm::VM::VM1>( 0x0018, 0x1060, ds,
                                                 el_TriggerTime ) == pcmr::OK)
    {
      TriggerTime = el_TriggerTime.GetValue();
      return pcmr::OK;
    }
  return pcmr::TAG_NOTFOUND;
}


pcmr::ComplexImageComponentType GetSiemensPcmrType( const gdcm::DataSet &ds )
{
  pcmr::ComplexImageComponentType type;
  
  // (0008,0008) CS [ORIGINAL\PRIMARY\P\ND]                  #  22, 4 ImageType
  // http://www.dabsoft.ch/dicom/6/6/#(0008,0008)
  gdcm::Element<gdcm::VR::CS,gdcm::VM::VM4> el_ImageType;
  if ( pcmr::TryTag<gdcm::VR::CS,gdcm::VM::VM4>( 0x0008, 0x0008, ds,
                                           el_ImageType ) == pcmr::OK)
    {
      const char *itype[] = { "ORIGINAL", "PRIMARY", "P", "ND" };

      for ( int i = 0; i < el_ImageType.GetLength(); i++ )
        {
          const gdcm::VRToType<gdcm::VR::CS>::Type & _itype = el_ImageType.GetValue( i );
//#define PCMR_DUMP_IMAGE_TYPE
#if defined(PCMR_DUMP_IMAGE_TYPE)
          std::cout << (i?"\\'":"'")
                    <<  _itype 
                    << (i==(el_ImageType.GetLength()-1)?"'\n":"'");
#endif
          if ( i == 2 )
            {
            if ( !_itype.compare( 0, 1, "P" ) )
              {
              type = pcmr::PHASE;
              }
            else if ( !_itype.compare( 0, 1, "M" ) )
              {
              type = pcmr::MAGNITUDE;
              }
            else
              {
              return pcmr::CIC_UNKNOWN;
              }
            }
          else 
            {
            if ( _itype.compare( 0, strlen( itype[ i ] ), itype[ i ] ) )
              {
              return pcmr::CIC_UNKNOWN;
              }
            }
        }
      return type;
    }
  return pcmr::CIC_UNKNOWN;
}

struct SiemensPcmrInfo
{
  unsigned int m_NumberOfVolumes;
  float m_TimeStep;
  float m_VelocityEncoding;
  
  SiemensPcmrInfo()
  {
    m_NumberOfVolumes = 0.0;
    m_TimeStep = 0.0;
    // this value must be read from DICOM, now it is fixed
    m_VelocityEncoding = 150.0;
  }

  void Print()
  {
    std::cout << "m_NumberOfVolumes = " << this->m_NumberOfVolumes << "\n";
    std::cout << "m_TimeStep = " << this->m_TimeStep << "\n";
    std::cout << "m_VelocityEncoding = " << this->m_VelocityEncoding << "\n";
  }
};

struct SiemensPcmrSliceInfo
{
  path m_Path;
  unsigned int m_InstanceNumber;
  float m_TimeStep;

  bool operator<( const SiemensPcmrSliceInfo & a ) const
  {
    return this->m_InstanceNumber < a.m_InstanceNumber;
  }
};

pcmr::StatusType SiemensPcmr_ReadStudyInfo( const path::value_type *pathDCM,
                                            SiemensPcmrInfo & pcmrStudyInfo )
{
  pcmr::ComplexImageComponentType pcmrType = pcmr::CIC_UNKNOWN;
  
  gdcm::Reader reader;
  std::ifstream inputStream;
  inputStream.open( pathDCM, std::ios::binary );
  reader.SetStream( inputStream );
  //reader.SetFileName( pathDCM );
  if( !reader.Read() )
    {
    std::cerr << "Could not read: \"" << pathDCM << "\"" << std::endl;
    return pcmr::ST_NOREAD;
    }
  // The output of gdcm::Reader is a gdcm::File
  gdcm::File &file = reader.GetFile();
  // the dataset is the the set of element we are interested in:
  gdcm::DataSet &ds = file.GetDataSet();

  pcmrType = GetSiemensPcmrType( ds );
  if ( pcmrType == pcmr::CIC_UNKNOWN )
    {
    return pcmr::DCM_NO_PM;
    }

  ReadCardiacNumberOfImages( ds, pcmrStudyInfo.m_NumberOfVolumes );    
  ReadRepetitionTime( ds, pcmrStudyInfo.m_TimeStep );
  return pcmr::OK;
}

pcmr::ComplexImageComponentType TryPcmrDicom( const path::value_type * pathDCM, SiemensPcmrInfo & pcmrStudyInfo )
{
  pcmr::ComplexImageComponentType pcmrType = pcmr::CIC_UNKNOWN;

  gdcm::Reader reader;
  std::ifstream inputStream;
  inputStream.open( pathDCM, std::ios::binary );
  reader.SetStream( inputStream );
  //reader.SetFileName( pathDCM );
  if( !reader.Read() )
    {
    std::cerr << "Could not read: " << pathDCM << std::endl;
    return pcmr::CIC_UNKNOWN;
    }
  // The output of gdcm::Reader is a gdcm::File
  gdcm::File &file = reader.GetFile();
  // the dataset is the the set of element we are interested in:
  gdcm::DataSet &ds = file.GetDataSet();

  pcmrType = GetSiemensPcmrType( ds );
  if ( pcmrType != pcmr::CIC_UNKNOWN )
    {
    ReadCardiacNumberOfImages( ds, pcmrStudyInfo.m_NumberOfVolumes );    
    ReadRepetitionTime( ds, pcmrStudyInfo.m_TimeStep );
    pcmrStudyInfo.Print();
    }
  return pcmrType;
}

pcmr::StatusType SiemensPCMR_TryImage( const gdcm::DataSet &ds,
                                       const path::value_type *pathDCM,
                                       pcmr::ComplexImageComponentType &imageType,
                                       SiemensPcmrSliceInfo &sliceInfo )
{
  imageType = GetSiemensPcmrType( ds );
  pcmr::StatusType status;

  if ( imageType == pcmr::MAGNITUDE || imageType == pcmr::PHASE )
    {
    sliceInfo.m_Path = pathDCM;
    status = ReadInstanceNumber( ds, sliceInfo.m_InstanceNumber );
    if ( status != pcmr::OK )
      {
      std::cerr <<  "Error '" << pcmr::GetStatusDescription( status ) 
                << "' while reading InstanceNumber from '" << pathDCM << "'\n";
      return status;
      }
    status = ReadTriggerTime( ds, sliceInfo.m_TimeStep );
    if ( status != pcmr::OK )
      {
      std::cerr <<  "Error '" << pcmr::GetStatusDescription( status ) 
                << "' while reading TriggerTime from '" << pathDCM << "'\n";
      return status;
      }
    return pcmr::OK;
    }
  else
    {
    /*
    std::cerr <<  "Error '" << pcmr::GetStatusDescription( pcmr::DCM_NO_PM ) 
              << "' while reading '" << pathDCM << "'\n";
    */
    return pcmr::DCM_NO_PM;
    }
}

pcmr::StatusType InsertNewPcmrSlice( const gdcm::DataSet &ds,
                                     const path::value_type *pathDCM,
                                     std::vector<SiemensPcmrSliceInfo>& container )
{
  SiemensPcmrSliceInfo sliceInfo;
  pcmr::ComplexImageComponentType pcmrType = pcmr::CIC_UNKNOWN;
  pcmr::StatusType status;
  
  pcmrType = GetSiemensPcmrType( ds );
  if ( pcmrType == pcmr::MAGNITUDE || pcmrType == pcmr::PHASE )
    {
    sliceInfo.m_Path = pathDCM;
    status = ReadInstanceNumber( ds, sliceInfo.m_InstanceNumber );
    if ( status != pcmr::OK )
      {
      std::cerr <<  "Error '" << pcmr::GetStatusDescription( status ) 
                << "' while reading InstanceNumber from '" << pathDCM << "'\n";
      return status;
      }
    status = ReadTriggerTime( ds, sliceInfo.m_TimeStep );
    if ( status != pcmr::OK )
      {
      std::cerr <<  "Error '" << pcmr::GetStatusDescription( status ) 
                << "' while reading TriggerTime from '" << pathDCM << "'\n";
      return status;
      }
    container.push_back( sliceInfo );
    return pcmr::OK;
    }
  else
    {
    std::cerr <<  "Error '" << pcmr::GetStatusDescription( pcmr::DCM_NO_PM ) 
              << "' while reading '" << pathDCM << "'\n";
    return pcmr::DCM_NO_PM;
    }
}

pcmr::StatusType InsertNewPcmrSlice( const path::value_type *pathDCM,
                                     std::vector<SiemensPcmrSliceInfo>& container )
{
  gdcm::Reader reader;
  std::ifstream inputStream;
  inputStream.open( pathDCM, std::ios::binary );
  reader.SetStream( inputStream );
  //reader.SetFileName( pathDCM );
  if( !reader.Read() )
    {
    std::cerr << "Could not read: " << pathDCM << std::endl;
    return pcmr::ST_NOREAD;
    }
  // The output of gdcm::Reader is a gdcm::File
  gdcm::File &file = reader.GetFile();
  // the dataset is the the set of element we are interested in:
  gdcm::DataSet &ds = file.GetDataSet();

  return InsertNewPcmrSlice( ds, pathDCM, container );
}

void PrintSlices( std::vector<SiemensPcmrSliceInfo>& container, size_t from, size_t to )
{
  for ( unsigned int i = from; i <= to; i++ )
    {
    std::cout << "PATH: " << container[i].m_Path << "\n";
    std::cout << "InstanceNumber: " << container[i].m_InstanceNumber << "\n";
    std::cout << "TimeStep: " << container[i].m_TimeStep << "\n";    
    }
}

typedef itk::GDCMImageIO ImageIOType;
typedef itk::ImageSeriesReader< itk::PhaseContrast3DImage > ReaderType;
typedef itk::RescaleIntensityImageFilter <itk::PhaseContrast3DImage,itk::PhaseContrast3DImage> RescaleFilterType;

typedef itk::UnaryFunctorImageFilter< itk::PhaseContrast3DImage,
                                      itk::PhaseContrast3DImage,
                                      NegateValue<
                                        itk::PhaseContrast3DImage::PixelType,
                                        itk::PhaseContrast3DImage::PixelType > > NegateImage3DType;
typedef itk::TileImageFilter < itk::PhaseContrast3DImage,
                               itk::PhaseContrastImage> TileFilterType;
typedef itk::ImageFileWriter< itk::PhaseContrast3DImage > Writer3DType;
//typedef itk::ImageAdaptor<itk::PhaseContrast3DImage, NegatePixelAccessor> NegateImage3DType;

itk::PhaseContrast3DImage::Pointer
ReadSeries3D( ReaderType::FileNamesContainer filenames )
{
  ImageIOType::Pointer gdcmIO = ImageIOType::New();
  ReaderType::Pointer seriesReader = ReaderType::New();

  seriesReader->SetImageIO( gdcmIO );
  seriesReader->SetFileNames( filenames );
  try
    {
    seriesReader->Update();
    }
  catch (itk::ExceptionObject &excp)
    {
    std::cerr << "Exception thrown while reading the series" << std::endl;
    std::cerr << excp << std::endl;
    return NULL;
    }
  return seriesReader->GetOutput();
}

int WriteImage3D( itk::PhaseContrast3DImage::Pointer image,
                  const char* outputImage )
{
  std::cout << "Writing the image as a vtk image '"
            << outputImage << "' ...\n";
   
  Writer3DType::Pointer writer = Writer3DType::New();
  writer->SetInput( image );
  writer->SetFileName( outputImage );
  try
    {
    writer->Update();
    }
  catch (itk::ExceptionObject &excp)
    {
    std::cerr << "Exception thrown while writing the image" << std::endl;
    std::cerr << excp << std::endl;
    return -1;
    }
  return 0;
}

int SelectMagnitudeVolume( size_t idxTimeStep, size_t numberOfTimeStep,
                           const std::vector<SiemensPcmrSliceInfo> &seriesMagnitude,
                           ReaderType::FileNamesContainer &filenames )
{
  size_t sizeVolume = seriesMagnitude.size() / numberOfTimeStep;
 
  for ( size_t i = sizeVolume * idxTimeStep;
        i < sizeVolume * ( idxTimeStep + 1 ); i++ ) 
    {
    filenames.insert( filenames.begin(), seriesMagnitude[i].m_Path.string() );
    //filenames.push_back( seriesMagnitude[i].m_Path );
    }

  return 0;
}

// orthogonal to axial view, phase y comes first in order
int SelectPhaseYVolume( size_t idxTimeStep, size_t numberOfTimeStep,
                        const std::vector<SiemensPcmrSliceInfo> &seriesPhase,
                        ReaderType::FileNamesContainer &filenames )
{
  size_t sizeVolume = seriesPhase.size() / (3 * numberOfTimeStep);
  size_t baseY = 0;

  for ( size_t i = baseY + sizeVolume * idxTimeStep;
        i < baseY + sizeVolume * ( idxTimeStep + 1 ); i++ ) 
    {
    filenames.insert( filenames.begin(), seriesPhase[i].m_Path.string() );
    //filenames.push_back( seriesPhase[i].m_Path );
    }
  return 0;
}

// orthogonal to coronal view
int SelectPhaseXVolume( size_t idxTimeStep, size_t numberOfTimeStep,
                        const std::vector<SiemensPcmrSliceInfo> &seriesPhase,
                        ReaderType::FileNamesContainer &filenames )
{
  size_t sizeVolume = seriesPhase.size() / (3 * numberOfTimeStep);
  // skip slices for PhaseY which comes first
  size_t baseX = seriesPhase.size()/3;

  for ( size_t i = baseX + sizeVolume * idxTimeStep;
        i < baseX + sizeVolume * ( idxTimeStep + 1 ); i++ ) 
    {
    filenames.insert( filenames.begin(), seriesPhase[i].m_Path.string() );
    //filenames.push_back( seriesPhase[i].m_Path );
    }
  return 0;
}

// orthogonal to sagittal view
int SelectPhaseZVolume( size_t idxTimeStep, size_t numberOfTimeStep,
                        const std::vector<SiemensPcmrSliceInfo> &seriesPhase,
                        ReaderType::FileNamesContainer &filenames )
{
  size_t sizeVolume = seriesPhase.size() / (3 * numberOfTimeStep);
  size_t baseX = seriesPhase.size()/3;
  size_t baseZ = baseX*2;

  for ( size_t i = baseZ + sizeVolume * idxTimeStep;
        i < baseZ + sizeVolume * ( idxTimeStep + 1 ); i++ ) 
    {
    filenames.insert( filenames.begin(), seriesPhase[i].m_Path.string() );
    //filenames.push_back( seriesPhase[i].m_Path );
    }
  return 0;
}

int TestReadOneTimeStep( size_t idxTimeStep, size_t numberOfTimeStep,
                         const std::vector<SiemensPcmrSliceInfo> &seriesMagnitude,
                         const std::vector<SiemensPcmrSliceInfo> &seriesPhase,
                         float velocityEncoding )
{
  ReaderType::FileNamesContainer filenamesMag, 
    filenamesX, filenamesY, filenamesZ;
  size_t sizeVolume = seriesMagnitude.size() / numberOfTimeStep;

  std::cout << "Reading image data for time step " <<  idxTimeStep << " ...\n";

  filenamesMag.reserve( sizeVolume );
  SelectMagnitudeVolume( idxTimeStep, numberOfTimeStep,
                         seriesMagnitude, filenamesMag );

  filenamesX.reserve( sizeVolume );
  SelectPhaseXVolume( idxTimeStep, numberOfTimeStep,
                      seriesPhase, filenamesX );

  filenamesY.reserve( sizeVolume );
  SelectPhaseYVolume( idxTimeStep, numberOfTimeStep,
                      seriesPhase, filenamesY );

  filenamesZ.reserve( sizeVolume );
  SelectPhaseZVolume( idxTimeStep, numberOfTimeStep,
                      seriesPhase, filenamesZ );

  typedef itk::FlipImageFilter<itk::PhaseContrast3DImage> FlipFilterType;
  FlipFilterType::Pointer flipper = FlipFilterType::New();
  bool flipAxes[3] = { true, true, true };
  flipper->SetFlipAxes(flipAxes);
  bool flipAxisY = false;

  itk::PhaseContrast3DImage::Pointer imgMag = ReadSeries3D( filenamesMag );
  if ( flipAxisY )
    {
    flipper->SetInput( imgMag );
    WriteImage3D( flipper->GetOutput(), "siem_mag.vtk" );
    }
  else
    {
    WriteImage3D( imgMag, "siem_mag.vtk" );
    }

  RescaleFilterType::Pointer rescaler = RescaleFilterType::New();
  rescaler->SetOutputMinimum( -velocityEncoding );
  rescaler->SetOutputMaximum( +velocityEncoding );

  itk::PhaseContrast3DImage::Pointer imgPhaseX = ReadSeries3D( filenamesX );
  rescaler->SetInput( imgPhaseX );
  if ( flipAxisY )
    {
    flipper->SetInput( rescaler->GetOutput() );
    WriteImage3D( flipper->GetOutput(), "siem_X.vtk" );
    }
  else
    {
    WriteImage3D( rescaler->GetOutput(), "siem_X.vtk" );
    }

  itk::PhaseContrast3DImage::Pointer imgPhaseY = ReadSeries3D( filenamesY );
  rescaler->SetInput( imgPhaseY );
  if ( flipAxisY )
    {
    flipper->SetInput( rescaler->GetOutput() );
    WriteImage3D( flipper->GetOutput(), "siem_Y.vtk" );
    }
  else
    {
    WriteImage3D( rescaler->GetOutput(), "siem_Y.vtk" );
    }

  itk::PhaseContrast3DImage::Pointer imgPhaseZ = ReadSeries3D( filenamesZ );
  rescaler->SetInput( imgPhaseZ );
  if ( flipAxisY )
    {
    flipper->SetInput( rescaler->GetOutput() );
    WriteImage3D( flipper->GetOutput(), "siem_Z.vtk" );
    }
  else
    {
    WriteImage3D( rescaler->GetOutput(), "siem_Z.vtk" );
    }

  return 0;
}

int InitAorta4DStudy( const path::value_type *studyDir,
                      float minVelocity, float maxVelocity,
                      float velocityEncoding,
                      size_t numberOfTimeSteps,
                      float lengthTimeStep)
{
  create_directory( studyDir );
  path headerPath( studyDir );
  headerPath /= "header.sth";
  std::ofstream ofs( headerPath.c_str() );
  if ( ofs.good() )
    {
    ofs << "time_step_count " << numberOfTimeSteps << " "
        << "venc " << velocityEncoding << " "
        << "patient_orientation " << "HFS" << " "
        << "vector_mag_min " << minVelocity << " "
        << "vector_mag_max " << maxVelocity << " ";
    ofs << "length_time_step " << lengthTimeStep;
    ofs <<  std::endl;
    } 
  ofs.close();
  return 0;
}

void Write3DImageToStudy( itk::PhaseContrast3DImage::ConstPointer image,
                          const path::value_type *dirOutput, const char *patternName,
                          size_t t = 0 )
{
    path pathOutput( dirOutput );
    pathOutput /= patternName;
    std::string outputName( pathOutput.string().c_str() );
    std::stringstream ts; ts << t;
    boost::replace_all( outputName, "%t", ts.str() );
    Writer3DType::Pointer writer = Writer3DType::New();
    writer->SetInput( image );
    writer->SetFileName( outputName );
    writer->Update();
}

int TestExportStudy( const path::value_type * dirOutput,
                     size_t numberOfTimeStep,
                     float velocityEncoding,
                     const std::vector<SiemensPcmrSliceInfo> &seriesMagnitude,
                     const std::vector<SiemensPcmrSliceInfo> &seriesPhase )
{
  std::cout << "Starting TestExportStudy\n";

  std::vector<itk::PhaseContrast3DImage::Pointer> arrayMag;
  std::vector<itk::PhaseContrast3DImage::Pointer> arrayPhaseX;
  std::vector<itk::PhaseContrast3DImage::Pointer> arrayPhaseY;
  std::vector<itk::PhaseContrast3DImage::Pointer> arrayPhaseZ;

  TileFilterType::Pointer tilers[4];
  TileFilterType::LayoutArrayType layout;
  layout[0] = 1;
  layout[1] = 1;
  layout[2] = 1;
  layout[3] = 0;
  
  for ( size_t i = 0; i < 4; i++ )
    {
    tilers[i] = TileFilterType::New();
    tilers[i]->SetLayout( layout );
    }

  size_t sizeVolume = seriesMagnitude.size() / numberOfTimeStep;
  ReaderType::FileNamesContainer filenamesMag, 
    filenamesX, filenamesY, filenamesZ;

  filenamesMag.reserve( sizeVolume );
  filenamesX.reserve( sizeVolume );
  filenamesY.reserve( sizeVolume );
  filenamesZ.reserve( sizeVolume );

  for ( size_t ts = 0; ts < numberOfTimeStep; ts++ )
    {
    std::cout << " Extracting timestep number " << ts << std::endl;

    // read magnitude for this timestep
    filenamesMag.clear();
    SelectMagnitudeVolume( ts, numberOfTimeStep,
                           seriesMagnitude, filenamesMag );
    itk::PhaseContrast3DImage::Pointer imgMag = ReadSeries3D( filenamesMag );
    imgMag->Update();
    tilers[0]->SetInput( ts, imgMag );

    // read v_x component for this timestep
    filenamesX.clear();
    SelectPhaseXVolume( ts, numberOfTimeStep,
                        seriesPhase, filenamesX );
    itk::PhaseContrast3DImage::Pointer imgPhaseX = ReadSeries3D( filenamesX );
    RescaleFilterType::Pointer rescalerX = RescaleFilterType::New();
    rescalerX->SetOutputMinimum( -velocityEncoding );
    rescalerX->SetOutputMaximum( +velocityEncoding );
    rescalerX->SetInput( imgPhaseX );
    NegateImage3DType::Pointer negateX = NegateImage3DType::New();
    negateX->SetInput( rescalerX->GetOutput() );
    negateX->Update();
    tilers[1]->SetInput( ts, negateX->GetOutput() );

    // read v_y component for this timestep
    filenamesY.clear();
    SelectPhaseYVolume( ts, numberOfTimeStep,
                        seriesPhase, filenamesY );
    itk::PhaseContrast3DImage::Pointer imgPhaseY = ReadSeries3D( filenamesY );
    RescaleFilterType::Pointer rescalerY = RescaleFilterType::New();
    rescalerY->SetOutputMinimum( -velocityEncoding );
    rescalerY->SetOutputMaximum( +velocityEncoding );
    rescalerY->SetInput( imgPhaseY );
    NegateImage3DType::Pointer negateY = NegateImage3DType::New();
    negateY->SetInput( rescalerY->GetOutput() );
    negateY->Update();
    tilers[2]->SetInput( ts, negateY->GetOutput() );

    // read v_z component for this timestep
    filenamesZ.clear();
    SelectPhaseZVolume( ts, numberOfTimeStep,
                        seriesPhase, filenamesZ );
    itk::PhaseContrast3DImage::Pointer imgPhaseZ = ReadSeries3D( filenamesZ );
    RescaleFilterType::Pointer rescalerZ = RescaleFilterType::New();
    rescalerZ->SetOutputMinimum( -velocityEncoding );
    rescalerZ->SetOutputMaximum( +velocityEncoding );
    rescalerZ->SetInput( imgPhaseZ );
    NegateImage3DType::Pointer negateZ = NegateImage3DType::New();
    negateZ->SetInput( rescalerZ->GetOutput() );
    negateZ->Update();
    tilers[3]->SetInput( ts, negateZ->GetOutput() );
    }
  
  std::cout << "Computing time averaged PCMRA filter ...\n";
  itk::PhaseContrastTimeAveragedImageFilter::Pointer PCMRAFilter = itk::PhaseContrastTimeAveragedImageFilter::New();
  //itk::PhaseContrastTimeStdDevImageFilter::Pointer PCMRAFilter = itk::PhaseContrastTimeStdDevImageFilter::New();
  PCMRAFilter->SetNoiseMaskThreshold( 0.01 );
  PCMRAFilter->SetMagnitudeImage( tilers[0]->GetOutput() );
  PCMRAFilter->SetPhaseXImage   ( tilers[1]->GetOutput() );
  PCMRAFilter->SetPhaseYImage   ( tilers[2]->GetOutput() );
  PCMRAFilter->SetPhaseZImage   ( tilers[3]->GetOutput() );
  PCMRAFilter->Update();
  PCMRAFilter->Print( std::cout );
  /*
  std::cout << "X=" << tilers[1]->GetOutput()->GetPixel( PCMRAFilter->GetMaximumVelocityIndex() ) 
            << "\n";
  std::cout << "Y=" << tilers[2]->GetOutput()->GetPixel( PCMRAFilter->GetMaximumVelocityIndex() )
            << "\n";
  std::cout << "Z=" << tilers[3]->GetOutput()->GetPixel( PCMRAFilter->GetMaximumVelocityIndex() )
            << "\n";
  */
  if ( InitAorta4DStudy( dirOutput, 0, velocityEncoding, velocityEncoding,
                         numberOfTimeStep, seriesPhase[0].m_TimeStep ) == 0 )
    {
    Write3DImageToStudy( PCMRAFilter->GetOutput(), dirOutput, "premask.vtk" );
    for ( size_t ts = 0; ts < numberOfTimeStep; ts++ )
      {
       Write3DImageToStudy( tilers[0]->GetInput( ts ), dirOutput,
                            "mag_%t.vtk", ts );
       Write3DImageToStudy( tilers[1]->GetInput( ts ), dirOutput,
                            "vct_%t_0.vtk", ts );
       Write3DImageToStudy( tilers[2]->GetInput( ts ), dirOutput,
                            "vct_%t_1.vtk", ts );
       Write3DImageToStudy( tilers[3]->GetInput( ts ), dirOutput,
                            "vct_%t_2.vtk", ts );
      }
    }
  return 0;
}

bool SiemensPCMR_IsDICOMDIR( gdcm::File &file )
{
  gdcm::FileMetaInformation &fmi = file.GetHeader();

  gdcm::MediaStorage ms;
  ms.SetFromFile(file);
  return ms == gdcm::MediaStorage::MediaStorageDirectoryStorage;
}

int SiemensPCMR_ScanDICOMDIR( const path::value_type * dicomDir,
                               SiemensPcmrInfo &pcmrStudyInfoFromMagnitude,
                               SiemensPcmrInfo &pcmrStudyInfoFromPhase,
                               std::vector<SiemensPcmrSliceInfo> &seriesMagnitude,
                               std::vector<SiemensPcmrSliceInfo> &seriesPhase,
                               size_t &numberOfSeries,
                               size_t &numberOfImages )
{
  numberOfSeries = 0;
  numberOfImages = 0;
  // Instanciate the reader:
  gdcm::Reader reader;
  std::ifstream inputStream;
  inputStream.open( dicomDir, std::ios::binary );
  reader.SetStream( inputStream );
  //reader.SetFileName( dicomDir );
  if( !reader.Read() )
    {
    std::cerr << "Could not read: " << dicomDir << std::endl;
    return -1;
    }
  std::stringstream strStream;
  path pathDcmRoot( path(dicomDir).parent_path() );

  std::cout << "pathDcmRoot: '" << pathDcmRoot << "'\n";
  std::cout.flush();
  // The output of gdcm::Reader is a gdcm::File
  gdcm::File &file = reader.GetFile();

  // the dataset is the the set of element we are interested in:
  gdcm::DataSet &ds = file.GetDataSet();

  gdcm::FileMetaInformation &fmi = file.GetHeader();

  gdcm::MediaStorage ms;
  ms.SetFromFile(file);
  if( ms != gdcm::MediaStorage::MediaStorageDirectoryStorage )
    {
    std::cout << "This file is not a DICOMDIR" << std::endl;
    return 1;
    }

  if (fmi.FindDataElement( gdcm::Tag (0x0002, 0x0002)))
    {  
    strStream.str("");
    fmi.GetDataElement( gdcm::Tag (0x0002, 0x0002) ).GetValue().Print(strStream);
    std::cout << fmi.GetDataElement( gdcm::Tag (0x0002, 0x0002) ) << "\n";
    }
  else
    {
    std::cerr << " Media Storage Sop Class UID not present" << std::cout;
    }

  //TODO il faut trimer strm.str() avant la comparaison au cas ou...
  if ("1.2.840.10008.1.3.10"!=strStream.str())
    {
    std::cout << "This file is not a DICOMDIR" << std::endl;
    return 1;
    }

  // (0004,1220) SQ (Sequence with explicit length #=1887)   # 1512498, 1 DirectoryRecordSequence
  gdcm::Tag tsq_DirectoryRecord(0x0004,0x1220);
  if( !ds.FindDataElement( tsq_DirectoryRecord ) )
    {
    std::cout << "Fatal error: DirectoryRecordSequence not found\n";
    return -1;
    }
  const gdcm::DataElement &sq_DirectoryRecord = ds.GetDataElement( tsq_DirectoryRecord );
  const gdcm::SmartPointer<gdcm::SequenceOfItems> sqi = sq_DirectoryRecord.GetValueAsSQ();
  if ( !sqi )
    {
    std::cout << "Fatal error: DirectoryRecordSequence found but it's empty\n";
    return -1;
    }
  
  std::cout << "DirectoryRecordSequence has " << sqi->GetNumberOfItems() << " number of entries\n";

  typedef enum
  {
    RT_DONTCARE,
    RT_SERIES,
    RT_IMAGE
  } DcmDirRecordType;

  DcmDirRecordType previousItemType = RT_DONTCARE;
  pcmr::ComplexImageComponentType collectImage = pcmr::CIC_UNKNOWN;
  
  for ( size_t i = 0; i < sqi->GetNumberOfItems(); i++ )
    {
    const gdcm::Item & item = sqi->GetItem( i + 1 );
    strStream.str("");
    if (item.FindDataElement(gdcm::Tag (0x0004, 0x1430)))
      {
      item.GetDataElement(gdcm::Tag (0x0004, 0x1430)).GetValue().Print(strStream);
      }
    if ( !STRING_CMP( strStream.str(), "SERIES" ) )
      {
      ++numberOfSeries;
      previousItemType = RT_SERIES;
      collectImage = pcmr::CIC_UNKNOWN;
      }
    else if ( !STRING_CMP( strStream.str(), "IMAGE" ) )
      {
      ++numberOfImages;
      strStream.str( "" );
      if ( item.FindDataElement( gdcm::Tag(0x0004, 0x1500) ) )
        {
        item.GetDataElement(gdcm::Tag(0x0004, 0x1500)).GetValue().Print(strStream); 
        }
      else
        {
        std::cout << "Error: there is an image with a path 0x0004,0x1500 unspecified\n";
        continue;
        }
      
      path pathImage( pathDcmRoot );
      std::string pathRel( strStream.str() );
      std::replace( pathRel.begin(), pathRel.end(), '\\', '/' );
      pathImage /= pathRel;
      // process first image from this serie
      if ( previousItemType == RT_SERIES )
        {
        // check if it is a 4D flow or magnitude series
        SiemensPcmrInfo studyInfo;
        pcmr::ComplexImageComponentType iType = TryPcmrDicom( pathImage.c_str(), studyInfo );
        if ( iType == pcmr::MAGNITUDE )
          {
          collectImage = pcmr::MAGNITUDE;
          pcmrStudyInfoFromMagnitude = studyInfo;
          }
        else if ( iType == pcmr::PHASE )
          {
          collectImage = pcmr::PHASE;
          pcmrStudyInfoFromPhase = studyInfo;
          }
        }
      if ( collectImage == pcmr::MAGNITUDE )
        {
        // insert in the collection of magnitude images
        InsertNewPcmrSlice( pathImage.c_str(), seriesMagnitude );
        }
      else if ( collectImage == pcmr::PHASE )
        {
        // insert in the collection of flow images
        InsertNewPcmrSlice( pathImage.c_str(), seriesPhase );
        }
      previousItemType = RT_IMAGE;
      }
    else
      {
      // interrupt series
      collectImage = pcmr::CIC_UNKNOWN;
      previousItemType = RT_DONTCARE;
      }
    }
  std::cout << "Sorting series ...\n";
  std::sort( seriesMagnitude.begin(), seriesMagnitude.end() );
  std::sort( seriesPhase.begin(), seriesPhase.end() );
  std::cout << "Series sorted!\n";

  return 0;
}

// REVIEW: This function is a general function!!!
pcmr::StatusType SiemensPCMR_CheckManufacturer( gdcm::DataSet &ds, const char* ID )
{
  // (0008,0070) ?? (LO) [SIEMENS ]                          # 8,1 Manufacturer
  // http://www.dabsoft.ch/dicom/6/6/#(0008,0070)
  gdcm::Element<gdcm::VR::LO,gdcm::VM::VM1> el_Manufacturer;
  if ( pcmr::TryTag<gdcm::VR::LO,gdcm::VM::VM1>( 0x0008, 0x0070, ds,
                                                 el_Manufacturer ) == pcmr::OK)
    {
    const gdcm::VRToType<gdcm::VR::LO>::Type & manufacturer = el_Manufacturer.GetValue();
    if ( !manufacturer.compare( 0, strlen( ID ), ID ) )
      {
      return pcmr::OK;
      }
    else
      {
      return pcmr::VALUE_MISSMATCH;
      }
    }
  return pcmr::TAG_NOTFOUND;
}

bool SiemensPCMR_HasPixelData( gdcm::DataSet &ds )
{
  // (7fe0,0010)
  gdcm::Tag tag( 0x7fe0, 0x0010 );
  if( ds.FindDataElement( tag ) ) 
    {
    return true;
    }
  else
    {
    return false;
    }
}

int SiemensPCMR_TryDICOM( const path::value_type *dcm,
                          std::vector<SiemensPcmrSliceInfo> &seriesMagnitude,
                          std::vector<SiemensPcmrSliceInfo> &seriesPhase,
                          size_t &numberOfSeries,
                          size_t &numberOfImages )
{
  pcmr::StatusType status;
  gdcm::Reader reader;
  std::ifstream inputStream;
  inputStream.open( dcm, std::ios::binary );
  reader.SetStream( inputStream );
  //reader.SetFileName( dcm );
  if( !reader.Read() )
    {
    std::cout << "Could not read: \"" << dcm << "\"" << std::endl;
    return 0;
    }
  // The output of gdcm::Reader is a gdcm::File
  gdcm::File &file = reader.GetFile();

  if ( SiemensPCMR_IsDICOMDIR( file ) )
    {
    std::cout << "Found DICOMDIR: \"" << dcm << "\"" << std::endl; 
    return 0;
    }

  // the dataset is the the set of element we are interested in:
  gdcm::DataSet &ds = file.GetDataSet();
  
  status = SiemensPCMR_CheckManufacturer( ds, "SIEMENS" );
  if ( status != pcmr::OK )
    {
    std::cerr << "Not a SIEMENS DICOM: "
              << pcmr::GetStatusDescription(status) << ", while reading \"" << dcm << "\"" << std::endl;
    return -1;
    }
  
  if ( !SiemensPCMR_HasPixelData( ds ) )
    {
    return 0;
    }
    ++numberOfImages;
    // check if it is a 4D flow or magnitude series
    pcmr::ComplexImageComponentType imageType;
    SiemensPcmrSliceInfo sliceInfo;
    status = SiemensPCMR_TryImage( ds, dcm, imageType, sliceInfo );
    if ( imageType == pcmr::MAGNITUDE )
      {
      seriesMagnitude.push_back( sliceInfo );
      }
    else if ( imageType == pcmr::PHASE )
      {
      seriesPhase.push_back( sliceInfo );
      }
    else 
      {
      return 0;
      }  
  return 0;
}

int SiemensPCMR_ScanDirectory0( const path::value_type * dir,
                                std::vector<SiemensPcmrSliceInfo> &seriesMagnitude,
                                std::vector<SiemensPcmrSliceInfo> &seriesPhase,
                                size_t &numberOfSeries,
                                size_t &numberOfImages )
{
  path pathDir( dir );
  std::cout << "Scanning directory \"" << dir << "\"" << std::endl;
  for ( directory_iterator it = directory_iterator( pathDir );
        it != directory_iterator(); it++ )
    {
    int status;
    if ( is_directory( it->status() ) )
      {
      status = SiemensPCMR_ScanDirectory0( it->path().c_str(),
                                           seriesMagnitude,
                                           seriesPhase,
                                           numberOfSeries,
                                           numberOfImages );                                              
      }
    else
      {
      status = SiemensPCMR_TryDICOM( it->path().c_str(),
                                     seriesMagnitude,
                                     seriesPhase,
                                     numberOfSeries,
                                     numberOfImages );
      }
      if ( status != 0 )
        {
        return status;
        }
    }
  
  return 0;
}

int SiemensPCMR_ScanDirectory( const path::value_type *dir,
                               SiemensPcmrInfo &pcmrStudyInfoFromMagnitude,
                               SiemensPcmrInfo &pcmrStudyInfoFromPhase,
                               std::vector<SiemensPcmrSliceInfo> &seriesMagnitude,
                               std::vector<SiemensPcmrSliceInfo> &seriesPhase,
                               size_t &numberOfSeries,
                               size_t &numberOfImages )
{
  int status = SiemensPCMR_ScanDirectory0( dir, seriesMagnitude, seriesPhase,
                                           numberOfSeries, numberOfImages );
  if ( status != 0 )  
    {
    return status;
    }
  if ( !seriesMagnitude.size() || !seriesPhase.size() )
    {
    std::cerr << "No magnitude or phase found during directory scan at: \"" 
              << dir << "\"" << std::endl;
    return -1;
    }
  SiemensPcmr_ReadStudyInfo( seriesMagnitude[0].m_Path.c_str(), pcmrStudyInfoFromMagnitude );
  std::cout << "pcmrStudyInfoFromMagnitude:\n";
  pcmrStudyInfoFromMagnitude.Print();
  SiemensPcmr_ReadStudyInfo( seriesPhase[1].m_Path.c_str(), pcmrStudyInfoFromPhase );
  std::cout << "pcmrStudyInfoFromPhase:\n";
  pcmrStudyInfoFromPhase.Print();

  std::cout << "Sorting series ...\n";
  std::sort( seriesMagnitude.begin(), seriesMagnitude.end() );
  std::sort( seriesPhase.begin(), seriesPhase.end() );
  std::cout << "Series sorted!\n";
  return 0;
}

int main( int argc, path::value_type *argv[] )
{
  if( argc < 2 )
    {
    std::cerr << argv[0] << " InputDir ?DirectoryStudy?" << std::endl;
    return 1;
    }
  const path::value_type *inputDir = argv[1];
  
  // check if the directory exists
  path pathInputDir( inputDir );
  if ( !exists( pathInputDir ) )
    {
    std::cerr << "Input path \"" << inputDir << "\" does not exists" << std::endl;
    return 1;
    }
  if ( !is_directory( pathInputDir ) ) 
    {
    std::cerr << "Input path \"" << inputDir << "\" is not a directory" << std::endl;
    return 1;
    }

  // check if the directory contains a DICOMDIR
  path pathDICOMDIR( pathInputDir );
  pathDICOMDIR /= "DICOMDIR";

  size_t numberOfSeries = 0;
  size_t numberOfImages = 0;

  SiemensPcmrInfo pcmrStudyInfoFromMagnitude;
  SiemensPcmrInfo pcmrStudyInfoFromPhase;
  std::vector<SiemensPcmrSliceInfo> seriesMagnitude;
  std::vector<SiemensPcmrSliceInfo> seriesPhase;

  if ( exists( pathDICOMDIR ) )
    {
    if ( SiemensPCMR_ScanDICOMDIR( pathDICOMDIR.c_str(),
                                   pcmrStudyInfoFromMagnitude,
                                   pcmrStudyInfoFromPhase,
                                   seriesMagnitude, seriesPhase,
                                   numberOfSeries, numberOfImages ) != 0 )
      {
      return -1;
      }
    }
  else
    {
    if ( SiemensPCMR_ScanDirectory( inputDir,
                                    pcmrStudyInfoFromMagnitude,
                                    pcmrStudyInfoFromPhase,
                                    seriesMagnitude, seriesPhase,
                                    numberOfSeries, numberOfImages ) != 0 )
      {
      std::cout << "Failed scanning directory of DICOM's from \"" << inputDir <<"\"" << std::endl;
      return -1;
      }
    }

  std::cout << "Info: there are " << numberOfSeries << " series\n";
  std::cout << "Info: there are " << numberOfImages << " images\n";
  std::cout << "Info: there are " << seriesMagnitude.size() << " images M\n";
  std::cout << "Info: there are " << seriesPhase.size() << " images P\n";

  // let's try to load a volume and write it back to disk in vtk
  // format
  
  /*
  TestReadOneTimeStep( 10, pcmrStudyInfoFromMagnitude.m_NumberOfVolumes,
                       seriesMagnitude, seriesPhase,
                       pcmrStudyInfoFromPhase.m_VelocityEncoding );
  */
  PrintSlices( seriesMagnitude, seriesMagnitude.size()-10, seriesMagnitude.size()-1 );
  //PrintSlices( seriesPhase, seriesPhase.size()-10, seriesPhase.size()-1 );
  if ( argc > 2 ) 
    {
    TestExportStudy( argv[2], pcmrStudyInfoFromMagnitude.m_NumberOfVolumes,
                     pcmrStudyInfoFromPhase.m_VelocityEncoding,
                     seriesMagnitude, seriesPhase );
    }

  return 0;
}
