/*=========================================================================

  Program: GDCM (Grassroots DICOM). A DICOM library

  Copyright (c) 2006-2011 Mathieu Malaterre
  All rights reserved.
  See Copyright.txt or http://gdcm.sourceforge.net/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
/*
 * This example is ... guess what this is for :)
 */

#include "gdcmReader.h"
#include "gdcmWriter.h"
#include "gdcmAttribute.h"
#include "gdcmPrivateTag.h"

#include <iostream>

namespace philips4d
{

struct indenter {
  unsigned int n;

  indenter( unsigned int _n = 0 )
    : n(_n) { }
  
  friend std::ostream &operator <<( std::ostream &_os, const indenter &ind )
  {
    for( int i = 0; i < ind.n; i++ )
      {
        _os << " ";
      }
    return _os;
  }
};

typedef enum
  {
    VC_UNKNOWN = 0,
    X,
    Y,
    Z,
    VelocityComponentTypeEnd
  } VelocityComponentType;
  
typedef enum
  {
    CIC_UNKNOWN = 0,
    MIXED,
    MAGNITUDE,
    PHASE,
    ComplexImageComponentTypeEnd
  } ComplexImageComponentType;

typedef enum
  {
    ST_UNDEFINED = 0,
    OK,
    NO_PHILIPS,
    NO_PHASE4D,
    TAG_NOTFOUND,
    IMG_MISSMATCH,
    NO_ENHANCEDMRI,
    NO_MODMR,
    NO_FLOWENCODED,
    NO_ACQ3D,
    NO_PHASECONTRAST,
    SQ_EMPTY,
    SQ_LENGTH_MISSMATCH,
    NO_MEMORY,
    UNK_COMPLEX_IC,
    INV_PCV_LENGTH,
    NULL_PCV,
    StatusTypeEnd
  } StatusType;

static const char*GetStatusDescription( int i )
{
  static const char *_desc[] =
    {
      "undefined status",
      "ok",
      "manufacturer is no philips",
      "dicom is not 4D",
      "tag not found",
      "image type missmatch",
      "not an enhanced MR image storage",
      "modality is not MR",
      "AcquisitionContrast is not FLOW_ENCODED",
      "MRAcquisitionType is not 3D",
      "PhaseContrast not equal to YES",
      "empty sequence of items",
      "length of sequence does not match expected size",
      "not enough memory",
      "unknown ComplexImageComponent",
      "invalid PCVelocity length",
      "PCVelocity is zero"
    };
  if ( i < ST_UNDEFINED || i >= StatusTypeEnd )
    {
      i = 0;
    }
  return _desc[ i ];
}
  
static const char*GetVelocityComponentName( int i )
{
  static const char *_name[] = { "UNKNOWN", "X", "Y", "Z" };
  if ( i < VC_UNKNOWN || i >= VelocityComponentTypeEnd )
    {
      i = 0;
    }
  return _name[ i ];
}
  
static const char*GetComplexImageComponentName( int i )
{
  static const char *_name[] = { "UNKOWN", "MIXED", "MAGNITUDE", "PHASE" };
  if ( i < CIC_UNKNOWN || i >= ComplexImageComponentTypeEnd )
    {
      i = 0;
    }
  return _name[ i ];
}

static ComplexImageComponentType GetComplexImageComponentId( const char *name )
{
  int i;
  for ( i = 0; i < ComplexImageComponentTypeEnd; i++ )
    {
      const char* _name = GetComplexImageComponentName( i );
      if ( !strncmp( _name, name, strlen(_name) ) )
        {
          return static_cast<ComplexImageComponentType>(i);
        }
    }
  return CIC_UNKNOWN;
}

struct FrameType
{
  unsigned int m_PhaseNumber;
  unsigned int m_SliceNumberMR;
  ComplexImageComponentType m_ComplexImageComponent;

  FrameType()
    : m_PhaseNumber(0),
      m_SliceNumberMR(0),
      m_ComplexImageComponent( CIC_UNKNOWN ) {}
  void Print( std::ostream &_os, unsigned int indent = 0 ) const
  {
    indenter ind( indent );
    _os << ind << "PhaseNumber = " << m_PhaseNumber << "\n"
        << ind << "SliceNumberMR = " << m_SliceNumberMR << "\n"
        << ind << "ComplexImageComponent = " << GetComplexImageComponentName( m_ComplexImageComponent ) << "\n";
  }
};
  
struct PPIType
{
};

struct POIType
{
};

struct RescaleType
{
  float m_Scale;
  float m_Intercept;
};

struct VolumeInfo
{
  float m_Origin[3];
  float m_Spacing[3];
  unsigned int m_Dimension[3];

  VolumeInfo()
  {
    for( int i = 0; i < 3; i++ )
      {
        m_Origin[i] = 0.0;
        m_Spacing[i] = 1.0;
        m_Dimension[i] = 1;
      }
  }
  
  void Print( std::ostream &_os, unsigned int indent = 0 ) const
  {
    indenter ind( indent );
    
    std::cout << ind << "Dimension: "
              << "(" << m_Dimension[0] << "," << m_Dimension[1] << "," << m_Dimension[2] << ")\n";
    std::cout << ind << "Origin:    "
              << "(" << m_Origin[0] << "," << m_Origin[1] << "," << m_Origin[2] << ")\n";
    std::cout << ind << "Spacing:   "
              << "(" << m_Spacing[0] << "," << m_Spacing[1] << "," << m_Spacing[2] << ")";
    
  }
};

class EnhancedMRI
{
public:

  EnhancedMRI();

  ~EnhancedMRI()
  {
    if ( this->m_FramesInfo )
      {
        delete []this->m_FramesInfo;
        this->m_FramesInfo = NULL;
      }
  }

  StatusType ReadDataSet( const gdcm::DataSet &ds );

  StatusType GetStatus() const
  {
    return this->m_Status;
  }

  ComplexImageComponentType GetComplexImageComponent() const
  {
    return this->m_ComplexImageComponent;
  }
  
  unsigned int GetNumberOfFrames() const
  {
    return this->m_NumberOfFrames;
  }
  
  unsigned int GetNumberOfPhases() const
  {
    return this->m_NumberOfPhases;
  }
  
  VelocityComponentType GetVelocityComponent() const
  {
    return this->m_VelocityComponent;
  }

  const VolumeInfo & GetVolumeInfo() const
  {
    return this->m_VolumeInfo;
  }

  float GetPCVelocity() const
  {
    return this->m_PCVelocity;
  }

  const FrameType *GetFrameInfo( int i ) const
  {
    if ( i >= 0 && i < this->GetNumberOfFrames() )
      {
        return this->m_FramesInfo + i;
      }
    return NULL;
  }
  
  void 	Print(std::ostream &_os) const;

protected:

  StatusType ReadGlobalParameters( const gdcm::DataSet &ds );
  
  template< int TVR, int TVM >
  static StatusType TryTag( uint16_t group, uint16_t element,
                     const gdcm::DataSet &ds,
                     gdcm::Element<TVR,TVM> &el )
  {
    gdcm::Tag tag( group, element );
    if( !ds.FindDataElement( tag ) ) {
      return TAG_NOTFOUND;
    }
    const gdcm::DataElement& data = ds.GetDataElement( tag );
    el.SetFromDataElement( data );
    return OK;
  }
  
  static
  StatusType CheckManufacturer( const gdcm::DataSet &ds )
  {
    // (0008,0070) LO [Philips Medical Systems]          #  24, 1 Manufacturer
    // http://www.dabsoft.ch/dicom/6/6/#(0008,0070)
    gdcm::Element<gdcm::VR::LO,gdcm::VM::VM1> el_Manufacturer;
    if ( TryTag<gdcm::VR::LO,gdcm::VM::VM1>( 0x0008, 0x0070, ds,
                                             el_Manufacturer ) == OK)
      {
        const gdcm::VRToType<gdcm::VR::LO>::Type & manufacturer = el_Manufacturer.GetValue();
        const std::string philipsID( "Philips Medical Systems" );
        if ( !manufacturer.compare( 0, philipsID.length(), philipsID ) )
          {
            return OK;
          }
        else
          {
            return NO_PHILIPS;
          }
      }
    return TAG_NOTFOUND;
  }

  static
  StatusType CheckEnhancedMRImageType( const gdcm::DataSet &ds )
  {
    // (0008,0008) CS [DERIVED\PRIMARY\FLOW_ENCODED\MIXED]     #  34, 4 ImageType
    // http://www.dabsoft.ch/dicom/6/6/#(0008,0008)
    gdcm::Element<gdcm::VR::CS,gdcm::VM::VM4> el_ImageType;
    if ( TryTag<gdcm::VR::CS,gdcm::VM::VM4>( 0x0008, 0x0008, ds,
                                             el_ImageType ) == OK)
      {
        const char *itype[] = { "DERIVED", "PRIMARY", "FLOW_ENCODED", "MIXED" };
        for ( int i = 0; i < el_ImageType.GetLength(); i++ )
          {
            const gdcm::VRToType<gdcm::VR::CS>::Type & _itype = el_ImageType.GetValue( i );
            if ( _itype.compare( itype[ i ] ) )
              {
                return IMG_MISSMATCH;
              }
          }
        return OK;
      }
    return TAG_NOTFOUND;
  }

  static
  StatusType ReadComplexImageComponent( const gdcm::DataSet &ds, std::string &comp )
  {
    //(0008,9208) CS [MIXED]                        #   6, 1 ComplexImageComponent
    // http://www.dabsoft.ch/dicom/6/6/#(0008,9208)
    gdcm::Element<gdcm::VR::CS,gdcm::VM::VM1> el_ComplexImageComponent;
    if ( TryTag<gdcm::VR::CS,gdcm::VM::VM1>( 0x0008, 0x9208, ds,
                                             el_ComplexImageComponent ) == OK )
      {
        comp = el_ComplexImageComponent.GetValue();
        return OK;
      }
    return TAG_NOTFOUND;
  }

  static
  StatusType CheckModalityMR( const gdcm::DataSet &ds )
  {
    // (0008,0060) CS [MR]                 #   2, 1 Modality
    // http://www.dabsoft.ch/dicom/6/6/#(0008,0060)
    gdcm::Element<gdcm::VR::CS,gdcm::VM::VM1> el_Modality;
    if ( TryTag<gdcm::VR::CS,gdcm::VM::VM1>( 0x0008, 0x0060, ds,
                                             el_Modality ) == OK)
      {
        const gdcm::VRToType<gdcm::VR::CS>::Type & modality = el_Modality.GetValue();
        
        if ( modality.compare( "MR" ) )
          {
            return NO_MODMR;
          }
        return OK;
      }
    return TAG_NOTFOUND;
  }

  static
  StatusType CheckFlowEncodeAcquisition( const gdcm::DataSet &ds )
  {
    //(0008,9209) CS [FLOW_ENCODED]               #  12, 1 AcquisitionContrast
    // http://www.dabsoft.ch/dicom/6/6/#(0008,9209)
    gdcm::Element<gdcm::VR::CS,gdcm::VM::VM1> el_AcquisitionContrast;
    if ( TryTag<gdcm::VR::CS,gdcm::VM::VM1>( 0x0008, 0x9209, ds,
                                             el_AcquisitionContrast ) == OK)
      {
        const gdcm::VRToType<gdcm::VR::CS>::Type & acquisition = el_AcquisitionContrast.GetValue();
        
        if ( acquisition.compare( "FLOW_ENCODED" ) )
          {
            return NO_FLOWENCODED;
          }
        return OK;
      }
    return TAG_NOTFOUND;
  }

  static
  StatusType CheckMRAcquisitionType3D( const gdcm::DataSet &ds )
  {
    //(0018,0023) CS [3D]                         #   2, 1 MRAcquisitionType
    // http://www.dabsoft.ch/dicom/6/6/#(0018,0023)
    gdcm::Element<gdcm::VR::CS,gdcm::VM::VM1> el_MRAcquisitionType;
    if ( TryTag<gdcm::VR::CS,gdcm::VM::VM1>( 0x0018, 0x0023, ds,
                                             el_MRAcquisitionType ) == OK)
      {
        const gdcm::VRToType<gdcm::VR::CS>::Type & acquisition = el_MRAcquisitionType.GetValue();
        
        if ( acquisition.compare( "3D" ) )
          {
            return NO_ACQ3D;
          }
        return OK;
      }
    return TAG_NOTFOUND;
  }

  static
  StatusType CheckPhaseContrast( const gdcm::DataSet &ds )
  {
    //(0018,9014) CS [YES]                        #   4, 1 PhaseContrast
    // http://www.dabsoft.ch/dicom/6/6/#(0018,9014)
    gdcm::Element<gdcm::VR::CS,gdcm::VM::VM1> el_PhaseContrast;
    if ( TryTag<gdcm::VR::CS,gdcm::VM::VM1>( 0x0018, 0x9014, ds,
                                             el_PhaseContrast ) == OK)
      {
        const gdcm::VRToType<gdcm::VR::CS>::Type & phase_contrast = el_PhaseContrast.GetValue();
        const char expected[] = "YES";
        if ( phase_contrast.compare( 0, strlen( expected ), expected ) )
          {
            return NO_PHASECONTRAST;
          }
        return OK;
      }
    return TAG_NOTFOUND;
  }

  static
  StatusType CheckEnhancedMRSOPClassUID( const gdcm::DataSet &ds )
  {
    // (0008,0016) UI =EnhancedMRImageStorage              #  28, 1 SOPClassUID
    // http://www.dabsoft.ch/dicom/4/B.5/
    // http://www.dabsoft.ch/dicom/6/6/#(0008,0016)
    gdcm::Element<gdcm::VR::UI,gdcm::VM::VM1> el_MediaStorage;
    if ( TryTag<gdcm::VR::UI,gdcm::VM::VM1>( 0x0008, 0x0016, ds,
                                             el_MediaStorage ) == OK)
      {
        const gdcm::VRToType<gdcm::VR::LO>::Type & SOPClassUID = el_MediaStorage.GetValue();
        const std::string EnhancedMRI_ID( "1.2.840.10008.5.1.4.1.1.4.1" );
        if ( !SOPClassUID.compare( 0, EnhancedMRI_ID.length(), EnhancedMRI_ID ) )
          {
            return OK;
          }
        else
          {
            return NO_ENHANCEDMRI;
          }
      }
    return TAG_NOTFOUND;
  }
  
  static
  StatusType ReadNumberOfFrames( const gdcm::DataSet &ds, unsigned int &nframes )
  {
    //(0028,0008) IS [2050]                       #   4, 1 NumberOfFrames
    // http://www.dabsoft.ch/dicom/6/6/#(0028,0008)
    gdcm::Element<gdcm::VR::IS,gdcm::VM::VM1> el_NumberOfFrames;
    if ( TryTag<gdcm::VR::IS,gdcm::VM::VM1>( 0x0028, 0x0008, ds,
                                             el_NumberOfFrames ) == OK)
      {
        nframes = el_NumberOfFrames.GetValue();
        return OK;
      }
    return TAG_NOTFOUND;
  }
  
  static
  StatusType ReadNumberOfRows( const gdcm::DataSet &ds, unsigned int &nrows )
  {
    //(0028,0010) US 256                          #   2, 1 Rows
    // http://www.dabsoft.ch/dicom/6/6/#(0028,0010)
    gdcm::Element<gdcm::VR::US,gdcm::VM::VM1> el_Rows;
    if ( TryTag<gdcm::VR::US,gdcm::VM::VM1>( 0x0028, 0x0010, ds,
                                             el_Rows ) == OK)
      {
        nrows = el_Rows.GetValue();
        return OK;
      }
    return TAG_NOTFOUND;
  }
  
  static
  StatusType ReadNumberOfColumns( const gdcm::DataSet &ds, unsigned int &ncolumns )
  {
    //(0028,0011) US 256                          #   2, 1 Columns
    // http://www.dabsoft.ch/dicom/6/6/#(0028,0011)
    gdcm::Element<gdcm::VR::US,gdcm::VM::VM1> el_Columns;
    if ( TryTag<gdcm::VR::US,gdcm::VM::VM1>( 0x0028, 0x0011, ds,
                                             el_Columns ) == OK)
      {
        ncolumns = el_Columns.GetValue();
        return OK;
      }
    return TAG_NOTFOUND;
  }

  static
  StatusType ReadNumberOfPhasesMR( const gdcm::DataSet &ds, unsigned int &nphases )
  {
    //(2001,1017) SL 25                           #   4, 1 NumberOfPhasesMR
    // Philips private
    gdcm::Element<gdcm::VR::SL,gdcm::VM::VM1> el_NumberOfPhasesMR;
    if ( TryTag<gdcm::VR::SL,gdcm::VM::VM1>( 0x2001, 0x1017, ds,
                                             el_NumberOfPhasesMR ) == OK)
      {
        nphases = el_NumberOfPhasesMR.GetValue();
        return OK;
      }
    return TAG_NOTFOUND;
  }

  static
  StatusType ReadPhaseNumber( const gdcm::DataSet &ds, unsigned int &fn )
  {
    // (2001,1008) IS [1]                         #   2, 1 PhaseNumber
    // Philips private
    gdcm::Element<gdcm::VR::IS,gdcm::VM::VM1> el_PhaseNumber;
    if ( TryTag<gdcm::VR::IS,gdcm::VM::VM1>( 0x2001, 0x1008, ds,
                                             el_PhaseNumber ) == OK)
      {
        fn = el_PhaseNumber.GetValue();
        return OK;
      }
    return TAG_NOTFOUND;
  }

  static
  StatusType ReadNumberOfSlicesMR( const gdcm::DataSet &ds, unsigned int &nslices )
  {
    //(2001,1018) SL 41                           #   4, 1 NumberOfSlicesMR
    // Philips private
    gdcm::Element<gdcm::VR::SL,gdcm::VM::VM1> el_NumberOfSlicesMR;
    if ( TryTag<gdcm::VR::SL,gdcm::VM::VM1>( 0x2001, 0x1018, ds,
                                             el_NumberOfSlicesMR ) == OK)
      {
        nslices = el_NumberOfSlicesMR.GetValue();
        return OK;
      }
    return TAG_NOTFOUND;
  }

  static
  StatusType ReadSliceNumberMR( const gdcm::DataSet &ds, unsigned int &sn )
  {
    // (2001,100a) IS [1]             #   2, 1 SliceNumberMR
    // Philips private
    gdcm::Element<gdcm::VR::IS,gdcm::VM::VM1> el_SliceNumberMR;
    if ( TryTag<gdcm::VR::IS,gdcm::VM::VM1>( 0x2001, 0x100a, ds,
                                             el_SliceNumberMR ) == OK)
      {
        sn = el_SliceNumberMR.GetValue();
        return OK;
      }
    return TAG_NOTFOUND;
  }

  static
  StatusType ReadPCVelocity( const gdcm::DataSet &ds, float pcv[] )
  {
    //(2001,101a) FL 0\200\0                                  #  12, 3 PCVelocity
    // Philips private
    gdcm::Element<gdcm::VR::FL,gdcm::VM::VM1_n> el_PCVelocity;
    if ( TryTag<gdcm::VR::FL,gdcm::VM::VM1_n>( 0x2001, 0x101a, ds,
                                               el_PCVelocity ) == OK)
      {
        if ( el_PCVelocity.GetLength() != 3 )
          {
            return INV_PCV_LENGTH;
          }
        for ( int i = 0; i < 3; i++ )
          {
            pcv[i] = el_PCVelocity.GetValue( i );
          }
        return OK;
      }
    return TAG_NOTFOUND;
  }

  static
  StatusType ReadVolumeInfo( const gdcm::DataSet &ds, VolumeInfo &vol )
  {
    StatusType status = ST_UNDEFINED;

    unsigned int n;

    if ( (status=EnhancedMRI::ReadNumberOfRows( ds, n)) != OK )
      {
        return status;
      }
    vol.m_Dimension[0] = n;
    
    if ( (status=EnhancedMRI::ReadNumberOfColumns( ds, n)) != OK )
      {
        return status;
      }
    vol.m_Dimension[1] = n;

    if ( (status=EnhancedMRI::ReadNumberOfSlicesMR( ds, n)) != OK )
      {
        return status;
      }
    vol.m_Dimension[2] = n;

    return OK;
  }
  
  static StatusType ReadFramesInfo( const gdcm::DataSet &ds,
                                    FrameType *& finfo, unsigned int size );

  
private:
  // we should deal with the following AT element
  // (0020,9167) AT (2005,140f)                              #   4, 1 FunctionalGroupPointer
  
  StatusType m_Status;
  bool m_MagnitudePresent;
  unsigned int m_NumberOfFrames;
  unsigned int m_NumberOfPhases;
  FrameType * m_FramesInfo;
  VolumeInfo m_VolumeInfo;
  float m_PCVelocity;
  VelocityComponentType m_VelocityComponent;
  ComplexImageComponentType m_ComplexImageComponent;
};

EnhancedMRI::EnhancedMRI()
  :
  m_Status(ST_UNDEFINED),
  m_MagnitudePresent(false),
  m_NumberOfFrames(0),
  m_NumberOfPhases(0),
  m_FramesInfo(NULL),
  m_PCVelocity(0.0),
  m_VolumeInfo(),
  m_VelocityComponent(VC_UNKNOWN),
  m_ComplexImageComponent(CIC_UNKNOWN)
{
}

StatusType EnhancedMRI::ReadDataSet( const gdcm::DataSet &ds )
{
  this->ReadGlobalParameters( ds );
  if ( this->GetStatus() != OK )
    {
      return this->GetStatus();
    }
  this->m_Status = EnhancedMRI::ReadFramesInfo( ds, this->m_FramesInfo, this->m_NumberOfFrames );
  if ( this->GetStatus() != OK )
    {
      return this->GetStatus();
    }
  return OK;
}

void EnhancedMRI::Print( std::ostream &_os ) const
{
  if ( this->m_Status == OK ) {
    indenter ind( 4 );
    _os << "Philip PhaseContrast MR Image 4D --\n"
        << ind << "ComplexImageComponent = " << GetComplexImageComponentName( this->GetComplexImageComponent() ) << "\n"
        << ind << "NumberOfFrames = " << this->GetNumberOfFrames() << "\n"
        << ind << "NumberOfPhases = " << this->GetNumberOfPhases() << "\n"
        << ind << "Velocity Component = " << GetVelocityComponentName( this->GetVelocityComponent() ) << "\n"
        << ind << "PCVelocity = " << this->GetPCVelocity() << "\n"
        << ind << "VolumeInfo =\n";
    this->m_VolumeInfo.Print( _os, 8 );
    _os << "\n";
  } else {
    _os << "Invalid Philip PhaseContrast MR Image 4D: "
        << GetStatusDescription( this->m_Status );
  }
}

StatusType EnhancedMRI::ReadGlobalParameters( const gdcm::DataSet &ds )
{
  int i;

  if ( (this->m_Status=EnhancedMRI::CheckManufacturer( ds )) != OK )
    {
      return this->m_Status;
    }
  if ( (this->m_Status=EnhancedMRI::CheckEnhancedMRImageType( ds )) != OK )
    {
      return this->m_Status;
    }
  if ( (this->m_Status=EnhancedMRI::CheckEnhancedMRSOPClassUID( ds )) != OK )
    {
      return this->m_Status;
    }
  if ( (this->m_Status=EnhancedMRI::CheckModalityMR( ds )) != OK )
    {
      return this->m_Status;
    }
  if ( (this->m_Status=EnhancedMRI::CheckFlowEncodeAcquisition( ds )) != OK )
    {
      return this->m_Status;
    }
  if ( (this->m_Status=EnhancedMRI::CheckMRAcquisitionType3D( ds )) != OK )
    {
      return this->m_Status;
    }
  if ( (this->m_Status=EnhancedMRI::CheckPhaseContrast( ds )) != OK )
    {
      return this->m_Status;
    }
  float pcv[3];
  if ( (this->m_Status=EnhancedMRI::ReadPCVelocity( ds, pcv )) != OK )
    {
      return this->m_Status;
    }
  for ( i = 0; i < 3; i++ )
    {
      if ( pcv[i] != 0.0 )
        {
          break;
        }
    }
  if ( i == 3 )
    {
      return NULL_PCV;
    }
  this->m_PCVelocity = pcv[i];
  switch ( i )
    {
    case 0:
      this->m_VelocityComponent = X;
      break;
    case 1:
      this->m_VelocityComponent = Y;
      break;
    default:
      this->m_VelocityComponent = Z;
      break;
    }
  // Global ComplexImageComponent
  std::string cicomp;
  if ( (this->m_Status=EnhancedMRI::ReadComplexImageComponent( ds, cicomp )) != OK )
    {
      return this->m_Status;
    }
  for ( i = CIC_UNKNOWN+1; i < ComplexImageComponentTypeEnd; i++ )
    {
      const char* cic = GetComplexImageComponentName( i );
      if ( !cicomp.compare( 0, strlen( cic ), cic ) )
        {
          this->m_ComplexImageComponent = static_cast<ComplexImageComponentType>( i );
          break;
        }
    }
  if ( i == ComplexImageComponentTypeEnd )
    {
      this->m_Status = UNK_COMPLEX_IC;
      return this->m_Status;
    }
  if ( (this->m_Status=EnhancedMRI::ReadNumberOfFrames( ds, this->m_NumberOfFrames )) != OK )
    {
      return this->m_Status;
    }
  if ( (this->m_Status=EnhancedMRI::ReadNumberOfPhasesMR( ds, this->m_NumberOfPhases )) != OK )
    {
      return this->m_Status;
    }
  if ( (this->m_Status=EnhancedMRI::ReadVolumeInfo( ds, this->m_VolumeInfo )) != OK )
    {
      return this->m_Status;
    }
  return this->m_Status;
}

StatusType EnhancedMRI::ReadFramesInfo( const gdcm::DataSet &ds,
                                        FrameType *& finfo, unsigned int size )
{
  StatusType status;

  //(5200,9230) SQ (Sequence with undefined length #=2050)  # u/l, 1 PerFrameFunctionalGroupsSequence
  // http://www.dabsoft.ch/dicom/6/6/#(5200,9230)
  gdcm::Tag tsq_PerFrameInfo(0x5200,0x9230);
  if( !ds.FindDataElement( tsq_PerFrameInfo ) )
    {
      return TAG_NOTFOUND;
    }
  const gdcm::DataElement &sq_PerFrameInfo = ds.GetDataElement( tsq_PerFrameInfo );
  const gdcm::SequenceOfItems *sqi = sq_PerFrameInfo.GetValueAsSQ();
  if ( !sqi )
    {
      return SQ_EMPTY;
    }
  if ( sqi->GetNumberOfItems() != size )
    {
      return SQ_LENGTH_MISSMATCH;
    }
  if ( !finfo )
    {
      finfo = new FrameType[ size ];
    }
  if ( !finfo )
    {
      return NO_MEMORY;
    }
  for( unsigned int i = 0; i < size; i++ )
    {
      const gdcm::Item & item = sqi->GetItem( i + 1 );
      const gdcm::DataSet& nds_item = item.GetNestedDataSet();
      // (0018,9226) SQ (Sequence with undefined length #=1)    # u/l, 1 MRImageFrameTypeSequence ## 4
      //http://www.dabsoft.ch/dicom/6/6/#(0018,9226)
      gdcm::Tag tsq_MRIFType(0x0018,0x9226);
      if( !nds_item.FindDataElement( tsq_MRIFType ) )
        {
          delete []finfo;
          finfo = NULL;
          return TAG_NOTFOUND;
        }
      const gdcm::DataElement &sq_MRIFType = nds_item.GetDataElement( tsq_MRIFType );
      const gdcm::SequenceOfItems *sqi_MRIFType = sq_MRIFType.GetValueAsSQ();
      if ( !sqi_MRIFType )
        {
          delete []finfo;
          finfo = NULL;
          return SQ_EMPTY;
        }
      const gdcm::Item & item_MRIFType = sqi_MRIFType->GetItem( 1 );
      const gdcm::DataSet& nds_MRIFType = item_MRIFType.GetNestedDataSet();
      std::string cicomp;
      if ( (status=EnhancedMRI::ReadComplexImageComponent( nds_MRIFType, cicomp )) != OK )
        {
          delete []finfo;
          finfo = NULL;
          return status;
        }
      finfo[ i ].m_ComplexImageComponent = GetComplexImageComponentId( cicomp.c_str() );
      //(2005,140f) SQ (Sequence with undefined length #=1)  # u/l, 1 Unknown Tag & Data 
      // PHILIP PRIVATE (2005 is odd), this tag appears in (0020,9222) SQ (Sequence with undefined length #=5)     # u/l, 1 DimensionIndexSequence
      // (0020,9167) AT (2005,140f)                              #   4, 1 FunctionalGroupPointer
      gdcm::Tag tsq_FGroup(0x2005,0x140f);
      if( !nds_item.FindDataElement( tsq_FGroup ) )
        {
          delete []finfo;
          finfo = NULL;
          return TAG_NOTFOUND;
        }
      const gdcm::DataElement &sq_FGroup = nds_item.GetDataElement( tsq_FGroup );
      const gdcm::SequenceOfItems *sqi_FGroup = sq_FGroup.GetValueAsSQ();
      if ( !sqi_FGroup )
        {
          delete []finfo;
          finfo = NULL;
          return SQ_EMPTY;
        }
      const gdcm::Item & item_FGroup = sqi_FGroup->GetItem( 1 );
      const gdcm::DataSet& nds_FGroup = item_FGroup.GetNestedDataSet();
      
      if ( (status=EnhancedMRI::ReadPhaseNumber( nds_FGroup, finfo[i].m_PhaseNumber )) != OK )
        {
          delete []finfo;
          finfo = NULL;
          return status;
        }
      
      if ( (status=EnhancedMRI::ReadSliceNumberMR( nds_FGroup, finfo[i].m_SliceNumberMR )) != OK )
        {
          delete []finfo;
          finfo = NULL;
          return status;
        }
    }
  return OK;
}

} // end namespace philips4d


void PrintFrame( const philips4d::EnhancedMRI &emri, int idx )
{
  const philips4d::FrameType *aFrame;

  aFrame = emri.GetFrameInfo( idx );
  if ( aFrame )
    {
      std::cout << "FrameInfo: " << idx + 1 << "\n";
      aFrame->Print( std::cout, 4 ); std::cout << "\n";
    }

}

int main(int argc, char *argv[])
{
  if( argc < 2 )
    {
    std::cerr << argv[0] << " input.dcm" << std::endl;
    return 1;
    }
  const char *filename = argv[1];

  
  // Instanciate the reader:
  gdcm::Reader reader;
  reader.SetFileName( filename );
  if( !reader.Read() )
    {
      std::cerr << "Could not read: " << filename << std::endl;
      return 1;
    }

  // If we reach here, we know for sure only 1 thing:
  // It is a valid DICOM file (potentially an old ACR-NEMA 1.0/2.0 file)
  // (Maybe, it's NOT a Dicom image -could be a DICOMDIR, a RTSTRUCT, etc-)

  // The output of gdcm::Reader is a gdcm::File
  gdcm::File &file = reader.GetFile();

  // the dataset is the the set of element we are interested in:
  gdcm::DataSet &ds = file.GetDataSet();

  philips4d::EnhancedMRI emri;

  philips4d::StatusType status = emri.ReadDataSet( ds );

  emri.Print( std::cout ); std::cout << "\n";

  PrintFrame( emri, 0 );
  PrintFrame( emri, 24 );
  PrintFrame( emri, 25 );
  PrintFrame( emri, 1025 );
  PrintFrame( emri, 2049 );
  
  /*
  if ( status == philips4d::EnhancedMRI::OK )
    {
      std::cout << "The DICOM is a valid Philips 4D MRI\n";
    }
  else
    {
      std::cout << "The DICOM is not a valid Philips 4D MRI: "
                << philips4d::EnhancedMRI::GetStatusDescription( status ) << " \n";
    }
  */
  return 0;
}

/*
 * (*) static type, means that extra DICOM information VR & VM are computed at compilation time.
 * The compiler is deducing those values from the template arguments of the class.
 */
