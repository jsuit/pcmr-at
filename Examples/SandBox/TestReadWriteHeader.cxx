#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/foreach.hpp>
#include <iostream>
#include <math.h>

using namespace boost::property_tree;

struct study_t
{
  size_t m_NumberOfTimeSteps;
  double m_LengthOfTimeStep;
  double m_VelocityEncoding;
  double m_MaximumVelocity;
  double m_MinimumVelocity;
  std::string m_ImageOrientation;
  std::vector<double> m_TimeValues;
  
  int load(const std::string& filename);
  int save(const std::string& filename);
};

int study_t::save(const std::string &filename)
{
  ptree pt;

  pt.put("Study.NumberOfTimeSteps", m_NumberOfTimeSteps);
  pt.put("Study.LengthOfTimeStep", m_LengthOfTimeStep);
  pt.put("Study.VelocityEncoding", m_VelocityEncoding);
  pt.put("Study.MaximumVelocity", m_MaximumVelocity);
  pt.put("Study.MinimumVelocity", m_MaximumVelocity);
  pt.put("Study.ImageOrientation", m_ImageOrientation);

  for(int i = 0; i < this->m_NumberOfTimeSteps; i++)
    {
    ptree& t = pt.add_child("Study.TimeValues.Time", ptree());
    t.put_value(this->m_TimeValues[i]);
    }

  boost::property_tree::xml_writer_settings<char> settings('\t', 1);
  std::ofstream ofs(filename.c_str());
  write_xml(ofs, pt, settings);
  return 0;
}
 
int study_t::load(const std::string &filename)
{
  ptree pt;

  read_xml(filename, pt);
  m_NumberOfTimeSteps = pt.get<size_t>("Study.NumberOfTimeSteps");
  m_LengthOfTimeStep = pt.get<double>("Study.LengthOfTimeStep");
  m_VelocityEncoding = pt.get<double>("Study.VelocityEncoding");
  m_MaximumVelocity = pt.get<double>("Study.MaximumVelocity");
  m_MinimumVelocity = pt.get<double>("Study.MinimumVelocity");
  m_ImageOrientation = pt.get<std::string>("Study.ImageOrientation");
  this->m_TimeValues.clear();
  char *endptr;
  BOOST_FOREACH(const boost::property_tree::ptree::value_type &v, 
                pt.get_child("Study.TimeValues"))
    {
    //std::string data(v.second.data());
    double t = strtod(v.second.data().c_str(), &endptr);
    this->m_TimeValues.push_back(t);
    }

  return 0;
}
 
int TestInitData(study_t &sample)
{
  sample.m_NumberOfTimeSteps = 10;
  sample.m_LengthOfTimeStep = 0.40;
  sample.m_ImageOrientation = "LAS";
  sample.m_VelocityEncoding = 150.0;
  sample.m_MaximumVelocity = 180;
  sample.m_MinimumVelocity = 0;
  for(int i = 0; i < sample.m_NumberOfTimeSteps; i++)
    {
    sample.m_TimeValues.push_back(i * sample.m_LengthOfTimeStep);
    }

  return 0;
}

int TestCheckHeader(const study_t& header1, const study_t& header2)
{
  if (header1.m_NumberOfTimeSteps != header2.m_NumberOfTimeSteps)
    {
    std::cout << "FAIL: m_NumberOfTimeSteps missmatch\n";
    return -1;
    }
  if (header1.m_LengthOfTimeStep != header2.m_LengthOfTimeStep)
    {
    std::cout << "FAIL: m_LengthOfTimeStep missmatch\n";
    return -1;
    }
  if (header1.m_ImageOrientation != header2.m_ImageOrientation)
    {
    std::cout << "FAIL: m_ImageOrientation missmatch\n";
    return -1;
    }
  if (header1.m_TimeValues.size() != header2.m_TimeValues.size())
    {
    std::cout << "FAIL: TimeValues size missmatch\n";
    return -1;
    }
  double eps = header2.m_LengthOfTimeStep * 1.0E-10;
  for (int i = 0; i < header1.m_TimeValues.size(); i++)
    {
    double d = header1.m_TimeValues[i] - header2.m_TimeValues[i];
    if (fabs(d) > eps)
      {
      std::cout << "FAIL: TimeValue missmatch at position " << i << "\n";
      std::cout << header1.m_TimeValues[i] << "!=" <<  header2.m_TimeValues[i] << "\n";
      std::cout << "d = " << d << "\n";      
      return -1;
      }
    }
  std::cout << "OK\n";
  return 0;
}

int main(int argc, const char* argv[])
{
  study_t sampleWrite;
  TestInitData(sampleWrite);
  
  sampleWrite.save("sample_header.xml");

  study_t sampleRead;
  sampleRead.load("sample_header.xml");

  TestCheckHeader(sampleRead, sampleWrite);

  return 0;
}
