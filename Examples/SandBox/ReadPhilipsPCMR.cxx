/*=========================================================================

  Program: GDCM (Grassroots DICOM). A DICOM library

  Copyright (c) 2006-2011 Mathieu Malaterre
  All rights reserved.
  See Copyright.txt or http://gdcm.sourceforge.net/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
/*
 * This example is ... guess what this is for :)
 */

#include "pcmrPhilipsGDCMIO.h"
#include <iostream>

void PrintFrame( const pcmr::Philips::EnhancedMRI &emri, int idx )
{
  const pcmr::Philips::FrameType *aFrame;

  aFrame = emri.GetFrameInfo( idx );
  if ( aFrame )
    {
      std::cout << "FrameInfo: " << idx + 1 << "\n";
      aFrame->Print( std::cout, 2 ); std::cout << "\n";
    }

}

int main(int argc, char *argv[])
{
  if( argc < 2 )
    {
    std::cerr << argv[0] << " input.dcm" << std::endl;
    return 1;
    }
  const char *filename = argv[1];
  
  // Instanciate the reader:
  gdcm::Reader reader;
  reader.SetFileName( filename );
  if( !reader.Read() )
    {
      std::cerr << "Could not read: " << filename << std::endl;
      return 1;
    }

  // If we reach here, we know for sure only 1 thing:
  // It is a valid DICOM file (potentially an old ACR-NEMA 1.0/2.0 file)
  // (Maybe, it's NOT a Dicom image -could be a DICOMDIR, a RTSTRUCT, etc-)

  // The output of gdcm::Reader is a gdcm::File
  gdcm::File &file = reader.GetFile();

  // the dataset is the the set of element we are interested in:
  gdcm::DataSet &ds = file.GetDataSet();

  pcmr::Philips::EnhancedMRI::Pointer emri = pcmr::Philips::EnhancedMRI::New();

  pcmr::StatusType status = emri->ReadDataSet( ds );

  emri.Print( std::cout ); std::cout << "\n";

  PrintFrame( *emri, 0 );
  PrintFrame( *emri, 24 );
  PrintFrame( *emri, 25 );
  PrintFrame( *emri, 1025 );
  PrintFrame( *emri, 1026 );
  PrintFrame( *emri, 1027 );
  PrintFrame( *emri, 1028 );
  PrintFrame( *emri, 1029 );
  PrintFrame( *emri, 2049 );
  
  return 0;
}
