#include "pcmrSiemensScanner.h"

int main(int argc, char *argv[])
{
  if (argc != 2)
    {
    std::cout << "Usage:\n"
              << argv[0] << " directory\n";
    return 1;
    }
  pcmr::Siemens::Scanner::Pointer SiemensScanner = pcmr::Siemens::Scanner::New();

  pcmr::StatusType status = SiemensScanner->Scan(argv[1]);
  if (status != pcmr::OK)
    {
    std::cout << "FAIL: " << pcmr::GetStatusDescription(status) << std::endl;
    return 1;
    }
  else
    std::cout << "OK\n";

  std::cout << "Number of TimeSteps = " << SiemensScanner->GetNumberOfTimeSteps() << std::endl;
  std::cout << "Velocity Encoding = " << SiemensScanner->GetVelocityEncoding()
            << std::endl;
  std::vector<double> timeValues;
  
  SiemensScanner->GetTimeValues(timeValues);
  for(size_t i = 0; i < timeValues.size(); i++)
    {
    std::cout << "Trigger(" << i << ") = " << timeValues[i] << std::endl;
    }
  return 0;
}
