#include <gdcmOrientation.h>
#include <boost/filesystem.hpp>
#include <iostream>
#include "itkImage.h"
#include "itkGDCMImageIO.h"
#include "itkImageSeriesReader.h"
#include "itkImageFileWriter.h"
#include "itkOrientImageFilter.h"
#include "itkImageFileWriter.h"

using namespace boost::filesystem;

typedef float   PixelType;
const unsigned int Dimension = 3;
typedef itk::Image< PixelType, Dimension >  ImageType;
typedef itk::ImageSeriesReader< ImageType > ReaderType;
typedef itk::GDCMImageIO ImageIOType;
typedef itk::ImageFileWriter< ImageType > WriterType;
typedef itk::OrientImageFilter<ImageType,ImageType> OrienterType;
typedef itk::ImageFileWriter<ImageType> WriterType;

const char* testSequence[] = 
{
  "anon.MR.PLM_AW_CARDIOLOGY_FLOW.0029.0096.2011.12.02.13.11.45.609375.1987943.IMA_anon.dcm",
  "anon.MR.PLM_AW_CARDIOLOGY_FLOW.0029.0095.2011.12.02.13.11.45.609375.1965434.IMA_anon.dcm",
  "anon.MR.PLM_AW_CARDIOLOGY_FLOW.0029.0094.2011.12.02.13.11.45.609375.1965056.IMA_anon.dcm",
  "anon.MR.PLM_AW_CARDIOLOGY_FLOW.0029.0093.2011.12.02.13.11.45.609375.1964678.IMA_anon.dcm",
  "anon.MR.PLM_AW_CARDIOLOGY_FLOW.0029.0092.2011.12.02.13.11.45.609375.1964300.IMA_anon.dcm",
  "anon.MR.PLM_AW_CARDIOLOGY_FLOW.0029.0091.2011.12.02.13.11.45.609375.1963922.IMA_anon.dcm",
  "anon.MR.PLM_AW_CARDIOLOGY_FLOW.0029.0090.2011.12.02.13.11.45.609375.1963544.IMA_anon.dcm",
  "anon.MR.PLM_AW_CARDIOLOGY_FLOW.0029.0089.2011.12.02.13.11.45.609375.1963166.IMA_anon.dcm",
  "anon.MR.PLM_AW_CARDIOLOGY_FLOW.0029.0088.2011.12.02.13.11.45.609375.1962788.IMA_anon.dcm",
  "anon.MR.PLM_AW_CARDIOLOGY_FLOW.0029.0087.2011.12.02.13.11.45.609375.1962410.IMA_anon.dcm",
  "anon.MR.PLM_AW_CARDIOLOGY_FLOW.0029.0086.2011.12.02.13.11.45.609375.1939904.IMA_anon.dcm",
  "anon.MR.PLM_AW_CARDIOLOGY_FLOW.0029.0085.2011.12.02.13.11.45.609375.1939526.IMA_anon.dcm",
  "anon.MR.PLM_AW_CARDIOLOGY_FLOW.0029.0084.2011.12.02.13.11.45.609375.1939148.IMA_anon.dcm",
  "anon.MR.PLM_AW_CARDIOLOGY_FLOW.0029.0083.2011.12.02.13.11.45.609375.1938770.IMA_anon.dcm",
  "anon.MR.PLM_AW_CARDIOLOGY_FLOW.0029.0082.2011.12.02.13.11.45.609375.1938392.IMA_anon.dcm",
  "anon.MR.PLM_AW_CARDIOLOGY_FLOW.0029.0081.2011.12.02.13.11.45.609375.1938014.IMA_anon.dcm",
  "anon.MR.PLM_AW_CARDIOLOGY_FLOW.0029.0080.2011.12.02.13.11.45.609375.1937636.IMA_anon.dcm",
  "anon.MR.PLM_AW_CARDIOLOGY_FLOW.0029.0079.2011.12.02.13.11.45.609375.1937258.IMA_anon.dcm",
  "anon.MR.PLM_AW_CARDIOLOGY_FLOW.0029.0078.2011.12.02.13.11.45.609375.1936880.IMA_anon.dcm",
  "anon.MR.PLM_AW_CARDIOLOGY_FLOW.0029.0077.2011.12.02.13.11.45.609375.1936502.IMA_anon.dcm",
  "anon.MR.PLM_AW_CARDIOLOGY_FLOW.0029.0076.2011.12.02.13.11.45.609375.1926999.IMA_anon.dcm",
  "anon.MR.PLM_AW_CARDIOLOGY_FLOW.0029.0075.2011.12.02.13.11.45.609375.1926621.IMA_anon.dcm",
  "anon.MR.PLM_AW_CARDIOLOGY_FLOW.0029.0074.2011.12.02.13.11.45.609375.1926243.IMA_anon.dcm",
  "anon.MR.PLM_AW_CARDIOLOGY_FLOW.0029.0073.2011.12.02.13.11.45.609375.1925865.IMA_anon.dcm"
};

int WriteImage3D( ImageType::Pointer image,
                  const char* outputImage )
{
  std::cout << "Writing the image as a vtk image '"
            << outputImage << "' ...\n";
   
  WriterType::Pointer writer = WriterType::New();
  writer->SetInput( image );
  writer->SetFileName( outputImage );
  try
    {
    writer->Update();
    }
  catch (itk::ExceptionObject &excp)
    {
    std::cerr << "Exception thrown while writing the image" << std::endl;
    std::cerr << excp << std::endl;
    return -1;
    }
  return 0;
}

int main(int argc, char*argv[])
{
  if (argc != 4)
    {
    std::cout << "Usage: " << argv[0] << " inputDir INV|KEEP LAS|RAS|IDENT" << std::endl;
    return 0;
    }
  const char *inputDir = argv[1];
  // check if the directory exists
  path pathInputDir( inputDir );
  if ( !exists( pathInputDir ) )
    {
    std::cerr << "Input path \"" << inputDir << "\" does not exists" << std::endl;
    return 1;
    }
  if ( !is_directory( pathInputDir ) ) 
    {
    std::cerr << "Input path \"" << inputDir << "\" is not a directory" << std::endl;
    return 1;
    }

  bool invSeq = false;
  if (!strcmp(argv[2], "INV"))
    {
    invSeq = true;
    }
  else if(!strcmp(argv[2], "KEEP"))
    {
    invSeq = false;
    }
  else
    {
    std::cout << "Invalid argument for sequence order '" << argv[2] << "' must be INV or KEEP\n";
    return 0;
    }
  itk::SpatialOrientation::ValidCoordinateOrientationFlags oriReq;
  if (!strcmp(argv[3], "IDENT"))
    {
    oriReq = itk::SpatialOrientation::ITK_COORDINATE_ORIENTATION_INVALID;
    }
  else if(!strcmp(argv[3], "LAS"))
    {
    oriReq = itk::SpatialOrientation::ITK_COORDINATE_ORIENTATION_LAS;
    }
  else if(!strcmp(argv[3], "RAS"))
    {
    oriReq = itk::SpatialOrientation::ITK_COORDINATE_ORIENTATION_RAS;
    }
  else
    {
    std::cout << "Invalid orientation requested '" << argv[3] << "' must be IDENT, LAS or RAS\n";
    return 0;
    }
  
  ImageIOType::Pointer gdcmIO = ImageIOType::New();
  ReaderType::FileNamesContainer filenames ;
  for(int i = 0; i < sizeof(testSequence)/sizeof(testSequence[0]); ++i)
    {
    path dcm(pathInputDir);
    dcm /= testSequence[i];
    if (invSeq)
      {
      filenames.insert(filenames.begin(), dcm.string().c_str());
      }
    else
      {
      filenames.push_back(dcm.string().c_str());
      }
    }

  ReaderType::Pointer reader = ReaderType::New();
 
  reader->SetImageIO( gdcmIO );
  reader->SetFileNames( filenames );
  try
    {
    reader->Update();
    }
  catch (itk::ExceptionObject &excp)
    {
    std::cerr << "Exception thrown while reading the series" << std::endl;
    std::cerr << excp << std::endl;
    return EXIT_FAILURE;
    }
  OrienterType::Pointer orienter = OrienterType::New();
  if (oriReq != itk::SpatialOrientation::ITK_COORDINATE_ORIENTATION_INVALID)
    {
    orienter->UseImageDirectionOn();
    orienter->SetDesiredCoordinateOrientation(oriReq);
    orienter->SetInput(reader->GetOutput());
    orienter->Update();
    }
  std::string name("TestImageOrientation");
  if (invSeq)
    {
    name += "_INV";
    }
  if (oriReq == itk::SpatialOrientation::ITK_COORDINATE_ORIENTATION_LAS)
    {
    name += "_LAS";
    }
  else if (oriReq == itk::SpatialOrientation::ITK_COORDINATE_ORIENTATION_RAS)
    {
    name += "_RAS";
    }
  name += ".vtk";
  if (oriReq == itk::SpatialOrientation::ITK_COORDINATE_ORIENTATION_INVALID)
    {
    WriteImage3D(reader->GetOutput(), name.c_str());
    }
  else
    {
    WriteImage3D(orienter->GetOutput(), name.c_str());
    }
  std::cout << "DONE\n";
  return 0;
}
