/*=========================================================================

  Program: GDCM (Grassroots DICOM). A DICOM library

  Copyright (c) 2006-2011 Mathieu Malaterre
  All rights reserved.
  See Copyright.txt or http://gdcm.sourceforge.net/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
/*
 * This example is ... guess what this is for :)
 */

#include "gdcmReader.h"
#include "gdcmWriter.h"
#include "gdcmAttribute.h"
#include "gdcmPrivateTag.h"
#include "gdcmStringFilter.h"

#include <iostream>
#include <typeinfo>

template< int TVR, int TVM >
int TryTag( uint16_t group, uint16_t element,
            gdcm::DataSet &ds,
            gdcm::Element<TVR,TVM> &el )
{
  gdcm::VR vr = gdcm::Element<TVR,TVM>::GetVR();
  const char* vrString = gdcm::VR::GetVRString( vr );
  gdcm::Tag tag( group, element );
  if( !ds.FindDataElement( tag ) ) {
    std::cout << "No aparece el tag " << tag << "\n";
    return 0;
  }
  std::cout << "El tag " << tag << " ha sido encontrado\n";
  const gdcm::DataElement& data = ds.GetDataElement( tag );
  std::cout << "ValueLength = " << data.GetVL() << std::endl;
  std::cout << "VR = " << gdcm::VR::GetVRString( data.GetVR() ) << std::endl;
  //gdcm::Element<gdcm::VR::SL,gdcm::VM::VM1> el2;
  el.SetFromDataElement( data );
  return 1;
}


int main(int argc, char *argv[])
{
  if( argc < 2 )
    {
    std::cerr << argv[0] << " input.dcm" << std::endl;
    return 1;
    }
  const char *filename = argv[1];

  // Instanciate the reader:
  gdcm::Reader reader;
  reader.SetFileName( filename );
  if( !reader.Read() )
    {
    std::cerr << "Could not read: " << filename << std::endl;
    return 1;
    }

  // The output of gdcm::Reader is a gdcm::File
  gdcm::File &file = reader.GetFile();

  // the dataset is the the set of element we are interested in:
  gdcm::DataSet &ds = file.GetDataSet();

  //gdcm::Attribute<0x0008,0x0008> at;
  //at.SetFromDataSet( ds );
  const gdcm::DataElement &de = ds.GetDataElement( gdcm::Tag(0x0008,0x0008 ) );
  gdcm::Value const & ve = de.GetValue();
  std::cout << typeid(ve).name() <<std::endl ;
  
  std::cout << "Value Length ve =" << ve.GetLength() << std::endl;
  std::cout << de << std::endl;

  gdcm::StringFilter sf;
  sf.SetFile(file);
  std::cout << "Attribute Value as String: " << "'" << sf.ToString( gdcm::Tag(0x0008,0x0008 ) ) << "'" << std::endl;


  std::stringstream strm;
  ds.GetDataElement( gdcm::Tag (0x0008, 0x0008) ).GetValue().Print(strm);
  std::cout << "Value=" << strm << std::endl;
  //gdcm::Attribute<0x0008,0x0008>::ArrayType v tImageType.GetValue();
  //std::cout << "ImageType from attribute=" << v << std::endl;

  // (0008,0008) ?? (CS) [ORIGINAL\PRIMARY\P\ND ]            # 22,2-n ImageType
  // http://www.dabsoft.ch/dicom/6/6/
  gdcm::Element<gdcm::VR::CS,gdcm::VM::VM2> el_ImageType;
  if ( TryTag<gdcm::VR::CS,gdcm::VM::VM2>( 0x0008, 0x0008, ds, el_ImageType ) ) {
    el_ImageType.Print( std::cout ); std::cout << "\n";
  }

  // (0008,0060) CS [MR]                                     #   2, 1 Modality
  // http://www.dabsoft.ch/dicom/6/6/#(0008,0060)
  gdcm::Element<gdcm::VR::CS,gdcm::VM::VM1> el_Modality;
  if ( TryTag<gdcm::VR::CS,gdcm::VM::VM1>( 0x0008, 0x0060, ds, el_Modality ) ) {
    el_Modality.Print( std::cout ); std::cout << "\n";
  }

  // (0008,0070) ?? (LO) [SIEMENS ]                          # 8,1 Manufacturer
  // http://www.dabsoft.ch/dicom/6/6/#(0008,0070)
  gdcm::Element<gdcm::VR::LO,gdcm::VM::VM1> el_Manufacturer;
  if ( TryTag<gdcm::VR::LO,gdcm::VM::VM1>( 0x0008, 0x0070, ds,
                                           el_Manufacturer ) ) 
    {
    el_Manufacturer.Print( std::cout ); std::cout << "'\n";
    }
  // (0008,1090) ?? (LO) [Avanto]                            # 6,1 Manufacturer's Model Name
  // http://www.dabsoft.ch/dicom/6/6/#(0008,1090)
  gdcm::Element<gdcm::VR::LO,gdcm::VM::VM1> el_ManufacturersModelName;
  if ( TryTag<gdcm::VR::LO,gdcm::VM::VM1>( 0x0008, 0x1090, ds,
                                           el_ManufacturersModelName ) ) 
    {
    el_ManufacturersModelName.Print( std::cout ); std::cout << "\n";
    }
  
  //(0018,0023) CS [3D]                         #   2, 1 MRAcquisitionType
  // http://www.dabsoft.ch/dicom/6/6/#(0018,0023)
  gdcm::Element<gdcm::VR::CS,gdcm::VM::VM1> el_MRAcquisitionType;
  if ( TryTag<gdcm::VR::CS,gdcm::VM::VM1>( 0x0018, 0x0023, ds,
                                           el_MRAcquisitionType ) ) 
    {
    el_MRAcquisitionType.Print( std::cout ); std::cout << "\n";
    }

  // (0018,1090) ?? (IS) [14]                   # 2,1 Cardiac Number of Images
  gdcm::Element<gdcm::VR::IS,gdcm::VM::VM1> el_CardiacNumberOfImages;
  if ( TryTag<gdcm::VR::IS,gdcm::VM::VM1>( 0x0018, 0x1090, ds,
                                           el_CardiacNumberOfImages ) ) 
    {
    el_CardiacNumberOfImages.Print( std::cout ); std::cout << "\n";
    }

  //(0028,0010) US 256                          #   2, 1 Rows
  // http://www.dabsoft.ch/dicom/6/6/#(0028,0010)
  gdcm::Element<gdcm::VR::US,gdcm::VM::VM1> el_Rows;
  if ( TryTag<gdcm::VR::US,gdcm::VM::VM1>( 0x0028, 0x0010, ds,
                                           el_Rows ) ) 
    {
    el_Rows.Print( std::cout ); std::cout << "\n";
    }

  //(0028,0011) US 256                          #   2, 1 Columns
  // http://www.dabsoft.ch/dicom/6/6/#(0028,0011)
  gdcm::Element<gdcm::VR::US,gdcm::VM::VM1> el_Columns;
  if ( TryTag<gdcm::VR::US,gdcm::VM::VM1>( 0x0028, 0x0011, ds,
                                           el_Columns ) ) 
    {
    el_Columns.Print( std::cout ); std::cout << "\n";
    }

  //(0028,0100) US 16                           #   2, 1 BitsAllocated
  // http://www.dabsoft.ch/dicom/6/6/#(0028,00100)
  gdcm::Element<gdcm::VR::US,gdcm::VM::VM1> el_BitsAllocated;
  if ( TryTag<gdcm::VR::US,gdcm::VM::VM1>( 0x0028, 0x0100, ds,
                                           el_BitsAllocated ) ) 
    {
    el_BitsAllocated.Print( std::cout ); std::cout << "\n";
    }
  
  //(0028,0101) US 12                           #   2, 1 BitsStored
  // http://www.dabsoft.ch/dicom/6/6/#(0028,00101)
  gdcm::Element<gdcm::VR::US,gdcm::VM::VM1> el_BitsStored;
  if ( TryTag<gdcm::VR::US,gdcm::VM::VM1>( 0x0028, 0x0101, ds,
                                           el_BitsStored ) )
    {
    el_BitsStored.Print( std::cout ); std::cout << "\n";
    }

  //(0028,0102) US 11                           #   2, 1 HighBit
  // http://www.dabsoft.ch/dicom/6/6/#(0028,0102)
  gdcm::Element<gdcm::VR::US,gdcm::VM::VM1> el_HighBit;
  if ( TryTag<gdcm::VR::US,gdcm::VM::VM1>( 0x0028, 0x0102, ds,
                                           el_HighBit ) )
    {
    el_HighBit.Print( std::cout ); std::cout << "\n";
    }

  // 0x001910cb	SS	1	K	Phase Contrast flow axis GEMS_ACQU_01
  gdcm::Element<gdcm::VR::CS,gdcm::VM::VM1> el_PhaseAxis;
  if ( TryTag<gdcm::VR::CS,gdcm::VM::VM1>( 0x0008, 0x9208, ds,
                                           el_PhaseAxis) )
    {
    el_PhaseAxis.Print( std::cout ); std::cout << "\n";
    }


  return 0;
}

/*
 * (*) static type, means that extra DICOM information VR & VM are computed at compilation time.
 * The compiler is deducing those values from the template arguments of the class.
 */
