#include "pcmrUtil.h"
#include "pcmrSiemensScanner.h"

int main(int argc, char *argv[])
{
  if (argc != 2)
    {
    std::cout << "Usage:\n"
              << argv[0] << " directory\n";
    return 1;
    }
  pcmr::Siemens::Scanner::Pointer SiemensScanner = pcmr::Siemens::Scanner::New(); 

  pcmr::StatusType status = SiemensScanner->Scan(argv[1]);
  if (status != pcmr::OK)
    {
    std::cout << "FAIL: " << pcmr::GetStatusDescription(status) << std::endl;
    return 1;
    }
  
  const std::string &centralSlice = SiemensScanner->GetMagnitudeCentralSlice(3);
  std::cout << "Central Slice: " << centralSlice << std::endl;
  
  status = pcmr::WriteSliceToPNG(centralSlice.c_str(),
                                 "/tmp/test.png", 0);

  std::cout << "OK\n";
  SiemensScanner = NULL;
    
  return 0;
}
