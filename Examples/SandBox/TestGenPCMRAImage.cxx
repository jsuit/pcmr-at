#include "pcmrPhilipsImageReader.h"
#include "itkPhaseContrastTimeAveragedImageFilter.h"
#include "itkExtractImageFilter.h"
#include "itkImageFileWriter.h"

template < class TImagePointer >
void PrintImage( const char * label, const TImagePointer image )
{
  std::cout << label << "\n";
  std::cout << image->GetOrigin(); std::cout << "\n";
  std::cout << image->GetSpacing(); std::cout << "\n";
  image->GetLargestPossibleRegion().Print( std::cout );
}

int main( int argc, char *argv[] )
{
  // Validate input parameters
  if( argc < 1 )
    {
      std::cerr << "Usage: " 
                << argv[0]
                << " DICOM_Directory"
                << std::endl;
      return EXIT_FAILURE;
    }
  itk::PhaseContrastImage::Pointer img4DMagnitude;
  itk::PhaseContrastImage::Pointer img4DPhaseX;
  itk::PhaseContrastImage::Pointer img4DPhaseY;
  itk::PhaseContrastImage::Pointer img4DPhaseZ;
  //pcmr::Philips::RescalerType phaseRescale;
  pcmr::Philips::RescalerType phaseRescale( 0.09768009768009/1.75555555555555, -200 );

  const unsigned int factorReduceXY = 4;
  
  std::string IM1( argv[1] );
  IM1 += "/IM_0001";
  std::cout << "Reading 4D magnitude from " << IM1 << "\n";
  img4DMagnitude = pcmr::Philips::ReadMagnitudeImage( IM1.c_str(), 25, 41, 1.5, factorReduceXY );

  std::cout << "Reading 4D phase from " << IM1 << "\n";
  img4DPhaseX = pcmr::Philips::ReadPhaseImage( IM1.c_str(), 25, 41, phaseRescale, 1.5, factorReduceXY );
  
  std::string IM3( argv[1] );
  IM3 += "/IM_0003";
  std::cout << "Reading 4D phase from " << IM3 << "\n";
  img4DPhaseY = pcmr::Philips::ReadPhaseImage( IM3.c_str(), 25, 41, phaseRescale, 1.5, factorReduceXY );
  
  std::string IM5( argv[1] );
  IM5 += "/IM_0005";
  std::cout << "Reading 4D phase from " << IM5 << "\n";
  img4DPhaseZ = pcmr::Philips::ReadPhaseImage( IM5.c_str(), 25, 41, phaseRescale, 1.5, factorReduceXY );

 
  PrintImage( "Magnitude Image:", img4DMagnitude );
  
  /*
  PrintImage( "Phase X Image:", img4DPhaseX );
  PrintImage( "Pahse Y Image:", img4DPhaseY );
  PrintImage( "Phase Z Image:", img4DPhaseZ );
  */
  
  std::cout << "Performing PC-MRA time average ...\n";
  
  itk::PhaseContrastTimeAveragedImageFilter::Pointer PCMRAFilter = itk::PhaseContrastTimeAveragedImageFilter::New();
  PCMRAFilter->SetMagnitudeImage( img4DMagnitude );
  PCMRAFilter->SetPhaseXImage( img4DPhaseX );
  PCMRAFilter->SetPhaseYImage( img4DPhaseY );
  PCMRAFilter->SetPhaseZImage( img4DPhaseZ );
  PCMRAFilter->Update();

  std::cout << "PC-MRA time average done!\n";
  std::cout << "  MinimunVelocity = " << PCMRAFilter->GetMinimumVelocity() << std::endl
            << "  MaximunVelocity = " << PCMRAFilter->GetMaximumVelocity() << std::endl;
  
  //PCMRAFilter->Print( std::cout );
  //PrintImage( "Result of PCMRA filter:",  PCMRAFilter->GetOutput() );
  
  typedef itk::ImageFileWriter< itk::PhaseContrast3DImage >
    WriterType;

  WriterType::Pointer writer = WriterType::New();
  writer->SetInput( PCMRAFilter->GetOutput() );
  std::string outputName( "pcmra.vtk" );
  //PCMRAFilter->GetOutput()->Print( std::cout );
  writer->SetFileName( outputName );
  writer->Update();
  
  return 0;
}
