#include <vector>
#include <boost/foreach.hpp>
#include "pcmrFlow4DReader.h"
#include <iostream>
#include "vtkMath.h"
#include "vtkNew.h"
#include "vtkSetGet.h"
#include "vtkObjectFactory.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkStreamingDemandDrivenPipeline.h"
#include "vtkParticleTracer.h"
#include "vtkParticlePathFilter.h"
#include "vtkStreaklineFilter.h"
#include "vtkPointSource.h"
#include <vtkPointData.h>
#include "vtkCellArray.h"
#include "vtkPolyDataWriter.h"
#include "vtkImageShiftScale.h"
#include "vtkDataSetAttributes.h"
#include "vtkArrayCalculator.h"

int RescaleVelocity(vtkDataArray *vectorData, double factor)
{
  if (vectorData->GetNumberOfComponents() != 3 )
    {
    std::cout << "RescaleVelocity: vectorData must be a vector array\n";
    return 0;
    }
  double v[3];
  for(vtkIdType i = 0; i < vectorData->GetNumberOfTuples(); i++)
    {
    vectorData->GetTuple(i, v);
    for(size_t j = 0; j < 3; j++)
      {
      v[j] *= factor;
      }
    vectorData->SetTuple(i, v);
    }
  return 1;
}

class TestTimeSource : public vtkAlgorithm
{
public:
  static TestTimeSource *New();
  vtkTypeMacro(TestTimeSource, vtkAlgorithm);

  void SetDataSetReader(pcmr::Flow4DReader *reader)
  {
    this->m_DataSetReader = reader;
    this->m_TimeValues.clear();
    reader->GetTimeValues(this->m_TimeValues);
    for(size_t i = 0; i < reader->GetNumberOfTimeSteps(); i++)
      {
      this->m_TimeValues[i] /= 1000.0;
      }
  }

  const std::vector<double>& GetTimeValues() const
  {
    return this->m_TimeValues;
  }

  int GetNumRequestData()
  {
    return this->m_NumRequestData;
  }

  int GetNumberOfTimeSteps()
  {
    if (this->m_DataSetReader)
      {
      return static_cast<int>(this->m_DataSetReader->GetNumberOfTimeSteps());
      }
    else
      {
      return 0;
      }
  }

  float GetLengthOfTimeStep() const
  {
    return this->m_DataSetReader->GetLengthOfTimeStep()/1000.0;
  }

protected:
  TestTimeSource()
  {
    this->m_DataSetReader = NULL;
    this->m_NumRequestData = 0;
    this->SetNumberOfInputPorts(0);
    this->SetNumberOfOutputPorts(1);
  }

  ~TestTimeSource()
  {
  }

  int FindIndexForTime(double t)
  {
    if (t < this->m_TimeValues[0])
      {
      return -1;
      }
    double deltaT = this->GetLengthOfTimeStep();
    double eps = deltaT * 1.0e-6;
    for(size_t i = 0; i < this->m_TimeValues.size(); i++)
      {
      if(fabs(t - this->m_TimeValues[i])<eps)
        {
        return static_cast<int>(i);
        }
      }
    return -1;
  }

  int ProcessRequest(vtkInformation* request,
                     vtkInformationVector** inputVector,
                     vtkInformationVector* outputVector)
  {
    // generate the data
    if(request->Has(vtkDemandDrivenPipeline::REQUEST_DATA()))
      {
      return this->RequestData(request, inputVector, outputVector);
      }

    // execute information
    if(request->Has(vtkDemandDrivenPipeline::REQUEST_INFORMATION()))
      {
      return this->RequestInformation(request, inputVector, outputVector);
      }
    return this->Superclass::ProcessRequest(request, inputVector, outputVector);
  }

  int FillOutputPortInformation(int, vtkInformation *info)
  {
    info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkImageData");
    return 1;
  }

  virtual int RequestInformation(vtkInformation *,
                                 vtkInformationVector **,
                                 vtkInformationVector *outputInfoVector)
  {
    // get the info objects
    vtkInformation *outInfo = outputInfoVector->GetInformationObject(0);
    
    int T = this->GetNumberOfTimeSteps();
    if (T<1)
      {
      return 0;
      }
    double range[2];
    range[0] = this->m_TimeValues[0];
    range[1] = this->m_TimeValues[T-1];
    outInfo->Set(vtkStreamingDemandDrivenPipeline::TIME_RANGE(),
                 range,2);

    outInfo->Set(vtkStreamingDemandDrivenPipeline::TIME_STEPS(),
                 &(this->m_TimeValues[0]), 
                 static_cast<int>(this->m_TimeValues.size()));

    pcmr::vtkImagePointer mag = this->m_DataSetReader->GetMagnitudeImage(0);
    
    int extent[6];
    mag->GetExtent(extent);
    outInfo->Set(vtkStreamingDemandDrivenPipeline::WHOLE_EXTENT(), extent, 6);

    double spacing[3];
    mag->GetSpacing(spacing);
    outInfo->Set(vtkDataObject::SPACING(), spacing[0], spacing[1], spacing[2]);

    double origin[3];
    mag->GetOrigin(origin);
    outInfo->Set(vtkDataObject::ORIGIN(), origin, 3);

    // ascending point
    int IJK0[] = {65, 115, 4};
    // descending point
    int IJK1[] = {111, 75, 10};
    double px = origin[0] + IJK1[0] * spacing[0];
    double py = origin[1] + IJK1[1] * spacing[1];
    double pz = origin[2] + IJK1[2] * spacing[2];
    std::cout << "p=(" << px << "," << py << "," << pz << ")\n";

    return 1;
  }

  int RequestData(
    vtkInformation* ,
    vtkInformationVector** vtkNotUsed( inputVector ),
    vtkInformationVector* outputVector)
  {
    this->m_NumRequestData++;
    vtkInformation *outInfo = outputVector->GetInformationObject(0);
    vtkDataObject* output = outInfo->Get(vtkDataObject::DATA_OBJECT());

    double timeStep = outInfo->Get( vtkStreamingDemandDrivenPipeline::UPDATE_TIME_STEP());

    std::cout << "output = " << output << "\n";
    std::cout << "requested ts = " << timeStep << "\n";
    output->GetInformation()->Set(vtkDataObject::DATA_TIME_STEP(), timeStep);
    //static_cast<size_t>(vtkMath::Ceil(timeStep));
    int idx = FindIndexForTime(timeStep);
    if (idx == -1)
      {
      std::cout << "could not find time interval for time-value" << timeStep << std::endl;
      return 0;
      }
    std::cout << "requested idx = " << idx << "\n";
    
    pcmr::vtkImagePointer imgFlow = this->m_DataSetReader->GetFlowImage(pcmr::Flow4DReader::FlowVector, idx);
    // shallow copy the vector image to output
    // set the extent to be the update extent
    vtkImageData *outImage = vtkImageData::SafeDownCast(output);
    if (!outImage)
      {
      std::cout << "could not convert output to vtkImageData\n";
      return 0;
      }

    std::cout << "outImage = " << outImage << "\n";
    outImage->DeepCopy(imgFlow);
    vtkDataArray *pointVectors = outImage->GetPointData()->GetVectors();
    RescaleVelocity(pointVectors, 10.0);
    /*
    double v[3];
    for(vtkIdType i = 0; i < pointVectors->GetNumberOfTuples(); i++)
      {
      pointVectors->GetTuple(i, v);
      for(size_t j = 0; j < 3; j++)
        {
        v[j] *= 10.0;
        }
      pointVectors->SetTuple(i, v);
      }
    */
    return 1;
    
    /*
    vtkNew<vtkArrayCalculator> calc;
    calc->SetInputData(imgFlow);
    calc->AddVectorArrayName("Velocity");
    calc->SetFunction("10*Velocity");
    calc->SetResultArrayName("Velocity");
    calc->Update();
    
    if (0 && idx == 4)
      {
      calc->GetOutput()->Print(std::cout);
      }
    outImage->DeepCopy(calc->GetOutput());
    //outImage->ShallowCopy(imgFlow);
    //outImage->Print(std::cout);
    return 1;
   */
  }


private:
  TestTimeSource(const TestTimeSource&); // Not implemented.
  void operator=(const TestTimeSource&);  // Not implemented.

  pcmr::Flow4DReader * m_DataSetReader;
  std::vector<double> m_TimeValues;
  float m_TimeStepLength;
  int m_NumRequestData;
};

vtkStandardNewMacro(TestTimeSource);

#define EXPECT(a,msg)\
  if(!(a)) {                                    \
  cerr<<"Line "<<__LINE__<<":"<<msg<<endl;\
  return EXIT_FAILURE;\
  }

int TestParticleTracer(pcmr::Flow4DReader *reader)
{
  vtkNew<TestTimeSource> imageSource;
  imageSource->SetDataSetReader(reader);
  std::cout << imageSource->GetNumberOfTimeSteps() << " time-steps\n";
  vtkNew<vtkPointSource> ps;
  //ps->SetCenter(0.5,0.,0.);
  ps->SetCenter(32.7068,66.9225,104.302);
  ps->SetRadius(2);
  ps->SetNumberOfPoints(50);
  vtkNew<vtkParticleTracer> filter;
  filter->SetInputConnection(0,imageSource->GetOutputPort());
  filter->SetInputConnection(1, ps->GetOutputPort());
  filter->SetComputeVorticity(0);
  filter->SetTerminalSpeed(0.1);
  
  std::cout << "filter->SetStartTime(0.1)\n";
  filter->SetStartTime(0.1);
  filter->SetTerminationTime(4.5);
  std::cout << "filter->SetTerminationTime(4.5)\n";
  filter->Update();
  std::cout << "AFTER filter->Update()\n";
  double data_time = filter->GetOutputDataObject(0)->GetInformation()->Get(vtkDataObject::DATA_TIME_STEP());
  int numRequestData = imageSource->GetNumRequestData();
  EXPECT(data_time==4.5,"Wrong time");

  if(numRequestData!=6)
    {
    cerr<<"Wrong num requests\n";
    return EXIT_FAILURE;
    }

  return 1;
}

int TestPathlineFilter(pcmr::Flow4DReader *reader)
{
  std::cout << "START: TestPathlineFilter\n";

  vtkNew<TestTimeSource> imageSource;
  imageSource->SetDataSetReader(reader);
  int T = imageSource->GetNumberOfTimeSteps();
  std::cout << imageSource->GetNumberOfTimeSteps() << " time-steps\n";
  vtkNew<vtkPointSource> ps;
  //ps->SetCenter(0.5,0.,0.);
  //ps->SetCenter(32.7068,66.9225,104.302);
  
  
  /*
  float x = 7.37117430244035;
  float y = 3.03041552700691;
  float z = 11.5368820784822;
  */
  /*
  float x = 7.43010082125806;
  float y = 6.94482659646116;
  float z = 11.5575215388111;
  */

  
  float x = 76.1085; 
  float y = 45.6454;
  float z = 119.214;
  
  /*
  float x = 43.3318; 
  float y = -3.02539;
  float z = 106.302;*/
  ps->SetCenter(x, y, z);
  ps->SetRadius(5);
  ps->SetNumberOfPoints(80);

  vtkNew<vtkParticlePathFilter> filter;
  //vtkNew<vtkStreaklineFilter> filter;
  filter->SetForceReinjectionEveryNSteps(0);
  filter->SetIntegratorType(vtkParticleTracerBase::RUNGE_KUTTA4);
  filter->SetInputConnection(0,imageSource->GetOutputPort());
  filter->SetInputConnection(1, ps->GetOutputPort());
  filter->SetComputeVorticity(0);
  filter->SetTerminalSpeed(10);
  const std::vector<double> &timeValues = imageSource->GetTimeValues();
  filter->SetStartTime(timeValues[0]);
  int N = imageSource->GetNumberOfTimeSteps();
  int R = 3;
  float deltaT = imageSource->GetLengthOfTimeStep();
  if (R == 1)
    {
    filter->SetTerminationTime(1.0);
    filter->Update();
    }
  else
    {
    //float delta = deltaT/R;
    for (int i = 1; i < N; i++)
      {
      double t0 = timeValues[i-1];
      std::cout << "t0 = " << t0 << ", " << i << "\n";
      double t1 = timeValues[i];
      double delta = (t1-t0)/R;
      for (int j = 1; j <= R; j++)
        {
        filter->SetTerminationTime(t0 + delta*j);
        filter->Update();
        }
      vtkNew<vtkPolyDataWriter> writer;
      char buffer[256];
      sprintf(buffer, "/tmp/path_line_%d.vtk", i);
      writer->SetFileName(buffer);
      writer->SetInputConnection(filter->GetOutputPort());
      writer->Update();
      }
    }
  //filter->Print(std::cout);

  //vtkPolyData* out = filter->GetOutput();
  //out->Print(std::cout);
  //return 1;

  vtkNew<vtkPolyDataWriter> writer;
  writer->SetFileName("/tmp/path_line_total.vtk");
  writer->SetInputConnection(filter->GetOutputPort());
  RescaleVelocity(filter->GetOutput()->GetPointData()->GetVectors(), 0.1);
  writer->Update();
  std::cout << "END: TestPathlineFilter\n";
  return 1;
}

int main(int argc, char *argv[])
{
  if (argc != 2)
    {
    std::cerr << "usage: '" << argv[0] << "' study\n";
    return -1;
    }
  pcmr::Flow4DReader::Pointer reader = pcmr::Flow4DReader::New();

  pcmr::StatusType status = reader->SetDirectory(argv[1]);
  if (status != pcmr::OK)
    {
    std::cerr << "SetDirectory: " << pcmr::GetStatusDescription(status) << "\n";
    return status;
    }
  std::cout << "Velocity Encoding = " << reader->GetVelocityEncoding() << "\n";
  std::cout << "Number of Time Steps = " << reader->GetNumberOfTimeSteps() << "\n";
  std::cout << "Length of Time Step = " << reader->GetLengthOfTimeStep() << "\n";
  std::cout << "Image Orientation = " << reader->GetImageOrientation() << "\n";
  std::vector<std::string> properties;
  reader->GetExtendedProperties(properties);
  pcmr::HeaderReader::Property prop;
  BOOST_FOREACH(std::string &v, properties)
    {
    reader->GetExtendedProperty(v, prop);
    std::cout << prop.description << " = " << prop.value << "\n";
    }

  //TestParticleTracer(reader);
  TestPathlineFilter(reader);
  return 0;
}

// DATA TEST
// Ascending
//   ps->SetCenter(32.7068,66.9225,104.302);
//   ../../../../../biomedical/data/Studies/A_APP

// Descending 
// 73.436,31.5059,116.302
