#include <boost/foreach.hpp>
#include "pcmrFlow4DReader.h"
#include <iostream>
#include <boost/filesystem.hpp>

#include <vtkNew.h>
#include <vtkDataSetWriter.h>
#include <vtkXMLImageDataWriter.h>
#include <vtkStructuredPointsWriter.h>

#ifdef _MSC_VER
#define snprintf _snprintf
#endif

using namespace boost::filesystem;

int main(int argc, char *argv[])
{
  if (argc != 3)
    {
    std::cerr << "usage: '" << argv[0] << "' study prefix_out\n";
    return -1;
    }
  path prefix_out(argv[2]);
  path destDir = prefix_out.parent_path();
  if (!exists(destDir))
    {
    std::cerr << "Destination directory " << destDir << "does not exists\n";
    return -1;
    }
  
  pcmr::Flow4DReader::Pointer reader = pcmr::Flow4DReader::New();

  pcmr::StatusType status = reader->SetDirectory(argv[1]);
  if (status != pcmr::OK)
    {
    std::cerr << "SetDirectory: " << pcmr::GetStatusDescription(status) << "\n";
    return status;
    }
  std::cout << "Velocity Encoding = " << reader->GetVelocityEncoding() << "\n";
  std::cout << "Number of Time Steps = " << reader->GetNumberOfTimeSteps() << "\n";
  std::cout << "Length of Time Step = " << reader->GetLengthOfTimeStep() << "\n";
  std::cout << "Image Orientation = " << reader->GetImageOrientation() << "\n";
  std::vector<std::string> properties;
  reader->GetExtendedProperties(properties);
  pcmr::HeaderReader::Property prop;
  BOOST_FOREACH(std::string &v, properties)
    {
    reader->GetExtendedProperty(v, prop);
    std::cout << prop.description << " = " << prop.value << "\n";
    }

  vtkNew<vtkDataSetWriter> writer;
  writer->SetFileTypeToBinary();
  //vtkNew<vtkXMLImageDataWriter> writer;
  std::string pattern(prefix_out.string());
  char tb[16];
  for(size_t i = 0; i < reader->GetNumberOfTimeSteps(); i++)
    {
    std::cout << "accessing timestep "<< i << " ...\n";
    pcmr::vtkImagePointer flow = reader->GetFlowImage(pcmr::Flow4DReader::FlowVector,i);
    std::string dest(prefix_out.string());
    snprintf(tb, 16, "%d", i);
    dest += "_";
    dest += tb;
    dest += ".vtk";
    if (!i) 
      {
      flow->Print(std::cout);
      //break;
      }
    writer->SetFileName(dest.c_str());
    //vtkNew<vtkImageData> tmp;
    //std::cout << "voy a construir tmp\n";    
    //tmp->DeepCopy(flow);
    //std::cout << "tmp construido\n";
    //writer->SetInputData(tmp.GetPointer());
    writer->SetInputData(flow);
    writer->Update();
    std::cout << dest << "\n";
    }
  return 0;
}
