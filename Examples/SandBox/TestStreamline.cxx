#include "pcmrFlow4DReader.h"
#include "vtkNew.h"
#include "vtkRungeKutta4.h"
#include "vtkStreamLine.h"
#include "vtkPointSource.h"
#include "vtkPolyDataWriter.h"

int TestStreamline(pcmr::Flow4DReader *reader)
{
  vtkNew<vtkPointSource> ps;
  // this point is to be used with Studies/A_APP
  float x = 76.1085; 
  float y = 47.6454;
  float z = 117.214;
  ps->SetCenter(x, y, z);
  ps->SetRadius(0);
  ps->SetNumberOfPoints(1);
  pcmr::vtkImagePointer imgFlow = reader->GetFlowImage(pcmr::Flow4DReader::FlowVector, 3);

  vtkNew<vtkRungeKutta4> RungeKutta4;
  vtkNew<vtkStreamLine> filter;
  filter->SetSourceConnection(ps->GetOutputPort());
  filter->SetInputData(imgFlow);
  filter->SetMaximumPropagationTime(10);
  filter->SetIntegrationStepLength(0.05);
  filter->SetStepLength(.05);
  filter->SetNumberOfThreads(2);
  filter->SetIntegrationDirectionToForward();
  filter->SpeedScalarsOn();
  filter->SetIntegrator(RungeKutta4.GetPointer());
  filter->Update();
  filter->GetOutput()->Print(std::cout);
  vtkNew<vtkPolyDataWriter> writer;
  writer->SetFileName("/tmp/stream_line.vtk");
  writer->SetInputConnection(filter->GetOutputPort());
  writer->Update();

  return 1;
}

int main(int argc, char *argv[])
{
  if (argc != 2)
    {
    std::cerr << "usage: '" << argv[0] << "' study\n";
    return -1;
    }
  pcmr::Flow4DReader::Pointer reader = pcmr::Flow4DReader::New();

  pcmr::StatusType status = reader->SetDirectory(argv[1]);
  if (status != pcmr::OK)
    {
    std::cerr << "SetDirectory: " << pcmr::GetStatusDescription(status) << "\n";
    return status;
    }

  TestStreamline(reader);
  return 0;
}
