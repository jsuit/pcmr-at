cmake_minimum_required (VERSION 2.8)
project (PCMR)

set(BUILD_SHARED_LIBS ON)

enable_testing()

if( APPLE )
  # enable @rpath in the install name for any shared library being built
  set(CMAKE_MACOSX_RPATH 1)
endif( APPLE )

macro( print_all_variables msg )
  if( PRINT_KNOWN_VARIABLES )
    message( STATUS " =======================================================" )
    message( STATUS " ======================== ${msg} =======================" )
    message( STATUS " =======================================================" )
    get_cmake_property(_variableNames VARIABLES)
    foreach (_variableName ${_variableNames})
      message(STATUS "${_variableName}=${${_variableName}}")
    endforeach()
  endif( )
endmacro( )


macro( check_vtk_enabled module var )
  list( FIND VTK_MODULES_ENABLED ${module} check_module )
  if( check_module EQUAL -1 )
    set( ${var} OFF )
  else( )
    set( ${var} ON )
  endif( )
endmacro( check_vtk_enabled )

function( link_ifvtk module target )
  check_vtk_enabled( ${module} enabled )
  if( enabled )
    target_link_libraries( ${target} ${ARGN} )
  endif( )
endfunction( link_ifvtk )

include (GenerateExportHeader)
# Enable ExternalProject CMake module
include (ExternalProject)

# Set default ExternalProject root directory
#SET_DIRECTORY_PROPERTIES(PROPERTIES EP_PREFIX ${CMAKE_BINARY_DIR}/ThirdParty)

#find_package (GDCM REQUIRED)
#include (${GDCM_USE_FILE})

find_package (ITK REQUIRED)
if( ITK_FOUND )
  include( ${ITK_USE_FILE} )
  if( NOT  ITKVtkGlue_LOADED )
    message( FATAL_ERROR "ITK must be compiled with module ITKVtkGlue enabled" )
  endif( NOT ITKVtkGlue_LOADED )
else( ITK_FOUND )
  message( FATAL_ERROR "ITK is needed and must be compiled with module ITKVtkGlue enabled" )
endif( ITK_FOUND )

print_all_variables( "AFTER ITK" )

find_package( VTK REQUIRED )
if( VTK_FOUND )
  include( ${VTK_USE_FILE} )
  if( NOT VTK_WRAP_TCL )
    message( FATAL_ERROR "VTK from ${VTK_USE_FILE} must be compiled with VTK_WRAP_TCL=ON" )
  endif( NOT VTK_WRAP_TCL )
  check_vtk_enabled( vtkParallelMPI VTK_WITH_MPI )
  if( VTK_WITH_MPI )
    include( ${VTK_CMAKE_DIR}/vtkMPI.cmake )
  endif( VTK_WITH_MPI )
else( VTK_FOUND )
  message( FATAL_ERROR "VTK with TCL_WRAPPING is needed!" )
endif( VTK_FOUND )

print_all_variables( "AFTER VTK" )
#message( FATAL_ERROR ABORT )

set( BOOST_COMPONENTS_NEEDED filesystem thread system )

# The following verifyies that BOOST_ROOT is set properly.
if(NOT BOOST_ROOT AND NOT $ENV{BOOST_ROOT} STREQUAL "")
  FILE( TO_CMAKE_PATH $ENV{BOOST_ROOT} BOOST_ROOT )
  if( NOT EXISTS ${BOOST_ROOT} )
    MESSAGE( STATUS  ${BOOST_ROOT} " does not exist. Checking if BOOST_ROOT was a quoted string.." )
    STRING( REPLACE "\"" "" BOOST_ROOT ${BOOST_ROOT} )
    if( EXISTS ${BOOST_ROOT} )
      MESSAGE( STATUS "After removing the quotes " ${BOOST_ROOT} " was now found by CMake" )
    endif( EXISTS ${BOOST_ROOT})
  endif( NOT EXISTS ${BOOST_ROOT} )

  # Save the BOOST_ROOT in the cache
  if( NOT EXISTS ${BOOST_ROOT} )
    MESSAGE( WARNING ${BOOST_ROOT} " does not exist." )
  else(NOT EXISTS ${BOOST_ROOT})
    SET (BOOST_ROOT ${BOOST_ROOT} CACHE STRING "Set the value of BOOST_ROOT to point to the root folder of your boost install." FORCE)
  endif( NOT EXISTS ${BOOST_ROOT} )

endif(NOT BOOST_ROOT AND NOT $ENV{BOOST_ROOT} STREQUAL "")

if( WIN32 AND NOT BOOST_ROOT )
  MESSAGE( WARNING "Please set the BOOST_ROOT environment variable." )
endif( WIN32 AND NOT BOOST_ROOT )

#set(Boost_ADDITIONAL_VERSIONS "1.47" "1.47.0")
set(Boost_DEBUG OFF)
set(Boost_USE_STATIC_LIBS       OFF)
set(Boost_USE_MULTITHREADED      ON)
set(Boost_USE_STATIC_RUNTIME    OFF)
FIND_PACKAGE(Boost REQUIRED COMPONENTS ${BOOST_COMPONENTS_NEEDED})
if(Boost_FOUND)
  MESSAGE( STATUS "Setting up boost." )
  include_directories(${Boost_INCLUDE_DIRS})
  if(Boost_DEBUG)
    MESSAGE( STATUS "BOOST Libraries " ${Boost_LIBRARIES} )
    MESSAGE( STATUS "BOOST LIBRARY DIRS = " ${Boost_LIBRARY_DIRS} )
    FOREACH(BOOST_COMPONENT ${BOOST_COMPONENTS_NEEDED})
      STRING( TOUPPER ${BOOST_COMPONENT} BOOST_COMPONENT_UPCASE )
      MESSAGE( STATUS "Boost " ${BOOST_COMPONENT} ": " ${Boost_${BOOST_COMPONENT_UPCASE}_LIBRARY} )
      MESSAGE( STATUS "Boost " ${BOOST_COMPONENT} " Debug: " ${Boost_${BOOST_COMPONENT_UPCASE}_LIBRARY_DEBUG} )
      MESSAGE( STATUS "Boost " ${BOOST_COMPONENT} " Release: " ${Boost_${BOOST_COMPONENT_UPCASE}_LIBRARY_RELEASE} )
    ENDFOREACH(BOOST_COMPONENT)
  endif(Boost_DEBUG)
endif(Boost_FOUND)

if( APPLE )
  add_definitions(-DGTEST_USE_OWN_TR1_TUPLE=1)
  set( CMAKE_GTEST_ARGS "-DCMAKE_CXX_FLAGS=-DGTEST_USE_OWN_TR1_TUPLE=1" )
elseif (MSVC)
  add_definitions (-D_SCL_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_WARNINGS -DBOOST_CONFIG_SUPPRESS_OUTDATED_MESSAGE)
  add_definitions (-DBOOST_ALL_NO_LIB)
  add_definitions (-D_HAS_ITERATOR_DEBUGGING=0)
  set(MSVC_CXX_OPT -D_HAS_ITERATOR_DEBUGGING=0 -Dgtest_force_shared_crt=ON)
  set( CMAKE_GTEST_ARGS "-DCMAKE_CXX_FLAGS=-DGTEST_USE_OWN_TR1_TUPLE=1 -D_HAS_ITERATOR_DEBUGGING=0 -Dgtest_force_shared_crt=ON" )
endif( APPLE )

find_package(GTest REQUIRED)
macro(pcmr_add_test t)
  target_link_libraries(${t} ${GTEST_BOTH_LIBRARIES})
  target_include_directories(${t} PRIVATE ${GTEST_INCLUDE_DIRS})
  add_test(
    NAME ${t}
    COMMAND ${t} ${ARGN})
endmacro()

macro(use_module_dir)
  foreach(dir ${ARGN})
    # Make sure the compiler can find include files for module in dir
    include_directories ("${PCMR_SOURCE_DIR}/Source/${dir}")
    include_directories ("${PCMR_BINARY_DIR}/Source/${dir}")
  endforeach()
endmacro()

set(Test_Sample_Study ""
  CACHE FILEPATH "study path to be used during the tests")

if( CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT OR INSTALL_WITH_VTK )
  set( CMAKE_INSTALL_PREFIX "${VTK_INSTALL_PREFIX}" )
  set( TCL_INSTALL_DIR "lib/tcltk" )
else( )
  set( TCL_INSTALL_DIR "lib" )
endif( CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT OR INSTALL_WITH_VTK )

add_subdirectory (Source)
#add_subdirectory (Examples)
add_subdirectory (Tcl)
