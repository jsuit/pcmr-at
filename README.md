# PCMR-AT = Phase Contrast Magnetic Resonance Analysis Tool

In this project we develop a code to:

* import 4D flow dataset from DICOM format: SIEMENS & PHILIPS.
* visualize the flow: streamline, vectors.
* quantify the flow: through cut plane and contour in plane.
* segment the vessel based on flow directions.
* export the velocity field interpolated on a FE mesh.
