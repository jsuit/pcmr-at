package require snit
package require BWidget
package require msgcat
package require Tk 8.5
package require Img
package require HeaderXml
package require FindFileSeries
package require vtk

if {$::tcl_platform(platform) eq "unix"} {
  package require fsdialog
}

namespace import -force msgcat::*

snit::type CFDImporterDialog {
  typevariable _win
  typevariable options
  typevariable inputDir
  typevariable outputDir
  typevariable serieID ""
  typevariable serieFinder
  typevariable data
  typevariable widgets
  typevariable cmdChooseDirectory
  typevariable t0
  typevariable delta
  typevariable venc
  typevariable geoUnits
  typevariable velUnits
  typevariable spacing -array {x "" y "" z ""}
  typevariable UserSelected
  typevariable VolumeGeometry -array {
    Origin ""
    Dimensions ""
    Spacing ""
  }

  typeconstructor {
    set serieFinder [FindFileSeries %AUTO%]
    if {$::tcl_platform(platform) eq "unix"} {
      set cmdChooseDirectory ttk::chooseDirectory
    } else {
      set cmdChooseDirectory tk_chooseDirectory
    }
    
    if {![info exists inputDir]} {
      set inputDir ""
    }
    if {![info exists outputDir]} {
      set outputDir ""
    }

    set data(progress) ""
    if { [ lsearch [ image names ] folder_yellow_16 ] == -1 } {
      image create photo folder_yellow_16 -data {
        R0lGODlhEAAQAPYAAK+KGLGMGLOOGbKNGrKMG7OMG7OOG7SOGbSPGbWPH7SQGaOFI8GLNMmJN8Se
        MMSXOcSfO9eQOuOePN6xLcOhN8WgNMahNcehNcmjNMijN8mmNcukN8ulN8umNsumN8KgOMeiOMei
        OcemPMekPselPsemPtGnN9uqNeG9K+K4LeO/LOS8LeW/LeytO/G5OeTALPTHN/XMNvTBOffQNcun
        QtGtQtCuSd6+Q922Sum8S/TCTvjETvjITvjOTe/USO7USfrTTfzaS/veTPzgTPzkTfzmUfvpXd3A
        ZurTg/rrhvLgl/nrqAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
        AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
        AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5
        BAEAAEwALAAAAAAQABAAAAeSgEyCg4SFhoeGBwgICgGIggYTKSssAgqXlwiEAC4yMDEzKi8vKioo
        A4MALScYGxwesLAdFRcgTAASJj9GSUu+v0tKFkwEERlCREW8wEtIIUwFDTVAQUPJy75HggUMODw9
        1NbKvTbbDzk7O9/h1w7bEDrp6evVPhqCCTTy++s3gwsfSJQoQWIEiYIjRFB4xLAhoUAAOw==
      }
    }
  }

  typemethod GetTmpDir {} {
    global env tcl_platform
    
    if {$tcl_platform(platform) eq "unix"} {
      set TmpDir /tmp
    } else {
      if {[info exist env(TEMP)] && [file exists $env(TEMP)]} {
        set TmpDir $env(TEMP)
      } elseif {[info exist env(TMP)] && [file exists $env(TMP)]} {
        set TmpDir $env(TMP)
      } else {
        set TmpDir "C:/"
      }
    }
    return $TmpDir
  }

  proc OnStartWriteEvent { } {
    variable widgets

    puts "Writing volumes"
    $widgets(status,msg) configure -text [mc "Writing volumes"]...
    update
  }

  proc OnAwakeEvent { } {
    variable data

    if { [catch {incr data(progress)} msg] } {
      puts $msg
    }
    update
  }

  typemethod CreateEntryDirectory {w args} {
    array set opts {
      -mustexist 1
      -varname ""
      -labeltext "Enter directory"
      -acceptcmd ""
      -validatecommand ""
    }
    array set opts $args
    frame $w
    ttk::label $w.lb -text "$opts(-labeltext):"
    set wentry [ttk::entry $w.ent -validatecommand $$opts(-validatecommand)]
    if {$opts(-varname) ne ""} {
      $wentry configure -textvariable [mytypevar $opts(-varname)]
    }
    if {$opts(-acceptcmd) ne ""} {
      bind $wentry <Return> $opts(-acceptcmd)
    }
    button $w.btn -image folder_yellow_16 \
        -command [mytypemethod ChooseDir $opts(-varname) -mustexist $opts(-mustexist) -acceptcmd $opts(-acceptcmd)]
    grid $w.lb -row 0 -column 0 -sticky "ne"
    grid $w.ent -row 0 -column 1 -sticky "ew"
    grid $w.btn -row 0 -column 2 -sticky "w"
    grid columnconfigure $w 1 -weight 1
    return $w
  }

  typemethod OnChangeOutputDir { args } {
    $type CheckExportState
  }

  typemethod OnAcceptInputDir { args } {
    $serieFinder Search $inputDir "*.vtu"
    
    foreach s [$serieFinder GetSeriesNames] {
      $widgets(series) insert end $s
    }
    set serieID ""
    $type CheckExportState
  }

  typemethod OnChangeInputDir { args } {
    $widgets(series) delete 0 end
    set serieID ""
    $type CheckExportState
  }

  typemethod ClearStatus {} {
    $widgets(status,msg) configure -text ""
    set data(progress) ""
  }

  typemethod StatusMessage { msg } {
    $widgets(status,msg) configure -text $msg
  }
  
  typemethod CheckExportState { args } {
    if {[file exists $inputDir] && [file isdirectory $inputDir] &&
        $outputDir ne "" && [string is double $t0] && $t0 >= 0 &&
        [string is double $delta] && $delta > 0 && 
        [string is double $spacing(x)] && $spacing(x) > 0 && 
        [string is double $spacing(y)] && $spacing(y) > 0 && 
        [string is double $spacing(z)] && $spacing(z) > 0 && 
        $serieID ne ""} {
      $_win itemconfigure 0 -state normal
    } else {
      $_win itemconfigure 0 -state disabled
    }
    return 1
  }
  
  typemethod Show { args } {
    set w .dlgCFDImport
    
    set options(-parent) "."
    array set options $args
    if {![winfo exists $w ]} {
      set wparent $options(-parent)
      set bg [ $wparent cget -background ]
      Dialog $w -title [ mc "Import CFD 4D Sequence" ]  \
          -parent $wparent -modal local -cancel 1 -separator 1 \
          -background $bg -transient false
      $w add -text [ mc "Import" ] -command [mytypemethod OnClickOK] \
          -state disabled -background $bg
      $w add -text [ mc "Close" ]  \
          -background $bg
      set f [$w getframe]

      $type CreateEntryDirectory $f.inputDir \
          -varname "inputDir" -labeltext [mc "Input directory"] \
          -acceptcmd [mytypemethod OnAcceptInputDir] \
                          -validatecommand [mytypemethod OnChangeInputDir]
      grid $f.inputDir -row 0 -column 1 -sticky snew
      
      # =========== File Series =============
      ttk::labelframe $f.fseries -text [mc "File Series"]
      grid $f.fseries -row 1 -column 0 -sticky snew -columnspan 2 
      set widgets(series) [listbox $f.fseries.list -background white\
                               -selectmode single]
      bind $f.fseries.list <<ListboxSelect>> [mytypemethod OnSelectSerie]
      grid $widgets(series) -row 0 -column 0 -sticky snew

      # ===========  Bounds  =============
      set fi [ttk::labelframe $f.fseries.finfo -text [mc "Bounds"]]
      grid $fi -row 1 -column 0 -sticky snew
      foreach a {X Y Z} r {1 2 3} {
        set widgets(bounds,${a}R) [ttk::label $fi.lb${a}R -text [mc "%s range" $a]:]
        grid $widgets(bounds,${a}R) -row $r -column 0 -sticky e
      }

      # ===========  Frame Parameters  =============
      set fp [frame $f.fseries.fparam]
      grid $fp -row 0 -column 1 -sticky snew -rowspan 2

      grid rowconfigure $f.fseries 0 -weight 1
      grid rowconfigure $f.fseries 1 -weight 1
      grid columnconfigure $f.fseries 0 -weight 1
      grid columnconfigure $f.fseries 1 -weight 1

      # =========== Start Time =============
      ttk::label $fp.lbt0 -text [mc "Start Time"]:
      ttk::entry $fp.entt0 -textvariable [mytypevar t0]
      trace add variable [mytypevar t0] write [mytypemethod CheckExportState]
      grid $fp.lbt0 -row 0 -column 1 -sticky en
      grid $fp.entt0 -row 0 -column 2 -sticky we

      # =========== Time-Step =============
      ttk::label $fp.lbdelta -text [mc "Time-Step"]:
      ttk::entry $fp.entdelta -textvariable [mytypevar delta]
      trace add variable [mytypevar delta] write [mytypemethod CheckExportState]
      grid $fp.lbdelta -row 1 -column 1 -sticky en
      grid $fp.entdelta -row 1 -column 2 -sticky we

      # =========== Velocity Encoding =============
      ttk::label $fp.lbvenc -text [mc "Velocity Encoding"]:
      ttk::entry $fp.entvenc -textvariable [mytypevar venc]
      trace add variable [mytypevar venc] write [mytypemethod CheckExportState]
      grid $fp.lbvenc -row 2 -column 1 -sticky en
      grid $fp.entvenc -row 2 -column 2 -sticky we

      # =========== Geometry Units =============
      ttk::label $fp.lbgounit -text [mc "Geometry"]:
      ttk::menubutton $fp.mbgeounit -textvariable [mytypevar geoUnits] \
          -menu $fp.mbgeounit.m
      set m [menu $fp.mbgeounit.m -tearoff 0]
      $m add command -label "mm" -command [mytypemethod OnChangeGeoUnits "mm"]
      $m add command -label "cm" -command [mytypemethod OnChangeGeoUnits "cm"]
      $m add command -label "m" -command [mytypemethod OnChangeGeoUnits "m"]
      set geoUnits "mm"
      grid $fp.lbgounit -row 3 -column 1 -sticky en
      grid $fp.mbgeounit -row 3 -column 2 -sticky we

      # =========== Velocity Units =============
      ttk::label $fp.lbvelunit -text [mc "Velocity"]:
      ttk::menubutton $fp.mbvelunit -textvariable [mytypevar velUnits] \
          -menu $fp.mbvelunit.m
      set m [menu $fp.mbvelunit.m -tearoff 0]
      $m add command -label "mm/s" -command [mytypemethod OnChangeVelUnits "mm/s"]
      $m add command -label "cm/s" -command [mytypemethod OnChangeVelUnits "cm/s"]
      $m add command -label "m/s" -command [mytypemethod OnChangeVelUnits "m/s"]
      set velUnits "mm/s"
      grid $fp.lbvelunit -row 4 -column 1 -sticky en
      grid $fp.mbvelunit -row 4 -column 2 -sticky we
      
      # ============ spacing ===============
      ttk::label $fp.lbspac -text [mc "Spacing"]:
      frame $fp.fspac
      $type createEntry $fp.fspac.esx spacing(x) -width 5
      $type createEntry $fp.fspac.esy spacing(y) -width 5
      $type createEntry $fp.fspac.esz spacing(z) -width 5
      grid $fp.fspac.esx $fp.fspac.esy $fp.fspac.esz -sticky ew
      grid columnconfigure $fp.fspac 0 -weight 1
      grid columnconfigure $fp.fspac 1 -weight 1
      grid columnconfigure $fp.fspac 2 -weight 1
      grid $fp.lbspac -row 5 -column 1 -sticky en
      grid $fp.fspac -row 5 -column 2 -sticky we

      $type CreateEntryDirectory $f.outputDir \
          -varname "outputDir" -labeltext [mc "Output directory"] -mustexist 0 \
          -acceptcmd [mytypemethod OnChangeOutputDir]
      grid $f.outputDir -row 2 -column 0 -sticky snew -columnspan 2

      frame $f.fstatus -bd 1 -relief ridge
      grid $f.fstatus -row 4 -column 0 -sticky snew -columnspan 2
      grid columnconfigure $f.fstatus 0 -weight 3
      grid columnconfigure $f.fstatus 1 -weight 1
      set widgets(status,msg) [ttk::label $f.fstatus.msg]
      grid $widgets(status,msg) -row 0 -column 0 -sticky snew
      ProgressBar $f.fstatus.pbar -type normal -maximum 100 \
          -variable  [mytypevar data(progress)] \
          -bd 1 -relief sunken \
          -troughcolor $bg -foreground #c3c3ff
      set widgets(status,pbar) $f.fstatus.pbar
      grid $widgets(status,pbar) -row 0 -column 1 -sticky snew
      grid columnconfigure $f 1 -weight 1
      grid rowconfigure $f 1 -weight 1
      set _win $w
      wm protocol $_win WM_DELETE_WINDOW [myproc WM_DELETE_WINDOW $_win]
    } else {
    }
    $type ClearStatus
    #$widgets(txtTrace) delete 0.0 end
    set f [$_win getframe]
    set x [expr [winfo screenwidth $_win]/2 - [winfo reqwidth $f]/2 \
               - [winfo vrootx [winfo parent $_win]]]
    set y [expr [winfo screenheight $_win]/2 - [winfo reqheight $f]/2 \
               - [winfo vrooty [winfo parent $_win]]]
    $_win configure -geometry +$x+$y
    $_win draw
  }
  
  typemethod createEntry {w tv args} {
    ttk::entry $w -textvariable [mytypevar $tv] {*}$args
    trace add variable [mytypevar $tv] write [mytypemethod CheckExportState]
  }
  
  typemethod OnChangeGeoUnits { u } {
    set geoUnits $u
  }

  typemethod OnChangeVelUnits { u } {
    set velUnits $u
  }

  typemethod ChooseDir { varname args } {    
    array set opts {
      -acceptcmd ""
      -mustexist 1
    }
    array set opts $args
    if {$varname ne ""} {
      set initialdir [set $varname]
    } else {
      set initialdir [pwd]
    }
    set res [$cmdChooseDirectory -parent $_win -initialdir $initialdir \
                 -mustexist $opts(-mustexist) -title [mc "Select output directory"]]
    update
    if {$res ne "" && $varname ne ""} {
      set $varname $res
      if {$opts(-acceptcmd) ne ""} {
        after idle $opts(-acceptcmd)
      }
    }
  }
  
  typemethod OnSelectSerie { } {
    set s [$widgets(series) get active]
    if {$s ne ""} {
      set serieID $s
      $type ReadBounds $s
      $type CheckExportState
    }
  }

  typemethod ReadBounds { s } {
    set oneFile [lindex [$serieFinder GetFilesInSerie $s] 0]
    if {$oneFile ne ""} {
      set reader [vtkXMLUnstructuredGridReader New]
      $reader SetFileName $oneFile
      $reader Update
      set vtu [$reader GetOutput]
      set f [$type GetGeometryFactor]
      foreach {l u} [$vtu GetBounds] a {X Y Z} {
        set _l [expr {$l*$f}]
        set _u [expr {$u*$f}]
        set d [format "%.4f" [expr {$_u - $_l}]]
        $widgets(bounds,${a}R) configure -text [mc "%s range: %.4f to %.4f (delta: %.4f)" $a $_l $_u $d]
      }
      puts "[$vtu GetBounds]"
      $reader Delete
    }
  }

  typemethod GetGeometryFactor { } {
    switch -exact $geoUnits {
      mm {
        return 1.0
      }
      cm {
        return 10
      }
      m {
        return 1000
      }
    } 
  }
  
  typemethod GetVelocityFactor { } {
    switch -exact $velUnits {
      mm/s {
        return 0.1
      }
      cm/s {
        return 1.0
      }
      m/s {
        return 100.0
      }
    }
  }

  typemethod OnClickOK { } {
    if {![file exists $inputDir] || ![file readable $inputDir]} {
      MessageDlg $_win.ask -title [ mc "Status Directory" ] \
          -icon error \
          -type ok -aspect 75 \
          -message [ mc "Directory '%s' does not exists or is not readable" $inputDir ]
      return
    }
    if {[file exists $outputDir]} {
      if {![file writable $outputDir]} {
        MessageDlg $_win.ask -title [mc "Status Directory"] \
            -icon error \
            -type ok -aspect 75 \
            -message [mc "Directory %s is not writable" $outputDir]
        return
      } else {
        set ans [MessageDlg $_win.ask -title [mc "Confirm overwrite"] \
                     -icon warning \
                     -type okcancel -aspect 75 \
                     -message [mc "Directory '%s' already exist, overwrite?" $outputDir]]
        if {$ans == 1} {
          return
        }
        set lsDir [glob -nocomplain -dir $outputDir *]
        if {[llength $lsDir]} {
          if {[catch {file delete -force {*}$lsDir} msgerror]} {
            MessageDlg $_win.ask -title [mc "Status Directory"] \
                -icon error \
                -type ok -aspect 75 \
                -message [ mc "While initializing '%s':\n %s" $outputDir $msgerror ]
            return
          }
        }
      }
    } else {
      set ans [MessageDlg $_win.ask -title [mc "Confirm create"] \
                   -icon warning \
                   -type okcancel -aspect 75 \
                   -message [ mc "Directory '%s' is going to be created, proceed?" $outputDir ] ]
      if { $ans == 1 } {
        return
      }
      
      if {[catch {file mkdir $outputDir} msgerror]} {
        MessageDlg $_win.ask -title [mc "Status Directory"] \
            -icon error \
            -type ok -aspect 75 \
            -message [ mc "While creating '%s':\n %s" $outputDir $msgerror ]
        return
      }
    }
    update
    $type doImport 
    #Dialog::enddialog $_win 1
  }
  
  typemethod doImport {} {
    $type ClearVolumeGeometry
    set data(progress) 0
    set t 0
    set files [$serieFinder GetFilesInSerie $serieID]
    set num [llength $files]
    foreach f $files {
      $type StatusMessage "Importing [file tail $f]"
      update
      $type importVelocity $f $t
      set data(progress) [expr {100.0*double($t+1)/$num}]
      incr t
    }
    file copy [file join $outputDir "mag_0.vtk"] [file join $outputDir "mask.vtk"]
    file copy [file join $outputDir "mag_0.vtk"] [file join $outputDir "premask.vtk"]
    set headerXML [HeaderXmlWriter %AUTO%]
    $headerXML SetTimeInfo $t $t0 $delta 
    $headerXML SetVelocityEncoding $venc
    foreach c { X Y Z } {
      $headerXML SetInvertComponent $c 0
    }
    $headerXML WriteToFile [file join $outputDir "header.xml"]
    $headerXML destroy

    MessageDlg $_win.msgStatus -title "Import Status" \
        -message "Import was successful" -icon info -type ok
    Dialog::enddialog $_win 1
  }

  typemethod importVelocity {f t} {
    puts "Importing velocity from $f"
    set vtuReader [vtkXMLUnstructuredGridReader New]
    $vtuReader SetFileName $f
    $vtuReader Update
    set pathPrefix [file join $outputDir "vct_${t}"]
    set sampleSize 50
    SampleCFDMeshToStudy [$vtuReader GetOutput] $pathPrefix \
        $spacing(x) $spacing(y) $spacing(z) $sampleSize 8 0.1
    file copy ${pathPrefix}_2.vtk [file join $outputDir "mag_${t}.vtk"]
    $vtuReader Delete
  }

  typemethod OnProbeProgressEvent { filter } {
    set data(progress) [expr {100.0*[$filter GetProgress]}]
    #puts -nonewline [$filter GetProgress]
    #flush stdout
  }

  typemethod WriteVelocityComponents {filter t} {
    puts "==> goint to write components resampled"
    set assign [vtkAssignAttribute New]
    $assign SetInputConnection [$filter GetOutputPort]
    $assign Assign VECTORS SCALARS POINT_DATA
    set extract [vtkImageExtractComponents New]
    $extract SetInputConnection [$assign GetOutputPort]
    set passFilter [vtkPassArrays New]
    $passFilter SetInputConnection [$extract GetOutputPort]
    $passFilter ClearArrays
    $passFilter AddPointDataArray "Velocity"
    set writer [vtkStructuredPointsWriter New]
    $writer SetInputConnection [$passFilter GetOutputPort]
    foreach c {0 1 2} {
      $extract SetComponents $c
      $writer SetFileName [file join $outputDir "vct_${t}_${c}.vtk"]
      $writer Update
    }
    $writer SetFileName [file join $outputDir "mag_${t}.vtk"]
    $writer Update
    $writer Delete
    $passFilter Delete
    $extract Delete
    $assign Delete
  }

  typemethod ComputeVolumeGeometry { grid } {
    if {[llength $VolumeGeometry(Origin)]} {
      return
    }
    set bounds [$grid GetBounds]
    foreach a {x y z} {min max} $bounds {
      set s $spacing($a)
      foreach {o n} [$type ComputeAxisPartition $min $max $s] break
      lappend VolumeGeometry(Origin) $o
      lappend VolumeGeometry(Dimensions) $n
      lappend VolumeGeometry(Spacing) $s
    }
  }

  typemethod ComputeAxisPartition {min max s} {
    set n1 [expr {int(ceil(double($max - $min)/$s))}]
    set n2 8
    set o [expr {$min - $n2*$s}]
    set n [expr {$n1 + 2*$n2}]
    return [list $o $n]
  }

  typemethod ClearVolumeGeometry { } {
    array set VolumeGeometry {
      Origin ""
      Dimensions ""
      Spacing ""
    }
  }

  proc WM_DELETE_WINDOW { w } {
    Dialog::enddialog $w 1
  }
}
