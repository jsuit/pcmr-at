package require BWidget
package require msgcat
package require Tk 8.5
package require ClipImage
package require Img

if {$::tcl_platform(platform) eq "unix"} {
  package require fsdialog
}

namespace import -force msgcat::*

namespace eval ImporterDialog {
  variable win
  variable options
  variable inputDir
  variable outputDir ""
  variable factorX 0
  variable factorY 0
  variable data
  variable widgets
  variable Importer ""
  variable cmdChooseDirectory
  variable checkROI 0

  if {$::tcl_platform(platform) eq "unix"} {
    set cmdChooseDirectory ttk::chooseDirectory
  } else {
    set cmdChooseDirectory tk_chooseDirectory
  }

  if {![info exists inputDir]} {
    set inputDir [pwd]
  }
  if {![info exists outputDir]} {
    set outputDir ""
  }

  variable cwd [ file normalize [ file dirname [ info script ] ] ]

  set data(progress) ""
  if { [ lsearch [ image names ] folder_yellow_16 ] == -1 } {
    image create photo folder_yellow_16 -data {
      R0lGODlhEAAQAPYAAK+KGLGMGLOOGbKNGrKMG7OMG7OOG7SOGbSPGbWPH7SQGaOFI8GLNMmJN8Se
      MMSXOcSfO9eQOuOePN6xLcOhN8WgNMahNcehNcmjNMijN8mmNcukN8ulN8umNsumN8KgOMeiOMei
      OcemPMekPselPsemPtGnN9uqNeG9K+K4LeO/LOS8LeW/LeytO/G5OeTALPTHN/XMNvTBOffQNcun
      QtGtQtCuSd6+Q922Sum8S/TCTvjETvjITvjOTe/USO7USfrTTfzaS/veTPzgTPzkTfzmUfvpXd3A
      ZurTg/rrhvLgl/nrqAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
      AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
      AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5
      BAEAAEwALAAAAAAQABAAAAeSgEyCg4SFhoeGBwgICgGIggYTKSssAgqXlwiEAC4yMDEzKi8vKioo
      A4MALScYGxwesLAdFRcgTAASJj9GSUu+v0tKFkwEERlCREW8wEtIIUwFDTVAQUPJy75HggUMODw9
      1NbKvTbbDzk7O9/h1w7bEDrp6evVPhqCCTTy++s3gwsfSJQoQWIEiYIjRFB4xLAhoUAAOw==
    }
  }

  proc GetTmpDir {} {
    global env tcl_platform

    if {$tcl_platform(platform) eq "unix"} {
      set TmpDir /tmp
    } else {
      if {[info exist env(TEMP)] && [file exists $env(TEMP)]} {
        set TmpDir $env(TEMP)
      } elseif {[info exist env(TMP)] && [file exists $env(TMP)]} {
        set TmpDir $env(TMP)
      } else {
        set TmpDir "C:/"
      }
    }
    return $TmpDir
  }

  proc DeleteImporter { } {
    variable Importer

    if {$Importer ne ""} {
      $Importer Delete
      set Importer ""
    }
  }

  proc OnStartScanEvent { } {
    variable widgets

    puts "Scanning DICOM files"
    $widgets(status,msg) configure -text [mc "Scanning DICOM files"]...
    update
  }

  proc OnStartPrepareWriteEvent { } {
    variable widgets

    puts "Prepare writing"
    $widgets(status,msg) configure -text [mc "Prepare writing"]...
    update
  }

  proc OnStartWriteEvent { } {
    variable widgets

    puts "Writing volumes"
    $widgets(status,msg) configure -text [mc "Writing volumes"]...
    update
  }

  proc OnAwakeEvent { } {
    variable data

    if { [catch {incr data(progress)} msg] } {
      puts $msg
    }
    update
  }

  proc TestEvents { } {
    variable data

    set data(progress) 0
    $Importer TestEvents
  }

  proc CreateEntryDirectory {w args} {
    array set opts {
      -mustexist 1
      -varname ""
      -labelinfo ""
      -acceptcmd ""
    }
    array set opts $args
    frame $w
    if { $opts(-labelinfo) eq "" } {
      set labeltext [mc "Directory:"]
    } else {
      set labeltext [mc "%s directory:" $opts(-labelinfo)]
    }
    ttk::label $w.lb -text $labeltext
    set wentry [ttk::entry $w.ent]
    if {$opts(-varname) ne ""} {
      set varInput "[namespace current]::$opts(-varname)"
      $wentry configure -textvariable $varInput
    }
    if {$opts(-acceptcmd) ne ""} {
      bind $wentry <Return> $opts(-acceptcmd)
    }
    button $w.btn -image folder_yellow_16 \
        -command [namespace code "ChooseDir $opts(-varname) -mustexist $opts(-mustexist) -titleinfo $opts(-labelinfo) -acceptcmd [list $opts(-acceptcmd)]" ]
    grid $w.lb -row 0 -column 0 -sticky "e"
    grid $w.ent -row 0 -column 1 -sticky "ew"
    grid $w.btn -row 0 -column 2 -sticky "w"
    grid columnconfigure $w 1 -weight 1
    return $w
  }

  proc CreateLabelCombo {w args} {
    array set opts {
      -varname ""
      -labeltext ""
      -values ""
      -command ""
    }
    array set opts $args
    frame $w
    ttk::label $w.lb -text "$opts(-labeltext):"
    ttk::combobox $w.cb -textvariable "[namespace current]::$opts(-varname)" \
        -values $opts(-values)
    if {$opts(-command) ne ""} {
      bind $w.cb <<ComboboxSelected>> [list {*}$opts(-command) %W]
    }
    grid $w.lb -row 0 -column 0 -sticky ew
    grid $w.cb -row 0 -column 1 -sticky ew
    return $w
  }

  proc OnChangeOutputDir { args } {
    CheckExportState
  }

  proc ClearStatus {} {
    variable widgets
    variable data

    $widgets(status,msg) configure -text ""
    set data(progress) ""
  }

  proc ScanSequence { } {
    variable inputDir
    variable Importer
    variable widgets
    variable checkROI
    variable win
    variable data

    set status [catch {
      $Importer SetSequenceDirectory $inputDir
      $Importer ReadInformation
      set filePNG [file join [GetTmpDir] Sample_Slice.png]
      $Importer WriteRepresentativeSlice $filePNG
    } msg]
    if {$status} {
      $win itemconfigure 0 -state disabled
      MessageDlg $win.error -message $msg -icon error -type ok
      ClearImage
    } else {
      set img [image create photo -file $filePNG]
      $widgets(canvas) configure -image $img
      $widgets(canvas) configure -boxvisible $checkROI
      CheckExportState
    }
  }

  proc DetectImporter { dir } {
    variable widgets
    variable Importer
    variable win

    DeleteImporter
    array set importer [DetectImporterFromSequence $dir]
    if { $importer(-id) eq "UNKNOWN" } {
      MessageDlg $win.error -message "Could not detect importer" \
          -icon error -type ok
      return
    }
    $widgets(lbManu) configure -text [mc "Manufacturer: %s" $importer(-id)]
    set Importer [$importer(-cmd)]
    $Importer AddObserver StartScanEvent [namespace code OnStartScanEvent]
    $Importer AddObserver StartPrepareWriteEvent [namespace code OnStartPrepareWriteEvent]
    $Importer AddObserver StartWriteEvent [namespace code OnStartWriteEvent]
    $Importer AddObserver AwakeEvent [namespace code OnAwakeEvent]

  }

  proc OnChangeInputDir { args } {
    variable inputDir
    variable Importer
    variable widgets
    variable checkROI
    variable win
    variable data

    update
    DetectImporter $inputDir
    if {$Importer ne ""} {
      set data(progress) 0
      update
      ScanSequence
      ClearStatus
    }
  }

  proc ClearImage { } {
    variable widgets

    set img [$widgets(canvas) cget -image]
    if {$img ne ""} {
      $widgets(canvas) configure -image ""
      image delete $img
    }
  }

  proc CheckExportState { } {
    variable win
    variable outputDir
    variable Importer
    variable widgets

    if {$outputDir ne "" && $Importer ne "" &&
        [$widgets(canvas) cget -image] ne ""} {
      $win itemconfigure 0 -state normal
    }
  }

  proc OnCheckROI { } {
    variable widgets
    variable checkROI
    $widgets(canvas) configure -boxvisible $checkROI
  }

  proc Show { args } {
    variable win
    variable widgets
    variable options
    variable data

    set w .dlgImport

    set options(-parent) "."
    array set options $args
    if {![winfo exists $w ]} {
      set wparent $options(-parent)
      set bg [ $wparent cget -background ]
      Dialog $w -title [ mc "Import PCMR 4D Sequence" ]  \
          -parent $wparent -modal local -cancel 1 -separator 1 \
          -background $bg -transient false
      $w add -text [ mc "Import" ] -command [namespace code OnClickOK] \
          -state disabled -background $bg
      $w add -text [ mc "Close" ]  \
          -background $bg
      set f [$w getframe]

      CreateEntryDirectory $f.inputDir \
          -varname "inputDir" -labelinfo Input -acceptcmd [namespace code OnChangeInputDir]
      #trace add variable [namespace current]::inputDir write [namespace code OnChangeInputDir]
      grid $f.inputDir -row 0 -column 0 -sticky snew
      set widgets(lbManu) [ttk::label $f.lbmanu \
                               -text [mc "Manufacturer: %s" UNKNOWN] -relief sunken]
      grid $widgets(lbManu) -row 0 -column 1 -sticky wns

      ttk::labelframe $f.fslice -text [mc "Region of Interest"]
      grid $f.fslice -row 1 -column 0 -sticky snew -columnspan 2
      set widgets(checkROI) [ttk::checkbutton $f.fslice.check  -text [mc "Apply ROI"]: \
                                 -command [namespace code OnCheckROI] \
                                 -variable "[namespace current]::checkROI" ]
      grid $f.fslice.check -row 0 -column 0 -sticky ne
      set widgets(canvas) [ClipImage $f.fslice.canvas -background white]
      puts "$widgets(canvas) -row 0 -column 0 "
      grid $widgets(canvas) -row 0 -column 1
      CreateEntryDirectory $f.outputDir \
          -varname "outputDir" -labelinfo Output -mustexist 0 \
          -acceptcmd [namespace code OnChangeOutputDir]
      grid $f.outputDir -row 2 -column 0 -sticky snew -columnspan 2

      frame $f.fstatus -bd 1 -relief ridge
      grid $f.fstatus -row 4 -column 0 -sticky snew -columnspan 2
      grid columnconfigure $f.fstatus 0 -weight 3
      grid columnconfigure $f.fstatus 1 -weight 1
      set widgets(status,msg) [ttk::label $f.fstatus.msg]
      grid $widgets(status,msg) -row 0 -column 0 -sticky snew
      ProgressBar $f.fstatus.pbar -type infinite -maximum 100 \
          -variable  "[namespace current]::data(progress)"  \
          -bd 1 -relief sunken \
          -troughcolor $bg -foreground #c3c3ff
      set widgets(status,pbar) $f.fstatus.pbar
      grid $widgets(status,pbar) -row 0 -column 1 -sticky snew
      grid columnconfigure $f 0 -weight 1
      grid rowconfigure $f 1 -weight 1
      set win $w
      wm protocol $win WM_DELETE_WINDOW [ namespace code WM_DELETE_WINDOW ]
    } else {
    }
    ClearStatus
    #$widgets(txtTrace) delete 0.0 end
    set f [$win getframe]
    set x [expr [winfo screenwidth $win]/2 - [winfo reqwidth $f]/2 \
               - [winfo vrootx [winfo parent $win]]]
    set y [expr [winfo screenheight $win]/2 - [winfo reqheight $f]/2 \
               - [winfo vrooty [winfo parent $win]]]
    $win configure -geometry +$x+$y
    $win draw
  }

  proc ChooseDir { varname args } {
    variable win
    variable cmdChooseDirectory
    variable UserSelected

    array set opts {
      -acceptcmd ""
      -mustexist 1
      -titleinfo ""
    }
    array set opts $args
    if { $opts(-titleinfo) eq "" } {
      set titletext [mc "Select directory"]
    } else {
      set titletext [mc "Select directory: %s" $opts(-titleinfo) ]
    }
    if {$varname ne ""} {
      variable $varname
      set initialdir [set $varname]
    } else {
      set initialdir [pwd]
    }
    set res [$cmdChooseDirectory -parent $win -initialdir $initialdir \
                 -mustexist $opts(-mustexist) -title $titletext ]
    update
    if {$res ne "" && $varname ne ""} {
      variable $varname
      set $varname $res
      if {$opts(-acceptcmd) ne ""} {
        #uplevel #0 $opts(-acceptcmd)
        after idle $opts(-acceptcmd)
      }
    }
  }

  proc OnClickOK { } {
    variable win
    variable inputDir
    variable outputDir
    variable widgets

    if {![file exists $inputDir] || ![file readable $inputDir]} {
      MessageDlg $win.ask -title [ mc "Status Directory" ] \
          -icon error \
          -type ok -aspect 75 \
          -message [ mc "Directory '%s' does not exists or is not readable" $inputDir ]
      return
    }
    if {[file exists $outputDir]} {
      if {![file writable $outputDir]} {
        MessageDlg $win.ask -title [mc "Status Directory"] \
            -icon error \
            -type ok -aspect 75 \
            -message [mc "Directory %s is not writable" $outputDir]
        return
      } else {
        set ans [MessageDlg $win.ask -title [mc "Confirm overwrite"] \
                     -icon warning \
                     -type okcancel -aspect 75 \
                     -message [mc "Directory '%s' already exist, overwrite?" $outputDir]]
        if {$ans == 1} {
          return
        }
      }
    } else {
      set ans [MessageDlg $win.ask -title [mc "Confirm create"] \
                   -icon warning \
                   -type okcancel -aspect 75 \
                   -message [ mc "Directory '%s' is going to be created, proceed?" $outputDir ] ]
      if { $ans == 1 } {
        return
      }

      if { 0 && [ catch { file mkdir $outputDir } msgerror ] } {
        MessageDlg $win.ask -title [mc "Status Directory"] \
            -icon error \
            -type ok -aspect 75 \
            -message [ mc "While creating '%s':\n %s" $outputDir $msgerror ]
        return
      }
    }
    update
    doImport $inputDir $outputDir
    #Dialog::enddialog $win 1
  }

  proc doImport {dirInput dirOutput} {
    variable win
    variable data
    variable cwd
    variable widgets
    variable Importer
    variable checkROI

    if {$checkROI} {
      foreach {minX minY maxX maxY} [$widgets(canvas) Box GetCoordinates] break
      $Importer SetOutputROI \
          [expr {int($minX)}] [expr {int($maxX)}] \
          [expr {int($minY)}] [expr {int($maxY)}]
    }
    set data(progress) 0
    if { [catch {$Importer WriteStudy $dirOutput} msg] } {
      MessageDlg $win.msgStatus -title "Import Status" -message $msg -icon error -type ok
    } else {
      MessageDlg $win.msgStatus -title "Import Status" \
          -message "Import was successful" -icon info -type ok
    }
    Dialog::enddialog $win 1
  }

  proc WM_DELETE_WINDOW { } {
    variable win

    Dialog::enddialog $win 1
  }
}
