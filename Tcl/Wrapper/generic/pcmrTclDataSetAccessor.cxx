#include "pcmrTclDataSetAccessor.h"
#include "vtkTclUtil.h"
#include <sstream>

BEGIN_PCMR_DECLS

TclDataSetAccessor::TclDataSetAccessor()
  : m_Interp(NULL), m_TclCommand("")
{
}

TclDataSetAccessor::~TclDataSetAccessor()
{
}

size_t TclDataSetAccessor::GetNumberOfTimeSteps()
{
  std::string script(m_TclCommand);
  script += " GetNumberOfTimeStep";
  if (Tcl_Eval(this->m_Interp, script.c_str()) == TCL_OK)
    {
    int n;
    if (Tcl_GetIntFromObj(this->m_Interp,
                          Tcl_GetObjResult(this->m_Interp), &n)==TCL_OK)
      {
      return static_cast<size_t>(n);
      }
    else
      {
      std::cerr << "TclDataSetAccessor::GetNumberOfTimeSteps error:\n" 
                << Tcl_GetString(Tcl_GetObjResult(this->m_Interp));
      return 0;
      }
    }
  else
    {
    std::cerr << "TclDataSetAccessor::GetNumberOfTimeSteps error:\n" 
              << Tcl_GetString(Tcl_GetObjResult(this->m_Interp));
    return 0;
    }
}

double TclDataSetAccessor::GetLengthOfTimeStep()
{
  std::string script(m_TclCommand);
  script += " GetLengthOfTimeStep";
  if (Tcl_Eval(this->m_Interp, script.c_str()) == TCL_OK)
    {
    double delta;
    if (Tcl_GetDoubleFromObj(this->m_Interp,
                                Tcl_GetObjResult(this->m_Interp),
                                &delta)==TCL_OK)
      {
      return delta;
      }
    else
      {
      std::cerr << "TclDataSetAccessor::GetLengthOfTimeStep error:\n" 
                << Tcl_GetString(Tcl_GetObjResult(this->m_Interp));
      return 0;
      }
    }
  else
    {
    std::cerr << "TclDataSetAccessor::GetLengthOfTimeStep error:\n" 
              << Tcl_GetString(Tcl_GetObjResult(this->m_Interp));
    return 0;
    }
}

void TclDataSetAccessor::GetTimeValues(std::vector<double>& timeValues)
{
  std::string script(m_TclCommand);
  script += " GetTimeValues";
  if (Tcl_Eval(this->m_Interp, script.c_str()) == TCL_OK)
    {
    int objc;
    Tcl_Obj **objv;
    if (Tcl_ListObjGetElements(this->m_Interp, Tcl_GetObjResult(this->m_Interp),
                               &objc, &objv)==TCL_OK)
      {
      timeValues.clear();
      double t;
      for(int i = 0; i < objc; i++)
        {
        Tcl_GetDoubleFromObj(this->m_Interp, objv[i], &t);
        timeValues.push_back(t);
        }
      }
    else
      {
      std::cerr << "TclDataSetAccessor::GetTimeValues error:\n" 
                << Tcl_GetString(Tcl_GetObjResult(this->m_Interp));
      }
    }
  else
    {
    std::cerr << "TclDataSetAccessor::GetTimeValues error:\n" 
              << Tcl_GetString(Tcl_GetObjResult(this->m_Interp));
    }
}

vtkImageData *TclDataSetAccessor::GetImage(size_t t)
{
  std::string script(m_TclCommand);
  std::stringstream ss;
  vtkImageData *img = NULL;
  ss << t;
  script += " GetVelocityImage -component FLOW -vtk yes -timestep ";
  script += ss.str();
  if (Tcl_Eval(this->m_Interp, script.c_str()) == TCL_OK)
    {
    const char *imgName = Tcl_GetString(Tcl_GetObjResult(this->m_Interp));
    int error;
    img = static_cast<vtkImageData*>(
      vtkTclGetPointerFromObject(imgName,"vtkImageData", 
                                 this->m_Interp, error));
    }
  else
    {
    std::cerr << "TclDataSetAccessor::GetImage error:\n" 
              << Tcl_GetString(Tcl_GetObjResult(this->m_Interp));
    }
  return img;
}

END_PCMR_DECLS
