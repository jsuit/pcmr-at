#ifndef __pcmrTclDataSetAccedor_h__
#define __pcmrTclDataSetAccedor_h__

#include "pcmrTemporalVelocitySource.h"
#include <vector>

struct Tcl_Interp;

BEGIN_PCMR_DECLS

class TclDataSetAccessor : public DataFlowAccessor
{
 public:
  TclDataSetAccessor();
  virtual ~TclDataSetAccessor();

  void SetTclInterpreter(Tcl_Interp *interp)
  {
    this->m_Interp = interp;
  }

  Tcl_Interp *GetTclInterpreter()
  {
    return this->m_Interp;
  }

  void SetTclDataSetCommand(const char *cmd)
  {
    this->m_TclCommand = cmd;
  }

  const char *GetTclDataSetCommand() const
  {
    return this->m_TclCommand.c_str();
  }

  virtual size_t GetNumberOfTimeSteps();
  virtual double GetLengthOfTimeStep();
  virtual void GetTimeValues(std::vector<double>& timeValues);
  virtual vtkImageData *GetImage(size_t t);
 private:
  Tcl_Interp *m_Interp;
  std::string m_TclCommand;
};

END_PCMR_DECLS

#endif
