#include "pcmrTcl.h"
#include "pcmrGDCMIO.h"
#include "pcmrSiemensImporter.h"
#include "pcmrPhilipsImporter.h"
#include "pcmrPhilipsImporterUnenhanced.h"
#include "itkTclCallback.h"
#include "itkCommand.h"
#include "pcmrEvents.h"

class PCMR_ClientData
{
public:
  pcmr::BaseImporter::Pointer ptr;
  Tcl_Command token;
};

pcmr::BaseImporter::Pointer GetBaseImporterFromClientData(void *clientData)
{
  PCMR_ClientData *wrapper = static_cast<PCMR_ClientData*>(clientData);
  return wrapper ? wrapper->ptr : NULL;
}

Tcl_Command GetCommandTokenFromClientData(void *clientData)
{
  PCMR_ClientData *wrapper = static_cast<PCMR_ClientData*>(clientData);
  return wrapper ? wrapper->token : NULL;
}

int PCMR_ImporterHandler(ClientData     clientData,
                         Tcl_Interp    *interp,
                         int            objc,
                         Tcl_Obj *const objv[])
{
  pcmr::BaseImporter::Pointer importer = GetBaseImporterFromClientData(clientData);

  const char* cmd = Tcl_GetString(objv[0]);
  if (!importer)
    {
    Tcl_AppendResult(interp, "importer command '", cmd,
                     "' is corrupted: clientData is NULL:",
                     NULL);
    return TCL_ERROR;
    }
  if (objc < 2)
    {
    Tcl_AppendResult(interp, "wrong # of arguments, should be\n",
                     "    ", cmd, " AddObserver\n",
                     "    ", cmd, " Delete\n",
                     "    ", cmd, " SetSequenceDirectory\n",
                     "    ", cmd, " GetSequenceDirectory\n",
                     "    ", cmd, " ReadInformation\n",
                     "    ", cmd, " SetOutputROI minX maxX minY maxY\n",
                     "    ", cmd, " WriteRepresentativeSlice filePNG\n",
                     "    ", cmd, " WriteStudy studyDirectory\n",
                     "    ", cmd, " GetStatus",
                     NULL);
        return TCL_ERROR;
    }
  const char* subcommand = Tcl_GetString(objv[1]);
  pcmr::StatusType status;

  if (!strcmp(subcommand, "AddObserver"))
    {
    if (objc != 4)
      {
      Tcl_AppendResult(interp, "wrong # of arguments, should be\n",
                       "    ", cmd, "  AddObserver EventName Script",
                       NULL);
      return TCL_ERROR;
      }
    const char* eventName = Tcl_GetString(objv[2]);
    Tcl_Obj *objScript = objv[3];
    itk::CStyleCommand::Pointer itkCommand = itk::CStyleCommand::New();
    /* StartScanEvent */
    if (!strcmp( "StartScanEvent", eventName)) 
      {
      importer->AddObserver(pcmr::StartScanEvent(), itkCommand);
      }
    /* EndScanEvent */
    else if (!strcmp( "EndScanEvent", eventName)) 
      {
      importer->AddObserver(pcmr::EndScanEvent(), itkCommand);
      }
    /* StartPrepareWriteEvent */
    else if (!strcmp( "StartPrepareWriteEvent", eventName)) 
      {
      importer->AddObserver(pcmr::StartPrepareWriteEvent(), itkCommand);
      }
    /* EndPrepareWriteEvent */
    else if (!strcmp( "EndPrepareWriteEvent", eventName)) 
      {
      importer->AddObserver(pcmr::EndPrepareWriteEvent(), itkCommand);
      }
    /* StartPremaskEvent */
    else if (!strcmp( "StartPremaskEvent", eventName)) 
      {
      importer->AddObserver(pcmr::StartPremaskEvent(), itkCommand);
      }
    /* EndPremaskEvent */
    else if (!strcmp( "EndPremaskEvent", eventName)) 
      {
      importer->AddObserver(pcmr::EndPremaskEvent(), itkCommand);
      }
    /* StartWriteEvent */
    else if (!strcmp( "StartWriteEvent", eventName)) 
      {
      importer->AddObserver(pcmr::StartWriteEvent(), itkCommand);
      }
    /* EndWriteEvent */
    else if (!strcmp( "EndWriteEvent", eventName)) 
      {
      importer->AddObserver(pcmr::EndWriteEvent(), itkCommand);
      }
    /* AwakeEvent */
    else if (!strcmp( "AwakeEvent", eventName)) 
      {
      importer->AddObserver(pcmr::AwakeEvent(), itkCommand);
      }
    /* DeleteEvent */
    else if (!strcmp("DeleteEvent", eventName)) 
      {
      importer->AddObserver(itk::DeleteEvent(), itkCommand);
      }
    else 
      {
      Tcl_AppendResult( interp, "unrecognized event '", eventName,
                      "' must be: StartScanEvent, EndScanEvent, StartPrepareWriteEvent, EndPrepareWriteEvent, StartPremaskEvent, EndPremaskEvent, StartWriteEvent, EndWriteEvent or AwakeEvent", NULL);
      return TCL_ERROR;
      }
    itk::TclCallback *cb = new itk::TclCallback(interp, objScript);
    itkCommand->SetClientData(cb);
    itkCommand->SetCallback(itk::TclCallback::Execute);
    itkCommand->SetConstCallback(itk::TclCallback::ConstExecute);
    itkCommand->SetClientDataDeleteCallback(itk::TclCallback::Delete);
    return TCL_OK;
    }
  else if (!strcmp(subcommand, "Delete"))
    {
    if (objc != 2)
      {
      Tcl_AppendResult(interp, "wrong # of arguments, should be\n",
                       "    ", cmd, " Delete",
                       NULL);
      return TCL_ERROR;
      }
    Tcl_Command token = GetCommandTokenFromClientData(clientData);
    //printf("A destruir command %s desde token %p\n", cmd, token); 
    return Tcl_DeleteCommandFromToken(interp, token);
    }
  else if (!strcmp(subcommand, "TestEvents"))
    {
    if (objc != 2)
      {
      Tcl_AppendResult(interp, "wrong # of arguments, should be\n",
                       "    ", cmd, " TestAwakeEvent",
                       NULL);
      return TCL_ERROR;
      }
    importer->TestEvents();
    return TCL_OK;
    }
  else if (!strcmp(subcommand, "SetSequenceDirectory"))
    {
    if (objc != 3)
      {
      Tcl_AppendResult(interp, "wrong # of arguments, should be\n",
                       "    ", cmd, " SetSequenceDirectory dir",
                       NULL);
      return TCL_ERROR;
      }
    importer->SetSequenceDirectory(Tcl_GetString(objv[2]));
    return TCL_OK;
    }
  else if (!strcmp(subcommand, "GetSequenceDirectory"))
    {
    if (objc != 2)
      {
      Tcl_AppendResult(interp, "wrong # of arguments, should be\n",
                       "    ", cmd, " GetSequenceDirectory",
                       NULL);
      return TCL_ERROR;
      }
    char *dir = const_cast<char*>(importer->GetSequenceDirectory().c_str());
    Tcl_SetResult(interp, dir, TCL_STATIC);
    return TCL_OK;
    }
  else if (!strcmp(subcommand, "ReadInformation"))
    {
    if (objc != 2)
      {
      Tcl_AppendResult(interp, "wrong # of arguments, should be\n",
                       "    ", cmd, " ReadInformation",
                       NULL);
      return TCL_ERROR;
      }
    status = importer->ReadInformation();
    }
  else if (!strcmp(subcommand, "SetOutputROI"))
    {
    if (objc != 6)
      {
      Tcl_AppendResult(interp, "wrong # of arguments, should be\n",
                       "    ", cmd, " SetOutputROI minX maxX minY maxY",
                       NULL);
      return TCL_ERROR;
     }
    int ROI[4];
    const char* nameROI[] = {"minX", "maxX", "minY", "maxY"};
    for(int i = 0; i < 4; i++)
      {
      if (Tcl_GetIntFromObj(interp, objv[2+i], ROI+i) != TCL_OK )
        {
        // unable to retrieve integer value
        Tcl_AppendResult(interp, "while converting '",
                         Tcl_GetString(objv[2+i]), "' to integer for argument ",
                         nameROI[i], NULL);        
        return TCL_ERROR;
        }
      if (ROI[i] < 0)
        {
        // unable to retrieve integer value
        Tcl_AppendResult(interp, "invalid ROI limit value '",
                         Tcl_GetString(objv[2+i]), "', must positive ",
                         NULL);        
        return TCL_ERROR;
        }
      }
    status = importer->SetOutputROI(static_cast<size_t>(ROI[0]),
                                    static_cast<size_t>(ROI[1]),
                                    static_cast<size_t>(ROI[2]),
                                    static_cast<size_t>(ROI[3]));
    }
  else if (!strcmp(subcommand, "WriteRepresentativeSlice"))
    {
    if (objc != 3)
      {
      Tcl_AppendResult(interp, "wrong # of arguments, should be\n",
                       "    ", cmd, " WriteRepresentativeSlice filePNG",
                       NULL);
      return TCL_ERROR;
      }
    status = importer->WriteRepresentativeSlice(Tcl_GetString(objv[2]));
    }
  else if (!strcmp(subcommand, "WriteStudy"))
    {
     if (objc != 3)
      {
      Tcl_AppendResult(interp, "wrong # of arguments, should be\n",
                       "    ", cmd, " WriteStudy studyDirectory",
                       NULL);
      return TCL_ERROR;
      }
    status = importer->WriteStudy(Tcl_GetString(objv[2]));
   }
  else if (!strcmp(subcommand, "GetStatus"))
    {
     if (objc != 2)
      {
      Tcl_AppendResult(interp, "wrong # of arguments, should be\n",
                       "    ", cmd, " GetStatus",
                       NULL);
      return TCL_ERROR;
      }
    status = importer->GetStatus();
    Tcl_SetObjResult(interp, Tcl_NewIntObj(status));
    return TCL_OK;
    }
  else
    {
    Tcl_AppendResult(interp, "wrong subcommand '", subcommand, ", should be\n",
                     "    ", cmd, " AddObserver\n",
                     "    ", cmd, " Delete\n",
                     "    ", cmd, " SetSequenceDirectory\n",
                     "    ", cmd, " GetSequenceDirectory\n",
                     "    ", cmd, " ReadInformation\n",
                     "    ", cmd, " SetOutputROI minX maxX minY maxY\n",
                     "    ", cmd, " WriteRepresentativeSlice filePNG\n",
                     "    ", cmd, " WriteStudy studyDirectory\n",
                     "    ", cmd, " GetStatus",
                     NULL);
    return TCL_ERROR;
    }
  if (status != pcmr::OK)
    {
    Tcl_AppendResult(interp, "while executing '",
                     cmd, " ", subcommand, "'\n",
                     pcmr::GetStatusDescription(status),
                     NULL);
    return TCL_ERROR;
    }
  else
    {
    return TCL_OK;
    }
}

void PCMR_DeleteImporterHandler(ClientData clientData)
{
  //printf("PCMR_DeleteImporterHandler %p\n", clientData);
  PCMR_ClientData *wrapper = static_cast<PCMR_ClientData*>(clientData);
  delete wrapper;
}

int PCMR_CreateImporterCommand(Tcl_Interp      *interp,
                               PCMR_ClientData *clientData)
{
  static int icmd = 0;

  if ( !clientData ) {
  Tcl_AppendResult(interp,
                   "could not create command from NULL importer object",
                   NULL );
    return TCL_ERROR;
  }
  char buffer[256];
  sprintf(buffer, "Importer%i", icmd++);
  Tcl_Command token =
    Tcl_CreateObjCommand(interp, buffer,
                         PCMR_ImporterHandler,
                         static_cast<ClientData>(clientData),
                         PCMR_DeleteImporterHandler);
  if (token) 
    {
    //importer->Register();
    //printf("Creado %s con token %p\n", buffer, token);
    clientData->token = token;
    Tcl_SetResult(interp, buffer, TCL_VOLATILE);
    return TCL_OK;
    }
  else
    {
    clientData->token = NULL;
    Tcl_AppendResult(interp, "fatal error: failed Tcl_CreateObjCommand",
                     NULL);
    return TCL_ERROR;
  }
}

int PCMR_CreateSiemensImporterTcl(ClientData clientData,
                                  Tcl_Interp *interp,
                                  int objc, Tcl_Obj *const objv[])
{
  const char* cmd = Tcl_GetString(objv[0]);
  if (objc != 1)
    {
    Tcl_AppendResult(interp, "wrong # of arguments, should be\n",
                     "    ", cmd, NULL);
    return TCL_ERROR;
    }
  PCMR_ClientData *cd = new PCMR_ClientData;
  cd->ptr = pcmr::Siemens::Importer::New();
  int status = PCMR_CreateImporterCommand(interp, cd);
  if (status != TCL_OK)
    {
    delete cd;
    }
  return status;
}

int PCMR_CreatePhilipsEnhancedImporterTcl(ClientData clientData,
                                          Tcl_Interp *interp,
                                          int objc, Tcl_Obj *const objv[])
{
  const char* cmd = Tcl_GetString(objv[0]);
  if (objc != 1)
    {
    Tcl_AppendResult(interp, "wrong # of arguments, should be\n",
                     "    ", cmd, NULL);
    return TCL_ERROR;
    }
  PCMR_ClientData *cd = new PCMR_ClientData;
  cd->ptr = pcmr::Philips::Importer::New();
  int status = PCMR_CreateImporterCommand(interp, cd);
  if (status != TCL_OK)
    {
    delete cd;
    }
  return status;  
}

int PCMR_CreatePhilipsUnenhancedImporterTcl(ClientData clientData,
                                            Tcl_Interp *interp,
                                            int objc, Tcl_Obj *const objv[])
{
  const char* cmd = Tcl_GetString(objv[0]);
  if (objc != 1)
    {
    Tcl_AppendResult(interp, "wrong # of arguments, should be\n",
                     "    ", cmd, NULL);
    return TCL_ERROR;
    }
  PCMR_ClientData *cd = new PCMR_ClientData;
  cd->ptr = pcmr::Philips::ImporterUnenhanced::New();
  int status = PCMR_CreateImporterCommand(interp, cd);
  if (status != TCL_OK)
    {
    delete cd;
    }
  return status;  
}

int PCMR_DetectImporterFromSequenceTcl(ClientData clientData,
                                       Tcl_Interp *interp,
                                       int objc, Tcl_Obj *const objv[])
{
  const char* cmd = Tcl_GetString(objv[0]);
  if (objc != 2)
    {
    Tcl_AppendResult(interp, "wrong # of arguments, should be\n",
                     "    ", cmd, " path_to_dir", NULL);
    return TCL_ERROR;
    }
  pcmr::ImporterType importerType = pcmr::UNKNOWN;
  pcmr::StatusType status = DetectImporterTypeAtDirectory( Tcl_GetString( objv[1] ), importerType );
  if ( status != pcmr::OK )
    {
    Tcl_AppendResult( interp, pcmr::GetStatusDescription( status ), NULL );
    return TCL_ERROR;
    }
  switch ( importerType )
    {
    case pcmr::SIEMENS:
      Tcl_AppendResult( interp, "-id SIEMENS -cmd SiemensImporter", NULL );
      break;
    case pcmr::PHILIPS_ENHANCED:
      Tcl_AppendResult( interp, "-id {PHILIPS ENHANCED} -cmd PhilipsEnhancedImporter", NULL );
      break;
    case pcmr::PHILIPS_UNENHANCED:
      Tcl_AppendResult( interp, "-id {PHILIPS UNENHANCED} -cmd PhilipsUnenhancedImporter", NULL );
      break;
    default:
      Tcl_AppendResult( interp, "-id UNKNOWN -cmd {}", NULL );
      break;
    }
  return TCL_OK;
}

int PCMR_ImporterTcl_Init(Tcl_Interp *interp)
{
  Tcl_CreateObjCommand(interp, "SiemensImporter",
                       PCMR_CreateSiemensImporterTcl, NULL, NULL);
  Tcl_CreateObjCommand(interp, "PhilipsUnenhancedImporter",
                       PCMR_CreatePhilipsUnenhancedImporterTcl, NULL, NULL);
  Tcl_CreateObjCommand(interp, "PhilipsEnhancedImporter",
                       PCMR_CreatePhilipsEnhancedImporterTcl, NULL, NULL);
  Tcl_CreateObjCommand(interp, "DetectImporterFromSequence",
                       PCMR_DetectImporterFromSequenceTcl, NULL, NULL);
  return TCL_OK;
}
