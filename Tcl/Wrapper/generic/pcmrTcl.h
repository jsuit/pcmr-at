#ifndef __pcmrTcl_h
#define __pcmrTcl_h

#include "tcl.h"
#include "pcmr4dtcl_Export.h"

extern "C" {
  int PCMR4DTCL_EXPORT Pcmr4d_Init(Tcl_Interp *interp);
}

int PCMR_ImporterTcl_Init(Tcl_Interp *interp);
int PCMR_FlowQuantifyContourTcl_Init(Tcl_Interp *interp);

#endif

