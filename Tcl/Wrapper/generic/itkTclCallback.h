#ifndef __itkTclCallBack_h
#define __itkTclCallBack_h

#include "itkObject.h"
#include "tcl.h"
#include <boost/thread/mutex.hpp>

namespace itk {
class TclCallback 
{
public:
  TclCallback(Tcl_Interp *interp, Tcl_Obj *script)
  {
    this->m_Interp = interp;
    this->m_ObjScript = script;
    Tcl_IncrRefCount(this->m_ObjScript);
  };

  ~TclCallback( )
  {
    if (this->m_ObjScript)
      Tcl_DecrRefCount(this->m_ObjScript);
  }

  /*
    This mutex is needed because Tcl_EvalObjEx could be invoked from
    different threads. In other case we can reach the error message:

    TclStackFree: incorrect freePtr. Call out of sequence?

    See the following thread

    http://tcl.2662.n7.nabble.com/tcl-Bugs-1909647-TclStackFree-incorrect-freePtr-Call-out-of-sequence-td38845.html

    where the problem is mentioned.
   */
  static boost::mutex eval_mutex;

  static void Execute(itk::Object *o, const itk::EventObject &e,
                       void* ClientData)
  {
    /*TclCallback *_this = static_cast<TclCallback*>(ClientData);
    Tcl_EvalObjEx(_this->m_Interp, _this->m_ObjScript, TCL_EVAL_GLOBAL);
    */
    ConstExecute(o, e, ClientData);
  }
  
  static void ConstExecute(const itk::Object *o, const itk::EventObject &e,
                           void* ClientData)
  {
    eval_mutex.lock();
    TclCallback *_this = static_cast<TclCallback*>(ClientData);
    Tcl_EvalObjEx( _this->m_Interp, _this->m_ObjScript, TCL_EVAL_GLOBAL);
    eval_mutex.unlock();
  }
  
  static void Delete(void* ClientData)
  {
    TclCallback *_this = static_cast<TclCallback*>(ClientData);
    //printf("voy a destruir TclCallback %p\n", _this);
    delete _this;
  }
private:
  Tcl_Interp *m_Interp;
  Tcl_Obj *m_ObjScript;
};

}

#endif
