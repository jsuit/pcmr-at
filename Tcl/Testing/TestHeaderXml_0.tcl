lappend auto_path ../lib

package require HeaderXml

set header [HeaderXml %AUTO%]

$header configure -xmlfile sample_header_01.xml

foreach p [HeaderXml GetGlobalProperties] {
  puts "$p : [$header Get$p]"
}

foreach p [$header GetExtendedProperties] {
  puts "$p : [$header GetExtendedProperty $p]"
}

$header configure -xmlfile sample_header.xml
foreach p [HeaderXml GetGlobalProperties] {
  puts "$p : [$header Get$p]"
}

