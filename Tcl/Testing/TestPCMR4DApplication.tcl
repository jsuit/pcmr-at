set trace_pcmr4d_load 1

set __cwd__ [file normalize [file dirname [info script]]]

set __testing__ [expr {[lrange [file split $__cwd__] end-3 end] eq {pcmr trunk Tcl Testing}}]

puts [lrange $__cwd__ end-3 end]

if {$tcl_platform(platform) eq "unix" && $__testing__} {
  lappend auto_path /usr/local/src/TCL/tcllib/tcllib/modules /usr/local/src/TCL/tcllib/tklib/modules
  set auto_path [linsert $auto_path 0 [file normalize ../lib]]
}

lappend auto_path /usr/local/lib /usr/local/lib/tcltk

wm state . withdrawn
package require Img
package require splash
set wsplash [splash::new -imgfile [file join $__cwd__ Pcmr4DSplash.png]]

package require PCMR4DWidgets

if {[tk windowingsystem] eq "aqua"} {
    ttk::style theme use aqua
} else {
    package require ttk_themes

    if {$tcl_platform(platform) eq "unix"} {
	#TKUtil::ChangeTheme clam
	ttk::style theme use clammod
    }
}


set mainApp [PCMR4DApplication GetInstance]
wm title $mainApp "PCMR 4D Explorer"

# REVIEW: if the following update is removed then some errors like the following are generated. This is not a critical error.
if {0} {
ERROR: In /usr/local/src/Medical/Kitware/git/VTK/Rendering/OpenGL/vtkOpenGLTexture.cxx, line 200
vtkOpenGLTexture (0xa64d650): No scalar values found for texture input!
}

update

if {0 && $__testing__} {
  $mainApp OpenDataSet [file join $env(HOME) Data/pcmr/Studies/4DFLOW_SampleDataset_Aorta_3D_24SL]
}
destroy $wsplash
