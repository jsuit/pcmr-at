package require vtk

vtkGenericDataObjectReader reader

reader SetFileName [lindex $argv 0]

vtkImageMarchingCubes iso
iso SetInputConnection [reader GetOutputPort]
iso SetValue 0 0.9999

if {[file extension [lindex $argv 1]] eq ".stl"} {
  vtkSTLWriter writer
  writer SetFileTypeToBinary
} else {
  vtkGenericDataObjectWriter writer
}

writer SetInputConnection [iso GetOutputPort]
writer SetFileName [lindex $argv 1]
writer Update

puts BYE!
exit
