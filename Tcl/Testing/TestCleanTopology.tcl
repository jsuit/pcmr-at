package require vtk

if {[file extension [lindex $argv 0]] eq ".stl"} {
  vtkSTLReader reader
} else {
  vtkGenericDataObjectReader reader
}
reader SetFileName [lindex $argv 0]

vtkTriangleFilter triangulate
triangulate SetInputConnection [reader GetOutputPort]

vtkCleanPolyData cleanPoly
cleanPoly SetInputConnection [triangulate GetOutputPort]


vtkPolyDataNormals normals
normals SetInputConnection [cleanPoly GetOutputPort]
normals AutoOrientNormalsOn

if {[file extension [lindex $argv 1]] eq ".stl"} {
  vtkSTLWriter writer
} else {
  vtkGenericDataObjectWriter writer
}
writer SetInputConnection [normals GetOutputPort]
writer SetFileName [lindex $argv 1]
writer Update

puts BYE!
exit
