package require gridplus

namespace import gridplus::*

text .txt

set w .txt

scale .sc -from 0 -to 25 -tick 0.1 -orient horizontal

gridplus entry .options -title Options { 
   {"Resample Factor"     .rfactor + 10 !int} 
   {"Reinjection Rate"   .rrate 10 !int} 
   {"Trace Length"    .tlength 10 !int}
} 

gridplus button .buttons { 
   {Apply .apply ~Apply !} {Exit .exit ~Exit}
} 

gridplus layout .left { 
   .options
   .buttons:ew
} 

gridplus layout .main [string map "%w $w" { 
   .left+nw %w:snew
      |      .sc=ew
}]

gridplus pack .main -resize xy

proc Apply { } {
  puts "buttons,apply"
}

proc Exit { } {
  exit
}
