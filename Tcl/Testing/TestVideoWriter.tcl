package require vtk

vtkImageCanvasSource2D source2d
source2d SetScalarTypeToUnsignedChar
source2d SetNumberOfScalarComponents 3
source2d SetExtent 0 100 0 100 0 0

vtkOggTheoraWriter writer
writer SetQuality 2
writer SetInputConnection [source2d GetOutputPort]
writer SetFileName "test.mpeg"
writer Start

for {set i 0} {$i < 100} {incr i} {
  source2d SetDrawColor 0 0 0 1
  source2d FillBox 0 100 0 100
  source2d SetDrawColor 255 0 0 1
  source2d FillBox $i 20 10 20
  source2d Update
  writer Write
}

writer End
exit
