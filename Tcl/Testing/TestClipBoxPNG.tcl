lappend auto_path ../lib

package require ClipBox
package require Img

set png [lindex $argv 0]
set img [image create photo -file $png]

toplevel .t
canvas .t.c -width [image width $img] -height [image height $img]
.t.c create image 0 0 -image $img -anchor nw
pack .t.c