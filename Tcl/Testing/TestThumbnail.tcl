lappend auto_path ../lib
package require ITKUtil
package require VolumeOrienter
package require ImageThumbnailManager
package require PCMR4DWidgets
package require BWidget

ITKUtil::LoadITKTcl

PCMR4DDataSet ds
ds Open ../../../../biomedical/data/Studies/4DFLOW_SampleDataset_Aorta_3D_24SL
MedicalVolumeOrienter volOrienter
volOrienter SetOrientation [ds GetImageOrientation]

ImageThumbnailManager thumber -thumbplane "Sagittal" -flipplane "Axial" -size 128
thumber SetVolumeOrienter volOrienter

#ScrolledFrame::ScrolledFrame .sf -xscrollcommand {.hs set} -fill y ;# try both, x, y or none
ScrollableFrame .sf -xscrollcommand {.hs set}
set f [.sf getframe]
ttk::scrollbar .hs -command {.sf xview} -orient horizontal

for {set t 0} {$t < [ds GetNumberOfTimeStep]} {incr t} {
  set thumbName "Thumbnail_$t"
  set thumbImg [thumber GetThumbnail $thumbName]
  if {$thumbImg eq ""} {
    ds ChangeTimeStep $t
    set velocity [ds GetVelocityImage -vtk yes]
    set thumbImg [thumber MakeThumbnail $velocity $thumbName]
  }
  set tvalue [expr {([ds GetLengthOfTimeStep]*$t)/1000}]
  label $f.thumb$t -image $thumbImg -highlightthickness 1 -compound top \
      -text [format "%.3f" $tvalue]
  bind $f.thumb$t <1> [string map "%t $t" {OnClickTS %W %t}]
  grid $f.thumb$t -row 0 -column $t -sticky snew  -ipadx 1 -ipady 1
}

proc OnClickTS { w t } {
  set f [winfo parent $w]
  set t0 [ds GetCurrentTimeStep]
  $f.thumb$t0 configure -bd 1 -relief flat
  $w configure -bd 2 -relief sunken
  ds ChangeTimeStep $t
}

ds ChangeTimeStep 0
$f.thumb0 configure -bd 1 -relief sunken

grid .sf -row 0 -column 0 -sticky ew
grid .hs -row 1 -column 0 -sticky new
grid columnconfigure . 0 -weight 1
grid rowconfigure . 2 -weight 1
#grid configure $f
after idle {.sf configure -height [winfo reqheight [.sf getframe]]}
puts "STATUS: OK"
