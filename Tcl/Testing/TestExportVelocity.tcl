if {$argc != 3} {
  puts $argc
  puts "Usage: $argv0 study ts output.vtk"
  exit
}

set __cwd__ [file normalize [file dirname [info script]]]
set __testing__ [expr {[lrange [file split $__cwd__] end-3 end] eq {pcmr trunk Tcl Testing}}]

if {$tcl_platform(platform) eq "unix" && $__testing__} {
  lappend auto_path /usr/local/src/TCL/tcllib/tcllib/modules /usr/local/src/TCL/tcllib/tklib/modules
  set auto_path [linsert $auto_path 0 [file normalize ../lib]]
}

package require PCMR4DWidgets

PCMR4DDataSet ds

ds Open [lindex $argv 0]
set ts [lindex $argv 1]

set img [ds GetVelocityImage -timestep $ts -component FLOW -vtk yes]
vtkGenericDataObjectWriter writer
writer SetFileName [lindex $argv 2]
writer SetInputData $img
writer Update
puts "Output written to [lindex $argv 2]"

exit
