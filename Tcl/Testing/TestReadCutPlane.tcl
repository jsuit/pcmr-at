package require tdom

set path "TestCutPlane.xml"

set fd [open $path]
set xmlBuffer [string trim [read $fd]]
close $fd

set doc [dom parse $xmlBuffer]
set root [$doc documentElement]
set name [$root getAttribute name]
puts "name = $name"

set NumberOfHandles [$doc selectNodes string(/QuantifyPlane/NumberOfHandles)]
puts "NumberOfHandles = $NumberOfHandles"

set HandleSize [$doc selectNodes string(/QuantifyPlane/HandleSize)]
puts "HandleSize = $HandleSize"

set nodeContours [$root selectNodes Contours]

foreach nodeSpline [$nodeContours childNodes] {
  puts "Spline [$nodeSpline getAttribute index]"
  foreach nodePoint [$nodeSpline childNodes] {
    set coord [$nodePoint selectNodes {string()}]
    puts "  Point [$nodePoint getAttribute index] $coord"
  }
}