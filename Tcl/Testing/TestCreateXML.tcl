package require tdom

set doc [dom createDocument QuantifyPlane]
set root [$doc documentElement]
$root setAttribute version 1.0
$root setAttribute name "base"


set subnode [$doc createElement NumberOfHandles]
$subnode appendChild [$doc createTextNode "10"]
$root appendChild $subnode

set subnode [$doc createElement HandleSize]
$subnode appendChild [$doc createTextNode "3.5"]
$root appendChild $subnode

set nodeContour [$doc createElement Contours]
$root appendChild $nodeContour

set nodeS [$doc createElement Spline]
$nodeContour appendChild $nodeS

set nodeP [$doc createElement Point]
$nodeP appendChild [$doc createTextNode {0.1 0.1 0.1}]
$nodeS appendChild $nodeP

set nodeP [$doc createElement Point]
$nodeP appendChild [$doc createTextNode {0.2 0.2 0.2}]
$nodeS appendChild $nodeP


set nodeS [$doc createElement Spline]
$nodeContour appendChild $nodeS

set nodeP [$doc createElement Point]
$nodeP appendChild [$doc createTextNode {0.1 0.1 0.1}]
$nodeS appendChild $nodeP

set nodeP [$doc createElement Point]
$nodeP appendChild [$doc createTextNode {0.2 0.2 0.2}]
$nodeS appendChild $nodeP


puts [$root asXML]