lappend auto_path ../lib

package require ITKUtil
package require VolumeOrienter
package require PCMR4DWidgets

ITKUtil::LoadITKTcl

PCMR4DDataSet ds
if {[llength $argv]} {
  ds Open [lindex $argv 0]
} else {
  ds Open [file normalize ~/Data/pcmr/Studies/4DFLOW_SampleDataset_Aorta_3D_24SL]
}
MedicalVolumeOrienter volOrienter
# volOrienter SetOrientation [ds GetImageOrientation]
ImageRenderWidget .irw -volumeorienter volOrienter
set img [ds GetVelocityImage -component MAG -timestep 3 -vtk yes]

.irw SetImageData $img -type MAG
#.irw SetImageData [ds GetVelocityImage -component FLOW -timestep 3 -vtk yes] -type velocity
.irw SetImageOrientation [ds GetImageOrientation]
.irw AlignToView Coronal
grid .irw -row 0 -column 0 -sticky snew
grid rowconfigure . 0 -weight 1
grid columnconfigure . 0 -weight 1
