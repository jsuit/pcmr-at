package require vtk
source TestHelper.tcl

package require PCMR4DWidgets

proc DumpStatTable { stats name } {
  set outputData1 [stats GetOutput 0]
  set outputMetaDS1 [stats GetOutputDataObject 1]
  set outputPrimary1 [$outputMetaDS1 GetBlock 0]
  set outputDerived1 [$outputMetaDS1 GetBlock 1]
  set outputTest1 [stats GetOutput 2]
  
  puts "Statistics for $name"
  for {set c 0} {$c < [$outputPrimary1 GetNumberOfColumns]} {incr c} {
    puts -nonewline "[format %10s [$outputPrimary1 GetColumnName $c]] "
  }
  puts ""
  for {set c 0} {$c < [$outputPrimary1 GetNumberOfColumns]} {incr c} {
    set colData [$outputPrimary1 GetColumn $c]
    if {[$colData GetDataTypeAsString] eq "string"} {
      set value [format "%10s " [$colData GetValue 0]]
    } else {
      set value [format "%10f " [$colData GetTuple1 0]]
    }
    puts -nonewline "$value "
  }
  puts ""
}

if {$argc != 2} {
  puts "usage: $argv0 study ts"
  exit
}

set pathStudy [lindex $argv 0]
set ts [lindex $argv 1]

PCMR4DDataSet ds
ds Open $pathStudy

set imgX [ds GetVelocityImage -component VX -vtk yes -timestep $ts]
set imgY [ds GetVelocityImage -component VY -vtk yes -timestep $ts]

vtkImageClip imgClip
imgClip SetInputData $imgX
#imgClip SetOutputWholeExtent 56 62 26 31 12 12
imgClip SetOutputWholeExtent 75 81 63 68 12 12
imgClip ClipDataOn

#imgClip Update
#puts [imgClip Print]
#puts [[imgClip GetOutput] Print]

vtkDataObjectToTable toTable
toTable SetInputConnection [imgClip GetOutputPort]
toTable Update

puts [[toTable GetOutput] Print]
puts "GetNumberOfColumns(toTable) = [[toTable GetOutput] GetNumberOfColumns]"
puts "GetNumberOfRows(toTable) = [[toTable GetOutput] GetNumberOfRows]"
puts "GetColumnName(toTabke,0) = [[toTable GetOutput] GetColumnName 0]"

vtkDescriptiveStatistics stats
stats SetInputConnection [toTable GetOutputPort]
stats AddColumn "scalars"
stats SetLearnOption 1
stats SetDeriveOption 1
stats SetTestOption 1
stats Update

DumpStatTable stats VX

imgClip SetInputData $imgY
stats Update

DumpStatTable stats VY

puts "bye!"
exit
