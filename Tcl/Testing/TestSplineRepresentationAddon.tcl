package require vtk

package require vtkTclAddon

proc DumpHandles { spline } {
  set coords [$spline GetHandlePositions]
  for {set i 0} {$i < [$coords GetNumberOfTuples]} {incr i} {
    puts "p($i) [$coords GetTuple3 $i]"
  }
}

proc TranslateHandles { spline dx dy dz } {
  set coords [$spline GetHandlePositions]
  for {set i 0} {$i < [$coords GetNumberOfTuples]} {incr i} {
    foreach {px py pz} [$coords GetTuple3 $i] break
    set px [expr {$px+$dx}]
    set py [expr {$py+$dy}]
    set pz [expr {$pz+$dz}]
    vtkSplineRepresentationAddon $spline \
        ChangeHandlePosition $i $px $py $pz
  }
  $spline BuildRepresentation
}

set spline [vtkSplineRepresentation New]
$spline ProjectToPlaneOff
puts "Initial positions:"
DumpHandles $spline 
TranslateHandles $spline 1 0 1
puts "Positions moved:"
DumpHandles $spline 

exit
