package require Tk
package require snit

snit::type ClipBox {
  typevariable CURSORS                        ;# Cursors to use while resizing

  variable xy {}                              ;# Coordinates of print box
  variable bxy {}                             ;# Button down location
  variable bdown 0                            ;# Button is down flag
  variable minSize 150                        ;# Minimum size of print box
  variable debug 0
  
  option -minsize -default 50
  option -grabsize -default 5                 ;# Size of "grab" area
  option -canvas -default "" -readonly yes
  option -visible -default no -configuremethod _ConfVisible
  option -debug -default no
  
  option -fill -default cyan -configuremethod _ConfFill
  option -stipple -default gray25 -configuremethod _ConfStipple
  option -outline -default gray90 -configuremethod _ConfOutline

  typeconstructor {
    if {$::tcl_platform(platform) == "windows"} {
      array set CURSORS {
        L size_we      R size_we
        B size_ns      T size_ns
        TL size_nw_se  BR size_nw_se
        TR size_ne_sw  BL size_ne_sw
      }
    } else {
      array set CURSORS {
        L sb_h_double_arrow      R sb_h_double_arrow
        B sb_v_double_arrow      T sb_v_double_arrow
        TL top_left_corner       BR bottom_right_corner
        TR top_right_corner      BL bottom_left_corner
      }
    }
  }

  constructor { args } {
    $self configurelist $args
    if {![winfo exist $options(-canvas)]} {
      error "invalid -canvas value, bad window path name '$options(-canvas)'"
    }
    append ::debug "\n" "ismapped = [winfo ismapped $options(-canvas)]"

    bind $options(-canvas) <Configure> [mymethod _OnConfigure]
  }
  
  destructor {
    catch {
      $options(-canvas) delete pBox
    }
  }

  method _ConfVisible { o v } {
    if {![string is boolean $v]} {
      error "wrong boolean value '$v'"
    }
    if {$v} {
      $self _Show
    } else {
      $self _Hide
    }
    set options(-visible) $v
  }

  method _ConfFill {o v} {
    if {[winfo exist $options(-canvas)]} {
      $options(-canvas) itemconfigure pBoxx -fill $v
    }
    set options($o) $v
  }

  method _ConfStipple {o v} {
    if {[winfo exist $options(-canvas)]} {
      $options(-canvas) itemconfigure pBoxx -stipple $v
    }
    set options($o) $v
  }

  method _ConfOutline {o v} {
    if {[winfo exist $options(-canvas)]} {
      $options(-canvas) itemconfigure pBoxx -outline $v
    }
    set options($o) $v
  }

  method GetCoordinates { } {
    return $xy
  }

  method _InitialLocation { } {
    set W $options(-canvas)
    if {![winfo exists $W] ||
        [winfo geometry $options(-canvas)] eq "1x1+0+0" } {
      return
    }

    # Get initial location
    set w [winfo width $W]
    set h [winfo height $W]
 
    append ::debug "\n" "w = $w, h = $h"

    set x0 [$W canvasx 0]
    set y0 [$W canvasy 0]

    append ::debug "\n" "x0 = $x0, y0 = $y0"

    set x1 [expr {int($x0 + $w - $w / 8)}]
    set y1 [expr {int($y0 + $h - $h / 8)}]
    set x0 [expr {int($x0 + $w / 8)}]
    set y0 [expr {int($y0 + $h / 8)}]
    set xy [list $x0 $y0 $x1 $y1]
  }

  method Reset { } {
    set xy {}
    $self _InitialLocation
    if {$options(-visible)} {
      $self _Resize
    }
  }

  method _OnConfigure { } {
    if {![llength $xy]} {
      $self _InitialLocation
      if {$options(-visible)} {
        $self _Show
      }
    }
  }

  method _Show {} {
    append ::debug "\n" "_Show"
    set W $options(-canvas)
    if {![winfo exists $W]} {
      return
    }
    append ::debug "\n" "xy = $xy"
    if {![llength $xy]} {
      $self _InitialLocation
      if {![llength $xy]} {
        return
      }
    }
    append ::debug "\n" "xy = $xy"
    foreach {x0 y0 x1 y1} $xy break
    # Create stubs items that _Resize will size correctly
    $W delete pBox
    #$W create line 0 0 1 1 -tag {pBox diag1} -width 2 -fill $options(-outline)
    #$W create line 0 1 1 $y0 -tag {pBox diag2} -width 2 -fill $options(-outline)
    $W create rect 0 0 1 1 -tag {pBox pBoxx} -width 2 -outline $options(-outline) \
        -fill $options(-fill) -stipple $options(-stipple)
    $W bind pBoxx <Enter> [list $W config -cursor hand2]
    $W bind pBoxx <ButtonPress-1> [mymethod _OnDown box %x %y]
    $W bind pBoxx <B1-Motion> [mymethod _OnMotion box %x %y]
 
    foreach {color1 color2} {{} {}} break
    if {$options(-debug)} {
      foreach {color1 color2} {yellow blue} break
    }
 
    # Hidden rectangles that we bind to for resizing
    $W create rect 0 0 0 1 -fill $color1 -stipple gray25 -width 0 -tag {pBox L}
    $W create rect 1 0 1 1 -fill $color1 -stipple gray25 -width 0 -tag {pBox R}
    $W create rect 0 0 1 0 -fill $color1 -stipple gray25 -width 0 -tag {pBox T}
    $W create rect 0 1 1 1 -fill $color1 -stipple gray25 -width 0 -tag {pBox B}
    $W create rect 0 0 0 0 -fill $color2 -stipple gray25 -width 0 -tag {pBox TL}
    $W create rect 1 0 1 0 -fill $color2 -stipple gray25 -width 0 -tag {pBox TR}
    $W create rect 0 1 0 1 -fill $color2 -stipple gray25 -width 0 -tag {pBox BL}
    $W create rect 1 1 1 1 -fill $color2 -stipple gray25 -width 0 -tag {pBox BR}
    foreach tag [array names CURSORS] {
      $W bind $tag <Enter> [mymethod _OnEnter $tag]
      $W bind $tag <Leave> [mymethod _OnLeave $tag]
      $W bind $tag <B1-Motion> [mymethod _OnMotion $tag %x %y]
      $W bind $tag <ButtonRelease-1> [mymethod _OnUp $tag]
      $W bind $tag <ButtonPress-1> [mymethod _OnDown $tag %x %y]
    }
    $self _Resize
  }

  method _Hide {} {
    $options(-canvas) delete pBox
  }

  method _Resize { } {
    set W $options(-canvas)
    set grabSize $options(-grabsize)
    foreach {x0 y0 x1 y1} $xy break
    $W coords pBoxx $x0 $y0 $x1 $y1
    $W coords diag1 $x0 $y0 $x1 $y1
    $W coords diag2 $x1 $y0 $x0 $y1
 
    set w1 [$W itemcget pBoxx -width]           ;# NB. width extends outward
    set w2 [expr {-1 * ($w1 + $grabSize)}]
    
    foreach {x0 y0 x1 y1} [$type GrowBox $x0 $y0 $x1 $y1 $w1] break
    foreach {x0_ y0_ x1_ y1_} [$type GrowBox $x0 $y0 $x1 $y1 $w2] break
    $W coords L $x0 $y0_ $x0_ $y1_
    $W coords R $x1 $y0_ $x1_ $y1_
    $W coords T $x0_ $y0 $x1_ $y0_
    $W coords B $x0_ $y1 $x1_ $y1_
    $W coords TL $x0 $y0 $x0_ $y0_
    $W coords TR $x1 $y0 $x1_ $y0_
    $W coords BL $x0 $y1 $x0_ $y1_
    $W coords BR $x1 $y1 $x1_ $y1_ 
  }

  ##+##########################################################################
  # 
  # GrowBox -- grows (or shrinks) rectangle coordinates
  # 
  typemethod GrowBox {x0 y0 x1 y1 d} {
    list [expr {$x0-$d}] [expr {$y0-$d}] [expr {$x1+$d}] [expr {$y1+$d}]
  }

  ##+##########################################################################
  # 
  # _OnDown -- handles button down in a print box
  # 
  method _OnDown {tag x y} {
    set bxy [list $x $y]
    set bdown 1
  }

  ##+##########################################################################
  # 
  # _OnUp -- handles button up in a print box
  # 
  method _OnUp {tag} {
    set bdown 0
  }
  
  ##+##########################################################################
  # 
  # _OnEnter -- handles <Enter> in a print box
  # 
  method _OnEnter {tag} {
    $options(-canvas) config -cursor $CURSORS($tag)
  }

  ##+##########################################################################
  # 
  # _OnLeave -- handles <Leave> in a print box
  # 
  method _OnLeave {tag} {
    if {!$bdown} {
      $options(-canvas) config -cursor {}
    }
  }

  ##+##########################################################################
  # 
  # _OnMotion -- handles button motion, moving or resizing as needed
  # 
  method _OnMotion {tag x y} {
    set minSize $options(-minsize)
    set W $options(-canvas)
    
    foreach {x0 y0 x1 y1} $xy break
    foreach {dx dy} $bxy break
    set dx [expr {$x - $dx}]
    set dy [expr {$y - $dy}]
    
    set w [winfo width $W]
    set h [winfo height $W]
    set wx0 [$W canvasx 0]
    set wy0 [$W canvasy 0]
    set wx1 [$W canvasx $w]
    set wy1 [$W canvasy $h]
    
   if {$tag eq "box"} {                        ;# Move the box
     if {$x0 + $dx < $wx0} {set dx [expr {$wx0 - $x0}]}
     if {$x1 + $dx > $wx1} {set dx [expr {$wx1 - $x1}]}
     if {$y0 + $dy < $wy0} {set dy [expr {$wy0 - $y0}]}
     if {$y1 + $dy > $wy1} {set dy [expr {$wy1 - $y1}]}
     
     set x0 [expr {$x0 + $dx}]
     set x1 [expr {$x1 + $dx}]
     set y0 [expr {$y0 + $dy}]
     set y1 [expr {$y1 + $dy}]
     
     set xy [list $x0 $y0 $x1 $y1]
     set bxy [list $x $y]
   } else {                                    ;# Resize the box
     if {$tag eq "L" || $tag eq "TL" || $tag eq "BL"} {
       set x0_ [expr {$x0 + $dx}]
       if {$x0_ < $wx0} {
         lset xy 0 $wx0
         lset bxy 0 0
       } elseif {$x1 - $x0_ >= $minSize} {
         lset xy 0 $x0_
         lset bxy 0 $x
       }
     }
     if {$tag eq "R" || $tag eq "TR" || $tag eq "BR"} {
       set x1_ [expr {$x1 + $dx}]
       if {$x1_ > $wx1} {
         lset xy 2 $wx1
         lset bxy 0 $w
       } elseif {$x1_ - $x0 >= $minSize} {
         lset xy 2 $x1_
         lset bxy 0 $x
       }
     }
     if {$tag eq "T" || $tag eq "TR" || $tag eq "TL"} {
       set y0_ [expr {$y0 + $dy}]
       if {$y0_ < $wy0} {
         lset xy 1 $wy0
         lset bxy 1 0
       } elseif {$y1 - $y0_ >= $minSize} {
         lset xy 1 $y0_
         lset bxy 1 $y
       }
     }
     if {$tag eq "B" || $tag eq "BR" || $tag eq "BL"} {
       set y1_ [expr {$y1 + $dy}]
       if {$y1_ > $wy1} {
         lset xy 3 $wy1
         lset bxy 1 $h
       } elseif {$y1_ - $y0 > $minSize} {
         lset xy 3 $y1_
         lset bxy 1 $y
       }
     }
   }
    $self _Resize
  }
  
  typemethod Test_00 { } {
    destroy .cbTest00
    set t [toplevel .cbTest00]
    canvas $t.c -bg white
    pack $t.c
    append ::debug "\n[wm state $t]"
    append ::debug "\n" [winfo geometry $t.c]
    set cb [ClipBox %AUTO% -canvas $t.c -visible no]
    bind $t.c <Destroy> "$cb destroy"
    return $cb
  }
}