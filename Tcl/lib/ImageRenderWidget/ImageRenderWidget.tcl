package require snit
package require vtk
package require vtkinteraction
package require Ttk
package require VTKUtil

if { [catch {package require vtkPcmrExtTCL}] } {
  set ::vtkPcmrExtTCL_Loaded 0
} else {
  set ::vtkPcmrExtTCL_Loaded 1
}

proc ::__my_cb_vtkw_key_binding {vtkw renwin x y ctrl shift event keycode keysym} {
  if {$keysym eq "Escape" || $keysym eq "i"
      || $keysym eq "q" || $keysym eq "e"} {
    return
  }
  __vtk_cb_vtkw_key_binding \
      $vtkw $renwin $x $y $ctrl $shift $event $keycode $keysym
}

snit::widget ImageRenderWidget {

  component RenderWindow -public RenderWindow
  component Renderer -public Renderer
  component Interactor -public Interactor
  component Image -public Image
  component LookupTable -public LookupTable

  typeconstructor {
    if {[info command ::__vtk_cb_vtkw_key_binding] eq ""} {
      rename ::vtk::cb_vtkw_key_binding ::__vtk_cb_vtkw_key_binding
      rename __my_cb_vtkw_key_binding ::vtk::cb_vtkw_key_binding
    }
  }

  typevariable OtherAxes -array {
    X {Y Z}
    Y {X Z}
    Z {X Y}
  }

  typevariable AnchorPosition -array {
    left {-row 1 -column 0 -sticky ns}
    right {-row 1 -column 2 -sticky ns}
    botton {-row 2 -column 1 -sticky ew}
    top {-row 0 -column 1 -sticky ew}
  }

  typevariable AnchorOrientation -array {
    left   "vertical"
    right  "vertical"
    botton "horizontal"
    top    "horizontal"
  }

  variable Outline -array {
    Filter ""
    Mapper ""
    Actor  ""
  }

  variable PlaneWidgetActivated 0

  variable PlaneWidget
  variable RenderWidget
  variable VolumeOrienter
  variable AnnotatedCube
  variable AxesActor
  variable OrientationMarker
  variable PropAssembly

  variable Slicer

  variable CurrentImageType "default"

  variable WindowLevel -array {}

  variable ContextMenu ""

  method DumpWL { } {
    parray WindowLevel
  }

  variable InteractionState -array {
    X 0
    Y 0
    Z 0
  }

  variable ImageExtend -array {
    X,min 0
    X,max 1
    Y,min 0
    Y,max 1
    Z,min 0
    Z,max 1
  }
  variable ImageCenter -array {
    X 0.5
    Y 0.5
    Z 0.5
  }
  variable ImageSpacing -array {
    X 1
    Y 1
    Z 1
  }
  variable ImageDimension -array {
    X 1
    Y 1
    Z 1
  }
  variable ImageOrigin -array {
    X 0
    Y 0
    Z 0
  }

  delegate method Render to RenderWindow
  delegate method AddActor to Renderer
  delegate method AddVolume to Renderer
  delegate method GetOrientation to VolumenOrienter as GetOrientation
  delegate method GetPlaneName to VolumeOrienter
  delegate method AlignToView to VolumeOrienter using "%c %m %s"
  delegate option * to hull
  delegate method * to hull

  option -sliceranchor -readonly yes -default {X left Y right Z botton}
  option -volumeorienter -readonly yes -default ""
  option -initialview ""

  variable SlicerAnchor -array {
    X left
    Y right
    Z botton
  }

  constructor {args} {
    $self configurelist $args
    if {$options(-volumeorienter) eq ""} {
      set VolumeOrienter [MedicalVolumeOrienter $win.medori]
    } else {
      set VolumeOrienter $options(-volumeorienter)
    }
    if {$options(-sliceranchor) ne ""} {
      array set SlicerAnchor $options(-sliceranchor)
    }
    $self _BuildAnnotatedCube
    $self _UpdateOrientationWidgets
    $self _BuildImagePlaneWidgets
    $self _BuildOutline
    $self _BuildRenderWidget

    if {0} {
      ERROR: In /home/jsperez/Sources/Kitware/VTK/Rendering/OpenGL/vtkOpenGLTexture.cxx, line 200
      vtkOpenGLTexture (0x1a6f2a0): No scalar values found for texture input!
    }
    # the previous error is related to the following sentence, at initialization of the application may be because there is no input specified
    $self _SetupInteractor
    $self _AddActorsToRenderer
    $self _BuildSlicers
    foreach a {X Y Z} {
      grid $Slicer($a) {*}$AnchorPosition($SlicerAnchor($a))
    }
    grid $RenderWidget   -row 1 -column 1 -sticky snew
    grid rowconfigure    $win 1 -weight 1
    grid columnconfigure $win 1 -weight 1
    $self _CreateContextMenu
  }

  destructor {
  }

  # TODO: Check memory use
  method SetImageData { ImageData args } {
    array set opts {
      -type "default"
      -keepview 0
      -keepplane 0
      -keepWL 0
    }
    array set opts $args
    set hadPreviousImage [expr {[info exists Image] && $Image ne ""}]
    set planePositions [$self GetPlaneWidgetPositions]
    set Image $ImageData
    if {$Image eq ""} {
      $self _ReleasePipeline
      $self _DisableWidgets
      return
    }
    $self _EnableWidgets
    set CurrentImageType $opts(-type)

    $self _UpdateImageInformation
    $self _UpdateOutlineInput
    $self _UpdatePlaneWidgetInput
    if {$hadPreviousImage && $opts(-keepplane)} {
      $self _PositionPlanesFromSlicers $planePositions
    } else {
      $self _CenterPlaneWidgets
    }
    #$RenderWindow Render
    if {!$hadPreviousImage ||
        ![llength [$self _GetStoredWindowLevel]] || !$opts(-keepWL)} {
      $self _SetInitialWindowLevel
    }
    $self _SetStoredWindowLevel
    $self _UpdateSlicerRanges
    if {!$hadPreviousImage || !$opts(-keepview)} {
      $self SetInitialView
    }
    # REVIEW: this may overide some external configuration!
    if {!$PlaneWidgetActivated} {
      $self _ActivatePlaneWidgets
      set PlaneWidgetActivated 1
    }
  }

  method _ReleasePipeline { } {
    $self _ResetImageInformation
    $self _ResetOutlineInput
    $self _ReleasePlaneWidgetInput
    $self _ResetWindowLevel
  }

  method _ResetWindowLevel { } {
    array unset WindowLevel
    array set WindowLevel {}
  }

  method _ResetImageInformation { } {
    array set ImageExtend {
      X,min 0
      X,max 1
      Y,min 0
      Y,max 1
      Z,min 0
      Z,max 1
    }
    array set ImageOrigin {
      X 0
      Y 0
      Z 0
    }
    array set ImageSpacing {
      X 1
      Y 1
      Z 1
    }
    array set ImageDimension {
      X 1
      Y 1
      Z 1
    }
    array set ImageCenter {
      X 0.5
      Y 0.5
      Z 0.5
    }
  }

  method GetActualRenderer { } {
    return $Renderer
  }

  method GetInteractor { } {
    return $Interactor
  }

  method GetPlaneWidgetPositions { } {
    set positions [dict create]
    foreach a {X Y Z} {
      set plane $PlaneWidget($a)
      foreach m {Origin Point1 Point2} {
        dict set positions ${a},${m} [$plane Get$m]
      }
    }
    return $positions
  }

  method OutlineActor {args} {
    $Outline(Actor) {*}$args
  }

  method _UpdateOutlineInput { } {
    $Outline(Filter) SetInputData $Image
    #$Outline(Actor) VisibilityOn
    $Outline(Actor) VisibilityOff
  }

  method _ResetOutlineInput { } {
    $Outline(Filter) SetInputData ""
    $Outline(Actor) VisibilityOff
  }

  method _CenterPlaneWidgets { } {
    foreach a {X Y Z} {
      set plane $PlaneWidget($a)
      $plane SetPlaneOrientationTo${a}Axes
      $plane SetSliceIndex [expr {$ImageDimension($a)/2}]
    }
  }

  method _GetStoredWindowLevel { } {
    if {[info exists WindowLevel($CurrentImageType)]} {
      return $WindowLevel($CurrentImageType)
    }
    return ""
  }

  method _SetStoredWindowLevel { } {
    foreach {w l} [$self _GetStoredWindowLevel] break
    foreach a {X Y Z} copy {0 1 1} {
      $PlaneWidget($a) SetWindowLevel $w $l $copy
    }
    $self _UpdateLookupTableRange $w $l
  }

  method _SetInitialWindowLevel { } {
    foreach {r0 r1} [$Image GetScalarRange] break
    set w [expr {$r1 - $r0}]
    set l [expr {0.5*($r1 + $r0)}]
    $self _StoreWindowLevel $w $l
    $self _SetStoredWindowLevel
  }

  method SetWindowLevel {w l} {
    set WindowLevel($CurrentImageType) [list $w $l]
  }

  method _UpdatePlaneWidgetInput { } {
    foreach a {X Y Z} {
      set plane $PlaneWidget($a)
      # REVIEW: this is because vtkPcmrImagePlaneWidget has commented SetPlaneOrientation at the end of SetInputConnection, if not we will see this:
      # ERROR: In /home/jsperez/Sources/Kitware/vtk.gitlab/Rendering/OpenGL2/vtkOpenGLTexture.cxx, line 224
      # vtkOpenGLTexture (0x2fd9d20): 3D texture maps currently are not supported!
      if { [$plane GetInput] eq "" } {
        set shouldSetOrientation 1
      } else {
        set shouldSetOrientation 0
      }
      $plane SetInputData $Image
      if { $shouldSetOrientation } {
        $plane SetPlaneOrientation [$plane GetPlaneOrientation]
      }
      $plane TextureVisibilityOn
    }
  }

  method _ReleasePlaneWidgetInput { } {
    foreach a {X Y Z} {
      set plane $PlaneWidget($a)
      $plane SetInputData ""
      $plane TextureVisibilityOff
      $plane Off
    }
    set PlaneWidgetActivated 0
  }

  method _DeactivatePlaneWidgets { } {
    foreach a {X Y Z} {
      $PlaneWidget($a) Off
    }
  }

  method _ActivatePlaneWidgets { } {
    foreach a {X Y Z} {
      $PlaneWidget($a) On
    }
  }

  method _UpdateImageInformation { } {
    foreach {xmin xmax ymin ymax zmin zmax} [$Image GetExtent] break
    set ImageExtend(X,min) $xmin
    set ImageExtend(X,max) $xmax
    set ImageExtend(Y,min) $ymin
    set ImageExtend(Y,max) $ymax
    set ImageExtend(Z,min) $zmin
    set ImageExtend(Z,max) $zmax
    foreach {ox oy oz} [$Image GetOrigin] break
    set ImageOrigin(X) $ox
    set ImageOrigin(Y) $oy
    set ImageOrigin(Z) $oz
    foreach {sx sy sz} [$Image GetSpacing] break
    set ImageSpacing(X) $sx
    set ImageSpacing(Y) $sy
    set ImageSpacing(Z) $sz
    foreach {nx ny nz} [$Image GetDimensions] break
    set ImageDimension(X) $nx
    set ImageDimension(Y) $ny
    set ImageDimension(Z) $nz
    foreach a {X Y Z} {
      set ImageCenter($a) [expr {$ImageOrigin($a) +
                                 0.5*($ImageExtend($a,max) -
                                      $ImageExtend($a,min))*$ImageSpacing($a)}]
    }
  }

  method GetImageCenter { } {
    return [list $ImageCenter(X) $ImageCenter(Y) $ImageCenter(Z)]
  }

  method GetImageSpacing { } {
    return [list $ImageSpacing(X) $ImageSpacing(Y) $ImageSpacing(Z)]
  }

  method GetImageAxisExtend { axis } {
    return [list $ImageExtend($axis,min) $ImageExtend($axis,max)]
  }

  method _BuildSlicers { } {
    foreach a {X Y Z} {
      set Slicer($a) [$self _BuildSlicer $a]
    }
    $self _UpdateSlicerLabels
  }

  method _BuildSlicer { axis } {
    set f [frame $win.slicer${axis}]
    set orient $ImageRenderWidget::AnchorOrientation($SlicerAnchor($axis))
    set slicer [ttk::scale $f.slicer -orient $orient \
                    -command [mymethod _SlicerMove $axis]]
    label $f.axis0 -text ?
    label $f.axis1 -text ?
    if {$orient eq "horizontal"} {
      grid $f.axis0 -row 0 -column 0 -sticky w
      grid $slicer -row 0 -column 1 -sticky ew
      grid $f.axis1 -row 0 -column 2 -sticky e
      grid columnconfigure $f 1 -weight 1
    } else {
      grid $f.axis0 -row 0 -column 0 -sticky n
      grid $slicer -row 1 -column 0 -sticky ns
      grid $f.axis1 -row 2 -column 0 -sticky s
      grid rowconfigure $f 1 -weight 1
    }
    return $f
  }

  method _UpdateSlicerLabels { } {
    $self _UpdateSlicerLabel X
    $self _UpdateSlicerLabel Y
    $self _UpdateSlicerLabel Z
  }

  method _UpdateSlicerLabel { axis } {
    set f $Slicer($axis)
    set labels [$VolumeOrienter GetOrientationLabel $axis]
    $f.axis0 configure -text [lindex $labels 0]
    $f.axis1 configure -text [lindex $labels 1]
  }

  method _CreateContextMenu { } {
    set m [menu $win.context -tearoff 0]
    foreach v [[$VolumeOrienter info type] GetOrthogonalViewNames] {
      $m add command -label "Orient to $v" -command [mymethod AlignToView $v]
    }
    foreach a {X Y Z} {
      set f $Slicer($a)
      bind $f.axis0 <1> [mymethod _PostContextMenu %X %Y]
      bind $f.axis1 <1> [mymethod _PostContextMenu %X %Y]
    }
  }

  method _PostContextMenu { x y } {
    tk_popup $win.context $x $y
  }

  method GetContextMenu { } {
    return $win.context
  }

  method _SlicerMove { axis value } {
    set f $Slicer($axis)
    if {!$InteractionState($axis)} {
      $PlaneWidget($axis) SetSliceIndex [expr {int(round($value))}]
      after idle [list $RenderWindow Render]
    }
  }

  method _PositionPlanesFromSlicers { positions } {
    foreach a {X Y Z} {
      set plane $PlaneWidget($a)
      set po [$plane GetPlaneOrientation]
      if {$po == 3} {
        foreach m {Origin Point1 Point2} {
          $plane Set$m {*}[dict get $positions ${a},${m}]
        }
        $plane UpdatePlacement
      } else {
        $plane SetSliceIndex [$self _GetScalePosition $Slicer($a).slicer]
      }
    }
  }

  method _GetScalePosition { scale } {
    return [expr {int(round([$scale get]))}]
  }

  method GetSlicerPosition { axis } {
    return [$self _GetScalePosition $Slicer($axis).slicer]
  }

  method _UpdateSlicerRanges { } {
    foreach a {X Y Z} {
      $Slicer($a).slicer configure -from 0 -to [expr $ImageDimension($a)-1]
      # simulate interaction callback in order to synchronize the scale
      # & plane positions.
      set InteractionState($a) 1
      $self _InteractionCallback $a
      set InteractionState($a) 0
    }
  }

  # notas sobre la opacidad
  if {0} {
    planeWidgetX->TextureInterpolateOn();
    planeWidgetX->GetTexturePlaneProperty()->SetOpacity(1.0);
  }

  method _BuildImagePlaneWidget { axis picker color } {
    set plane $win.plane${axis}
    if {$::vtkPcmrExtTCL_Loaded} {
      vtkPcmrImagePlaneWidget $plane
    } else {
      vtkImagePlaneWidget $plane
    }
    $plane SetPriority 0.1
    $plane DisplayTextOn
    $plane TextureInterpolateOn
    $plane TextureVisibilityOff
    $plane SetPicker $picker
    $plane SetKeyPressActivationValue [string tolower $axis]
    set property [$plane GetPlaneProperty]
    $property SetColor {*}$color
    puts "_BuildImagePlaneWidget: $plane"
    return $plane
  }

  method _BuildOutline { } {
    # An outline is shown for context.
    #
    set Outline(Filter) [vtkOutlineFilter $win.outlineFilter]
    set Outline(Mapper) [vtkPolyDataMapper $win.outlineMapper]
    $Outline(Mapper) SetInputConnection [$Outline(Filter) GetOutputPort]

    set Outline(Actor) [vtkActor $win.outlineActor]
    $Outline(Actor) SetMapper $Outline(Mapper)
    $Outline(Actor) VisibilityOff
  }

  method _GetOnePlaneWidget { } {
    return $PlaneWidget(X)
  }

  method _SetUserControlledLookupTable { state } {
    set onoff [expr {$state?1:0}]
    foreach a {X Y Z} {
      set plane $PlaneWidget($a)
      $plane SetUserControlledLookupTable $a
    }
  }

  method _BuildImagePlaneWidgets { } {
    # The shared picker enables us to use 3 planes at one time
    # and gets the picking order right
    #
    set picker $win.picker
    vtkCellPicker $picker
    $win.picker SetTolerance 0.005

    set PlaneWidget(X) [$self _BuildImagePlaneWidget X $picker {1 0 0}]
    set PlaneWidget(Y) [$self _BuildImagePlaneWidget Y $picker {1 1 0}]
    set LookupTable [$PlaneWidget(X) GetLookupTable]
    $PlaneWidget(Y) SetLookupTable $LookupTable
    set PlaneWidget(Z) [$self _BuildImagePlaneWidget Z $picker {0 0 1}]
    $PlaneWidget(Z) SetLookupTable $LookupTable
    foreach a {X Y Z} {
      set plane $PlaneWidget($a)
      $plane SetUserControlledLookupTable 1
      $plane AddObserver WindowLevelEvent \
          [mymethod _WindowLevelCallback $a]
      $plane AddObserver EndWindowLevelEvent \
          [mymethod _StoreWindowLevelCallback $a]
      $plane AddObserver ResetWindowLevelEvent \
          [mymethod _StoreWindowLevelCallback $a]
      $plane AddObserver InteractionEvent \
          [mymethod _InteractionCallback $a]
      $plane AddObserver StartInteractionEvent \
          [mymethod _StartInteractionCallback $a]
      $plane AddObserver EndInteractionEvent \
          [mymethod _EndInteractionCallback $a]
    }
    puts "_BuildImagePlaneWidgets"
  }

  method GetPlaneWidget { axis } {
    return $PlaneWidget($axis)
  }

  method _BuildAnnotatedCube { } {
    set AnnotatedCube [vtkAnnotatedCubeActor $win.cube]
    # TODO: may be this rotation angles should be set from
    # VolumeOrienter
    $AnnotatedCube SetXFaceTextRotation 90
    #$AnnotatedCube SetYFaceTextRotation 180
    $AnnotatedCube SetZFaceTextRotation -90
    $AnnotatedCube SetFaceTextScale 0.65
    set property [$AnnotatedCube GetCubeProperty]
    $property SetColor 0.5 1 1
    set property [$AnnotatedCube GetTextEdgesProperty]
    $property SetLineWidth 1
    $property SetDiffuse 0
    $property SetAmbient 1
    $property SetColor 0.18 0.28 0.23
    set property [$AnnotatedCube GetXPlusFaceProperty]
    $property SetColor 1 0 0
    $property SetInterpolationToFlat
    set property [$AnnotatedCube GetXMinusFaceProperty]
    $property SetColor 1 0 0
    $property SetInterpolationToFlat
    set property [$AnnotatedCube GetYPlusFaceProperty]
    $property SetColor 0 1 0
    $property SetInterpolationToFlat
    set property [$AnnotatedCube GetYMinusFaceProperty]
    $property SetColor 0 1 0
    $property SetInterpolationToFlat
    set property [$AnnotatedCube GetZPlusFaceProperty]
    $property SetColor 0 0 1
    $property SetInterpolationToFlat
    set property [$AnnotatedCube GetZMinusFaceProperty]
    $property SetColor 0 0 1
    $property SetInterpolationToFlat

    set AxesActor [vtkAxesActor $win.axes]
    $AxesActor SetShaftTypeToCylinder
    $AxesActor SetXAxisLabelText "x"
    $AxesActor SetYAxisLabelText "y"
    $AxesActor SetZAxisLabelText "z"
    $AxesActor SetTotalLength 1.5 1.5 1.5
    set tprop [vtkTextProperty $win.tprop]
    $tprop ItalicOn
    $tprop ShadowOff
    $tprop SetFontFamilyToTimes
    [$AxesActor GetXAxisCaptionActor2D] SetCaptionTextProperty $tprop
    set tprop2 [vtkTextProperty $win.tprop2]
    $tprop2 ShallowCopy $tprop
    [$AxesActor GetYAxisCaptionActor2D] SetCaptionTextProperty $tprop2
    set tprop3 [vtkTextProperty $win.tprop3]
    $tprop3 ShallowCopy $tprop
    [$AxesActor GetZAxisCaptionActor2D] SetCaptionTextProperty $tprop3

    # Combine the two actors into one with vtkPropAssembly ...
    #
    set PropAssembly [vtkPropAssembly $win.assembly]
    $PropAssembly AddPart $AxesActor
    $PropAssembly AddPart $AnnotatedCube

    # Add the composite marker to the widget.  The widget
    # should be kept in non-interactive mode and the aspect
    # ratio of the render window taken into account explicitly,
    # since the widget currently does not take this into
    # account in a multi-renderer environment.
    #
    set OrientationMarker [vtkOrientationMarkerWidget $win.marker]
    $OrientationMarker SetOutlineColor 0.93 0.57 0.13
    $OrientationMarker SetOrientationMarker $PropAssembly
    $OrientationMarker SetViewport 0.0 0.0 0.15 0.3
  }

  method _BuildRenderWidget { } {
    # If the following message appear while destroying the widget:

    # A TkRenderWidget is being destroyed before it associated
    # vtkRenderWindow is destroyed.This is very bad and usually due to
    # the order in which objects are being destroyed.Always destroy
    # the vtkRenderWindow before destroying the user interface
    # components.

    # Look at
    # http://web.archiveorange.com/archive/v/50zHjiTb6RnnAya1Ned9 for
    # a solution.

    set RenderWidget [vtkTkRenderWidget $win.renderWidget]
    set RenderWindow [$RenderWidget GetRenderWindow]
    set Renderer [vtkRenderer $win.ren]
    $RenderWindow AddRenderer $Renderer
  }

  method _AddActorsToRenderer { } {
    $Renderer AddActor $Outline(Actor)
  }

  method _SetupInteractor { } {
    ::vtk::bind_tk_render_widget $RenderWidget
    set Interactor [$RenderWindow GetInteractor]
    set style [vtkInteractorStyleTrackballCamera New]
    $Interactor SetInteractorStyle $style
    $OrientationMarker SetInteractor $Interactor
    $OrientationMarker SetEnabled 1
    $OrientationMarker InteractiveOn
    foreach a {X Y Z} {
      $PlaneWidget($a) SetInteractor $Interactor
    }
    puts "_SetupInteractor"
  }

  method _UpdateLookupTableRange { w l } {
    set hw [expr {$w/2.0}]
    set rmin [expr {$l - $hw}]
    set rmax [expr {$l + $hw}]
    $LookupTable SetRange $rmin $rmax
  }

  method _WindowLevelCallback { a } {
    set plane $PlaneWidget($a)
    set w [$plane GetWindow]
    set l [$plane GetLevel]
    $self _UpdateLookupTableRange $w $l
  }

  method _StoreWindowLevelCallback { a } {
    set plane $PlaneWidget($a)
    set w [$plane GetWindow]
    set l [$plane GetLevel]
    $self _UpdateLookupTableRange $w $l
    foreach oa $ImageRenderWidget::OtherAxes($a) {
      $PlaneWidget($oa) SetWindowLevel $w $l 1
    }
    $self _StoreWindowLevel $w $l
  }

  method _StoreWindowLevel {w l} {
    set WindowLevel($CurrentImageType) [list $w $l]
  }

  method _DisableSlicer { slicer } {
    $slicer state disabled
  }

  method _EnableSlicer { slicer } {
    $slicer state !disabled
  }

  method EnableSlicer { axis } {
    $self _EnableSlicer $Slicer($axis).slicer
  }

  method _StartInteractionCallback { P } {
    set InteractionState($P) 1
  }

  method _EndInteractionCallback { P } {
    set InteractionState($P) 0
  }

  method _DisableWidgets { } {
    foreach a {X Y Z} {
      set slicer $Slicer($a).slicer
      $self _DisableSlicer $slicer
    }
  }

  method _EnableWidgets { } {
    foreach a {X Y Z} {
      set slicer $Slicer($a).slicer
      $self _EnableSlicer $slicer
    }
  }

  method _InteractionCallback { axis } {
    set plane $PlaneWidget($axis)
    set po [$plane GetPlaneOrientation]
    set slicer $Slicer($axis).slicer
    if {$po == 3} {
      # not axis-aligned
      $self _DisableSlicer $slicer
    } else {
      $self _EnableSlicer $slicer
      $slicer set [$plane GetSliceIndex]
    }
  }

  method SetImageOrientation { O } {
    $VolumeOrienter SetOrientation $O
    $self _UpdateOrientationWidgets
    $self _UpdateSlicerLabels
    $self AlignToView
  }

  method _UpdateOrientationWidgets { } {
    foreach a {X Y Z} {
      set labels [$VolumeOrienter GetOrientationLabel $a]
      $AnnotatedCube Set${a}MinusFaceText [lindex $labels 0]
      $AnnotatedCube Set${a}PlusFaceText  [lindex $labels 1]
    }
  }

  method SetInitialView { } {
    if {$options(-initialview) eq ""} {
      set cam1 [$Renderer GetActiveCamera]
      $cam1 Elevation 110
      $cam1 SetViewUp 0 0 -1
      $cam1 Azimuth 45
      $Renderer ResetCamera
    } else {
      $self AlignToView $options(-initialview)
    }
  }
}
