namespace eval ITKUtil {
  proc DeleteCmd { cmd } {
    rename $cmd ""
  }

  proc LoadITKTcl { } {
    global env tcl_platform

    if {[info exists env(USE_ITKTCL_LIB)]} {
      puts "using lib $::env(USE_ITKTCL_LIB)"
      load $env(USE_ITKTCL_LIB)
    }
    package require itktcl
  }
}