package require snit
package require tdom

snit::type HeaderXml {
  component Document
  variable Xml

  option -xmlfile -configuremethod _confXmlFile
  option -xmltext -configuremethod _confXmlText

  constructor { args } {
    $self configurelist $args
  }

  destructor {
    $self _deleteDocument
  }

  method _deleteDocument { } {
    if {$Document ne ""} {
      $Document delete
      set Document delete
    }
  }

  method _confXmlFile { o v } {
    if {![file readable $v]} {
      error "File $v is not readable"
    }
    $self _deleteDocument
    set options(-xmltext) ""
    set options($o) $v
    set fd [open $v]
    set xmlBuffer [string trim [read $fd]]
    close $fd
    set Document [dom parse $xmlBuffer]
  }

  method _confXmlText { o v } {
    $self _deleteDocument
    set options(-xmlfile) ""
    set options($o) $v
    set xmlBuffer [string trim $v]
    set Document [dom parse $xmlBuffer]
  }

  typemethod GetGlobalProperties { } {
    set props [list \
                   FormatVersion SourceSequence \
                   TimeValues NumberOfTimeSteps LengthOfTimeStep \
                   VelocityEncoding \
                   MinimumVelocity MaximumVelocity ImageOrientation \
                   InvertComponentX InvertComponentY InvertComponentZ]
    return $props
  }

  method GetFormatVersion { } {
    return [$self GetGlobalProperty FormatVersion -default "0.0"]
  }

  method GetSourceSequence { } {
    return [$self GetGlobalProperty SourceSequence -default ""]
  }

  method GetTimeValues { } {
    set root [$Document documentElement]
    set nodeTimeValues [$root selectNodes TimeValues]
    set timeValues {}
    if {$nodeTimeValues ne ""} {
      foreach tnode [$nodeTimeValues childNodes] {
        lappend timeValues [$tnode selectNodes {string()}]
      }
    }
    return $timeValues
  }

  method GetNumberOfTimeSteps { } {
    return [$self GetGlobalProperty NumberOfTimeSteps]
  }

  method GetLengthOfTimeStep { } {
    return [$self GetGlobalProperty LengthOfTimeStep]
  }

  method GetVelocityEncoding { } {
    return [$self GetGlobalProperty VelocityEncoding]
  }

  method GetMinimumVelocity { } {
    return [$self GetGlobalProperty MinimumVelocity]
  }

  method GetMaximumVelocity { } {
    return [$self GetGlobalProperty MaximumVelocity]
  }

  method GetImageOrientation { } {
    return [$self GetGlobalProperty ImageOrientation]
  }

  method GetInvertComponentX { } {
    return [$self GetGlobalProperty InvertComponentX -default 0]
  }

  method GetInvertComponentY { } {
    return [$self GetGlobalProperty InvertComponentY -default 1]
  }

  method GetInvertComponentZ { } {
    return [$self GetGlobalProperty InvertComponentZ -default 0]
  }

  method GetGlobalProperty { key args } {
    array set opt {
      -default ""
    }
    array set opt $args
    set v [$Document selectNodes string(/Study/${key})]
    if {$v eq ""} {
      set v $opt(-default)
    }
    return $v
  }

  method GetExtendedProperties { } {
    set allKeys {}
    set nodeExt [$Document selectNodes /Study/ExtendedFields]
    if {$nodeExt ne ""} {
      foreach ef [$nodeExt childNodes] {
        lappend allKeys [$ef nodeName]
      }
    }
    return $allKeys
  }

  method GetExtendedProperty { key } {
    set extended [$Document selectNodes /Study/ExtendedFields]
    if {$extended eq ""} {
      return ""
    }
    set node [$extended selectNodes $key]
    set description [$node selectNodes {string(Description)}]
    set value [$node selectNodes {string(Value)}]
    return [list -description $description -value [string trim $value]]
  }
}