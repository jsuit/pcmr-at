namespace eval VTKUtil {
  variable vtkVersionObject [vtkVersion New]

  proc GetVTKMajorVersion { } {
    variable vtkVersionObject
    return [$vtkVersionObject GetVTKMajorVersion]
  }

  proc CreateThumbnail { Image args } {
    array set opts {
      -filename "thumbnail.png"
      -slicepos -1
      -flipaxis -1
      -size 64
      -axis 2
    }
    array set opts $args
    $Image Update
    set extent [$Image GetExtent]
    set ia0 [expr {2*$opts(-axis)}]
    set ia1 [expr {$ia0+1}]
    set imin [lindex $extent $ia0]
    set imax [lindex $extent $ia1]
    if {$opts(-slicepos)>=$imin && $opts(-slicepos)<=$imax} {
      set i $opts(-slicepos)
    } else {
      set i [expr {($imax + $imin) / 2}]
    }

    # vtkExtractVOI to extract the mean slice
    set extract [vtkExtractVOI New]
    $extract SetInput $Image
    $extract SetVOI {*}[lreplace $extent $ia0 $ia1 $i $i]
    $extract SetSampleRate 1 1 1
    $extract Update
    
    # vtkImageResize to resize up to the thumbnail size requested
    set resize [vtkImageResize New]
    $resize SetInputConnection [$extract GetOutputPort] 
    set thumbSize {1 1 1}
    set taxis $i
    set j 0
    for {set i 0} {$i < 3} {incr i} {
      if {$i ne $opts(-axis)} {
        lappend taxis $i
        set j1 [expr {$i*2}]
        set j2 [expr {$i*2+1}]
        set thumbInfo($j,size) [expr {[lindex $extent $j2]-[lindex $extent $j1]}]
        set thumbInfo($j,index) $i
        incr j
      }
    }
    set size0 $thumbInfo(0,size)
    set size1 $thumbInfo(1,size)
    if {$size0>$size1} {
      set i $thumbInfo(0,index)
      set thumbSize [lreplace $thumbSize $i $i $opts(-size)]
      set i $thumbInfo(1,index)
      set thumbSize [lreplace $thumbSize $i $i [expr {($size1*$opts(-size))/$size0}]]
    } else {
      set i $thumbInfo(1,index)
      set thumbSize [lreplace $thumbSize $i $i $opts(-size)]
      set i $thumbInfo(0,index)
      set thumbSize [lreplace $thumbSize $i $i [expr {($size0*$opts(-size))/$size1}]]
    }
    puts "thumbSize = $thumbSize"
    $resize SetOutputDimensions {*}$thumbSize
    $resize Update
    
    # flip requested axis
    if {$opts(-flipaxis)>=0 && $opts(-flipaxis)<=2} {
      set flip [vtkImageFlip New]
      $flip SetInputConnection [$resize GetOutputPort]
      $flip SetFilteredAxis $opts(-flipaxis)
      $flip Update
      set targetFilter $flip
    } else {
      set targetFilter $resize
    }
    
    # vtkImageMapToWindowLevelColors to obtaing a color representation
    # of the thumbnail.
    foreach {rmin rmax} [[$resize GetOutput] GetScalarRange] break
    set w [expr {$rmax - $rmin}]
    set l [expr {0.5*($rmax + $rmin)}]
    set wlMapper [vtkImageMapToWindowLevelColors New]
    $wlMapper SetWindow $w
    $wlMapper SetLevel   $l
    $wlMapper SetInputConnection [$targetFilter GetOutputPort]

    # vtkPNGWriter the thumbnail writer
    set writer [vtkPNGWriter New]
    $writer SetInputConnection [$wlMapper GetOutputPort]
    $writer SetFileName $opts(-filename)
    $writer SetFileDimensionality 2
    $writer Write

    # release the pipeline
    $writer Delete
    $wlMapper Delete
    if {$targetFilter ne $resize} {
      $targetFilter Delete
    }
    $resize Delete
    $extract Delete
  }

  proc GetPlaneSizes { plane } {
    set origin  [$plane GetOrigin]
    set point1 [$plane GetPoint1]
    set point2 [$plane GetPoint2]
    set s1 0
    set s2 0
    foreach co $origin c1 $point1 c2 $point2 {
      set d1 [expr {$c1 - $co}]
      set d2 [expr {$c2 - $co}]
      set s1 [expr {$s1 + $d1*$d1}]
      set s2 [expr {$s2 + $d2*$d2}]
    }
    return [list [expr {sqrt($s1)}] [expr {sqrt($s2)}]]
  }

  proc SetPlaneElementSize { planeSource s } {
    foreach {s1 s2} [GetPlaneSizes $planeSource] break
    if {$s1 > $s2} {
      set r [expr {$s2/$s}]
      set r2 [expr {int($r)}]
      set r1 [expr {int($r2*double($s1)/$s2)}]
    } else {
      set r [expr {$s1/$s}]
      set r1 [expr {int($r)}]
      set r2 [expr {int($r1*double($s2)/$s1)}]
    }
    $planeSource SetResolution $r1 $r2
  }

  proc GetRegularPolygonPoints { plane n } {
    set algPoly [vtkRegularPolygonSource New]
    $algPoly SetCenter {*}[$plane GetCenter]
    $algPoly SetNormal {*}[$plane GetNormal]
    foreach {s1 s2} [GetPlaneSizes $plane] break
    if {$s1 < $s2} {
      set d $s1
    } else {
      set d $s2
    }
    $algPoly SetRadius [expr {$d/2.0}]
    $algPoly GeneratePolygonOff
    $algPoly GeneratePolylineOn
    $algPoly SetNumberOfSides $n
    $algPoly Update
    set polygon [$algPoly GetOutput]
    set points {}
    for {set i 0} {$i < $n} {incr i} {
      lappend points [$polygon GetPoint $i]
    }
    $algPoly Delete
    return $points
  }
}