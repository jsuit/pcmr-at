package require Ttk

namespace eval TKUtil {
  variable currentTheme "default"

  proc ChangeTheme { theme } {
    variable currentTheme

    ttk::style theme use $theme
    set currentTheme $theme
  }

  proc GetCurrentTheme { } {
    variable currentTheme

    return $currentTheme
  }

  # in order to discover the options of a style do this
  #
  # 1- find the elements:
  #
  #    ttk::style layout Vertical.TScale
  #
  # 2- The list the options for one element
  #
  #    ttk::style element options Vertical.Scale.slider
  #
  # 3- finally you can connfigure the options per widget state
  #
  #    ttk::style map TScale -gripcount {disabled 0}
  #
  proc OnThemeChange { w } {
    if {$w ne "." || $::tcl_platform(platform) ne "unix"} {
      return
    }
    set theme [GetCurrentTheme]
    if {$theme eq "clam"} {
      ttk::style map TScale -gripcount {disabled 0}
      ttk::style map TScale -bordercolor {disabled #ffffff}
      ttk::style map TScrollbar -gripcount {disabled 0}
      ttk::style map TScrollbar -bordercolor {disabled #ffffff}
    } elseif {$theme eq "default" || $theme eq "classic" || $theme eq "alt"} {
      ttk::style map TScale -sliderrelief {disabled flat}
    } else {
    }
  }

  proc Center {path} {
    update idletasks
    set w [winfo reqwidth  $path]
    set h [winfo reqheight $path]
    set sw [winfo screenwidth  $path]
    set sh [winfo screenheight $path]
    set x0 [expr {([winfo screenwidth  $path] - $w)/2 - [winfo vrootx $path]}]
    set y0 [expr {([winfo screenheight $path] - $h)/2 - [winfo vrooty $path]}]
    set x "+$x0"
    set y "+$y0"
    if {$::tcl_platform(platform) != "windows"} {
      if { $x0+$w > $sw } {set x "-0"; set x0 [expr {$sw-$w}]}
      if { $x0 < 0 }      {set x "+0"}
      if { $y0+$h > $sh } {set y "-0"; set y0 [expr {$sh-$h}]}
      if { $y0 < 0 }      {set y "+0"}
    }c
    after idle wm geometry $path "${w}x${h}${x}${y}"
  }

    proc ScrollBarState { sb state } {
	if { [winfo class $sb] eq "TScrollbar" } {
	    if {[tk windowingsystem] eq "aqua"} {
	    } else {
		$sb state $state
	    }
	} else {
	}
    }
}

bind . <<ThemeChanged>> {TKUtil::OnThemeChange %W}

if {0 && $::tcl_platform(platform) eq "unix"} {
  if {[lsearch [image names] img:Sash.xsash] == -1} {
    image create photo img:sash -data {
      R0lGODlhBQB5AKECADMzM9TQyP///////yH+EUNyZWF0ZWQgd2l0aCBHSU1QACH5BAEKAAMALAAA
      AAAFAHkAAAJOjA95y+0LUAxpUkufboLh1UFPiIyeKTqk8R1rpp5xysk1zbytoaPl/LsFczYiDlRE
      Hl1J5pLXhD4DPSDLd7XChFnudgO2KC5izA6sqTQKADs=
    }
    ttk::style element create Sash.xsash image \
        [list img:sash ] \
        -border {1 1} -sticky ew -padding {1 1}
    ttk::style layout TPanedwindow {
      Sash.xsash
    }
  }
}
