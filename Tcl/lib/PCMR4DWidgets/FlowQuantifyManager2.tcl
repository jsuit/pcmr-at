package require vtk
package require snit
package require msgcat
package require hook
package require VTKUtil
package require TKUtil
package require CScale
package require Icons
package require tdom
package require autoscroll
package require Pcmr4d
package require vtkTclAddon

namespace import msgcat::*

snit::widget FlowQuantifyManager2 {

  typevariable MagnitudeInfo -array {
    area,label "Contour Area"
    area,unit "mm^2"
    flow,label "Flow Rate"
    flow,unit "mL/s"
    flow_forward,label "Flow Rate (+)"
    flow_forward,unit "mL/s"
    flow_backward,label "Flow Rate (-)"
    flow_backward,unit "mL/s"
    min_velocity,label "Min. Velocity"
    min_velocity,unit "cm/s"
    max_velocity,label "Max. Velocity"
    max_velocity,unit "cm/s"
  }

  variable ColorInfo -array {} 

  component VolumeOrienter -public VolumeOrienter

  component PropPicker -public PropPicker
  component PlaneWidget -public PlaneWidget
  component SplineWidget -public SplineWidget

  component TemporalSource -public TemporalSource

  variable DataSet ""
  variable ClampVelocity 0
  variable PlaneCollection -array {}
  variable PickPlaneMapping -array {}
  variable TableData -array {
    current,edited ""
    current,selected ""
  }
  variable vtkChartObjects -array {}

  variable Widgets
  variable WidgetOnOff 0
  variable Renderer

  delegate option * to hull
  delegate method * to hull

  constructor {args} {
    catch {
    $self configurelist $args
    $self buildFrames
    $self buildTable
    $self buildChart
    $self buildVTKObjects
    } msg
    puts "$type: $msg"
  }

  destructor {
    [[$Renderer GetInteractor] GetPickingManager] \
        RemovePicker $PropPicker [$Renderer ImageRender GetActualRenderer]
    $PlaneWidget Delete
    $SplineWidget Delete
    $PropPicker Delete
    $TemporalSource Delete
    $self destroyPlaneCollection
  }

  method SetVolumeOrienter { orienter } {
    set VolumeOrienter $orienter
  }

  method SetVisibility {v} {
    return
    set icn [Icons GetInstance]
    set imgON [$icn Get light_bulb_on -size 16]
    set imgOFF [$icn Get light_bulb_off -size 16]
    if {$v} {
      set img $imgON
    } else {
      set img $imgOFF
    }
    foreach name [array names PlaneCollection] {
      $PlaneCollection($name) SetVisibility $v
      set I [list tag "--${name}--"]
      set C [list tag "STATE"]
      $Widgets(tree,planes) tree item element configure $I $C eIMG -image $img
    }
    set activePlane [$self GetActivePlane]
    if {$activePlane eq ""} {
      set v 0
    } else {
      set v [expr {$WidgetOnOff && $v}]
      # update active properties
      $self UpdateActiveProperties
    }
    $PlaneWidget SetEnabled $v
    $SplineWidget SetEnabled $v

  }

  method UpdateActiveProperties { } {
    set activePlane [$self GetActivePlane]
    if {$activePlane eq ""} {
      return
    }
    set icn [Icons GetInstance]
    set imgON [$icn Get light_bulb_on -size 16]
    set imgOFF [$icn Get light_bulb_off -size 16]
    $Widgets(button,HedgeHogOnOff) configure \
        -image [expr {[$activePlane GetHedgeHogVisibility]?$imgON:$imgOFF}]
    $Widgets(button,StreamlineOnOff) configure \
        -image [expr {[$activePlane GetStreamlineVisibility]?$imgON:$imgOFF}]
    $Widgets(button,PathlineOnOff) configure \
        -image [expr {[$activePlane GetPathlineVisibility]?$imgON:$imgOFF}]
  }

  method SetActivePropertiesToOff { } {
    set icn [Icons GetInstance]
    set imgOFF [$icn Get light_bulb_off -size 16]
    $Widgets(button,HedgeHogOnOff) configure -image $imgOFF
    $Widgets(button,StreamlineOnOff) configure -image $imgOFF
    $Widgets(button,PathlineOnOff) configure -image $imgOFF
    if {$WidgetOnOff} {
      $Widgets(button,WidgetOnOff) invoke
    }
  }

  method GetVisibility { } {
    foreach idx [array names PlaneCollection] {
      if {[$PlaneCollection($idx) GetVisibility]} {
        return 1
      }
    }
    return 0
  }

  method GetLabel { } {
    return "Flow Quantify 2"
  }

  method SetDataSet { dataSet } {
    set DataSet $dataSet
    $dataSet AddObserver AfterOpenEvent [mymethod OnOpenDataSet]
    $dataSet AddObserver ChangeTimeStep [mymethod OnChangeTimeStep %T]
    $dataSet AddObserver BeforeCloseEvent [mymethod OnCloseDataSet]
  }

  method NewPlane { {name ""} } {
    set objPlane [PlaneQuantifier %AUTO%]
    $objPlane SetDataSet $DataSet
    $objPlane AddToRenderer $Renderer
    $objPlane SetColorPreset "BlueRedRainbow" Plane
    $objPlane SetColorPreset "BlueRedRainbow" Flow
    $objPlane SetLookupTableRange 0 $ClampVelocity
    $objPlane SetTemporalSource $TemporalSource
    $objPlane OnOpenDataSet
    if {$name ne ""} {
      set isNew 0
      $objPlane LoadFromXml [$self GetPlanePath $name]
    } else {
      set isNew 1
      set name [namespace tail $objPlane]
    }
    set PlaneCollection($name) $objPlane
    $self insertPlaneInTable $name DIRTY $isNew
    set actor [$objPlane GetProbeActor]
    $PropPicker AddPickList $actor
    set PickPlaneMapping($actor) $name
    return $name
  }

  method OnOpenDataSet { } {
    set ClampVelocity [$DataSet GetVelocityEncoding]
    foreach p [$self GetAllStoredPlanes] {
      set t0 [clock millisecond]
      $self NewPlane $p
      set t1 [clock milliseconds]
      puts "Plano $p procesado en [expr {($t1-$t0)/1000.0}]"
    }
    set vectorImage [$DataSet GetVelocityImage -component FLOW -vtk yes]
    $TemporalSource SetTclDataSet $DataSet
    # REVIEW: this is needed to obtain the requested handlesize
    $PlaneWidget SetInputData $vectorImage
    $PlaneWidget PlaceWidget
  }

  method OnCloseDataSet { } {
    $self SetActivePropertiesToOff
    $self destroyPlaneCollection
    $Widgets(tree,planes) item delete 0 end
    $self SetCurrentEditedName ""
    $self ClearChartData flow
    $self RedrawPlot
  }

  method OnChangeTimeStep { t } {
    foreach idx [array names PlaneCollection] {
      $PlaneCollection($idx) ChangeTimeStep $t
    }
    set plane [$self GetActivePlane]
    if {$plane ne ""} {
      $self UpdateSplineWidgetFromPlane $plane
    }
    $self UpdateFlowTable
    $self UpdateChartTimeStep
    hook call $self <Render>
  }

  method ComputeFlowTableForPlaneName { {name ""} } {
    set plane [$self GetPlaneObject $name]
    if {$plane ne ""} {
      $plane ComputeFlowTable
    }
  }

  method CmdComputeFlow { } {
    foreach {name plane} [array get PlaneCollection] {
      $plane ComputeFlowTable
    }
    $self UpdateFlowTable
    $self PlotVariable "flow"
  }

  method UpdateFlowTable { } {
    set tagMap [$self GetTagMapping]
    set unk [$self GetFlowUnknown]
    set t [$DataSet GetCurrentTimeStep]
    foreach {name plane} [array get PlaneCollection] {
      set flowAt [$plane GetFlowAtTimeStep $t]
      if {$flowAt eq ""} {
        set row $unk
      } else {
        array set record $flowAt
        set row [list]
        foreach {t1 t2} $tagMap {
          lappend row $t1 [format "%.2f" $record($t2)]
        }
      }
      $self UpdatePlaneItem $name {*}$row
    }
  }

  method AddToRenderer { ren } {
    set Renderer $ren
    [$Renderer GetInteractor] AddObserver \
        LeftButtonPressEvent [mymethod OnLeftButtonPressEvent]
    [[$Renderer GetInteractor] GetPickingManager] \
        AddPicker $PropPicker [$Renderer ImageRender GetActualRenderer]
    $PlaneWidget SetInteractor [$Renderer GetInteractor]
    $SplineWidget SetInteractor [$Renderer GetInteractor]
  }

  method buildFrames { } {
    ttk::panedwindow $win.pwMain -orient vertical
    grid $win.pwMain -row 0 -column 0 -sticky snew
    grid rowconfigure $win 0 -weight 1
    grid columnconfigure $win 0 -weight 1

    set Widgets(frame,table) [frame $win.pwMain.fTable]
    $win.pwMain add $Widgets(frame,table)
    set Widgets(frame,chart) [frame $win.pwMain.fChart]
    $win.pwMain add $Widgets(frame,chart)
  }

  method buildTable { } {
    set fcontrol0 [frame $Widgets(frame,table).control0]
    $self buildTableControl0 $fcontrol0
    set fcontrol1 [frame $Widgets(frame,table).control1]
    $self buildTableControl1 $fcontrol1
    set fcontrol2 [frame $Widgets(frame,table).control2 -relief sunken -bd 1]
    set ::_f $fcontrol2
    $self buildTableControl2 $fcontrol2
    grid $fcontrol0 -row 0 -column 0 -sticky snew
    grid $fcontrol1 -row 1 -column 0 -sticky snew

    set Widgets(tree,planes) [wtree $Widgets(frame,table).treePlanes -font TkTextFont -table yes -filter no \
                                  -showheader yes -showbuttons no \
                                  -columns [list \
                                                [list text -tags NAME -label [mc "Name"]] \
                                                [list image -tags DIRTY -label [mc "Saved"]] \
                                                [list text -tags MINVEL -label [mc "Min. Vel."]] \
                                                [list text -tags MAXVEL -label [mc "Max. Vel."]] \
                                                [list text -tags FLOWPOS -label [mc "(+) Flow"]] \
                                                [list text -tags FLOWNEG -label [mc "(-) Flow"]] \
                                                [list text -tags FLOWRES -label [mc "Flow"]] \
                                                [list text -tags ACCVOL -label [mc "Acc. Vol."]] ]]
    $Widgets(tree,planes) column configure all -gridleftcolor gray70 -borderwidth 1 -font TkTextFont
    $Widgets(tree,planes) notify bind $self <ActiveItem> [mymethod OnActivateItem]
    grid $Widgets(tree,planes) -row 2 -column 0 -sticky snew
    grid $fcontrol2 -row 3 -column 0 -sticky snew
    grid rowconfigure $Widgets(frame,table) 2 -weight 1
    grid columnconfigure $Widgets(frame,table) 0 -weight 1
  }

  method GetTipOnColumn { } {
    set x [winfo x $Widgets(tree,planes)]
    set y [winfo y $Widgets(tree,planes)]
    $Widgets(tree,planes) identify -array pos $x $y
    return $pos(where)
  }

  method buildTableControl0 { f } {
    set icn [Icons GetInstance]
    ttk::label $f.lbCurrent -text [mc "Current:"]
    ttk::entry $f.entCurrent \
        -textvariable [myvar TableData(current,edited)]
    set Widgets(button,RenamePlane) [Button $f.btnRenamePlane -image [$icn Get "rename" -size 16] \
                                        -command [mymethod CmdRenamePlane] \
                                        -helptext [mc "Rename current plane"]]
    set Widgets(button,StorePlane) [Button $f.btnStorePlane -image [$icn Get "save" -size 16] \
                                        -command [mymethod CmdStorePlane] \
                                        -helptext [mc "Store plane in study"]]
    set Widgets(button,RemovePlane) [Button $f.btnRemovePlane -image [$icn Get "document_delete" -size 16] \
                                         -command [mymethod CmdRemovePlane] \
                                         -helptext [mc "Remove current plane"]]
    set Widgets(button,InvertNormal) [Button $f.btnInvertNormal -image [$icn Get "rotate_ccw" -size 16] \
                                          -command [mymethod CmdInvertPlaneNormal] \
                                          -helptext [mc "Invert plane normal"]]
    set Widgets(button,Compute) [Button $f.btnCompute -image [$icn Get "make" -size 16] \
                                     -command [mymethod CmdComputeFlow] \
                                     -helptext [mc "Compute flow along time"]]
    set Widgets(button,NewPlane) \
        [Button $f.btnNewPlane \
             -image [$icn Get "plus_green" -size 16] \
             -command [mymethod CmdNewPlane] \
             -helptext [mc "Add a new plane"]]
    set Widgets(button,WidgetOnOff) \
        [Button $f.btnWidgetOnOff \
             -image [$icn Get "arrow_move" -size 16] \
             -command [mymethod CmdWidgetOnOff] \
             -helptext [mc "Activate plane & spline widgets"]]

    set Widgets(button,Reposition) [ttk::menubutton $f.btnReposition \
                                        -text [mc "Reset splines"] \
                                        -menu $f.btnReposition.m \
                                        -padding 0 ]
    

    $self defineMenuReposition $Widgets(button,Reposition).m

    set Widgets(button,Align) [ttk::menubutton $f.btnAlign \
                                        -text [mc "Align plane"] \
                                        -menu $f.btnAlign.m \
                                        -padding 0]
    

    $self defineMenuAlign $Widgets(button,Align).m

    grid $f.lbCurrent -row 0 -column 0 -sticky e
    grid $f.entCurrent -row 0 -column 1 -sticky e
    set c 2
    foreach b {RenamePlane NewPlane StorePlane RemovePlane InvertNormal Compute \
                   WidgetOnOff Reposition Align} {
      grid $Widgets(button,$b) -row 0 -column $c -sticky w
      incr c
    }
    grid columnconfigure $f $c -weight 1
  }
  
  method buildTableControl1 { f } {
    set icn [Icons GetInstance]
    set Widgets(button,PlaneOnOff) \
        [Button $f.btnPlaneOnOff -relief link \
             -compound right -padx 1 -pady 1 \
             -text [mc "Plane"]: \
             -image [$icn Get "light_bulb_off" -size 16] \
             -command [mymethod CmdPlaneOnOff] \
             -helptext [mc "On/Off plane"]]

    set Widgets(button,HedgeHogOnOff) \
        [Button $f.btnHedgeHogOnOff -relief link \
             -compound right -padx 1 -pady 1 \
             -text [mc "Vectors"]: \
             -image [$icn Get "light_bulb_off" -size 16] \
             -command [mymethod CmdHedgeHogOnOff] \
             -helptext [mc "On/Off arrow vectors"]]
   
    set Widgets(button,StreamlineOnOff) \
        [Button $f.btnStreamlineOnOff -relief link \
             -compound right -padx 1 -pady 1 \
             -text [mc "Stream"]: \
             -image [$icn Get "light_bulb_off" -size 16] \
             -command [mymethod CmdStreamlineOnOff] \
             -helptext [mc "On/Off streamlines"]]
   
    set Widgets(button,PathlineOnOff) \
        [Button $f.btnPathlineOnOff -relief link \
             -compound right -padx 1 -pady 1 \
             -text [mc "Path"]: \
             -image [$icn Get "light_bulb_off" -size 16] \
             -command [mymethod CmdPathlineOnOff] \
             -helptext [mc "On/Off pathlines"]]

    set c 0
    foreach {l w} [list "" $Widgets(button,PlaneOnOff) \
                       "" $Widgets(button,HedgeHogOnOff) \
                       "" $Widgets(button,StreamlineOnOff) \
                       "" $Widgets(button,PathlineOnOff)] {
      
      if {$l ne ""} {
        grid $l -row 0 -column $c -sticky w
        incr c
      }
      grid $w -row 0 -column $c -sticky w
      incr c
    }
    grid columnconfigure $f $c -weight 1
  }

  method buildTableControl2 { f } {
    set icn [Icons GetInstance]

    ttk::label $f.lbProbeRes -text [mc "Resolution"]:
    set Widgets(scale,proberes) \
        [CScale $f.scProbeRes -from 1 -to 0.05 -value 1.0]
    bind $Widgets(scale,proberes) <ButtonRelease> \
        [mymethod CmdUpdateProbeResolution]

    ttk::label $f.lbColorPresetPlane -text [mc "Plane Color"]:
    set img [$icn Get "BlueRedRainbow"]
    set Widgets(menubutton,ColorPresetPlane) \
        [ttk::menubutton $f.mbtnColorPresetPlane \
             -image $img \
             -menu $f.mbtnColorPresetPlane.m \
             -padding 1]
    $self defineMenuPreset $f.mbtnColorPresetPlane.m Plane

    ttk::label $f.lbColorPresetFlow -text [mc "Flow Color"]:
    set img [$icn Get "BlueRedRainbow"]
    set Widgets(menubutton,ColorPresetFlow) \
        [ttk::menubutton $f.mbtnColorPresetFlow \
             -image $img \
             -menu $f.mbtnColorPresetFlow.m \
             -padding 1]
    $self defineMenuPreset $f.mbtnColorPresetFlow.m Flow

    set c 0
    foreach {l w} [list $f.lbProbeRes $Widgets(scale,proberes) \
                       $f.lbColorPresetPlane $Widgets(menubutton,ColorPresetPlane) \
                       $f.lbColorPresetFlow $Widgets(menubutton,ColorPresetFlow)] {
      
      if {$l ne ""} {
        grid $l -row 0 -column $c -sticky w
        incr c
      }
      grid $w -row 0 -column $c -sticky w
      incr c
    }
    grid columnconfigure $f $c -weight 1
  }

  method GetProbeResolutionFactor { } {
    return [$Widgets(scale,proberes) get]
  }

  method defineMenuPreset { mm typeLT } {
    set icn [Icons GetInstance]
    menu $mm -tearoff no
    $mm add command -image [$icn Get "BlueRedRainbow"] \
        -command [mymethod CmdChangeLookupTable "BlueRedRainbow" $typeLT]
    $mm add command -image [$icn Get "CyanBlueMagenta"] \
        -command [mymethod CmdChangeLookupTable "CyanBlueMagenta" $typeLT]
    $mm add command -image [$icn Get "GreenYellowRed"] \
        -command [mymethod CmdChangeLookupTable "GreenYellowRed" $typeLT]
    $mm add command -image [$icn Get "Blue2Cyan"] \
        -command [mymethod CmdChangeLookupTable "Blue2Cyan" $typeLT]
    $mm add command -image [$icn Get "Red2Yellow"] \
        -command [mymethod CmdChangeLookupTable "Red2Yellow" $typeLT]
    $mm add command -image [$icn Get "Black2White"] \
        -command [mymethod CmdChangeLookupTable "Black2White" $typeLT]
 }
  
  method defineMenuReposition { m } {
    menu $m -tearoff no
    $m add command \
        -label [mc "Reset current spline position"] \
        -command [mymethod CmdResetSplinePosition]
    $m add command \
        -label [mc "Reset all spline position"] \
          -command [mymethod CmdResetAllSplinePosition]
    $m add command \
        -label [mc "All equals to current"] \
        -command [mymethod CmdCopySplineAll]
    $m add command \
        -label [mc "All next equal to current"] \
        -command [mymethod CmdCopySplineForward]
    $m add command \
        -label [mc "Next equal to current"] \
        -command [mymethod CmdCopySplineNext]
    $m add command \
          -label [mc "All previous equal to current"] \
        -command [mymethod CmdCopySplineBackward]
    $m add command \
        -label [mc "Previous equal to current"] \
        -command [mymethod CmdCopySplinePrevious]
  }

  method defineMenuAlign { m } {
    menu $m -tearoff no
    $m add command \
        -label [mc "Axial"] \
        -command [mymethod CmdAlignPlane "Axial"]
    $m add command \
        -label [mc "Sagittal"] \
        -command [mymethod CmdAlignPlane "Sagittal"]
    $m add command \
        -label [mc "Coronal"] \
        -command [mymethod CmdAlignPlane "Coronal"]
  }

  method buildChart { } {

    set Widgets(render,chart) [vtkTkRenderWidget $Widgets(frame,chart).tkrw]
    ::vtk::bind_tk_render_widget $Widgets(render,chart)
    set renWin [$Widgets(render,chart) GetRenderWindow]
    set view [vtkContextView $Widgets(render,chart).view]
    set vtkChartObjects(view) $view
    $view SetRenderWindow $renWin
    set top [winfo toplevel $Widgets(frame,chart)]
    wm protocol $top WM_DELETE_WINDOW [string map "%W $top %V $view" {
      $view Delete
      destroy $top
    }]
    # REVIEW: the array objects can be deleted after they are added to the table
    set vtkChartObjects(table,flow) [vtkTable $Widgets(render,chart).tableFlow]
    set vtkChartObjects(array,time) [vtkFloatArray $Widgets(render,chart).arrTime]
    $vtkChartObjects(array,time) SetName "Time"
    set vtkChartObjects(array,flow) [vtkFloatArray $Widgets(render,chart).arrFlow]
    $vtkChartObjects(array,flow) SetName "Flow"
    $vtkChartObjects(table,flow) AddColumn $vtkChartObjects(array,time)
    $vtkChartObjects(table,flow) AddColumn $vtkChartObjects(array,flow)
    set vtkChartObjects(chartXY) [vtkChartXY $Widgets(render,chart).chart]
    [$view GetScene] AddItem $vtkChartObjects(chartXY) 
    set axisX [$vtkChartObjects(chartXY) GetAxis 1]
    vtkAxisAddon $axisX SetTitle "Seconds"
    set axisY [$vtkChartObjects(chartXY) GetAxis 0]
    vtkAxisAddon $axisY SetTitle "$MagnitudeInfo(flow,label) $MagnitudeInfo(flow,unit)"

    set vtkChartObjects(plot,flow) [$vtkChartObjects(chartXY) AddPlot 0]
    $vtkChartObjects(plot,flow) SetInputData $vtkChartObjects(table,flow) 0 1

    set vtkChartObjects(table,flowMark) [vtkTable $Widgets(render,chart).tableFlowMark]
    set vtkChartObjects(array,timeMark) [vtkFloatArray $Widgets(render,chart).arrTimeMark]
    $vtkChartObjects(array,timeMark) SetName "Time"
    set vtkChartObjects(array,flowMark) [vtkFloatArray $Widgets(render,chart).arrFlowMark]
    $vtkChartObjects(array,flowMark) SetName "Flow"
    $vtkChartObjects(array,timeMark) SetNumberOfTuples 1
    $vtkChartObjects(array,flowMark) SetNumberOfTuples 1
    $vtkChartObjects(table,flowMark) AddColumn $vtkChartObjects(array,timeMark)
    $vtkChartObjects(table,flowMark) AddColumn $vtkChartObjects(array,flowMark)
    set vtkChartObjects(plot,flowMark) [$vtkChartObjects(chartXY) AddPlot 1]
    $vtkChartObjects(plot,flowMark) SetInputData $vtkChartObjects(table,flowMark) 0 1
    $vtkChartObjects(plot,flowMark) SetColor 255 0 0 255
    $vtkChartObjects(plot,flowMark) SetMarkerSize 7
    $vtkChartObjects(plot,flowMark) SetMarkerStyle 4
    $self ClearChartData flow
    grid $Widgets(render,chart) -row 0 -column 0 -sticky snew
    grid rowconfigure $Widgets(frame,chart) 0 -weight 1
    grid columnconfigure $Widgets(frame,chart) 0 -weight 1
   }

  method PlotVariable { name } {
    set plane [$self GetActivePlane]
    if {$plane ne ""} {
      set T [$DataSet GetNumberOfTimeStep]
      # REVIEW: only one array is present with name "flow"
      $vtkChartObjects(array,time) SetNumberOfTuples $T
      $vtkChartObjects(array,flow) SetNumberOfTuples $T
      set i 0
      foreach {t y} [$plane GetVariableTimeSerie $name] {
        $vtkChartObjects(array,time) SetValue $i $t
        $vtkChartObjects(array,flow) SetValue $i $y
        incr i
      }
    }
    $self RedrawPlot
  }

  method ClearChartData { name } {
    $vtkChartObjects(array,time) SetNumberOfTuples 2
    $vtkChartObjects(array,$name) SetNumberOfTuples 2
    $vtkChartObjects(array,time) SetValue 0 0.0
    $vtkChartObjects(array,$name) SetValue 0 0.0
    $vtkChartObjects(array,time) SetValue 1 1.0
    $vtkChartObjects(array,$name) SetValue 1 0.0
  }

  method RedrawPlot { } {
    $vtkChartObjects(plot,flow) Modified
    $vtkChartObjects(chartXY) RecalculateBounds
    $self UpdateChartTimeStep 
  }

  method UpdateChartTimeStep { } {
    set t [$DataSet GetCurrentTimeStep]
    # REVIEW: check this logic GetNumberOfTuples] <= $t, why?
    if {$t == -1 || [$vtkChartObjects(array,time) GetNumberOfTuples] <= $t} {
      return
    }
    $vtkChartObjects(array,timeMark) SetValue 0 [$vtkChartObjects(array,time) GetValue $t]
    $vtkChartObjects(array,flowMark) SetValue 0 [$vtkChartObjects(array,flow) GetValue $t]
    $vtkChartObjects(plot,flowMark) Modified

    # CHECK to avoid
    #  Generic Warning: In /home/jsperez/Sources/Kitware/vtk.gitlab/Common/Core/vtkMath.cxx, line 762
    #  vtkMath::Jacobi: Error extracting eigenfunctions
    
    if { [$self GetActivePlane] ne "" } {
      $vtkChartObjects(view) Render
    }
  }

  method buildVTKObjects { } {
    puts "$type buildVTKObjects .........................................."
    install PropPicker using vtkPropPicker $win.propPicker
    $PropPicker InitializePickList
    $PropPicker PickFromListOn
    install PlaneWidget using vtkPlaneWidget $win.planeWidget 
    #$PlaneWidget SetEnabled 0
    $PlaneWidget SetRepresentationToOutline
    $PlaneWidget SetHandleSize 0.01
    $PlaneWidget AddObserver StartInteractionEvent \
        [mymethod OnPlaneWidgetStartInteraction]
    $PlaneWidget AddObserver EndInteractionEvent \
        [mymethod OnPlaneWidgetEndInteraction]
    $PlaneWidget AddObserver InteractionEvent \
        [mymethod OnPlaneWidgetInteraction]    
    install SplineWidget using vtkSplineWidget2 $win.splineWidget
    #$SplineWidget SetEnabled 0
    set spline [$SplineWidget GetRepresentation]
    $spline SetPlaneSource [$PlaneWidget GetPolyDataAlgorithm]
    $spline ProjectToPlaneOn 
    $spline SetProjectionNormal 3
    $spline ClosedOn
    $SplineWidget SetPriority 1
    $SplineWidget AddObserver StartInteractionEvent \
        [mymethod OnSplineWidgetBeginInteraction]
    $SplineWidget AddObserver InteractionEvent \
        [mymethod OnSplineWidgetInteraction]
    $SplineWidget AddObserver EndInteractionEvent \
        [mymethod OnSplineWidgetEndInteraction]

    set TemporalSource [TemporalVelocitySource]
    $TemporalSource SetRescaleFactor 10
  }

  method OnSplineWidgetBeginInteraction { } {
    set plane [$self GetActivePlane]
    $plane TemporalDisableFlowActors
  }

  method OnSplineWidgetInteraction { } {
  }

  method OnSplineWidgetEndInteraction { } {
    set plane [$self GetActivePlane]
    $plane SetSplinePoints [$self GetSplineWidgetPoints]
    $plane TemporalRestoreFlowActors
    $self SetDirtyState 1
  }

  method GetSplineWidgetPoints { } {
    set points [[$SplineWidget GetRepresentation] GetHandlePositions]
    set result [list]
    for {set i 0} {$i < [$points GetNumberOfTuples]} {incr i} {
      lappend result [$points GetTuple3 $i]
    }
    return $result
  }

  method OnPlaneWidgetStartInteraction { } {
    set plane [$self GetActivePlane]
    $plane TemporalDisableFlowActors
  }

  method OnPlaneWidgetInteraction { } {
    $self RepositionSplineWidget
    set plane [$self GetActivePlane]
    foreach P {Origin Point1 Point2} {
      $plane PlaneSource Set${P} {*}[$PlaneWidget Get${P}]
    }
  }

  method RepositionSplineWidget { } {
    set spline [$SplineWidget GetRepresentation]
    $spline SetProjectionPosition 0.0
  }

  method AlignPlaneToAxis { name axis } {
    set plane [$self GetPlaneObject $name]
    if {$plane eq ""} {
      return
    }
    $plane AlignToAxis $axis
    $self UpdatePlaneWidgetFromPlane $plane
    $self UpdateSplineWidgetFromPlane $plane
    hook call $self <Render>
  }

  method OnPlaneWidgetEndInteraction { } {
    catch {
    set plane [$self GetActivePlane]
    $plane ChangePlane [$PlaneWidget GetOrigin] \
        [$PlaneWidget GetPoint1] [$PlaneWidget GetPoint2]
    #REVIEW: the SplineWidget does not project the same as
    #vtkSplineProjectOnPlane
    $plane SetSplinePoints [$self GetSplineWidgetPoints] 
    $plane TemporalRestoreFlowActors
    $self SetDirtyState 1
    } msg
    puts "OnPlaneWidgetEndInteraction: $msg"
  }

  method OnActivateItem { } {
    set icn [Icons GetInstance]    
    puts "OnActivateItem: [$Widgets(tree,planes) index active]"
    set name [$self GetPlaneName active]
    set objPlane $PlaneCollection($name)
    puts "objPlane = $objPlane"
    set imgON [$icn Get light_bulb_on -size 16]
    set imgOFF [$icn Get light_bulb_off -size 16]
    if {[$objPlane GetProbePlaneVisibility]} {
      $Widgets(button,PlaneOnOff) configure -image $imgON
    } else {
      $Widgets(button,PlaneOnOff) configure -image $imgOFF
      # hide vtk widgets because active plane is not visible
      $PlaneWidget SetEnabled 0
      $SplineWidget SetEnabled 0
      $SplineWidget Render
    }
    foreach o {HedgeHog Streamline Pathline} {
      if {[$objPlane Get${o}Visibility]} {
        $Widgets(button,${o}OnOff) configure -image $imgON
      } else {
        $Widgets(button,${o}OnOff) configure -image $imgOFF
      }
    }
    $self UpdatePlaneWidgetFromPlane $objPlane
    $self UpdateSplineWidgetFromPlane $objPlane
    if {[$objPlane GetProbePlaneVisibility]} {
      # the active plane is visible, so set the widget visibility
      # depending on OnOff
      $PlaneWidget SetEnabled $WidgetOnOff
      $SplineWidget SetEnabled $WidgetOnOff
      if {[$PlaneWidget GetEnabled]} {
        hook call $self <Render>
      }
    }
    set TableData(current,edited) $name
    foreach typeLT {Plane Flow} {
      $Widgets(menubutton,ColorPreset${typeLT}) configure \
          -image [$icn Get [$objPlane GetColorPreset $typeLT]]
    }
    $self UpdateActiveProperties
    $self PlotVariable "flow"
    hook call $self <ChangePlane>
  }

  method GetActivePlane { } {
    set name [$self GetPlaneName active]
    if {$name ne ""} {
      return $PlaneCollection($name)
    }
    return ""
  }

  method GetPlaneObject { {name ""} } {
    if {$name eq ""} {
      set name [$self GetPlaneName active]
    }
    return [expr {$name ne "" ? $PlaneCollection($name) : ""}]
  }

  method GetPlaneName { item } {
    set idx [$Widgets(tree,planes) item id $item]
    if {$idx == 0} {
      return ""
    } else {
      return [$Widgets(tree,planes) item element cget $idx "tag NAME" eTXT -text]
    }
  }

  method CmdPlaneOnOff { } {
    set planeActive [$self GetActivePlane]
    if {$planeActive eq ""} {
      return
    }
    set state [$self SwitchButtonOnOff PlaneOnOff]

    if {0} {
      set icn [Icons GetInstance]
      if {[$plane GetProbePlaneVisibility]} {
        $self ChangePlaneState $plane 0
        set img [$icn Get light_bulb_off -size 16]
      } else {
        $self ChangePlaneState $plane 1
        set img [$icn Get light_bulb_on -size 16]
      }
      $Widgets(button,PlaneOnOff) configure -image $img
    }

    foreach item [$Widgets(tree,planes) selection get] {
      set name [$self GetPlaneName $item]
      set plane [$self GetPlaneObject [$self GetPlaneName $item]]
      $plane SetProbePlaneVisibility $state
      if { $plane eq $planeActive } {
        $PlaneWidget SetEnabled [expr {$state?$WidgetOnOff:0}]
        $SplineWidget SetEnabled [expr {$state?$WidgetOnOff:0}]
      }
    }
    hook call $self <Visibility>
    hook call $self <Render>
 }

  method OnChangeCurrentPlane { name } {
    set TableData(current,selected) $name
    set TableData(current,edited) $name
  }

  method SetCurrentEditedName { name } {
    set TableData(current,edited) $name
  }

  method GetCurrentEditedName { } {
    return $TableData(current,edited)
  }

  method SetDirtyState { state } {
    set name [$self GetPlaneName active] 
    if {$name ne ""} {
      $self SetDirtyStateForPlane $name $state
    }
  }

  method SetDirtyStateForPlane { name state } {
    set icn [Icons GetInstance]
    set imgNOR [$icn Get tick -size 16]
    set imgMOD [$icn Get pencil3 -size 16]
    set I [list tag "--${name}--"]
        set C [list tag DIRTY]
    $Widgets(tree,planes) tree item element configure \
        $I $C eIMG -image [expr {$state?$imgMOD:$imgNOR}]
  }

  method ChangePlaneItemName { currentName newName } {
    set tag1 "--${currentName}--"
    set tag2 "--${newName}--"
    set I [list tag $tag1]
    set id [$Widgets(tree,planes) tree item id $I]
    if {$id == 0 || $id eq ""} {
      return
    }
    set C [list tag "NAME"]
    $Widgets(tree,planes) tree item element configure \
        $id $C eTXT -text $newName
    $Widgets(tree,planes) item tag remove $id $tag1
    $Widgets(tree,planes) item tag add $id $tag2
  }

  method GetTagMapping { } {
    return \
        {MINVEL min_velocity
          MAXVEL max_velocity 
          FLOWPOS flow_forward 
          FLOWNEG flow_backward 
          FLOWRES flow 
          ACCVOL flow_accum}
  }

  method GetFlowUnknown { } {
    return {MINVEL  ?
      MAXVEL  ?
      FLOWPOS ?
      FLOWNEG ?
      FLOWRES ?
      ACCVOL  ?}
  }

  method UpdatePlaneItem { name args } {
    array set opts $args
    set I [list tag "--${name}--"]
    set cmdConfigure "$Widgets(tree,planes) tree item element configure [list $I]"
    set first 1
    foreach tag {MINVEL MAXVEL FLOWPOS FLOWNEG FLOWRES ACCVOL} {
      if {[info exists opts($tag)]} {
        if {!$first} {
          append cmdConfigure " ,"
        } else {
          set first 0
        }
        set C [list tag $tag]
        append cmdConfigure " [list $C] eTXT -text [list $opts($tag)]"
      }
    }
    {*}$cmdConfigure
  }

  method insertPlaneInTable { name args } {
    array set opts {
      STATE   0
      DIRTY   0
      MINVEL  ?
      MAXVEL  ?
      FLOWPOS ?
      FLOWNEG ?
      FLOWRES ?
      ACCVOL  ?
    }
    array set opts $args
    set icn [Icons GetInstance]
    set imgON [$icn Get light_bulb_on -size 16]
    set imgOFF [$icn Get light_bulb_off -size 16]
    set imgNOR [$icn Get tick -size 16]
    set imgMOD [$icn Get pencil3 -size 16]
    set id [$Widgets(tree,planes) insert \
                [list [list $name] \
                     [list [expr {$opts(DIRTY)?$imgMOD:$imgNOR}]] \
                     [list $opts(MINVEL)] \
                     [list $opts(MAXVEL)] \
                     [list $opts(FLOWPOS)] \
                     [list $opts(FLOWNEG)] \
                     [list $opts(FLOWRES)] \
                     [list $opts(ACCVOL)]]]
    $Widgets(tree,planes) item tag add $id "--${name}--"
  }
  
  method GetPlaneRootPath { } {
    if {$DataSet eq ""} {
      error [mc "data-set is empty"]
    }
    set studyDir [$DataSet GetDirectory]
    if {$studyDir eq ""} {
      error "study directory is empty"
    }
    set planeDir [file join $studyDir FlowQuantify]
    if {[file exist $planeDir]} {
      if {![file isdirectory $planeDir]} {
        error "'$planeDir' should be a directory"
      }
    } else {
      file mkdir $planeDir
    }
    return $planeDir
  }

  method GetPlanePath {name} {
    set planeRootDir [$self GetPlaneRootPath]
    return [file join $planeRootDir "${name}.xml"]
  }

  method GetAllStoredPlanes { } {
    set planeRootDir [$self GetPlaneRootPath]
    set fileList [lsort [glob -nocomplain -dir $planeRootDir *.xml]]
    set result {}
    foreach f $fileList {
      set tail [file tail $f]
      lappend result [string range $tail 0 end-4]
    }
    return $result
  }

  method destroyPlaneCollection { } {
    $PropPicker InitializePickList
    foreach idx [array names PlaneCollection] {
      $PlaneCollection($idx) destroy
    }
    array unset PickPlaneMapping
    array unset PlaneCollection
  }

  method OnLeftButtonPressEvent { } {
    if {![$self GetVisibility]} {
      return
    }
    puts "$self OnLeftButtonPressEvent"
    puts "=== START ==="
    set ePos [[$Renderer GetInteractor] GetEventPosition]
    puts "$PropPicker $ePos 0 [$Renderer ImageRender GetActualRenderer]"
    #puts [$PropPicker PickProp {*}$ePos [$Renderer ImageRender GetActualRenderer]]
    puts [$PropPicker Pick {*}$ePos 0 [$Renderer ImageRender GetActualRenderer]]
    puts "Actor = [$PropPicker GetActor]"
    set actor [$PropPicker GetActor]
    if {[info exists PickPlaneMapping($actor)]} {
      $self SelectActivePlane $PickPlaneMapping($actor)
    } else {
      puts "The actor $actor is not registered"
    }
    puts "=== END ==="
  }

  method SelectActivePlane { plane } {
    set I [list tag "--${plane}--"]
    puts "I = $I"
    puts "objPlane = [$self GetPlaneObject $plane]"
    $Widgets(tree,planes) selection clear
    $Widgets(tree,planes) selection add $I
    $Widgets(tree,planes) selection anchor $I
    $Widgets(tree,planes) activate $I
    $Widgets(tree,planes) see $I
  }

  method UpdatePlaneWidgetFromPlane { objPlane } {
    $PlaneWidget SetOrigin {*}[$objPlane PlaneSource GetOrigin]
    $PlaneWidget SetPoint1 {*}[$objPlane PlaneSource GetPoint1]
    $PlaneWidget SetPoint2 {*}[$objPlane PlaneSource GetPoint2]
    $PlaneWidget UpdatePlacement
  }

  method UpdateSplineWidgetFromPlane { objPlane } {
    set points [$objPlane GetSplinePoints]
    set spline [$SplineWidget GetRepresentation]
    $spline SetNumberOfHandles [llength $points]
    set i 0
    foreach p $points {
      $spline SetHandlePosition $i {*}$p
      incr i
    }
  }

  method ActivateControlWidgets { state } {
  }

  method CmdAlignPlane { view } {
    set name [$self GetPlaneName active]
    if {$name ne ""} {
      $self AlignPlaneToAxis $name [{*}$VolumeOrienter GetAxisFromView $view]
    }
  }

  method CmdResetSplinePosition { } {
    set plane [$self  GetActivePlane]
    if {$plane ne ""} {
      $plane ResetSplinePosition
      $self UpdateSplineWidgetFromPlane $plane
      hook call $self <Render>
    }
  }

  method CmdResetAllSplinePosition { } {
    set plane [$self  GetActivePlane]
    if {$plane ne ""} {
      $plane ResetAllSplinePosition
      $self UpdateSplineWidgetFromPlane $plane
      hook call $self <Render>
    }
  }

  method CmdCopySplineAll { } {
    set plane [$self  GetActivePlane]
    if {$plane ne ""} {
      $plane CopySplineAll
      hook call $self <Render>
    }
  }

  method CmdCopySplineForward { } {
    set plane [$self  GetActivePlane]
    if {$plane ne ""} {
      $plane CopySplineForward
      hook call $self <Render>
    }
  }

  method CmdCopySplineNext { } {
    set plane [$self  GetActivePlane]
    if {$plane ne ""} {
      $plane CopySplineNext
      hook call $self <Render>
    }
  }

  method CmdCopySplineBackward { } {
    set plane [$self  GetActivePlane]
    if {$plane ne ""} {
      $plane CopySplineBackward
      hook call $self <Render>
    }
  }

  method CmdCopySplinePrevious { } {
    set plane [$self  GetActivePlane]
    if {$plane ne ""} {
      $plane CopySplinePrevious
      hook call $self <Render>
    }
  }

  method CmdInvertPlaneNormal { } {
    set plane [$self GetActivePlane]
    if {$plane ne ""} {
      $plane InvertNormal
      $PlaneWidget SetNormal {*}[$plane PlaneSource GetNormal]
      hook call $self <Render>
      $self SetDirtyState 1
    }
  }

  method CmdNewPlane { } {
    if {$DataSet eq "" || ![$DataSet HasData]} {
      return
    }
    set name [$self NewPlane]
    $PlaneCollection($name) InitializeDefault
    $PlaneCollection($name) ChangeTimeStep [$DataSet GetCurrentTimeStep]
  }

  method RemovePlane { name } {
    set I [list tag "--${name}--"]
    $Widgets(tree,planes) item delete $I
    set plane $PlaneCollection($name)
    set actor [$plane GetProbeActor]
    $PropPicker DeletePickList $actor
    array unset PickPlaneMapping $actor
    array unset PlaneCollection $name
    $plane destroy
    set planePath [$self GetPlanePath $name]
    if {[file exists $planePath]} {
      file delete $planePath
    }
    hook call $self <Status> [mc "Plane %s removed" $name]
    hook call $self <Render> 
    #REVIEW: if removed plane was visible then it should be updated
    #the visibility of the manager
  }

  method CmdRemovePlane { } {
    set name [$self GetPlaneName active]
    if {$name eq ""} {
      return
    }
    set ans [MessageDlg $win.ask -title [mc "Confirm remove"] \
                 -icon warning \
                 -type okcancel -aspect 75 \
                 -message [mc "Do you really want to remove plane '%s'?" $name]]
    if {$ans == 1} {
      return
    }
    $self RemovePlane $name
  }

  method CmdRenamePlane { } {
    set newName [string trim $TableData(current,edited)]
    set currentName [$self GetPlaneName active]
    if {$currentName eq "" || $newName eq "" || $newName eq $currentName} {
      return
    }
    set idCheck [$Widgets(tree,planes) item id [list tag "--${newName}--"]]
    puts "idCheck = $idCheck"
    if {$idCheck != 0 && $idCheck ne ""} {
      MessageDlg $win.ask -title [mc "Error rename"] \
          -icon error \
          -type ok -aspect 75 \
          -message [mc "Unable to rename %s: the name '%s' is already in use" \
                        $currentName $newName]
      return
    }
    set currentPath [$self GetPlanePath $currentName]
    if {[file exists $currentPath]} {
      file rename $currentPath [$self GetPlanePath $newName]
    }
    set objPlane $PlaneCollection($currentName)
    array unset PlaneCollection $currentName
    set PlaneCollection($newName) $objPlane
    set actor [$objPlane GetProbeActor]
    set PickPlaneMapping($actor) $newName
    $self ChangePlaneItemName $currentName $newName
    hook call $self <Status> [mc "Plane %s renamed to %s" $currentName $newName]
  }

  method CmdStorePlane { } {
    set name [$self GetPlaneName active]
    if {$name eq ""} {
      return
    }
    set plane $PlaneCollection($name)
    $plane Store $name [$self GetPlanePath $name]
    $self SetDirtyState 0
    hook call $self <Status> [mc "Plane %s stored" $name]
  }

  method SwitchButtonOnOff { name } {
    set icn [Icons GetInstance]
    set imgON [$icn Get "light_bulb_on" -size 16]
    set imgOFF [$icn Get "light_bulb_off" -size 16]
    set img [$Widgets(button,$name) cget -image]
    if {$img eq $imgON} {
      set img $imgOFF
      set state 0
    } else {
      set img $imgON
      set state 1
    }
    $Widgets(button,$name) configure -image $img
    return $state
  }

  method CmdHedgeHogOnOff { } {
    set plane [$self GetActivePlane]
    if {$plane eq ""} {
      return
    }
    set state [$self SwitchButtonOnOff HedgeHogOnOff]
    foreach item [$Widgets(tree,planes) selection get] {
      set name [$self GetPlaneName $item]
      set plane [$self GetPlaneObject [$self GetPlaneName $item]]
      $plane SetHedgeHogVisibility $state
    }
    hook call $self <Render>
  }

  method CmdStreamlineOnOff { } {
    set plane [$self GetActivePlane]
    if {$plane eq ""} {
      return
    }
    set state [$self SwitchButtonOnOff StreamlineOnOff]
    foreach item [$Widgets(tree,planes) selection get] {
      set name [$self GetPlaneName $item]
      set plane [$self GetPlaneObject [$self GetPlaneName $item]]
      $plane SetStreamlineVisibility $state
    }
    hook call $self <Render>
  }

  method CmdPathlineOnOff { } {
    set plane [$self GetActivePlane]
    if {$plane eq ""} {
      return
    }
    set state [$self SwitchButtonOnOff PathlineOnOff]
    foreach item [$Widgets(tree,planes) selection get] {
      set name [$self GetPlaneName $item]
      set plane [$self GetPlaneObject [$self GetPlaneName $item]]
      $plane SetPathlineVisibility $state
    }
    hook call $self <Render>
  }

  method CmdWidgetOnOff { } {
    if {$WidgetOnOff} {
      $Widgets(button,WidgetOnOff) configure -relief raised \
          -helptext [mc "Activate plane & spline widgets"]
      set WidgetOnOff 0
    } else {
      $Widgets(button,WidgetOnOff) configure -relief sunken \
          -helptext [mc "Deactivate plane & spline widgets"]     
      set WidgetOnOff 1
    }
    set plane [$self GetActivePlane]
    if {$plane eq "" || ![$plane GetProbePlaneVisibility]} {
      $PlaneWidget SetEnabled 0
      $SplineWidget SetEnabled 0
    } else {
      $PlaneWidget SetEnabled $WidgetOnOff
      $SplineWidget SetEnabled $WidgetOnOff
    }
    $SplineWidget Render
  }
  
  method CmdUpdateProbeResolution { } {
    foreach name [array name PlaneCollection] {
      $PlaneCollection($name) SetProbeResolutionFactor [$self GetProbeResolutionFactor]
    }
    hook call $self <Render>
  }
 
  method CmdChangeLookupTable { preset typeLT } {
    set icn [Icons GetInstance]    
    $Widgets(menubutton,ColorPreset${typeLT}) configure \
        -image [$icn Get $preset]
    set plane [$self GetActivePlane]
    if {$plane eq ""} {
      return
    }
    foreach item [$Widgets(tree,planes) selection get] {
      set name [$self GetPlaneName $item]
      set plane [$self GetPlaneObject [$self GetPlaneName $item]]
      $plane SetColorPreset $preset $typeLT
    }
    hook call $self <Render>
  }

  method ApplyProperties { p } {
    set t [$p GetTargetType]
    $self Apply${t}Properties $p
    hook call $self <Render>
  }

  method UpdatePropertiesFromActivePlane { prop } {
    set plane [$self GetActivePlane]
    if {$plane eq ""} {
      return
    }
    set t [$p GetTargetType]
    $prop CopyPropertiesFrom $plane
  }

  method ApplyStreamlineProperties { p } {
    set plane [$self GetActivePlane]
    if {$plane eq ""} {
      return
    }
    array set directionMethod {
      FORWARD  SetIntegrationDirectionToForward
      BACKWARD SetIntegrationDirectionToBackward
      BOTH     SetIntegrationDirectionToBoth
    }
    array set integrationMethod {
      RUNGE_KUTTA2  SetIntegratorTypeToRungeKutta2
      RUNGE_KUTTA4  SetIntegratorTypeToRungeKutta4
      RUNGE_KUTTA45 SetIntegratorTypeToRungeKutta45
    }
    $plane Streamer SetTerminalSpeed [$p GetTerminalSpeed]
    $plane Streamer $directionMethod([$p GetDirection])
    $plane Streamer $integrationMethod([$p GetIntegratorType])
    $plane Streamer SetMaximumError [$p GetMaximumError]
    $plane Streamer SetMaximumPropagation [$p GetMaximumPropagation]
    $plane Streamer SetInitialIntegrationStep [$p GetInitialIntegrationStep]
    $plane Streamer SetMinimumIntegrationStep [$p GetMinimumIntegrationStep]
    $plane Streamer SetMaximumIntegrationStep [$p GetMaximumIntegrationStep]
    $plane Streamer SetMaximumNumberOfSteps [$p GetMaximumNumberOfSteps]
  }


  method ApplyLookupTableProperties { p } {
    set plane [$self GetActivePlane]
    if {$plane eq ""} {
      return
    }
    $plane SetLookupTableRange {*}[$p GetScalarRange]
  }

  method ApplyPathlineProperties { p } {
    set plane [$self GetActivePlane]
    if {$plane eq ""} {
      return
    }
    #$plane SetPathlineResolution [$p GetResolution]
    $plane SetPathlineTraceLength [$p GetTraceLength]
    $plane SetPathlineReinjectEvery [$p GetReinjectEvery]
    $plane UpdatePathlines
  }


  method WriteActiveLines { } {
    foreach name [array names PlaneCollection] {
      puts "checking plane $name"
      if {[$PlaneCollection($name) GetStreamlineVisibility]} {
        $PlaneCollection($name) WriteLines "/tmp/streamlines_${name}.vtk"
      }
    }
  }

  method WriteCenterLines { } {
    foreach name [array names PlaneCollection] {
      puts "checking plane $name"
      if {[$PlaneCollection($name) GetStreamlineVisibility]} {
        $PlaneCollection($name) WriteCenterLine "/tmp/centerline_${name}.vtk"
      }
    }
  }

  method GetLargestLine { filter } {
    # loop over all planes
    vtkPolyDataConnectivityFilter connectivityFilter
    connectivityFilter SetExtractionModeToLargestRegion
    vtkPolyDataWriter __writer
    __writer SetFileName "/tmp/all_streams.vtk"
    foreach name [array names PlaneCollection] {
      puts "checking plane $name"
      if {[$PlaneCollection($name) GetStreamlineVisibility]} {
        puts "using plane $name"
        __writer SetInputConnection [$PlaneCollection($name) Streamer GetOutputPort]
        __writer Update
        __writer Delete
        connectivityFilter SetInputConnection [$PlaneCollection($name) Streamer GetOutputPort]
        $filter SetInputConnection [connectivityFilter GetOutputPort]
        $filter Update
        connectivityFilter Delete
        return
      }
    }
  }

  method AppendStreamlinePoints { appendPoints } {
    # loop over all planes
    foreach name [array names PlaneCollection] {
      puts "checking plane $name"
      if {[$PlaneCollection($name) GetStreamlineVisibility]} {
        puts "using plane $name"
        $appendPoints AddInputConnection [$PlaneCollection($name) Streamer GetOutputPort]
      }
    }
    $appendPoints Update
  }

  method AppendActivePlanes { collection } {
    # loop over all planes
    foreach name [array names PlaneCollection] {
      puts "checking plane $name"
      set planeQuantifier $PlaneCollection($name)
      if {[$planeQuantifier GetProbePlaneVisibility]} {
        puts "using plane $name"
        set aux [vtkPlane New]
        $aux SetOrigin {*}[$planeQuantifier PlaneSource GetOrigin]
        $aux SetNormal {*}[$planeQuantifier PlaneSource GetNormal]
        $collection AddItem $aux
        $aux Delete
      }
    }
  }

  method BinFromStreamline { args } {
    array set opts {
      -divisions {100 100 50}
      -maxpts 20
      -file /tmp/BinBoundary.vtk
      -fileMask /tmp/BinMask.vtk
    }
    array set opts $args
    vtkAppendPoints appendPoints
    # loop over all planes
    foreach name [array names PlaneCollection] {
      puts "checking plane $name"
      if {[$PlaneCollection($name) GetStreamlineVisibility]} {
        puts "using plane $name"
        appendPoints AddInputConnection [$PlaneCollection($name) Streamer GetOutputPort]
      }
    }
    appendPoints Update
    vtkPointLocator bin
    bin SetDivisions {*}$opts(-divisions)
    bin SetDataSet [appendPoints GetOutput]
    bin AutomaticOff
    bin BuildLocator
    vtkPolyData boundary
    vtkImageData imageData
    bin GenerateRepresentation 0 boundary
    bin GenerateBinaryRepresentation 0 imageData
    #puts [bin Print]
    #puts [boundary Print]
    puts [imageData Print]

    vtkGenericDataObjectWriter writer
    writer SetInputData boundary
    writer SetFileName $opts(-file)
    writer Update

    writer SetInputData imageData
    writer SetFileName $opts(-fileMask)
    writer Update

    writer Delete
    appendPoints Delete
    bin Delete
    boundary Delete
    imageData Delete
  }

  method OctreeFromStreamlines { args } {
    array set opts {
      -maxlevel 8
      -maxpts 50
      -file /tmp/octreeBoundary.vtk
      -type polydata
    }
    array set opts $args
    catch {appendPoints Delete}
    vtkAppendPoints appendPoints
    catch {octree Delete}
    vtkOctreePointLocator octree
    octree SetMaxLevel $opts(-maxlevel)
    octree SetMaximumPointsPerRegion $opts(-maxpts)
    # loop over all planes
    foreach name [array names PlaneCollection] {
      puts "checking plane $name"
      if {[$PlaneCollection($name) GetStreamlineVisibility]} {
        puts "using plane $name"
        appendPoints AddInputConnection [$PlaneCollection($name) Streamer GetOutputPort]
      }
    }
    appendPoints Update
    octree SetDataSet [appendPoints GetOutput]
    octree BuildLocator
    if {[info exists opts(-level)]} {
      set level $opts(-level)
    } else {
      set level [octree GetLevel]
    }

    catch {octreeMesh Delete}
    if {$opts(-type) eq "polydata"} {
      vtkPolyData octreeMesh
    } else {
      vtkUnstructuredGrid octreeMesh
    }
    octree GenerateRepresentation $level octreeMesh
    puts "Octree $opts(-type) ---"
    puts [octreeMesh Print]
    catch {cleanMesh Delete}
    if {$opts(-type) eq "polydata"} {
      vtkCleanPolyData cleanMesh
      # only isolated points need to be removed
      cleanMesh PointMergingOff
    } else {
      vtkUnstructuredGridGeometryFilter cleanMesh
      puts [cleanMesh Print]
    }
    cleanMesh SetInputData octreeMesh
    cleanMesh Update
    puts "Clean PolyData ---"
    puts [[cleanMesh GetOutput] Print]

    catch {writer Delete}
    vtkGenericDataObjectWriter writer
   
    if {1} {
      writer SetInputConnection [cleanMesh GetOutputPort]
      writer SetFileName "/tmp/octreeBoundaryOriginal.vtk"
      writer Update
    }

    return 

    catch {smoother0 Delete}
    vtkWindowedSincPolyDataFilter smoother0
    smoother0 SetInputConnection [cleanMesh GetOutputPort]
    smoother0 SetNumberOfIterations 15
    smoother0 BoundarySmoothingOff
    smoother0 FeatureEdgeSmoothingOff
    #smoother0 SetFeatureAngle 120
    #smoother0 SetEdgeAngle 180
    smoother0 SetPassBand 0.01
    #smoother0 NonManifoldSmoothingOn
    smoother0 NormalizeCoordinatesOn

    writer SetInputConnection [smoother0 GetOutputPort]
    writer SetFileName "/tmp/octreeBoundarySmooth0.vtk"
    writer Update

    catch {decimate Delete}
    vtkDecimatePro decimate
    decimate SetInputConnection [triangulator GetOutputPort]
    decimate PreserveTopologyOn
    decimate SetTargetReduction 0.6
    decimate SetFeatureAngle 15
    decimate SetSplitAngle 60
    decimate PreSplitMeshOn
    decimate SplittingOn

    catch {smoother1 Delete}
    vtkSmoothPolyDataFilter smoother1
    smoother1 SetNumberOfIterations 400
    smoother1 FeatureEdgeSmoothingOff
    smoother1 SetRelaxationFactor 0.01
    smoother1 SetInputConnection [decimate GetOutputPort]
    
    writer SetInputConnection [smoother1 GetOutputPort]
    writer SetFileName $opts(-file)
    writer Update

    smoother1 Delete
    decimate Delete
    triangulator Delete
    smoother0 Delete
    cleanMesh Delete
    writer Delete
    octreeMesh Delete
    octree Delete
    appendPoints Delete
  }


}
