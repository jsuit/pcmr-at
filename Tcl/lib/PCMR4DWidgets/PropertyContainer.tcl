package require snit
package require hook
package require wcb

snit::widget PropertyContainer {
  variable DataSet ""
  variable Widgets -array {}

  delegate option * to hull
  delegate method * to hull

  proc checkEntryIntRange { min max w idx str } {
    set newText [wcb::postInsertEntryText $w $idx $str]
    if {![string is integer $newText] || 
        $newText < $min || $newText > $max} {
      wcb::cancel
    }
  }

  #
  # Callback procedure checkNumber
  #
  proc checkNumber {args} {
    puts "checkNumber $args"
    if {[llength $args] != 4} {
      return
    }
    foreach {w idx str} $args break
    set newText [wcb::postInsertEntryText $w $idx $str]
    if {![regexp {^[0-9]*\.?[0-9]?[0-9]?$} $newText]} {
      wcb::cancel
    }
  }

  proc checkRealBeforeDelete { w args } {
    set newText [wcb::postDeleteEntryText $w {*}$args]
    puts "checkRealBeforeDelete: '$newText'"
    if {$newText eq "" || ![string is double $newText]} {
      puts "voy a cancelar"
      wcb::cancel
    }
  }

  constructor { args } {
    $self configurelist $args
    set mf [$self buildMainFrame]
    set cf [$self buildControlFrame]
    grid $mf -row 0 -column 0 -sticky snew
    grid $cf -row 1 -column 0 -sticky ew
    grid rowconfigure $win 0 -weight 1
    grid columnconfigure $win 0 -weight 1
  }
  
  destructor {
  }

  method buildMainFrame { } {
    set Widgets(frame,main) [ttk::frame $win.main]
    return $Widgets(frame,main)
  }

  method GetMainFrame { } {
    return $Widgets(frame,main)
  }

  method DisableApply { } {
    $Widgets(button,apply) configure -state disable
  }

  method EnableApply { args } {
    $Widgets(button,apply) configure -state normal
  }

  method buildControlFrame { } {
    set f [ttk::frame $win.control -borderwidth 1 -relief ridge]
    set Widgets(button,apply) \
        [ttk::button $f.apply -text [mc "Apply"] -state disable \
             -command [mymethod CmdApply]]
    grid $f.apply -row 0 -column 0
    grid columnconfigure $f 0 -weight 1
    return $f
  }
  
  method CmdApply { } {
    hook call $win <PropertyApply> -container $win
    $Widgets(button,apply) configure -state disable
  }

  method SetDataSet { dataSet } {
    set DataSet $dataSet
    $dataSet AddObserver AfterOpenEvent [mymethod OnOpenDataSet]
    $dataSet AddObserver ChangeTimeStep [mymethod OnChangeTimeStep %T]
    $dataSet AddObserver BeforeCloseEvent [mymethod OnCloseDataSet]
  }

  method GetDataSet { } {
    return $DataSet
  }

  method OnOpenDataSet { } {
    catch {
      $win _OnOpenDataSet
    }
  }

  method OnChangeTimeStep { t } {
    catch {
      $win _OnChangeTimeStep $t
    }
  }

  method OnCloseDataSet { } {
    catch {
      $win _OnCloseDataSet
    }
  }
}
