package require vtk
package require snit
package require msgcat
package require hook
package require BWidget
package require Icons

namespace import msgcat::*

snit::widget SlicerManager {
  typevariable OtherAxis -array {
    X {Y Z}
    Y {X Z}
    Z {X Y}
  }

  variable Widgets

  option -imagerender -default "" -readonly yes

  hulltype ttk::frame

  constructor { args } {
    $self configurelist $args
    if {$options(-imagerender) eq ""} {
      error "Could not build $type, -imagerender must be provided"
    }
    set icn [Icons GetInstance]
    ttk::label $win.lbLink -text [mc "Opacity linked"]:
    ttk::button $win.linkOpacity -image [$icn Get "link" -size 16] -command [mymethod _OnLinkClick $win.lbLink $win.linkOpacity]
    #Button $win.linkOpacity -relief link -image [$icn Get "link" -size 16] -command [mymethod _OnLinkClick $win.lbLink $win.linkOpacity]
    set Widgets(button,LinkOpacity) $win.linkOpacity
    grid $win.lbLink -row 0 -column 0
    grid $win.linkOpacity -row 0 -column 1
    set r 1
    foreach a {X Y Z} {
      set child [$self _BuildUIForSlicer $a]
      set c 0
      foreach w $child {
        set s [expr {$c%2 ? "w":"e"}]
        grid $w -row $r -column $c -sticky $s
        incr c
      }
      incr r
    }
    grid rowconfigure $win $r -weight 1
    grid columnconfigure $win $c -weight 1
    # because $options(-imagerender) = $4DImageViewer ImageRender
    hook bind [lindex $options(-imagerender) 0] <ChangeImageOrientation> $self [mymethod HookChangeImageOrientation]
  }

  method DisableWidgets { } {
    $Widgets(button,LinkOpacity) configure -state disabled
    $Widgets(button,X) configure -state disabled
    $Widgets(scale,X) state disabled
    $Widgets(button,Y) configure -state disabled
    $Widgets(scale,Y) state disabled
    $Widgets(button,Z) configure -state disabled
    $Widgets(scale,Z) state disabled
  }

  method EnableWidgets { } {
    $Widgets(button,LinkOpacity) configure -state normal
    $Widgets(button,X) configure -state normal
    $Widgets(scale,X) state !disabled
    $Widgets(button,Y) configure -state normal
    $Widgets(scale,Y) state !disabled
    $Widgets(button,Z) configure -state normal
    $Widgets(scale,Z) state !disabled
  }

  method GetPlaneWidget { axis } {
    return [{*}$options(-imagerender) GetPlaneWidget $axis]
  }

  method _BuildUIForSlicer { axis } {
    set ir $options(-imagerender)
    set icn [Icons GetInstance]
    ttk::label $win.lb$axis -text "[{*}$ir GetPlaneName $axis]:"
    set Widgets(label,$axis) $win.lb$axis
    Button $win.btnOpacity$axis -relief link \
        -image [$icn Get "light_bulb_on" -size 16] \
        -command [mymethod _OnClickVisibility $axis]
    set Widgets(button,$axis) $win.btnOpacity$axis
    set plane [$self GetPlaneWidget $axis]
    set v [[$plane GetTexturePlaneProperty] GetOpacity]
    ttk::scale $win.scOpacity$axis -value $v \
        -command  [mymethod _OnSetOpacity $axis ]
    set Widgets(scale,$axis) $win.scOpacity$axis
    return [list $win.lb$axis $win.btnOpacity$axis $win.scOpacity$axis]
  }

  method _OnLinkClick { lb btn } {
    set icn [Icons GetInstance]
    set img [$btn cget -image]
    set imgLink [$icn Get "link" -size 16]
    set imgLinkBreak [$icn Get "link_break" -size 16]
    if {$img eq $imgLink} {
      set txt [mc "Opacity unlinked"]
      set img $imgLinkBreak
    } else {
      set txt [mc "Opacity linked"]
      set img $imgLink
    }
    $lb configure -text "${txt}:"
    $btn configure -image $img
  }

  method IsOpacityLinked { } {
    set icn [Icons GetInstance]
    set linked [$icn Get "link" -size 16]
    return [expr {[$win.linkOpacity cget -image] eq $linked}]
  }

  method _OnSetOpacity { axis v } {
    set plane [$self GetPlaneWidget $axis]
    [$plane GetTexturePlaneProperty] SetOpacity $v
    if {[$self IsOpacityLinked]} {
      foreach a $OtherAxis($axis) {
        set w $Widgets(scale,$a)
        $w configure -value $v
        set plane [$self GetPlaneWidget $a]
        [$plane GetTexturePlaneProperty] SetOpacity $v
      }
    }
    hook call $self <Render>
  }

  method GetLabel { } {
    return [mc "Slicers"]
  }

  method SetVisibility { v } {
    set icn [Icons GetInstance]
    set imgON [$icn Get "light_bulb_on" -size 16]
    set imgOFF [$icn Get "light_bulb_off" -size 16]
    foreach a {X Y Z} {
      set plane [$self GetPlaneWidget $a]
      $plane SetEnabled $v
      set w $Widgets(button,$a)
      if {$v} {
        $w configure -image $imgON
      } else {
        $w configure -image $imgOFF
      }
    }
  }

  method GetVisibility { } {
    set planeX [$self GetPlaneWidget X]
    set planeY [$self GetPlaneWidget Y]
    set planeZ [$self GetPlaneWidget Z]
    return [expr {[$planeX GetEnabled] || [$planeY GetEnabled] ||
                  [$planeZ GetEnabled]}]
  }

  method _OnClickVisibility { axis } {
    set icn [Icons GetInstance]
    set imgON [$icn Get "light_bulb_on" -size 16]
    set imgOFF [$icn Get "light_bulb_off" -size 16]
    set w $Widgets(button,$axis)
    set img [$w cget -image]
    if {$img eq $imgON} {
      set v 0
      set img $imgOFF
    } else {
      set v 1
      set img $imgON
    }
    $w configure -image $img
    $self SetPlaneVisibility $axis $v
  }

  method SetPlaneVisibility { axis v } {
    set previous [$self GetVisibility]
    set plane [$self GetPlaneWidget $axis]
    $plane SetEnabled $v
    hook call $self <Render>
    set current [$self GetVisibility]
    if {$current != $previous} {
      hook call $self <Visibility>
    }
  }

  method AddToRenderer { renderer } {
  }

  method SetDataSet { dataSet } {
    $dataSet AddObserver AfterOpenEvent [mymethod OnOpenDataSet]
    $dataSet AddObserver BeforeCloseEvent [mymethod OnCloseDataSet]
  }

  method _ReconfigureLabels { } {
    set ir $options(-imagerender)
    puts $ir
    foreach axis {X Y Z} {
      $win.lb$axis configure -text "[{*}$ir GetPlaneName $axis]:"
    }
  }

  method HookChangeImageOrientation { } {
    $self _ReconfigureLabels
  }

  method OnOpenDataSet { } {
    $self EnableWidgets
    $self SetVisibility 1
  }

  method OnCloseDataSet { } {
    $self DisableWidgets
  }
}
