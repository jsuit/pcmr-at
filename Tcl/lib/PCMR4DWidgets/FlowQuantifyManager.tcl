package require vtk
package require snit
package require msgcat
package require hook
package require VTKUtil
package require CScale
package require Icons
package require tdom
package require autoscroll
package require Pcmr4d
package require Plotchart
package require vtkTclAddon

namespace import msgcat::*

snit::widget FlowQuantifyManager {

  typevariable MagnitudeInfo -array {
    area,label "Contour Area"
    area,unit "mm^2"
    flow,label "Flow Rate"
    flow,unit "mL/s"
    flow_forward,label "Flow Rate (+)"
    flow_forward,unit "mL/s"
    flow_backward,label "Flow Rate (-)"
    flow_backward,unit "mL/s"
    min_velocity,label "Min. Velocity"
    min_velocity,unit "cm/s"
    max_velocity,label "Max. Velocity"
    max_velocity,unit "cm/s"
  }
  component PlaneWidget      -public PlaneWidget
  component ProbeFilter      -public ProbeFilter
  component HedgeHog         -public HedgeHog
  component VectorNorm       -public VectorNorm
  component VectorMapper     -public VectorMapper
  component VectorActor      -public VectorActor
  component ContourMapper    -public ContourMapper
  component ContourActor     -public ContourActor
  component LookupTable      -public LookupTable
 
  component ImplicitLoop     -public ImplicitLoop
  component ExtractCell      -public ExtractCell
  component CleanContour     -public CleanContour

  variable Renderer
  variable WidgetPlaced 0
  variable Widgets
  variable XYPlotData -array {}
  variable DataSet
  variable SplineCollection
  variable IndexActiveSpline -1
  variable NumberOfControlPoints 10
  variable PlaneElementSize 0.5
  variable LastPlanePosition -array {}
  variable FlowTableData -array {}
  variable TableEntryInfo -array {}
  variable ContourResolution 30
  variable InteractionCounter 0
  variable CurrentTimeStepTicks {}
  variable vtkChartObjects -array {}

  delegate option * to hull
  delegate method * to hull
  
  option -lookuptable -default "" -readonly yes
  
  constructor {args} {
    catch {
      set icn [Icons GetInstance]
      $self configurelist $args
      
      ttk::panedwindow $win.pwData -orient vertical
      grid $win.pwData -row 0 -column 0 -sticky snew
      grid rowconfigure $win 0 -weight 1
      grid columnconfigure $win 0 -weight 1
      set fTop [frame $win.pwData.fTop]
      $win.pwData add $fTop
      set fControl [frame $fTop.fControl -borderwidth 1 -relief ridge]
      grid $fControl -row 0 -column 0 -sticky snew
      grid rowconfigure $fTop 0 -weight 1
      grid columnconfigure $fTop 0 -weight 1
      grid columnconfigure $fTop 1 -weight 1
      set fBottom [frame $win.pwData.fBottom]
      $win.pwData add $fBottom
      grid rowconfigure $fBottom 0 -weight 1
      grid rowconfigure $fBottom 1 -weight 1
      grid columnconfigure $fBottom 0 -weight 1
      
      set Widgets(button,Reposition) [ttk::menubutton $fControl.btnReposition \
                                          -text [mc "Reposition splines"]: \
                                          -menu $fControl.btnReposition.m]
      $self _defineMenuReposition $fControl.btnReposition.m
      ttk::label $fControl.lbSplineSizeHandle -text [mc "Handle size:"]
      set Widgets(scale,SplineHandleSize) \
          [CScale $fControl.scSplineSizeHandle -from 0 -to 5.0 -value 5.0 \
               -command [mymethod CmdChangeSplineHandleSize]]
      ttk::label $fControl.lbPlaneName -text [mc "Plane name:"]
      set Widgets(entry,PlaneName) [ttk::entry $fControl.entPlaneName]
      Button $fControl.btnLoadPlane -image [$icn Get "folder_open" -size 16] \
          -command [mymethod CmdLoadPlane] \
          -helptext [mc "Load plane from disk"]
      set Widgets(button,LoadPlane) $fControl.btnLoadPlane
      Button $fControl.btnStorePlane -image [$icn Get "save" -size 16] \
          -command [mymethod CmdStorePlane] \
          -helptext [mc "Store plane in study"]
      set Widgets(button,StorePlane) $fControl.btnStorePlane
      Button $fControl.btnRemovePlane -image [$icn Get "document_delete" -size 16] \
          -command [mymethod CmdRemovePlane] \
          -helptext [mc "Remove plane from disk"]
      set Widgets(button,RemovePlane) $fControl.btnRemovePlane
      Button $fControl.btnInvertNormal -image [$icn Get "rotate_ccw" -size 16] \
          -command [mymethod CmdInvertPlaneNormal] \
          -helptext [mc "Invert plane normal"]
      set Widgets(button,InvertNormal) $fControl.btnInvertNormal
      ttk::label $fControl.lbScale -text [mc "Arrow scale"]:
      CScale $fControl.scFactor -command  [mymethod _OnSetFactor] -value 0.1
      set Widgets(scale,Factor) $fControl.scFactor
      set Widgets(button,OnOffHH) [Button $fControl.btnOnOffHH -relief link \
                                       -image [$icn Get "light_bulb_off" -size 16] \
                                       -command [mymethod _OnClickOnOffHH]]
      set Widgets(Tree,FlowTable) [wtree $fTop.table -font TkTextFont -table 1 -filter no -buttonstyle mac \
                                       -columns [list \
                                                     {text -tags MAGNITUDE -label "Magnitude"} \
                                                     {text -tags VALUE -label "Value"}]]
      $Widgets(Tree,FlowTable) column configure all -gridleftcolor gray70 -borderwidth 1 -font TkTextFont
      grid $Widgets(Tree,FlowTable) -row 0 -column 1 -sticky snew
      set Widgets(canvas,Plotchart) [$self _makeChartWidget $fBottom]
      set Widgets(vtkChart) [$self _makeChartWidget_VTK $fBottom]
      if {0} {
      set Widgets(canvas,Plotchart) \
          [canvas $fBottom.c -background gray90]
      Button $fBottom.btnRefreshChart \
          -image [$icn Get "Synchronize" -size 24] \
          -command [mymethod ComputeFlowTableForAllTimeStep] \
          -helptext [mc "Refresh time-chart"]
      place $fBottom.btnRefreshChart \
          -in $Widgets(canvas,Plotchart) -x 0 -y 0
      }

     #grid $Widgets(canvas,Plotchart) -row 0 -column 0 -sticky snew
      grid $Widgets(vtkChart) -row 1 -column 0 -sticky snew
      grid $fControl.lbPlaneName -row 0 -column 0 -sticky ew
      grid $fControl.entPlaneName -row 0 -column 1 -sticky ew
      grid $fControl.btnLoadPlane -row 0 -column 2 -sticky w
      grid $fControl.btnStorePlane -row 0 -column 3 -sticky w
      grid $fControl.btnRemovePlane -row 0 -column 4 -sticky w
      grid $fControl.btnInvertNormal -row 0 -column 5 -sticky w
      grid $fControl.lbSplineSizeHandle -row 1 -column 0 -sticky e
      grid $fControl.scSplineSizeHandle -row 1 -column 1 -sticky ew
      grid $fControl.btnReposition -row 2 -column 1 -sticky ew
      grid $fControl.lbScale -row 3 -column 0 -sticky e
      grid $fControl.scFactor -row 3 -column 1 -sticky ew
      grid $Widgets(button,OnOffHH) -row 3 -column 2 -sticky w
      grid rowconfigure $fControl 4 -weight 1
      grid columnconfigure $fControl 5 -weight 1
      bind $Widgets(canvas,Plotchart) <Configure> [mymethod OnResizeChart]
      
      $self _buildVTKObjects
    } msg
    puts "AAAAAAAAAAAAA: $msg"
    puts "Quantify Object is $self"
  }

  destructor {
    $VectorActor Delete
    $VectorMapper Delete
    $ContourActor Delete
    $ContourMapper Delete
    $VectorNorm Delete
    $ProbeFilter Delete
    $HedgeHog Delete
    $self _destroySplineCollection
    $PlaneWidget Delete
    if {$LookupTable ne ""} {
      $LookupTable Delete
    }
    $ExtractoCell Delete
    $ImplicitLoop Delete
  }

  method DisableWidgets { } {
    $Widgets(entry,PlaneName) configure -state disabled
    $Widgets(button,LoadPlane) configure -state disabled
    $Widgets(button,StorePlane) configure -state disabled
    $Widgets(button,RemovePlane) configure -state disabled
    $Widgets(scale,SplineHandleSize) state disabled
    $Widgets(scale,Factor) state disabled
    $Widgets(button,OnOffHH) configure -state disabled
    $Widgets(button,Reposition) state disabled
  }

  method EnableWidgets { } {
    $Widgets(entry,PlaneName) configure -state normal
    $Widgets(button,LoadPlane) configure -state normal
    $Widgets(button,StorePlane) configure -state normal
    $Widgets(button,RemovePlane) configure -state normal
    $Widgets(scale,SplineHandleSize) state !disabled
    $Widgets(scale,Factor) state !disabled
    $Widgets(button,OnOffHH) configure -state normal
    $Widgets(button,Reposition) state !disabled
  }

  method _makeChartWidget { f } {
    set icn [Icons GetInstance]
    set w [canvas $f.c -background gray90]
    Button $f.btnRefreshChart \
        -image [$icn Get "Synchronize" -size 24] \
        -command [mymethod ComputeFlowTableForAllTimeStep] \
        -helptext [mc "Refresh time-chart"]
    place $f.btnRefreshChart -in $w -relx 1 -y 0 -anchor ne
    return $w
  }

  method _makeChartWidget_VTK { f } {
    set icn [Icons GetInstance]
    set w [vtkTkRenderWidget $f.tkrw]
    ::vtk::bind_tk_render_widget $w
    set renWin [$w GetRenderWindow]
    set view [vtkContextView $w.view]
    set vtkChartObjects(view) $view
    $view SetRenderWindow $renWin
    set top [winfo toplevel $f]
    wm protocol $top WM_DELETE_WINDOW [string map "%W $top %V $view" {
      $view Delete
      destroy $top
    }]
    Button $w.btnRefreshChart \
        -image [$icn Get "Synchronize" -size 24] \
        -command [mymethod ComputeFlowTableForAllTimeStep] \
        -helptext [mc "Refresh time-chart"]
    place $w.btnRefreshChart -in $w -relx 1 -y 0 -anchor ne
    # REVIEW: the array objects can be deleted after they are added to
    # the table
    set vtkChartObjects(table,flow) [vtkTable $w.tableFlow]
    set vtkChartObjects(array,time) [vtkFloatArray $w.arrTime]
    $vtkChartObjects(array,time) SetName "Time"
    set vtkChartObjects(array,flow) [vtkFloatArray $w.arrFlow]
    $vtkChartObjects(array,flow) SetName "Flow"
    $vtkChartObjects(table,flow) AddColumn $vtkChartObjects(array,time)
    $vtkChartObjects(table,flow) AddColumn $vtkChartObjects(array,flow)
    set vtkChartObjects(chartXY) [vtkChartXY $w.chart]
    [$view GetScene] AddItem $vtkChartObjects(chartXY) 
    set axisX [$vtkChartObjects(chartXY) GetAxis 1]
    vtkAxisAddon $axisX SetTitle "Seconds"
    set axisY [$vtkChartObjects(chartXY) GetAxis 0]
    vtkAxisAddon $axisY SetTitle "$MagnitudeInfo(flow,label) $MagnitudeInfo(flow,unit)"

    set vtkChartObjects(plot,flow) [$vtkChartObjects(chartXY) AddPlot 0]
    $vtkChartObjects(plot,flow) SetInputData $vtkChartObjects(table,flow) 0 1

    set vtkChartObjects(table,flowMark) [vtkTable $w.tableFlowMark]
    set vtkChartObjects(array,timeMark) [vtkFloatArray $w.arrTimeMark]
    $vtkChartObjects(array,timeMark) SetName "Time"
    set vtkChartObjects(array,flowMark) [vtkFloatArray $w.arrFlowMark]
    $vtkChartObjects(array,flowMark) SetName "Flow"
    $vtkChartObjects(array,timeMark) SetNumberOfTuples 1
    $vtkChartObjects(array,flowMark) SetNumberOfTuples 1
    $vtkChartObjects(table,flowMark) AddColumn $vtkChartObjects(array,timeMark)
    $vtkChartObjects(table,flowMark) AddColumn $vtkChartObjects(array,flowMark)
    set vtkChartObjects(plot,flowMark) [$vtkChartObjects(chartXY) AddPlot 1]
    $vtkChartObjects(plot,flowMark) SetInputData $vtkChartObjects(table,flowMark) 0 1
    $vtkChartObjects(plot,flowMark) SetColor 255 0 0 255
    $vtkChartObjects(plot,flowMark) SetMarkerSize 7
    $vtkChartObjects(plot,flowMark) SetMarkerStyle 4

    return $w
  }

  method _defineMenuReposition { m } {
    menu $m -tearoff no
    $m add command \
        -label [mc "Reset current spline position"] \
        -command [mymethod ResetSplinePosition]
    $m add command \
        -label [mc "Reset all spline position"] \
          -command [mymethod ResetAllSplinePosition]
    $m add command \
        -label [mc "All equals to current"] \
        -command [mymethod CopySplineAll]
    $m add command \
        -label [mc "All next equal to current"] \
        -command [mymethod CopySplineForward]
    $m add command \
        -label [mc "Next equal to current"] \
        -command [mymethod CopySplineNext]
    $m add command \
          -label [mc "All previous equal to current"] \
        -command [mymethod CopySplineBackward]
    $m add command \
        -label [mc "Previous equal to current"] \
        -command [mymethod CopySplinePrevious]
  }

  method _destroySplineCollection {} {
    foreach i [array names SplineCollection] {
      $SplineCollection($i) Delete
    }
    array unset SplineCollection
    set IndexActiveSpline -1
  }

  method SetVisibility {v} {
    puts $PlaneWidget
    set icn [Icons GetInstance]
    set commands [list "$PlaneWidget SetEnabled" \
                      "$ContourActor SetVisibility" \
                      "$VectorActor SetVisibility"]
    set splineWidget [$self GetActiveSplineWidget]
    if {$splineWidget ne ""} {
      lappend commands "$splineWidget SetEnabled"
    }
    foreach c $commands { 
      {*}$c $v
    }
    set widgets {OnOffHH}
    set img [expr {$v ? [$icn Get "light_bulb_on" -size 16]:
                   [$icn Get "light_bulb_off" -size 16]}]
    foreach w $widgets {
      $Widgets(button,$w) configure -image $img
    }
    if {$v} {
      set t [$DataSet GetCurrentTimeStep]
      $self OnChangeTimeStep $t
      #after idle $self UpdateFlowTable
      #$self UpdateImplicitLoop
    }
  }

  method GetVisibility { } {
    set splineWidget [$self GetActiveSplineWidget]
    return [expr {[$PlaneWidget GetEnabled] || 
                  ($splineWidget ne "" && [$splineWidget GetEnabled]) ||
                  [$ContourActor GetVisibility] ||
                  [$VectorActor GetVisibility]}]
  }

  method SetPlaneWidgetVisibility {v} {
    set previous [$self GetVisibility]
    $PlaneWidget SetEnabled $v
    hook call $self <Render>
    set current [$self GetVisibility]
    if {$current != $previous} {
      hook call $self <Visibility>
    }
  }

  method SetContourVisibility {v} {
    set previous [$self GetVisibility]
    $ContourActor SetVisibility $v
    hook call $self <Render>
    set current [$self GetVisibility]
    if {$current != $previous} {
      hook call $self <Visibility>
    }
  }

  method SetSplineVisibility {v} {
    set previous [$self GetVisibility]
    set splineWidget [$self GetActiveSplineWidget]
    if {$splineWidget eq ""} {
      return
    }
    $splineWidget SetEnabled $v
    hook call $self <Render>
    set current [$self GetVisibility]
    if {$current != $previous} {
      hook call $self <Visibility>
    }
  }

  method SetHedgeHogVisibility {v} {
    set previous [$self GetVisibility]
    $VectorActor SetVisibility $v
    hook call $self <Render>
    set current [$self GetVisibility]
    if {$current != $previous} {
      hook call $self <Visibility>
    }
  }

  method _buildVTKObjects { } {
    # PlaneWidget
    install PlaneWidget using vtkPlaneWidget $win.planeWidget 
    $PlaneWidget NormalToXAxisOn
    $PlaneWidget SetResolution 50
    $PlaneWidget SetRepresentationToOutline
    $PlaneWidget SetHandleSize 0.01

    $PlaneWidget AddObserver EnableEvent [mymethod OnPlaneWidgetEnable]
    $PlaneWidget AddObserver StartInteractionEvent \
        [mymethod OnPlaneWidgetStartInteraction]
    $PlaneWidget AddObserver EndInteractionEvent \
        [mymethod OnPlaneWidgetEndInteraction]
    $PlaneWidget AddObserver InteractionEvent \
        [mymethod OnPlaneWidgetInteraction]
    
    # Filter to probe the velocity magnitude along a plane
    install ProbeFilter using vtkProbeFilter $win.probeFilter
    if {[VTKUtil::GetVTKMajorVersion] > 5} {
      $ProbeFilter SetInputConnection \
          [[$PlaneWidget GetPolyDataAlgorithm] GetOutputPort]
    } else {
      $ProbeFilter SetInput [[$PlaneWidget GetPolyDataAlgorithm] GetOutput]
    }
    # TBD. when changing time step
    #$ProbeFilter SetSource $imgVector
    puts "----------------------------------------------------"
    puts "ProbeFilter = $ProbeFilter"
    puts "----------------------------------------------------"
    
    # Velocity magnitude
    install VectorNorm using vtkVectorNorm $win.vectorNorm
    $VectorNorm NormalizeOff
    $VectorNorm SetInputConnection [$ProbeFilter GetOutputPort]

    # Mapper for velocity magnitude contour
    install ContourMapper using vtkPolyDataMapper $win.contourMapper
    #$ContourMapper SetInputConnection [$ExtractCell GetOutputPort]
    $ContourMapper SetInputConnection [$VectorNorm GetOutputPort]
    $ContourMapper UseLookupTableScalarRangeOn
    if {$options(-lookuptable) ne ""} {
      $ContourMapper SetLookupTable $options(-lookuptable)
    }

    # Actor for velocity magnitude contour
    install ContourActor using vtkActor $win.contourActor
    $ContourActor SetMapper $ContourMapper
    $ContourActor VisibilityOff


    # ImplicitLoop
    install ImplicitLoop using vtkImplicitSelectionLoop $win.implicitLoop
    $ImplicitLoop AutomaticNormalGenerationOff

    # ExtractCell
    install ExtractCell using vtkExtractPolyDataGeometry $win.extractCell
    $ExtractCell SetInputConnection [$VectorNorm GetOutputPort]
    
    # CleanContour: to remove isolated points.
    install CleanContour using vtkCleanPolyData $win.cleanContour
    $CleanContour SetInputConnection [$ExtractCell GetOutputPort]
    $CleanContour PointMergingOff

    # FILTER: HedgeHog
    install HedgeHog using vtkHedgeHog $win.hedgeHog
    $HedgeHog SetInputConnection [$CleanContour GetOutputPort]
    $HedgeHog SetScaleFactor [$self GetVectorFactorFromScale]

    # MAPPER: VectorMapper
    install VectorMapper using vtkPolyDataMapper $win.vectorMapper
    $VectorMapper SetInputConnection [$HedgeHog GetOutputPort]
    $VectorMapper UseLookupTableScalarRangeOn
    if {$options(-lookuptable) ne ""} {
      $VectorMapper SetLookupTable $options(-lookuptable)
    }
    # ACTOR: VectorActor
    install VectorActor using vtkActor $win.vectorActor
    $VectorActor SetMapper $VectorMapper

  }

  method GetLabel { } {
    return "Flow Quantify"
  }

  method AddToRenderer { renderer } {
    set Renderer $renderer
    $renderer AddActor $ContourActor
    $renderer AddActor $VectorActor
    $PlaneWidget SetInteractor [$renderer GetInteractor]
  }

  method IsActive { } {
    return [$self GetVisibility]
  }

  method SetDataSet { dataSet } {
    if {$options(-lookuptable) eq ""} {
      install LookupTable using vtkLookupTable $win.lookupTable
      $LookupTable SetHueRange 0.667 0.0
      $LookupTable SetVectorModeToMagnitude
      $ContourMapper SetLookupTable $LookupTable
      $VectorMapper SetLookupTable $LookupTable
    }
    set DataSet $dataSet
    $dataSet AddObserver AfterOpenEvent [mymethod OnOpenDataSet]
    $dataSet AddObserver ChangeTimeStep [mymethod OnChangeTimeStep %T]
    $dataSet AddObserver BeforeCloseEvent [mymethod OnCloseDataSet]
  }

  method OnOpenDataSet { } {
    if {$LookupTable ne ""} {
      $LookupTable SetRange 0 [$DataSet GetVelocityEncoding]
      $LookupTable Build
    }
    $self _initGraph_VTK
    $self EnableWidgets
  }

  method OnCloseDataSet { } {
    $self SetVisibility 0
    if {[VTKUtil::GetVTKMajorVersion] > 5} {
      $PlaneWidget SetInputData ""
    } else {
      $PlaneWidget SetInput ""
    }
    if {[VTKUtil::GetVTKMajorVersion] > 5} {
      $ProbeFilter SetSourceData ""
    } else {
      $ProbeFilter SetSource ""
    }
    $self _destroySplineCollection
    set WidgetPlaced 0
    #$self _initGraph
    $self _initGraph_VTK
    $self DisableWidgets
  }

  method _buildSplineWidget { t } {
    set splineWidget [vtkSplineWidget2 $win.splineWidget$t]
    set spline [$splineWidget GetRepresentation]
    # REVIEW: check that SplineCollection($t) does not exist
    if {[info exists SplineCollection($t)]} {
      error "SplineCollection($t) = $SplineCollection($t)"
    }
    set SplineCollection($t) $splineWidget
    $spline SetPlaneSource [$PlaneWidget GetPolyDataAlgorithm]
    $spline SetNumberOfHandles $NumberOfControlPoints
    $spline ProjectToPlaneOn
    $spline SetProjectionNormal 3
    $spline ClosedOn
    $splineWidget SetPriority 1
    $splineWidget SetInteractor [$Renderer GetInteractor]
    $splineWidget AddObserver BeginInteractionEvent \
        [mymethod OnSplineWidgetBeginInteraction]
    $splineWidget AddObserver InteractionEvent \
        [mymethod OnSplineWidgetInteraction]
    $splineWidget AddObserver EndInteractionEvent \
        [mymethod OnSplineWidgetEndInteraction]

    return $splineWidget
  }

  method _copySpline {fromSplineWidget toSplineWidget} {
    set fromSpline [$fromSplineWidget GetRepresentation]
    set toSpline [$toSplineWidget GetRepresentation]
    set coordinates [$fromSpline GetHandlePositions]
    for {set i 0} {$i < [$coordinates GetNumberOfTuples]} {incr i} {
      set hp [$coordinates GetTuple3 $i]
      $toSpline SetHandlePosition $i {*}$hp
    }
  }

  method GetActivePlaneInfo { } {
    set planeInfo {}
    foreach m {Origin Point1 Point2} {
      lappend planeInfo $m [$PlaneWidget Get$m]
    }
    return $planeInfo
  }

  method GetActiveSplineInfo { } {
    set splineWidget [$self GetActiveSplineWidget]
    set splineInfo {}
    if {$splineWidget ne ""} {
      set spline [$splineWidget GetRepresentation]
      set coordinates [$spline GetHandlePositions]
      for {set i 0} {$i < [$coordinates GetNumberOfTuples]} {incr i} {
        lappend splineInfo [$coordinates GetTuple3 $i]
      }
    }
    return $splineInfo
  }

  method InvertComputedFlowData { } {
    foreach idx [array names FlowTableData] {
      set FlowTableData($idx) [expr {-1.0*$FlowTableData($idx)}]
    }
  }

  method InvertNormal { } {
    foreach {nx ny nz} [$PlaneWidget GetNormal] break
    $PlaneWidget SetNormal [expr {-1.0*$nx}] [expr {-1.0*$ny}] [expr {-1.0*$nz}]
    $self InvertComputedFlowData
    $self UpdateImplicitLoop
    $self UpdateFlowTable
    $self PlotMagnitude_VTK flow
    hook call $self <Render>
  }

  method CmdInvertPlaneNormal { } {
    $self InvertNormal
  }

  method CopySplineForward { } {
    set splineWidget [$self GetActiveSplineWidget]
    set T [$DataSet GetNumberOfTimeStep]
    for {set i [expr {$IndexActiveSpline + 1}]} {$i < $T} {incr i} {
      $self _copySpline $splineWidget $SplineCollection($i)
    }
  }

  method CopySplineNext { } {
    set splineWidget [$self GetActiveSplineWidget]
    set next [expr {$IndexActiveSpline + 1}]
    if {$next < [$DataSet GetNumberOfTimeStep]} {
      $self _copySpline $splineWidget $SplineCollection($next)
    }
  }

  method CopySplineBackward { } {
    set splineWidget [$self GetActiveSplineWidget]
    for {set i [expr {$IndexActiveSpline - 1}]} {$i >= 0} {incr i -1} {
      $self _copySpline $splineWidget $SplineCollection($i)
    }
  }

  method CopySplinePrevious { } {
    set splineWidget [$self GetActiveSplineWidget]
    set previous [expr {$IndexActiveSpline - 1}]
    if {$previous >= 0} {
      $self _copySpline $splineWidget $SplineCollection($previous)
    }
  }

  method CopySplineAll { } {
    set splineWidget [$self GetActiveSplineWidget]
    set T [$DataSet GetNumberOfTimeStep]
    for {set i 0} {$i < $T} {incr i} {
      if {$i != $IndexActiveSpline} {
        $self _copySpline $splineWidget $SplineCollection($i)
      }
    }
  }

  method _buildRegularPolygonSource { } {
    set algPoly [vtkRegularPolygonSource New]
    $algPoly SetCenter {*}[$PlaneWidget GetCenter]
    $algPoly SetNormal {*}[$PlaneWidget GetNormal]
    foreach {s1 s2} [VTKUtil::GetPlaneSizes $PlaneWidget] break
    if {$s1 < $s2} {
      set d $s1
    } else {
      set d $s2
    }
    $algPoly SetRadius [expr {$d/2.0}]
    $algPoly GeneratePolygonOff
    $algPoly GeneratePolylineOn
    $algPoly SetNumberOfSides $NumberOfControlPoints
    $algPoly Update
    return $algPoly
  }

  method ResetSplinePosition { } {
    set polygonSource [$self _buildRegularPolygonSource]
    set polygon [$polygonSource GetOutput]
    set n [$polygon GetNumberOfPoints]
    set splineWidget [$self GetActiveSplineWidget]
    set spline [$splineWidget GetRepresentation]
    for {set i 0} {$i < $n} {incr i} {
      $spline SetHandlePosition $i {*}[$polygon GetPoint $i]
    }
    $polygonSource Delete
    # REVIEW: should I destroy $polygon?, NO!
    $splineWidget Render
  }

  method ResetAllSplinePosition { } {
    set polygonSource [$self _buildRegularPolygonSource]
    set polygon [$polygonSource GetOutput]
    set n [$polygon GetNumberOfPoints]
    set T [$DataSet GetNumberOfTimeStep]
    for {set t 0} {$t < $T} {incr t} {
      set splineWidget $SplineCollection($t)
      set spline [$splineWidget GetRepresentation]
      for {set i 0} {$i < $n} {incr i} {
        $spline SetHandlePosition $i {*}[$polygon GetPoint $i]
      }
    }
    $polygonSource Delete  
    # REVIEW: should I destroy $polygon?, NO!
    set splineWidget [$self GetActiveSplineWidget]
    if {$splineWidget ne ""} {
      $splineWidget Render
    }
  }

  method _CmdOnSelectPlaneName { lb } {
    if {[llength [$lb curselection]]} {
      $Widgets(dialog,LoadPlane) itemconfigure 0 -state normal
    } else {
      $Widgets(dialog,LoadPlane) itemconfigure 0 -state disabled
    }
  }

  method _SelectPlaneFromStudy { } {
    set baseDir [$self _GetStorageBaseDir]
    if {$baseDir eq ""} {
      return ""
    }
    set w $win.dlgImport
    if {![winfo exists $w ]} {
      set wparent [winfo toplevel $win]
      set bg [ $wparent cget -background ]
      Dialog $w -title [ mc "Select stored plane" ]  \
          -parent $wparent -modal local -cancel 1 -separator 1 \
          -background $bg -transient false
      set Widgets(dialog,LoadPlane) $w
      $w add -text [ mc "Load" ] -state disabled -background $bg
      $w add -text [ mc "Close" ] -background $bg
      set f [$w getframe]
      set lb [listbox $f.lb]
      set xscroll [ttk::scrollbar $f.xscroll \
                       -orient horizontal -command [list $lb xview]]
      $lb configure -xscrollcommand [list $xscroll set]
      set yscroll [ttk::scrollbar $f.yscroll \
                       -orient vertical -command [list $lb yview]]
      $lb configure -yscrollcommand [list $yscroll set]
      grid $f.lb -row 0 -column 0 -sticky snew
      grid $xscroll -row 1 -column 0 -sticky ew
      grid $yscroll -row 0 -column 1 -sticky ns
      autoscroll::autoscroll $xscroll
      autoscroll::autoscroll $yscroll
      grid columnconfigure $f 0 -weight 1
      grid rowconfigure $f 0 -weight 1
      bind $lb <<ListboxSelect>> [mymethod _CmdOnSelectPlaneName $lb]
    } else {
      set lb "[$w getframe].lb"
    }
    $Widgets(dialog,LoadPlane) itemconfigure 0 -state disabled
    $lb delete 0 end
    set fileList [lsort [glob -nocomplain -dir $baseDir *.xml]]
    foreach f $fileList {
      set tail [file tail $f]
      $lb insert end [string range $tail 0 end-4]
    }
    set ans [$w draw]
    if {$ans == 0} {
      return [$lb get [$lb curselection]]
    } else {
      return ""
    }
  }

  method CmdLoadPlane { } {
    set plane [$self _SelectPlaneFromStudy]
    if {$plane eq ""} {
      return
    }
    $self _LoadPlaneFromFile $plane
    hook call $self <Status> [mc "Plane %s loaded" $plane]
    $self InvalidateFullRangeData
    $self UpdateFlowTable
    $self UpdateImplicitLoop
    hook call $self <Render>
  }

  method _LoadPlaneFromFile { planeName } {
    puts "_LoadPlaneFromFile $planeName"
    set pathXML [$self GetPlanePath $planeName]
    if {$pathXML eq ""} {
      return
    }
    set fd [open $pathXML]
    set bufferXML [string trim [read $fd]]
    close $fd
    set doc [dom parse $bufferXML]
    set root [$doc documentElement]
    set NumberOfControlPoints \
        [$doc selectNodes string(/QuantifyPlane/NumberOfHandles)]
    set handleSize [$doc selectNodes string(/QuantifyPlane/HandleSize)]
    puts "handleSize = $handleSize"
    if {$handleSize eq ""} {
      set handleSize 5.0
    }
    set planeOrigin [$doc selectNodes string(/QuantifyPlane/Plane/Origin)]
    set planePoint1 [$doc selectNodes string(/QuantifyPlane/Plane/Point1)]
    set planePoint2 [$doc selectNodes string(/QuantifyPlane/Plane/Point2)]
    $PlaneWidget SetOrigin {*}$planeOrigin
    $PlaneWidget SetPoint1 {*}$planePoint1
    $PlaneWidget SetPoint2 {*}$planePoint2

    set nodeContours [$root selectNodes Contours]

    foreach nodeSpline [$nodeContours childNodes] {
      set splineIndex [$nodeSpline getAttribute index]
      set splineWidget $SplineCollection($splineIndex)
      set spline [$splineWidget GetRepresentation]
      $spline SetHandleSize $handleSize
      foreach nodePoint [$nodeSpline childNodes] {
        set pointIndex [$nodePoint getAttribute index]
        set coord [$nodePoint selectNodes {string()}]
        $spline SetHandlePosition $pointIndex {*}$coord
      }
    }

    $Widgets(scale,SplineHandleSize) configure -value $handleSize
    set name [$root getAttribute name]
    $Widgets(entry,PlaneName) delete 0 end
    $Widgets(entry,PlaneName) insert 0 $name
  }

  method GetCurrentXMLPath { } {
    set name [$Widgets(entry,PlaneName) get]
    if {$name eq ""} {
      return
    }
    set path [$self GetPlanePath $name]
    return $path
  }

  method CmdStorePlane { } {
    set name [$Widgets(entry,PlaneName) get]
    set path [$self GetCurrentXMLPath]
    puts "GetCurrentXMLPath: $path"
    if {$path eq ""} {
      return
    }
    if {[file exists $path]} {
      set ans [MessageDlg $win.ask -title [mc "Confirm overwrite"] \
                   -icon warning \
                   -type okcancel -aspect 75 \
                   -message [mc "Plane '%s' already exist, overwrite?" $name]]
      if {$ans == 1} {
        return
      }
    }
    $self _StorePlaneInFile $name $path
    hook call $self <Status> [mc "Plane %s stored" $name]
  }

  method _StorePlaneInFile {name path} {
    set doc [dom createDocument QuantifyPlane]
    set root [$doc documentElement]
    $root setAttribute version 1.0
    $root setAttribute name $name
    # NumberOfHandles
    set subnode [$doc createElement NumberOfHandles]
    $subnode appendChild [$doc createTextNode $NumberOfControlPoints]
    $root appendChild $subnode
    # HandleSize
    set subnode [$doc createElement HandleSize]
    $subnode appendChild \
        [$doc createTextNode [$Widgets(scale,SplineHandleSize) get]]
    $root appendChild $subnode
    # Plane 
    set nodePlane [$doc createElement Plane]
    $root appendChild $nodePlane
    # Plane Origin
    set nodeOrigin [$doc createElement Origin]
    $nodePlane appendChild $nodeOrigin
    $nodeOrigin appendChild [$doc createTextNode [$PlaneWidget GetOrigin]]
    # Plane Point1
    set nodePoint1 [$doc createElement Point1]
    $nodePlane appendChild $nodePoint1
    $nodePoint1 appendChild [$doc createTextNode [$PlaneWidget GetPoint1]]
    # Plane Point2
    set nodePoint2 [$doc createElement Point2]
    $nodePlane appendChild $nodePoint2
    $nodePoint2 appendChild [$doc createTextNode [$PlaneWidget GetPoint2]]
    # Contours
    set nodeContour [$doc createElement Contours]
    $root appendChild $nodeContour
    set T [$DataSet GetNumberOfTimeStep]
    for {set t 0} {$t < $T} {incr t} {
      set nodeS [$doc createElement Spline]
      $nodeS setAttribute index $t
      $nodeContour appendChild $nodeS
      # iterate over control points
      set splineWidget $SplineCollection($t)
      set spline [$splineWidget GetRepresentation]
      set coordinates [$spline GetHandlePositions]
      for {set i 0} {$i < [$coordinates GetNumberOfTuples]} {incr i} {
        set nodeP [$doc createElement Point]
        $nodeP setAttribute index $i
        $nodeS appendChild $nodeP
        set hp [$coordinates GetTuple3 $i]
        $nodeP appendChild [$doc createTextNode $hp]
      }
    }
    set fd [open $path w]
    puts -nonewline $fd [$doc asXML]
    close $fd
    $doc delete
  }

  method CmdRemovePlane { } {
    set name [$Widgets(entry,PlaneName) get]
    set path [$self GetCurrentXMLPath]
    if {$path eq ""} {
      return
    }
    file delete $path
    hook call $self <Status> [mc "Plane %s removed" $name] 
  }
  
  method _GetStorageBaseDir { } {
    set studyDir [$DataSet GetDirectory]
    if {$studyDir eq ""} {
      return ""
    }
    set planeDir [file join $studyDir FlowQuantify]
    if {[file exist $planeDir]} {
      if {![file isdirectory $planeDir]} {
        error "$type::_GetStorageBaseDir: '$planeDir' should be a directory"
      }
    } else {
      file mkdir $planeDir
    }
    return $planeDir
  }

  method GetPlanePath { name } {
    set baseDir [$self _GetStorageBaseDir]
    if {$baseDir eq ""} {
      return ""
    }
    return [file join $baseDir "${name}.xml"]
  }

  method CmdChangeSplineHandleSize { v } {
    set T [$DataSet GetNumberOfTimeStep]
    for {set t 0} {$t < $T} {incr t} {
      set splineWidget $SplineCollection($t)
      set spline [$splineWidget GetRepresentation]
      $spline SetHandleSize $v
    }
    set splineWidget [$self GetActiveSplineWidget]
    #$self RedrawActiveSpline
    set ::cs $splineWidget
    $splineWidget Render
  }

  method SetSplineInitialPosition { } {
    set T [$DataSet GetNumberOfTimeStep]
    for {set i 0} {$i < $T} {incr i} {
      $self _buildSplineWidget $i
    }
    $self ResetAllSplinePosition
    set splineWidget0 $SplineCollection(0)
    set spline [$splineWidget0 GetRepresentation]
    set size [$spline GetHandleSize]
    $Widgets(scale,SplineHandleSize) configure -value $size
  }

  method SavePlanePosition { } {
    foreach m {Origin Point1 Point2} {
      set planeData($m) [$PlaneWidget Get$m]
      if {[llength $planeData($m)] != 3} {
        $self RestorePlanePosition
        return
      }
    }
    foreach m {Origin Point1 Point2} {
      set LastPlanePosition($m) $planeData($m)
    }
  }
  
  method RestorePlanePosition { } {
    $PlaneWidget SetOrigin {*}$LastPlanePosition(Origin)
    $PlaneWidget SetPoint1 {*}$LastPlanePosition(Point1)
    $PlaneWidget SetPoint2 {*}$LastPlanePosition(Point2)
  }

  method SetPlaneInitialPosition { } {
    $PlaneWidget PlaceWidget
    $self  SavePlanePosition
    $self RecomputePlaneResolution
  }

  method SetInitialPosition { } {
    $self SetPlaneInitialPosition
    $self SetSplineInitialPosition
    set WidgetPlaced 1
  }

  method GetSplineWidget { t } {
    return $SplineCollection($t)
  }
 
  method GetActiveSplineWidget { } {
    if {$IndexActiveSpline<0} {
      return ""
    } else {
      return $SplineCollection($IndexActiveSpline)
    }
  }

  method GetSplineWidgetVisibility {} {
    set splineWidget [$self GetActiveSplineWidget]
    if {$splineWidget eq ""} {
      return 0
    } else {
      return [$splineWidget GetEnabled]
    }
  }

  method ShowActiveSpline {} {
    set splineWidget [$self GetActiveSplineWidget]
    if {$splineWidget ne ""} {
      $splineWidget SetEnabled 1
      set ::cs $splineWidget
    }
  }

  method ___GetCurrentTimeStep { } {
    return $IndexActiveSpline
  }

  method ChangeCurrentSpline { t } {
    set splineWidget [$self GetActiveSplineWidget]
    if {$splineWidget ne ""} {
      set status [$splineWidget GetEnabled]
      $splineWidget SetEnabled 0
    } else {
      set status [$self GetVisibility]
    }
    # change the index to the requested time-step
    set IndexActiveSpline $t
    if {$status} {
      # show the new active spline widget if the previous was on
      $self ShowActiveSpline
    }
  }

  method OnChangeTimeStep { t } {
    #maskPoint SetInput [ StudyHelper::GetVectorImage -masked no ]
    set imgVector [$DataSet GetVelocityImage -component FLOW -vtk yes]
    if {[VTKUtil::GetVTKMajorVersion] > 5} {
      $PlaneWidget SetInputData $imgVector
    } else {
      $PlaneWidget SetInput $imgVector
    }
    if {!$WidgetPlaced} {
      $self SetInitialPosition
    }
    if {[VTKUtil::GetVTKMajorVersion] > 5} {
      $ProbeFilter SetSourceData $imgVector
    } else {
      $ProbeFilter SetSource $imgVector
    }
    $self ChangeCurrentSpline $t
    hook call $self <Render>
    $self UpdateImplicitLoop
    after idle $self UpdateFlowTable
    #set cts [lindex $CurrentTimeStepTicks $t]
    $self UpdateChartTimeStep
    #$Widgets(Graph,Plot) marker configure cts -coords [list $cts -Inf $cts +Inf]
  }

  method FixPlaneWidgetPlacement { } {
    puts "FixPlaneWidgetPlacement"
    return
    foreach m {Origin Point1 Point2 Normal Center} {
      $PlaneWidget SetOrigin {*}[$LoResPlane Get$m]
    }
  }

  method UpdatePlaneSourceFromPlaneWidget { } {
    return

    foreach m {Origin Point1 Point2 Normal Center} {
      set planeData($m) [$PlaneWidget Get$m]
      if {[llength $planeData($m)] != 3} {
        $self FixPlaneWidgetPlacement
        return
      }
    }
    foreach plane [list $LoResPlane $HiResPlane] {
      $plane SetOrigin {*}[$PlaneWidget GetOrigin]
      $plane SetPoint1 {*}[$PlaneWidget GetPoint1]
      $plane SetPoint2 {*}[$PlaneWidget GetPoint2]
      $plane SetNormal {*}[$PlaneWidget GetNormal]
      $plane SetCenter {*}[$PlaneWidget GetCenter]
    }
  }

  method OnPlaneWidgetStartInteraction { } {
    set InteractionCounter 0
  }

  method RepositionSplineWidget { i } {
    if {[info exists SplineCollection($i)]} {
      set splineWidget $SplineCollection($i)
      set spline [$splineWidget GetRepresentation]
      $spline SetProjectionPosition 0.0
      #set hp [$splineWidget GetHandlePosition 0]
      #$splineWidget SetHandlePosition 0 {*}$hp
    }
  }

  method RepositionAllSplineWidget {} {
    foreach i [array names SplineCollection] {
      $self RepositionSplineWidget $i
    }
    #$Renderer Render
    #hook call $self <Render>
  }

  method ComputePlaneElementArea { } {
    set planeSource [$PlaneWidget GetPolyDataAlgorithm]
    foreach {s1 s2} [VTKUtil::GetPlaneSizes $planeSource] break
    puts "Plane Sizes: $s1 $s2"
    set n1 [$planeSource GetXResolution]
    set n2 [$planeSource GetYResolution]
    puts "Plane Resolution: $n1 $n2"
    puts "Plane Element Sizes: [expr {($s1/double($n1))}] [expr {($s2/double($n2))}]"
    return [expr {($s1/double($n1))*($s2/double($n2))}]
  }

  method RecomputePlaneResolution { } {
    set planeSource [$PlaneWidget GetPolyDataAlgorithm]
    puts "PlaneWidget Resolution = [$PlaneWidget GetResolution]"
    foreach {s1 s2} [VTKUtil::GetPlaneSizes $planeSource] break
    if {$s1 > $s2} {
      set r [expr {$s2/$PlaneElementSize}]
      set r2 [expr {int($r)}]
      set r1 [expr {int($r2*double($s1)/$s2)}]
    } else {
      set r [expr {$s1/$PlaneElementSize}]
      set r1 [expr {int($r)}]
      set r2 [expr {int($r1*double($s2)/$s1)}]
    }
    $planeSource SetResolution $r1 $r2
    puts "$r1 $r2"
  }

  method UpdateImplicitLoop { } {
    if {![$self IsActive]} {
      return
    }
    set splineWidget [$self GetActiveSplineWidget]
    set spline [$splineWidget GetRepresentation]
    set parametricSpline [$spline GetParametricSpline]
    set points [$ImplicitLoop GetLoop]
    if {$points eq ""} {
      set points [vtkPoints New]
      $points SetNumberOfPoints [expr {$ContourResolution-1}]
    }
    set i 0
    set sample [SampleParametricSpline $parametricSpline $ContourResolution]
    set l [llength $sample]
    puts "Lenght of sample $l"
    incr l -1
    foreach pt $sample {
      if {$i == 0} {
        puts "$i -- $pt"
      }
      $points SetPoint $i {*}$pt
      if {[incr i] == $l} break
    }
    #set implicitLoop [vtkImplicitSelectionLoop New]
    set implicitLoop $ImplicitLoop
    $points Modified
    $implicitLoop SetLoop $points
    $implicitLoop SetNormal {*}[$PlaneWidget GetNormal]
    $implicitLoop AutomaticNormalGenerationOff
    $ExtractCell ExtractInsideOn
    $ExtractCell ExtractBoundaryCellsOn
    $ExtractCell SetImplicitFunction $implicitLoop
    #$ExtractCell SetInputConnection [$VectorNorm GetOutputPort]
    puts "VectorNorm $VectorNorm"
    puts "ExtractCell $ExtractCell"
   }

  method OnSplineWidgetBeginInteraction { } {
    set InteractionCounter 0
  }

  method OnSplineWidgetInteraction { } {
    if {[incr InteractionCounter]%5} {
      $self UpdateImplicitLoop
    }
  }

  method OnSplineWidgetEndInteraction { } {
    $self InvalidateFullRangeData
    $self UpdateFlowTable
    $self UpdateImplicitLoop
    set InteractionCounter 0
  }
  
  method OnPlaneWidgetEndInteraction { } {
    $self RepositionAllSplineWidget
    $self RecomputePlaneResolution
    $self InvalidateFullRangeData
    $self UpdateFlowTable
    $self UpdateImplicitLoop
    set InteractionCounter 0
  }

  method OnPlaneWidgetInteraction { } {
    $self SavePlanePosition
    $self RepositionSplineWidget $IndexActiveSpline
    if {[incr InteractionCounter] % 10} {
      $self UpdateImplicitLoop
    }
  }

  method GetPlaneResolution {plane s} {
    set origin  [$plane GetOrigin]
    set point1 [$plane GetPoint1]
    set point2 [$plane GetPoint2]
    set s1 0
    set s2 0
    foreach co $origin c1 $point1 c2 $point2 {
      set d1 [expr {$c1-$co}] 
      set d2 [expr {$c2-$co}]
      set s1 [expr {$s1 + $d1*$d1}]
      set s2 [expr {$s2 + $d2*$d2}]
    }
    set n1 [expr {int(ceil(sqrt($s1)/$s))}]
    set n2 [expr {int(ceil(sqrt($s2)/$s))}]
    return [list $n1 $n2]
  }

  method _ComputeAccumulatedVolume { {t -1}} {
    if {$t == -1} {
      set t [$DataSet GetCurrentTimeStep]
    }
    if {![info exists FlowTableData(flow,$t)]} {
      return -1
    }
    set acc 0
    set f0 $FlowTableData(flow,0)
    set dt [expr {[$DataSet GetLengthOfTimeStep]/1000.0}]
    for {set i 1} {$i <= $t} {incr i} {
      set f1 $FlowTableData(flow,$i)
      set acc [expr {$acc + ($f0+$f1)}]
      set f0 $f1
    }
    return [expr {$acc*$dt/2.0}]
  }

  method UpdateFlowTable { } {
    if {![$self IsActive]} {
      return
    }
    set splineWidget [$self GetActiveSplineWidget]
    set spline [$splineWidget GetRepresentation]
    set parametricSpline [$spline GetParametricSpline]
    set tableFlow [FlowQuantifyVtkContour [$ProbeFilter GetOutput] $parametricSpline [$PlaneWidget GetNormal] $ContourResolution]
    # fix units & convert to liters
    array set _tableFlow $tableFlow
    foreach f {flow flow_forward flow_backward} {
      set _tableFlow($f) [expr {$_tableFlow($f) * 1.0e-2}]
    }
    $Widgets(Tree,FlowTable) item delete 0 end
    foreach i [array names _tableFlow] {
      set label $MagnitudeInfo($i,label)
      set value [format %.3f $_tableFlow($i)]
      set unit $MagnitudeInfo($i,unit)
      set id [$Widgets(Tree,FlowTable) insert [list [list $label] [list "-- $value $unit"]]]
      set TableEntryInfo($i) $id
    }
    set accv [format "%.3f" [$self _ComputeAccumulatedVolume]]
    set id [$Widgets(Tree,FlowTable) insert [list [list "Accum. Volume"] [list "-- $accv mL"]]]
    set TableEntryInfo(accflow) $id
  }

  method ComputeFlowTableForAllTimeStep { } {
    array unset FlowTableData
    set planeSource0 [$PlaneWidget GetPolyDataAlgorithm]
    set planeSource [vtkPlaneSource New]
    $planeSource SetOrigin {*}[$planeSource0 GetOrigin]
    $planeSource SetPoint1 {*}[$planeSource0 GetPoint1]
    $planeSource SetPoint2 {*}[$planeSource0 GetPoint2]
    $planeSource SetXResolution [$planeSource0 GetXResolution]
    $planeSource SetYResolution [$planeSource0 GetYResolution]
    set probePlane [vtkProbeFilter New]
    $probePlane SetInputConnection [$planeSource GetOutputPort]
    set T [$DataSet GetNumberOfTimeStep]
    for {set t 0} {$t < $T} {incr t} {
      set imgVector [$DataSet GetVelocityImage -timestep $t \
                         -component FLOW -vtk yes]
      if {[VTKUtil::GetVTKMajorVersion] > 5} {
        $probePlane SetSourceData $imgVector
      } else {
        $probePlane SetSource $imgVector
      }
      set splineWidget [$self GetSplineWidget $t]
      set spline [$splineWidget GetRepresentation]
      set parametricSpline [$spline GetParametricSpline]
      $probePlane Update
      set tableFlow [FlowQuantifyVtkContour [$probePlane GetOutput] \
                         $parametricSpline [$PlaneWidget GetNormal] \
                         $ContourResolution]
      # fix units & convert to liters
      array set _tableFlow $tableFlow
      foreach f {flow flow_forward flow_backward} {
        set _tableFlow($f) [expr {$_tableFlow($f) * 1.0e-2}]
      }
      foreach f [array names _tableFlow] {
        set FlowTableData($f,$t) $_tableFlow($f)
      }
    }
    $probePlane Delete
    $planeSource Delete
    $self PlotMagnitude flow
    $self PlotMagnitude_VTK flow
  }
  
  method InvalidateFullRangeData { } {
    # REVIEW: update here to Plotchart
    array unset FlowTableData
    $self _initGraph
    $self _initGraph_VTK
  }

  method UpdateChartTimeStep { } {
    set t [$DataSet GetCurrentTimeStep]
    if {$t == -1 || [$vtkChartObjects(array,time) GetNumberOfTuples] <= $t} {
      return
    }
    $vtkChartObjects(array,timeMark) SetValue 0 [$vtkChartObjects(array,time) GetValue $t]
    $vtkChartObjects(array,flowMark) SetValue 0 [$vtkChartObjects(array,flow) GetValue $t]
    $vtkChartObjects(plot,flowMark) Modified
    $vtkChartObjects(view) Render

    return
    set t [$DataSet GetCurrentTimeStep]
    if {$t == -1} {
      return
    }
    set c $Widgets(canvas,Plotchart)
    $c delete band BalloonText BalloonFrame
    set plot $XYPlotData(plot)
    set dt [lindex $XYPlotData(steps) 0]
    set _dt [expr {$dt/20.0}]
    
    set cts [lindex $XYPlotData(serie,xdata) $t]
    $plot yband [expr {$cts - $_dt}] [expr {$cts + $_dt}]
    set v [lindex $XYPlotData(serie,ydata) $t]
    $XYPlotData(plot) balloon $cts $v [format "%0.2f" $v] south-east
  }

  method RedrawXYPlot { } {
    set c $Widgets(canvas,Plotchart)
    $c delete all
    foreach {mx Mx my My} $XYPlotData(limits) break
    foreach {dx dy} $XYPlotData(steps) break
    set plot [::Plotchart::createXYPlot \
                  $c [list $mx $Mx $dx] [list $my $My $dy]]
    set XYPlotData(plot) $plot 
    set sid $XYPlotData(serie,name)
    $plot xtext [mc "sec"]
    $plot ytext $MagnitudeInfo($sid,unit)
    $plot xconfig -format "%0.2f"
    $plot yconfig -format "%0.2f"
    $plot dataconfig $sid -color red
    $plot legend $sid [mc $MagnitudeInfo($sid,label)]
    $plot balloonconfig -font TkTextFont -margin 2
    foreach x $XYPlotData(serie,xdata) y $XYPlotData(serie,ydata) {
      $plot plot $sid $x $y
    }
    $self UpdateChartTimeStep
  }

  method RedrawXYPlot_VTK { } {
    #$vtkChartObjects(plot,flow) SetInputData ""
    #$vtkChartObjects(plot,flow) SetInputData $vtkChartObjects(table,flow) 0 1
    $vtkChartObjects(plot,flow) Modified
    $vtkChartObjects(chartXY) RecalculateBounds
    $self UpdateChartTimeStep 
    return 

    if {[info exist vtkChartObjects(plot,flow)]} {
      $vtkChartObjects(chartXY) RemovePlot 0
    }
    if {[$vtkChartObjects(array,time) GetNumberOfTuples] > 0} {
      set vtkChartObjects(plot,flow) [$vtkChartObjects(chartXY) AddPlot 0]
      $vtkChartObjects(plot,flow) SetInputData $vtkChartObjects(table) 0 1
    }
  }

  method PlotMagnitude { m } {
    set T [$DataSet GetNumberOfTimeStep]
    set dt [expr {[$DataSet GetLengthOfTimeStep]/1000.0}]
    set xdata {}
    set ydata {}
    set mx 0x7fffffff
    set Mx -0x7fffffff
    set my 0x7fffffff
    set My -0x7fffffff
    for {set t 0} {$t < $T} {incr t} {
      set x [expr {$t*$dt}]
      set y $FlowTableData($m,$t)
      if {$x < $mx} {
        set mx $x
      } elseif {$x > $Mx} {
        set Mx $x
      }
      if {$y < $my} {
        set my $y
      } elseif {$y > $My} {
        set My $y
      }
      lappend xdata $x 
      lappend ydata $y
    }
    set XYPlotData(serie,xdata) $xdata
    set XYPlotData(serie,ydata) $ydata
    set rangex [expr {($Mx - $mx)}]
    set rangey [expr {($My - $my)}]
    set dy [expr {($My - $my)/10.0}]
    set _ex [expr {$rangex/100}]
    set _ey [expr {$rangey/100}]
    set XYPlotData(limits) \
        [list [expr {$mx-$_ex}] [expr {$Mx+$_ex}] \
             [expr {$my-$_ey}] [expr {$My+$_ey}]]
    set XYPlotData(steps) [list $dt $dy] 
    set XYPlotData(serie,name) "$m"
    $self RedrawXYPlot
  }

  method PlotMagnitude_VTK { m } {
    set T [$DataSet GetNumberOfTimeStep]
    if {[llength [array names FlowTableData ${m},*]] != $T} {
      return
    } 
    set dt [expr {[$DataSet GetLengthOfTimeStep]/1000.0}]
    $vtkChartObjects(array,time) SetNumberOfTuples $T
    $vtkChartObjects(array,flow) SetNumberOfTuples $T
    #$vtkChartObjects(table) SetNumberOfPoints $T
    for {set t 0} {$t < $T} {incr t} {
      set x [expr {$t*$dt}]
      set y $FlowTableData($m,$t)
      $vtkChartObjects(array,time) SetValue $t $x
      $vtkChartObjects(array,flow) SetValue $t $y
    }
    $self RedrawXYPlot_VTK
  }

  method _initGraph {} {
    set c $Widgets(canvas,Plotchart)
    $c delete all
    set T [$DataSet GetNumberOfTimeStep]
    set dt [expr {[$DataSet GetLengthOfTimeStep]/1000.0}]
    set tmax [expr {($T-1)*$dt}]
    set mx 0.0
    set Mx $tmax
    set my 0.0
    set My 1.5
    set xdata {}
    set ydata {}
    for {set t 0} {$t < $T} {incr t} {
      set x [expr {$t*$dt}]
      lappend xdata $x
      lappend ydata 1
    }
    set XYPlotData(serie,xdata) $xdata
    set XYPlotData(serie,ydata) $ydata
    set XYPlotData(limits) [list $mx $Mx $my $My]
    set XYPlotData(steps) [list $dt 0.2] 
    set XYPlotData(serie,name) "flow"
    $self RedrawXYPlot    
  }

  method _initGraph_VTK {} {
    $vtkChartObjects(array,time) SetNumberOfTuples 0
    $vtkChartObjects(array,flow) SetNumberOfTuples 0
    $self RedrawXYPlot_VTK
  }

  method _cancelChartAfter { } {
    if {[info exists XYPlotData(after_id)]} {
      catch {after cancel $XYPlotData(after_id)}
      array unset XYPlotData after_id
    }
  }

  method OnResizeChart { } {
    $self _cancelChartAfter
    set XYPlotData(after_id) [after idle [mymethod RedrawXYPlot]]
    return
  }

  method TestQuantify { } {
    set splineWidget [$self GetActiveSplineWidget]
    set spline [$splineWidget GetRepresentation]
    set parametricSpline [$spline GetParametricSpline]
    set points [vtkPoints New]
    $points SetNumberOfPoints [expr {$ContourResolution-1}]
    set i 0
    set sample [SampleParametricSpline $parametricSpline $ContourResolution]
    set l [llength $sample]
    puts "Lenght of sample $l"
    incr l -1
    foreach pt $sample {
      #puts "$i -- $pt"
      $points SetPoint $i {*}$pt
      if {[incr i] == $l} break
    }
    set implicitLoop [vtkImplicitSelectionLoop New]
    $implicitLoop SetLoop $points
    $implicitLoop SetNormal {*}[$PlaneWidget GetNormal]
    $implicitLoop AutomaticNormalGenerationOff
    set ptCenter [$PlaneWidget GetCenter]
    puts "Center is : [$implicitLoop FunctionValue {*}$ptCenter]"
    puts "Center is : [$implicitLoop EvaluateFunction {*}$ptCenter]"
    set ptOrigin [$PlaneWidget GetOrigin]
    puts "Origin is : [$implicitLoop FunctionValue {*}$ptOrigin]"
    puts "Origin is : [$implicitLoop EvaluateFunction {*}$ptOrigin]"
    set cellExtractor [vtkExtractPolyDataGeometry New]
    $cellExtractor SetImplicitFunction $implicitLoop
    $cellExtractor ExtractInsideOn
    $cellExtractor ExtractBoundaryCellsOn
    $cellExtractor SetInputConnection [$ProbeFilter GetOutputPort]
    $cellExtractor Update
    set area [$self ComputePlaneElementArea]
    puts "Tcl - Element area: $area"
    puts "Tcl - Plane Elements Inside: [[$cellExtractor GetOutput] GetNumberOfPolys]"
    puts "Tcl - Loop area = [expr {$area*[[$cellExtractor GetOutput] GetNumberOfPolys]}]"
    set tableFlow [FlowQuantifyVtkContour [$ProbeFilter GetOutput] $parametricSpline [$PlaneWidget GetNormal] $ContourResolution]
    # fix units & convert to liters
    array set _tableFlow $tableFlow
    foreach f {flow flow_forward flow_backward} {
      set _tableFlow($f) [expr {$_tableFlow($f) * 1.0e-5}]
    }
    parray _tableFlow
    if {0} {
      vtkPolyDataWriter pwriter
      pwriter SetInputConnection [$cellExtractor GetOutputPort]
      pwriter SetFileName /tmp/inside.vtk
      pwriter SetFileTypeToASCII
      pwriter Update
      pwriter SetFileName /tmp/probe.vtk
      pwriter SetInputConnection [$ProbeFilter GetOutputPort]
      pwriter Update
      pwriter Delete
    }
    $cellExtractor Delete
    $implicitLoop Delete
    $points Delete
  }

  method TestLoadPolyData { fileName } {
    set pdReader [vtkPolyDataReader $win.pdReader]
    $pdReader SetFileName $fileName
    $pdReader Update
    set pd [vtkPolyData New]
    $pd DeepCopy [$pdReader GetOutput]
    $pdReader Delete
    #puts [[$pdReader GetOutput] Print]
    set cleaner $win.cleaner
    if {[info command $cleaner] ne ""} {
      $cleaner SetInputData $pd
    } else {
      set cleaner [vtkCleanPolyData $win.cleaner]
      $cleaner SetInputData $pd
      $cleaner SetTolerance 0.0001
      $cleaner PointMergingOn
      set tubes [vtkTubeFilter $win.tubes]
      $tubes SetInputConnection [$cleaner GetOutputPort]
      $tubes SetRadius 0.2
      $tubes SetNumberOfSides 4
      set lineMapper [vtkPolyDataMapper $win.lineMapper]
      $lineMapper SetInputConnection [$tubes GetOutputPort]
      $lineMapper UseLookupTableScalarRangeOn
      $lineMapper SetLookupTable $options(-lookuptable)
      set lineActor [vtkActor $win.lineActor]
      $lineActor SetMapper $lineMapper
      $lineActor VisibilityOn
      $Renderer AddActor $lineActor
    }
  }
  
  method GetHighResolution { } {
    set v [$Widgets(scale,hires) get]
    return [$self GetPlaneResolution $HiResPlane $v]
  }

  method GetLowResolution { } {
    set v [$Widgets(scale,lores) get]
    return [$self GetPlaneResolution $LoResPlane $v]
  }

  method _CmdChangeHighResolution { v } {
    $HiResPlane SetResolution {*}[$self GetPlaneResolution $HiResPlane $v]
    hook call $self <Render>
  }

  method _CmdChangeLowResolution { v } {
    $LoResPlane SetResolution {*}[$self GetPlaneResolution $LoResPlane $v]
  }

  method _ResetProgressControl { } {
    array set Progress {
      value,absolute 0
      value,relative 0
      counter 0
      aborted 0
    }
  }

  method _OnStartEvent { } {
    #puts "$type _OnStartEvent"
    $self _ResetProgressControl
    hook call $self <Status> [mc "Start streamlines ..."]
    update idletask
  }

  method _OnProgressEvent { } {
    set p [$Streamer GetProgress]
     #puts "$type _OnProgressEvent $p"
    if {$p != $Progress(value,absolute)} {
      set $Progress(value,absolute) $p
      hook call $self <Status> [mc "Streamline progress %g" $p]
    } else {
      if {[incr Progress(counter)] >= 5} {
        set Progress(aborted) 1
        $Streamer AbortExecuteOn
        hook call $self <Status> [mc "Streamline aborted"]
      }
    }
    update idletask
  }

  method GetVectorFactorFromScale { } {
    return [$Widgets(scale,Factor) get]
  }
  
  method _OnSetFactor { v } {
    if {$v != [$HedgeHog GetScaleFactor]} {
      $HedgeHog SetScaleFactor $v
      hook call $self <Render>
    }
  }

  method _OnClickOnOffHH {} {
    set icn [Icons GetInstance]
    set imgON [$icn Get "light_bulb_on" -size 16]
    set imgOFF [$icn Get "light_bulb_off" -size 16]
    set w $Widgets(button,OnOffHH)
    set img [$w cget -image]
    if {$img eq $imgON} {
      set v 0
      set img $imgOFF
    } else {
      set v 1
      set img $imgON
    }
    $w configure -image $img
    $self SetHedgeHogVisibility $v
  }

}
