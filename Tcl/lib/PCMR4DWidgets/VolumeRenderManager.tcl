package require vtk
package require snit
package require CScale
package require msgcat
package require hook

namespace import msgcat::*

snit::widget VolumeRenderManager {

  component OpacityTransferFunction -public OpacityTransferFunction
  component ColorTransferFunction   -public ColorTransferFunction
  component VolumeProperty          -public VolumeProperty
  component VolumeMapper            -public VolumeMapper
  component Volume                  -public Volume

  variable Widgets
  variable DataSet
  variable ScalarMinimum
  variable ScalarMaximum
  variable OpacityLevel 0.5
  variable OpacityWindow 0.5
  variable OpacityMaximum 0.2
  variable UseShading 1

  delegate method SetVisibility to Volume
  delegate method GetVisibility to Volume

  delegate option * to hull
  delegate method * to hull
  
  constructor {args} {
    $self configurelist $args
    ttk::label $win.lbOpacity -text [mc "Opacity"]:
    CScale $win.scOpacity -command [mymethod _CmdChangeOpacity] -value 0.2
    set Widgets(scale,opacity) $win.scOpacity
    ttk::label $win.lbLevel -text [mc "Level"]:
    CScale $win.scLevel -command [mymethod _CmdChangeLevel] -value 0.5
    set Widgets(scale,level) $win.scLevel
    ttk::label $win.lbWindow -text [mc "window"]:
    CScale $win.scWindow -command [mymethod _CmdChangeWindow] -value 0.5
    set Widgets(scale,window) $win.scWindow
    ttk::label $win.lbShading -text [mc "Shading"]:
      ttk::checkbutton $win.chkShading -command [mymethod _CmdChangeShanding] \
        -variable [myvar UseShading]
    set Widgets(scale,chkShading) $win.chkShading

    grid $win.lbOpacity -row 0 -column 0 -sticky "e"
    grid $win.scOpacity -row 0 -column 1 -sticky "w"
    grid $win.lbLevel   -row 1 -column 0 -sticky "e"
    grid $win.scLevel   -row 1 -column 1 -sticky "w"
    grid $win.lbWindow  -row 2 -column 0 -sticky "e"
    grid $win.scWindow  -row 2 -column 1 -sticky "w"
    grid $win.lbShading  -row 3 -column 0 -sticky "e"
    grid $win.chkShading  -row 3 -column 1 -sticky "w"

    grid rowconfigure    $win 4 -weight 1
    grid columnconfigure $win 2 -weight 1
    $self _buildVTKObjects
    puts "VolumeRenderManager := $self"
  }

  destructor {
    $Volume Delete
    $VolumeMapper Delete
    $OpacityTransferFunction Delete
    $ColorTransferFunction Delete
    $VolumeProperty Delete
  }  

  method DisableWidgets { } {
    $Widgets(scale,opacity) state disabled
    $Widgets(scale,level) state disabled
    $Widgets(scale,window) state disabled
    $Widgets(scale,chkShading) state disabled
  }

  method EnableWidgets { } {
    $Widgets(scale,opacity) state !disabled
    $Widgets(scale,level) state !disabled
    $Widgets(scale,window) state !disabled
    $Widgets(scale,chkShading) state !disabled
  }

  method GetLabel { } {
    return "Volume Render"
  }

  method _buildVTKObjects { } {
    # Create transfer mapping scalar value to opacity
    install OpacityTransferFunction using vtkPiecewiseFunction $win.opacityTransferFunction

    # Create transfer mapping scalar value to color
    install ColorTransferFunction using vtkColorTransferFunction $win.colorTransferFunction

    # The property describes how the data will look
    install VolumeProperty using vtkVolumeProperty $win.volumeProperty
    $VolumeProperty SetShade $UseShading
    $VolumeProperty SetInterpolationTypeToLinear
    $VolumeProperty SetScalarOpacity $OpacityTransferFunction

    install VolumeMapper using vtkSmartVolumeMapper $win.volumeMapper

    install Volume using vtkVolume $win.volume
    $Volume SetMapper $VolumeMapper
    $Volume SetProperty $VolumeProperty
  }

  method AddToRenderer { renderer } {
    $renderer AddVolume $Volume
  }

  method GetOpacityLevel { } {
    return [$Widgets(scale,level) cget -value]
  }

  method GetOpacityWindow { } {
    return [$Widgets(scale,window) cget -value]
  }

  method GetMaximumOpacity { } {
    return [$Widgets(scale,opacity) cget -value]
  }

  method GetPointStartOpacityRamp { } {
    set l [$self GetOpacityLevel]
    set w [$self GetOpacityWindow]
    return [expr {$l + $w/2.0}]
  }

  method GetPointEndOpacityRamp { } {
    set l [$self GetOpacityLevel]
    set w [$self GetOpacityWindow]
    return [expr {$l + $w/2.0}]
  }
  
  method SetDataSet { dataSet } {
    set DataSet $dataSet
    $dataSet AddObserver AfterOpenEvent [mymethod OnOpenDataSet]
    #$dataSet AddObserver ChangeTimeStep [mymethod OnChangeTimeStep %T]
    $dataSet AddObserver BeforeCloseEvent [mymethod OnCloseDataSet]
  }

  method OnOpenDataSet { } {
    #set img [$DataSet GetMagnitudeImage -vtk yes]
    set img [$DataSet GetPremaskImage -vtk yes]
    if {[VTKUtil::GetVTKMajorVersion] > 5} {
      $VolumeMapper SetInputData $img
    } else {
      $VolumeMapper SetInput $img
    }

    foreach {ScalarMinimum ScalarMaximum} [$img GetScalarRange] break
    $self _BuildOpacityTransferFunction
    $self _BuildColorTransferFunction
    #$VolumeProperty SetColor $ColorTransferFunction
    $self EnableWidgets
  }

  method OnCloseDataSet { } {
    $self SetVisibility 0
    if {[VTKUtil::GetVTKMajorVersion] > 5} {
      $VolumeMapper SetInputData ""
    } else {
      $VolumeMapper SetInput ""
    }
    $self DisableWidgets
  }

  method OnChangeTimeStep { t } {
    # do nothing
    #puts "GetMagnitudeImage -timestep $t -vtk yes"
    #$VolumeMapper SetInput [$DataSet GetMagnitudeImage -timestep $t -vtk yes]
    #hook call $self <Render>
  }

  method _CmdChangeOpacity { v } {
    set a1 [$self GetPointEndOpacityRamp]
    set x1 [expr {$ScalarMaximum * $a1}]
    $OpacityTransferFunction AddPoint $x1 [$self GetMaximumOpacity]   
    hook call $self <Render>
  }

  method _CmdChangeLevel { v } {
    set w0 [expr {2.0*$v}]
    set w1 [expr {2.0*(1-$v)}]
    $Widgets(scale,window) configure -to [expr {$w0<=$w1 ? $w0 : $w1}]
    $self _BuildOpacityTransferFunction
    hook call $self <Render>
  }

  method _CmdChangeWindow { v } {
    set hw [expr {$v/2.0}]
    $Widgets(scale,level) configure -from $hw -to [expr {1.0 - $hw}]
    $self _BuildOpacityTransferFunction
    hook call $self <Render>
  }

  method _CmdChangeShanding { } {
    $VolumeProperty SetShade $UseShading
    hook call $self <Render>
  }

  method _BuildOpacityTransferFunction { } {
    set a0 [$self GetPointStartOpacityRamp]
    set a1 [$self GetPointEndOpacityRamp]
    set x0 [expr {$ScalarMinimum * $a0}]
    set x1 [expr {$ScalarMaximum * $a1}]
    set y1 [$self GetMaximumOpacity]
    $OpacityTransferFunction RemoveAllPoints
    $OpacityTransferFunction AddPoint $x0 0.0
    $OpacityTransferFunction AddPoint $x1 $y1
    #$OpacityTransferFunction AddPoint $x1 0.2
  }

  method _BuildColorTransferFunction { } {
    $ColorTransferFunction RemoveAllPoints
    $ColorTransferFunction AddRGBPoint $ScalarMinimum 0.0 0.0 1.0
    $ColorTransferFunction AddRGBPoint $ScalarMaximum 1.0 0.0 0.0
  }
}
