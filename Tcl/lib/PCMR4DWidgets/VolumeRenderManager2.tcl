package require vtk
package require snit
package require CScale
package require msgcat
package require hook

namespace import msgcat::*

snit::widget VolumeRenderManager {

  component OpacityTransferFunction -public OpacityTransferFunction
  component ColorTransferFunction   -public ColorTransferFunction
  component ImageMultiply           -public ImageMultiply
  component ImageShiftScale         -public ImageShiftScale
  component VolumeProperty          -public VolumeProperty
  component VolumeMapper            -public VolumeMapper
  component Volume                  -public Volume

  variable Widgets
  variable DataSet

  variable ScalarMinimum 0
  variable ScalarMaximum 180
  variable UseShading 0

  #delegate method SetVisibility to Volume
  delegate method GetVisibility to Volume

  delegate option * to hull
  delegate method * to hull
  
  hulltype ttk::frame

  constructor {args} {
    set icn [Icons GetInstance]
    $self configurelist $args
    ttk::label $win.lbVisible -text [mc "Visible"]:
    set Widgets(button,Visible) $win.btnVisible
    Button $Widgets(button,Visible) -relief link \
        -image [$icn Get "light_bulb_off" -size 16] \
        -command [mymethod _OnClickVisibility]
    ttk::label $win.lbOpacity -text [mc "Opacity"]:
    CScale $win.scOpacity -command [mymethod _CmdChangeOpacity] -value 0.6
    set Widgets(scale,opacity) $win.scOpacity
    ttk::label $win.lbShading -text [mc "Shading"]:
      ttk::checkbutton $win.chkShading -command [mymethod _CmdChangeShanding] \
        -variable [myvar UseShading]
    set Widgets(scale,chkShading) $win.chkShading

    grid $win.lbVisible -row 0 -column 0 -sticky "e"
    grid $Widgets(button,Visible) -row 0 -column 1 -sticky "w"
    grid $win.lbOpacity -row 1 -column 0 -sticky "e"
    grid $win.scOpacity -row 1 -column 1 -sticky "w"
    grid $win.lbShading  -row 2 -column 0 -sticky "e"
    grid $win.chkShading  -row 2 -column 1 -sticky "w"

    grid rowconfigure    $win 3 -weight 1
    grid columnconfigure $win 3 -weight 1
    catch {
    $self _buildVTKObjects
    } msg
    puts "ooooooooooooooooo = $msg"
    puts "VolumeRenderManager2 := $self"
  }

  destructor {
    $Volume Delete
    $VolumeMapper Delete
    $VolumeProperty Delete
    $ImageShiftScale Delete
    $ImageMultiply Delete
  }  

  method DisableWidgets { } {
    $Widgets(scale,chkShading) state disabled
  }

  method EnableWidgets { } {
    $Widgets(scale,chkShading) state !disabled
  }

  method GetLabel { } {
    return "Volume Render"
  }

  method _buildVTKObjects { } {
    puts "_buildVTKObjects"
    # Create transfer mapping scalar value to opacity
    #install OpacityTransferFunction using vtkPiecewiseFunction $win.opacityTransferFunction

    # Create transfer mapping scalar value to color
    #install ColorTransferFunction using vtkColorTransferFunction $win.colorTransferFunction

    install ImageShiftScale using vtkImageShiftScale $win.imageShiftScale
    
    install ImageMultiply using vtkImageMathematics $win.imageMultiply
    $ImageMultiply SetOperationToMultiply
    $ImageMultiply SetInputConnection 0 [$ImageShiftScale GetOutputPort]

    # The property describes how the data will look
    install VolumeProperty using vtkVolumeProperty $win.volumeProperty
    #$VolumeProperty SetColor $ColorTransferFunction
    #$VolumeProperty SetScalarOpacity $OpacityTransferFunction
    $VolumeProperty SetShade $UseShading
    $VolumeProperty SetInterpolationTypeToLinear
    set ColorTransferFunction [$VolumeProperty GetRGBTransferFunction]
    set OpacityTransferFunction [$VolumeProperty GetScalarOpacity]

    install VolumeMapper using vtkSmartVolumeMapper $win.volumeMapper

    install Volume using vtkVolume $win.volume
    $Volume SetMapper $VolumeMapper
    $Volume SetProperty $VolumeProperty
  }

  method AddToRenderer { renderer } {
    $renderer AddVolume $Volume
  }

  method SetDataSet { dataSet } {
    set DataSet $dataSet
    $dataSet AddObserver AfterOpenEvent [mymethod OnOpenDataSet]
    $dataSet AddObserver ChangeTimeStep [mymethod OnChangeTimeStep %T]
    $dataSet AddObserver BeforeCloseEvent [mymethod OnCloseDataSet]
  }

  method OnOpenDataSet { } {
    #set img [$DataSet GetMagnitudeImage -vtk yes]
    #set img [$DataSet GetPremaskImage -vtk yes]
    set premask [$DataSet GetPremaskImage -vtk yes]
    foreach {ScalarMinimum ScalarMaximum} [$premask GetScalarRange] break
    $ImageShiftScale SetShift [expr {-$ScalarMinimum}]
    $ImageShiftScale SetScale [expr {1/($ScalarMaximum - $ScalarMinimum)}]
    $ImageShiftScale SetInputData $premask
    
    $ImageShiftScale Update

    puts "=========================================================="
    puts "Image Normalized = [[$ImageShiftScale GetOutput] GetScalarRange]"
    puts "=========================================================="


    set venc [$DataSet GetVelocityEncoding]
    set ScalarMinimum 0
    set ScalarMaximum [expr {$venc*1.5}]
    set x0 [ expr { ($ScalarMaximum+$ScalarMinimum)/32.0 } ]

    $OpacityTransferFunction RemoveAllPoints
    $OpacityTransferFunction AddPoint  $x0   0.0
    $OpacityTransferFunction AddPoint  $ScalarMaximum [$self GetCurrentOpacity]

    $ColorTransferFunction RemoveAllPoints
    $ColorTransferFunction AddRGBPoint  $ScalarMinimum 0.3 0.3 0.3
    $ColorTransferFunction AddRGBPoint  $ScalarMaximum 0.0 0.9 0.9

    if {0} {
    set img [$DataSet GetVelocityImage -component |V| -vtk yes -timestep 3]
    if {[VTKUtil::GetVTKMajorVersion] > 5} {
      $VolumeMapper SetInputData $img
    } else {
      $VolumeMapper SetInput $img
    }
    }
    $self EnableWidgets
  }

  method _OnClickVisibility { } {
    set icn [Icons GetInstance]
    set imgON [$icn Get "light_bulb_on" -size 16]
    set imgOFF [$icn Get "light_bulb_off" -size 16]
    set w $Widgets(button,Visible)
    set img [$w cget -image]
    if {$img eq $imgON} {
      set v 0
      set img $imgOFF
    } else {
      set v 1
      set img $imgON
    }
    $w configure -image $img
    $self SetVisibility $v
  }

  method SetVisibility { v } {
    if {$v == [$self GetVisibility]} {
      return
    }
    $Volume SetVisibility $v
    if {$v} {
      set t [$DataSet GetCurrentTimeStep]
      $self OnChangeTimeStep $t
    }
  }

  method OnCloseDataSet { } {
    $self SetVisibility 0
    if {[VTKUtil::GetVTKMajorVersion] > 5} {
      $VolumeMapper SetInputData ""
    } else {
      $VolumeMapper SetInput ""
    }
    $self DisableWidgets
  }

  method OnChangeTimeStep { t } {
    if {![$self GetVisibility]} {
      # do nothing
      return 
    }
    set img [$DataSet GetVelocityImage -component |V| -vtk yes -timestep $t]
    $img Modified
    
    $ImageMultiply SetInput2Data $img
    
    $ImageMultiply Update
    puts "=========================================================="
    puts "Image Multiplied = [[$ImageMultiply GetOutput] GetScalarRange]"
    puts "=========================================================="
    
    foreach {ScalarMinimum ScalarMaximum} [[$ImageMultiply GetOutput] GetScalarRange] break
    
    set venc [$DataSet GetVelocityEncoding]
    set ScalarMinimum 0
    set ScalarMaximum [expr {$venc*0.5}]
    set x0 [ expr { ($ScalarMinimum+$ScalarMaximum)/100.0 } ]
    #set x0 0

    $OpacityTransferFunction RemoveAllPoints
    $OpacityTransferFunction AddPoint  $x0   0.0
    $OpacityTransferFunction AddPoint  $ScalarMaximum [$self GetCurrentOpacity]

    $ColorTransferFunction RemoveAllPoints
    $ColorTransferFunction AddRGBPoint  $ScalarMinimum 0.3 0.3 0.3
    $ColorTransferFunction AddRGBPoint  $ScalarMaximum 1.0 0.9 0.9

    $VolumeMapper SetInputConnection [$ImageMultiply GetOutputPort]
    if {0} {
    if {[VTKUtil::GetVTKMajorVersion] > 5} {
      $VolumeMapper SetInputData $img
    } else {
      $VolumeMapper SetInput $img
    }
    }
    hook call $self <Render>
  }

  method GetCurrentOpacity { } {
    return [$Widgets(scale,opacity) cget -value]
  }

  method _CmdChangeOpacity { v } {
    $OpacityTransferFunction AddPoint $ScalarMaximum [$self GetCurrentOpacity]
    hook call $self <Render>
  }

  method _CmdChangeShanding { } {
    $VolumeProperty SetShade $UseShading
    hook call $self <Render>
  }

  method SaveCurrentVolume { {filename /tmp/vol.vtk}} {
    vtkGenericDataObjectWriter writer
    writer SetInputConnection [$ImageMultiply GetOutputPort]
    writer SetFileName $filename
    writer Update
    writer Delete
  }
}
