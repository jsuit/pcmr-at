package require Ttk
package require snit
package require ImageRenderWidget
package require VolumeOrienter

snit::widget PCMR4DImageViewer {

  typevariable ComponentType -array {
    |V| |V|
    VX  VelocityC
    VY  VelocityC
    VZ  VelocityC
    MAG MAG
  }
  typemethod GetComponentType { name } {
    return $ComponentType($name)
  }

  component ImageRender -public ImageRender
  component TimeSlicer -public TimeSlicer
  component VolumeOrienter -public VolumeOrienter

  variable CurrentComponent "|V|"
  variable ContextMenu

  delegate method AddActor to ImageRender
  delegate method AddVolume to ImageRender
  delegate method Render   to ImageRender
  delegate method GetInteractor to ImageRender
  delegate option * to hull
  delegate method * to hull

  option -dataset -default "" -configuremethod _ConfDataSet

  constructor { args } {
    set VolumeOrienter [MedicalVolumeOrienter %AUTO%]
    set ImageRender [ImageRenderWidget $win.irw \
                         -initialview "Sagittal" \
                         -volumeorienter $VolumeOrienter]
    set TimeSlicer [PCMR4DTimeSlicer $win.tslicer \
                        -command [mymethod OnActivateTimeStep %i] -size 128]
    $self configurelist $args
    grid $ImageRender -row 0 -column 0 -sticky snew
    grid $TimeSlicer -row 1 -column 0 -sticky ew
    grid rowconfigure $win 0 -weight 1
    grid columnconfigure $win 0 -weight 1
    $self _AddContextMenu
  }

  destructor {
    if {[info exists VolumeOrienter]} {
      $VolumeOrienter destroy
    }
  }

  method _ConfImageOrientation { } {
    $ImageRender SetImageOrientation [$options(-dataset) GetImageOrientation]
    hook call $self <ChangeImageOrientation>
  }

  method _ConfDataSet { o ds } {
    if {$ds ne "" && ([catch {$ds info type} dsType] || $dsType ne "::PCMR4DDataSet")} {
      error "could not set -dataset, '$ds' must be a valid PCMR4DDataSet"
    }
    set options(-dataset) $ds
    $TimeSlicer configure -dataset $ds
    if {$ds eq ""} {
      # we should release the pipeline
      $ImageRender SetImageData ""
    } else {
      after idle $self _ConfImageOrientation
    }
  }

  method _AddContextMenu { } {
    set m [$ImageRender GetContextMenu]
    set m [$ImageRender GetContextMenu]
    $m add separator
    set ContextMenu(components) [menu $m.comp -tearoff 0 \
                                     -postcommand [mymethod _FillMenuComponents $m.comp]]
    $m add cascade -menu $ContextMenu(components) -label "Activate component"
  }

  method _FillMenuComponents { m } {
    set ds $options(-dataset)
    $m delete 0 end
    if {$ds ne ""} {
      foreach c [[$ds info type] GetComponentNames] {
        $m add radiobutton -label "$c" \
            -variable [myvar CurrentComponent] -value $c \
            -command [mymethod SetCurrentComponent $c 1]
      }
    }
  }

  method OnActivateTimeStep { t } {
    set ds $options(-dataset)
    $ImageRender SetImageData [$ds GetComponentImage \
                                   -component $CurrentComponent \
                                   -timestep $t -vtk yes] \
        -type [$type GetComponentType $CurrentComponent] \
        -keepview 1 -keepplane 1 -keepWL 1
  }

  method GetCurrentComponent { } {
    return $CurrentComponent
  }

  method SetCurrentComponent { c {force 0}} {
    set ds $options(-dataset)
    [$ds info type] CheckComponentName $c
    if {$force || $c ne $CurrentComponent} {
      set CurrentComponent $c
      $self OnActivateTimeStep [$TimeSlicer GetCurrentTime]
      $self Render
    }
  }
}
