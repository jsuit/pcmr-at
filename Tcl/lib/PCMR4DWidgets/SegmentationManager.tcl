package require vtk
package require vmtkWrapperTCL
package require snit
package require CScale
package require msgcat
package require hook
package require fileutil
package require vtkPcmrExtTCL
package require jobexec

namespace import msgcat::*

snit::widget SegmentationManager {

  typevariable ProgramMesher /home/jsperez/Sources/Meshing/tetgen1.5.1-beta1/build/tetgen

  variable Widgets
  variable DataSet
  variable LargestLine ""
  variable BinaryMask ""
  variable VolumeMapper ""
  variable VolumeMask ""
  variable Boundary ""
  variable BoundaryMapper ""
  variable BoundaryActor ""
  variable ClipClosedSurface ""
  variable ThresholdCaps ""
  variable ToPolyData ""
  variable ClipBoundary 1

  delegate option * to hull
  delegate method * to hull

  option -container -readonly yes -default ""

  variable Renderer
  variable Spacing -array {
    X 1
    Y 1
    Z 1
  }
  variable PathBinMask ""

  hulltype ttk::frame

  constructor {args} {
    set icn [Icons GetInstance]
    $self configurelist $args
    set f [ttk::labelframe $win.lfSpacing -text "Spacing" -borderwidth 1 -relief solid]
    set c -1
    foreach a {X Y Z} {
      ttk::label $f.lb${a} -text [mc "${a}:"]
      grid $f.lb${a} -row 0 -column [incr c]
      ttk::entry $f.entry${a} -textvariable [myvar Spacing($a)] -width 5
      grid $f.entry${a} -row 0 -column [incr c]
    }
    grid columnconfigure $f [incr c] -weight 1

    set fmask [ttk::labelframe $win.lfMask -text "Mask" -borderwidth 1 -relief solid]

    ttk::label $fmask.lbMaskVisible -text [mc "Visible"]:
    set Widgets(button,MaskVisible) $fmask.btnMaskVisible
    Button $Widgets(button,MaskVisible) -relief link \
        -image [$icn Get "light_bulb_off" -size 16] \
        -command [mymethod _OnClickVisibility_Mask] -state disabled
    ttk::label $fmask.lbMaskOpacity -text [mc "Opacity"]:
    CScale $fmask.scMaskOpacity -command [mymethod _CmdChangeOpacity_Mask] -value 1.0
    set Widgets(scale,MaskOpacity) $fmask.scMaskOpacity
    $Widgets(scale,MaskOpacity) state disabled
    grid $fmask.lbMaskVisible -row 0 -column 0 -sticky "e"
    grid $Widgets(button,MaskVisible) -row 0 -column 1 -sticky "w"
    grid $fmask.lbMaskOpacity -row 1 -column 0 -sticky "e"
    grid $Widgets(scale,MaskOpacity) -row 1 -column 1 -sticky "w"

    set fbdry [ttk::labelframe $win.lfBdry -text "Boundary" -borderwidth 1 -relief solid]

    ttk::label $fbdry.lbVisible -text [mc "Visible"]:
    set Widgets(button,BdryVisible) $fbdry.btnVisible
    Button $Widgets(button,BdryVisible) -relief link \
        -image [$icn Get "light_bulb_off" -size 16] \
        -command [mymethod _OnClickVisibility_Boundary] -state disabled
    ttk::label $fbdry.lbOpacity -text [mc "Opacity"]:
    CScale $fbdry.scOpacity -command [mymethod _CmdChangeOpacity_Boundary] -value 1.0
    set Widgets(scale,BdryOpacity) $fbdry.scOpacity
    $Widgets(scale,BdryOpacity) state disabled
    ttk::label $fbdry.lbClip -text [mc "Clip"]:
    ttk::checkbutton $fbdry.chkClip -command [mymethod _CmdChangeClipBoundary] \
        -variable [myvar ClipBoundary]
    set Widgets(check,chkClip) $fbdry.chkClip

    grid $fbdry.lbVisible -row 0 -column 0 -sticky "e"
    grid $Widgets(button,BdryVisible) -row 0 -column 1 -sticky "w"
    grid $fbdry.lbOpacity -row 1 -column 0 -sticky "e"
    grid $Widgets(scale,BdryOpacity) -row 1 -column 1 -sticky "w"
    grid $fbdry.lbClip -row 2 -column 0 -sticky "e"
    grid $Widgets(check,chkClip) -row 2 -column 1 -sticky "w"

    grid $f -row 0 -column 0 -sticky snew
    grid $fmask -row 1 -column 0 -sticky snew
    grid $fbdry -row 3 -column 0 -sticky snew

    grid rowconfigure $win 3 -weight 1
    grid columnconfigure $win 2 -weight 1

    puts "====================="
    puts "win(Segmentation) = $win"
    puts "====================="


    ttk::frame $win.faction -borderwidth 1 -relief sunken
    ttk::button $win.faction.apply -text [mc "Compute"] \
        -command [mymethod _CmdComputeSegmentation]
    ttk::button $win.faction.genTetMesh -text [mc "Tet Mesh"] \
        -command [mymethod GenerateTetMesh]
    grid $win.faction.apply $win.faction.genTetMesh
    grid $win.faction -row 5 -column 0 -columnspan 3 -sticky snew
    $self buildVTKObjects
  }

  destructor {
    foreach obj [list $VolumeMask $VolumeMapper $BinaryMask \
                     $BoundaryActor $BoundaryMapper $Boundary $ToPolyData $ThresholdCaps $ClipClosedSurface] {
      if {$obj ne ""} {
        $obj Delete
      }
    }
  }

  method DisableWidgets { } {
    foreach btn [list MaskVisible BdryVisible] {
      $Widgets(button,$btn) configure -state disabled
    }
    foreach sc [list MaskOpacity BdryOpacity] {
      $Widgets(scale,$sc) state disabled
    }
  }

  method EnableWidgets { } {
    foreach btn [list MaskVisible BdryVisible] {
      $Widgets(button,$btn) configure -state normal
    }
    foreach sc [list MaskOpacity BdryOpacity] {
      $Widgets(scale,$sc) state !disabled
    }
  }

  method GetLabel { } {
    return "Segmentations"
  }

  method AddToRenderer { renderer } {
    set Renderer $renderer
    hook bind $Renderer <ChangeImageOrientation> $self [mymethod HookChangeImageOrientation]
    $renderer AddVolume $VolumeMask
    $renderer AddActor $BoundaryActor
  }

  method SetDataSet { dataSet } {
    set DataSet $dataSet
    $dataSet AddObserver AfterOpenEvent [mymethod OnOpenDataSet]
    $dataSet AddObserver ChangeTimeStep [mymethod OnChangeTimeStep %T]
    $dataSet AddObserver BeforeCloseEvent [mymethod OnCloseDataSet]
  }

  method OnOpenDataSet { } {
  }

  method OnCloseDataSet { } {
    $self ClearSegmentation
  }

  method OnChangeTimeStep { t } {
  }

  method _ReconfigureLabels { } {
    foreach axis {X Y Z} {
      set labelAxis [join [$Renderer VolumeOrienter GetOrientationLabel $axis] ""]
      $win.lfSpacing.lb${axis} configure -text "${labelAxis}:"
    }
  }

  method HookChangeImageOrientation { } {
    $self _ReconfigureLabels
  }

  method SetVisibility { v } {
    $self SetVisibility_Mask $v
    $self SetVisibility_Boundary $v
    #$VolumeMask SetVisibility $v
  }

  method SetVisibility_Mask { v } {
    $VolumeMask SetVisibility $v
    set icn [Icons GetInstance]
    set imgON [$icn Get "light_bulb_on" -size 16]
    set imgOFF [$icn Get "light_bulb_off" -size 16]
    $Widgets(button,MaskVisible) configure \
        -image [expr {$v ? $imgON : $imgOFF} ]
  }

  method SetVisibility_Boundary { v } {
    puts "============= SetVisibility_Boundary ============="
    puts "$BoundaryActor"
    puts "$BoundaryMapper"
    $BoundaryActor SetVisibility $v
    set icn [Icons GetInstance]
    set imgON [$icn Get "light_bulb_on" -size 16]
    set imgOFF [$icn Get "light_bulb_off" -size 16]
    $Widgets(button,BdryVisible) configure \
        -image [expr {$v ? $imgON : $imgOFF} ]
  }

  method GetVisibility { } {
    return [$VolumeMask GetVisibility]
  }

  method ClearSegmentation { } {
    $BoundaryMapper SetInputData ""
    $VolumeMapper SetInputData ""
    if { [info command $BinaryMask] eq "" } {
      set BinaryMask ""
    }
    if { $BinaryMask ne "" } {
      $BinaryMask Delete
      set BinaryMask ""
    }
    if { $ClipClosedSurface ne "" } {
      $ClipClosedSurface SetInputData ""
    }
    if { [info command $Boundary] eq "" } {
      set Boundary ""
    }
    if { $Boundary ne "" } {
      $Boundary Delete
      set Boundary ""
    }
    $self SetVisibility 0
    $self DisableWidgets
  }

  method GetSpacing { } {
    foreach a {X Y Z} {
      #set al [join [$Renderer VolumeOrienter GetOrientationLabel $a] ""]
      lappend spacing $Spacing($a)
    }
    return $spacing
  }

  method GetResolution { bounds } {
    foreach {min max} $bounds s [$self GetSpacing] {
      lappend resolution [expr { int( ceil( ($max - $min) / double($s) ) ) } ]
    }
    puts "Segmentation Resolution = $resolution"
    return $resolution
  }

  method _CmdComputeSegmentation { } {
    if { [$self ComputeSegmentation ] } {
      $self SetVisibility 1
      $self EnableWidgets
      hook call $self <Render>
    }
  }

  method _CmdChangeClipBoundary { } {
    puts "ClipClosedSurface = $ClipClosedSurface"
    puts "Boundary = $Boundary"
    puts "ToPolyData = $ToPolyData"
    $self _UpdateClipBoundary
    hook call $self <Render>
  }

  method FindLargestLine { } {
    if { [info command $LargestLine] ne "" } {
      $LargestLine Delete
    }
    vtkPolyDataWriter writer
    writer SetFileName "/tmp/centerline.vtk"
    hook call $self <RequestLargestLine> -filter writer
    writer Delete
  }

  method DumpMask { } {
    if { $BinaryMask ne "" } {
      puts [$BinaryMask Print]
    }
  }

  method ExportMask { fileName } {
    #$self ExportGenericObject $fileName $BinaryMask
    if { $BinaryMask ne "" } {
      #vtkStructuredPointsWriter writer
      vtkXMLImageDataWriter writer
      writer SetFileName $fileName
      writer SetInputData $BinaryMask
      writer Update
      writer Delete
    }
  }

  method ExportBoundary { fileName } {
    $self ExportGenericObject $fileName $Boundary
  }

  method ExportGenericObject { fileName obj } {
    if { $obj ne "" } {
      vtkGenericDataObjectWriter writer
      writer SetFileName $fileName
      writer SetInputData $obj
      writer Update
      writer Delete
    }
  }

  method GenerateTetMesh { } {
    set tmpdir [fileutil::tempdir]
    set tgdir [file join $tmpdir mesh[pid]]
    file mkdir $tgdir
    set stlInput [file join $tgdir "input.stl"]
    vtkSTLWriter writer
    writer SetFileName $stlInput
    $self _InjectBoundary writer
    writer Update
    writer Delete
    $self _InvokeMesher $stlInput
    #file delete -force $tgdir
  }

  method _InvokeMesher { pathSTL } {
    # TODO: Define a plugin structure to add different meshers and use
    # hook mechanism to invoke
    if { ![winfo exists $win.mesherLog] } {
      toplevel $win.mesherLog
      wm title $win.mesherLog "Mesher Output"
      text $win.mesherLog.t -wrap none \
          -yscrollcommand "$win.mesherLog.scrolly set" \
          -xscrollcommand "$win.mesherLog.scrollx set"
      ttk::scrollbar $win.mesherLog.scrolly \
          -orient v -command "$win.mesherLog.t yview"
      ttk::scrollbar $win.mesherLog.scrollx \
          -orient h -command "$win.mesherLog.t xview"
      grid $win.mesherLog.t -row 0 -column 0 -sticky snew
      grid $win.mesherLog.scrolly -row 0 -column 1 -sticky ns
      grid $win.mesherLog.scrollx -row 1 -column 0 -sticky ew
      grid columnconfigure $win.mesherLog 0 -weight 1
      grid rowconfigure $win.mesherLog 0 -weight 1
    } else {
      $win.mesherLog.t delete 1.0 end
    }
    set job [ jobexec create %AUTO% \
                  -command $ProgramMesher \
                  -arguments [list "-pkq1.2/10" $pathSTL] \
                  -verbose 1 \
                  -inbackground 1 \
                  -onoutputcmd [mytypemethod OnJobOutput $win.mesherLog.t] ]
    $job configure -jobshutdowncmd [mytypemethod OnJobFinish $win.mesherLog.t ]
    $job execute
  }

  typemethod OnJobFinish { w job } {
    $w insert end "Finishing job $job"
    $w see end
    $job destroy
  }

  typemethod OnJobOutput { w job data } {
    $w insert end "$data\n"
    $w see end
  }

  method ComputeSegmentation { args } {
    vtkAppendPoints appendPoints
    hook call $self <Status> [mc "Collecting points from streamlines ..."]
    hook call $self <RequestStreamPoints> -appendpoints appendPoints
    set numberOfPoints [[appendPoints GetOutput] GetNumberOfPoints]
    if {!$numberOfPoints} {
      MessageDlg $win.info -title [mc "Warning"] -icon warning -type ok \
          -message [mc "There are no points to segment"]
      appendPoints Delete
      hook call $self <Status> ""
      return 0
    }
    $self ClearSegmentation
    hook call $self <Status> \
        [mc "Using %d points in segmentation ..." $numberOfPoints]
    vtkPcmrPointLocator bin
    set resolution [$self GetResolution \
                        [[appendPoints GetOutput] GetBounds] ]
    puts "bin SetDivisions $resolution"
    bin SetDivisions {*}$resolution
    bin SetDataSet [appendPoints GetOutput]
    bin AutomaticOff
    bin BuildLocator
    set BinaryMask [vtkImageData New]
    bin GenerateBinaryRepresentation $BinaryMask
    #puts [bin Print]
    #puts [$BinaryMask Print]

    array set _args $args
    if {[info exist _args(-fileMask)]} {
      vtkGenericDataObjectWriter writer
      writer SetInputData $BinaryMask
      writer SetFileName $_args(-fileMask)
      writer Update
      hook call $self <Status> \
          [mc "Binary mask wrote in %s" $_args(-fileMask)]
      writer Delete
    }
    appendPoints Delete
    bin Delete
    $VolumeMapper SetInputData $BinaryMask
    vtkPcmrMaskSmoother smoother
    smoother SetInputMask $BinaryMask
    smoother SetPadding 8 8 8
    puts "voy a GetBoundarySmoothed"
    set Boundary [smoother GetBoundarySmoothed]
    puts [$Boundary Print]
    $Boundary Register ""
    # VER SI ESTA VIVO
    puts "$Boundary 0: [$Boundary GetClassName] ([$Boundary GetReferenceCount])"
    $self _UpdateClipBoundary
    # VER SI SIGUE VIVO
    puts "$Boundary 1: [$Boundary GetClassName] ([$Boundary GetReferenceCount])"
    smoother Delete
    puts "$Boundary 2: $Boundary [$Boundary GetClassName] ([$Boundary GetReferenceCount])"
    return 1
  }

  method GetBoundaryMesh { } {
    if { [$ClipClosedSurface GetNumberOfInputConnections 0] } {
      $ToPolyData Update
      return [$ToPolyData GetOutput]
    } else {
      return $Boundary
    }
  }

  method _InjectBoundary { filter } {
    if { [$ClipClosedSurface GetNumberOfInputConnections 0] } {
      $filter SetInputConnection [$ClipClosedSurface GetOutputPort]
    } else {
      $filter SetInputData $Boundary
    }
  }

  method _UpdateClipBoundary { } {
    $ClipClosedSurface SetInputData ""
    if { $ClipBoundary } {
      vtkPlaneCollection planeCollection
      hook call $self <RequestActivePlanes> -collection planeCollection
      puts [planeCollection Print]
      if { [planeCollection GetNumberOfItems] } {
        puts "$ClipClosedSurface SetInputData $Boundary"
        $ClipClosedSurface SetInputData $Boundary
        $ClipClosedSurface SetClippingPlanes planeCollection
        $BoundaryMapper SetInputConnection [$ToPolyData GetOutputPort]
      } else {
        $BoundaryMapper SetInputData $Boundary
      }
      planeCollection Delete
    } else {
      $BoundaryMapper SetInputData $Boundary
    }
  }

  method buildVTKObjects { } {
    set VolumeMapper [vtkSmartVolumeMapper $win.volumeMapper]
    set VolumeMask [vtkVolume $win.volumeMask]
    $VolumeMask VisibilityOff
    $VolumeMask SetMapper $VolumeMapper

    puts "VolumeMask = $VolumeMask"
    set volProperty [$VolumeMask GetProperty]
    $volProperty ShadeOff
    $volProperty SetInterpolationTypeToLinear

    set volColor [$volProperty GetRGBTransferFunction]
    $volColor RemoveAllPoints
    $volColor AddRGBPoint 0.0 0 0 0
    $volColor AddRGBPoint 1.0 1 1 1

    set volOpacity [$volProperty GetScalarOpacity]
    $volOpacity RemoveAllPoints
    $volOpacity AddPoint 0.0 0.0
    $volOpacity AddPoint 1.0 1.0

    set ClipClosedSurface [vtkClipClosedSurface New]
    $ClipClosedSurface SetScalarModeToLabels
    set ThresholdCaps [vtkThreshold New]
    $ThresholdCaps SetInputConnection [$ClipClosedSurface GetOutputPort]
    $ThresholdCaps SetInputArrayToProcess 0 0 0 1 "Labels"
    $ThresholdCaps ThresholdByLower 0.5
    set ToPolyData [vtkGeometryFilter New]
    $ToPolyData SetInputConnection [$ThresholdCaps GetOutputPort]

    set BoundaryMapper [vtkPolyDataMapper $win.bdryMapper]
    $BoundaryMapper ScalarVisibilityOff
    set BoundaryActor [vtkActor $win.bdryActor]
    $BoundaryActor SetMapper $BoundaryMapper
    $BoundaryActor VisibilityOff
    #[$BoundaryActor GetProperty] SetColor 216 101 79
    [$BoundaryActor GetProperty] SetDiffuseColor 0.84375 0.39453125 0.30859375
  }

  method GetCurrentOpacity { obj } {
    return [$Widgets(scale,${obj}Opacity) cget -value]
  }

  method _CmdChangeOpacity_Mask { v } {
    set funOpacity [[$VolumeMask GetProperty] GetScalarOpacity]
    $funOpacity AddPoint 1.0 [$self GetCurrentOpacity Mask]
    hook call $self <Render>
  }

  method _OnClickVisibility_Mask { } {
    set isON [$VolumeMask GetVisibility]
    $self SetVisibility_Mask [expr {$isON ? 0 : 1}]
    hook call $self <Render>
  }

  method _CmdChangeOpacity_Boundary { v } {
    [$BoundaryActor GetProperty] SetOpacity [$self GetCurrentOpacity Bdry]
    hook call $self <Render>
  }

  method _OnClickVisibility_Boundary { } {
    set isON [$BoundaryActor GetVisibility]
    $self SetVisibility_Boundary [expr {$isON ? 0 : 1}]
    hook call $self <Render>
  }

}
