package require vtk
package require vtkTclAddon
package require snit

snit::type PlaneQuantifier {
  typevariable ColorPresetTable -array {
    BlueRedRainbow,HueRange         {0.667 0}
    BlueRedRainbow,SaturationRange  {1 1}
    BlueRedRainbow,ValueRange       {1 1}
    CyanBlueMagenta,HueRange        {0.50 0.87}
    CyanBlueMagenta,SaturationRange {0.4 0.9}
    CyanBlueMagenta,ValueRange       {1 1}
    GreenYellowRed,HueRange         {0.3 0}
    GreenYellowRed,SaturationRange  {1 1}
    GreenYellowRed,ValueRange       {1 1}
    Red2Yellow,HueRange         {0 0.1}
    Red2Yellow,SaturationRange  {0.95 0.4}
    Red2Yellow,ValueRange       {0.3 1}
    Blue2Cyan,HueRange         {0.667 0.5}
    Blue2Cyan,SaturationRange  {0.95 0.4}
    Blue2Cyan,ValueRange       {0.3 1}
    Black2White,HueRange         {0 1}
    Black2White,SaturationRange  {0 0}
    Black2White,ValueRange       {0 1}
  }

  typevariable StreamlinePropertyNames {IntegrationDirection IntegratorType \
                                            TerminalSpeed \
                                            MaximumError MaximumPropagation \
                                            InitialIntegrationStep \
                                            MinimumIntegrationStep \
                                            MaximumIntegrationStep \
                                            MaximumNumberOfSteps}

  variable ColorPreset -array {
    Plane "BlueRedRainbow"
    Flow "BlueRedRainbow"
  }
  #variable ColorPreset "BlueRedRainbow"

  variable SeedSource  "Loop"

  variable DataSet ""
  variable TimeValues
  variable TemporalSource
  variable Renderer
  variable ProbeResolutionFactor 1.0
  variable VoxelSize
  variable PathlineResolution 5
  variable PathlineReinjectEvery 0
  variable PathlineTraceLength 2

  variable FlowTableData -array {}

  variable Visibility -array {
    Global 0
    ProbeActor 1
    SplineActor 1
    VectorActor 0
    StreamlineActor 0
    PathlineActor 0
  }

  variable StreamlineProgress -array {
    value,absolute 0
    value,relative 0
    counter 0
    aborted 0    
  }

  variable LookupTable -array {
    Plane ""
    Flow ""
  }

  #component LookupTable   -public LookupTable

  # Info for each timestep
  # TimeStepObject($t,ParametricFunctionSource)
  # TimeStepObject($t,ParametricSpline)  
  # TimeStepObject($t,ImplicitLoop)
  # TimeStepObject($t,ExtractCell)
  # TimeStepObject($t,CleanContour)

  # TimeStepObject($t,Pathline)
  # TimeStepObject($t,ScaleToCM)
  # TimeStepObject($t,ClipAgeUpper)
  # TimeStepObject($t,ClipAgeLower)
  # TimeStepObject($t,ToPolyData)
  # TimeStepObject($t,PathlineMapper)
  # TimeStepObject($t,PathlineActor)
  # TimeStepObject($t,)
  variable TimeStepObject
  component SplineMapper -public SplineMapper
  component SplineActor -public SplineActor
  
  # Pipeline: ProbePlane
  component PlaneSource   -public PlaneSource
  component ProbeFilter   -public ProbeFilter
  component ProbeMapper   -public ProbeMapper
  component ProbeActor    -public ProbeActor

  # Pipeline: HedgeHog
  component ProbeVector   -public ProbeVector
  component HedgeHog      -public HedgeHog
  component VectorMapper  -public VectorMapper
  component VectorActor   -public VectorActor

  # Pipeline: Streamline
  component Streamer         -public Streamer
  component StreamlineMapper -public StreamlineMapper
  component StreamlineActor  -public StreamlineActor
  
  # Pipeline: Pathline

  variable NumberOfSplineVertex 10
  variable ParametricResolution 100
  variable CurrentTimeStep

  delegate method GetIntegratorType to Streamer
  delegate method GetIntegrationDirection to Streamer
  delegate method GetTerminalSpeed to Streamer
  delegate method GetInitialIntegrationStep to Streamer
  delegate method GetMinimumIntegrationStep to Streamer
  delegate method GetMaximumIntegrationStep to Streamer
  delegate method GetMaximumError to Streamer
  delegate method GetMaximumPropagation to Streamer
  delegate method GetMaximumNumberOfSteps to Streamer

  constructor { args } {
    catch {
    set t0 [clock millisecond]
    $self buildVTKObjects
    set t1 [clock milliseconds]
    puts "Plano construido en [expr {($t1-$t0)/1000.0}]"    
    } msg
    puts "catched msg = $msg"
  }

  destructor {
    puts "destroying $self ..."
    set ren [$Renderer ImageRender GetActualRenderer]
    $ren RemoveActor $SplineActor 
    $ren RemoveActor $ProbeActor
    $ren RemoveActor $VectorActor
    $ren RemoveActor $StreamlineActor
    #$LookupTable Delete
    $LookupTable(Plane) Delete
    $LookupTable(Flow) Delete
    $SplineMapper Delete
    $SplineActor Delete
    $ProbeFilter Delete
    $ProbeMapper Delete
    $ProbeActor Delete
    $ProbeVector Delete
    $HedgeHog Delete
    $VectorMapper Delete
    $VectorActor Delete
    $Streamer Delete
    $StreamlineMapper Delete
    $StreamlineActor Delete
    $self DestroyTimeStepObject
  }
  
  method AddToRenderer { renderer } {
    set Renderer $renderer
    $Renderer AddActor $SplineActor
    $Renderer AddActor $ProbeActor
    $Renderer AddActor $VectorActor
    $Renderer AddActor $StreamlineActor
  }
  
  method SetDataSet { dataSet } {
    set DataSet $dataSet
  }

  method SetTemporalSource { TmpSource } {
    set TemporalSource $TmpSource
  }

  method OnOpenDataSet { } {
    set min -1
    foreach s [$DataSet GetVoxelSpacing] {
      if {$min == -1 || $s < $min} {
        set min $s
      }
    }
    set VoxelSize $min    
    $self UpdatePlaneResolution
    $TemporalSource SetTclDataSet $DataSet
    set TimeValues [$TemporalSource GetTimeValues]    
  }

  method CloseDataSet { } {
    $ProbeFilter SetSourceData ""
    $self DestroyTimeStepObject
  }

  method GetTimeStepObject { t object } {
    return $TimeStepObject($t,$object)
  }

  method ChangeTimeStep { t } {
    set CurrentTimeStep $t
    if {![$self GetVisibility]} {
      return
    }
    set vectorImage [$DataSet GetVelocityImage -component FLOW -vtk yes]
    $ProbeFilter SetSourceData $vectorImage
    $SplineMapper SetInputConnection \
        [[$self GetTimeStepObject $t ParametricFunctionSource] GetOutputPort]
    $ProbeVector SetSourceData $vectorImage
    $ProbeVector SetInputConnection \
        [[$self GetTimeStepObject $t CleanContour] GetOutputPort]
    $Streamer SetSourceConnection \
        [[$self GetTimeStepObject $t CleanContour] GetOutputPort]
    $Streamer SetInputData $vectorImage
    if {[$self GetTimeStepObject $CurrentTimeStep UpdateImplicitLoop]} {
      $self UpdateImplicitLoop $CurrentTimeStep

      #REVIEW: remove this
      if {0 && $t == 0} {
        $self UpdatePathline 0
      }
    }

    # REMOVE:
    if { 0 } {
      set extractCell [$self GetTimeStepObject $CurrentTimeStep ExtractCell]
      if {[$extractCell GetImplicitFunction] eq ""} {
        $self UpdateImplicitLoop $CurrentTimeStep
      }
    }

    $self UpdatePathlineThreshold $t
  }

  method UpdatePathlineThreshold { ct } {
    # REVIEW: we start visualizing just the pathline for t=0, when
    # reinjection be implemented a more complex Update should be
    # implemented.
    set ut [lindex $TimeValues $ct]
    set i0 [expr {$ct-$PathlineTraceLength}]
    if {$i0 < 0} {
      set i0 0
    }
    set lt [lindex $TimeValues $i0]
   set state $Visibility(PathlineActor)

    puts "UpdatePathlineThreshold ($ct): state = $state"
    if {$state} {
      # the pathline for current timestep will be disabled
      for {set t 0} {$t < $ct} {incr t} {
        set actor [$self GetTimeStepObject $t PathlineActor]
        puts "$t && (!$PathlineReinjectEvery || !($t % $PathlineReinjectEvery)) = [expr {$t && (!$PathlineReinjectEvery || !($t % $PathlineReinjectEvery))}]"
        if {$t && (!$PathlineReinjectEvery || ($t % $PathlineReinjectEvery))} {
          $actor SetVisibility 0
          continue
        }
        $self UpdateImplicitLoop $t
        set clipAgeUpper [$self GetTimeStepObject $t ClipAgeUpper]
        set clipAgeLower [$self GetTimeStepObject $t ClipAgeLower]
        set scaler [$self GetTimeStepObject $t ScaleToCM]
        puts "scaler = $scaler ($lt,$ut)"
        set bt [lindex $TimeValues $t]
        $clipAgeUpper SetValue [expr {$ut-$bt}]
        $clipAgeLower SetValue [expr {$lt-$bt}]
        $actor SetVisibility 1
      }
    } else {
      set t 0
    }
    # disable the following pathline
    set T [$DataSet GetNumberOfTimeStep]
    for {} {$t < $T} {incr t} {
      set actor [$self GetTimeStepObject $t PathlineActor]
      $actor SetVisibility 0
    }
  }
  
  method SetVisibility0 { state } {
    if {$Visibility(Global) == $state} {
      return
    }
    set Visibility(Global) $state
    if {$state} {
      foreach {cls met} [list SplineActor SetVisibility \
                             ProbeActor SetVisibility \
                             VectorActor SetVisibility \
                             StreamlineActor SetVisibility] {
        set obj [set $cls]
        $obj $met $Visibility($cls)
      }
      $self ChangeTimeStep [$DataSet GetCurrentTimeStep]
    } else {
      foreach {cls met} {SplineActor Visibility \
                             ProbeActor Visibility \
                             VectorActor Visibility \
                             StreamlineActor Visibility} {
        set obj [set $cls]
        set $Visibility($cls) [$obj Get$met]
        $obj Set$met $state
      }
    }
  }

  method GetProbeActor { } {
    return $ProbeActor
  }

  method GetVisibility { } {
    foreach o [list ProbePlane HedgeHog Streamline Pathline] {
      if {[$self Get${o}Visibility]} {
        return 1
      }
    }
    return 0
  }

  method SetLookupTableRange { min max } {
    foreach typeLT {Plane Flow} {
      $LookupTable($typeLT) SetRange $min $max
      $LookupTable($typeLT) Build
    }
  }

  #method SetLookupTableRange { min max } {
  #  $LookupTable SetRange $min $max
  #  $LookupTable Build
  #}

  method UpdatePlaneResolution { } {
    VTKUtil::SetPlaneElementSize $PlaneSource [expr {$VoxelSize*$ProbeResolutionFactor}]
  }

  method SetProbeResolutionFactor { f } {
    set ProbeResolutionFactor $f
    $self UpdatePlaneResolution
    # REVIEW: no reinjection is considered.
    #[$self GetTimeStepObject 0 Pathline] Modified
    #$self UpdatePathline 0
  }

  method buildVTKObjects { } {
    #install LookupTable using vtkLookupTable $self.lookupTable
    #$LookupTable SetVectorModeToMagnitude

    foreach typeLT {Plane Flow} {
      set LookupTable($typeLT) [vtkLookupTable $self.lookupTable${typeLT}]
      $LookupTable($typeLT) SetVectorModeToMagnitude
    }

    parray LookupTable

    # Spline
    install SplineMapper using vtkPolyDataMapper $self.splineMapper
    $SplineMapper SetResolveCoincidentTopologyToPolygonOffset
    install SplineActor using  vtkActor $self.splineActor
    $SplineActor SetMapper $SplineMapper
    $SplineActor VisibilityOff

    # Filter to probe the velocity magnitude along a plane
    install PlaneSource using vtkPlaneSource $self.planeSource
    install ProbeFilter using vtkProbeFilter $win.probeFilter
    $ProbeFilter SetInputConnection [$PlaneSource GetOutputPort]

    # Mapper for velocity magnitude contour
    install ProbeMapper using vtkPolyDataMapper $self.probeMapper
    $ProbeMapper SetInputConnection [$ProbeFilter GetOutputPort]
    $self _setLookupTableForMapper $ProbeMapper Plane

    # Actor for velocity magnitude contour
    install ProbeActor using vtkActor $self.probeActor
    $ProbeActor SetMapper $ProbeMapper
    $ProbeActor VisibilityOff

    # HedgeHog pipeline
    install ProbeVector using vtkProbeFilter $win.probeVector
    install HedgeHog using vtkHedgeHog $self.hedgeHog
    $HedgeHog SetInputConnection [$ProbeVector GetOutputPort]
    $HedgeHog SetScaleFactor 0.1
    install VectorMapper using vtkPolyDataMapper $self.vectorMapper
    $VectorMapper SetInputConnection [$HedgeHog GetOutputPort]
    $self _setLookupTableForMapper $VectorMapper Flow
    install VectorActor using vtkActor  $self.vectorActor
    $VectorActor SetMapper $VectorMapper
    $VectorActor VisibilityOff

    # Streamline pipeline
    install Streamer using vtkStreamTracer $self.streamer
    $Streamer SetTerminalSpeed 0.1
    $Streamer SetMaximumError 0.01
    $Streamer SetMaximumPropagation 500
    $Streamer SetIntegrationStepUnit 2
    $Streamer SetInitialIntegrationStep 1.0
    $Streamer SetMinimumIntegrationStep 0.4
    $Streamer SetMaximumIntegrationStep 3.0
    $Streamer SetIntegratorTypeToRungeKutta45
    $Streamer SetComputeVorticity 0
    $Streamer SetIntegrationDirectionToForward
    $Streamer AddObserver StartEvent [mymethod OnStreamlineStartEvent]
    $Streamer AddObserver ProgressEvent [mymethod OnStreamlineProgressEvent]
    $Streamer AddObserver EndEvent [mymethod OnStreamlineEndEvent]

    install StreamlineMapper using vtkPolyDataMapper $self.streamlineMapper
    $StreamlineMapper SetInputConnection [$Streamer GetOutputPort]
    $self _setLookupTableForMapper $StreamlineMapper Flow
    install StreamlineActor using vtkActor $self.streamlineActor
    $StreamlineActor SetMapper $StreamlineMapper
    $StreamlineActor VisibilityOff
  }

  method WriteLines { path } {
    set w [ vtkPolyDataWriter New ]
    $w SetFileName $path
    $w SetInputConnection [$Streamer GetOutputPort]
    $w Update
    $w Delete
  }

  method WriteCenterLine { path } {
    set clean [vtkCleanPolyData New]
    $clean SetInputConnection [$Streamer GetOutputPort]
    vtkPolyDataConnectivityFilter connectivityFilter
    connectivityFilter ScalarConnectivityOn
    connectivityFilter SetExtractionModeToLargestRegion
    connectivityFilter SetInputConnection [$clean GetOutputPort]
    set w [ vtkPolyDataWriter New ]
    $w SetFileName $path
    $w SetInputConnection [connectivityFilter GetOutputPort]
    $w Update
    $w Delete
    connectivityFilter Delete
    $clean Delete
  }

  method InitializeTimeStepObject { } {
    $self DestroyTimeStepObject
    set T [$DataSet GetNumberOfTimeStep]
    set TimeStepObject(NumberOfTimeStep) $T
    for {set t 0} {$t < $T}  {incr t} {
      $self _initializeSplineObject $t
      $self _initializeLoopObject $t
      $self _initializePathlineObject $t
    }
  }

  method _initializeSplineObject { t } {
    set spline [vtkParametricSpline New]
    $spline ClosedOn
    set functionSource [vtkParametricFunctionSource New]
    $functionSource SetParametricFunction $spline
    $functionSource SetScalarModeToNone
    $functionSource GenerateTextureCoordinatesOff
    $functionSource SetUResolution $ParametricResolution
    set TimeStepObject($t,ParametricFunctionSource) $functionSource
    set TimeStepObject($t,ParametricSpline) $spline
  }

  method _initializeLoopObject { t } {
    set TimeStepObject($t,ImplicitLoop) [vtkImplicitSelectionLoop New]
    $TimeStepObject($t,ImplicitLoop) AutomaticNormalGenerationOff
    set TimeStepObject($t,UpdateImplicitLoop) 1
    set TimeStepObject($t,ExtractCell) [vtkExtractPolyDataGeometry New]
    $TimeStepObject($t,ExtractCell) SetInputConnection \
        [$PlaneSource GetOutputPort]
    $TimeStepObject($t,ExtractCell) ExtractInsideOn
    $TimeStepObject($t,ExtractCell) ExtractBoundaryCellsOff
    set TimeStepObject($t,CleanContour) [vtkCleanPolyData New]
    $TimeStepObject($t,CleanContour) SetInputConnection \
        [$TimeStepObject($t,ExtractCell) GetOutputPort]
    $TimeStepObject($t,CleanContour) PointMergingOff
  }

  method _initializeHedgeHogObject { t } {
  }

  method _initializePathlineObject { t } {
    set pathline [set TimeStepObject($t,Pathline) [vtkParticlePathFilter New]]
    $pathline SetInputConnection 0 [$TemporalSource GetOutputPort]
    $pathline SetInputConnection 1 \
        [[$self GetTimeStepObject $t CleanContour] GetOutputPort]
    $pathline SetStartTime [lindex $TimeValues $t]
    $pathline SetTerminationTime [lindex $TimeValues end]
    $pathline SetComputeVorticity 0
    $pathline SetTerminalSpeed 1
    $pathline SetIntegratorType 1

    $pathline AddObserver StartEvent [mymethod OnPathlineStartEvent $t]
    $pathline AddObserver EndEvent [mymethod OnPathlineEndEvent $t]
    set TimeStepObject($t,NeedReupdatePathline) 1
    #set TimeStepObject($t,LastPathlineMTime) [$pathline GetMTime]

    set scaleToCM [set TimeStepObject($t,ScaleToCM) [vtkArrayCalculator New]]
    $scaleToCM SetInputConnection [$pathline GetOutputPort]
    $scaleToCM AddVectorVariable "V" "scalars" 0 1 2
    $scaleToCM SetFunction "0.1*V"
    $scaleToCM SetResultArrayName "scalars"
    
    set clipAgeUpper [set TimeStepObject($t,ClipAgeUpper) [vtkClipDataSet New]]
    $clipAgeUpper SetInputArrayToProcess 0 0 0 0 "ParticleAge"
    $clipAgeUpper SetInputConnection [$scaleToCM GetOutputPort]
    $clipAgeUpper SetValue 1.0
    $clipAgeUpper InsideOutOn
    
    set clipAgeLower [set TimeStepObject($t,ClipAgeLower) [vtkClipDataSet New]]
    $clipAgeLower SetInputArrayToProcess 0 0 0 0 "ParticleAge"
    $clipAgeLower SetInputConnection [$clipAgeUpper GetOutputPort]
    $clipAgeLower SetValue 0.0
    $clipAgeLower InsideOutOff
 
    set toPolyData [set TimeStepObject($t,ToPolyData) [vtkGeometryFilter New]]
    $toPolyData SetInputConnection [$clipAgeLower GetOutputPort]
    $toPolyData MergingOn

    set pathlineMapper [set TimeStepObject($t,PathlineMapper) [vtkPolyDataMapper New]]
    $pathlineMapper SetInputConnection [$toPolyData GetOutputPort]
    $self _setLookupTableForMapper $pathlineMapper Flow

    set pathlineActor [set TimeStepObject($t,PathlineActor) [vtkActor New]]
    $pathlineActor SetMapper $pathlineMapper
    $pathlineActor VisibilityOff
    $Renderer AddActor $pathlineActor
  }

  method _getVectorName { obj } {
    return [[[$obj GetPointData] GetVectors] GetName]
  }

  method _getVectorNameInput { filter } {
    set inputAlg [$filter GetInputAlgorithm]
    $inputAlg Update
    set inputFilter [$inputAlg GetOutput]
    return [$self _getVectorName $inputFilter]
  }

  method _setLookupTableForMapper { mapper typeLT } {
    $mapper SetScalarModeToUsePointFieldData
    #$ProbeFilter Update
    #set dataExample [$ProbeFilter GetOutput]
    $mapper SelectColorArray "scalars"
    #[$self _getVectorName $dataExample]
    $mapper UseLookupTableScalarRangeOn
    $mapper SetLookupTable $LookupTable($typeLT)
  }

  method LoadFromXml { pathXML } {
    $self InitializeTimeStepObject
    set t0 [clock milliseconds]
    set fd [open $pathXML]
    set bufferXML [string trim [read $fd]]
    close $fd
    set doc [dom parse $bufferXML]
    set root [$doc documentElement]
    set NumberOfControlPoints \
        [$doc selectNodes string(/QuantifyPlane/NumberOfHandles)]
    #set handleSize [$doc selectNodes string(/QuantifyPlane/HandleSize)]
    set planeOrigin [$doc selectNodes string(/QuantifyPlane/Plane/Origin)]
    set planePoint1 [$doc selectNodes string(/QuantifyPlane/Plane/Point1)]
    set planePoint2 [$doc selectNodes string(/QuantifyPlane/Plane/Point2)]

    $PlaneSource SetOrigin {*}$planeOrigin
    $PlaneSource SetPoint1 {*}$planePoint1
    $PlaneSource SetPoint2 {*}$planePoint2

    set nodeContours [$root selectNodes Contours]

    set t0.1 [clock milliseconds]
    puts "antes de cargar nodos: [expr {(${t0.1}-$t0)/1000.0}]"
    foreach nodeSpline [$nodeContours childNodes] {
      set splineIndex [$nodeSpline getAttribute index]
      set spline [$self GetTimeStepObject $splineIndex ParametricSpline]
      $spline SetNumberOfPoints $NumberOfControlPoints
      foreach nodePoint [$nodeSpline childNodes] {
        set pointIndex [$nodePoint getAttribute index]
        $spline SetPoint $pointIndex {*}[$nodePoint selectNodes {string()}]
      }
    }

    $self xmlLoadProperties $Streamer [$root selectNodes Streamline]
    
    $self UpdatePlaneResolution
    $doc delete
  }
  
  method Store { name path } {
    set doc [dom createDocument QuantifyPlane]
    set root [$doc documentElement]
    $root setAttribute version 1.0
    $root setAttribute name $name
    # NumberOfHandles
    set subnode [$doc createElement NumberOfHandles]
    set spline [$self GetTimeStepObject $CurrentTimeStep ParametricSpline]
    $subnode appendChild [$doc createTextNode [[$spline GetPoints] GetNumberOfPoints]]
    $root appendChild $subnode
    # Plane 
    set nodePlane [$doc createElement Plane]
    $root appendChild $nodePlane
    # Plane Origin
    set nodeOrigin [$doc createElement Origin]
    $nodePlane appendChild $nodeOrigin
    $nodeOrigin appendChild [$doc createTextNode [$PlaneSource GetOrigin]]
    # Plane Point1
    set nodePoint1 [$doc createElement Point1]
    $nodePlane appendChild $nodePoint1
    $nodePoint1 appendChild [$doc createTextNode [$PlaneSource GetPoint1]]
    # Plane Point2
    set nodePoint2 [$doc createElement Point2]
    $nodePlane appendChild $nodePoint2
    $nodePoint2 appendChild [$doc createTextNode [$PlaneSource GetPoint2]]
    # Contours
    set nodeContour [$doc createElement Contours]
    $root appendChild $nodeContour
    set T [$DataSet GetNumberOfTimeStep]
    for {set t 0} {$t < $T} {incr t} {
      set nodeS [$doc createElement Spline]
      $nodeS setAttribute index $t
      $nodeContour appendChild $nodeS
      # iterate over control points
      set spline [$self GetTimeStepObject $t ParametricSpline]
      set points [$spline GetPoints]
      for {set i 0} {$i < [$points GetNumberOfPoints]} {incr i} {
        set nodeP [$doc createElement Point]
        $nodeP setAttribute index $i
        $nodeS appendChild $nodeP
        set hp [$points GetPoint $i]
        $nodeP appendChild [$doc createTextNode $hp]
      }
    }
    # Streamline
    $self xmlAppendNodeProperties $doc $root "Streamline" \
        [$self GetStreamlineProperties]
        
    set fd [open $path w]
    puts -nonewline $fd [$doc asXML]
    close $fd
    $doc delete
  }

  method xmlAppendNodeProperties {doc parent name properties} {
    set node [$doc createElement $name]
    $parent appendChild $node
    foreach {name value} $properties {
      $self xmlAppendItem $doc $node $name $value
    }
  }

  method xmlAppendItem {doc parent name value} {
    set nodeItem [$doc createElement $name]
    $parent appendChild $nodeItem
    $nodeItem appendChild [$doc createTextNode $value]
  }

  method xmlLoadProperties {obj node} {
    #puts "xmlLoadProperties, node = '$node'"
    if {$node eq ""} {
      return
    }
    foreach item [$node childNodes] {
      set p [$item nodeName]
      #$Streamer Set${p} [$item selectNodes {string()}]
      $obj Set${p} [$item selectNodes {string()}]
    }
   
  }

  method GetStreamlineProperties { } {
    set result [list ]
    foreach p $StreamlinePropertyNames {
      lappend result $p [$Streamer Get${p}]
    }
    return $result
  }

  method DestroyTimeStepObject { } {
    if {![info exists TimeStepObject(NumberOfTimeStep)]} {
      return
    }
    set T $TimeStepObject(NumberOfTimeStep)
    for {set t 0} {$t < $T} {incr t} {
      $self _destroySplineObject $t
      $self _destroyLoopObject $t
      $self _destroyPathlineObject $t
    }
    array unset TimeStepObject
  }

  method _destroySplineObject { t } {
    $TimeStepObject($t,ParametricFunctionSource) Delete
    $TimeStepObject($t,ParametricSpline) Delete
  }

  method _destroyLoopObject { t } {
    $TimeStepObject($t,ImplicitLoop) Delete
    $TimeStepObject($t,ExtractCell) Delete
    $TimeStepObject($t,CleanContour) Delete
  }

  method _destroyPathlineObject { t } {
    set ren [$Renderer ImageRender GetActualRenderer]
    $ren RemoveActor $TimeStepObject($t,PathlineActor)
    foreach obj {PathlineActor PathlineMapper ToPolyData ClipAgeLower \
                     ClipAgeUpper ScaleToCM Pathline} {
      $TimeStepObject($t,$obj) Delete
    }
  }

  method AlignToAxis { a } {
    set vectorImage [$DataSet GetVelocityImage -component FLOW -vtk yes]
    set origin [$vectorImage GetOrigin]
    set spacing [$vectorImage GetSpacing]
    set dimensions [$vectorImage GetDimensions]
    set corner [list]
    set center [list]
    foreach o $origin s $spacing d $dimensions {
      set c [expr {$o + $s * $d}]
      lappend corner $c
      lappend center [expr {($o + $c)/2}]
    }
    switch $a {
      X {
        set hx [lindex $center 0]
        set origin [list $hx [lindex $origin 1] [lindex $origin 2]]
        set point1 [list $hx [lindex $corner 1] [lindex $origin 2]]
        set point2 [list $hx [lindex $origin 1] [lindex $corner 2]]
      }
      Y {
        set hy [lindex $center 1]
        set origin [list [lindex $origin 0] $hy [lindex $origin 2]]
        set point1 [list [lindex $corner 0] $hy [lindex $origin 2]]
        set point2 [list [lindex $origin 0] $hy [lindex $corner 2]]
      }
      Z {
        set hz [lindex $center 2]
        set origin [list [lindex $origin 0] [lindex $origin 1] $hz]
        set point1 [list [lindex $corner 0] [lindex $origin 1] $hz]
        set point2 [list [lindex $origin 0] [lindex $corner 1] $hz]
      }
    }
    $self ChangePlane $origin $point1 $point2
    $self UpdatePlaneResolution
  }

  method InitializeDefault { } {
    $self InitializeTimeStepObject
    set vectorImage [$DataSet GetVelocityImage -component FLOW -vtk yes]
    set origin [$vectorImage GetOrigin]
    set spacing [$vectorImage GetSpacing]
    set dimensions [$vectorImage GetDimensions]
    set corner [list]
    foreach o $origin s $spacing d $dimensions {
      lappend corner [expr {$o + $s * $d}]
    }
    set hy [expr {([lindex $origin 1]+[lindex $corner 1])/2.0}]
    $PlaneSource SetOrigin [lindex $origin 0] $hy [lindex $origin 2]
    $PlaneSource SetPoint1 [lindex $corner 0] $hy [lindex $origin 2]
    $PlaneSource SetPoint2 [lindex $origin 0] $hy [lindex $corner 2]
    set points [VTKUtil::GetRegularPolygonPoints $PlaneSource $NumberOfSplineVertex]
    set T [$DataSet GetNumberOfTimeStep]
    for {set t 0} {$t < $T}  {incr t} {
      set spline [$self GetTimeStepObject $t ParametricSpline]
      set i 0
      $spline SetNumberOfPoints $NumberOfSplineVertex
      foreach pt $points {
        $spline SetPoint $i {*}$pt
        incr i
      }
    }
    $self UpdatePlaneResolution
  }
  
  method ResetSplinePosition { } {
    set points [VTKUtil::GetRegularPolygonPoints \
                    $PlaneSource $NumberOfSplineVertex]
    $self SetSplinePoints $points
  }

  method ResetAllSplinePosition { } {
    set points [VTKUtil::GetRegularPolygonPoints \
                    $PlaneSource $NumberOfSplineVertex]
    set T [$DataSet GetNumberOfTimeStep]
    for {set t 0} {$t < $T} {incr t} {
      $self SetSplinePointsAt $t $points
    }
  }

  method CopySplineAll { } {
    set points [$self GetSplinePoints]
    set T [$DataSet GetNumberOfTimeStep]
    for {set t 0} {$t < $T} {incr t} {
      if {$t != $CurrentTimeStep} {
        $self SetSplinePointsAt $t $points
      }
    }
  }

  method CopySplineForward { } {
    set points [$self GetSplinePoints]
    set T [$DataSet GetNumberOfTimeStep]
    for {set t [expr {$CurrentTimeStep+1}]} {$t < $T} {incr t} {
      $self SetSplinePointsAt $t $points
    }
  }

  method CopySplineNext { } {
    set next [expr {$CurrentTimeStep + 1}]
    if {$next < [$DataSet GetNumberOfTimeStep]} {
      set points [$self GetSplinePoints]
      $self SetSplinePointsAt $next $points
    }
  }

  method CopySplineBackward { } {
    set points [$self GetSplinePoints]
    for {set t [expr {$CurrentTimeStep-1}]} {$t >= 0} {incr t -1} {
      $self SetSplinePointsAt $t $points
    }
  }

  method CopySplinePrevious { } {
    set previous [expr {$CurrentTimeStep - 1}]
    if {$previous >= 0} {
      set points [$self GetSplinePoints]
      $self SetSplinePointsAt $previous $points
    }
  }

  # REVIEW: this method is not used, could be removed
  method RefreshPathline { t } {
    #REVIEW: only pathline 0 is considered
    [$self GetTimeStepObject $t Pathline] Modified
    $self UpdatePathline $t
  }

  method GetSplinePoints { } {
    set spline [$self GetTimeStepObject $CurrentTimeStep ParametricSpline]
    set points [$spline GetPoints]
    set result [list]
    set n [$points GetNumberOfPoints]
    for {set i 0} {$i < $n} {incr i} {
      lappend result [$points GetPoint $i]
    }
    return $result
  }

  method GetProbePlaneVisibility { } {
    return [$ProbeActor GetVisibility]    
  }

  method SetProbePlaneVisibility { state } {
    $ProbeActor SetVisibility $state
    $SplineActor SetVisibility $state
    # REVIEW: remove the array Visibility
    set Visibility(ProbeActor) $state
    set Visibility(SplineActor) $state
    $self ChangeTimeStep [$DataSet GetCurrentTimeStep]
  }

  method GetHedgeHogVisibility { } {
    return [$VectorActor GetVisibility]
  }

  method SetHedgeHogVisibility { state } {
    $VectorActor SetVisibility $state
    set Visibility(VectorActor) $state
    puts "HedgeHog = $HedgeHog"
    puts "VectorActor = $VectorActor"
    puts "ExtractCell = [$self GetTimeStepObject $CurrentTimeStep ExtractCell]"
    $self ChangeTimeStep [$DataSet GetCurrentTimeStep]
  }

  method GetStreamlineVisibility { } {
    return [$StreamlineActor GetVisibility]
  }

  method SetStreamlineVisibility { state } {
    $StreamlineActor SetVisibility $state
    set Visibility(StreamlineActor) $state
    $self ChangeTimeStep [$DataSet GetCurrentTimeStep]
  }

  method GetPathlineVisibility { } {
    return $Visibility(PathlineActor)
  }

  method SetPathlineVisibility { state } {
    set Visibility(PathlineActor) $state
    $self UpdatePathlines
  }

  method SetSplinePointsAt { t points } {
    puts "CHECK SetSplinePointsAt $t"
    set spline [$self GetTimeStepObject $t ParametricSpline]
    set i 0
    foreach pt $points {
      $spline SetPoint $i {*}$pt
      incr i
    }
    set TimeStepObject($t,UpdateImplicitLoop) 1
  }

  method SetSplinePoints { points } {
    $self SetSplinePointsAt $CurrentTimeStep $points
    $self UpdateImplicitLoop $CurrentTimeStep
  }

  method ChangePlane { origin point1 point2 } {
    $PlaneSource SetOrigin {*}$origin
    $PlaneSource SetPoint1 {*}$point1
    $PlaneSource SetPoint2 {*}$point2
    set T [$DataSet GetNumberOfTimeStep]
    # project all spline onto new plane position
    for {set t 0} {$t < $T} {incr t} {
      set spline [$self GetTimeStepObject $t ParametricSpline]
      # REVIEW: change to use vtkPcmrWrapper 
      vtkSplineProjectOnPlane $spline $origin [$PlaneSource GetNormal]
      set TimeStepObject($t,UpdateImplicitLoop) 1
    }
    #REVIEW: after ChangePlane always comes SetSplinePoints
    #$self UpdateImplicitLoop
  }

  method TemporalRestoreFlowActors { } {
    $StreamlineActor SetVisibility $Visibility(StreamlineActor)
    $self TemporalRestorePathlineActors
  }

  method TemporalRestorePathlineActors { } {
    # REVIEW: for now only first pathline is shown
    set pathlineActor [$self GetTimeStepObject 0 PathlineActor]
    $pathlineActor SetVisibility $Visibility(PathlineActor)

    #REVIEW: this will recompute the pathline if needed
    #[$self GetTimeStepObject 0 Pathline] Modified
    #$self UpdatePathline 0
  }

  method TemporalDisableFlowActors { } {
    $StreamlineActor SetVisibility 0
    $self TemporalDisablePathlineActors
  }

  method TemporalDisablePathlineActors { } {
    # REVIEW: for now only first pathline is shown
    set pathlineActor [$self GetTimeStepObject 0 PathlineActor]
    $pathlineActor SetVisibility 0
  }

  method InvertNormal { } {
    foreach {nx ny nz} [$PlaneSource GetNormal] break
    $PlaneSource SetNormal [expr {-1.0*$nx}] [expr {-1.0*$ny}] [expr {-1.0*$nz}]
  }

  method GetColorPreset { typeLT } {
    return $ColorPreset($typeLT)
  }

  method SetColorPreset { preset typeLT } {
    if {![info exists ColorPresetTable($preset,HueRange)]} {
      return
    }
    parray LookupTable
    set ColorPreset($typeLT) $preset
    $LookupTable($typeLT) SetHueRange {*}$ColorPresetTable($preset,HueRange)
    $LookupTable($typeLT) SetSaturationRange {*}$ColorPresetTable($preset,SaturationRange)
    $LookupTable($typeLT) SetValueRange {*}$ColorPresetTable($preset,ValueRange)
    $LookupTable($typeLT) Build
  }

  method GetSeedSource { } {
    return $SeedSource
  }

  method SetSeedSource { src } {
    if {[lsearch -exact -- {"Loop" "Plane"} $src] == -1} {
      return
    }
    set SeedSource $src
    $self SetSeedSourceTo${src}
  }

  # This method is used to avoid unnecesary computation for the
  # interior of the implicit loop at each time-step
  method UpdateImplicitLoop { t } {
    puts "ENTER: UpdateImplicitLoop"
    if {!$TimeStepObject($t,UpdateImplicitLoop)} {
      return
    }
    puts "UpdateImplicitLoop"
    set implicitLoop [$self GetTimeStepObject $t ImplicitLoop]
    set pointsLoop [$implicitLoop GetLoop]
    set deletePoints 0
    if {$pointsLoop eq ""} {
      set pointsLoop [vtkPoints New]
      set deletePoints 1
    }
    set splineFilter [$self GetTimeStepObject $t ParametricFunctionSource]
    $splineFilter Update
    #$points ShallowCopy [[$splineFilter GetOutput] GetPoints]
    set pointsSpline [[$splineFilter GetOutput] GetPoints]
    set n [expr {[$pointsSpline GetNumberOfPoints]-1}]
    $pointsLoop SetNumberOfPoints $n
    for {set i 0} {$i < $n} {incr i} {
      $pointsLoop SetPoint $i {*}[$pointsSpline GetPoint $i]
    }
    #puts "[$pointsLoop Print]"
    set ::points $pointsLoop
    # WATCH: this Modified is important, very important, in order to
    # the interior points be recomputed.
    $pointsLoop Modified
    $implicitLoop SetLoop $pointsLoop
    $implicitLoop SetNormal {*}[$PlaneSource GetNormal]
    $implicitLoop AutomaticNormalGenerationOff
    if {$deletePoints} {
      $pointsLoop Delete
    }
    set extractCell [$self GetTimeStepObject $t ExtractCell]
    $extractCell SetImplicitFunction $implicitLoop
    $extractCell Modified
    [$self GetTimeStepObject $t Pathline] Modified
    set TimeStepObject($t,UpdateImplicitLoop) 0 
    #$self DumpCleanContour
  }

  method GetFlowAtTimeStep { t } {
    set result {}
    set l [string length ",$t"]
    set i0 "end-$l"
    foreach {n v} [array get FlowTableData *,$t] {
      set k [string range $n 0 $i0]
      lappend result $k $v
    }
    return $result
  }

  method ComputeFlowTable { } {
    array unset FlowTableData
    set planeSource [vtkPlaneSource New]
    $planeSource SetOrigin {*}[$PlaneSource GetOrigin]
    $planeSource SetPoint1 {*}[$PlaneSource GetPoint1]
    $planeSource SetPoint2 {*}[$PlaneSource GetPoint2]
    $planeSource SetXResolution [$PlaneSource GetXResolution]
    $planeSource SetYResolution [$PlaneSource GetYResolution]
    set probePlane [vtkProbeFilter New]
    $probePlane SetInputConnection [$planeSource GetOutputPort]
    set T [$DataSet GetNumberOfTimeStep]
    for {set t 0} {$t < $T} {incr t} {
      set imgVector [$DataSet GetVelocityImage -timestep $t \
                         -component FLOW -vtk yes]
      $probePlane SetSourceData $imgVector
      $probePlane Update
      set parametricSpline [$self GetTimeStepObject $t ParametricSpline]
      set tableFlow [FlowQuantifyVtkContour [$probePlane GetOutput] \
                         $parametricSpline [$planeSource GetNormal] \
                         $ParametricResolution]
      # fix units & convert to liters
      array set _tableFlow $tableFlow
      foreach f {flow flow_forward flow_backward} {
        set _tableFlow($f) [expr {$_tableFlow($f) * 1.0e-2}]
      }
      if {$t == 0} {
        set accumFlow 0
        set _tableFlow(flow_accum) 0
        set f0 $_tableFlow(flow)
        set t0 [lindex $TimeValues 0]
      } else {
        set f1  $_tableFlow(flow)
        set t1 [lindex $TimeValues $t]
        set accumFlow [expr {$accumFlow + 0.5*($t1-$t0)*($f0+$f1)}]
        set _tableFlow(flow_accum) $accumFlow
        set t0 $t1
        set f0 $f1
      }
      foreach f [array names _tableFlow] {
        set FlowTableData($f,$t) $_tableFlow($f)
      }
    }
    $probePlane Delete
    $planeSource Delete
  }

  method GetVariableTimeSerie { name } {
    set T [$DataSet GetNumberOfTimeStep] 
    if {[info exists FlowTableData($name,0)]} {
      for {set i 0} {$i < $T} {incr i} {
        lappend result [lindex $TimeValues $i] $FlowTableData($name,$i)
      }
    } else {
      for {set i 0} {$i < $T} {incr i} {
        lappend result [lindex $TimeValues $i] 0
      }
    }
    return $result
  }

  method _ResetStreamlineProgressControl { } {
    array set StreamlineProgress {
      value,absolute 0
      value,relative 0
      counter 0
      aborted 0
    }
  }

  method OnPathlineStartEvent { t } {
    #puts "================================"
    #puts "OnPathlineStartEvent t=$t $TimeStepObject($t,NeedReupdatePathline)"
    #puts "================================"
    if {$TimeStepObject($t,NeedReupdatePathline)} {
      set pathline [$self GetTimeStepObject $t Pathline]
      puts "about to abort: $pathline AbortExecuteOn"
      $pathline SetTerminationTime [$pathline GetStartTime]
    }
  }

  method OnPathlineEndEvent { t } {
    #puts "================================"
    #puts "OnPathlineEndEvent t=$t"
    #puts "================================"
    if {$TimeStepObject($t,NeedReupdatePathline)} {
      set pathline [$self GetTimeStepObject $t Pathline]
      puts "Here we should recompute pathline $t"
      # $self RefreshPathline $t
      $self RecomputePathline $t $PathlineResolution
    }
  }

  method OnStreamlineStartEvent { } {
    #puts "$type _OnStartEvent"
    $self _ResetStreamlineProgressControl
    hook call $self <Status> [mc "Start streamlines ..."]
    update idletask
  }

  method OnStreamlineProgressEvent { } {
    set p [$Streamer GetProgress]
    #puts "$type _OnProgressEvent $p"
    if {$p != $StreamlineProgress(value,absolute)} {
      set $StreamlineProgress(value,absolute) $p
      hook call $self <Status> [mc "Streamline progress %g" $p]
    } else {
      if {[incr StreamlineProgress(counter)] >= 5} {
        set StreamlineProgress(aborted) 1
        $Streamer AbortExecuteOn
        hook call $self <Status> [mc "Streamline aborted"]
      }
    }
    update idletask
  }

  method OnStreamlineEndEvent { } {
    #puts "$self _OnEndEvent"
    if {!$StreamlineProgress(aborted)} {
      hook call $self <Status> [mc "Streamline finished"]
    }
    $self _ResetStreamlineProgressControl
    update idletask
  }

  method PathlineModified { t } {
    set pathline [$self GetTimeStepObject $t Pathline]
    set lastMTime [$self GetTimeStepObject $t LastPathlineMTime]
    return [expr {[$pathline GetMTime] > $lastMTime}]
  }

  method GetPathlineResolution { } {
    return $PathlineResolution
  }
  
  method SetPathlineResolution { r } {
    if {[string is integer $r] && $r > 0 } {
      set PathlineResolution $r
      set T [$DataSet GetNumberOfTimeStep]
      for {set t 0} {$t < $T} {incr t} {
        [$self GetTimeStepObject $t Pathline] Modified
      }
    }
  }

  method GetPathlineReinjectEvery { } {
    return $PathlineReinjectEvery
  }


  method SetPathlineReinjectEvery { re } {
    set PathlineReinjectEvery $re
  }

  method GetPathlineTraceLength { } {
    return $PathlineTraceLength
  }


  method SetPathlineTraceLength { re } {
    set PathlineTraceLength $re
  }

  method UpdatePathlines { } {
    $self UpdatePathlineThreshold $CurrentTimeStep
  }

  method RecomputePathline { t r } {
    puts "RecomputePathline: $t"
    set TimeStepObject($t,NeedReupdatePathline) 0
    set pathline [$self GetTimeStepObject $t Pathline]
    set t0 [lindex $TimeValues $t]
    $pathline SetStartTime $t0
    puts "$pathline SetStartTime $t0"
    foreach t1 [lrange $TimeValues [expr {$t+1}] end] {
      set dt [expr {double($t1 - $t0)/double($r)}]
      set n [expr {$r - 1}]
      for {set i 1} {$i <= $n} {incr i} {
        puts "$pathline SetTerminationTime $t0 + $i*$dt = [expr {$t0 + $i*$dt}]"
        $pathline SetTerminationTime [expr {$t0 + $i*$dt}]
        $pathline Update
      }
      puts "$pathline SetTerminationTime $t1"
      $pathline SetTerminationTime $t1
      $pathline Update
      set t0 $t1
    }
    set TimeStepObject($t,NeedReupdatePathline) 1
  }

  # ==============

  method DumpCleanContour { } {
    set extractCell [$self GetTimeStepObject $CurrentTimeStep ExtractCell]
    puts "extractCell($CurrentTimeStep) = $extractCell"
    $extractCell Modified
    set clean [$self GetTimeStepObject $CurrentTimeStep CleanContour]
    $clean Update
    puts "clean($CurrentTimeStep) = $clean"
    puts "[[$clean GetOutput] Print]"
  }

  method DelaunayStreamline { args } {
    array set opts {
      -file /tmp/del3d.vtk
      -tol 0.01
      -alpha 4
    }
    vtkDelaunay3D del
    del SetInputConnection [$Streamer GetOutputPort]
    #del SetTolerance $opts(-tol)
    del SetAlpha $opts(-alpha)
    del BoundingTriangulationOff
    vtkGenericDataObjectWriter writer
    writer SetInputConnection [del GetOutputPort]
    writer SetFileName $opts(-file)
    writer Update
    del Delete
    writer Delete
  }

  # REVIEW: va superlento
  method VoxelizeStreamline { args } {
    array set opts {
      -file /tmp/voxelModel.vtk
    }
    [$Streamer GetOutput] ComputeBounds
    vtkVoxelModeller voxelModel
    voxelModel SetInputConnection [$Streamer GetOutputPort]
    voxelModel SetSampleDimensions 60 60 30
    voxelModel SetModelBounds {*}[[$Streamer GetOutput] GetBounds]
    voxelModel SetScalarTypeToUnsignedChar
    voxelModel SetForegroundValue 255
    voxelModel SetBackgroundValue 0 
    vtkGenericDataObjectWriter writer
    writer SetInputConnection [voxelModel GetOutputPort]
    writer SetFileName $opts(-file)
    writer Update
    writer Delete
    voxelModel Delete
  }

  method BinFromStreamline { args } {
    array set opts {
      -divisions {100 100 50}
      -maxpts 20
      -file /tmp/octreeBoundary.vtk
    }
    array set opts $args
    $Streamer Update
    vtkPointLocator bin
    bin SetDivisions {*}$opts(-divisions)
    bin SetDataSet [$Streamer GetOutput]
    bin AutomaticOff
    bin BuildLocator
    vtkPolyData boundary
    bin GenerateRepresentation 0 boundary
    puts [bin Print]
    #puts [boundary Print]

    vtkGenericDataObjectWriter writer
    writer SetInputData boundary
    writer SetFileName $opts(-file)
    writer Update
    writer Delete
    bin Delete
    boundary Delete
  }

  method OctreeFromStreamline { args } {
    array set opts {
      -maxlevel 8
      -maxpts 50
      -file /tmp/octreeBoundary.vtk
    }
    array set opts $args
    $Streamer Update
    vtkOctreePointLocator octree
    octree SetMaxLevel $opts(-maxlevel)
    octree SetDataSet [$Streamer GetOutput]
    octree SetMaximumPointsPerRegion $opts(-maxpts)
    octree BuildLocator
    vtkPolyData boundary
    if {[info exists opts(-level)]} {
      set level $opts(-level)
    } else {
      set level [octree GetLevel]
    }
    octree GenerateRepresentation $level boundary
    #puts [octree Print]
    #puts [boundary Print]

    vtkGenericDataObjectWriter writer
    writer SetInputData boundary
    writer SetFileName $opts(-file)
    writer Update
    writer Delete
    octree Delete
    boundary Delete
  }
}
