package require snit
package require wcb

namespace import msgcat::*

snit::widgetadaptor StreamlineProperties {

  typevariable IntegratorTypeInfo -array {
    NAMES         {RUNGE_KUTTA2 RUNGE_KUTTA4 RUNGE_KUTTA45}
    RUNGE_KUTTA2  "Runge Kutta 2"
    RUNGE_KUTTA4  "Runge Kutta 4"
    RUNGE_KUTTA45 "Runge Kutta 4-5"
  }
  typevariable DirectionInfo -array {
    NAMES    {FORWARD BACKWARD BOTH}
    FORWARD  "Forward"
    BACKWARD "Backward"
    BOTH     "Both"
  }
  variable IntegratorType RUNGE_KUTTA45
  variable TerminalSpeed 0.1
  variable Direction FORWARD
  variable InitialIntegrationStep 1.0
  variable MinimumIntegrationStep 0.4
  variable MaximumIntegrationStep 3.0
  variable MaximumError 0.01
  variable MaximumPropagation 500
  variable MaximumNumberOfSteps 2000

  variable Widgets -array {}

  delegate option * to hull
  delegate method * to hull

  constructor { args } {
    installhull using PropertyContainer
    $self configurelist $args
    $self buildMainFrame
  }
  
  destructor {
  }

  method GetLabel { } {
    return [mc "Streamline Options"]
  }

  method buildMainFrame { } {
    set f [$self GetMainFrame]
    ttk::label $f.lbTVelocity -text [mc "Terminal Speed"]:
    ttk::entry $f.entTVelocity -textvariable [myvar TerminalSpeed]
    wcb::callback $f.entTVelocity before insert \
        {wcb::checkEntryLen 10} \
        PropertyContainer::checkNumber
    wcb::callback $f.entTVelocity after insert [mymethod EnableApply]
    wcb::callback $f.entTVelocity after delete [mymethod EnableApply]
    ttk::label $f.lbIntegrator -text [mc "Integrator"]:    
    set Widgets(button,Integrator) [ttk::menubutton $f.btnIntegrator \
                                        -text "Runge Kutta 4-5" \
                                        -menu $f.btnIntegrator.m \
                                        -padding 1]
    $self defineMenuIntegrator
    ttk::label $f.lbDirection -text [mc "Direction"]:    
    set Widgets(button,Direction) [ttk::menubutton $f.btnDirection \
                                        -text "Forward" \
                                        -menu $f.btnDirection.m \
                                        -padding 1]
    $self defineMenuDirection
    set Widgets(label,TimeStep) \
        [ttk::label $f.lbInitialTS -text [mc "Initial Time Step"]:]
    set Widgets(entry,TimeStep) \
        [ttk::entry $f.entInitialTS -textvariable [myvar InitialIntegrationStep]]
    wcb::callback $Widgets(entry,TimeStep) before insert \
        {wcb::checkEntryLen 5} \
        PropertyContainer::checkNumber
    wcb::callback $Widgets(entry,TimeStep) after insert [mymethod EnableApply]
    wcb::callback $Widgets(entry,TimeStep) after delete [mymethod EnableApply]
    # Minimum Integration Time-Step
    set Widgets(label,MinimumTimeStep) \
        [ttk::label $f.lbMinimumTS -text [mc "Minimum Time Step"]:]
    set Widgets(entry,MinimumTimeStep) \
        [ttk::entry $f.entMinimumTS -textvariable [myvar MinimumIntegrationStep]]
    wcb::callback $Widgets(entry,MinimumTimeStep) before insert \
        {wcb::checkEntryLen 5} \
        PropertyContainer::checkNumber
    wcb::callback $Widgets(entry,MinimumTimeStep) after insert [mymethod EnableApply]
    wcb::callback $Widgets(entry,MinimumTimeStep) after delete [mymethod EnableApply]
    # Maximum Integration Time-Step
    set Widgets(label,MaximumTimeStep) \
        [ttk::label $f.lbMaximumTS -text [mc "Maximum Time Step"]:]
    set Widgets(entry,MaximumTimeStep) \
        [ttk::entry $f.entMaximumTS -textvariable [myvar MaximumIntegrationStep]]
    wcb::callback $Widgets(entry,MaximumTimeStep) before insert \
        {wcb::checkEntryLen 5} \
        PropertyContainer::checkNumber
    wcb::callback $Widgets(entry,MaximumTimeStep) after insert [mymethod EnableApply]
    wcb::callback $Widgets(entry,MaximumTimeStep) after delete [mymethod EnableApply]
    # Maximum Error
    set Widgets(label,MaximumError) \
        [ttk::label $f.lbMaximumError -text [mc "Maximum Error"]:]
    set Widgets(entry,MaximumError) \
        [ttk::entry $f.entMaximumError -textvariable [myvar MaximumError]]
    wcb::callback $Widgets(entry,MaximumError) before insert \
        {wcb::checkEntryLen 7} \
        PropertyContainer::checkNumber
    wcb::callback $Widgets(entry,MaximumError) after insert [mymethod EnableApply]
    wcb::callback $Widgets(entry,MaximumError) after delete [mymethod EnableApply]
    # Maximum Length
    set Widgets(label,MaximumLength) \
        [ttk::label $f.lbMaximumLength -text [mc "Maximum Length"]:]
    set Widgets(entry,MaximumLength) \
        [ttk::entry $f.entMaximumLength -textvariable [myvar MaximumPropagation]]
    wcb::callback $Widgets(entry,MaximumLength) before insert \
        {wcb::checkEntryLen 5} \
        PropertyContainer::checkNumber
    wcb::callback $Widgets(entry,MaximumLength) after insert [mymethod EnableApply]
    wcb::callback $Widgets(entry,MaximumLength) after delete [mymethod EnableApply]

    # Maximum Number of Steps
    set Widgets(label,MaximumSteps) \
        [ttk::label $f.lbMaximumSteps -text [mc "Maximum Steps"]:]
    set Widgets(entry,MaximumSteps) \
        [ttk::entry $f.entMaximumSteps -textvariable [myvar MaximumNumberOfSteps]]
    wcb::callback $Widgets(entry,MaximumSteps) before insert \
        {wcb::checkEntryForUInt 100000}
    wcb::callback $Widgets(entry,MaximumSteps) after insert [mymethod EnableApply]
    wcb::callback $Widgets(entry,MaximumSteps) after delete [mymethod EnableApply]
    
    # Gridding
    grid $f.lbDirection -row 0 -column 0 -sticky w 
    grid $f.btnDirection -row 0 -column 1 -sticky w
    grid $f.lbTVelocity -row 1 -column 0 -sticky w 
    grid $f.entTVelocity -row 1 -column 1 -sticky w
    grid $f.lbIntegrator -row 2 -column 0 -sticky w 
    grid $f.btnIntegrator -row 2 -column 1 -sticky w
    grid $Widgets(label,TimeStep) -row 3 -column 0 -sticky w 
    grid $Widgets(entry,TimeStep) -row 3 -column 1 -sticky w
    grid $Widgets(label,MinimumTimeStep) -row 4 -column 0 -sticky w 
    grid $Widgets(entry,MinimumTimeStep) -row 4 -column 1 -sticky w
    grid $Widgets(label,MaximumTimeStep) -row 5 -column 0 -sticky w 
    grid $Widgets(entry,MaximumTimeStep) -row 5 -column 1 -sticky w
    grid $Widgets(label,MaximumError) -row 6 -column 0 -sticky w 
    grid $Widgets(entry,MaximumError) -row 6 -column 1 -sticky w
    grid $Widgets(label,MaximumLength) -row 7 -column 0 -sticky w 
    grid $Widgets(entry,MaximumLength) -row 7 -column 1 -sticky w
    grid $Widgets(label,MaximumSteps) -row 8 -column 0 -sticky w 
    grid $Widgets(entry,MaximumSteps) -row 8 -column 1 -sticky w
    grid columnconfigure $f 2 -weight 1
    grid rowconfigure $f 9 -weight 1
    return $f
  }

  method defineMenuIntegrator { } {
    set m [menu $Widgets(button,Integrator).m -tearoff 0]
    foreach t $IntegratorTypeInfo(NAMES) {
      $m add command \
          -label $IntegratorTypeInfo($t) \
          -command [mymethod SetIntegratorType $t]
    }
  }

  method defineMenuDirection { } {
    set m [menu $Widgets(button,Direction).m -tearoff 0]
    foreach d $DirectionInfo(NAMES) {
      $m add command \
          -label $DirectionInfo($d) \
          -command [mymethod SetDirection $d]
    }
  }

  method disableTimeStepEntries { } {
    $Widgets(label,TimeStep) configure -text [mc "Time Step"]:
    foreach n {MinimumTimeStep MaximumTimeStep} {
      $Widgets(label,$n) configure -state disable
      $Widgets(entry,$n) configure -state disable
    }
  }

  method enableTimeStepEntries { } {
    $Widgets(label,TimeStep) configure -text [mc "Initial Time Step"]:
    foreach n {MinimumTimeStep MaximumTimeStep} {
      $Widgets(label,$n) configure -state normal
      $Widgets(entry,$n) configure -state normal
    }
  }

  method SetIntegratorType { t } {
    puts "SetIntegratorType $t"
    if {[lsearch $IntegratorTypeInfo(NAMES) $t] != -1} {
      $Widgets(button,Integrator) configure -text $IntegratorTypeInfo($t)
      set IntegratorType $t
      if {$t eq "RUNGE_KUTTA45"} {
        $self enableTimeStepEntries
      } else {
        $self disableTimeStepEntries
      }
      $self EnableApply
    }
  }

  method CmdSetIntegratorToRungeKutta2 { } {
    $Widgets(button,Integrator) configure -text "Runge Kutta 2"
    $self disableTimeStepEntries
    set IntegratorType RUNGE_KUTTA2
    $self EnableApply
  }

  method CmdSetIntegratorToRungeKutta4 { } {
    $Widgets(button,Integrator) configure -text "Runge Kutta 4"
    $self disableTimeStepEntries
    set IntegratorType RUNGE_KUTTA4
    $self EnableApply
  }

  method CmdSetIntegratorToRungeKutta45 { } {
    $Widgets(button,Integrator) configure -text "Runge Kutta 4-5"
    $self enableTimeStepEntries
    set IntegratorType RUNGE_KUTTA45
    $self EnableApply
  }

  method SetDirection { d } {
    if {[lsearch $DirectionInfo(NAMES) $d] != -1} {
      $Widgets(button,Direction) configure -text [mc $DirectionInfo($d)]
      set Direction $d
      $self EnableApply
    }
  }

  method CmdSetDirectionToForward { } {
    $Widgets(button,Direction) configure -text "Forward"
    set Direction FORWARD
    $self EnableApply
  }

  method CmdSetDirectionToBackward { } {
    $Widgets(button,Direction) configure -text "Backward"
    set Direction BACKWARD
    $self EnableApply
  }

  method CmdSetDirectionToBoth { } {
    $Widgets(button,Direction) configure -text "Both"
    set Direction BOTH
    $self EnableApply
  }

  method ApplyToTarget { target } {
    $target SetStreamlineOption InitialIntegrationStep [$self GetInitialIntegrationStep]
    $target Set
  }

  method GetTargetType { } {
    return "Streamline"
  }
  
  method GetIntegratorType { } {
    return $IntegratorType
  }

  method GetTerminalSpeed { } {
    return $TerminalSpeed
  }

  method SetTerminalSpeed { s } {
    if {[string is double $s]} {
      set TerminalSpeed $s
    }
  }

  method GetDirection { } {
    return $Direction
  }

  method GetInitialIntegrationStep { } {
    return $InitialIntegrationStep
  }

  method SetInitialIntegrationStep { t } {
    set InitialIntegrationStep $t
  }

  method GetMinimumIntegrationStep { } {
    return $MinimumIntegrationStep
  }
  
  method SetMinimumIntegrationStep { t } {
    set MinimumIntegrationStep $t
  }
  
  method GetMaximumIntegrationStep { } {
    return $MaximumIntegrationStep
  }
  
  method SetMaximumIntegrationStep { t } {
    set MaximumIntegrationStep $t
  }
  
  method GetMaximumError { } {
    return $MaximumError
  }

  method SetMaximumError { x } {
    set MaximumError $x
  }

  method GetMaximumPropagation { } {
    return $MaximumPropagation
  }

  method SetMaximumPropagation { x } {
    set MaximumPropagation $x
  }

  method GetMaximumNumberOfSteps { } {
    return $MaximumNumberOfSteps
  }

  method SetMaximumNumberOfSteps { n } {
    set MaximumNumberOfSteps $n
  }

  method CopyPropertiesFrom { obj } {
    set id [$obj GetIntegratorType]
    puts "obj = $obj"
    puts "GetIntegratorType ==> $id"
    puts "GetIntegratorTypeName ==> [lindex $IntegratorTypeInfo(NAMES) $id]"
    $self SetIntegratorType [lindex $IntegratorTypeInfo(NAMES) $id]
    set id [$obj GetIntegrationDirection]
    $self SetDirection [lindex $DirectionInfo(NAMES) $id]
    $self SetTerminalSpeed [$obj GetTerminalSpeed]
    $self SetInitialIntegrationStep [$obj GetInitialIntegrationStep]
    $self SetMinimumIntegrationStep [$obj GetMinimumIntegrationStep]
    $self SetMaximumIntegrationStep [$obj GetMaximumIntegrationStep]
    $self SetMaximumError [$obj GetMaximumError]
    $self SetMaximumPropagation [$obj GetMaximumPropagation]
    $self SetMaximumNumberOfSteps [$obj GetMaximumNumberOfSteps]
  }
  
}
