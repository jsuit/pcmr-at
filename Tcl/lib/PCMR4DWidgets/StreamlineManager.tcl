package require vtk
package require snit
package require msgcat
package require hook

namespace import msgcat::*

snit::widget StreamlineManager {

  typevariable USE_vtkStreamLine 0

  component PlaneWidget      -public PlaneWidget
  component LoResPlane       -public LoResPlane
  component HiResPlane       -public HiResPlane
  component ProbePlane       -public ProbePlane
  component ProbeFilter      -public ProbeFilter
  component VectorNorm       -public VectorNorm
  component SL_VectorNorm    -public SL_VectorNorm
  component ContourMapper    -public ContourMapper
  component ContourActor     -public ContourActor
  component RungeKutta4      -public RungeKutta4
  component Streamer         -public Streamer
  component GeometryCleaner  -public GeometryCleaner
  component Tubes            -public Tubes
  component StreamlineMapper -public StreamlineMapper
  component StreamlineActor  -public StreamlineActor
  component LookupTable      -public LookupTable

  variable WidgetPlaced 0
  variable Widgets
  variable DataSet
  variable VoxelSize
  variable Progress -array {
    value,absolute 0
    value,relative 0
    counter 0
    aborted 0    
  }

  delegate option * to hull
  delegate method * to hull
  
  option -lookuptable -default "" -readonly yes
  
  constructor {args} {
    catch {
    $self configurelist $args
    ttk::label $win.lbLoRes -text [mc "Low Resolution"]:
    set Widgets(label,lores) $win.lbLoRes
    CScale $win.scLoRes -command [mymethod _CmdChangeLowResolution]
    set Widgets(scale,lores) $win.scLoRes
    ttk::label $win.lbHiRes -text [mc "High Resolution"]:
    set Widgets(label,hires) $win.lbHiRes
    CScale $win.scHiRes -command [mymethod _CmdChangeHighResolution]
    set Widgets(scale,hires) $win.scHiRes
    set icn [Icons GetInstance]
    ttk::label $win.lbEnablePlaneWidget -text [mc "Plane widget"]:
    Button $win.btnEnablePlaneWidget -relief link \
        -image [$icn Get "light_bulb_off" -size 16] \
        -command [mymethod _CmdClickVisibility \
                      SetPlaneWidgetVisibility planewidget]
    set Widgets(button,planewidget) $win.btnEnablePlaneWidget
    ttk::label $win.lbEnableContourPlane -text [mc "Contour Plane"]:
    Button $win.btnEnableContourPlane -relief link \
        -image [$icn Get "light_bulb_off" -size 16] \
        -command [mymethod _CmdClickVisibility \
                      SetContourVisibility contourplane]
    set Widgets(button,contourplane) $win.btnEnableContourPlane
    ttk::label $win.lbEnableStreamline -text [mc "Streamline"]:
    Button $win.btnEnableStreamline -relief link \
        -image [$icn Get "light_bulb_off" -size 16] \
        -command [mymethod _CmdClickVisibility \
                      SetStreamlineVisibility streamline]
    set Widgets(button,streamline) $win.btnEnableStreamline

    grid $win.lbLoRes -row 0 -column 0 -sticky e
    grid $win.scLoRes -row 0 -column 1 -sticky w
    grid $win.lbHiRes -row 1 -column 0 -sticky e
    grid $win.scHiRes -row 1 -column 1 -sticky w
    grid $win.lbEnablePlaneWidget -row 2 -column 0 -sticky e
    grid $win.btnEnablePlaneWidget -row 2 -column 1 -sticky w
    grid $win.lbEnableContourPlane -row 3 -column 0 -sticky e
    grid $win.btnEnableContourPlane -row 3 -column 1 -sticky w
    grid $win.lbEnableStreamline -row 4 -column 0 -sticky e
    grid $win.btnEnableStreamline -row 4 -column 1 -sticky w
    grid rowconfigure $win 5 -weight 1
    grid columnconfigure $win 2 -weight 1
    } msg
    puts "AAAAAAAAAAAAA: $msg"
    catch {
      $self _buildVTKObjects
    } msg
    puts "AAAAAAAAAAAAA: $msg"
    puts "Streamline Object is $self"
  }

  destructor {
    $StreamlineActor Delete
    $StreamlineMapper Delete
    $Tubes Delete
    $GeometryCleaner Delete
    $Streamer Delete
    $RungeKutta4 Delete
    $ContourActor Delete
    $ContourMapper Delete
    $VectorNorm Delete
    if {$SL_VectorNorm ne ""} {
      $SL_VectorNorm Delete
    }
    $ProbeFilter Delete
    $ProbePlane Delete
    $HiResPlane Delete
    $LoResPlane Delete
    $PlaneWidget Delete
    if {$LookupTable ne ""} {
      $LookupTable Delete
    }
  }

  method DisableWidgets { } {
    $Widgets(scale,lores) state disabled
    $Widgets(scale,hires) state disabled
    $Widgets(button,planewidget) configure -state disabled
    $Widgets(button,contourplane) configure -state disabled
    $Widgets(button,streamline) configure -state disabled
  }
  
  method EnableWidgets { } {
    $Widgets(scale,lores) state !disabled
    $Widgets(scale,hires) state !disabled
    $Widgets(button,planewidget) configure -state normal
    $Widgets(button,contourplane) configure -state normal
    $Widgets(button,streamline) configure -state normal
  }

  method SetVisibility {v} {
    set icn [Icons GetInstance]
    set commands [list "$PlaneWidget SetEnabled" \
                      "$ContourActor SetVisibility" \
                      "$StreamlineActor SetVisibility"]
    set widgets {planewidget contourplane streamline}
    set img [expr {$v ? [$icn Get "light_bulb_on" -size 16]:
                   [$icn Get "light_bulb_off" -size 16]}]
    foreach c $commands w $widgets {
      {*}$c $v
      $Widgets(button,$w) configure -image $img
    }
  }

  method _CmdClickVisibility { met w } {
    set icn [Icons GetInstance]
    set imgON [$icn Get "light_bulb_on" -size 16]
    set imgOFF [$icn Get "light_bulb_off" -size 16]
    set img [$Widgets(button,$w) cget -image]
    if {$img eq $imgON} {
      set v 0
      set img $imgOFF
    } else {
      set v 1
      set img $imgON
    }
    $Widgets(button,$w) configure -image $img
    $self $met $v    
  }

  method GetVisibility { } {
    return [expr {[$PlaneWidget GetEnabled] || 
                  [$ContourActor GetVisibility] ||
                  [$StreamlineActor GetVisibility]}]
  }

  method SetPlaneWidgetVisibility {v} {
    set previous [$self GetVisibility]
    $PlaneWidget SetEnabled $v
    hook call $self <Render>
    set current [$self GetVisibility]
    if {$current != $previous} {
      hook call $self <Visibility>
    }
  }

  method SetContourVisibility {v} {
    set previous [$self GetVisibility]
    $ContourActor SetVisibility $v
    hook call $self <Render>
    set current [$self GetVisibility]
    if {$current != $previous} {
      hook call $self <Visibility>
    }
  }

  method SetStreamlineVisibility {v} {
    set previous [$self GetVisibility]
    $StreamlineActor SetVisibility $v
    hook call $self <Render>
    set current [$self GetVisibility]
    if {$current != $previous} {
      hook call $self <Visibility>
    }
  }

  method _buildVTKObjects { } {
    # PlaneWidget
    install PlaneWidget using vtkPlaneWidget $win.planeWidget 
    $PlaneWidget NormalToXAxisOn
    $PlaneWidget SetResolution 50
    $PlaneWidget SetRepresentationToOutline
    $PlaneWidget SetHandleSize 0.02

    $PlaneWidget AddObserver EnableEvent [mymethod OnPlaneWidgetEnable]
    $PlaneWidget AddObserver StartInteractionEvent \
        [mymethod OnPlaneWidgetStartInteraction]
    $PlaneWidget AddObserver EndInteractionEvent \
        [mymethod OnPlaneWidgetEndInteraction]
    $PlaneWidget AddObserver InteractionEvent \
        [mymethod OnPlaneWidgetInteraction]

    # Seed Plane Sources
    install LoResPlane using vtkPlaneSource $win.lowresPlane
    $LoResPlane SetResolution 5 5
    install HiResPlane using vtkPlaneSource $win.hiresPlane
    $HiResPlane SetResolution 10 10
    
    # Filter to probe the velocity magnitude along a plane
    install ProbePlane using vtkPolyData $win.probePlane
    $PlaneWidget GetPolyData $ProbePlane
    install ProbeFilter using vtkProbeFilter $win.probeFilter
    if {[VTKUtil::GetVTKMajorVersion] > 5} {
      $ProbeFilter SetInputData $ProbePlane
    } else {
      $ProbeFilter SetInput $ProbePlane
    }
    # TBD. when changing time step
    #$ProbeFilter SetSource $imgVector
    puts "----------------------------------------------------"
    puts "ProbeFilter = $ProbeFilter"
    puts "----------------------------------------------------"
    
    # Velocity magnitude
    install VectorNorm using vtkVectorNorm $win.vectorNorm
    $VectorNorm NormalizeOff
    $VectorNorm SetInputConnection [$ProbeFilter GetOutputPort]

    # Mapper for velocity magnitude contour
    install ContourMapper using vtkPolyDataMapper $win.contourMapper
    $ContourMapper SetInputConnection [$VectorNorm GetOutputPort]
    $ContourMapper UseLookupTableScalarRangeOn
    if {$options(-lookuptable) ne ""} {
      $ContourMapper SetLookupTable $options(-lookuptable)
    }

    # Actor for velocity magnitude contour
    install ContourActor using vtkActor $win.contourActor
    $ContourActor SetMapper $ContourMapper
    $ContourActor VisibilityOff

    # StreamLine
    if {$USE_vtkStreamLine} {
      set streamerClass vtkStreamLine
      install RungeKutta4 using vtkRungeKutta4 $win.rk4
    } else {
      set streamerClass vtkStreamTracer
      install RungeKutta4 using vtkRungeKutta45 $win.rk4
    }
    install Streamer using $streamerClass $win.streamer
    if {$USE_vtkStreamLine} {
      $Streamer SetMaximumPropagationTime 10
      $Streamer SetIntegrationStepLength 0.05
      $Streamer SetStepLength 0.05
      $Streamer SetNumberOfThreads 2
      $Streamer SetVorticity 0
      $Streamer SpeedScalarsOn
    } else {
      $Streamer SetMaximumPropagation 500
      $Streamer SetIntegrationStepUnit 2
      $Streamer SetInitialIntegrationStep 1.0
      $Streamer SetMinimumIntegrationStep 0.4
      $Streamer SetMaximumIntegrationStep 3.0
      $Streamer SetComputeVorticity 0
    }
    $Streamer SetIntegrationDirectionToForward

    $Streamer SetIntegrator $RungeKutta4
    $Streamer AddObserver StartEvent [mymethod _OnStartEvent]
    $Streamer AddObserver ProgressEvent [mymethod _OnProgressEvent]
    $Streamer AddObserver EndEvent [mymethod _OnEndEvent]

    install GeometryCleaner using vtkCleanPolyData $win.cleanFilter
    $GeometryCleaner SetInputConnection [$Streamer GetOutputPort]
    $GeometryCleaner PointMergingOn

    install Tubes using vtkTubeFilter $win.tubes
    if {$USE_vtkStreamLine} {
      $GeometryCleaner SetTolerance 0.001
      # vtkStreamLine generate a SpeedScalar array
      $Tubes SetInputConnection [$GeometryCleaner GetOutputPort]
    } else {
      $GeometryCleaner SetTolerance 0.001
      # Velocity magnitude
      install SL_VectorNorm using vtkVectorNorm $win.sl_vectorNorm
      $SL_VectorNorm NormalizeOff
      $SL_VectorNorm SetInputConnection [$GeometryCleaner GetOutputPort]
      $Tubes SetInputConnection [$SL_VectorNorm GetOutputPort]
    }
    $Tubes SetRadius 0.2
    $Tubes SetNumberOfSides 4

    install StreamlineMapper using vtkPolyDataMapper $win.streamlineMapper
    $StreamlineMapper SetInputConnection [$Tubes GetOutputPort]
    $StreamlineMapper UseLookupTableScalarRangeOn
    if {$options(-lookuptable) ne ""} {
      $StreamlineMapper SetLookupTable $options(-lookuptable)
    }

    install StreamlineActor using vtkActor $win.streamlineActor
    $StreamlineActor SetMapper $StreamlineMapper
    $StreamlineActor VisibilityOff
  }

  method GetLabel { } {
    return "Streamline"
  }

  method AddToRenderer { renderer } {
    $renderer AddActor $StreamlineActor
    $renderer AddActor $ContourActor
    $PlaneWidget SetInteractor [$renderer GetInteractor]
  }

  method SetDataSet { dataSet } {
    if {$options(-lookuptable) eq ""} {
      install LookupTable using vtkLookupTable $win.lookupTable
      $LookupTable SetHueRange 0.667 0.0
      $LookupTable SetVectorModeToMagnitude
      $ContourMapper SetLookupTable $LookupTable
      $StreamlineMapper SetLookupTable $LookupTable
    }
    set DataSet $dataSet
    $dataSet AddObserver AfterOpenEvent [mymethod OnOpenDataSet]
    $dataSet AddObserver ChangeTimeStep [mymethod OnChangeTimeStep %T]
    $dataSet AddObserver BeforeCloseEvent [mymethod OnCloseDataSet]
  }

  method OnOpenDataSet { } {
    if {$LookupTable ne ""} {
      $LookupTable SetRange 0 [$DataSet GetVelocityEncoding]
      $LookupTable Build
    }
    set min -1
    foreach s [$DataSet GetVoxelSpacing] {
      if {$min == -1 || $s < $min} {
        set min $s
      }
    }
    set VoxelSize [expr {(2.0*$min)/3.0}]
    $Widgets(scale,lores) configure \
        -from [expr {$VoxelSize*15}] \
        -to [expr {$VoxelSize*8}] -value [expr {$VoxelSize*10}]
    $Widgets(scale,hires) configure \
        -from [expr {$VoxelSize*8}] \
        -to $VoxelSize -value [expr {4*$VoxelSize}]
    $self EnableWidgets
  }

  method OnCloseDataSet { } {
    $self SetVisibility 0
    if {[VTKUtil::GetVTKMajorVersion] > 5} {
      $PlaneWidget SetInputData ""
    } else {
      $PlaneWidget SetInput ""
    }
    if {[VTKUtil::GetVTKMajorVersion] > 5} {
      $ProbeFilter SetSourceData ""
      $Streamer SetInputData ""
    } else {
      $ProbeFilter SetSource ""
      $Streamer SetInput ""
    }
    set WidgetPlaced 0
    $self DisableWidgets
  }

  method OnChangeTimeStep { t } {
    #maskPoint SetInput [ StudyHelper::GetVectorImage -masked no ]
    set imgVector [$DataSet GetVelocityImage -component FLOW -vtk yes]
    if {[VTKUtil::GetVTKMajorVersion] > 5} {
      $PlaneWidget SetInputData $imgVector
    } else {
      $PlaneWidget SetInput $imgVector
    }
    if {!$WidgetPlaced} {
      $PlaneWidget PlaceWidget
      set WidgetPlaced 1
    }
    if {[VTKUtil::GetVTKMajorVersion] > 5} {
      $ProbeFilter SetSourceData $imgVector
      $Streamer SetInputData $imgVector 
    } else {
      $ProbeFilter SetSource $imgVector
      $Streamer SetInput $imgVector
    }
    hook call $self <Render>
  }

  method FixPlaneWidgetPlacement { } {
    puts "FixPlaneWidgetPlacement"
    foreach m {Origin Point1 Point2 Normal Center} {
      $PlaneWidget SetOrigin {*}[$LoResPlane Get$m]
    }
  }

  method UpdatePlaneSourceFromPlaneWidget { } {
    foreach m {Origin Point1 Point2 Normal Center} {
      set planeData($m) [$PlaneWidget Get$m]
      if {[llength $planeData($m)] != 3} {
        $self FixPlaneWidgetPlacement
        return
      }
    }
    foreach plane [list $LoResPlane $HiResPlane] {
      $plane SetOrigin {*}[$PlaneWidget GetOrigin]
      $plane SetPoint1 {*}[$PlaneWidget GetPoint1]
      $plane SetPoint2 {*}[$PlaneWidget GetPoint2]
      $plane SetNormal {*}[$PlaneWidget GetNormal]
      $plane SetCenter {*}[$PlaneWidget GetCenter]
    }
  }

  method OnPlaneWidgetEnable { } {
    $self UpdatePlaneSourceFromPlaneWidget
    $HiResPlane SetResolution {*}[$self GetHighResolution]
    if {[VTKUtil::GetVTKMajorVersion] > 5} {
      $Streamer SetSourceConnection [$HiResPlane GetOutputPort]
      #$Streamer SetSourceData [$HiResPlane GetOutput]
    } else {
      $Streamer SetSource [$HiResPlane GetOutput]
    }
    $PlaneWidget GetPolyData $ProbePlane
  }

  method OnPlaneWidgetStartInteraction { } {
    if {[VTKUtil::GetVTKMajorVersion] > 5} {
      $Streamer SetSourceConnection [$LoResPlane GetOutputPort]
      #$Streamer SetSourceData [$LoResPlane GetOutput]
    } else {
      $Streamer SetSource [$LoResPlane GetOutput]
    }
  }

  method OnPlaneWidgetEndInteraction { } {
    $HiResPlane SetResolution {*}[$self GetHighResolution]
     if {[VTKUtil::GetVTKMajorVersion] > 5} {
      $Streamer SetSourceConnection [$HiResPlane GetOutputPort]
      #$Streamer SetSourceData [$HiResPlane GetOutput]
    } else {
      $Streamer SetSource [$HiResPlane GetOutput]
    }
  }

  method OnPlaneWidgetInteraction { } {
    $self UpdatePlaneSourceFromPlaneWidget
    $LoResPlane SetResolution {*}[$self GetLowResolution]
    if {[VTKUtil::GetVTKMajorVersion] <= 5} {
      $Streamer Update
    }
    $PlaneWidget GetPolyData $ProbePlane
    if {[VTKUtil::GetVTKMajorVersion] <= 5} {
      [$ProbeFilter GetOutput] Update
    }
  }

  method GetPlaneResolution {plane s} {
    set origin  [$plane GetOrigin]
    set point1 [$plane GetPoint1]
    set point2 [$plane GetPoint2]
    set s1 0
    set s2 0
    foreach co $origin c1 $point1 c2 $point2 {
      set d1 [expr {$c1-$co}] 
      set d2 [expr {$c2-$co}]
      set s1 [expr {$s1 + $d1*$d1}]
      set s2 [expr {$s2 + $d2*$d2}]
    }
    set n1 [expr {int(ceil(sqrt($s1)/$s))}]
    set n2 [expr {int(ceil(sqrt($s2)/$s))}]
    return [list $n1 $n2]
  }

  method GetHighResolution { } {
    set v [$Widgets(scale,hires) get]
    return [$self GetPlaneResolution $HiResPlane $v]
  }

  method GetLowResolution { } {
    set v [$Widgets(scale,lores) get]
    return [$self GetPlaneResolution $LoResPlane $v]
  }

  method _CmdChangeHighResolution { v } {
    $HiResPlane SetResolution {*}[$self GetPlaneResolution $HiResPlane $v]
    hook call $self <Render>
  }

  method _CmdChangeLowResolution { v } {
    $LoResPlane SetResolution {*}[$self GetPlaneResolution $LoResPlane $v]
  }

  method _ResetProgressControl { } {
    array set Progress {
      value,absolute 0
      value,relative 0
      counter 0
      aborted 0
    }
  }

  method _OnStartEvent { } {
      #puts "$type _OnStartEvent"
    $self _ResetProgressControl
    hook call $self <Status> [mc "Start streamlines ..."]
    update idletask
  }

  method _OnProgressEvent { } {
    set p [$Streamer GetProgress]
     #puts "$type _OnProgressEvent $p"
    if {$p != $Progress(value,absolute)} {
      set $Progress(value,absolute) $p
      hook call $self <Status> [mc "Streamline progress %g" $p]
    } else {
      if {[incr Progress(counter)] >= 5} {
        set Progress(aborted) 1
        $Streamer AbortExecuteOn
        hook call $self <Status> [mc "Streamline aborted"]
      }
    }
    update idletask
  }

  method _OnEndEvent { } {
    #puts "$self _OnEndEvent"
    if {!$Progress(aborted)} {
      hook call $self <Status> [mc "Streamline finished"]
    }
    $self _ResetProgressControl
    update idletask
  }
}

if {0} {
set str .mainPCMR4D.pnw.fNotebook.nb.vispw.frameContent.visStreamlineManager
$str GeometryCleaner GetOutput
vtkPolyDataWriter pwriter
pwriter SetInputConnection [$str GeometryCleaner GetOutputPort]
pwriter SetFileName /tmp/str.vtk
pwriter SetFileTypeToASCII
pwriter Update
}