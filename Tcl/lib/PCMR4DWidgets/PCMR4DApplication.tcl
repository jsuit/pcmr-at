package require Ttk
package require snit
package require TKUtil
package require BWidget
package require wtree
package require menubar
package require Icons
package require tooltip
package require fileutil
package require hook

if {$::tcl_platform(platform) eq "unix"} {
  package require fsdialog
}

snit::widgetadaptor PCMR4DApplication {
  component DataSet -public DataSet
  component 4DImageViewer -public 4DImageViewer

  variable PanedWindow
  variable Notebook
  variable StatusBar
  variable ToolBar
  variable MenuBar
  variable TreeVisualizers
  variable FrameVisualizer
  variable FrameFlowQuantify
  variable FlowQuantifyManager
  variable IdRenderPending ""
  variable FlagRedraw 0

  variable PropertyContainers {}

  variable VisualizerItemInfo -array {
  }

  typevariable ImportScript {
    package require Pcmr4d

    ImporterApplication::Run
  }

  typevariable CFDImportScript {
    package require Pcmr4d

    CFDImporterApplication::Run
  }

  typevariable Singleton ""

  typeconstructor {
  }

  typemethod GetInstance { } {
    if {$Singleton eq ""} {
      set Singleton [$type .mainPCMR4D]
    }
    return $Singleton
  }

  delegate method Render to 4DImageViewer
  delegate method * to hull
  delegate option * to hull

  proc cmdOpenDataSet { self args } {
    set path [tk_chooseDirectory -parent [$self GetWindow] \
                  -title "Select PCMR 4D Study" -mustexist true]
    if {$path ne ""} {
      $self OpenDataSet $path
    }
  }

  proc cmdCloseDataSet { self args } {
    $self CloseDataSet
  }

  proc cmdExit { self args } {
    $self _AtExit
  }

  constructor { args } {
    if { $Singleton ne "" } {
      error "The unique instance for this class could only be accessed by calling GetInstance"
    }
    installhull using toplevel
    #$self _CreateMainMenu
    $self _CreateToolBar
    set DataSet [PCMR4DDataSet %AUTO%]
    set PanedWindow [ttk::panedwindow $win.pnw -orient horizontal]
    set fNotebook [frame $PanedWindow.fNotebook -bd 1 -relief raised]
    set 4DImageViewer [PCMR4DImageViewer $PanedWindow.viewer]
    $4DImageViewer ImageRender configure -bd 1 -relief raised
    $4DImageViewer TimeSlicer configure -bd 1 -relief raised
    $self _CreateNotebook $fNotebook
    $PanedWindow add $fNotebook
    $PanedWindow add $4DImageViewer
    $self _CreateStatusBar
    grid $ToolBar(container) -row 0 -column 0 -sticky ew
    grid $PanedWindow -row 1 -column 0 -sticky snew
    grid $StatusBar(container) -row 2 -column 0 -sticky snew
    grid rowconfigure $win 1 -weight 1
    grid columnconfigure $win 0 -weight 1
    wm protocol $win WM_DELETE_WINDOW [mymethod _AtExit]
    puts "HE CONSTRUIIIIDOOOO .............................................."
  }

  destructor {
    hook forget $self
    if {[winfo exists $4DImageViewer]} {
      $4DImageViewer configure -dataset ""
    }
    $DataSet Close
  }

  method _RenderPending {} {
    if {$FlagRedraw} {
      set FlagRedraw 0
      $self Render
    }
    set IdRenderPending ""
  }

  method GetWindow { } {
    return $win
  }

  method _AtExit { args } {
    puts $args
    exit
  }

  method OpenDataSet { path } {
    #$4DImageViewer configure -dataset ""
    $self CloseDataSet
    $DataSet Open $path
    $4DImageViewer configure -dataset $DataSet
    $self _FillPageInfo
    #$self RestoreVisibility
  }

  method cmdOpenDataSet { } {
    if {$::tcl_platform(platform) eq "unix"} {
      set cmdChooseDirectory ttk::chooseDirectory
    } else {
      set cmdChooseDirectory tk_chooseDirectory
    }
    set path [$cmdChooseDirectory -parent $win \
                  -title "Select PCMR 4D Study" -mustexist true]
    if {$path ne ""} {
      $self OpenDataSet $path
    }
  }

  method CloseDataSet {} {
    if {[$DataSet HasData]} {
      #$self VisualizersOff
    }
    $4DImageViewer configure -dataset ""
    $Notebook(page,info) item delete 0 end
    $DataSet Close
  }

  method cmdImportSequence { mainScript } {
    set fileName "[fileutil::tempfile pcmr4d_]"
    set fd [open $fileName w]
    puts $fd "set auto_path [list $::auto_path]"
    puts $fd $mainScript
    close $fd
    set main [info nameofexecutable]
    set out [exec $main $fileName &]
    #after idle file delete $fileName
    #puts $out
  }

  method cmdImportDICOMSequence { } {
    $self cmdImportSequence $ImportScript
  }

  method cmdImportCFDSequence { } {
    $self cmdImportSequence $CFDImportScript
  }

  method _CreateNotebook { parent } {
    set Notebook(container) [ttk::notebook $parent.nb]
    $self _CreatePageInfo
    $self _CreateVisualizationInfo
    grid $parent.nb -row 0 -column 0 -sticky snew
    grid rowconfigure $parent 0 -weight 1
    grid columnconfigure $parent 0 -weight 1
  }

  method _CreatePageInfo { } {
    set nb $Notebook(container)
    set t [wtree $nb.treeInfo -font TkTextFont -table 1 -filter yes -buttonstyle mac \
               -columns [list \
                             {text -tags ID -label "Information"} \
                             {text -tags DESC -label "Value"}]]
    $t column configure all -gridleftcolor gray70 -borderwidth 1 -font TkTextFont
    $nb add $t -text "Study Info"
    set Notebook(page,info) $t
  }

  method OnActivateItem { } {
    if {![$DataSet HasData]} {
      return
    }
    set item [$TreeVisualizers index active]
    set obj $VisualizerItemInfo(object,$item)
    set slave [grid slave $FrameVisualizer]
    if {$slave ne $obj} {
      if {$slave ne ""} {
        grid remove $slave
      }
      grid $obj -row 0 -column 0 -sticky snew
    }
  }

  # REVIEW: this method can be removed
  method _OnActivateItemVisualization {T x y} {
    if {![$DataSet HasData]} {
      return
    }
    set it [$T tree identify $x $y]
    if {![llength $it] || [llength $it]==3} {
      return
    }
    array set item $it
    set id $item(item)
    set obj $VisualizerItemInfo(object,$id)
    if {$item(column) == 0} {
      set icn [Icons GetInstance]
      set imgON [$icn Get light_bulb_on -size 16]
      set imgOFF [$icn Get light_bulb_off -size 16]
      set img [$T tree item element cget \
                   $id $item(column) eIMG -image]
      if {$img eq $imgON} {
        set img $imgOFF
        $obj SetVisibility 0
      } elseif {$img eq $imgOFF} {
        set img $imgON
        $obj SetVisibility 1
      }
      $T tree item element configure \
          $id $item(column) eIMG -image $img
      $self Render
    }
    set slave [grid slave $FrameVisualizer]
    if {$slave ne $obj} {
      if {$slave ne ""} {
        grid remove $slave
      }
      grid $obj -row 0 -column 0 -sticky snew
    }
  }

  method _CreateVisualizationInfo { } {
    set nb $Notebook(container)
    set vispwMain [ttk::panedwindow $nb.vispw -orient vertical]
    $nb add $vispwMain -text "Visualization"
    set vispw [ttk::panedwindow $vispwMain.vispw -orient horizontal]
    $vispwMain add $vispw
    set FrameFlowQuantify [frame $vispwMain.frmFQ -bd 1 -relief ridge]
    $vispwMain add $FrameFlowQuantify
    set t [wtree $vispw.treeInfo -font TkTextFont -table no -filter no \
               -showheader no -showbuttons no \
               -columns [list {text -tags ACTOR -label "Actor"}]]
    set TreeVisualizers $t
    $t configure -treecolumn ACTOR
    #set sty [$t column cget STATE -itemstyle]
    #$t style layout $sty eRECT -visible no
    #bind $t <ButtonPress-1> [mymethod _OnActivateItemVisualization %W %x %y]
    $t notify bind $self <ActiveItem> [mymethod OnActivateItem]
    set icn [Icons GetInstance]
    set imgON [$icn Get light_bulb_on -size 16]
    set imgOFF [$icn Get light_bulb_off -size 16]
    set FrameVisualizer [frame $vispw.frameContent -bd 1 -relief ridge]
    grid rowconfigure $FrameVisualizer 0 -weight 1
    grid columnconfigure $FrameVisualizer 0 -weight 1
    $self _InsertScalarVisualizers
    $self _InsertPropertyContainers
    $self _CreateFlowQuantifyManager
    $t column configure all -itembackground white
    $vispw add $t
    $vispw add $FrameVisualizer
  }

  method _InsertScalarVisualizers {} {
    set VisualizerInfo [list \
                            [list -name SlicerManager -visible 1 \
                                 -args [list -imagerender [list $4DImageViewer ImageRender]]] \
                            [list -name VolumeRenderManager -visible 0] \
                            [list -name SegmentationManager] \
                            [list -name ExportFEMeshManager] ]
    foreach vi $VisualizerInfo {
      array unset viInfo
      array set viInfo {-args "" -visible 1}
      array set viInfo $vi
      set obj [$viInfo(-name) $FrameVisualizer.vis$viInfo(-name) \
                   {*}$viInfo(-args)]
      $obj SetDataSet $DataSet
      $obj AddToRenderer $4DImageViewer
      $obj SetVisibility 0
      set label [$obj GetLabel]
      set id [$TreeVisualizers insert [list [list $label]]]
      lappend VisualizerItemInfo(items) $id
      set VisualizerItemInfo(object,$id) $obj
      set VisualizerItemInfo(object,$viInfo(-name)) $obj
      set VisualizerItemInfo(visibility,$id) $viInfo(-visible)
      #hook bind $obj <Visibility> $self [mymethod ChangeVisibility $obj $id]
      hook bind $obj <Render> $self [mymethod ScheduleRender]
      hook bind $obj <Status> $self [mymethod HookStatus]
      hook bind $obj <RequestStreamPoints> $self [mymethod HookRequestStreamPoints]
      hook bind $obj <RequestLargestLine> $self [mymethod HookRequestLargestLine]
      hook bind $obj <WriteActiveLines> $self [mymethod HookWriteActiveLines]
      hook bind $obj <RequestActivePlanes> $self [mymethod HookRequestActivePlanes]
      hook bind $obj <RequestBoundaryMesh> $self [mymethod HookRequestBoundaryMesh]
    }

    if {0} {
      set ::LT [ScalarBarManager $FrameVisualizer.lt]
      $::LT SetDataSet $DataSet
      $::LT AddToRenderer $4DImageViewer
      $::LT SetVisibility 1
    }
  }

  method _InsertPropertyContainers {} {
    foreach t {RangeProperties StreamlineProperties PathlineProperties} {
      set obj [$t $FrameVisualizer.props$t]
      set label [$obj GetLabel]
      set id [$TreeVisualizers insert [list [list $label]]]
      lappend VisualizerItemInfo(items) $id
      set VisualizerItemInfo(object,$id) $obj
      hook bind $obj <PropertyApply> $self [mymethod HookApply $obj]
      $obj SetDataSet $DataSet
      lappend PropertyContainers $obj
    }
  }

  method _CreateFlowQuantifyManager { } {
    #set objLUT $VisualizerItemInfo(class,ScalarBarManager)
    #set lut [$objLUT GetLookupTable]
    set FlowQuantifyManager [FlowQuantifyManager2 $FrameFlowQuantify.visFlowQuantifyManager]
    $FlowQuantifyManager SetVolumeOrienter [list $4DImageViewer VolumeOrienter]
    $FlowQuantifyManager SetDataSet $DataSet
    $FlowQuantifyManager AddToRenderer $4DImageViewer
    hook bind $FlowQuantifyManager <Render> $self [mymethod ScheduleRender]
    hook bind $FlowQuantifyManager <Status> $self [mymethod HookStatus]
    hook bind $FlowQuantifyManager <ChangePlane> $self [mymethod HookChangeActivePlane]
    grid $FlowQuantifyManager -row 0 -column 0 -sticky snew
    grid rowconfigure $FrameFlowQuantify 0 -weight 1
    grid columnconfigure $FrameFlowQuantify 0 -weight 1
  }

  method ChangeVisibility {obj id} {
    set v [$obj GetVisibility]
    set icn [Icons GetInstance]
    if {$v} {
      set img [$icn Get light_bulb_on -size 16]
    } else {
      set img [$icn Get light_bulb_off -size 16]
    }
    $TreeVisualizers item element configure $id 0 eIMG -image $img
  }

  # This method applies options changes to FlowQuantifyManager
  method HookApply { obj args } {
    $FlowQuantifyManager ApplyProperties $obj
  }

  method HookChangeActivePlane { } {
    set plane [$FlowQuantifyManager GetActivePlane]
    foreach pc $PropertyContainers {
      $pc CopyPropertiesFrom $plane
    }
  }

  method ScheduleRender { } {
    set FlagRedraw 1
    if {[catch {after info $IdRenderPending}]} {
      set IdRenderPending [after idle [mymethod _RenderPending]]
    }
  }

  method HookCopyPlane { args } {
    array set opts $args
    set objFrom $VisualizerItemInfo(class,$opts(-from))
    set planeInfo [$objFrom GetActivePlaneInfo]
    set objTo $opts(-to)
    $objTo SetActivePlaneInfo $planeInfo
  }

  method HookCopySpline { args } {
    array set opts $args
    set objFrom $VisualizerItemInfo(class,$opts(-from))
    set splineInfo [$objFrom GetActiveSplineInfo]
    set objTo $opts(-to)
    $objTo SetActiveSplineInfo $splineInfo
  }

  method HookWriteActiveLines { args } {
    puts "HookWriteActiveLines $args"
    array set _args $args
    $FlowQuantifyManager WriteActivetLines
  }

  method HookRequestLargestLine { args } {
    puts "HookRequestLargestLine $args"
    array set _args $args
    if {![info exist _args(-filter)]} {
      error "HookRequestLargestLine: missing argument -filter"
    }
    $FlowQuantifyManager GetLargestLine $_args(-filter)
  }

  method HookRequestStreamPoints { args } {
    puts "HookRequestStreamPoints $args"
    array set _args $args
    if {![info exist _args(-appendpoints)]} {
      error "HookRequestStreamPoints: missing argument -appendpoints"
    }
    $FlowQuantifyManager AppendStreamlinePoints $_args(-appendpoints)
  }

  method HookRequestActivePlanes { args } {
    puts "HookRequestActivePlanes $args"
    array set _args $args
    if {![info exist _args(-collection)]} {
      error "HookRequestActivePlanes: missing argument -collection"
    }
    $FlowQuantifyManager AppendActivePlanes $_args(-collection)
  }

  method HookRequestBoundaryMesh { args } {
    puts "HookRequestBoundaryMesh $args"
    array set _args $args
    if {![info exist _args(-setmethod)]} {
      error "HookRequestBoundaryMesh: missing argument -setmethod"
    }
    {*}$_args(-setmethod) [$VisualizerItemInfo(object,SegmentationManager) GetBoundaryMesh]
  }
  
  method HookStatus { args } {
    $StatusBar(label,message) configure -text [lindex $args 0]
    # REVIEW: the following update generate a bug inside VTK, may be
    # an after idle will solve it
    #
    #update
  }

  method _FillPageInfo { } {
    set t $Notebook(page,info)
    set typeDataSet [$DataSet info type]
    $t item delete 0 end
    foreach entry [$typeDataSet GetDictionary] {
      foreach {label met} $entry break
      if {$met eq ""} {
        set value "Unknown"
      } else {
        set value [$DataSet $met]
      }
      $t insert [list [list $label] [list "-- $value"]]
    }
    foreach {key info} [$DataSet GetExtendedDictionary] {
      array set property $info
      $t insert [list [list $property(-description)] [list "-- $property(-value)"]]
    }
  }

  method _CreateStatusBar { } {
    set w [frame $win.statusBar -relief raised -bd 1]
    ttk::label $w.message -anchor w
    set StatusBar(label,message) $w.message
    ttk::progressbar $w.progress -variable [myvar StatusBar(progress,variable)]
    grid $w.message -row 0 -column 0 -sticky snew
    grid $w.progress -row 0 -column 1 -sticky snew
    grid columnconfigure $w 0 -weight 1
    set StatusBar(container) $w
  }

  method _SetStatusMessage { msg } {
    set lb $StatusBar(container).message
    $lb configure -text $msg
  }

  method _CreateToolBar { } {
    set f [frame $win.toolBar -bd 1 -relief raised]
    set ToolBar(container) $f
    set icn [Icons GetInstance]
    $self _AddToolBox File
    $self _AddTool File ttk::button \
        -compound top -text "Import DICOM" -tooltip "Import DICOM Sequence" \
        -image [$icn Get DICOM -size 32] -width -6 \
        -command [mymethod cmdImportDICOMSequence]
    $self _AddTool File ttk::button \
        -compound top -text "Import CFD" -tooltip "Import CFD Sequence" \
        -image [$icn Get bici_cfd -size 32] -width -6 \
        -command [mymethod cmdImportCFDSequence]
    $self _AddTool File ttk::button \
        -compound top -text "Open" -tooltip "Open Study" \
        -image [$icn Get folder_open -size 32] -width -6 \
        -command [mymethod cmdOpenDataSet]
    $self _AddTool File ttk::button \
        -compound top -text "Close" -tooltip "Close Study" \
        -image [$icn Get folder_close -size 32] -width -6 \
        -command [mymethod CloseDataSet]
    $self _AddTool File ttk::button \
        -compound top -text "Exit" -tooltip "Exit Application" \
        -image [$icn Get door_out -size 32] -width -6 \
        -command [mymethod _AtExit]
  }

  method _AddToolBox { toolBoxName args } {
    set f [frame $ToolBar(container).tool${toolBoxName}]
    set ToolBar(toolbox,$toolBoxName) $f
    foreach {ncol ncol} [grid size $ToolBar(container)] break
    grid columnconfigure $ToolBar(container) $ncol -weight 0
    grid $f -row 0 -column $ncol -sticky wns
    incr ncol
    grid columnconfigure $ToolBar(container) $ncol -weight 1
  }

  method _AddTool { toolBoxName wtype args } {
    set f $ToolBar(toolbox,$toolBoxName)
    foreach {ncol nrow} [grid size $f] break
    set id [expr {$ncol+1}]
    set tip [from args -tooltip ""]
    set t [$wtype $f.tool$id {*}$args]
    bind $t <Motion> [mymethod _SetStatusMessage $tip]
    grid $t -row 0 -column $ncol -sticky e
    # add the tooltip
    if {$tip ne ""} {
      tooltip::tooltip $t $tip
    }
  }

  method _CreateMainMenu { } {
    set mbar [menubar new]
    $mbar define {
      File M:file {
        #   Label                Type   Tag Name(s)     
        #   -----------------    ----   ---------
        Open                      C      t:open
        Close                     C      t:close
        --                        S      s0
        Exit                      C      t:exit
      }
    }
    ${mbar} install $win {
      ${mbar} menu.configure -command [string map "%s $self" {
        t:open {cmdOpenDataSet %s}
        t:close {cmdCloseDataSet %s}
        t:exit {cmdExit %s}
      }]
    }
    set MenuBar $mbar
  }
}
