package require vtk
package require snit
package require Img
package require ITKUtil
package require HeaderXml

snit::type PCMR4DDataSet {
  variable DataSetCache
  variable ThumbnailCache
  variable HeaderInfo
  variable BaseDir
  variable CurrentTimeStep -1
  variable Events -array {
    AfterOpenEvent,enabled   1
    BeforeCloseEvent,enabled 1
    AfterCloseEvent,enabled  1
    ChangeTimeStep,enabled   1
  }

  variable Observers -array {
  }

  typevariable ITKTCL_Version

  typeconstructor {
    set ITKTCL_Version [ITKUtil::LoadITKTcl]
  }

  typevariable Dictionary {
    {"Format Version"       GetFormatVersion}
    {"Source Sequence"      GetSourceSequence}
    {"Velocity Encoding"    GetVelocityEncoding}
    {"Number of Time Steps" GetNumberOfTimeStep}
    {"Length of Time Step " GetLengthOfTimeStep}
    {"Image Orientation"    GetImageOrientation}
    {"Video Frame Rate"     GetNeededFrameRate}
    {"Distance Units"       GetDistanceUnit}
    {"Slice Thickness"      GetSliceThickness}
    {"Image Resolution"     GetImageResolutionAsString}
    {"Voxel Spacing"        GetVoxelSpacingAsString}
    {"Physical Origin"      GetPhysicalOriginAsString}
    {"Physical Dimensions"  GetPhysicalDimensionsAsString}
    {"Study Directory"      GetDirectory}
  }

  typemethod GetDictionary { } {
    return $Dictionary
  }

  # no yes no
  typevariable ShouldInvertComponent -array {
    0 no
    1 yes
    2 no
  }

  typevariable ComponentNames {MAG VX VY VZ |V|}

  typemethod GetComponentNames { } {
    return $ComponentNames
  }
  
  typemethod CheckComponentName { name } {
    if {[lsearch $ComponentNames $name] == -1} {
      set msg "[join [lrange $ComponentNames 0 end-1] ,] or [lindex $ComponentNames end]"
      error "Invalid component name '$name', must be $msg"
    }
  }

  typemethod GetTmpDir {} {
    global env tcl_platform

    if {$tcl_platform(platform) eq "unix"} {
      set TmpDir /tmp
    } else {
      if {[info exist env(TEMP)] && [file exists $env(TEMP)]} {
        set TmpDir $env(TEMP)
      } elseif {[info exist env(TMP)] && [file exists $env(TMP)]} {
        set TmpDir $env(TMP)
      } else {
        set TmpDir "C:/"
      }
    }
    return $TmpDir
  }

  typemethod WriteImage { fileName image args } {
    array set options {
      -writer "VTKScalarImageWriter"
      -msg ""
    }
    array set options $args
    set outputName [file join [$type GetTmpDir] $fileName]
    if { $options(-writer) eq "VTKScalarImageWriter" } {
      set writer [VTKScalarImageWriter]
      $writer SetInput $image
    } else {
      set writer [$options(-writer) New]
      $writer SetInputData $image
    }
    $writer SetFileName $outputName
    if {$options(-msg) eq ""} {
      set msg "Writing imagen to $outputName ..."
    } else {
      set msg [string map [list %O $outputName] $options(-msg)]
    }
    puts $msg
    $writer Update
    if { $options(-writer) eq "VTKScalarImageWriter" } {
      ITKUtil::DeleteCmd $writer
    } else {
      $writer Delete
    }
  }

  typemethod ReadImageFromFile { filename args } {
    set opts(-invert) 0
    array set opts $args
    set reader [Image3DReader]
    $reader SetFileName $filename
    $reader Update
    if {$opts(-invert)} {
      set invert [ShiftScaleFilter]
      $invert SetShift 0
      $invert SetScale -1
      # this [$reader GetOutput] create a tcl command which set a
      # reference on the image object associated to the output of the
      # reader, how to know when this command it is orphaned?
      #puts "OJO: [$reader GetOutput] ([[$reader GetOutput] GetReferenceCount])"
      #$invert SetInput [$reader GetOutput]
      $invert SetInputFrom $reader
      set img [$invert GetOutput]
    } else {
      set img [$reader GetOutput]
    }
    $img Update
    $img DisconnectPipeline
    if {$opts(-invert)} {
      ITKUtil::DeleteCmd $invert
      #DestroyITKCmd [$reader GetOutput]
    }
    ITKUtil::DeleteCmd $reader
    return $img
  }

  typemethod CheckBaseDirectory { studyDir } {
    if {![file exists $studyDir]} {
      error "The study '$studyDir' does not exists."
    }
    if {[file type $studyDir] eq "link"} {
      set _studyDir [file readlink $studyDir]
      set studyDir $_studyDir
    }
    if {[file type $studyDir] ne "directory"} {
      error "The study requested must be a directory: '$studyDir'"
    }
    if {![file exists [file join $studyDir "header.sth"]]} {
      error "The directory '$studyDir' is not a valid Aorta4D study: does not contains header.sth"
    }
  }

  constructor {} {
    $self _InitEmptyHeader
  }

  destructor {
  }

  method GetDirectory {} {
    return $BaseDir
  }

  method Open { path } {
    $self Close
    # REVIEW: without this test pvtk do a "Segmentation fault"
    if { [file exists $path] && [file isdir $path] } {
      set BaseDir $path
      set CurrentTimeStep 0
      $self ReadHeaderInfoFromDir $BaseDir
      $self NotifyObservers AfterOpenEvent
    }
  }

  method Close {} {
    if {![info exists HeaderInfo] || ![dict size $HeaderInfo]} {
      $self _InitEmptyHeader
    }
    $self NotifyObservers BeforeCloseEvent
    for {set t 0} {$t < [$self GetNumberOfTimeStep]} {incr t} {
      foreach comp [list "PREMASK" "FLOW" {*}[$type GetComponentNames]] {
        if {[info exists DataSetCache($t,$comp)]} {
          $DataSetCache($t,$comp,VTK) Delete
          if {[package vcompare $ITKTCL_Version "2.0"]>=0} {
            # ITKTCL2.0 on: the importer is a ITK filter
            ITKUtil::DeleteCmd $DataSetCache($t,$comp,importer)
          } else {
            # ITKTCL1.0: the importer is a VTK filter
            $DataSetCache($t,$comp,importer) Delete            
          }
          if {$DataSetCache($t,$comp,exporter) ne ""} {
            # ITKTCL2.0 on: the exporter is empty
            ITKUtil::DeleteCmd $DataSetCache($t,$comp,exporter) 
          }
          ITKUtil::DeleteCmd $DataSetCache($t,$comp)
        }
      }
    }
    array unset DataSetCache
    $self _InitEmptyHeader
    $self NotifyObservers AfterCloseEvent    
    set BaseDir ""
  }

  method HasData { } {
    return [expr {[$self GetNumberOfTimeStep]>0}]
  }

  method _InitEmptyHeader { } {
    set CurrentTimeStep -1
    set HeaderInfo {
      NumberOfTimeSteps 0
      LengthOfTimeStep 0
      VelocityEncoding 150.0
      ExtendedProperties {}
      ImageOrientation "ASL"
    }
    return 0
  }

  method CheckTimeStepValue { timestep } {
    set numberTS [$self GetNumberOfTimeStep]
    if { $timestep < 0 || $timestep >= $numberTS || 
         ![string is integer $timestep] } {
      error "invalid timestep value '$timestep', must be in \[0..$numberTS)"
    }
    return 1
  }

  method GetFormatVersion { } {
    return [dict get $HeaderInfo FormatVersion]
  }

  method GetSourceSequence { } {
    return [dict get $HeaderInfo SourceSequence]
  }

  method GetNumberOfTimeStep { } {
    return [dict get $HeaderInfo NumberOfTimeSteps]
  }

  method GetTimeValues { } {
    set timeValues [dict get $HeaderInfo TimeValues]
    if {![llength $timeValues]} {
      set d [$self GetLengthOfTimeStep]
      for {set i 0} {$i < [$self GetNumberOfTimeStep]} {incr i} {
        lappend timeValues [expr {double($i)*$d}]
      }
      dict set HeaderInfo TimeValues $timeValues
    }
    return $timeValues
  }

  method GetVelocityEncoding { } {
    return [dict get $HeaderInfo VelocityEncoding]
  }

  method GetLengthOfTimeStep { } {
    return [dict get $HeaderInfo LengthOfTimeStep]
  }

  method GetInvertComponentX { } {
    return [dict get $HeaderInfo InvertComponentX]
  }
  
  method GetInvertComponentY { } {
    return [dict get $HeaderInfo InvertComponentY]
  }
  
  method GetInvertComponentZ { } {
    return [dict get $HeaderInfo InvertComponentZ]
  }
  
  method GetCurrentTimeStep { } {
    return $CurrentTimeStep
  }

  method GetNextTimeStep { } {
    set ts $CurrentTimeStep
    incr ts
    if { $ts >= [$self GetNumberOfTimeStep] } {
      set ts 0
    }
    return $ts
  }

  method ReaderXmlHeaderInfoFromFile { xmlfile } {
    set header [HeaderXml %AUTO% -xmlfile $xmlfile]
    foreach p [HeaderXml GetGlobalProperties] {
      dict set HeaderInfo $p [$header Get$p]
    }
    foreach p [$header GetExtendedProperties] {
      dict set HeaderInfo ExtendedProperties $p [$header GetExtendedProperty $p]
    }
    # REVIEW: remove it after fixing the task ...
    #dict unset HeaderInfo TimeValues
    $header destroy
  }

  method ReaderSthHeaderInfoFromFile { sthfile } {
    set handle [open $sthfile "r"]
    set HeaderInfo [read -nonewline $handle]
    close $handle
  }

  method PreprocessHeaderProperties { } {
    # FormatVersion
    if {![dict exists $HeaderInfo FormatVersion]} {
      dict set HeaderInfo FormatVersion "0.0"
    }

    # SourceSequence
    if {![dict exists $HeaderInfo SourceSequence]} {
      dict set HeaderInfo SourceSequence ""
    }

    # FormatVersion
    if {![dict exists $HeaderInfo TimeValues]} {
      dict set HeaderInfo TimeValues ""
    }

    # NumberOfTimeSteps
    if {![dict exists $HeaderInfo NumberOfTimeSteps]} {
      if {[dict exists $HeaderInfo time_step_count]} {
        dict set HeaderInfo NumberOfTimeSteps [dict get $HeaderInfo time_step_count]
      } else {
        error "Undefined NumberOfTimeSteps"
      }
    }

    # VelocityEncoding
    if {![dict exists $HeaderInfo VelocityEncoding]} {
      if {[dict exists $HeaderInfo venc]} {
        dict set HeaderInfo VelocityEncoding [dict get $HeaderInfo venc]
      } else {
        dict set HeaderInfo VelocityEncoding 150.0
      }
    }

    # ImageOrientation
    if {![dict exists $HeaderInfo ImageOrientation]} {
      if {[dict exists $HeaderInfo image_orientation]} {
        dict set HeaderInfo ImageOrientation [dict get $HeaderInfo image_orientation]
      } else {
        dict set HeaderInfo ImageOrientation "ASL"
      }
     }

    # LengthOfTimeStep
    if {![dict exists $HeaderInfo LengthOfTimeStep]} {
      if {[dict exists $HeaderInfo length_time_step]} {
        dict set HeaderInfo LengthOfTimeStep [dict get $HeaderInfo length_time_step]
      } else {
        set n [$self GetNumberOfTimeStep]
        # assume the adquisition is 1 second long, the timestep is
        # meassured in miliseconds
        dict set HeaderInfo LengthOfTimeStep [expr {1000.0/$n}]
      }
    }

    # SliceThickness
    if {![dict exists $HeaderInfo SliceThickness]} {
      dict set HeaderInfo SliceThickness ""
    }

    # DistanceUnit
    if {![dict exists $HeaderInfo DistanceUnit]} {
      dict set HeaderInfo DistanceUnit "mm"
    }

    # InvertComponentX
    if {![dict exists $HeaderInfo InvertComponentX]} {
      dict set HeaderInfo InvertComponentX 0
    }

    # InvertComponentY
    if {![dict exists $HeaderInfo InvertComponentY]} {
      dict set HeaderInfo InvertComponentY 1
    }

    # InvertComponentZ
    if {![dict exists $HeaderInfo InvertComponentZ]} {
      dict set HeaderInfo InvertComponentZ 0
    }

    # ExtendedProperties
    if {![dict exists $HeaderInfo ExtendedProperties]} {
      dict set HeaderInfo ExtendedProperties ""
    }
  }

  method ReadHeaderInfoFromDir { dir } {
    set xmlfile [file join $dir "header.xml"]
    if {[file exists $xmlfile]} {
      $self ReaderXmlHeaderInfoFromFile $xmlfile
    } else {
      $self ReaderSthHeaderInfoFromFile [file join $dir "header.sth"]
    }
    $self PreprocessHeaderProperties
  }

  method GetNeededFrameRate { } {
    return [expr {int((1.0/([$self GetLengthOfTimeStep])) * 1000)}]
  }

  method GetImageOrientation { } {
    return [dict get $HeaderInfo ImageOrientation]
  }

  method GetExtendedDictionary { } {
    return [dict get $HeaderInfo ExtendedProperties]
 }

  method GetSliceThickness { } {
    return [dict get $HeaderInfo SliceThickness]
  }

  method GetDistanceUnit { } {
    return [dict get $HeaderInfo DistanceUnit]
  }

  method GetImageResolution { } {
    if {[$self HasData]} {
      set img [$self GetMagnitudeImage]
      return [$img GetSize]
    } else {
      return {}
    }
  }

  method GetImageResolutionAsString { } {
    return [join [$self GetImageResolution] " x "]
  }

  method GetVoxelSpacing { } {
    if {[$self HasData]} {
      set img [$self GetMagnitudeImage]
      return [$img GetSpacing]
    } else {
      return {}
    }
  }

  method GetVoxelSpacingAsString { } {
    set unit [$self GetDistanceUnit]
    if { $unit eq "Unknown"} {
      set unit ""
    }
    return "[join [$self GetVoxelSpacing] ,] ${unit}"
  }

  method GetPhysicalOrigin { } {
    if {[$self HasData]} {
      set img [$self GetMagnitudeImage]
      return [$img GetOrigin]
    } else {
      return {}
    }
  }

  method GetPhysicalOriginAsString { } {
    set unit [$self GetDistanceUnit]
    if { $unit eq "Unknown"} {
      set unit ""
    }
    return "[join [$self GetPhysicalOrigin] ,] ${unit}"
  }

  method GetPhysicalDimensions { } {
    if {[$self HasData]} {
      set img [$self GetMagnitudeImage]
      set phSize {}
      foreach d [$img GetSpacing] n [$img GetSize] {
        lappend phSize [expr {$d*$n}]
      }
      return $phSize
    } else {
      return {}
    }
  }

  method GetPhysicalDimensionsAsString { } {
    set unit [$self GetDistanceUnit]
    if { $unit eq "Unknown"} {
      set unit ""
    }
    return "[join [$self GetPhysicalDimensions] { x }] ${unit}"
  }

  method GetMagnitudeFileName { timestep } {
    $self CheckTimeStepValue $timestep
    return [file join $BaseDir "mag_${timestep}.vtk"]
  }

  method GetVectorFileNames { timestep } {
    $self CheckTimeStepValue $timestep
    array unset fileComponent
    foreach comp {0 1 2} {
      set fname [file join $BaseDir "vct_${timestep}_${comp}.vtk"]
      if {![ file exists $fname]} {
	error "Invalid study, the file '$fname' does not exists"
      }
      set fileComponent($comp) $fname
    }
    return [array get fileComponent]
  }
  
  method _BuildVelocityVector {timestep} {
    if {[package vcompare $ITKTCL_Version "2.0"]>=0} {
      set composerFilter [ComposeImageFilter]
    } else {
      set composerFilter [ComposeFilter]
    }
    $composerFilter SetInput1 $DataSetCache($timestep,VX)
    $composerFilter SetInput2 $DataSetCache($timestep,VY)
    $composerFilter SetInput3 $DataSetCache($timestep,VZ)
    $composerFilter Update
    set img [$composerFilter GetOutput]
    $img Update
    $img DisconnectPipeline
    ITKUtil::DeleteCmd $composerFilter
    set DataSetCache($timestep,FLOW) $img
  }

  method _BuildVelocityMagnitude {timestep} {
    set VelMagFilter [TernaryMagnitudeFilter]
    $VelMagFilter SetInput1 $DataSetCache($timestep,VX)
    $VelMagFilter SetInput2 $DataSetCache($timestep,VY)
    $VelMagFilter SetInput3 $DataSetCache($timestep,VZ)
    $VelMagFilter Update
    set img [$VelMagFilter GetOutput]
    $img DisconnectPipeline
    ITKUtil::DeleteCmd $VelMagFilter
    set DataSetCache($timestep,|V|) $img
  }

  # set toVTK [ImageToVTKImageFilter_Float]
  # set vecToVTK [ImageToVTKImageFilter_Vector]
  # $vecToVTK SetInputFrom $vecImage
  method _BuildVTKImporter {timestep comp} {
    if {$comp eq "FLOW"} {
      if {[package vcompare $ITKTCL_Version "2.0"]>=0} {
        set exporter [ImageToVTKImageFilter_Vector]
      } else {
        set exporter [VTKVectorImageExport]
      }
    } else {
      if {[package vcompare $ITKTCL_Version "2.0"]>=0} {
        set exporter [ImageToVTKImageFilter_Float]
      } else {
        set exporter [VTKScalarImageExport]
      }
    }
    $exporter SetInput $DataSetCache($timestep,$comp)
    if {[package vcompare $ITKTCL_Version "2.0"] < 0} {
      set importer [vtkImageImport New]
      $exporter Connect2VTK $importer
      set DataSetCache($timestep,$comp,exporter) $exporter
    } else {
      set importer $exporter
      set DataSetCache($timestep,$comp,exporter) ""
    }
    set DataSetCache($timestep,$comp,importer) $importer
    $importer Update
    if {$comp eq "FLOW"} {
      # this piece of code copy the scalars to vectors, don't know how
      # to move by just simple renaming
      set imgVector [$importer GetOutput]
      set pd [$imgVector GetPointData]
      set scalars [$pd GetScalars]
      set vectors [vtkFloatArray New]
      $vectors SetName "Velocity"
      $vectors DeepCopy $scalars
      $pd RemoveArray "scalars"
      $pd SetVectors $vectors
      $pd Delete
      #puts "vectors = $vectors [$vectors GetReferenceCount]"
      $vectors Delete
      #puts "vectors = [[[$imgVector GetPointData] GetVectors] GetReferenceCount]"
    }
    set DataSetCache($timestep,$comp,VTK) [$importer GetOutput]
  }

  method LoadPremaskImage { } {
    if {[info exists DataSetCache(0,PREMASK)]} {
      return
    }
    set premaskFile [file join [$self GetDirectory] "premask.vtk"]
    if {![file exists $premaskFile] || ![file readable $premaskFile]} {
      error "could not load premask image: file '$premaskFile' does not exists or is not readable"
    }
    set DataSetCache(0,PREMASK) [$type ReadImageFromFile $premaskFile]
    $self _BuildVTKImporter 0 PREMASK
  }

  method LoadTimeStep { timestep } {
    $self CheckTimeStepValue $timestep
    if {[info exists DataSetCache($timestep,VX)]} {
      # already loaded
      return $timestep
    }
    array set fileComponent [$self GetVectorFileNames $timestep]
    array set idcomp {0 X 1 Y 2 Z} 
    foreach comp {0 1 2} compName {VX VY VZ} {
      set invert [$self GetInvertComponent$idcomp($comp)]
      set DataSetCache($timestep,$compName) \
          [$type ReadImageFromFile $fileComponent($comp) \
               -invert $invert]
      $DataSetCache($timestep,$compName) Update
      $self _BuildVTKImporter $timestep $compName
    }
    set DataSetCache($timestep,MAG) \
        [$type ReadImageFromFile [$self GetMagnitudeFileName $timestep]]
    $self _BuildVTKImporter $timestep MAG
    $self _BuildVelocityMagnitude $timestep
    $self _BuildVTKImporter $timestep |V|
    $self _BuildVelocityVector $timestep
    $self _BuildVTKImporter $timestep FLOW
    return $timestep
  }

  method ChangeTimeStep { timestep } {
    $self CheckTimeStepValue $timestep
    set CurrentTimeStep $timestep
    $self LoadTimeStep $timestep
    $self NotifyObservers ChangeTimeStep
  }

  method GetThumbnailImage { args } {
    set opts(-timestep) [$self GetCurrentTimeStep]
    array set opts $args
    set timestep [$self LoadTimeStep $opts(-timestep)]
    return $ThumbnailCache($opts(-timestep))
  }

  method GetPremaskImage { args } {
    $self LoadPremaskImage
    set opts(-timestep) [$self GetCurrentTimeStep]
    set opts(-vtk) no
    array set opts $args
    set timestep [$self LoadTimeStep $opts(-timestep)]
    if {$opts(-vtk)} {
      return [$DataSetCache(0,PREMASK,importer) GetOutput]
    } else {
      return $DataSetCache(0,PREMASK)
    }
  }

  method GetMagnitudeImage { args } {
    set opts(-timestep) [$self GetCurrentTimeStep]
    set opts(-vtk) no
    array set opts $args
    set timestep [$self LoadTimeStep $opts(-timestep)]
    if {$opts(-vtk)} {
      return [$DataSetCache($timestep,MAG,importer) GetOutput]
    } else {
      return $DataSetCache($timestep,MAG)
    }
  }

  method GetVelocityImage { args } {
    set opts(-timestep) [$self GetCurrentTimeStep]
    set opts(-component) "|V|"
    set opts(-vtk) no
    array set opts $args
    set timestep [$self LoadTimeStep $opts(-timestep)]
    if {$opts(-vtk)} {
      return [$DataSetCache($timestep,$opts(-component),importer) GetOutput]
    } else {
      return $DataSetCache($timestep,$opts(-component))
    }
  }

  method GetComponentImage { args } {
    set opts(-timestep) [$self GetCurrentTimeStep]
    set opts(-component) "|V|"
    set opts(-vtk) no
    array set opts $args
    set c $opts(-component)
    $type CheckComponentName $c
    set timestep [$self LoadTimeStep $opts(-timestep)]
    if {$opts(-vtk)} {
      return [$DataSetCache($timestep,$c,importer) GetOutput]
    } else {
      return $DataSetCache($timestep,$c)
    }
  }

  method GetObservers { event } {
    if {[info exists Observers($event)]} {
      set result $Observers($event)
    } else {
      set result ""
    }
    if {[info exists Observers($event,last)]} {
      lappend result $Observers($event,last)
    }
    return $result
  }

  method InvokeScript { script } {
    set toeval [string map [list %T [$self GetCurrentTimeStep]] $script]
    eval $toeval
  }

  method NotifyObservers { event } {
    if {$Events($event,enabled)} {
      foreach script [$self GetObservers $event] {
        $self InvokeScript $script
      }
    }
  }

  method SetEventEnabled { event s } {
    set v  $Events($event,enabled)
    set Events($event,enabled) $s
    return $v
  }

  method AddObserver { event script args } {
    array set options {
      -last no
    }
    array set options $args
    if { $options(-last) } {
      set Observers($event,last) $script
    } else {
      lappend Observers($event) $script
    }
  }
}

# PCMR4DDataSet mngr
# mngr Open ../../../../../biomedical/data/Studies/4DFLOW_SampleDataset_Aorta_3D_24SL
# for {set t 0} {$t < [mngr GetNumberOfTimeStep]} {incr t} {mngr LoadTimeStep $t}
