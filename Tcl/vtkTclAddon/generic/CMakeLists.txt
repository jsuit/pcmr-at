configure_file (
  "${CMAKE_CURRENT_SOURCE_DIR}/vtkTclAddon_Version.h.in"
  "${CMAKE_CURRENT_BINARY_DIR}/vtkTclAddon_Version.h"
  )

add_compiler_export_flags()
include_directories ("${CMAKE_CURRENT_BINARY_DIR}")

if(CMAKE_COMPILER_IS_GNUCXX)
  add_definitions(-Wall)
endif(CMAKE_COMPILER_IS_GNUCXX)

message( STATUS vtkRenderingVolume${VTK_RENDERING_BACKEND} )

add_library(vtkTclAddon SHARED 
  vtkTclAddon.cxx
  vtkChartAddon.cxx
  vtkSplineRepresentationAddon.cxx)
target_link_libraries (vtkTclAddon 
  ${TCL_STUB_LIBRARY} ${VTK_LIBRARIES} vtkCommonCoreTCL
  #vtkCommonCoreTCL vtkInteractionWidgets vtkRenderingVolume${VTK_RENDERING_BACKEND} 
  )

generate_export_header(vtkTclAddon
  EXPORT_FILE_NAME vtkTclAddon_Export.h)


if(WIN32)
  install( TARGETS vtkTclAddon RUNTIME DESTINATION ${VTKTCLADDON_INSTALL_DIR} )
else(WIN32)
  install( TARGETS vtkTclAddon RUNTIME LIBRARY DESTINATION ${VTKTCLADDON_INSTALL_DIR} )
endif(WIN32)
