#include "vtkChartAddon.h"
#include "vtkTclUtil.h"
#include "vtkAxis.h"

int vtkAxisAddon_Proc(ClientData clientData,
                Tcl_Interp *interp,
                int objc, Tcl_Obj *const objv[])
{
  const char* cmd = Tcl_GetString(objv[0]);

  if (objc < 3)
    {
    Tcl_AppendResult(interp,
                     "vtkAxisAddon ERROR --\n",
                     "wrong # of arguments, should be\n",
                     "    ", cmd, " vtkAxis cmd ?args?",
                     NULL);
    return TCL_ERROR;
    }
  int error = 0;
  const char* argAxis = Tcl_GetString(objv[1]);
  vtkAxis *axis = static_cast<vtkAxis *>(
    vtkTclGetPointerFromObject(argAxis,"vtkAxis",
                               interp, error));
  if (error || !axis)
    {
    Tcl_AppendResult(interp,
                     "vtkAxisAddon ERROR --\n",
                     "could not convert '", argAxis, 
                     "' to vtkAxis", NULL);
    return TCL_ERROR;
    }
  const char* subcmd = Tcl_GetString(objv[2]);
  if (!strcmp(subcmd, "SetTitle"))
    {
    if (objc != 4)
      {
      Tcl_AppendResult(interp,
                       "vtkAxisAddon ERROR --\n",
                       "wrong # of arguments, should be\n",
                       "    ", cmd, " vtkAxis SetTitle title",
                       NULL);
      return TCL_ERROR;
      }
    axis->SetTitle(Tcl_GetString(objv[3]));
    return TCL_OK;
    }
  else
    {
    Tcl_AppendResult(interp,
                     "vtkAxisAddon ERROR --\n",
                     "wrong subcommand, should be\n",
                     "    ", cmd, " vtkAxis SetTitle title",
                     NULL);
    return TCL_ERROR;
    }
}

int vtkChartAddon_Init(Tcl_Interp *interp)
{
  Tcl_CreateObjCommand(interp, "vtkAxisAddon",
                       vtkAxisAddon_Proc, NULL, NULL);
  return TCL_OK;
}
