#include "vtkTclAddon.h"
#include "vtkTclAddon_Version.h"
#include "vtkChartAddon.h"
#include "vtkSplineRepresentationAddon.h"

int Vtktcladdon_Init(Tcl_Interp *interp)
{
  if (Tcl_InitStubs(interp, "8.4", 0) == NULL) {
    return TCL_ERROR;
  }
  if (Tcl_PkgRequire(interp, "Tcl", "8.4", 0) == NULL) {
    return TCL_ERROR;
  }
  
  vtkChartAddon_Init(interp);
  vtkSplineRepresentationAddon_Init(interp);

  char pkgName[]=VTKTCLADDON_PKG_NAME;
  char pkgVers[]=VTKTCLADDON_VERSION_STRING;
  Tcl_PkgProvide(interp, pkgName, pkgVers);
  return TCL_OK;
}
