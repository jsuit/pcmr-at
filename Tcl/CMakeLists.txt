# look for tcl
find_package (TclStub REQUIRED)
if (TCL_FOUND)
  add_definitions (-DUSE_TCL_STUBS)
  add_definitions (-DTCL_THREADS)
  include_directories ( ${TCL_INCLUDE_PATH} )
else (TCL_FOUND)
  message ( SEND_ERROR "TCL extensions need Tcl Stubs library" )
endif (TCL_FOUND)

add_subdirectory( Wrapper )
add_subdirectory( vtkTclAddon )
add_subdirectory( lib )
